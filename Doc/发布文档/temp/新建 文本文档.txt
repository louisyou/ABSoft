
简版的DELPHI在安装后需加入环境变量中：C:\Program Files\Embarcadero\RAD Studio\8.0\bin;C:\Documents and Settings\All Users\Documents\RAD Studio\8.0\Bpl;

编译文件时常见问题:
1.DesignIntf.dcu 找不到
  解决方法:在DELPHI开发环境的包路径加入$(DELPHI)\Source\ToolsAPI
2.DockForm.dcu 找不到或[DCC Error] ToolsAPI.pas(1340): E2003 Undeclared identifier: 'TDockableForm'
  解决方法:1.修改ToolsAPI.pas单元中引用的DockForm为uDockForm,
           2.在DELPHI开发环境的包路径中加入C:\Documents and Settings\All Users\Documents\RAD Studio\6.0\Demos\DelphiWin32\VCLWin32\Docking
3.编译时报找不到文件 forms.dcu/controls.dcu 等问题
在*.dproj文件中找到如下內容
<DCC_Namespace>System;Xml;Data;Datasnap;Web;Soap;$(DCC_Namespace)</DCC_Namespace>
修改为
<DCC_Namespace>System;Xml;Data;Datasnap;Web;Soap;Vcl;Vcl.Imaging;Vcl.Touch;Vcl.Samples;Vcl.Shell;$(DCC_Namespace)</DCC_Namespace>
4.编译时报找不到文件 EditCharTeeProcs,TeEngine,Chart 等问题
因为高版本DELPHI自带了Teechar,但没有源码,路径放到了C:\Program Files\Embarcadero\RAD Studio\9.0\lib\win32\debug中,引用的单元名有前缀VCLTee.
所以1.在DELPHI开发环境的包路径中加入C:\Program Files\Embarcadero\RAD Studio\9.0\lib\win32\debug
    2.将单元名称修改为  VCLTee.EditChar,VCLTee.Series,VCLTee.TeeProcs, VCLTee.TeEngine, VCLTee.Chart



3、不带包编译时需加入的路径
D:\ABSoft\Code\Third\FastReport\FastScript;D:\ABSoft\Code\Third\FastReport\FastQB;D:\ABSoft\Code\Third\FastReport\Source\ADO;D:\ABSoft\Code\Third\FastReport\Source\DBX;D:\ABSoft\Code\Third\FastReport\Source\ExportPack;D:\ABSoft\Code\Third\FastReport\Source;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCore Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCore Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressBars\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressBars\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCommon Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCommon Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDataController\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDataController\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDBTree Suite\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDBTree Suite\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDocking Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDocking Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressEditors Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressEditors Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressExport Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressFlowChart\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressFlowChart\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressGDI+ Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressGDI+ Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLayout Control\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLayout Control\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLibrary\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLibrary\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressMemData\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressExport Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressMemData\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressNavBar\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressNavBar\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressOrgChart\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressOrgChart\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPageControl\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPageControl\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPivotGrid\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPivotGrid\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPrinting System\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPrinting System\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumGrid\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumGrid\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumTreeList\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumTreeList\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressScheduler\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressScheduler\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSkins Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSkins Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpreadSheet\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpreadSheet\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpreadSheet (deprecated)\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressVerticalGrid\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressVerticalGrid\Sources;D:\ABSoft\Code\Third\DevExpressVCL\XP Theme Manager\Packages;D:\ABSoft\Code\Third\DevExpressVCL\XP Theme Manager\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpellChecker\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpellChecker\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressTile Control\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressTile Control\Sources


