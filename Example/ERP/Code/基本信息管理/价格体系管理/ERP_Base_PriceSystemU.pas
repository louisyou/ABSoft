unit ERP_Base_PriceSystemU;

interface

uses
  ABFrameworkFuncFormU,
  ABFrameworkFuncU,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, dxStatusBar,
  ABFrameworkControlU, ABFrameworkDBPanelU, Vcl.ExtCtrls, ABPubPanelU,
  ABFrameworkDBNavigatorU, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, ABFrameworkcxGridU, cxGrid, ABFrameworkQueryU,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ABThirdDBU, ABThirdCustomQueryU,
  ABThirdQueryU, ABFrameworkDictionaryQueryU, dxBarBuiltInMenu, cxPC, cxSplitter,
  cxContainer, cxImage, cxDBEdit, ABFrameworkQuerySelectFieldPanelU, Vcl.Menus,
  Vcl.StdCtrls, cxButtons;

type
  TERP_Base_PriceSystemForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    ABDatasource2_1: TABDatasource;
    ABDatasource3_1: TABDatasource;
    ABQuery3_1: TABQuery;
    ABQuery2_1: TABQuery;
    ABDatasource3: TABDatasource;
    ABQuery3: TABQuery;
    ABQuery2: TABQuery;
    ABDatasource2: TABDatasource;
    ABDBNavigator3: TABDBNavigator;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet_11: TcxTabSheet;
    ABDBPanel1: TABDBPanel;
    ABcxPageControl3: TABcxPageControl;
    cxTabSheet1_1: TcxTabSheet;
    ABcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABDBNavigator2: TABDBNavigator;
    ABcxSplitter2: TABcxSplitter;
    ABcxSplitter1: TABcxSplitter;
    ABDBNavigator1: TABDBNavigator;
    cxTabSheet2: TcxTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    ABcxPageControl4: TABcxPageControl;
    cxTabSheet4: TcxTabSheet;
    ABDBPanel2: TABDBPanel;
    ABcxPageControl5: TABcxPageControl;
    cxTabSheet5: TcxTabSheet;
    ABcxGrid3: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABDBNavigator4: TABDBNavigator;
    ABcxSplitter3: TABcxSplitter;
    ABcxGrid4: TABcxGrid;
    ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView;
    cxGridLevel3: TcxGridLevel;
    ABcxSplitter4: TABcxSplitter;
    cxTabSheet3: TcxTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    ABcxPageControl6: TABcxPageControl;
    cxTabSheet6: TcxTabSheet;
    ABDBPanel3: TABDBPanel;
    ABcxPageControl7: TABcxPageControl;
    cxTabSheet7: TcxTabSheet;
    ABcxGrid5: TABcxGrid;
    ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView;
    cxGridLevel4: TcxGridLevel;
    ABDBNavigator5: TABDBNavigator;
    ABcxSplitter5: TABcxSplitter;
    ABcxGrid6: TABcxGrid;
    ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView;
    cxGridLevel5: TcxGridLevel;
    ABcxSplitter6: TABcxSplitter;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBNavigator7: TABDBNavigator;
    Panel7: TPanel;
    ABcxGrid1: TABcxGrid;
    ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxGrid1Level1: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
    procedure ABcxPageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ERP_Base_PriceSystemForm: TERP_Base_PriceSystemForm;

implementation
{$R *.dfm}
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TERP_Base_PriceSystemForm.ClassName;
end;

exports
   ABRegister ;


procedure TERP_Base_PriceSystemForm.ABcxPageControl1Change(Sender: TObject);
begin
  ABInitFormPageControl(
                        ABcxPageControl1,

                        ABDBNavigator3,
                        ABDBStatusBar1,

                        [ABDatasource1,ABDatasource2,ABDatasource3],
                        [[ABDBPanel1],[ABDBPanel2],[ABDBPanel3]],
                        [[ABcxGrid1ABcxGridDBBandedTableView1],[ABcxGridDBBandedTableView3],[ABcxGridDBBandedTableView5]],
                        [true,True,True],
                        [true,True,True],
                        [true,True,True],

                        [[ABDatasource1_1],[ABDatasource2_1],[ABDatasource3_1]],
                        [[[]],[[]],[[]]],
                        [[[ABcxGridDBBandedTableView1]],[[ABcxGridDBBandedTableView2]],[[ABcxGridDBBandedTableView4]]],
                        [[true],[true],[true]],
                        [[true],[true],[true]],
                        [[true],[true],[true]],

                        [true,True,True]
                        );
end;

procedure TERP_Base_PriceSystemForm.FormCreate(Sender: TObject);
begin
  ABcxPageControl1Change(ABcxPageControl1);
end;

initialization
  RegisterClass(TERP_Base_PriceSystemForm);
finalization
  UnRegisterClass(TERP_Base_PriceSystemForm);
end.

