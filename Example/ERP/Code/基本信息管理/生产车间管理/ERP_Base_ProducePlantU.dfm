object ERP_Base_ProducePlantForm: TERP_Base_ProducePlantForm
  Left = 0
  Top = 0
  Caption = #29983#20135#36710#38388#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object ABDBPanel1: TABDBPanel
    Left = 0
    Top = 25
    Width = 750
    Height = 144
    Align = alTop
    BevelOuter = bvNone
    Caption = 'ABDBPanel1'
    ShowCaption = False
    TabOrder = 1
    ReadOnly = False
    DataSource = ABDatasource1
    AddAnchors_akRight = True
    AddAnchors_akBottom = True
    AutoHeight = True
    AutoWidth = True
  end
  object ABDBNavigator1: TABDBNavigator
    Left = 0
    Top = 0
    Width = 750
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Caption = 'ABDBNavigator1'
    ShowCaption = False
    TabOrder = 2
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtMain
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbNull
    ApprovedCommitButton = nbNull
    ButtonNativeStyle = False
    BtnCaptions.Strings = (
      'BtnFirstRecord='
      'BtnPreviousRecord='
      'BtnNextRecord='
      'BtnLastRecord='
      'BtnInsert='
      'btnCopy='
      'BtnDelete='
      'BtnEdit='
      'BtnPost='
      'BtnCancel='
      'btnQuery='
      'BtnReport='
      'BtnCustom1='
      'BtnCustom2='
      'BtnCustom3='
      'BtnCustom4='
      'BtnCustom5='
      'BtnCustom6='
      'BtnCustom7='
      'BtnCustom8='
      'BtnCustom9='
      'BtnCustom10='
      'BtnExit=')
  end
  object ABcxGrid1: TABcxGrid
    Left = 0
    Top = 169
    Width = 750
    Height = 362
    Align = alClient
    TabOrder = 3
    object ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
      PopupMenu.AutoHotkeys = maManual
      PopupMenu.CloseFootStr = False
      PopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
      PopupMenu.AutoApplyBestFit = True
      PopupMenu.AutoCreateAllItem = True
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ABDatasource1
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Filter.AutoDataSetFilter = True
      DataController.Filter.TranslateBetween = True
      DataController.Filter.TranslateIn = True
      DataController.Filter.TranslateLike = True
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.DataRowSizing = True
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      Bands = <
        item
        end>
      ExtPopupMenu.AutoHotkeys = maManual
      ExtPopupMenu.CloseFootStr = False
      ExtPopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
      ExtPopupMenu.AutoApplyBestFit = True
      ExtPopupMenu.AutoCreateAllItem = True
    end
    object ABcxGrid1Level1: TcxGridLevel
      GridView = ABcxGrid1ABcxGridDBBandedTableView1
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select * from ERP_Base_ProducePlant')
    ConnName = 'ERP'
    SqlUpdateDatetime = 42213.524856666660000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_ProducePlant')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_ProducePlant')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 296
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 320
    Top = 296
  end
end
