unit ERP_Base_StorageU;

interface

uses
  ABFrameworkFuncFormU,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, dxStatusBar,
  ABFrameworkControlU, ABFrameworkDBPanelU, Vcl.ExtCtrls, ABPubPanelU,
  ABFrameworkDBNavigatorU, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, ABFrameworkcxGridU, cxGrid, ABFrameworkQueryU,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ABThirdDBU, ABThirdCustomQueryU,
  ABThirdQueryU, ABFrameworkDictionaryQueryU, dxBarBuiltInMenu, cxPC, cxSplitter,
  cxContainer, cxImage, cxDBEdit;

type
  TERP_Base_StorageForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    Panel1: TPanel;
    Panel3: TPanel;
    ABDBNavigator1: TABDBNavigator;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet_11: TcxTabSheet;
    ABcxPageControl3: TABcxPageControl;
    cxTabSheet1_1: TcxTabSheet;
    ABDBPanel1: TABDBPanel;
    ABcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABcxGrid1: TABcxGrid;
    ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxGrid1Level1: TcxGridLevel;
    ABcxSplitter1: TABcxSplitter;
    ABcxSplitter2: TABcxSplitter;
    ABDBNavigator2: TABDBNavigator;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ERP_Base_StorageForm: TERP_Base_StorageForm;

implementation
{$R *.dfm}
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TERP_Base_StorageForm.ClassName;
end;

exports
   ABRegister ;

procedure TERP_Base_StorageForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([ABDBPanel1],[ABcxGrid1ABcxGridDBBandedTableView1],[],ABQuery1);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],ABQuery1_1);
end;

initialization
  RegisterClass(TERP_Base_StorageForm);
finalization
  UnRegisterClass(TERP_Base_StorageForm);
end.
