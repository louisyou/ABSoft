//第三方包-->摄像头单元
unit ABExpandThirdVideoCapU;
                             
interface
// 关闭RTTI反射机制减少EXE文件尺寸
{$IF CompilerVersion >= 21.0}
{$WEAKLINKRTTI ON}
{$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,ComCtrls,
  ExtCtrls,StdCtrls,jpeg,Math,Videocap,Dialogs;


type
  TABVideoCapFrame = class(TFrame)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel2: TPanel;
    Button_From: TButton;
    Button_Format: TButton;
    Button_Zip: TButton;
    Button_StartAvi: TButton;
    Button_StopAvi: TButton;
    Button_Open: TButton;
    Button_Close: TButton;
    Button_GetPicture: TButton;
    VideoCap1: TVideoCap;
    Panel4: TPanel;
    Panel3: TPanel;
    ComboBox2: TComboBox;
    Button_CutOut: TButton;
    Button_GetPicture2: TButton;
    Button_Save: TButton;
    GroupBox1: TGroupBox;
    CutOutImage: TImage;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    InputImage: TImage;
    CutOutPanel: TPanel;
    Image1: TImage;
    procedure CutOutPanelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button_CloseClick(Sender: TObject);
    procedure Button_FromClick(Sender: TObject);
    procedure Button_FormatClick(Sender: TObject);
    procedure Button_ZipClick(Sender: TObject);
    procedure Button_StartAviClick(Sender: TObject);
    procedure Button_StopAviClick(Sender: TObject);
    procedure Button_GetPictureClick(Sender: TObject);
    procedure Button_CutOutClick(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure Button_OpenClick(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure VideoCap1DblClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
  private
    { Private declarations }
  public
    destructor Destroy; override;
    constructor Create(AOwner: TComponent); override;

    procedure VideoCap_Close;
    procedure VideoCap_Open;
    { Public declarations }
  end;

  TABVideoCap = class(Tpanel)
  private
    FFrame: TABVideoCapFrame;
    FOnlyCutOut: boolean;
    FActivePageIndex: Longint;
    FSaveButton: boolean;
    FOnABSave: TNotifyEvent;
    FOnABGetPicture: TNotifyEvent;
    FOnABCutOut: TNotifyEvent;
    FCloseSetButton: boolean;
    FViewDBImagePage: boolean;
    procedure SetActivePageIndex(const Value: Longint);
    procedure SetOnlyCutOut(const Value: boolean);
    procedure SetSaveButton(const Value: boolean);
    procedure SetCloseSetButton(const Value: boolean);
    procedure SetViewDBImagePage(const Value: boolean);
  protected
    procedure Paint;override;
    procedure Resize; override;
  public
	 constructor Create(AOwner: TComponent); override;
	 destructor Destroy; override;

    procedure Input(aBitmap: TBitmap);overload;
    procedure Input(aBmpFileName: String);overload;
  Published
    property Frame: TABVideoCapFrame     read FFrame     write FFrame     ;
    property OnlyCutOut: boolean     read FOnlyCutOut     write SetOnlyCutOut     ;
    property ActivePageIndex: Longint     read FActivePageIndex     write SetActivePageIndex  ;

    property SaveButton: boolean     read FSaveButton   write SetSaveButton   ;

    property OnABSave: TNotifyEvent read FOnABSave write FOnABSave;
    property OnABGetPicture: TNotifyEvent read FOnABGetPicture write FOnABGetPicture;
    property OnABCutOut: TNotifyEvent read FOnABCutOut write FOnABCutOut;

    property CloseSetButton: boolean     read FCloseSetButton   write SetCloseSetButton ;
    property ViewDBImagePage: boolean     read FViewDBImagePage   write SetViewDBImagePage ;
  end;

procedure Register;


implementation

{$R *.dfm}

procedure Register;
begin
  RegisterComponents('ABExpandThird',
    [
    TABVideoCap
    ]);
end;

procedure TABVideoCapFrame.Button_SaveClick(Sender: TObject);
begin
  if (Assigned(CutOutImage.Picture)) and (Assigned(CutOutImage.Picture.Graphic)) then
  begin
    if Assigned(TABVideoCap(owner).OnABSave) then
      TABVideoCap(owner).OnABSave(self);
  end;
end;

procedure TABVideoCapFrame.Button_CutOutClick(Sender: TObject);
var
  bmp: TBitmap;
  jpeg: TJPEGImage;
  Picture: TPicture;
begin
  if (Assigned(InputImage.Picture)) and (Assigned(InputImage.Picture.Graphic)) then
  begin
    if ComboBox2.ItemIndex=2 then
    begin
      //ABChangPICSize(InputImage.Picture.Graphic,InputImage.Picture,InputImage.Width,InputImage.Height);
      CutOutImage.Picture.Assign(InputImage.Picture);
    end
    else
    begin
      bmp := tbitmap.Create;
      jpeg := TJPEGImage.Create;
      Picture := TPicture.Create;
      //因为InputImage使用了Stretch，所以不能直接CopyRect块
      Picture.Bitmap.Width := InputImage.Width;
      Picture.Bitmap.Height := InputImage.Height;
      Picture.Bitmap.PixelFormat := pf32bit;
      Picture.Bitmap.Canvas.StretchDraw(Rect(0, 0,InputImage.Width, InputImage.Height), InputImage.Picture.Bitmap);

      jpeg.Assign(Picture.Graphic);
      bmp.Assign(jpeg);
      try
        CutOutImage.Canvas.CopyRect(
          bounds(0, 0, CutOutImage.Width, CutOutImage.Height),
          bmp.Canvas,
          Bounds(CutOutPanel.Left, CutOutPanel.top, CutOutPanel.Width, CutOutPanel.Height));

        if Assigned(TABVideoCap(owner).OnABCutOut) then
          TABVideoCap(owner).OnABCutOut(self);
      finally
        bmp.Free;
        jpeg.Free;
        Picture.Free;
      end;
    end;
  end;
end;

procedure TABVideoCapFrame.Button_FromClick(Sender: TObject);
begin
  VideoCap1.DlgVSource;
end;

procedure TABVideoCapFrame.Button_FormatClick(Sender: TObject);
begin
  VideoCap1.DlgVFormat;
  VideoCap_Close;
  VideoCap_Open;
end;

procedure TABVideoCapFrame.Button_ZipClick(Sender: TObject);
begin
  VideoCap1.DlgVCompression;
end;

procedure TABVideoCapFrame.Button_StartAviClick(Sender: TObject);
begin
  VideoCap1.StartCapture;
end;

procedure TABVideoCapFrame.Button_StopAviClick(Sender: TObject);
  function ABSaveFile(aDefaultDirOrPath: string; aDefaultFile: string; aFilter: string;aOwner:TComponent):
    string;
  var
    SaveDialog1: TSaveDialog;
  begin
    Result := EmptyStr;
    SaveDialog1 := TSaveDialog.Create(aOwner);
    try
      if SysUtils.DirectoryExists(aDefaultDirOrPath) then
        SaveDialog1.InitialDir := aDefaultDirOrPath;

      if aDefaultFile<>EmptyStr then
        SaveDialog1.FileName := aDefaultFile;

      SaveDialog1.Filter := aFilter;
      if (SaveDialog1.Execute) then
      begin
        result := SaveDialog1.FileName;
      end;
    finally
      SaveDialog1.Free;
    end;
  end;
var
  tempFileName,tempDFileName:string;
begin
  if VideoCap1.StopCapture then
  begin
    tempFileName:=emptystr;
    if FileExists(ExtractFilePath(Application.EXEName) + 'tempVideoCapAviFile.avi') then
    begin
      tempFileName:=ExtractFilePath(Application.EXEName) + 'tempVideoCapAviFile.avi';
    end;
    tempDFileName:=ABSaveFile('','','PAK FILES|*.avi',nil);
    try
      if (tempFileName<>emptystr) and (tempDFileName<>emptystr) then
        MoveFile(PChar(tempFileName),PChar(tempDFileName))
    finally
      //ABDeleteFile(tempFileName)
    end;
  end;
end;

procedure TABVideoCapFrame.Button_OpenClick(Sender: TObject);
begin
  VideoCap_Open;
end;

procedure TABVideoCapFrame.Button_CloseClick(Sender: TObject);
begin
  VideoCap_Close;
end;

procedure TABVideoCapFrame.Button_GetPictureClick(Sender: TObject);
var
  tempFileName:string;
begin
  VideoCap_Open;
  if not VideoCap1.DriverOpen then
     exit;

  if sender=Button_GetPicture2 then
  begin
    PageControl1.ActivePageIndex:=0;
    Sleep(100);
    PageControl1.ActivePageIndex:=1;
  end;

  if VideoCap1.SaveAsDIB then
  begin
    if (VideoCap1.CapWidth<>0) and (VideoCap1.CapHeight<>0) and
       (Panel1.Width<>0) and (Panel1.Height<>0) then
    begin
      InputImage.Width:=trunc(VideoCap1.CapWidth/max(VideoCap1.CapWidth/Panel1.Width,VideoCap1.CapHeight/Panel1.Height));
      InputImage.Height:=trunc(VideoCap1.CapHeight/max(VideoCap1.CapWidth/Panel1.Width,VideoCap1.CapHeight/Panel1.Height));
      if ComboBox2.ItemIndex=2 then
      begin
        CutOutPanel.Left := InputImage.Left;
        CutOutPanel.Top := InputImage.Top;
        CutOutPanel.Height := InputImage.Height;
        CutOutPanel.Width := InputImage.Width;
      end
    end;

    tempFileName:=emptystr;
    if FileExists(ExtractFilePath(Application.EXEName) + 'tempVideoCapPictureFile.bmp') then
    begin
      tempFileName:=ExtractFilePath(Application.EXEName) + 'tempVideoCapPictureFile.bmp';
    end;
    try
      if tempFileName<>emptystr then
      begin
        InputImage.Picture.LoadFromFile(tempFileName);
      end;

      CutOutPanel.Repaint;
      CutOutPanel.Refresh;

      InputImage.Repaint;
      InputImage.Refresh;

      if Assigned(TABVideoCap(owner).OnABGetPicture) then
        TABVideoCap(owner).OnABGetPicture(self);
    finally
      //ABDeleteFile(tempFileName)
    end;
  end;
end;

//初始化摄像头连接
procedure TABVideoCapFrame.VideoCap_Open;
begin
  if not VideoCap1.DriverOpen then
  begin
    VideoCap1.DriverIndex := 0;
    VideoCap1.DriverOpen := True;
    VideoCap1.VideoPreview := True;

    ComboBox2Change(ComboBox2);
  end;
end;

//退出程序前断开摄像头连接
procedure TABVideoCapFrame.VideoCap1DblClick(Sender: TObject);
begin
  TABVideoCap(owner).CloseSetButton :=not TABVideoCap(owner).CloseSetButton;
end;

procedure TABVideoCapFrame.VideoCap_Close;
begin
  if VideoCap1.DriverOpen then
  begin
    VideoCap1.DriverOpen := False;
  end;
end;

constructor TABVideoCapFrame.Create(AOwner: TComponent);
begin
  inherited;
  Width:=650;
  Height:=550;
end;

destructor TABVideoCapFrame.Destroy;
begin

  inherited;
end;

procedure TABVideoCapFrame.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange:=not TABVideoCap(self).OnlyCutOut;
end;

//照片尺寸选择
procedure TABVideoCapFrame.ComboBox2Change(Sender: TObject);
begin
  if ComboBox2.ItemIndex=0 then              
  begin
    if Assigned(CutOutImage.Picture) then
      CutOutImage.Picture.Graphic := nil;
    CutOutPanel.Height := 155;          
    CutOutPanel.Width := 110;
    
    CutOutImage.Height := 155;
    CutOutImage.Width := 110;
  end
  else if ComboBox2.ItemIndex=1 then
  begin               
    if Assigned(CutOutImage.Picture) then
      CutOutImage.Picture.Graphic := nil;
    CutOutPanel.Height := 197;
    CutOutPanel.Width := 130;
    
    CutOutImage.Height := 197;
    CutOutImage.Width := 130;
  end
  else if ComboBox2.ItemIndex=2 then
  begin
    if Assigned(CutOutImage.Picture) then
      CutOutImage.Picture.Graphic := nil;

    CutOutPanel.Left := InputImage.Left;
    CutOutPanel.Top := InputImage.Top;
    CutOutPanel.Height := InputImage.Height;
    CutOutPanel.Width := InputImage.Width;

    CutOutImage.Height := 197;
    CutOutImage.Width := 130;
  end
end;

//移动裁剪框
procedure TABVideoCapFrame.CutOutPanelMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_DragMove = $F012; {   a   magic   number   }
begin
  //释放鼠标捕获
  ReleaseCapture;

  CutOutPanel.Perform(WM_SysCommand, SC_DragMove, 0);
  if CutOutPanel.Top < InputImage.Top then
    CutOutPanel.top := InputImage.Top;
  if CutOutPanel.left < InputImage.Left then
    CutOutPanel.Left := InputImage.Left;
  if (CutOutPanel.Top + CutOutPanel.Height) > (InputImage.top + InputImage.Height) then
    CutOutPanel.Top := InputImage.Top + InputImage.Height - CutOutPanel.Height;
  if (CutOutPanel.left + CutOutPanel.width) > (InputImage.Left + InputImage.width) then
    CutOutPanel.left := InputImage.Left + InputImage.width - CutOutPanel.width;
end;

{ TABVideoCap }

//输入照片
procedure TABVideoCap.Input(aBmpFileName: String);
var
  bmp: TBitmap;
begin
  bmp := TBitmap.Create;
  try
    bmp.LoadFromFile(aBmpFileName);
    Input(bmp);
  finally
    bmp.free;
  end;
end;

//输入照片
procedure TABVideoCap.Input(aBitmap: TBitmap);
var
  jpeg: TJPEGImage;
begin
  jpeg := TJPEGImage.Create;
  try
    jpeg.Assign(aBitmap);
    FFrame.InputImage.Picture.Graphic := jpeg;
    FFrame.CutOutPanel.Refresh;
  finally
    jpeg.free;
  end;
end;

constructor TABVideoCap.Create(AOwner: TComponent);
begin
  inherited;
  FOnlyCutOut:=false;
  Caption:=EmptyStr;
  FFrame:= TABVideoCapFrame.Create(self);
  FFrame.Parent:=Self;
  if not FOnlyCutOut then
    FFrame.PageControl1.ActivePageIndex:=0
  else
    FFrame.PageControl1.ActivePageIndex:=1;

  FViewDBImagePage := False;
  FFrame.TabSheet3.TabVisible:=False;
  FFrame.Show;
end;

destructor TABVideoCap.Destroy;
begin
  FFrame.VideoCap_Close;
  FFrame.Free;
  inherited;
end;

procedure TABVideoCap.Resize;
begin
  inherited;
end;

procedure TABVideoCap.SetActivePageIndex(const Value: Longint);
begin
  FActivePageIndex := Value;
  Frame.PageControl1.ActivePageIndex:=FActivePageIndex;
end;

procedure TABVideoCap.SetCloseSetButton(const Value: boolean);
begin
  FCloseSetButton := Value;
  FFrame.Panel2.Visible:=not FCloseSetButton;

  if FCloseSetButton then
    FFrame.Panel2.height:=0
  else
    FFrame.Panel2.height:=40;
end;

procedure TABVideoCap.SetOnlyCutOut(const Value: boolean);
begin
  FOnlyCutOut:=Value;
  if FOnlyCutOut then
  begin
    FFrame.PageControl1.ActivePageIndex:= 1;
  end;
end;

procedure TABVideoCap.SetSaveButton(const Value: boolean);
begin
  FSaveButton := Value;
  Frame.Button_Save.Visible:=FSaveButton;
  if Frame.Button_Save.Visible then
  begin
    Frame.Button_Save.Width:=45;

    Frame.Button_GetPicture2.Left    := 5  ;
    Frame.Button_GetPicture2.Top     := 28 ;
    Frame.Button_GetPicture2.Width   := 45 ;
    Frame.Button_GetPicture2.Height  := 25 ;

    Frame.Button_CutOut.Left   := 50 ;
    Frame.Button_CutOut.Top    := 28 ;
    Frame.Button_CutOut.Width  := 45 ;
    Frame.Button_CutOut.Height := 25 ;

    Frame.Button_Save.Left   := 95 ;
    Frame.Button_Save.Top    := 28 ;
    Frame.Button_Save.Width  := 45 ;
    Frame.Button_Save.Height := 25 ;
  end
  else
  begin
    Frame.Button_Save.Width:=0;

    Frame.Button_GetPicture2.Left    := 6  ;
    Frame.Button_GetPicture2.Top     := 28 ;
    Frame.Button_GetPicture2.Width   := 65 ;
    Frame.Button_GetPicture2.Height  := 25 ;

    Frame.Button_CutOut.Left   := 75 ;
    Frame.Button_CutOut.Top    := 28 ;
    Frame.Button_CutOut.Width  := 65 ;
    Frame.Button_CutOut.Height := 25 ;
  end;
end;

procedure TABVideoCap.SetViewDBImagePage(const Value: boolean);
begin
  FViewDBImagePage := Value;
  FFrame.TabSheet3.TabVisible:=FViewDBImagePage;
end;

procedure TABVideoCap.Paint;
begin
  inherited;
  {
  if  FFrame.Width<>Width then
    FFrame.Width:=Width;
  if  FFrame.Height<>Height then
    FFrame.Height:=Height;
    }
end;


end.



