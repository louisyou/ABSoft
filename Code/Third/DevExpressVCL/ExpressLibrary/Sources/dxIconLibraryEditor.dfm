object dxfmImagePicker: TdxfmImagePicker
  Left = 646
  Top = 345
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Image Picker'
  ClientHeight = 414
  ClientWidth = 546
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 380
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShortCut = FormShortCut
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TcxButton
    Left = 383
    Top = 383
    Width = 75
    Height = 24
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    LookAndFeel.NativeStyle = True
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TcxButton
    Left = 464
    Top = 383
    Width = 75
    Height = 24
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    LookAndFeel.NativeStyle = True
    ModalResult = 2
    TabOrder = 2
  end
  object cxPageControl1: TcxPageControl
    Left = 8
    Top = 8
    Width = 530
    Height = 368
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    Properties.ActivePage = tsDXImageGallery
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.NativeStyle = True
    OnChange = cxPageControl1Change
    ClientRectBottom = 364
    ClientRectLeft = 4
    ClientRectRight = 526
    ClientRectTop = 24
    object tsPictureEditor: TcxTabSheet
      Caption = 'Picture Editor'
      object btnLoad: TcxButton
        Left = 440
        Top = 8
        Width = 75
        Height = 23
        Anchors = [akTop, akRight]
        Caption = '&Load...'
        LookAndFeel.NativeStyle = True
        TabOrder = 0
        OnClick = btnLoadClick
      end
      object btnSave: TcxButton
        Left = 440
        Top = 37
        Width = 75
        Height = 23
        Anchors = [akTop, akRight]
        Caption = '&Save...'
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        OnClick = btnSaveClick
      end
      object btnClear: TcxButton
        Left = 440
        Top = 66
        Width = 75
        Height = 23
        Anchors = [akTop, akRight]
        Caption = '&Clear'
        LookAndFeel.NativeStyle = True
        TabOrder = 2
        OnClick = btnClearClick
      end
      object Panel1: TPanel
        Left = 8
        Top = 8
        Width = 425
        Height = 324
        Anchors = [akLeft, akTop, akRight, akBottom]
        BorderStyle = bsSingle
        Color = clWhite
        TabOrder = 3
        object ImagePaintBox: TPaintBox
          Left = 1
          Top = 1
          Width = 419
          Height = 318
          Align = alClient
          OnPaint = ImagePaintBoxPaint
        end
      end
      object btnImport: TcxButton
        Left = 440
        Top = 98
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '&Import...'
        DropDownMenu = pmImport
        Kind = cxbkOfficeDropDown
        LookAndFeel.NativeStyle = True
        TabOrder = 4
        OnClick = btnImportClick
      end
    end
    object tsDXImageGallery: TcxTabSheet
      Caption = 'DevExpress Icon Library'
      object beFind: TcxButtonEdit
        Left = 369
        Top = 7
        Anchors = [akTop, akRight]
        Properties.Buttons = <
          item
            Default = True
            ImageIndex = 0
            Kind = bkGlyph
          end>
        Properties.Images = cxImageList1
        Properties.OnButtonClick = beFindPropertiesButtonClick
        Properties.OnChange = beFindPropertiesChange
        TabOrder = 3
        Width = 146
      end
      object cxLabel1: TcxLabel
        Left = 8
        Top = 21
        Caption = 'Categories'
        Style.TransparentBorder = False
        Transparent = True
      end
      object cxLabel2: TcxLabel
        Left = 8
        Top = 205
        Anchors = [akLeft, akBottom]
        Caption = 'Size'
        Style.TransparentBorder = False
        Transparent = True
      end
      object cxLabel3: TcxLabel
        Left = 8
        Top = 266
        Anchors = [akLeft, akBottom]
        Caption = 'Collection'
        Style.TransparentBorder = False
        Transparent = True
      end
      object gcIcons: TdxGalleryControl
        Left = 167
        Top = 36
        Width = 347
        Height = 296
        Anchors = [akLeft, akTop, akRight, akBottom]
        PopupMenu = pmSelection
        LookAndFeel.NativeStyle = True
        OptionsBehavior.ItemMultiSelectKind = imskListView
        OptionsBehavior.ItemCheckMode = icmMultiple
        OptionsBehavior.ItemShowHint = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Item.Image.ShowFrame = False
        OnDblClick = gcIconsDblClick
      end
      object clbCategories: TcxCheckListBox
        Left = 8
        Top = 36
        Width = 150
        Height = 160
        Anchors = [akLeft, akTop, akBottom]
        Items = <>
        PopupMenu = pmSelection
        Sorted = True
        Style.StyleController = cxEditStyleController1
        Style.TransparentBorder = False
        TabOrder = 0
        OnClickCheck = clbCategoriesClickCheck
      end
      object clbSize: TcxCheckListBox
        Left = 8
        Top = 220
        Width = 150
        Height = 36
        Anchors = [akLeft, akBottom]
        Items = <>
        PopupMenu = pmSelection
        Style.StyleController = cxEditStyleController1
        Style.TransparentBorder = False
        TabOrder = 1
        OnClickCheck = clbCollectionClickCheck
      end
      object clbCollection: TcxCheckListBox
        Left = 8
        Top = 281
        Width = 150
        Height = 51
        Anchors = [akLeft, akBottom]
        Items = <>
        PopupMenu = pmSelection
        Style.StyleController = cxEditStyleController1
        Style.TransparentBorder = False
        TabOrder = 2
        OnClickCheck = clbCollectionClickCheck
      end
    end
  end
  object OpenDialog: TOpenPictureDialog
    Filter = 
      'All (*.bmp;*.ico;*.emf;*.wmf)|*.bmp;*.ico;*.emf;*.wmf|Bitmaps (*' +
      '.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf' +
      '|Metafiles (*.wmf)|*.wmf|Metafiles (*.png)|*.png'
    Left = 436
    Top = 4
  end
  object SaveDialog: TSavePictureDialog
    Filter = 
      'All (*.bmp;*.ico;*.emf;*.wmf)|*.bmp;*.ico;*.emf;*.wmf|Bitmaps (*' +
      '.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf' +
      '|Metafiles (*.wmf)|*.wmf'
    Options = [ofOverwritePrompt, ofEnableSizing]
    Left = 468
    Top = 4
  end
  object cxImageList1: TcxImageList
    FormatVersion = 1
    DesignInfo = 131576
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000020000000E0B14308329448DFB1D2F58A5000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000020000000E0D1937883C6DB2FF5BB1F9FF325196F4000000000000
          00000000000100000004000000090000000D0000000F0000000F0000000C0000
          00070000000E0F1D3C864A7CBCFF73C4FFFF467CC3FF17254485000000000000
          0002000000081C130F465A3B31BC7C5043F87F5244FF7B4E42FA57382FC11E14
          1059112142875686C2FF88D0FFFF5186C7FF142343880000000F000000010302
          02104A332C91946B5DFDC6ACA1FFE4D1C6FFEDDDD2FFE2D0C5FFC0A599FF855C
          50FF6E6B7EFF98D4F8FF5B8ECBFF152545840000000D00000002000000076046
          3DA6B39288FFE9DAD0FFDAC0A1FFCBA87AFFC49B66FFCCAA7EFFDCC2A5FFE5D2
          C6FF9A766AFF736A77FF162747850000000E00000002000000002A201D4AAE88
          7CFFEFE6DFFFCDA67CFFCDA26BFFE3C28CFFEDD5A2FFE7CD9EFFD3B182FFD0AE
          88FFE7D5CAFF885F53FF25181464000000070000000000000000755B53ACDFCE
          C9FFDDC1A8FFC99865FFE8BE83FFE9C388FFEDCA97FFEFD3A7FFF2D9B0FFD5B1
          87FFDBBEA6FFC5ACA2FF5A3D33C10000000C0000000000000000A9877CE9F8F4
          F2FFC79873FFDEAB77FFEFCDABFFF0D0B1FFEDC9A1FFECC69AFFEFCFA9FFE9C9
          A4FFC89B77FFE6D6CEFF7C5448F10000000F0000000000000000C09C90FFFDFD
          FCFFBE875FFFEDCFB9FFF5DFD2FFF2D6C1FFF1CFB4FFEDC6A4FFECC19BFFEFC8
          A6FFC08B67FFF1E6DFFF8B6154FF0000000F0000000000000000AF9186E6F9F5
          F4FFC69474FFE8CDC3FFF9E8E4FFF6DED2FFF3D4C2FFF0CBB2FFEBB78EFFE5B7
          92FFC59172FFEBDFD9FF866055EE0000000D0000000000000000876F68B0E7D9
          D4FFE2C6B7FFC89072FFFAEFF2FFF9E7E4FFF6DDD3FFF1C8B2FFEBAF88FFC98E
          6CFFDCBBAAFFD3C0B7FF6B4F46BC00000009000000000000000026201E36CCAF
          A7FAFBF8F7FFCF9F88FFC78E72FFE9CDC6FFEDC7B5FFDD9F79FFC88865FFCE9D
          84FFF5EFEBFFB39387FF2A201D52000000040000000000000000000000036454
          4F84D9C2BAFFFDFBFAFFE2C6B8FFCB977EFFC08163FFCB977DFFE0C4B4FFFAF6
          F5FFC9B0A7FF6B564EA700000009000000010000000000000000000000000202
          020762534D81CEB2A9FAEADDD8FFF9F5F4FFFFFFFFFFF9F5F4FFE9DCD7FFC8AC
          A2FC62504B900404031000000002000000000000000000000000000000000000
          000000000003241F1D3486726BADB69B91E6CCADA1FFB99C92E988736CB22822
          1F3E000000060000000100000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000020000000C05031A46110852AB190C76E31D0E89FF1C0E89FF190C
          76E4120852AD06031B4D0000000E000000030000000000000000000000000000
          000301010519130A55A9211593FF2225AEFF2430C2FF2535CBFF2535CCFF2430
          C3FF2225AFFF211594FF140B58B20101051E0000000400000000000000020101
          03151C1270CD2522A6FF2D3DCCFF394BD3FF3445D1FF2939CDFF2839CDFF3344
          D0FF394AD4FF2D3CCDFF2523A8FF1C1270D20101051D00000003000000091912
          5BA72A27AAFF2F41D0FF3541C7FF2726ABFF3137BCFF384AD3FF384BD3FF3137
          BCFF2726ABFF3540C7FF2E40D0FF2927ACFF1A115EB10000000D08061C3D3129
          A2FD2C3CCCFF3842C6FF5F5DBDFFEDEDF8FF8B89CEFF3337B9FF3437B9FF8B89
          CEFFEDEDF8FF5F5DBDFF3741C6FF2B3ACDFF3028A4FF0907204A1E185F9F373B
          BCFF3042D0FF2621A5FFECE7ECFFF5EBE4FFF8F2EEFF9491D1FF9491D1FFF8F1
          EDFFF3E9E2FFECE6EBFF2621A5FF2E3FCFFF343ABEFF201A66B0312A92E03542
          CBFF3446D1FF2C2FB5FF8070ADFFEBDBD3FFF4EAE4FFF7F2EDFFF8F1EDFFF4E9
          E2FFEADAD1FF7F6FACFF2B2EB5FF3144D0FF3040CBFF312A95E53E37AEFA3648
          D0FF374AD3FF3A4ED5FF3234B4FF8A7FB9FFF6ECE7FFF5ECE6FFF4EBE5FFF6EB
          E5FF897DB8FF3233B4FF384BD3FF3547D2FF3446D1FF3E37AEFA453FB4FA4557
          D7FF3B50D5FF4C5FDAFF4343B7FF9189C7FFF7EFE9FFF6EEE9FFF6EFE8FFF7ED
          E8FF9087C5FF4242B7FF495DD8FF394CD4FF3F52D4FF443FB3FA403DA1DC5967
          DAFF5B6EDDFF4F4DBAFF8F89CAFFFBF6F4FFF7F1ECFFEDE1D9FFEDE0D9FFF7F0
          EAFFFAF5F2FF8F89CAFF4E4DB9FF576ADCFF5765D9FF403EA4E12E2D70987C85
          DDFF8798E8FF291D9BFFE5DADEFFF6EEEBFFEDDFDAFF816EA9FF816EA9FFEDDF
          D8FFF4ECE7FFE5D9DCFF291D9BFF8494E7FF7A81DDFF33317BAC111125356768
          D0FC9EACEDFF686FCEFF5646A1FFCCB6BCFF7A68A8FF4C4AB6FF4D4BB7FF7A68
          A8FFCBB5BCFF5646A1FF666DCCFF9BAAEEFF696CD0FD1212273F000000043B3B
          79977D84DFFFA5B6F1FF6D74D0FF2D219BFF5151B9FF8EA2ECFF8EA1ECFF5252
          BBFF2D219BFF6B72D0FFA2B3F0FF8086E0FF404183A700000008000000010303
          050C4E509DBC8087E2FFAEBDF3FFA3B6F1FF9DAFF0FF95A9EEFF95A8EEFF9BAD
          EFFFA2B3F0FFACBCF3FF838AE3FF4F52A0C10303051100000002000000000000
          000100000005323464797378D9F8929CEAFFA1AEEFFFB0BFF3FFB0BFF4FFA2AE
          EFFF939DE9FF7479DAF83234647D000000080000000200000000000000000000
          000000000000000000031213232D40437D935D61B5D07378DFFC7378DFFC5D61
          B5D040437D951212223000000004000000010000000000000000}
      end>
  end
  object cxEditStyleController1: TcxEditStyleController
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.NativeStyle = True
    Left = 404
    Top = 6
    PixelsPerInch = 96
  end
  object ActionList1: TActionList
    Left = 368
    Top = 8
    object actF3: TAction
      Caption = 'actF3'
      ShortCut = 114
      OnExecute = actF3Execute
    end
  end
  object pmSelection: TPopupMenu
    OnPopup = pmSelectionPopup
    Left = 504
    Top = 32
    object miCheckSelected: TMenuItem
      Tag = 1
      Caption = '&Check Selected'
      OnClick = miUncheckSelectedClick
    end
    object miUncheckSelected: TMenuItem
      Caption = '&Uncheck Selected'
      OnClick = miUncheckSelectedClick
    end
    object miLine1: TMenuItem
      Caption = '-'
    end
    object miSelectAll: TMenuItem
      Tag = 1
      Caption = '&Select All'
      ShortCut = 16449
      OnClick = miSelectClick
    end
    object miSelectAllinThisGroup: TMenuItem
      Tag = 1
      Caption = 'Select All in Group'
      OnClick = miSelectAllinThisGroupClick
    end
    object miSelectNone: TMenuItem
      Caption = '&Deselect All'
      OnClick = miSelectClick
    end
    object miUnselectAllinThisGroup: TMenuItem
      Caption = 'Deselect All in Group'
      OnClick = miSelectAllinThisGroupClick
    end
  end
  object pmImport: TPopupMenu
    Left = 448
    Top = 176
  end
end
