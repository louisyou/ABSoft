{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Export.Rtf.Keywords;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Generics.Collections, dxRichEdit.DocumentModel.PieceTable;

type

  TdxRtfExportSR = class
  public const
    OpenGroup = '{';
    CloseGroup = '}';
    RtfSignature = '\rtf1';

    DefaultFontIndex = '\deff';
    StyleTable = '\stylesheet';
    ColorTable = '\colortbl';
    FontTable = '\fonttbl';
    FontCharset = '\fcharset';
    UserTable = '\*\protusertbl';

    DocumentInformation = '\info';
    Password = '\*\password';
    PasswordHash = '\*\passwordhash';
    EnforceProtection = '\enforceprot';
    AnnotationProtection = '\annotprot';
    ReadOnlyProtection = '\readprot';
    ProtectionLevel = '\protlevel';
    NoUICompatible = '\nouicompat';  
    DefaultTabWidth = '\deftab';
    HyphenateDocument = '\hyphauto';
    PageFacing = '\facingp';
    DisplayBackgroundShape = '\viewbksp';

    ColorRed = '\red';
    ColorGreen = '\green';
    ColorBlue = '\blue';

    ResetParagraphProperties = '\pard';
    FrameHorizontalPosition = '\posx';
    FrameVerticalPosition = '\posy';
    FrameWidth = '\absw';
    FrameHeight = '\absh';

    TopParagraphBorder = '\brdrt';
    BottomParagraphBorder = '\brdrb';
    LeftParagraphBorder = '\brdrl';
    RightParagraphBorder = '\brdrr';

    ParagraphHorizontalPositionTypeMargin = '\phmrg';
    ParagraphHorizontalPositionTypePage = '\phpg';
    ParagraphHorizontalPositionTypeColumn = '\phcol';
    ParagraphVerticalPositionTypeMargin = '\pvmrg';
    ParagraphVerticalPositionTypePage = '\pvpg';
    ParagraphVerticalPositionTypeLine = '\pvpara';
    ResetCharacterFormatting = '\plain';
    EndOfParagraph = '\par';
    LeftAlignment = '\ql';
    RightAlignment = '\qr';
    CenterAlignment = '\qc';
    JustifyAlignment = '\qj';
    FirstLineIndentInTwips = '\fi';
    LeftIndentInTwips = '\li';
    LeftIndentInTwips_Lin = '\lin';
    RightIndentInTwips = '\ri';
    RightIndentInTwips_Rin = '\rin';
    AutomaticParagraphHyphenation = '\hyphpar';
    SuppressLineNumbering = '\noline';
    ContextualSpacing = '\contextualspace';
    PageBreakBefore = '\pagebb';
    BeforeAutoSpacing = '\sbauto';
    AfterAutoSpacing = '\saauto';
    KeepWithNext = '\keepn';
    KeepLinesTogether = '\keep';
    WidowOrphanControlOn = '\widctlpar';
    WidowOrphanControlOff = '\nowidctlpar';
    OutlineLevel = '\outlinelevel';
    ParagraphBackgroundColor = '\cbpat';
    RtfLineSpacingValue = '\sl';
    RtfLineSpacingMultiple = '\slmult';
    SpaceBefore = '\sb';
    SpaceAfter = '\sa';
    ListIndex = '\ls';
    LevelIndex = '\ilvl';
    AlternativeText = '\listtext';
    ParagraphNumerationText = '\*\pntext';

    CenteredTab = '\tqc';
    DecimalTab = '\tqdec';
    FlushRightTab = '\tqr';
    TabLeaderDots = '\tldot';
    TabLeaderEqualSign = '\tleq';
    TabLeaderHyphens = '\tlhyph';
    TabLeaderMiddleDots = '\tlmdot';
    TabLeaderThickLine = '\tlth';
    TabLeaderUnderline = '\tlul';
    TabPosition = '\tx';

    AllCapitals = '\caps';
    LangInfo = '\lang';
    LangInfo1 = '\langfe';
    LangInfo2 = '\langnp';
    NoProof = '\noproof';
    HiddenText = '\v';
    FontBold = '\b';
    FontItalic = '\i';
    FontStrikeout = '\strike';
    FontDoubleStrikeout = '\striked1';
    FontUnderline = '\ul';
    FontUnderlineDotted = '\uld';
    FontUnderlineDashed = '\uldash';
    FontUnderlineDashDotted = '\uldashd';
    FontUnderlineDashDotDotted = '\uldashdd';
    FontUnderlineDouble = '\uldb';
    FontUnderlineHeavyWave = '\ulhwave';
    FontUnderlineLongDashed = '\ulldash';
    FontUnderlineThickSingle = '\ulth';
    FontUnderlineThickDotted = '\ulthd';
    FontUnderlineThickDashed = '\ulthdash';
    FontUnderlineThickDashDotted = '\ulthdashd';
    FontUnderlineThickDashDotDotted = '\ulthdashdd';
    FontUnderlineThickLongDashed = '\ulthldash';
    FontUnderlineDoubleWave = '\ululdbwave';
    FontUnderlineWave = '\ulwave';
    FontUnderlineWordsOnly = '\ulw';
    FontNumber = '\f';
    FontSize = '\fs';
    RunBackgroundColor = '\chcbpat';  
    RunBackgroundColor2 = '\highlight'; 
    RunForegroundColor = '\cf';
    RunUnderlineColor = '\ulc';
    RunSuperScript = '\super';
    RunSubScript = '\sub';

    Picture = '\pict';

    PictureWidth = '\picw';
    PictureHeight = '\pich';
    PictureDesiredWidth = '\picwgoal';
    PictureDesiredHeight = '\pichgoal';
    PictureScaleX = '\picscalex';
    PictureScaleY = '\picscaley';
    ShapePicture = '\*\shppict';
    NonShapePicture = '\nonshppict';
    DxImageUri = '\*\dximageuri';

    Space = ' ';
    CLRF = #13#10;

    ResetSectionProperties = '\sectd';
    SectionEndMark = '\sect';
    SectionMarginsLeft = '\marglsxn';
    SectionMarginsRight = '\margrsxn';
    SectionMarginsTop = '\margtsxn';
    SectionMarginsBottom = '\margbsxn';
    SectionMarginsHeaderOffset = '\headery';
    SectionMarginsFooterOffset = '\footery';
    SectionMarginsGutter = '\guttersxn';
    SectionFirstPageHeader = '\headerf';
    SectionOddPageHeader = '\headerr';
    SectionEvenPageHeader = '\headerl';
    SectionFirstPageFooter = '\footerf';
    SectionOddPageFooter = '\footerr';
    SectionEvenPageFooter = '\footerl';

    SectionPageWidth = '\pgwsxn';
    SectionPageHeight = '\pghsxn';
    SectionPageLandscape = '\lndscpsxn';
    PaperKind = '\psz';
    SectionFirstPagePaperSource = '\binfsxn';
    SectionOtherPagePaperSource = '\binsxn';
    SectionOnlyAllowEditingOfFormFields = '\sectunlocked';
    SectionTextFlow = '\stextflow';
    SectionTitlePage = '\titlepg';

    VerticalAlignmentBottom = '\vertal\vertalb';
    VerticalAlignmentTop = '\vertalt';
    VerticalAlignmentCenter = '\vertalc';
    VerticalAlignmentJustify = '\vertalj';

    SectionBreakTypeNextPage = '\sbkpage';
    SectionBreakTypeOddPage = '\sbkodd';
    SectionBreakTypeEvenPage = '\sbkeven';
    SectionBreakTypeColumn = '\sbkcol';
    SectionBreakTypeContinuous = '\sbknone';

    SectionChapterSeparatorHyphen = '\pgnhnsh';
    SectionChapterSeparatorPeriod = '\pgnhnsp';
    SectionChapterSeparatorColon = '\pgnhnsc';
    SectionChapterSeparatorEmDash = '\pgnhnsm';
    SectionChapterSeparatorEnDash = '\pgnhnsn';

    SectionChapterHeaderStyle = '\pgnhn';
    SectionPageNumberingStart = '\pgnstarts';
    SectionPageNumberingContinuous = '\pgncont';
    SectionPageNumberingRestart = '\pgnrestart';

    SectionPageNumberingDecimal = '\pgndec';
    SectionPageNumberingUpperRoman = '\pgnucrm';
    SectionPageNumberingLowerRoman = '\pgnlcrm';
    SectionPageNumberingUpperLetter = '\pgnucltr';
    SectionPageNumberingLowerLetter = '\pgnlcltr';
    SectionPageNumberingArabicAbjad = '\pgnbidia';
    SectionPageNumberingArabicAlpha = '\pgnbidib';
    SectionPageNumberingChosung = '\pgnchosung';
    SectionPageNumberingDecimalEnclosedCircle = '\pgncnum';
    SectionPageNumberingDecimalFullWidth = '\pgndecd';
    SectionPageNumberingGanada = '\pgnganada';
    SectionPageNumberingHindiVowels = '\pgnhindia';
    SectionPageNumberingHindiConsonants = '\pgnhindib';
    SectionPageNumberingHindiNumbers = '\pgnhindic';
    SectionPageNumberingHindiDescriptive = '\pgnhindid';
    SectionPageNumberingThaiLetters = '\pgnthaia';
    SectionPageNumberingThaiNumbers = '\pgnthaib';
    SectionPageNumberingThaiDescriptive = '\pgnthaic';
    SectionPageNumberingVietnameseDescriptive = '\pgnvieta';

    SectionLineNumberingContinuous = '\linecont';
    SectionLineNumberingStartingLineNumber = '\linestarts';
    SectionLineNumberingRestartNewPage = '\lineppage';
    SectionLineNumberingRestartNewSection = '\linerestart';
    SectionLineNumberingStep = '\linemod';
    SectionLineNumberingDistance = '\linex';

    SectionColumnsCount = '\cols';
    SectionSpaceBetweenColumns = '\colsx';
    SectionColumnsDrawVerticalSeparator = '\linebetcol';

    SectionColumnNumber = '\colno';
    SectionColumnWidth = '\colw';
    SectionColumnSpace = '\colsr';

    SectionFootNotePlacementBelowText = '\sftntj';
    SectionFootNotePlacementPageBottom = '\sftnbj';
    SectionFootNoteNumberingStart = '\sftnstart';
    SectionFootNoteNumberingRestartEachPage = '\sftnrstpg';
    SectionFootNoteNumberingRestartEachSection = '\sftnrestart';
    SectionFootNoteNumberingRestartContinuous = '\sftnrstcont';

    SectionFootNoteNumberingFormatDecimal = '\sftnnar';
    SectionFootNoteNumberingFormatUpperRoman = '\sftnnruc';
    SectionFootNoteNumberingFormatLowerRoman = '\sftnnrlc';
    SectionFootNoteNumberingFormatUpperLetter = '\sftnnauc';
    SectionFootNoteNumberingFormatLowerLetter = '\sftnnalc';
    SectionFootNoteNumberingFormatChicago = '\sftnnchi';
    SectionFootNoteNumberingFormatChosung = '\sftnnchosung';
    SectionFootNoteNumberingFormatDecimalEnclosedCircle = '\sftnncnum';
    SectionFootNoteNumberingFormatDecimalFullWidth = '\sftnndbar';
    SectionFootNoteNumberingFormatGanada = '\sftnnganada';

    SectionEndNoteNumberingStart = '\saftnstart';
    SectionEndNoteNumberingRestartEachSection = '\saftnrestart';
    SectionEndNoteNumberingRestartContinuous = '\saftnrstcont';

    SectionEndNoteNumberingFormatDecimal = '\saftnnar';
    SectionEndNoteNumberingFormatUpperRoman = '\saftnnruc';
    SectionEndNoteNumberingFormatLowerRoman = '\saftnnrlc';
    SectionEndNoteNumberingFormatUpperLetter = '\saftnnauc';
    SectionEndNoteNumberingFormatLowerLetter = '\saftnnalc';
    SectionEndNoteNumberingFormatChicago = '\saftnnchi';
    SectionEndNoteNumberingFormatChosung = '\saftnnchosung';
    SectionEndNoteNumberingFormatDecimalEnclosedCircle = '\saftnncnum';
    SectionEndNoteNumberingFormatDecimalFullWidth = '\saftnndbar';
    SectionEndNoteNumberingFormatGanada = '\saftnnganada';

    LegacyPaperWidth = '\paperw';
    LegacyPaperHeight = '\paperh';
    LegacyLandscape = '\landscape';
    LegacyPageNumberingStart = '\pgnstart';
    LegacyMarginsLeft = '\margl';
    LegacyMarginsRight = '\margr';
    LegacyMarginsTop = '\margt';
    LegacyMarginsBottom = '\margb';
    LegacyMarginsGutter = '\gutter';
    LegacyMarginsGutterAtRight = '\rtlgutter';

    FootNotePlacementBelowText = '\ftntj';
    FootNotePlacementPageBottom = '\ftnbj';
    FootNoteNumberingStart = '\ftnstart';
    FootNoteNumberingRestartEachPage = '\ftnrstpg';
    FootNoteNumberingRestartEachSection = '\ftnrestart';
    FootNoteNumberingRestartContinuous = '\ftnrstcont';
    FootNoteNumberingFormatDecimal = '\ftnnar';
    FootNoteNumberingFormatUpperRoman = '\ftnnruc';
    FootNoteNumberingFormatLowerRoman = '\ftnnrlc';
    FootNoteNumberingFormatUpperLetter = '\ftnnauc';
    FootNoteNumberingFormatLowerLetter = '\ftnnalc';
    FootNoteNumberingFormatChicago = '\ftnnchi';
    FootNoteNumberingFormatChosung = '\ftnnchosung';
    FootNoteNumberingFormatDecimalEnclosedCircle = '\ftnncnum';
    FootNoteNumberingFormatDecimalFullWidth = '\ftnndbar';
    FootNoteNumberingFormatGanada = '\ftnnganada';

    EndNotePlacementEndOfSection = '\aendnotes';
    EndNotePlacementEndOfDocument = '\aenddoc';
    EndNoteNumberingStart = '\aftnstart';
    EndNoteNumberingRestartEachSection = '\aftnrestart';
    EndNoteNumberingRestartContinuous = '\aftnrstcont';
    EndNoteNumberingFormatDecimal = '\aftnnar';
    EndNoteNumberingFormatUpperRoman = '\aftnnruc';
    EndNoteNumberingFormatLowerRoman = '\aftnnrlc';
    EndNoteNumberingFormatUpperLetter = '\aftnnauc';
    EndNoteNumberingFormatLowerLetter = '\aftnnalc';
    EndNoteNumberingFormatChicago = '\aftnnchi';
    EndNoteNumberingFormatChosung = '\aftnnchosung';
    EndNoteNumberingFormatDecimalEnclosedCircle = '\aftnncnum';
    EndNoteNumberingFormatDecimalFullWidth = '\aftnndbar';
    EndNoteNumberingFormatGanada = '\aftnnganada';

    Field = '\field';
    FieldInstructions = '\*\fldinst';
    FieldResult = '\fldrslt';

    FieldMapData = '\*\mmodsofldmpdata';
    FieldTypeNull = '\mmfttypenull';
    FieldTypeColumn = '\mmfttypedbcolumn';
    FieldTypeAddress = '\mmfttypeaddress';
    FieldTypeSalutation = '\mmfttypesalutation';
    FieldTypeMapped = '\mmfttypemapped';
    FieldTypeBarcode = '\mmfttypebarcode';
    MailMergeDataSourceObjectName = '\mmodsoname';
    MailMergeDataSourceObjectMappedName = '\mmodsomappedname';
    MailMergeDataSourceObjectColumnIndex = '\mmodsofmcolumn';
    MailMergeDataSourceObjectDynamicAddress = '\mmodsodynaddr';
    MailMergeDataSourceObjectLanguageId = '\mmodsolid';

    BookmarkStart = '\*\bkmkstart';
    BookmarkEnd = '\*\bkmkend';
    RangePermissionStart = '\*\protstart';
    RangePermissionEnd = '\*\protend';
    CommentStart = '\*\atrfstart';
    CommentEnd = '\*\atrfend';
    CommentId = '\*\atnid';
    CommentAuthor = '\*\atnauthor';
    CommentTime = '\*\atntime';
    CommentChatn = '\chatn';
    CommentAnnotation = '\*\annotation';
    CommentDate = '\*\atndate';
    CommentRef = '\*\atnref';
    CommentParent = '\*\atnparent';

    HyperlinkFieldType = 'HYPERLINK';

    DocumentVariable = '\*\docvar';
    FootNote = '\footnote';
    FootNoteReference = '\chftn';
    EndNote = '\ftnalt';

    PageBackground = '\*\background';
    Shape = '\shp';
    ShapeInstance = '\*\shpinst';
    ShapeText = '\shptxt';
    ShapeLeft = '\shpleft';
    ShapeRight = '\shpright';
    ShapeTop = '\shptop';
    ShapeBottom = '\shpbottom';
    ShapeZOrder = '\shpz';
    ShapeLegacyHorizontalPositionTypePage = '\shpbxpage';
    ShapeLegacyHorizontalPositionTypeMargin = '\shpbxmargin';
    ShapeLegacyHorizontalPositionTypeColumn = '\shpbxcolumn';
    ShapeIgnoreLegacyHorizontalPositionType = '\shpbxignore';
    ShapeLegacyVerticalPositionTypePage = '\shpbypage';
    ShapeLegacyVerticalPositionTypeMargin = '\shpbymargin';
    ShapeLegacyVerticalPositionTypeParagraph = '\shpbypara';
    ShapeIgnoreLegacyVerticalPositionType = '\shpbyignore';
    ShapeWrapTextType = '\shpwr';
    ShapeWrapTextTypeZOrder = '\shpfblwtxt';
    ShapeWrapTextSide = '\shpwrk';
    ShapeLocked = '\shplockanchor';
    ShapeProperty = '\sp';
    ShapePropertyName = '\sn';
    ShapePropertyValue = '\sv';
    ShapeResult = '\shprslt';
    ShapeDoNotLay = '\splytwnine';
    HtmlAutoSpacing = '\htmautsp';

    CustomRunData = '\*\dxcustomrundata';

    ParagraphGroupPropertiesTable = '\*\pgptbl';
    ParagraphGroupProperties = '\pgp';
    ParagraphGroupPropertiesId = '\ipgp';

  {$region Tables}
    ResetTableProperties = '\trowd';
    InTableParagraph = '\intbl';
    TableEndCell = '\cell';
    NestedTableEndCell = '\nestcell';
    TableEndRow = '\row';
    NestedTableEndRow = '\nestrow';
    NestedTableProperties = '\*\nesttableprops';
    NoNestedTable = '\nonesttables';
    ParagraphNestingLevel = '\itap';
    TableCellRight = '\cellx';
    TableCellPreferredWidth = '\clwWidth';
    TableCellPreferredWidthType = '\clftsWidth';
    TableCellBottomMargin = '\clpadb';
    TableCellLeftMargin = '\clpadl';
    TableCellRightMargin = '\clpadr';
    TableCellTopMargin = '\clpadt';
    TableCellBottomMarginType = '\clpadfb';
    TableCellLeftMarginType = '\clpadfl';
    TableCellRightMarginType = '\clpadfr';
    TableCellTopMarginType = '\clpadft';
    TableRowIndex = '\irow';
    TableRowBandIndex = '\irowband';
    TableRowLeftAlignment = '\trql';
    TableRowRightAlignment = '\trqr';
    TableRowCenterAlignment = '\trqc';
    TableIndent = '\tblind';
    TableIndentType = '\tblindtype';

    TableCellBottomBorder = '\clbrdrb';
    TableCellTopBorder = '\clbrdrt';
    TableCellLeftBorder = '\clbrdrl';
    TableCellRightBorder = '\clbrdrr';
    TableCellUpperLeftToLowerRightBorder = '\cldglu';
    TableCellUpperRightToLowerLeftBorder = '\cldgll';
    TableCellStartHorizontalMerging = '\clmgf';
    TableCellContinueHorizontalMerging = '\clmrg';
    TableCellStartVerticalMerging = '\clvmgf';
    TableCellContinueVerticalMerging = '\clvmrg';
    TableCellTextTopAlignment = '\clvertalt';
    TableCellTextCenterAlignment = '\clvertalc';
    TableCellTextBottomAlignment = '\clvertalb';
    TableCellLeftToRightTopToBottomTextDirection = '\cltxlrtb';
    TableCellTopToBottomRightToLeftTextDirection = '\cltxtbrl';
    TableCellBottomToTopLeftToRightTextDirection = '\cltxbtlr';
    TableCellLeftToRightTopToBottomVerticalTextDirection = '\cltxlrtbv';
    TableCellTopToBottomRightToLeftVerticalTextDirection = '\cltxtbrlv';
    TableCellFitText = '\clFitText';
    TableCellNoWrap = '\clNoWrap';
    TableCellHideMark = '\clhidemark';
    TableCellBackgroundColor = '\clcbpat';
    TableTopBorder = '\trbrdrt';
    TableLeftBorder = '\trbrdrl';
    TableBottomBorder = '\trbrdrb';
    TableRightBorder = '\trbrdrr';
    TableHorizontalBorder = '\trbrdrh';
    TableVerticalBorder = '\trbrdrv';
    TableRowHorizontalAnchorColumn = '\tphcol';
    TableRowHorizontalAnchorMargin = '\tphmrg';
    TableRowHorizontalAnchorPage = '\tphpg';
    TableRowVerticalAnchorMargin = '\tpvmrg';
    TableRowVerticalAnchorParagraph = '\tpvpara';
    TableRowVerticalAnchorPage = '\tpvpg';
    TableRowHorizontalAlignCenter = '\tposxc';
    TableRowHorizontalAlignInside = '\tposxi';
    TableRowHorizontalAlignLeft = '\tposxl';
    TableRowHorizontalAlignOutside = '\tposxo';
    TableRowHorizontalAlignRight = '\tposxr';
    TableRowHorizontalPosition = '\tposx';
    TableRowHorizontalPositionNeg = '\tposnegx';
    TableRowVerticalAlignBottom = '\tposyb';
    TableRowVerticalAlignCenter = '\tposyc';
    TableRowVerticalAlignInline = '\tposyil';
    TableRowVerticalAlignInside = '\tposyin';
    TableRowVerticalAlignOutside = '\tposyout';
    TableRowVerticalAlignTop = '\tposyt';
    TableRowVerticalPosition = '\tposy';
    TableRowVerticalPositionNeg = '\tposnegy';
    TableRowLeftFromText = '\tdfrmtxtLeft';
    TableRowBottomFromText = '\tdfrmtxtBottom';
    TableRowRightFromText = '\tdfrmtxtRight';
    TableRowTopFromText = '\tdfrmtxtTop';
    TableNoOverlap = '\tabsnoovrlp';
    TableHalfSpaceBetweenCells = '\trgaph';
    TableRowLeft = '\trleft';
    TableRowHeight = '\trrh';
    TableRowHeader = '\trhdr';
    TableRowCantSplit = '\trkeep';
    TablePreferredWidth = '\trwWidth';
    TablePreferredWidthType = '\trftsWidth';
    TableRowWidthBefore = '\trwWidthB';
    TableRowWidthBeforeType = '\trftsWidthB';
    TableRowWidthAfter = '\trwWidthA';
    TableRowWidthAfterType = '\trftsWidthA';
    TableLayout = '\trautofit';
    TableCellSpacingBottom = '\trspdb';
    TableCellSpacingLeft = '\trspdl';
    TableCellSpacingRight = '\trspdr';
    TableCellSpacingTop = '\trspdt';
    TableCellSpacingBottomType = '\trspdfb';
    TableCellSpacingLeftType = '\trspdfl';
    TableCellSpacingRightType = '\trspdfr';
    TableCellSpacingTopType = '\trspdft';
    TableCellMarginsBottom = '\trpaddb';
    TableCellMarginsLeft = '\trpaddl';
    TableCellMarginsRight = '\trpaddr';
    TableCellMarginsTop = '\trpaddt';
    TableCellMarginsBottomType = '\trpaddfb';
    TableCellMarginsLeftType = '\trpaddfl';
    TableCellMarginsRightType = '\trpaddfr';
    TableCellMarginsTopType = '\trpaddft';
    TableApplyFirstRow = '\tbllkhdrrows';
    TableApplyLastRow = '\tbllklastrow';
    TableApplyFirstColumn = '\tbllkhdrcols';
    TableApplyLastColumn = '\tbllklastcol';
    TableDoNotApplyRowBanding = '\tbllknorowband';
    TableDoNotApplyColumnBanding = '\tbllknocolband';
    TableLastRow = '\lastrow';
  {$endregion}

  {$region Table Style}
    TableStyleResetTableProperties = '\tsrowd';
    TableStyleCellVerticalAlignmentTop = '\tsvertalt';
    TableStyleCellVerticalAlignmentCenter = '\tsvertalc';
    TableStyleCellVerticalAlignmentBottom = '\tsvertalb';

    TableStyleRowBandSize = '\tscbandsh';
    TableStyleColumnBandSize = '\tscbandsv';
    TableStyleCellBackgroundColor = '\tscellcbpat';

    TableStyleTopCellBorder = '\tsbrdrt';
    TableStyleLeftCellBorder = '\tsbrdrl';
    TableStyleBottomCellBorder = '\tsbrdrb';
    TableStyleRightCellBorder = '\tsbrdrr';
    TableStyleHorizontalCellBorder = '\tsbrdrh';
    TableStyleVerticalCellBorder = '\tsbrdrv';

    TableStyleCellNoWrap = '\tsnowrap';
    TableStyleTableBottomCellMargin = '\tscellpaddb';
    TableStyleTableLeftCellMargin = '\tscellpaddl';
    TableStyleTableRightCellMargin = '\tscellpaddr';
    TableStyleTableTopCellMargin = '\tscellpaddt';
    TableStyleTableBottomCellMarginUnitType = '\tscellpaddfb';
    TableStyleTableLeftCellMarginUnitType = '\tscellpaddfl';
    TableStyleTableRightCellMarginUnitType = '\tscellpaddfr';
    TableStyleTableTopCellMarginUnitType = '\tscellpaddft';
    TableStyleUpperLeftToLowerRightBorder = '\tsbrdrdgl';
    TableStyleUpperRightToLowerLeftBorder = '\tsbrdrdgr';
  {$endregion}
  {$region Table Conditional Style}
    TableConditionalStyleFirstRow = '\tscfirstrow';
    TableConditionalStyleLastRow = '\tsclastrow';
    TableConditionalStyleFirstColumn = '\tscfirstcol';
    TableConditionalStyleLastColumn = '\tsclastcol';
    TableConditionalStyleOddRowBanding = '\tscbandhorzodd';
    TableConditionalStyleEvenRowBanding = '\tscbandhorzeven';
    TableConditionalStyleOddColumnBanding = '\tscbandvertodd';
    TableConditionalStyleEvenColumnBanding = '\tscbandverteven';
    TableConditionalStyleTopLeftCell = '\tscnwcell';
    TableConditionalStyleTopRightCell = '\tscnecell';
    TableConditionalStyleBottomLeftCell = '\tscswcell';
    TableConditionalStyleBottomRightCell = '\tscsecell';
  {$endregion}
  {$region Border}
    NoTableBorder = '\brdrtbl';
    NoBorder = '\brdrnil';
    BorderWidth = '\brdrw';
    BorderColor = '\brdrcf';
    BorderFrame = '\brdrframe';
    BorderSpace = '\brsp';
    BorderArtIndex = '\brdrart';
    BorderSingleWidth = '\brdrs';
    BorderDoubleWidth = '\brdrth';
    BorderShadow = '\brdrsh';
    BorderDouble = '\brdrdb';
    BorderDotted = '\brdrdot';
    BorderDashed = '\brdrdash';
    BorderSingle = '\brdrhair';
    BorderDashedSmall = '\brdrdashsm';
    BorderDotDashed = '\brdrdashd';
    BorderDotDotDashed = '\brdrdashdd';
    BorderInset = '\brdrinset';
    BorderNone = '\brdrnone';
    BorderOutset = '\brdroutset';
    BorderTriple = '\brdrtriple';
    BorderThickThinSmall = '\brdrtnthsg';
    BorderThinThickSmall = '\brdrthtnsg';
    BorderThinThickThinSmall = '\brdrtnthtnsg';
    BorderThickThinMedium = '\brdrtnthmg';
    BorderThinThickMedium = '\brdrthtnmg';
    BorderThinThickThinMedium = '\brdrtnthtnmg';
    BorderThickThinLarge = '\brdrtnthlg';
    BorderThinThickLarge = '\brdrthtnlg';
    BorderThinThickThinLarge = '\brdrtnthtnlg';
    BorderWavy = '\brdrwavy';
    BorderDoubleWavy = '\brdrwavydb';
    BorderDashDotStroked = '\brdrdashdotstr';
    BorderThreeDEmboss = '\brdremboss';
    BorderThreeDEngrave = '\brdrengrave';
  {$endregion}
  {$region NumberingList}
    NumberingListTable = '\*\listtable';
    ListOverrideTable = '\*\listoverridetable';

    NumberingList = '\list';
    NumberingListId = '\listid';
    NumberingListTemplateId = '\listtemplateid';
    NumberingListStyleId = '\liststyleid';
    NumberingListStyleName = '\*\liststylename ';

    NumberingListName = '\listname ;';
    NumberingListHybrid = '\listhybrid';
    ListLevel = '\listlevel';

    ListLevelStart = '\levelstartat';
    ListLevelTentative = '\lvltentative';
    ListLevelNumberingFormat = '\levelnfc';
    ListLevelAlignment = '\leveljc';
    ListLevelNumberingFormatN = '\levelnfcn';
    ListLevelAlignmentN = '\leveljcn';

    LisLeveltOld = '\levelold';
    ListLevelPrev = '\levelprev';
    ListLevelPrevSpase = '\levelprevspace';
    ListLevelSpace = '\levelspace';
    ListLevelIntdent = '\levelindent';
    ListLevelNumbers = '\levelnumbers';
    ListLevelText = '\leveltext';
    LevelTemplateId = '\leveltemplateid';
    ListLevelFollow = '\levelfollow';
    ListLevelLegal = '\levellegal';
    ListLevelNoRestart = '\levelnorestart';
    ListLevelPicture = '\levelpicture';
    ListLevelPictureNoSize = '\levelpicturenosize';
    ListLevelLegacy = '\levelold';
    ListLevelLegacySpace = '\levelspace';
    ListLevelLegacyIndent = '\levelindent';

    ListOverride = '\listoverride';
    ListOverrideListId = '\listid';
    ListOverrideCount = '\listoverridecount';
    ListOverrideLevel = '\lfolevel';

    ListOverrideFormat = '\listoverrideformat';
    ListOverrideStart = '\listoverridestartat';
    ListOverrideStartValue = '\levelstartat';
    ListOverrideListLevel = '\listlevel';
  {$endregion}
  {$region DefaultProperties}
    DefaultCharacterProperties = '\*\defchp';
    DefaultParagraphProperties = '\*\defpap';
  {$endregion}
  {$region Style}
    StyleSheet = '\stylesheet';
    ParagraphStyle = '\s';
    CharacterStyle = '\*\cs';
    CharacterStyleIndex = '\cs';
    TableStyle = '\*\ts';
    TableStyleIndex = '\ts';
    TableStyleCellIndex = '\yts';
    ParentStyle = '\sbasedon';
    LinkedStyle = '\slink';
    NextStyle = '\snext';
    QuickFormatStyle = '\sqformat';
  {$endregion}
  strict private class var
    FFloatingObjectTextWrapTypeTable: TDictionary<TdxFloatingObjectTextWrapType, Integer>;
    FFloatingObjectTextWrapSideTable: TDictionary<TdxFloatingObjectTextWrapSide, Integer>;
    FFloatingObjectHorizontalPositionTypeTable: TDictionary<TdxFloatingObjectHorizontalPositionType, Integer>;
    FFloatingObjectHorizontalPositionAlignmentTable: TDictionary<TdxFloatingObjectHorizontalPositionAlignment, Integer>;
    FFloatingObjectVerticalPositionTypeTable: TDictionary<TdxFloatingObjectVerticalPositionType, Integer>;
    FFloatingObjectVerticalPositionAlignmentTable: TDictionary<TdxFloatingObjectVerticalPositionAlignment, Integer>;
    FFloatingObjectRelativeFromHorizontalTable: TDictionary<TdxFloatingObjectRelativeFromHorizontal, Integer>;
    FFloatingObjectRelativeFromVerticalTable: TDictionary<TdxFloatingObjectRelativeFromVertical, Integer>;
    class constructor Initialize;
    class destructor Finalize;

    class function CreateFloatingObjectRelativeFromHorizontalTable: TDictionary<TdxFloatingObjectRelativeFromHorizontal, Integer>; static;
    class function CreateFloatingObjectRelativeFromVerticalTable: TDictionary<TdxFloatingObjectRelativeFromVertical, Integer>; static;
    class function CreateFloatingObjectVerticalPositionAlignmentTable: TDictionary<TdxFloatingObjectVerticalPositionAlignment, Integer>; static;
    class function CreateFloatingObjectVerticalPositionTypeTable: TDictionary<TdxFloatingObjectVerticalPositionType, Integer>; static;
    class function CreateFloatingObjectHorizontalPositionAlignmentTable: TDictionary<TdxFloatingObjectHorizontalPositionAlignment, Integer>; static;
    class function CreateFloatingObjectHorizontalPositionTypeTable: TDictionary<TdxFloatingObjectHorizontalPositionType, Integer>; static;
    class function CreateFloatingObjectTextWrapSideTable: TDictionary<TdxFloatingObjectTextWrapSide, Integer>; static;
    class function CreateFloatingObjectTextWrapTypeTable: TDictionary<TdxFloatingObjectTextWrapType, Integer>; static;
  public
    class property FloatingObjectTextWrapTypeTable: TDictionary<TdxFloatingObjectTextWrapType, Integer> read FFloatingObjectTextWrapTypeTable;
    class property FloatingObjectTextWrapSideTable: TDictionary<TdxFloatingObjectTextWrapSide, Integer> read FFloatingObjectTextWrapSideTable;
    class property FloatingObjectHorizontalPositionTypeTable: TDictionary<TdxFloatingObjectHorizontalPositionType, Integer> read FFloatingObjectHorizontalPositionTypeTable;
    class property FloatingObjectHorizontalPositionAlignmentTable: TDictionary<TdxFloatingObjectHorizontalPositionAlignment, Integer> read FFloatingObjectHorizontalPositionAlignmentTable;
    class property FloatingObjectVerticalPositionTypeTable: TDictionary<TdxFloatingObjectVerticalPositionType, Integer> read FFloatingObjectVerticalPositionTypeTable;
    class property FloatingObjectVerticalPositionAlignmentTable: TDictionary<TdxFloatingObjectVerticalPositionAlignment, Integer> read FFloatingObjectVerticalPositionAlignmentTable;
    class property FloatingObjectRelativeFromHorizontalTable: TDictionary<TdxFloatingObjectRelativeFromHorizontal, Integer> read FFloatingObjectRelativeFromHorizontalTable;
    class property FloatingObjectRelativeFromVerticalTable: TDictionary<TdxFloatingObjectRelativeFromVertical, Integer> read FFloatingObjectRelativeFromVerticalTable;
  end;

implementation

{ TdxRtfExportSR }

class constructor TdxRtfExportSR.Initialize;
begin
  FFloatingObjectTextWrapTypeTable := CreateFloatingObjectTextWrapTypeTable;
  FFloatingObjectTextWrapSideTable := CreateFloatingObjectTextWrapSideTable;
  FFloatingObjectHorizontalPositionTypeTable := CreateFloatingObjectHorizontalPositionTypeTable;
  FFloatingObjectHorizontalPositionAlignmentTable := CreateFloatingObjectHorizontalPositionAlignmentTable;
  FFloatingObjectVerticalPositionTypeTable := CreateFloatingObjectVerticalPositionTypeTable;
  FFloatingObjectVerticalPositionAlignmentTable := CreateFloatingObjectVerticalPositionAlignmentTable;
  FFloatingObjectRelativeFromHorizontalTable := CreateFloatingObjectRelativeFromHorizontalTable;
  FFloatingObjectRelativeFromVerticalTable := CreateFloatingObjectRelativeFromVerticalTable;
end;

class destructor TdxRtfExportSR.Finalize;
begin
  FFloatingObjectTextWrapTypeTable.Free;
  FFloatingObjectTextWrapSideTable.Free;
  FFloatingObjectHorizontalPositionTypeTable.Free;
  FFloatingObjectHorizontalPositionAlignmentTable.Free;
  FFloatingObjectVerticalPositionTypeTable.Free;
  FFloatingObjectVerticalPositionAlignmentTable.Free;
  FFloatingObjectRelativeFromHorizontalTable.Free;
  FFloatingObjectRelativeFromVerticalTable.Free;
end;

class function TdxRtfExportSR.CreateFloatingObjectRelativeFromHorizontalTable: TDictionary<TdxFloatingObjectRelativeFromHorizontal, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectRelativeFromHorizontal, Integer>.Create;
  Result.Add(TdxFloatingObjectRelativeFromHorizontal.Margin, 0);
  Result.Add(TdxFloatingObjectRelativeFromHorizontal.Page, 1);
  Result.Add(TdxFloatingObjectRelativeFromHorizontal.LeftMargin, 2);
  Result.Add(TdxFloatingObjectRelativeFromHorizontal.RightMargin, 3);
  Result.Add(TdxFloatingObjectRelativeFromHorizontal.InsideMargin, 4);
  Result.Add(TdxFloatingObjectRelativeFromHorizontal.OutsideMargin, 5);
end;

class function TdxRtfExportSR.CreateFloatingObjectRelativeFromVerticalTable: TDictionary<TdxFloatingObjectRelativeFromVertical, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectRelativeFromVertical, Integer>.Create;
  Result.Add(TdxFloatingObjectRelativeFromVertical.Margin, 0);
  Result.Add(TdxFloatingObjectRelativeFromVertical.Page, 1);
  Result.Add(TdxFloatingObjectRelativeFromVertical.TopMargin, 2);
  Result.Add(TdxFloatingObjectRelativeFromVertical.BottomMargin, 3);
  Result.Add(TdxFloatingObjectRelativeFromVertical.InsideMargin, 4);
  Result.Add(TdxFloatingObjectRelativeFromVertical.OutsideMargin, 5);
end;

class function TdxRtfExportSR.CreateFloatingObjectVerticalPositionAlignmentTable: TDictionary<TdxFloatingObjectVerticalPositionAlignment, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectVerticalPositionAlignment, Integer>.Create;
  Result.Add(TdxFloatingObjectVerticalPositionAlignment.None, 0);
  Result.Add(TdxFloatingObjectVerticalPositionAlignment.Top, 1);
  Result.Add(TdxFloatingObjectVerticalPositionAlignment.Center, 2);
  Result.Add(TdxFloatingObjectVerticalPositionAlignment.Bottom, 3);
  Result.Add(TdxFloatingObjectVerticalPositionAlignment.Inside, 4);
  Result.Add(TdxFloatingObjectVerticalPositionAlignment.Outside, 5);
end;

class function TdxRtfExportSR.CreateFloatingObjectVerticalPositionTypeTable: TDictionary<TdxFloatingObjectVerticalPositionType, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectVerticalPositionType, Integer>.Create;
  Result.Add(TdxFloatingObjectVerticalPositionType.Margin, 0);
  Result.Add(TdxFloatingObjectVerticalPositionType.Page, 1);
  Result.Add(TdxFloatingObjectVerticalPositionType.Paragraph, 2);
  Result.Add(TdxFloatingObjectVerticalPositionType.Line, 3);
  Result.Add(TdxFloatingObjectVerticalPositionType.TopMargin, 4);
  Result.Add(TdxFloatingObjectVerticalPositionType.BottomMargin, 5);
  Result.Add(TdxFloatingObjectVerticalPositionType.InsideMargin, 6);
  Result.Add(TdxFloatingObjectVerticalPositionType.OutsideMargin, 7);
end;

class function TdxRtfExportSR.CreateFloatingObjectHorizontalPositionAlignmentTable: TDictionary<TdxFloatingObjectHorizontalPositionAlignment, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectHorizontalPositionAlignment, Integer>.Create;
  Result.Add(TdxFloatingObjectHorizontalPositionAlignment.None, 0);
  Result.Add(TdxFloatingObjectHorizontalPositionAlignment.Left, 1);
  Result.Add(TdxFloatingObjectHorizontalPositionAlignment.Center, 2);
  Result.Add(TdxFloatingObjectHorizontalPositionAlignment.Right, 3);
  Result.Add(TdxFloatingObjectHorizontalPositionAlignment.Inside, 4);
  Result.Add(TdxFloatingObjectHorizontalPositionAlignment.Outside, 5);
end;

class function TdxRtfExportSR.CreateFloatingObjectHorizontalPositionTypeTable: TDictionary<TdxFloatingObjectHorizontalPositionType, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectHorizontalPositionType, Integer>.Create;
  Result.Add(TdxFloatingObjectHorizontalPositionType.Margin, 0);
  Result.Add(TdxFloatingObjectHorizontalPositionType.Page, 1);
  Result.Add(TdxFloatingObjectHorizontalPositionType.Column, 2);
  Result.Add(TdxFloatingObjectHorizontalPositionType.Character, 3);
  Result.Add(TdxFloatingObjectHorizontalPositionType.LeftMargin, 4);
  Result.Add(TdxFloatingObjectHorizontalPositionType.RightMargin, 5);
  Result.Add(TdxFloatingObjectHorizontalPositionType.InsideMargin, 6);
  Result.Add(TdxFloatingObjectHorizontalPositionType.OutsideMargin, 7);
end;

class function TdxRtfExportSR.CreateFloatingObjectTextWrapSideTable: TDictionary<TdxFloatingObjectTextWrapSide, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectTextWrapSide, Integer>.Create;
  Result.Add(TdxFloatingObjectTextWrapSide.Both, 0);
  Result.Add(TdxFloatingObjectTextWrapSide.Left, 1);
  Result.Add(TdxFloatingObjectTextWrapSide.Right, 2);
  Result.Add(TdxFloatingObjectTextWrapSide.Largest, 3);
end;

class function TdxRtfExportSR.CreateFloatingObjectTextWrapTypeTable: TDictionary<TdxFloatingObjectTextWrapType, Integer>;
begin
  Result := TDictionary<TdxFloatingObjectTextWrapType, Integer>.Create;
  Result.Add(TdxFloatingObjectTextWrapType.TopAndBottom, 1);
  Result.Add(TdxFloatingObjectTextWrapType.Square, 2);
  Result.Add(TdxFloatingObjectTextWrapType.None, 3);
  Result.Add(TdxFloatingObjectTextWrapType.Tight, 4);
  Result.Add(TdxFloatingObjectTextWrapType.Through, 5);
end;

end.

