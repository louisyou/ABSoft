{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Platform.Win.Font;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
   Types, SysUtils, Windows, Graphics, Classes, dxCoreGraphics,
   dxRichEdit.DocumentLayout.UnitConverter, dxRichEdit.Platform.Font, dxRichEdit.Utils.UnicodeRangeInfo,
   cxGraphics;

type
  { TdxGdiUnicodeSubrangeBits }

  TdxUnicodeSubrangeBits = array[0..3] of DWORD;

  TdxGdiUnicodeSubrangeBits = record
    Data: TdxUnicodeSubrangeBits;
    function GetBit(AIndex: Integer): Boolean; inline;
    procedure SetBit(AIndex: Integer; const Value: Boolean); inline;
    procedure Clear;

    property Bits[Index: Integer]: Boolean read GetBit write SetBit;
  end;

  { TdxCharacterDrawingAbilityTable }

  TdxCharacterDrawingAbilityTable = class
  private
    FDrawingAbility: TBits;
    FCalculated: TBits;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetDrawingAbility(ACharacter: WideChar; AValue: Boolean);
    function GetDrawingAbility(ACharacter: WideChar; out AAbility: Boolean): Boolean;
  end;

  { GdiFontInfo }

  TdxGdiFontInfo = class(TdxFontInfo)
  private
    FFontName: string;
    FFontStyle: TFontStyles;
    FGdiFontHandle: THandle;
    FCharacterDrawingAbilityTable: TdxCharacterDrawingAbilityTable;
    FSizeInPoints: Integer; 
    FPanose: TPanose;
    FTrueType: Boolean;
    FUnicodeSubrangeBits: TdxUnicodeSubrangeBits;
    FUnicodeSubrangeBitsCalculated: Boolean;
    function CalculateFontCharsetCore(AMeasurer: TdxFontInfoMeasurer): Integer;
    procedure CalculateUnderlineAndStrikeoutParameters(const AOutlineTextmetric: TOutlineTextmetricW);
  protected
    procedure Initialize(AMeasurer: TdxFontInfoMeasurer); override;
    procedure CreateFont(AMeasurer: TdxFontInfoMeasurer; const AName: string; ADoubleSize: Integer; const AFontStyle: TFontStyles); override;
    function CalculateFontSizeInPoints: Single; override;
    function CalculateCanDrawCharacter(AUnicodeRangeInfo: TdxUnicodeRangeInfo; ACanvas: TCanvas; ACharacter: Char): Boolean; virtual;
    function CalculateSupportedUnicodeSubrangeBits(AUnicodeRangeInfo: TdxUnicodeRangeInfo; ACanvas: TCanvas): TdxGdiUnicodeSubrangeBits; virtual;

    function GetBold: Boolean; override;
    function GetItalic: Boolean; override;
    function GetName: string; override;
    function GetSize: Single; override;
    function GetUnderline: Boolean; override;
    procedure SetTextMetrics(const ATextMetric: TTextMetricW);
  public
    destructor Destroy; override;
    procedure CalculateSuperscriptOffset(ABaseFontInfo: TdxFontInfo); override;
    procedure CalculateSubscriptOffset(ABaseFontInfo: TdxFontInfo); override;

    function CalculateFontSizeInLayoutUnits(AFontUnit: TdxGraphicUnit; AUnitConverter: TdxDocumentLayoutUnitConverter): Single;
    procedure CalculateFontVerticalParameters(AMeasurer: TdxFontInfoMeasurer); override;
    function CalculateFontCharset(AMeasurer: TdxFontInfoMeasurer): Integer; override;
    function CanDrawCharacter(AUnicodeRangeInfo: TdxUnicodeRangeInfo; ACanvas: TCanvas; ACharacter: Char): Boolean; virtual;
    class function GetFontUnicodeRanges(ADC: HDC; AFont: HFONT): TdxFontCharacterRangeArray; static;

    property GdiFontHandle: THandle read FGdiFontHandle;
    property Panose: TPanose read FPanose;
  end;

  { TdxGdiFontInfoMeasurer }

  TdxGdiFontInfoMeasurer = class(TdxFontInfoMeasurer)
  private class var
    FDefaultCharSet: Byte;
    FSystemFontQuality: Byte;
    class constructor Initialize;
  private
    FDpi: Single;
    FMeasureGraphics: TCanvas;
    class function CalculateActualFontQuality: Byte; static;
  protected
    function CreateMeasureGraphics: TCanvas;
    procedure Initialize; override;

    class property DefaultCharSet: Byte read FDefaultCharSet;
    class property SystemFontQuality: Byte read FSystemFontQuality;
  public
    constructor Create(AUnitConverter: TdxDocumentLayoutUnitConverter); override;
    destructor Destroy; override;
    class function CreateFont(const AName: string; ASize: Single; const AFontStyle: TFontStyles;
      AUnitConverter: TdxDocumentLayoutUnitConverter): THandle; static;
    function MeasureCharacterWidthF(ACharacter: WideChar; AFontInfo: TdxFontInfo): Single; override;
    function MeasureString(const AText: string; AFontInfo: TdxFontInfo): Size; override;
    function MeasureMaxDigitWidthF(AFontInfo: TdxFontInfo): Single; override;

    property MeasureGraphics: TCanvas read FMeasureGraphics;
  end;

implementation

uses
  Math, cxGeometry, dxCore;

const
  MeasureDigits: array[0..9] of WideChar = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

{ TdxGdiUnicodeSubrangeBits }

procedure TdxGdiUnicodeSubrangeBits.Clear;
begin
  Data[0] := 0;
  Data[1] := 0;
  Data[2] := 0;
  Data[3] := 0;
end;

const
  BitPerDWORD = 8 * SizeOf(DWORD);

function TdxGdiUnicodeSubrangeBits.GetBit(AIndex: Integer): Boolean;
begin
  Result := Data[AIndex div BitPerDWORD] and (1 shl (AIndex mod BitPerDWORD)) <> 0;
end;

procedure TdxGdiUnicodeSubrangeBits.SetBit(AIndex: Integer;
  const Value: Boolean);
begin
  if Value then
    Data[AIndex div BitPerDWORD] := Data[AIndex div BitPerDWORD] or (1 shl (AIndex mod BitPerDWORD))
  else
    Data[AIndex div BitPerDWORD] := Data[AIndex div BitPerDWORD] and not DWORD((1 shl (AIndex mod BitPerDWORD)));
end;

{ TdxCharacterDrawingAbilityTable }

constructor TdxCharacterDrawingAbilityTable.Create;
begin
  FDrawingAbility := TBits.Create;
  FDrawingAbility.Size := $0600; 
  FCalculated := TBits.Create;
  FCalculated.Size := $0600;     
end;

destructor TdxCharacterDrawingAbilityTable.Destroy;
begin
  FCalculated.Free;
  FDrawingAbility.Free;
  inherited Destroy;
end;

function TdxCharacterDrawingAbilityTable.GetDrawingAbility(ACharacter: WideChar;
  out AAbility: Boolean): Boolean;
begin
  if Ord(ACharacter) >= FCalculated.Size then
  begin
    FCalculated.Size := Ord(ACharacter) + 1;
    FDrawingAbility.Size := Ord(ACharacter) + 1;
  end;
  Result := FCalculated[Ord(ACharacter)];
  if Result then
    AAbility := FDrawingAbility[Ord(ACharacter)];
end;

procedure TdxCharacterDrawingAbilityTable.SetDrawingAbility(ACharacter: WideChar;
  AValue: Boolean);
begin
  FCalculated[Ord(ACharacter)] := True;
  FDrawingAbility[Ord(ACharacter)] := AValue;
end;

{ TdxGdiFontInfo }

destructor TdxGdiFontInfo.Destroy;
begin
  FCharacterDrawingAbilityTable.Free;
  if GdiFontHandle <> 0 then
    DeleteObject(GdiFontHandle);
  inherited Destroy;
end;

procedure TdxGdiFontInfo.Initialize(AMeasurer: TdxFontInfoMeasurer);
begin
  FCharacterDrawingAbilityTable := TdxCharacterDrawingAbilityTable.Create;
end;

procedure TdxGdiFontInfo.CreateFont(AMeasurer: TdxFontInfoMeasurer;
  const AName: string; ADoubleSize: Integer; const AFontStyle: TFontStyles);
var
  AGdiPlusMeasurer: TdxGdiFontInfoMeasurer absolute AMeasurer;
begin
  FSizeInPoints := ADoubleSize div 2;
  FGdiFontHandle := AGdiPlusMeasurer.CreateFont(AName, FSizeInPoints, AFontStyle, AGdiPlusMeasurer.UnitConverter);
  FFontStyle := AFontStyle;
  FFontName := AName;
end;

function TdxGdiFontInfo.CalculateFontSizeInPoints: Single;
begin
  Result := FSizeInPoints;
end;

function TdxGdiFontInfo.CalculateFontSizeInLayoutUnits(AFontUnit: TdxGraphicUnit; AUnitConverter: TdxDocumentLayoutUnitConverter): Single;
begin
  case AFontUnit of
    guDocument:
      Result := AUnitConverter.DocumentsToFontUnitsF(FSizeInPoints);
    guInch:
      Result := AUnitConverter.InchesToFontUnitsF(FSizeInPoints);
    guMillimeter:
      Result := AUnitConverter.MillimetersToFontUnitsF(FSizeInPoints);
    guPoint:
      Result := AUnitConverter.PointsToFontUnitsF(FSizeInPoints);
    else
      raise Exception.Create('Unsupported units');
  end;
end;

procedure TdxGdiFontInfo.CalculateFontVerticalParameters(AMeasurer: TdxFontInfoMeasurer);
const
  CJK_CODEPAGE_BITS =
    (1 shl 17) or 
    (1 shl 18) or 
    (1 shl 19) or 
    (1 shl 20) or 
    (1 shl 21);   
var
  AGdiMeasurer: TdxGdiFontInfoMeasurer absolute AMeasurer;
  AOutlineTextMetrics: TOutlineTextmetricW;
  ATextMetric: TTextMetricW;
  AFontSignature: TFontSignature;
  ADC: THandle;
begin
  ADC := AGdiMeasurer.MeasureGraphics.Handle;
  SelectObject(ADC, GdiFontHandle);
  GetTextMetricsW(ADC, ATextMetric);
  if Integer(GetTextCharsetInfo(ADC, @AFontSignature, 0)) = DEFAULT_CHARSET then
    SetCJKFont(False)
  else
    SetCJKFont(AFontSignature.fsCsb[0] and CJK_CODEPAGE_BITS <> 0);
  FTrueType := ATextMetric.tmPitchAndFamily and TMPF_TRUETYPE <> 0;
  if FTrueType then
  begin
    AOutlineTextMetrics.otmSize := SizeOf(AOutlineTextMetrics);
    if Windows.GetOutlineTextMetricsW(ADC, SizeOf(AOutlineTextMetrics), @AOutlineTextMetrics) <> 0 then
    begin
      MacAscent := AOutlineTextMetrics.otmMacAscent;
      MacDescent := Abs(AOutlineTextMetrics.otmMacDescent);
      SetTextMetrics(AOutlineTextMetrics.otmTextMetrics);
      FPanose := AOutlineTextMetrics.otmPanoseNumber;
      CalculateUnderlineAndStrikeoutParameters(AOutlineTextMetrics);
      Exit;
    end;
  end;
  SetTextMetrics(ATextMetric);
  MacAscent := Ascent;
  MacDescent := Descent;
end;

function TdxGdiFontInfo.CalculateFontCharset(AMeasurer: TdxFontInfoMeasurer): Integer;
begin
  Result := CalculateFontCharsetCore(AMeasurer);
end;

function TdxGdiFontInfo.CalculateFontCharsetCore(AMeasurer: TdxFontInfoMeasurer): Integer;
var
  ALogFont: TLogFontW;
begin
  if (GdiFontHandle <> 0) and (GetObjectW(GdiFontHandle, SizeOf(ALogFont), @ALogFont) <> 0) then
    Result := ALogFont.lfCharSet
  else
    Result := GetDefFontCharSet;
end;

procedure TdxGdiFontInfo.CalculateUnderlineAndStrikeoutParameters(const AOutlineTextmetric: TOutlineTextmetricW);
var
  AOffset: TPoint;
begin
  UnderlinePosition := -AOutlineTextmetric.otmsUnderscorePosition;
  UnderlineThickness := AOutlineTextmetric.otmsUnderscoreSize;
  StrikeoutPosition := AOutlineTextmetric.otmsStrikeoutPosition;
  StrikeoutThickness := (Integer(AOutlineTextmetric.otmsStrikeoutSize));
  SubscriptSize := TSize(AOutlineTextmetric.otmptSubscriptSize);
  SubscriptOffset := AOutlineTextmetric.otmptSubscriptOffset;
  SuperscriptOffset := AOutlineTextmetric.otmptSuperscriptOffset;
  AOffset := SuperscriptOffset;
  AOffset.Y := -AOffset.Y;
  SuperscriptOffset := AOffset;
  SuperscriptSize := TSize(AOutlineTextmetric.otmptSuperscriptSize);
end;

procedure TdxGdiFontInfo.CalculateSuperscriptOffset(ABaseFontInfo: TdxFontInfo);
begin
end;

procedure TdxGdiFontInfo.CalculateSubscriptOffset(ABaseFontInfo: TdxFontInfo);
begin
end;

function TdxGdiFontInfo.CanDrawCharacter(AUnicodeRangeInfo: TdxUnicodeRangeInfo;
  ACanvas: TCanvas; ACharacter: Char): Boolean;
begin
  if not FCharacterDrawingAbilityTable.GetDrawingAbility(ACharacter, Result) then
  begin
    Result := CalculateCanDrawCharacter(AUnicodeRangeInfo, ACanvas, ACharacter);
    FCharacterDrawingAbilityTable.SetDrawingAbility(ACharacter, Result);
  end;
end;

function TdxGdiFontInfo.CalculateCanDrawCharacter(AUnicodeRangeInfo: TdxUnicodeRangeInfo;
  ACanvas: TCanvas; ACharacter: Char): Boolean;
var
  AUnicodeSubrangeBits: TdxGdiUnicodeSubrangeBits;
  AUnicodeSubRange: PdxUnicodeSubrange;
begin
  AUnicodeSubRange := AUnicodeRangeInfo.LookupSubrange(ACharacter);
  if AUnicodeSubRange <> nil then
  begin
    Assert(AUnicodeSubRange.Bit < 126, '');
    AUnicodeSubrangeBits := CalculateSupportedUnicodeSubrangeBits(AUnicodeRangeInfo, ACanvas);
    Result := AUnicodeSubrangeBits.Bits[AUnicodeSubRange.Bit];
  end
  else
    Result := False;
end;

class function TdxGdiFontInfo.GetFontUnicodeRanges(ADC: HDC; AFont: HFONT): TdxFontCharacterRangeArray;
var
  I, AGlyphCount, ASize: Integer;
  AGlyphSet: PGlyphSet;
  AInMainThread: Boolean;
  AOldFont: HFONT;
begin
  AInMainThread := (AFont <> 0) and (GetCurrentThreadID = MainThreadID);
  if not AInMainThread then
    ADC := CreateCompatibleDC(0);
  AOldFont := SelectObject(ADC, AFont);
  try
    ASize := Windows.GetFontUnicodeRanges(ADC, nil);
    Assert(ASize <> 0);
    if ASize = 0 then
      Exit(nil);
    GetMem(AGlyphSet, ASize);
    try
      Windows.GetFontUnicodeRanges(ADC, AGlyphSet);
      AGlyphCount := AGlyphSet.cRanges;
      SetLength(Result, AGlyphCount);
      for I := 0 to AGlyphCount - 1 do
        Result[I].CopyFrom(AGlyphSet.ranges[I]);
    finally
      FreeMem(AGlyphSet);
    end;
  finally
    if AInMainThread then
      SelectObject(ADC, AOldFont)
    else
      DeleteDC(ADC);
  end;
end;

function TdxGdiFontInfo.GetSize: Single;
begin
  Result := FSizeInPoints;
end;

function TdxGdiFontInfo.GetBold: Boolean;
begin
  Result := fsBold in FFontStyle;
end;

function TdxGdiFontInfo.GetItalic: Boolean;
begin
  Result := fsItalic in FFontStyle;
end;

function TdxGdiFontInfo.GetName: string;
begin
  Result := FFontName;
end;

function TdxGdiFontInfo.GetUnderline: Boolean;
begin
  Result := fsUnderline in FFontStyle;
end;

procedure TdxGdiFontInfo.SetTextMetrics(const ATextMetric: TTextMetricW);
var
  ACJKExtLeading, AHalfTmpExtLeading, AOtherHalfTmpExtLeading: Integer;
begin
  if IsCJKFont then
  begin
    AHalfTmpExtLeading := ATextMetric.tmExternalLeading div 2;
    AOtherHalfTmpExtLeading :=  ATextMetric.tmExternalLeading - AHalfTmpExtLeading;

    Ascent := ATextMetric.tmAscent + AHalfTmpExtLeading;
    Descent := ATextMetric.tmDescent + AOtherHalfTmpExtLeading;

    ACJKExtLeading := Trunc(0.3 * (ATextMetric.tmAscent + ATextMetric.tmDescent));
    Dec(ACJKExtLeading, ATextMetric.tmExternalLeading);
    if ACJKExtLeading > 0 then
      LineSpacing := Ascent + Descent + (ATextMetric.tmExternalLeading + ACJKExtLeading)
    else
      LineSpacing := Ascent + Descent + ATextMetric.tmExternalLeading;
  end
  else
  begin
    Ascent := ATextMetric.tmAscent;
    Descent := ATextMetric.tmDescent;
    LineSpacing := Ascent + Descent + ATextMetric.tmExternalLeading;
  end;
end;

function TdxGdiFontInfo.CalculateSupportedUnicodeSubrangeBits(
  AUnicodeRangeInfo: TdxUnicodeRangeInfo; ACanvas: TCanvas): TdxGdiUnicodeSubrangeBits;
var
  ADC, AOldFont: THandle;
  AFontSignature: TFontSignature;
  ALocaleSignature: TLocaleSignature;
begin
  if not FUnicodeSubrangeBitsCalculated then
  begin
    if not FTrueType then
    begin
      GetLocaleInfoW(LOCALE_INVARIANT, LOCALE_FONTSIGNATURE, PWideChar(@ALocaleSignature),
        SizeOf(ALocaleSignature) div SizeOf(WideChar));
      Move(ALocaleSignature.lsUsb, FUnicodeSubrangeBits, SizeOf(FUnicodeSubrangeBits));
    end;
    ADC := ACanvas.Handle;
    AOldFont := SelectObject(ADC, GdiFontHandle);
    try
      if Integer(GetTextCharsetInfo(ADC, @AFontSignature, 0)) = DEFAULT_CHARSET then
        Result.Clear
      else
        Move(AFontSignature.fsUsb, FUnicodeSubrangeBits, SizeOf(FUnicodeSubrangeBits));
    finally
      SelectObject(ADC, AOldFont);
    end;
    FUnicodeSubrangeBitsCalculated := True;
  end;
  Result.Data := FUnicodeSubrangeBits;
end;

{ GdiFontInfoMeasurer }

constructor TdxGdiFontInfoMeasurer.Create(
  AUnitConverter: TdxDocumentLayoutUnitConverter);
begin
  inherited Create(AUnitConverter);
  FDpi := AUnitConverter.Dpi; 
end;

destructor TdxGdiFontInfoMeasurer.Destroy;
begin
  DeleteDC(FMeasureGraphics.Handle);
  FMeasureGraphics.Handle := 0;
  FMeasureGraphics.Free;

  inherited Destroy;
end;

class constructor TdxGdiFontInfoMeasurer.Initialize;
begin
  FDefaultCharSet := GetDefFontCharset;
  FSystemFontQuality := CalculateActualFontQuality;
end;

procedure TdxGdiFontInfoMeasurer.Initialize;
begin
  FMeasureGraphics := CreateMeasureGraphics;
end;

class function TdxGdiFontInfoMeasurer.CalculateActualFontQuality: Byte;
const
  CLEARTYPE_QUALITY = 5;
  CLEARTYPE_NATURAL_QUALITY = 6;
begin
  if IsWin2K then
    Result := CLEARTYPE_QUALITY
  else
    if IsWinSevenOrLater then
      Result := CLEARTYPE_NATURAL_QUALITY
    else
      Result := DEFAULT_QUALITY;
end;

function TdxGdiFontInfoMeasurer.CreateMeasureGraphics: TCanvas;
begin
  Result := TCanvas.Create;
  Result.Handle := CreateCompatibleDC(0);
end;

function TdxGdiFontInfoMeasurer.MeasureCharacterWidthF(ACharacter: WideChar; AFontInfo: TdxFontInfo): Single;
var
  ACharacterSize: TSize;
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  SelectObject(MeasureGraphics.Handle, AGdiFontInfo.GdiFontHandle);
  ACharacterSize := MeasureGraphics.TextExtent(ACharacter);
  Result := ACharacterSize.cx;
end;

function TdxGdiFontInfoMeasurer.MeasureString(const AText: string; AFontInfo: TdxFontInfo): Size;
var
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  SelectObject(MeasureGraphics.Handle, AGdiFontInfo.GdiFontHandle);
  Result := MeasureGraphics.TextExtent(AText);
end;

class function TdxGdiFontInfoMeasurer.CreateFont(const AName: string; ASize: Single; const AFontStyle: TFontStyles;
  AUnitConverter: TdxDocumentLayoutUnitConverter): THandle;
begin

  Result := Windows.CreateFont(
    -Round(ASize * AUnitConverter.Dpi / 72),          // height of font
    0,                                                // average character width
    0,                                                // angle of escapement
    0,                                                // base-line orientation angle
    IfThen(fsBold in AFontStyle, FW_BOLD, FW_NORMAL), // font weight
    Byte(fsItalic in AFontStyle),                     // italic attribute option
    Byte(fsUnderline in AFontStyle),                  // underline attribute option
    Byte(fsStrikeOut in AFontStyle),                  // strikeout attribute option
    1{FDefaultCharSet},                               // character set identifier
    OUT_TT_ONLY_PRECIS,                               // output precision
    CLIP_DEFAULT_PRECIS,                              // clipping precision

    SystemFontQuality,

    DEFAULT_PITCH,                                    // pitch and family
    PChar(AName)                                      // typeface name
    );
end;

function TdxGdiFontInfoMeasurer.MeasureMaxDigitWidthF(AFontInfo: TdxFontInfo): Single;
type
  TDigitWidths = packed array[0..9] of TABCFloat;
var
  I: Integer;
  ADigitWidths: TDigitWidths;
  AFont, ADC: THandle;
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
  AWidth: Single;
begin
  AWidth := 0;
  ADC := MeasureGraphics.Handle;
  AFont := AGdiFontInfo.GdiFontHandle;
  SelectObject(ADC, AFont);
  if GetCharABCWidthsFloat(ADC, Ord('0'), Ord('9'), ADigitWidths) then
  begin
    for I := Low(ADigitWidths) to High(ADigitWidths) do
      with ADigitWidths[I] do
        AWidth := Max(AWidth, abcfA + abcfB + abcfC);
  end;
  if AWidth > 0 then
    Result := UnitConverter.PixelsToLayoutUnitsF(AWidth, FDpi) 
  else
    Result := 0;
end;

end.

