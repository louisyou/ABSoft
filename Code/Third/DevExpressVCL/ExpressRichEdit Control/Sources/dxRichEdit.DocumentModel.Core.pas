{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Core;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

{.$DEFINE DXLOGGING}

interface

uses
  Types, Classes, SysUtils, Generics.Collections, dxCore, dxCoreClasses,
  dxRichEdit.Platform.Font, dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.UnitTwipsConverter,
  dxRichEdit.DocumentLayout.UnitConverter, dxRichEdit.DocumentModel.UnitToLayoutUnitConverter,
  dxRichEdit.DocumentModel.DocumentsToLayoutPixelsConverter, dxRichEdit.Utils.BatchUpdateHelper;

type
  TdxParagraphIndex = Integer;
  TdxLayoutUnit = Integer;
  TdxModelUnit  = Integer;

  { TdxDocumentFormat }

  TdxDocumentFormat = (
    Undefined,
    PlainText,
    Rtf,
    Html,
    OpenXml,
    Mht,
    WordML,
    OpenDocument,
    Xaml,
    ePub,
    Doc,
    Last
  );

  TdxEventArgs = class(TObject);
  TdxEventHandler = procedure (ASender: TObject; E: TdxEventArgs) of object;

  TdxDocumentModelPart = class;
  TdxCustomDocumentModel = class;
  TdxHistoryItem = class;

  TdxDocumentModelChangeAction = (
    Redraw,
    ResetPrimaryLayout,
    ResetAllPrimaryLayout,
    ResetSecondaryLayout,
    ResetSelectionLayout,
    ResetCaretInputPositionFormatting,
    RaiseSelectionChanged,
    RaiseContentChanged,
    ScrollToBeginOfDocument,
    RaiseEmptyDocumentCreated,
    RaiseDocumentLoaded,
    RaiseModifiedChanged,
    ResetSpellingCheck,
    ResetIgnoredList,
    ForceResize,
    ValidateSelectionInterval,
    PerformActionsOnIdle,
    SplitRunByCharset,
    ResetRuler,
    SuppressBindingsNotifications,
    ActivePieceTableChanged,
    RaiseDocumentProtectionChanged,
    ForceResetHorizontalRuler,
    ForceResetVerticalRuler,
    ForceSyntaxHighlight,
    ApplyAutoCorrect,
    SuppressRaiseContentChangedCalculationByCurrentTransactionChanges,
    Fields
  );
  TdxDocumentModelChangeActions = set of TdxDocumentModelChangeAction;

  IdxIndexBasedObject = interface
    function GetDocumentModelPart: TdxDocumentModelPart;
    function GetIndex: Integer;
    procedure SetIndex(AIndex: Integer; const AChangeActions: TdxDocumentModelChangeActions);

    property DocumentModelPart: TdxDocumentModelPart read GetDocumentModelPart;
  end;

  IdxIndexBasedObject<TActions> = interface
    function GetDocumentModelPart: TdxDocumentModelPart;
    function GetIndex: Integer;
    procedure SetIndex(AIndex: Integer; const AChangeActions: TActions);

    property DocumentModelPart: TdxDocumentModelPart read GetDocumentModelPart;
  end;

  { TdxDocumentModelPart }

  TdxDocumentModelPart = class(TcxIUnknownObject)
  protected
    FDocumentModel: TdxCustomDocumentModel;
  public
    constructor Create(const ADocumentModel: TdxCustomDocumentModel);
    procedure ApplyChangesCore(const Actions: TdxDocumentModelChangeActions; AStartRunIndex, AEndRunIndex: Integer); virtual; abstract;

    property DocumentModel: TdxCustomDocumentModel read FDocumentModel;
  end;

  { TdxHistoryItem }

  TdxHistoryItem = class abstract
  private
    FDocumentModelPart: TdxDocumentModelPart;
    function GetDocumentModel: TdxCustomDocumentModel; inline;
  protected
    procedure UndoCore; virtual; abstract;
    procedure RedoCore; virtual; abstract;
    function GetChangeModified: Boolean; virtual;
  public
    IsLastInserted: Boolean;
    constructor Create(const ADocumentModelPart: TdxDocumentModelPart); virtual;

    procedure Execute; virtual;
    procedure Undo;  inline; 
    procedure Redo;  inline; 

    property DocumentModelPart: TdxDocumentModelPart read FDocumentModelPart;
    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
    property ChangeModified: Boolean read GetChangeModified;
  end;

  TdxHistoryItemList = class(TObjectList<TdxHistoryItem>);

  { TdxCompositeHistoryItem }

  TdxCompositeHistoryItem = class(TdxHistoryItem)
  private
    FItems: TdxHistoryItemList;
    function GetCount: Integer;
    function GetItem(Index: Integer): TdxHistoryItem;
  protected
    procedure UndoCore; override;
    procedure Clear; virtual;
    procedure RedoCore; override;
    function GetChangeModified: Boolean; override;
  public
    constructor Create(const APart: TdxDocumentModelPart); override;
    destructor Destroy; override;
    procedure AddItem(AItem: TdxHistoryItem);

    property Self[Index: Integer]: TdxHistoryItem read GetItem; default; 
    property Items: TdxHistoryItemList read FItems;
    property Count: Integer read GetCount;
  end;

  { TdxNotificationIdGenerator }

  TdxNotificationIdGenerator = class
  const
    EmptyId = 0;
  private
    FLastId: Integer;
  public
    constructor Create;
    function GenerateId: Integer; virtual;
  end;

  { TdxDocumentHistory }

  TdxDocumentHistory = class
  const
    ForceModifiedIndex = -2;
  private
    FDocumentModel: TdxCustomDocumentModel;
    FUnmodifiedIndex: Integer;
    FCurrentIndex: Integer;
    FIdGenerator: TdxNotificationIdGenerator;
    FTransactionLevel: Integer;
    FDisableCount: Integer;
    FSuppressRaiseOperationComplete: Boolean;
    FItems: TdxHistoryItemList;
    FPreviousModifiedValue: Boolean;
    FTransaction: TdxCompositeHistoryItem;

    FOnOperationCompleted: TdxMulticastMethod<TdxEventHandler>;
    FOnModifiedChanged: TdxMulticastMethod<TdxEventHandler>;

    function GetCurrent: TdxHistoryItem;
    function GetItem(Index: Integer): TdxHistoryItem;
    function GetCanUndo: Boolean;
    function GetCanRedo: Boolean;
    function GetCount: Integer;
    function GetModified: Boolean;
    procedure SetModified(const Value: Boolean);
    function GetIsHistoryDisabled: Boolean;
    procedure ClearCore(ADisposeOnlyCutOffItems: Boolean);
    function CommitTransaction: TdxHistoryItem;
    procedure InternalAdd(AItem: TdxHistoryItem);
    procedure CutOffHistory;
    function CheckIsTransactionChangeModifiedForward(AIndex, AStartInnerIndex: Integer): Boolean;
    function CheckIsTransactionChangeModifiedBackward(AIndex, AStartInnerIndex: Integer): Boolean;
    procedure DisposeContent(ACutOffItemsOnly: Boolean);
  protected
    procedure SetTransaction(AValue: TdxCompositeHistoryItem);
    procedure SetTransactionLevel(AValue: Integer);
    procedure SetModifiedTextAppended(FForceRaiseModifiedChanged: Boolean); virtual;
    function CreateIdGenerator: TdxNotificationIdGenerator; virtual;
    procedure BeginTrackModifiedChanged; virtual;
    procedure EndTrackModifiedChanged; virtual;
    procedure RaiseModifiedChanged; virtual;
    procedure OnEndUndoCore; virtual;
    procedure UndoCore; virtual;
    procedure BeginUndoCurrent; virtual;
    procedure EndUndoCurrent; virtual;
    procedure RedoCore; virtual;
    function CommitAsSingleItem: TdxHistoryItem; virtual;
    function CommitAsSingleItemCore(ASingleItem: TdxHistoryItem): TdxHistoryItem; virtual;
    procedure OnCutOffHistory; virtual;
    function CreateCompositeHistoryItem: TdxCompositeHistoryItem; virtual;

    property Current: TdxHistoryItem read GetCurrent;
    property DocumentModel: TdxCustomDocumentModel read FDocumentModel;
    property UnmodifiedIndex: Integer read FUnmodifiedIndex write FUnmodifiedIndex;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); virtual;
    destructor Destroy; override;
    function GetNotificationId: Integer;
    procedure SmartClear;
    procedure Clear;
    function Add(AItem: TdxHistoryItem): TdxHistoryItem; virtual;
    function BeginSyntaxHighlight: TdxHistoryItem; virtual;
    procedure EndSyntaxHighlight; virtual;
    function HasChangesInCurrentTransaction: Boolean; virtual;
    function IsModified(AUnmodifiedIndex, AUnmodifiedTransactionIndex: Integer): Boolean; overload;
    function IsModified(AUnmodifiedIndex: Integer): Boolean; overload;
    procedure Undo;
    procedure Redo;
    procedure DisableHistory;
    procedure EnableHistory;
    function BeginTransaction: TdxHistoryItem; virtual;
    function EndTransaction: TdxHistoryItem; virtual;
    procedure RaiseOperationCompleted; virtual;

    property IsHistoryDisabled: Boolean read GetIsHistoryDisabled;
    property Items: TdxHistoryItemList read FItems;
    property Self[Index: Integer]: TdxHistoryItem read GetItem; default; 
    property Count: Integer read GetCount;
    property CurrentIndex: Integer read FCurrentIndex write FCurrentIndex;
    property CanUndo: Boolean read GetCanUndo;
    property CanRedo: Boolean read GetCanRedo;
    property Modified: Boolean read GetModified write SetModified;
    property Transaction: TdxCompositeHistoryItem read FTransaction;
    property TransactionLevel: Integer read FTransactionLevel;
    property OperationCompleted: TdxMulticastMethod<TdxEventHandler> read FOnOperationCompleted;
    property SuppressRaiseOperationComplete: Boolean read FSuppressRaiseOperationComplete write FSuppressRaiseOperationComplete;
    property ModifiedChanged: TdxMulticastMethod<TdxEventHandler> read FOnModifiedChanged;
  end;

  { TdxEmptyNotificationIdGenerator }

  TdxEmptyNotificationIdGenerator = class(TdxNotificationIdGenerator)
  public
    function GenerateId: Integer; override;
  end;

  { TdxEmptyHistory }

  TdxEmptyHistory = class(TdxDocumentHistory)
  private
    FPrevItem: TdxHistoryItem;
    FTransactionItemCount: Integer;
    FItems: TdxFastObjectList; 
  protected
    function CreateIdGenerator: TdxNotificationIdGenerator; override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); override;
    destructor Destroy; override;
    function Add(AItem: TdxHistoryItem): TdxHistoryItem; override;
    function BeginTransaction: TdxHistoryItem; override;
    function HasChangesInCurrentTransaction: Boolean; override;
  end;

  { TdxHistoryTransaction }

  TdxHistoryTransaction = class
  private
    FHistory: TdxDocumentHistory;
    FSuppressRaiseOperationComplete: Boolean;

    procedure EndHistoryTransaction;
  public
    constructor Create(AHistory: TdxDocumentHistory);
    destructor Destroy; override;

    property SuppressRaiseOperationComplete: Boolean read FSuppressRaiseOperationComplete write FSuppressRaiseOperationComplete; 
  end;

  { TdxCustomDocumentModel }

  TdxCustomDocumentModel = class(TcxIUnknownObject,
    IdxBatchUpdateable,
    IdxBatchUpdateHandler)
  strict private class var
    FDpi: Double;
    FDpiX: Double;
    FDpiY: Double;
    class constructor Initialize;
  strict private
    FBatchUpdateHelper: TdxBatchUpdateHelper;
    FHistory: TdxDocumentHistory;
    FUnitConverter: TdxDocumentModelUnitConverter;
    FToDocumentLayoutUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
    FLayoutUnitConverter: TdxDocumentLayoutUnitConverter;
    FLayoutUnit: TdxDocumentLayoutUnit; 
    FFontCache: TdxFontCache;
    FFontCacheManager: TdxFontCacheManager;
    function GetFontCache: TdxFontCache;
    procedure SetLayoutUnit(const Value: TdxDocumentLayoutUnit);
  protected
    procedure UpdateFontCache;

    //IdxBatchUpdateable part I
    function GetIsUpdateLocked: Boolean;
    function GetBatchUpdateHelper: TdxBatchUpdateHelper;


    procedure ClearCore; virtual;
    procedure ClearFontCache; virtual;
    procedure DisposeCore; virtual;
    function GetMainPart: TdxDocumentModelPart; virtual; abstract;

    procedure CreateDocumentObjects; virtual;
    procedure DestroyDocumentObjects; virtual;
    procedure Initialize(AAddDefaultsList, AChangeDefaultTableStyle: Boolean); virtual;
    procedure CreateOptions; virtual;
    procedure SubscribeOptionsEvents; virtual;
    procedure SubscribeDocumentObjectsEvents; virtual;

    function CreateDocumentHistory: TdxDocumentHistory; virtual;
    procedure SwitchToEmptyHistory(ADisposeHistory: Boolean); virtual;
    procedure SwitchToNormalHistory(ADisposeHistory: Boolean); virtual;
    procedure DisposeHistory; virtual;
    procedure SubscribeHistoryEvents; virtual; 
    procedure UnsubscribeHistoryEvents; virtual; 
    procedure OnHistoryOperationCompleted(ASender: TObject; E: TdxEventArgs); virtual; abstract; 
    procedure OnHistoryModifiedChanged(ASender: TObject; E: TdxEventArgs); virtual; abstract; 

    property BatchUpdateHelper: TdxBatchUpdateHelper read GetBatchUpdateHelper;
  public
    constructor Create;
    destructor Destroy; override;

    function AddHistoryItem(AItem: TdxHistoryItem): TdxHistoryItem; virtual; abstract;
    procedure ResetMerging; virtual;
    procedure SetFontCacheManager(AFontCacheManager: TdxFontCacheManager); virtual;
    //IdxBatchUpdateable part II
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure CancelUpdate;
    //IdxBatchUpdateHandler
    procedure OnBeginUpdate; virtual; abstract;
    procedure OnEndUpdate; virtual; abstract;
    procedure OnCancelUpdate; virtual; abstract;
    procedure OnFirstBeginUpdate; virtual; abstract;
    procedure OnLastEndUpdate; virtual; abstract;
    procedure OnLastCancelUpdate; virtual; abstract;

    class property Dpi: Double read FDpi;
    class property DpiX: Double read FDpiX;
    class property DpiY: Double read FDpiY;
    property FontCache: TdxFontCache read GetFontCache;
    property FontCacheManager: TdxFontCacheManager read FFontCacheManager write SetFontCacheManager;
    property History: TdxDocumentHistory read FHistory;
    property IsUpdateLocked: Boolean read GetIsUpdateLocked;
    property LayoutUnitConverter: TdxDocumentLayoutUnitConverter read FLayoutUnitConverter;
    property MainPart: TdxDocumentModelPart read GetMainPart;
    property UnitConverter: TdxDocumentModelUnitConverter read FUnitConverter;
    property ToDocumentLayoutUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter read FToDocumentLayoutUnitConverter;
    property LayoutUnit: TdxDocumentLayoutUnit read FLayoutUnit write SetLayoutUnit; 
  end;

function NotImplemented(const AMethodName: string = ''): Pointer;

implementation

uses
  Windows, 
  Forms, Graphics, dxRichEdit.DocumentModel.TwipsToLayoutPixelsConverter, dxRichEdit.DocumentModel.PieceTable;

function NotImplemented(const AMethodName: string = ''): Pointer;
begin
  if AMethodName <> '' then
    raise Exception.Create('Method ' + AMethodName + ' is not implemented')
  else
    raise Exception.Create('Not implemented');
end;

{ TdxDocumentModelPart }

constructor TdxDocumentModelPart.Create(const ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
end;

{ TdxHistoryItem }

constructor TdxHistoryItem.Create(const ADocumentModelPart: TdxDocumentModelPart);
begin
  inherited Create;
  FDocumentModelPart := ADocumentModelPart;
end;


procedure TdxHistoryItem.Execute;
begin
  RedoCore;
end;

function TdxHistoryItem.GetChangeModified: Boolean;
begin
  Result := True;
end;

function TdxHistoryItem.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := FDocumentModelPart.DocumentModel;
end;

procedure TdxHistoryItem.Redo;
begin
  RedoCore;
end;

procedure TdxHistoryItem.Undo;
begin
  UndoCore;
end;

{ TdxCompositeHistoryItem }

constructor TdxCompositeHistoryItem.Create(const APart: TdxDocumentModelPart);
begin
  inherited Create(APart);
  FItems := TdxHistoryItemList.Create; 
end;

destructor TdxCompositeHistoryItem.Destroy;
begin
  FreeAndNil(FItems);
  inherited Destroy;
end;

procedure TdxCompositeHistoryItem.AddItem(AItem: TdxHistoryItem);
begin
  FItems.Add(AItem);
end;

procedure TdxCompositeHistoryItem.Clear;
begin
  FItems.Clear;
end;

function TdxCompositeHistoryItem.GetChangeModified: Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Count - 1 do
    if FItems[I].ChangeModified then
    begin
      Result := True;
      Break;
    end;
end;

function TdxCompositeHistoryItem.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TdxCompositeHistoryItem.GetItem(Index: Integer): TdxHistoryItem;
begin
  Result := FItems[Index];
end;

procedure TdxCompositeHistoryItem.RedoCore;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    FItems[I].Redo;
  end;
end;

procedure TdxCompositeHistoryItem.UndoCore;
var
  I: Integer;
begin
  for I := FItems.Count - 1 downto 0 do
  begin
    FItems[I].Undo;
  end;
end;

{ TdxNotificationIdGenerator }

constructor TdxNotificationIdGenerator.Create;
begin
  inherited Create;
  FLastId := MinInt;
end;

function TdxNotificationIdGenerator.GenerateId: Integer;
begin
  Inc(FLastId);
  if FLastId = EmptyId then
    Inc(FLastId);
  Result := FLastId;
end;

{ TdxDocumentHistory }

constructor TdxDocumentHistory.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create;
  FUnmodifiedIndex := -1;
  FCurrentIndex := -1;
  FItems := TdxHistoryItemList.Create;
  FDocumentModel := ADocumentModel;
  FIdGenerator := CreateIdGenerator;
end;

destructor TdxDocumentHistory.Destroy;
begin
  DisposeContent(False);
  FreeAndNil(FIdGenerator);
  FreeAndNil(FItems);
  inherited Destroy;
end;

function TdxDocumentHistory.Add(AItem: TdxHistoryItem): TdxHistoryItem;
begin
  if TransactionLevel <> 0 then
    Transaction.AddItem(AItem)
  else
    InternalAdd(AItem);
  Result := AItem;
end;

function TdxDocumentHistory.BeginSyntaxHighlight: TdxHistoryItem;
begin
  Result := Transaction;
end;

procedure TdxDocumentHistory.BeginTrackModifiedChanged;
begin
  FPreviousModifiedValue := Modified;
end;

function TdxDocumentHistory.BeginTransaction: TdxHistoryItem;
begin
  if FTransactionLevel = 0 then
    FTransaction := CreateCompositeHistoryItem;
  Inc(FTransactionLevel);
  Result := Transaction;
end;

procedure TdxDocumentHistory.BeginUndoCurrent;
begin
end;

function TdxDocumentHistory.CheckIsTransactionChangeModifiedBackward(AIndex, AStartInnerIndex: Integer): Boolean;
var
  ATransaction: TdxCompositeHistoryItem;
  I: Integer;
begin
  if AIndex < 0 then
    Exit(False);

  Assert(Items[AIndex] is TdxCompositeHistoryItem);

  ATransaction := TdxCompositeHistoryItem(Items[AIndex]);
  for I := AStartInnerIndex downto 0 do
    if ATransaction.Items[I].ChangeModified then
      Exit(True);
  Result := False;
end;

procedure TdxDocumentHistory.DisposeContent(ACutOffItemsOnly: Boolean);
begin
  FreeAndNil(FTransaction);
  if ACutOffItemsOnly then
    CutOffHistory
  else
    Items.Clear;
end;

function TdxDocumentHistory.CheckIsTransactionChangeModifiedForward(AIndex, AStartInnerIndex: Integer): Boolean;
var
  ATransaction: TdxCompositeHistoryItem;
  I: Integer;
begin
  if AIndex < 0 then
    Exit(False);

  Assert(Items[AIndex] is TdxCompositeHistoryItem);

  ATransaction := TdxCompositeHistoryItem(Items[AIndex]);
  for I := AStartInnerIndex to ATransaction.Items.Count - 1 do
    if ATransaction.Items[I].ChangeModified then
      Exit(True);
  Result := False;
end;

procedure TdxDocumentHistory.Clear;
begin
  ClearCore(False);
end;

procedure TdxDocumentHistory.ClearCore(ADisposeOnlyCutOffItems: Boolean);
begin
  DisposeContent(ADisposeOnlyCutOffItems);
  FItems.Clear;
  BeginTrackModifiedChanged;
  try
    FCurrentIndex := -1;
    FUnmodifiedIndex := -1;
  finally
    EndTrackModifiedChanged;
  end;
end;

function TdxDocumentHistory.CommitAsSingleItem: TdxHistoryItem;
begin
  Assert(Transaction.Count = 1);
  Result := CommitAsSingleItemCore(Transaction[0]);
  if Result <> nil then
    Transaction.Items.Extract(Result);
end;

function TdxDocumentHistory.CommitAsSingleItemCore(ASingleItem: TdxHistoryItem): TdxHistoryItem;
begin
  Add(ASingleItem);
  Result := ASingleItem;
end;

function TdxDocumentHistory.CommitTransaction: TdxHistoryItem;
var
  AItemsCount: Integer;
begin
  AItemsCount := Transaction.Count;
  if AItemsCount > 1 then
    Result := Add(FTransaction)
  else
  begin
    if AItemsCount = 1 then
      Result := CommitAsSingleItem
    else
      Result := nil;
  end;
  if AItemsCount <= 1 then
    FTransaction.Free;
  FTransaction := nil;

end;

function TdxDocumentHistory.CreateCompositeHistoryItem: TdxCompositeHistoryItem;
begin
  Result := TdxCompositeHistoryItem.Create(FDocumentModel.MainPart);
end;

function TdxDocumentHistory.CreateIdGenerator: TdxNotificationIdGenerator;
begin
  Result := TdxNotificationIdGenerator.Create;
end;

procedure TdxDocumentHistory.CutOffHistory;
var
  AIndex: Integer;
begin
  AIndex := FCurrentIndex + 1;
  if AIndex < Count then
    OnCutOffHistory;
  while AIndex < Count do
    FItems.Delete(AIndex);
  if FUnmodifiedIndex > FCurrentIndex then
    FUnmodifiedIndex := ForceModifiedIndex;
end;

procedure TdxDocumentHistory.DisableHistory;
begin
  Inc(FDisableCount);
end;

procedure TdxDocumentHistory.EnableHistory;
begin
  if FDisableCount > 0 then
    Dec(FDisableCount);
end;

procedure TdxDocumentHistory.EndSyntaxHighlight;
begin
end;

procedure TdxDocumentHistory.EndTrackModifiedChanged;
begin
  if FPreviousModifiedValue <> Modified then
    RaiseModifiedChanged;
end;

function TdxDocumentHistory.EndTransaction: TdxHistoryItem;
begin
  Result := FTransaction;
  if FTransactionLevel > 0 then
  begin
    Dec(FTransactionLevel);
    if FTransactionLevel = 0 then
      Result := CommitTransaction;
  end;
end;

procedure TdxDocumentHistory.EndUndoCurrent;
begin
end;

function TdxDocumentHistory.GetCanRedo: Boolean;
begin
  Result := (Count > 0) and (CurrentIndex < Count - 1);
end;

function TdxDocumentHistory.GetCanUndo: Boolean;
begin
  Result := CurrentIndex >= 0;
end;

function TdxDocumentHistory.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TdxDocumentHistory.GetCurrent: TdxHistoryItem;
begin
  Result := nil;
  if (CurrentIndex >= 0) and (CurrentIndex < Count) then
     Result := Items[CurrentIndex];
end;

function TdxDocumentHistory.GetIsHistoryDisabled: Boolean;
begin
  Result := FDisableCount > 0;
end;

function TdxDocumentHistory.GetItem(Index: Integer): TdxHistoryItem;
begin
  Result := FItems[Index];
end;

function TdxDocumentHistory.GetModified: Boolean;
begin
  Result := IsModified(FUnmodifiedIndex);
end;

function TdxDocumentHistory.GetNotificationId: Integer;
begin
  Result := FIdGenerator.GenerateId;
end;

function TdxDocumentHistory.HasChangesInCurrentTransaction: Boolean;
begin
  if FTransactionLevel <= 0 then
    Result := False
  else
    Result := FTransaction.Count > 0;
end;

function TdxDocumentHistory.IsModified(AUnmodifiedIndex, AUnmodifiedTransactionIndex: Integer): Boolean;
var
  I: Integer;
begin
  if CurrentIndex = AUnmodifiedIndex then
  begin
    Result := (AUnmodifiedTransactionIndex >= 0) and (CheckIsTransactionChangeModifiedBackward(CurrentIndex, AUnmodifiedTransactionIndex));
    Exit;
  end;
  if AUnmodifiedIndex < -1 then
    Exit(True);
  if AUnmodifiedIndex < CurrentIndex then
  begin
    Inc(AUnmodifiedIndex);
    if AUnmodifiedTransactionIndex >= 0 then
    begin
      if CheckIsTransactionChangeModifiedForward(AUnmodifiedIndex, AUnmodifiedTransactionIndex + 1) then
        Exit(True);
      Inc(AUnmodifiedIndex);
    end;
    I := AUnmodifiedIndex;
    while (I <= CurrentIndex) and (I < Count) do
    begin
      if FItems[I].ChangeModified then
        Exit(True);
      Inc(I);
    end;
  end
  else
  begin
    if AUnmodifiedTransactionIndex >= 0 then
    begin
      CheckIsTransactionChangeModifiedBackward(AUnmodifiedIndex, AUnmodifiedTransactionIndex);
      Dec(AUnmodifiedIndex);
    end;
    I := CurrentIndex + 1;
    while (I <= AUnmodifiedIndex) and (I < Count) do
    begin
      if Items[I].ChangeModified then
        Exit(True);
      Inc(I);
    end;
  end;
  Result := False;
end;

procedure TdxDocumentHistory.InternalAdd(AItem: TdxHistoryItem);
begin
  if not IsHistoryDisabled then
  begin
    CutOffHistory;
    FItems.Add(AItem);
    BeginTrackModifiedChanged;
    try
      Inc(FCurrentIndex);
    finally
      EndTrackModifiedChanged;
    end;
  end;
  if not FSuppressRaiseOperationComplete then
    RaiseOperationCompleted;
end;

function TdxDocumentHistory.IsModified(AUnmodifiedIndex: Integer): Boolean;
begin
  Result := IsModified(AUnmodifiedIndex, -1);
end;

procedure TdxDocumentHistory.OnCutOffHistory;
begin
end;

procedure TdxDocumentHistory.OnEndUndoCore;
begin
end;

procedure TdxDocumentHistory.RaiseModifiedChanged;
begin
  FOnModifiedChanged.Invoke(Self, nil); 
end;

procedure TdxDocumentHistory.RaiseOperationCompleted;
begin
  FOnOperationCompleted.Invoke(Self, nil); 
end;

procedure TdxDocumentHistory.Redo;
begin
  if CanRedo then
  begin
    DisableHistory;
    try
      FDocumentModel.BeginUpdate;
      try
        BeginTrackModifiedChanged;
        try
          RedoCore;
        finally
          EndTrackModifiedChanged;
        end;
      finally
      documentModel.EndUpdate;
      end;
    finally
      EnableHistory;
    end;
  end;
end;

procedure TdxDocumentHistory.RedoCore;
begin
  Inc(FCurrentIndex);
  Current.Redo;
  RaiseOperationCompleted;
end;

procedure TdxDocumentHistory.SetModified(const Value: Boolean);
begin
  if Value <> Modified then
  begin
    if Value then
      FUnmodifiedIndex := ForceModifiedIndex
    else
      FUnmodifiedIndex := CurrentIndex;
    RaiseModifiedChanged;
  end;
end;

procedure TdxDocumentHistory.SetModifiedTextAppended(FForceRaiseModifiedChanged: Boolean);
begin
  if not Modified then
  begin
    Dec(FUnmodifiedIndex);
    RaiseModifiedChanged;
  end;
end;

procedure TdxDocumentHistory.SetTransaction(AValue: TdxCompositeHistoryItem);
begin
  FTransaction := AValue;
end;

procedure TdxDocumentHistory.SetTransactionLevel(AValue: Integer);
begin
  FTransactionLevel := AValue;
end;

procedure TdxDocumentHistory.SmartClear;
begin
  ClearCore(True);
end;

procedure TdxDocumentHistory.Undo;
begin
  if CanUndo then
  begin
    DisableHistory;
    try
      FDocumentModel.BeginUpdate;
      try
        BeginTrackModifiedChanged;
        try
          UndoCore;
        finally
          EndTrackModifiedChanged;
        end;
      finally
        FDocumentModel.EndUpdate;
      end;
    finally
      EnableHistory;
    end;
  end;
end;

procedure TdxDocumentHistory.UndoCore;
begin
  BeginUndoCurrent;
  Current.Undo;
  Dec(FCurrentIndex);
  EndUndoCurrent;
  OnEndUndoCore;
  RaiseOperationCompleted;
end;

{ TdxEmptyNotificationIdGenerator }

function TdxEmptyNotificationIdGenerator.GenerateId: Integer;
begin
  Result := EmptyId;
end;

{ TdxEmptyHistory }

constructor TdxEmptyHistory.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create(ADocumentModel);
  FItems := TdxFastObjectList.Create(True, 1024);
end;

destructor TdxEmptyHistory.Destroy;
begin
  FItems.Free;
  inherited Destroy;
end;

function TdxEmptyHistory.Add(AItem: TdxHistoryItem): TdxHistoryItem;
begin
  Inc(FTransactionItemCount);
  Assert(FPrevItem <> AItem);
  FItems.Add(AItem);
  Result := AItem;
end;

function TdxEmptyHistory.BeginTransaction: TdxHistoryItem;
begin
  if TransactionLevel = 0 then
    FTransactionItemCount := 0;
  Result := inherited BeginTransaction;
end;

function TdxEmptyHistory.HasChangesInCurrentTransaction: Boolean;
begin
  if TransactionLevel <= 0 then
    Result := inherited HasChangesInCurrentTransaction
  else
    Result := FTransactionItemCount <> 0;
end;

function TdxEmptyHistory.CreateIdGenerator: TdxNotificationIdGenerator;
begin
  Result := TdxEmptyNotificationIdGenerator.Create;
end;

{ TdxHistoryTransaction }

constructor TdxHistoryTransaction.Create(AHistory: TdxDocumentHistory);
begin
  inherited Create;
  Assert(AHistory <> nil);
  FHistory := AHistory;
  AHistory.BeginTransaction;
end;

destructor TdxHistoryTransaction.Destroy;
begin
  EndHistoryTransaction;
  inherited Destroy;
end;

procedure TdxHistoryTransaction.EndHistoryTransaction;
var
  ATopLevelTransaction: Boolean;
begin
  ATopLevelTransaction := FHistory.TransactionLevel = 1;
  if ATopLevelTransaction and SuppressRaiseOperationComplete then
  begin
    FHistory.SuppressRaiseOperationComplete := True;
    FHistory.EndTransaction;
    FHistory.SuppressRaiseOperationComplete := False;
  end
  else
    FHistory.EndTransaction;
end;

{ TdxCustomDocumentModel }

constructor TdxCustomDocumentModel.Create;
begin
  inherited Create;
  FLayoutUnit := TdxDocumentLayoutUnit.Document; 
  FBatchUpdateHelper := TdxBatchUpdateHelper.Create(Self);
  FUnitConverter := TdxDocumentModelUnitTwipsConverter.Create;
  FToDocumentLayoutUnitConverter := TdxDocumentModelTwipsToLayoutPixelsConverter.Create(Dpi); 
  FLayoutUnitConverter := TdxDocumentLayoutUnitConverter.CreateConverter(TdxDocumentLayoutUnit.Pixel, Dpi); 
end;

class constructor TdxCustomDocumentModel.Initialize;
var
  DC: HDC;
begin

  DC := GetDC(0);
  FDpi := GetDeviceCaps(DC, LOGPIXELSY); 
  ReleaseDC(0, DC);

  FDpiX := FDpi;
  FDpiY := FDpi;
end;

destructor TdxCustomDocumentModel.Destroy;
begin
  DisposeCore;
  DestroyDocumentObjects;

  FreeAndNil(FBatchUpdateHelper);
  FreeAndNil(FLayoutUnitConverter);
  FreeAndNil(FToDocumentLayoutUnitConverter);
  FreeAndNil(FUnitConverter);
  inherited Destroy;
end;

procedure TdxCustomDocumentModel.DestroyDocumentObjects;
begin
end;

procedure TdxCustomDocumentModel.BeginUpdate;
begin
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxCustomDocumentModel.ClearCore;
begin
  ClearFontCache;
  if FHistory <> nil then
  begin
    UnsubscribeHistoryEvents;
    FreeAndNil(FHistory);
  end;
end;

procedure TdxCustomDocumentModel.ClearFontCache;
begin
  if FFontCache <> nil then
  begin
    if FFontCacheManager <> nil then
      FFontCacheManager.ReleaseFontCache(FFontCache);
    FFontCache := nil;
  end;
end;

procedure TdxCustomDocumentModel.DisposeCore;
begin
  ClearCore;
  FreeAndNil(FFontCache); 
  FreeAndNil(FFontCacheManager);
end;

procedure TdxCustomDocumentModel.ResetMerging;
begin
end;

function TdxCustomDocumentModel.CreateDocumentHistory: TdxDocumentHistory;
begin
  Result := TdxDocumentHistory.Create(Self);
end;

procedure TdxCustomDocumentModel.CreateDocumentObjects;
begin
end;

procedure TdxCustomDocumentModel.SwitchToEmptyHistory(ADisposeHistory: Boolean);
begin
  if ADisposeHistory then
    DisposeHistory;
  FHistory := TdxEmptyHistory.Create(Self);
  SubscribeHistoryEvents;
end;

procedure TdxCustomDocumentModel.SwitchToNormalHistory(ADisposeHistory: Boolean);
begin
  if ADisposeHistory then
    DisposeHistory;
  FHistory := CreateDocumentHistory;
  ResetMerging;
  SubscribeHistoryEvents;
end;

procedure TdxCustomDocumentModel.DisposeHistory;
begin
  if History <> nil then
  begin
    UnsubscribeHistoryEvents;
    FreeAndNil(FHistory);
  end;
end;

procedure TdxCustomDocumentModel.SubscribeHistoryEvents;
begin
  History.OperationCompleted.Add(OnHistoryOperationCompleted);
  History.ModifiedChanged.Add(OnHistoryModifiedChanged);
end;

procedure TdxCustomDocumentModel.UnsubscribeHistoryEvents;
begin
  History.OperationCompleted.Remove(OnHistoryOperationCompleted);
  History.ModifiedChanged.Remove(OnHistoryModifiedChanged);
end;

procedure TdxCustomDocumentModel.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

procedure TdxCustomDocumentModel.CancelUpdate;
begin
  FBatchUpdateHelper.CancelUpdate;
end;

function TdxCustomDocumentModel.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

function TdxCustomDocumentModel.GetFontCache: TdxFontCache;
begin
  Result := FFontCache;
end;

function TdxCustomDocumentModel.GetIsUpdateLocked: Boolean;
begin
  Result := FBatchUpdateHelper.IsUpdateLocked;
end;

procedure TdxCustomDocumentModel.Initialize(AAddDefaultsList,
  AChangeDefaultTableStyle: Boolean);
begin
  CreateOptions;
  UpdateFontCache;
  CreateDocumentObjects;
end;

procedure TdxCustomDocumentModel.CreateOptions;
begin
end;

procedure TdxCustomDocumentModel.SubscribeOptionsEvents;
begin
end;

procedure TdxCustomDocumentModel.SubscribeDocumentObjectsEvents;
begin
end;

procedure TdxCustomDocumentModel.SetFontCacheManager(AFontCacheManager: TdxFontCacheManager);
begin
  if AFontCacheManager = FontCacheManager then
    Exit;
  ClearFontCache;
  FreeAndNil(FFontCacheManager);
  FFontCacheManager := AFontCacheManager;
  FFontCache := AFontCacheManager.CreateFontCache;
end;

procedure TdxCustomDocumentModel.SetLayoutUnit(const Value: TdxDocumentLayoutUnit);
begin
  if FLayoutUnit = Value then
    Exit;
  FLayoutUnit := Value;
end;

procedure TdxCustomDocumentModel.UpdateFontCache;
begin
  FFontCacheManager.Free;
  FFontCacheManager := TdxFontCacheManager.CreateDefault(LayoutUnitConverter);
  ClearFontCache;
  FFontCache := FFontCacheManager.CreateFontCache;
end;


end.
