{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Commands.Selection;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Generics.Collections, dxRichEdit.DocumentModel.Core, dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.Control.HitTest, dxRichEdit.DocumentModel.PieceTableIterators,
  dxRichEdit.DocumentModel.PieceTableModifiers,
  dxRichEdit.DocumentModel.Commands, dxRichEdit.Commands, dxRichEdit.View.Core, dxRichEdit.Commands.IDs;

type
  { TdxEnhancedSelectionManager }

  TdxEnhancedSelectionManager = class
  private
    FPieceTable: TdxPieceTable;
    FMinDistance: Integer;
    FMinBorderWidthToHitTest: Integer;
    function GetSelectionEndParagraphIndex(ASelection: TdxSelection): TdxParagraphIndex;
    function GetDocumentModel: TdxDocumentModel;
  public
    function CalculateTableRowToResize(AHitTestResult: TdxRichEditHitTestResult): TdxTableRowViewInfoBase;
    function ShouldResizeTableRow(AControl: IdxRichEditControl;
      AHitTestResult: TdxRichEditHitTestResult; ATableRow: TdxTableRowViewInfoBase): Boolean;
    function ShouldSelectEntireTableCell(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldSelectEntireTableColumn(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldSelectEntireTableRow(AHitTestResult: TdxRichEditHitTestResult): Boolean;
  public
    constructor Create(APieceTable: TdxPieceTable);

    function ShouldSelectToTheEndOfRow(AHitTestResult: TdxRichEditHitTestResult): Boolean; virtual;
    function ShouldSelectEntireRow(AHitTestResult: TdxRichEditHitTestResult): Boolean;
    function ShouldSelectFloatingObject(AHitTestResult: TdxRichEditHitTestResult): Boolean; virtual; 
    function ExtendSelectionEndToParagraphMark(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
    function ExtendSelectionStartToParagraphMark(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition; virtual;

    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property PieceTable: TdxPieceTable read FPieceTable;


  end;

  { TdxExtendSelectionByRangesCommandBase }

  TdxExtendSelectionByRangesCommandBase = class abstract(TdxPlaceCaretToPhysicalPointCommand)
  private
    FInitialBox: TdxBox;
    FInitialLogPosition: TdxDocumentLogPosition;
    procedure SetInitialBox(const Value: TdxBox);
  protected
    function ExtendSelection: Boolean; override;
    function HitTestOnlyInPageClientBounds: Boolean; override;
  public
    property InitialBox: TdxBox read FInitialBox write SetInitialBox;
    property InitialLogPosition: TdxDocumentLogPosition read FInitialLogPosition;
  end;

  TdxExtendSelectionByCharactersCommand = class(TdxExtendSelectionByRangesCommandBase);

  { TdxExtendSelectionByRangesCommand }

  TdxExtendSelectionByRangesCommand = class(TdxExtendSelectionByRangesCommandBase)
  protected
    function ChangeSelectionEnd(ASelection: TdxSelection; ALogPosition: Integer;
      AHitTestResult: TdxRichEditHitTestResult): Boolean; override;
    procedure ChangeSelectionStart(ASelection: TdxSelection;
      ALogPosition: Integer; AHitTestResult: TdxRichEditHitTestResult); override;

    function CreateIterator: TdxPieceTableIterator; virtual; abstract;
    function ExtendEnd1(ASelection: TdxSelection; AHitTestResult: TdxRichEditHitTestResult): Boolean; virtual;
    function ExtendEnd2(AHitTestResult: TdxRichEditHitTestResult): TdxDocumentLogPosition; virtual;
    function ExtendStart1: TdxDocumentLogPosition; virtual;
    function ExtendStart2: TdxDocumentLogPosition; virtual;
  end;

  { TdxExtendSelectionByWordsCommand }

  TdxExtendSelectionByWordsCommand = class(TdxExtendSelectionByRangesCommand)
  protected
    function CreateIterator: TdxPieceTableIterator; override;
  end;

  { TdxExtendSelectionByParagraphsCommand }

  TdxExtendSelectionByParagraphsCommand = class(TdxExtendSelectionByRangesCommand)
  protected
    function CreateIterator: TdxPieceTableIterator; override;
  end;

  { TdxExtendSelectionByLinesCommand }

  TdxExtendSelectionByLinesCommand = class(TdxExtendSelectionByRangesCommandBase)
  protected
    function ExtendEnd1(ASelection: TdxSelection; AHitTestResult: TdxRichEditHitTestResult): Boolean; virtual;
    function ExtendEnd2(AHitTestResult: TdxRichEditHitTestResult): TdxDocumentLogPosition; virtual;
    function ExtendStart1: TdxDocumentLogPosition; virtual;
    function ExtendStart2: TdxDocumentLogPosition; virtual;
    function ChangeSelectionEnd(ASelection: TdxSelection; ALogPosition: Integer;
      AHitTestResult: TdxRichEditHitTestResult): Boolean; override;
    procedure ChangeSelectionStart(ASelection: TdxSelection;
      ALogPosition: Integer; AHitTestResult: TdxRichEditHitTestResult); override;
  end;

  { TdxRichEditSelectionCommand }

  TdxRichEditSelectionCommand = class(TdxRichEditCaretBasedCommand) 
  private
    FShouldEnsureCaretVisibleVerticallyBeforeUpdate: Boolean;
    FShouldEnsureCaretVisibleVerticallyAfterUpdate: Boolean;
  protected
    function GetTryToKeepCaretX: Boolean; virtual; abstract;
    function GetTreatStartPositionAsCurrent: Boolean; virtual; abstract;
    function GetExtendSelection: Boolean; virtual; abstract;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; virtual; abstract;
    function GetShouldUpdateCaretY: Boolean; virtual;
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; virtual; abstract; 
    function ChangePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition; virtual; abstract; 
    procedure PerformModifyModel; virtual; 
		function CalculateSelectionCurrentPosition(ASelection: TdxSelection): TdxDocumentModelPosition; virtual; 
    function ExtendSelectionStartToParagraphMark(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition; virtual; 
    function ExtendSelectionEndToParagraphMark(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition; virtual; 
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: TdxDocumentLogPosition); virtual; 
    procedure ChangeSelection(ASelection: TdxSelection); virtual; 
    procedure ValidateSelection(ASelection: TdxSelection; AIsSelectionExtended: Boolean); virtual; 
    function ApplyNewPositionToSelectionEnd(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition): Boolean; virtual; 
    procedure ChangeSelectionStart(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition); virtual; 
    procedure BeforeUpdate; virtual; 
    procedure AfterUpdate; virtual; 
    function PerformChangeSelection: Boolean; virtual; 
    procedure EnsureCaretVisibleVertically; virtual; 
    function GetSelectionEndParagraphIndex: TdxParagraphIndex; virtual; 
    function IsSelectionEndInTableCell: Boolean; virtual; 
    function IsSelectionEndAfterTableCell: Boolean; virtual; 
    function GetSelectionEndTableCell: TdxTableCell; virtual; 
    function CreateEnsureCaretVisibleVerticallyCommand: TdxRichEditCommand; virtual;
    procedure UpdateCaretPosition; reintroduce; overload; virtual; 
		procedure UpdateUIStateCore(const AState: IdxCommandUIState); override; 
    function GetLeftVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition; virtual; 
    procedure EnsureCaretVisible; 
    property TryToKeepCaretX: Boolean read GetTryToKeepCaretX; 
    property TreatStartPositionAsCurrent: Boolean read GetTreatStartPositionAsCurrent; 
    property ExtendSelection: Boolean read GetExtendSelection; 
    property UpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel read GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel; 
    property ShouldUpdateCaretY: Boolean read GetShouldUpdateCaretY; 
    property ShouldEnsureCaretVisibleVerticallyBeforeUpdate: Boolean read FShouldEnsureCaretVisibleVerticallyBeforeUpdate write FShouldEnsureCaretVisibleVerticallyBeforeUpdate; 
    property ShouldEnsureCaretVisibleVerticallyAfterUpdate: Boolean read FShouldEnsureCaretVisibleVerticallyAfterUpdate write FShouldEnsureCaretVisibleVerticallyAfterUpdate;  
  public
    constructor Create(const AControl: IdxRichEditControl); override;
    procedure ExecuteCore; override;
  end;

  { TdxSelectFieldNextPrevToCaretCommand }

  TdxSelectFieldNextPrevToCaretCommand = class(TdxRichEditSelectionCommand)
  protected
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
    procedure ChangeSelection(ASelection: TdxSelection); override;

    function GetDeletedPosition(ASelection: TdxSelection): TdxDocumentModelPosition; virtual;
    function IsFieldRun(ARun: TdxTextRunBase): Boolean;
  public
    procedure UpdateUIState(const AState: IdxCommandUIState); override;

  end;

  { TdxSelectFieldPrevToCaretCommand }

  TdxSelectFieldPrevToCaretCommand = class(TdxSelectFieldNextPrevToCaretCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition; override;
    function GetDeletedPosition(ASelection: TdxSelection): TdxDocumentModelPosition; override;
  end;

  { TdxSelectFieldNextToCaretCommand }

  TdxSelectFieldNextToCaretCommand = class(TdxSelectFieldNextPrevToCaretCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
  end;

  { TdxSelectAllCommand }

  TdxSelectAllCommand = class(TdxRichEditSelectionCommand)
  protected
    function GetTryToKeepCaretX: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetExtendSelection: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    procedure EnsureCaretVisibleVertically; override;
    procedure ChangeSelection(ASelection: TdxSelection); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxChangeActivePieceTableCommand }

  TdxChangeActivePieceTableCommand = class(TdxRichEditSelectionCommand)
  strict private
    FNewActivePieceTable: TdxPieceTable;
    FSection: TdxSection;
    FPreferredPageIndex: Integer;
  protected
    procedure AfterUpdate; override;
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
    function PerformChangeSelection: Boolean; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

    procedure SetSelection(AStart: TdxDocumentLogPosition; ALength: Integer); virtual;
  public
    constructor Create(AControl: IdxRichEditControl; ANewActivePieceTable: TdxPieceTable;
      ASection: TdxSection; APreferredPageIndex: Integer); reintroduce;
    procedure ActivatePieceTable(ANewPieceTable: TdxPieceTable; ASection: TdxSection); virtual;
  end;

  { TdxStartOfLineCommand }

  TdxStartOfLineCommand = class(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendStartOfLineCommand }

  TdxExtendStartOfLineCommand = class(TdxStartOfLineCommand)
  protected
    function GetExtendSelection: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxEndOfLineCommand }

  TdxEndOfLineCommand = class(TdxRichEditSelectionCommand)
  protected
    function ApplyNewPositionToSelectionEnd(ASelection: TdxSelection;
      ALogPosition: Integer): Boolean; override;
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function ExtendSelectionEndToParagraphMark(ASelection: TdxSelection;
      ALogPosition: Integer): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendEndOfLineCommand }

  TdxExtendEndOfLineCommand = class(TdxEndOfLineCommand)
  protected
    function GetExtendSelection: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxPreviousCharacterCommand }

  TdxPreviousCharacterCommand = class(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;

    function GetPrevVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendPreviousCharacterCommand }

  TdxExtendPreviousCharacterCommand = class(TdxPreviousCharacterCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxPrevNextWordCommand }

  TdxPrevNextWordCommand = class abstract(TdxRichEditSelectionCommand)
  private
    function GetVisibleTextFilter: IVisibleTextFilter;
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;

    function GetNextVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
    function GetPrevVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
    function GetModelPosition(const APos: TdxDocumentLogPosition): TdxDocumentModelPosition;

    property VisibleTextFilter: IVisibleTextFilter read GetVisibleTextFilter;
  end;

  { TdxNextCharacterCommand }

  TdxNextCharacterCommand = class(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;

    function GetMaxPosition: TdxDocumentLogPosition; virtual;
    function GetNextVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendNextCharacterCommand }

  TdxExtendNextCharacterCommand = class(TdxNextCharacterCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetMaxPosition: Integer; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxPreviousWordCommand }

  TdxPreviousWordCommand = class(TdxPrevNextWordCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendPreviousWordCommand }

  TdxExtendPreviousWordCommand = class(TdxPreviousWordCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxNextWordCommand }

  TdxNextWordCommand = class(TdxPrevNextWordCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendNextWordCommand }

  TdxExtendNextWordCommand = class(TdxNextWordCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxLineUpDownCommandBase }

  TdxLineUpDownCommandBase = class(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;

    function CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; virtual; abstract;
  end;

  { TdxPreviousLineCommand }

  TdxPreviousLineCommand = class(TdxLineUpDownCommandBase)
  protected
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendPreviousLineCommand }

  TdxExtendPreviousLineCommand = class(TdxPreviousLineCommand)
  protected
    function CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; override;
    function GetExtendSelection: Boolean; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxNextLineCommand }

  TdxNextLineCommand = class(TdxLineUpDownCommandBase)
  protected
    function CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendNextLineCommand }

  TdxExtendNextLineCommand = class(TdxNextLineCommand)
  protected
    function CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; override;
    function GetExtendSelection: Boolean; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxPrevNextParagraphCommandBase }

  TdxPrevNextParagraphCommandBase = class(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;

    function GetNextVisiblePosition(ARunIndex: TdxRunIndex): TdxDocumentModelPosition; overload; virtual;
    function GetNextVisiblePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition; overload; virtual;
    function GetPrevVisiblePosition(ARunIndex: TdxRunIndex): TdxDocumentModelPosition; overload; virtual;
    function GetPrevVisiblePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition; overload; virtual;
    function GetValidLogPosition(const ANewPos: TdxDocumentModelPosition; AParagraph: TdxParagraph): TdxDocumentLogPosition; virtual;
    function GetVisibleLogPosition(AParagraph: TdxParagraph): TdxDocumentLogPosition; virtual;
    function VisibleTextFilter: IVisibleTextFilter;
  end;

  TdxPreviousParagraphCommand = class(TdxPrevNextParagraphCommandBase)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetValidLogPosition(const ANewPos: TdxDocumentModelPosition;
      AParagraph: TdxParagraph): Integer; override;

    function GetMinPosition: TdxDocumentLogPosition;
    function GetPrevPosition(AParagraphIndex: TdxParagraphIndex): TdxDocumentLogPosition;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendPreviousParagraphCommand }

  TdxExtendPreviousParagraphCommand = class(TdxPreviousParagraphCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxNextParagraphCommand }

  TdxNextParagraphCommand = class(TdxPrevNextParagraphCommandBase)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetMaxPosition: TdxDocumentLogPosition; virtual;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendNextParagraphCommand }

  TdxExtendNextParagraphCommand = class(TdxNextParagraphCommand)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetMaxPosition: Integer; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxPrevNextPageCommandBase }

  TdxPrevNextPageCommandBase = class abstract(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function CreateEnsureCaretVisibleVerticallyCommand: TdxRichEditCommand; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
  end;

  { TdxPreviousPageCommand }

  TdxPreviousPageCommand = class(TdxPrevNextPageCommandBase)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendPreviousPageCommand }

  TdxExtendPreviousPageCommand = class(TdxPreviousPageCommand)
  protected
    function GetExtendSelection: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxNextPageCommand }

  TdxNextPageCommand = class(TdxPrevNextPageCommandBase)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;

    function GetLastPosition(APageIndex: Integer): TdxDocumentLogPosition; virtual;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendNextPageCommand }

  TdxExtendNextPageCommand = class(TdxNextPageCommand)
  protected
    function GetExtendSelection: Boolean; override;
    function GetLastPosition(APageIndex: Integer): Integer; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxPrevNextScreenCommandBase }

  TdxPrevNextScreenCommandBase = class abstract(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    procedure ChangeSelection(ASelection: TdxSelection); override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;

    function CalculateCaretNewLogPosition(const APhysicalPoint: TPoint): TdxDocumentLogPosition;
    function CreateCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; virtual; abstract;
    function CreatePlaceCaretToPhysicalPointCommand: TdxPlaceCaretToPhysicalPointCommand; virtual;
    function GetDefaultLogPosition: TdxDocumentLogPosition; virtual; abstract;
    function GetTargetRowAtColumn(AColumn: TdxColumn): TdxRow; virtual; abstract;
    function IsNewPositionInCorrectDirection(APreviousLogPosition, ACurrentLogPosition: TdxDocumentLogPosition): Boolean; virtual; abstract;
    function ScrollScreen: Boolean; virtual; abstract;
    function ShouldCalculateNewCaretLogPosition(APreviousLogPosition, ACurrentLogPosition: TdxDocumentLogPosition): Boolean; virtual; abstract;
    function ShouldUseAnotherPage(AColumn: TdxColumn; const ALogicalCaretPoint: TPoint): Boolean; virtual; abstract;
  end;

  { TdxPreviousScreenCommand }

  TdxPreviousScreenCommand = class(TdxPrevNextScreenCommandBase)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function CreateCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; override;
    function GetDefaultLogPosition: Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTargetRowAtColumn(AColumn: TdxColumn): TdxRow; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function IsNewPositionInCorrectDirection(APreviousLogPosition: Integer;
      ACurrentLogPosition: Integer): Boolean; override;
    function ScrollScreen: Boolean; override;
    function ShouldCalculateNewCaretLogPosition(APreviousLogPosition: Integer;
      ACurrentLogPosition: Integer): Boolean; override;
    function ShouldUseAnotherPage(AColumn: TdxColumn; const ALogicalCaretPoint: TPoint): Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendPreviousScreenCommand }

  TdxExtendPreviousScreenCommand = class(TdxPreviousScreenCommand)
  protected
    function CreatePlaceCaretToPhysicalPointCommand: TdxPlaceCaretToPhysicalPointCommand; override;
    function GetExtendSelection: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxNextScreenCommand }

  TdxNextScreenCommand = class(TdxPrevNextScreenCommandBase)
  protected
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function CreateCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator; override;
    function GetDefaultLogPosition: Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTargetRowAtColumn(AColumn: TdxColumn): TdxRow; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function IsNewPositionInCorrectDirection(APreviousLogPosition: Integer;
      ACurrentLogPosition: Integer): Boolean; override;
    function ScrollScreen: Boolean; override;
    function ShouldUseAnotherPage(AColumn: TdxColumn;
      const ALogicalCaretPoint: TPoint): Boolean; override;
    function ShouldCalculateNewCaretLogPosition(APreviousLogPosition: Integer;
      ACurrentLogPosition: Integer): Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendNextScreenCommand }

  TdxExtendNextScreenCommand = class(TdxNextScreenCommand)
  protected
    function CreatePlaceCaretToPhysicalPointCommand: TdxPlaceCaretToPhysicalPointCommand; override;
    function GetExtendSelection: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxStartOfDocumentCommand }

  TdxStartOfDocumentCommand = class(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendStartOfDocumentCommand }

  TdxExtendStartOfDocumentCommand = class(TdxStartOfDocumentCommand)
  protected
    function GetExtendSelection: Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxEndOfDocumentCommand }

  TdxEndOfDocumentCommand = class(TdxRichEditSelectionCommand)
  protected
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxExtendEndOfDocumentCommand }

  TdxExtendEndOfDocumentCommand = class(TdxEndOfDocumentCommand)
  protected
    function GetExtendSelection: Boolean; override;
    procedure UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

    { TdxSelectionBasedCommandBase }

  TdxSelectionBasedCommandBase = class abstract(TdxRichEditCommand)
  protected
    function CalculateStartPosition(AItem: TdxSelectionItem; AllowSelectionExpanding: Boolean): TdxDocumentModelPosition; virtual;
    function CalculateEndPosition(AItem: TdxSelectionItem; AllowSelectionExpanding: Boolean): TdxDocumentModelPosition; virtual;
  end;

  { TdxExtendSelectionToPhysicalPointCommand }

  TdxExtendSelectionToPhysicalPointCommand = class(TdxPlaceCaretToPhysicalPointCommand)
  protected
    function ExtendSelection: Boolean; override;
  end;

  { TdxSelectionBasedPropertyChangeCommandBase }

  TdxSelectionBasedPropertyChangeCommandBase = class(TdxSelectionBasedCommandBase)
  protected
    function ActivePieceTableObtainParagraphsPropertyValue<T>(ALogPositionStart: TdxDocumentLogPosition;
      ALength: Integer; AModifier: TdxParagraphPropertyModifier<T>; out AValue: T): Boolean;
    function ActivePieceTableObtainRunsPropertyValue<T>(const ALogPositionStart: TdxDocumentLogPosition;
      ALength: Integer; AModifier: TdxRunPropertyModifier<T>; out AValue: T): Boolean;

    function CanEditSelection: Boolean; override;
    function ChangeProperty(const AStart, AEnd: TdxDocumentModelPosition;
      const AState: IdxCommandUIState): TdxDocumentModelChangeActions; virtual; abstract;
    function GetSelectionItems: TdxSelectionItems;
    procedure ModifyDocumentModel(AState: IdxCommandUIState); virtual;
    procedure ModifyDocumentModelCore(AState: IdxCommandUIState); virtual;
    function ValidateSelectionInterval(AItem: TdxSelectionItem): Boolean; virtual;
    function ValidateUIState(AState: IdxCommandUIState): Boolean; virtual;
  public
    procedure ForceExecute(const AState: IdxCommandUIState); override;
  end;

  { TdxSelectLineCommand }

  TdxSelectLineCommand = class(TdxPlaceCaretToPhysicalPointCommand)
  protected
    function ExtendSelection: Boolean; override;
    function HitTestOnlyInPageClientBounds: Boolean; override;
    function ChangeSelectionEnd(ASelection: TdxSelection; ALogPosition: Integer;
      AHitTestResult: TdxRichEditHitTestResult): Boolean; override;
    procedure ChangeSelectionStart(ASelection: TdxSelection;
      ALogPosition: Integer; AHitTestResult: TdxRichEditHitTestResult); override;
    procedure ChangeSelection(ASelection: TdxSelection; ALogPosition: Integer;
      AHitTestResult: TdxRichEditHitTestResult); override;
  end;

  { TdxSimpleSetSelectionCommand }

  TdxSimpleSetSelectionCommand = class(TdxRichEditMenuItemSimpleCommand)
  strict private
    FPos: PdxDocumentModelPosition;
  protected
    procedure ExecuteCore; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    property Position: PdxDocumentModelPosition read FPos write FPos;

  end;

implementation

uses
  SysUtils, Math, dxTypeHelpers,
  dxRichEdit.Utils.Characters,
  dxRichEdit.Utils.ChunkedStringBuilder,
  dxRichEdit.DocumentModel.FieldRange,
  dxRichEdit.DocumentModel.ParagraphRange,
  dxRichEdit.View.PageViewInfoGenerator,
  dxRichEdit.View.ViewInfo;

{ TdxEnhancedSelectionManager }

constructor TdxEnhancedSelectionManager.Create(APieceTable: TdxPieceTable);
begin
  inherited Create;
  FPieceTable := APieceTable;
  FMinDistance := DocumentModel.LayoutUnitConverter.TwipsToLayoutUnits(14);
  FMinBorderWidthToHitTest := DocumentModel.LayoutUnitConverter.PixelsToLayoutUnits(4, DocumentModel.DpiX);
end;

function TdxEnhancedSelectionManager.ExtendSelectionEndToParagraphMark(
  ASelection: TdxSelection;
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
var
  ASelectionEndParagraph: TdxParagraphIndex;
  AParagraph: TdxParagraph;
begin
  ASelectionEndParagraph := GetSelectionEndParagraphIndex(ASelection);
  AParagraph := PieceTable.Paragraphs[ASelectionEndParagraph];
  if AParagraph.LogPosition + AParagraph.Length - 1 = ALogPosition then
  begin
    if ASelection.NormalizedStart <= AParagraph.LogPosition then
    begin
      if AParagraph.Length > 1 then
      begin
        if ASelection.NormalizedEnd <= PieceTable.DocumentEndLogPosition then
          Exit(ALogPosition + 1);
      end;
    end;
  end;
  Result := ALogPosition;
end;

function TdxEnhancedSelectionManager.ExtendSelectionStartToParagraphMark(
  ASelection: TdxSelection;
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
var
  ASelectionEndParagraph: TdxParagraphIndex;
  AParagraph: TdxParagraph;
begin
  Result := ASelection.Start;
  ASelectionEndParagraph := GetSelectionEndParagraphIndex(ASelection);
  AParagraph := PieceTable.Paragraphs[ASelectionEndParagraph];
  if (ALogPosition <= AParagraph.LogPosition) and (ASelection.Start = AParagraph.LogPosition + AParagraph.Length - 1) then
  begin
    if AParagraph.Length > 1 then
      Result := ASelection.Start + 1;
  end;
end;

function TdxEnhancedSelectionManager.CalculateTableRowToResize(AHitTestResult: TdxRichEditHitTestResult): TdxTableRowViewInfoBase;
begin
  Result := nil;
  if AHitTestResult.Accuracy and ExactTableCell = 0 then
    Exit;
  Assert(False, 'not implemented');
end;

function TdxEnhancedSelectionManager.ShouldResizeTableRow(AControl: IdxRichEditControl;
  AHitTestResult: TdxRichEditHitTestResult; ATableRow: TdxTableRowViewInfoBase): Boolean;
begin
  Result := False;
  if (ATableRow = nil) or (AHitTestResult.PieceTable <> PieceTable) then
    Exit;
  Assert(False, 'not implemented');
end;

function TdxEnhancedSelectionManager.ShouldSelectEntireRow(AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  if (AHitTestResult.PieceTable <> PieceTable) or (AHitTestResult.Accuracy and ExactCharacter <> 0) then
    Result := False
  else
    Result := AHitTestResult.Row.Bounds.Left > AHitTestResult.LogicalPoint.X;
end;

function TdxEnhancedSelectionManager.ShouldSelectFloatingObject(AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  APoint: TPoint;
  AFloatingObjectBox: TdxFloatingObjectBox;
  ARun: TdxFloatingObjectAnchorRun;
begin
  if AHitTestResult.DetailsLevel < TdxDocumentLayoutDetailsLevel.Page then
    Exit(False);
  AFloatingObjectBox := AHitTestResult.FloatingObjectBox; 
  if AFloatingObjectBox = nil then
    Exit(False);
  ARun := AFloatingObjectBox.GetFloatingObjectRun;
  if (ARun = nil) or (AFloatingObjectBox.DocumentLayout = nil) then
    Exit(True);
  if not (ARun.Content is TdxTextBoxFloatingObjectContent) then
    Exit(True);
  APoint := AFloatingObjectBox.TransformPointBackward(AHitTestResult.LogicalPoint);
  Result := not AFloatingObjectBox.GetTextBoxContentBounds.Contains(APoint);
end;

function TdxEnhancedSelectionManager.ShouldSelectEntireTableCell(AHitTestResult: TdxRichEditHitTestResult): Boolean;
//  ACellViewInfo: TdxTableCellViewInfo;
//  ALeft, ALogicalX, ACellMargin: Integer;
begin
  Result := False;
  if (AHitTestResult.PieceTable <> PieceTable) or (AHitTestResult.Accuracy and ExactTableCell = 0) then
    Exit;
  Assert(False, 'not implemented');
end;

function TdxEnhancedSelectionManager.ShouldSelectEntireTableColumn(AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := False;
  if (AHitTestResult.PieceTable <> PieceTable) or (AHitTestResult.Accuracy and ExactTableCell = 0) then
    Exit;
  Assert(False, 'not implemented');


end;

function TdxEnhancedSelectionManager.ShouldSelectEntireTableRow(AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := False;
  if (AHitTestResult.PieceTable <> PieceTable) or (AHitTestResult.Accuracy and ExactTableRow = 0) then
    Exit;
  Assert(False, 'not implemented');
end;

function TdxEnhancedSelectionManager.ShouldSelectToTheEndOfRow(
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := AHitTestResult.PieceTable = PieceTable;
  Result := Result and ((AHitTestResult.Accuracy and ExactCharacter) = 0) and
    (AHitTestResult.Character.Bounds.Right < AHitTestResult.LogicalPoint.X);
end;

function TdxEnhancedSelectionManager.GetDocumentModel: TdxDocumentModel;
begin
  Result := PieceTable.DocumentModel;
end;

function TdxEnhancedSelectionManager.GetSelectionEndParagraphIndex(ASelection: TdxSelection): TdxParagraphIndex;
var
  ALastParagraphIndex: TdxParagraphIndex;
begin
  Result := ASelection.Interval.NormalizedEnd.ParagraphIndex;
  ALastParagraphIndex := ASelection.PieceTable.Paragraphs.Count - 1;
  if Result > ALastParagraphIndex then
    Result := ALastParagraphIndex;
end;

{ TdxExtendSelectionByRangesCommandBase }

function TdxExtendSelectionByRangesCommandBase.ExtendSelection: Boolean;
begin
  Result := True;
end;

function TdxExtendSelectionByRangesCommandBase.HitTestOnlyInPageClientBounds: Boolean;
begin
  Result := False;
end;

procedure TdxExtendSelectionByRangesCommandBase.SetInitialBox(
  const Value: TdxBox);
begin
  FInitialBox := Value;
  FInitialLogPosition := InitialBox.GetFirstPosition(ActivePieceTable).LogPosition;
end;

{ TdxRichEditSelectionCommand }

constructor TdxRichEditSelectionCommand.Create(const AControl: IdxRichEditControl);
begin
  inherited Create(AControl);
  ShouldEnsureCaretVisibleVerticallyBeforeUpdate := True;
  ShouldEnsureCaretVisibleVerticallyAfterUpdate := True;
end;

procedure TdxRichEditSelectionCommand.AfterUpdate;
begin
end;

function TdxRichEditSelectionCommand.ApplyNewPositionToSelectionEnd(ASelection: TdxSelection;
  ALogPosition: TdxDocumentLogPosition): Boolean;
begin
  ASelection.&End := ALogPosition;
  Result := False;
end;

procedure TdxRichEditSelectionCommand.BeforeUpdate;
begin
end;

function TdxRichEditSelectionCommand.CalculateSelectionCurrentPosition(
  ASelection: TdxSelection): TdxDocumentModelPosition;
var
  AStart, AEnd, ATemp: TdxDocumentModelPosition;
begin
  if ExtendSelection then
    Result := ASelection.Interval.&End.Clone
  else
  begin
    AStart := ASelection.Interval.Start;
    AEnd := ASelection.Interval.&End;
    if AStart.LogPosition > AEnd.LogPosition then
    begin
      ATemp := AStart;
      AStart := AEnd;
      AEnd := ATemp;
    end;
    if TreatStartPositionAsCurrent then
      Result := AStart.Clone
    else
      Result := AEnd.Clone;
  end;
end;

procedure TdxRichEditSelectionCommand.ChangeSelection(ASelection: TdxSelection);
var
  APos: TdxDocumentModelPosition;
  ALogPosition: TdxDocumentLogPosition;
  AUsePreviousBoxBounds: Boolean;
  ANewLogPosition: TdxDocumentLogPosition;
  AOldSelectionLength: Integer;
begin
  APos := CalculateSelectionCurrentPosition(ASelection);
  ALogPosition := ChangePosition(APos);
  AUsePreviousBoxBounds := False;
  if not ExtendSelection then
    ALogPosition := Min(ALogPosition, ActivePieceTable.DocumentEndLogPosition)
  else
  begin
    ALogPosition := Min(ALogPosition, ActivePieceTable.DocumentEndLogPosition + 1);
    ANewLogPosition := ExtendSelectionEndToParagraphMark(ASelection, ALogPosition);
    if ANewLogPosition <> ALogPosition then
      AUsePreviousBoxBounds := True;
  end;
  AOldSelectionLength := ASelection.Length;
  AUsePreviousBoxBounds := ApplyNewPositionToSelectionEnd(ASelection, ALogPosition) or AUsePreviousBoxBounds;
  ChangeSelectionStart(ASelection, ALogPosition);
  if not ExtendSelection then
    ASelection.&End := ASelection.Start;
  ASelection.UsePreviousBoxBounds := AUsePreviousBoxBounds;
  if not ExtendSelection then
    ASelection.ClearMultiSelection;
  UpdateTableSelectionAfterSelectionUpdated(ALogPosition);
  ValidateSelection(ASelection, ASelection.Length > AOldSelectionLength);
end;

procedure TdxRichEditSelectionCommand.ChangeSelectionStart(ASelection: TdxSelection;
  ALogPosition: TdxDocumentLogPosition);
begin
  if not ExtendSelection then
    ASelection.Start := ASelection.VirtualEnd
  else
    ASelection.Start := ExtendSelectionStartToParagraphMark(ASelection, ALogPosition);
end;

function TdxRichEditSelectionCommand.CreateEnsureCaretVisibleVerticallyCommand: TdxRichEditCommand;
begin
  Result := TdxEnsureCaretVisibleVerticallyCommand.Create(RichEditControl);
end;

procedure TdxRichEditSelectionCommand.EnsureCaretVisible;
begin
  if not DocumentModel.IsUpdateLocked then
    ActiveView.EnsureCaretVisible
  else
    DocumentModel.DeferredChanges.EnsureCaretVisible := True;
end;

procedure TdxRichEditSelectionCommand.EnsureCaretVisibleVertically;
var
  ACommand: TdxRichEditCommand;
begin
  if DocumentModel.IsUpdateLocked then
    Exit;
  ACommand := CreateEnsureCaretVisibleVerticallyCommand;
  try
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
  UpdateCaretPosition;
  if not TryToKeepCaretX then
  begin
    ACommand := TdxEnsureCaretVisibleHorizontallyCommand.Create(RichEditControl);
    try
      ACommand.Execute;
    finally
      ACommand.Free;
    end;
  end;
end;

procedure TdxRichEditSelectionCommand.ExecuteCore;
var
  AShouldRedrawControl: Boolean;
begin
  CheckExecutedAtUIThread;
  RichEditControl.BeginUpdate;
  try
    UpdateCaretPosition;
    if ShouldEnsureCaretVisibleVerticallyBeforeUpdate then
      EnsureCaretVisibleVertically;
    BeforeUpdate;
    DocumentModel.BeginUpdate;
    try
      PerformModifyModel;
      AShouldRedrawControl := PerformChangeSelection;
    finally
      DocumentModel.EndUpdate;
    end;
    AfterUpdate;
    if ShouldEnsureCaretVisibleVerticallyAfterUpdate then
      EnsureCaretVisibleVertically;
      if not TryToKeepCaretX then
      begin
        if CaretPosition.LayoutPosition.Character <> nil then
          CaretPosition.X := CaretPosition.CalculateCaretBounds.Left;
      end;
      if ShouldUpdateCaretY and (CaretPosition.LayoutPosition.TableCell <> nil) then
      begin
        Assert(False);
      end;
      if AShouldRedrawControl then
      begin
        UpdateCaretPosition(TdxDocumentLayoutDetailsLevel.Character);
        RichEditControl.RedrawEnsureSecondaryFormattingComplete(TdxRefreshAction.Selection);
      end;
  finally
    RichEditControl.EndUpdate;
  end;
end;

function TdxRichEditSelectionCommand.ExtendSelectionEndToParagraphMark(ASelection: TdxSelection;
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  ASelectionManager := TdxEnhancedSelectionManager.Create(ActivePieceTable);
  try
    Result := ASelectionManager.ExtendSelectionEndToParagraphMark(ASelection, ALogPosition);
  finally
    ASelectionManager.Free;
  end;
end;

function TdxRichEditSelectionCommand.ExtendSelectionStartToParagraphMark(ASelection: TdxSelection;
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  ASelectionManager := TdxEnhancedSelectionManager.Create(ActivePieceTable);
  try
    Result := ASelectionManager.ExtendSelectionStartToParagraphMark(ASelection, ALogPosition);
  finally
    ASelectionManager.Free;
  end;
end;

function TdxRichEditSelectionCommand.GetLeftVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
var
  ATextFilter: IVisibleTextFilter;
begin
  ATextFilter := ActivePieceTable.NavigationVisibleTextFilter;
  Result := ATextFilter.GetVisibleLogPosition(APos, True);
  if Result < ActivePieceTable.DocumentStartLogPosition then
    Result := APos.LogPosition;
end;

function TdxRichEditSelectionCommand.GetSelectionEndParagraphIndex: TdxParagraphIndex;
var
  ASelection: TdxSelectionRangeCollection;
  AEndParagraphIndex: TdxParagraphIndex;
begin
  ASelection := DocumentModel.Selection.GetSortedSelectionCollection;
  try
    AEndParagraphIndex := ActivePieceTable.FindParagraphIndex(ASelection.Last.&End);
    Result := AEndParagraphIndex;
  finally
    ASelection.Free;
  end;
end;

function TdxRichEditSelectionCommand.GetSelectionEndTableCell: TdxTableCell;
begin
  Result := ActivePieceTable.Paragraphs[GetSelectionEndParagraphIndex].GetCell;
end;

function TdxRichEditSelectionCommand.GetShouldUpdateCaretY: Boolean;
begin
  Result := True;
end;

function TdxRichEditSelectionCommand.IsSelectionEndAfterTableCell: Boolean;
var
  AIndex: TdxParagraphIndex;
begin
  AIndex := GetSelectionEndParagraphIndex;
  if AIndex = 0 then 
    Exit(False);
  Result := ActivePieceTable.Paragraphs[AIndex - 1].IsInCell and not ActivePieceTable.Paragraphs[AIndex].IsInCell;
end;

function TdxRichEditSelectionCommand.IsSelectionEndInTableCell: Boolean;
var
  AIndex: TdxParagraphIndex;
begin
  AIndex := GetSelectionEndParagraphIndex;
  Result := ActivePieceTable.Paragraphs[AIndex].IsInCell;
end;

function TdxRichEditSelectionCommand.PerformChangeSelection: Boolean;
var
  ASelection: TdxSelection;
  AInitialSelectionLength, ACurrentSelectionLength: Integer;
begin
  ASelection := DocumentModel.Selection;
  AInitialSelectionLength := ASelection.Length;
  DocumentModel.BeginUpdate;
  try
    ChangeSelection(ASelection);
  finally
    DocumentModel.EndUpdate;
  end;
  ACurrentSelectionLength := ASelection.Length;
  Result := (ACurrentSelectionLength <> 0) or (AInitialSelectionLength <> 0);
end;

procedure TdxRichEditSelectionCommand.PerformModifyModel;
begin
end;

procedure TdxRichEditSelectionCommand.UpdateCaretPosition;
begin
  UpdateCaretPosition(UpdateCaretPositionBeforeChangeSelectionDetailsLevel);
end;

procedure TdxRichEditSelectionCommand.UpdateTableSelectionAfterSelectionUpdated(ALogPosition: TdxDocumentLogPosition);
begin
  DocumentModel.Selection.SetStartCell(ALogPosition);
end;

procedure TdxRichEditSelectionCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
var
  APos: TdxDocumentModelPosition;
begin
  CheckExecutedAtUIThread;
  APos := CalculateSelectionCurrentPosition(DocumentModel.Selection);
  AState.Enabled := CanChangePosition(APos);
  AState.Visible := True;
  AState.Checked := false;
end;

procedure TdxRichEditSelectionCommand.ValidateSelection(ASelection: TdxSelection; AIsSelectionExtended: Boolean);
var
  AValidator: TdxSelectionValidator;
begin
  if AIsSelectionExtended then
    AValidator := TdxFieldIsSelectValidator.Create(ActivePieceTable)
  else
    AValidator := TdxFieldIsUnselectValidator.Create(ActivePieceTable);
  try
    AValidator.ValidateSelection(ASelection);
  finally
    AValidator.Free;
  end;
end;

{ TdxSelectFieldNextPrevToCaretCommand }

procedure TdxSelectFieldNextPrevToCaretCommand.ChangeSelection(
  ASelection: TdxSelection);
begin
  Assert(False);
end;

function TdxSelectFieldNextPrevToCaretCommand.GetDeletedPosition(
  ASelection: TdxSelection): TdxDocumentModelPosition;
begin
  Result := ASelection.Interval.NormalizedEnd;
end;

function TdxSelectFieldNextPrevToCaretCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

function TdxSelectFieldNextPrevToCaretCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxSelectFieldNextPrevToCaretCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxSelectFieldNextPrevToCaretCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

function TdxSelectFieldNextPrevToCaretCommand.IsFieldRun(
  ARun: TdxTextRunBase): Boolean;
begin
  Result := (ARun is TdxFieldCodeRunBase) or (ARun is TdxFieldResultEndRun);
end;

procedure TdxSelectFieldNextPrevToCaretCommand.UpdateUIState(const AState: IdxCommandUIState);
var
  ARunIndex: TdxRunIndex;
begin
  ARunIndex := GetDeletedPosition(DocumentModel.Selection).RunIndex;
  AState.Enabled := (DocumentModel.Selection.Length = 0) and IsFieldRun(ActivePieceTable.Runs[ARunIndex]);
end;

{ TdxSelectFieldPrevToCaretCommand }

function TdxSelectFieldPrevToCaretCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxSelectFieldPrevToCaretCommand.ChangePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
begin
  Result := APos.LogPosition;
end;

function TdxSelectFieldPrevToCaretCommand.GetDeletedPosition(ASelection: TdxSelection): TdxDocumentModelPosition;
begin
  if ASelection.NormalizedStart > ActivePieceTable.DocumentStartLogPosition then
    Result := TdxDocumentModelPosition.MoveBackward(ASelection.Interval.NormalizedStart)
  else
    Result := TdxDocumentModelPosition.Create(ActivePieceTable);
end;

{ TdxChangeActivePieceTableCommand }

procedure TdxChangeActivePieceTableCommand.ActivatePieceTable(
  ANewPieceTable: TdxPieceTable; ASection: TdxSection);
var
  APos: TdxDocumentModelPosition;
begin
  DocumentModel.SetActivePieceTable(ANewPieceTable, ASection);
  if ANewPieceTable.IsMain then
  begin
    if CaretPosition.LayoutPosition.IsValid(TdxDocumentLayoutDetailsLevel.Page) then
    begin
      APos := CaretPosition.LayoutPosition.Page.GetFirstPosition(DocumentModel.MainPieceTable);
      SetSelection(APos.LogPosition, 0);
    end;
  end
  else
    DocumentModel.Selection.SetStartCell(DocumentModel.Selection.Start);
end;

procedure TdxChangeActivePieceTableCommand.SetSelection(AStart: TdxDocumentLogPosition; ALength: Integer);
begin
  DocumentModel.BeginUpdate;
  try
    DocumentModel.Selection.Start := AStart;
    DocumentModel.Selection.&End := AStart + ALength;
    DocumentModel.Selection.SetStartCell(DocumentModel.Selection.Start);
  finally
    DocumentModel.EndUpdate;
  end;
end;

procedure TdxChangeActivePieceTableCommand.AfterUpdate;
begin
  inherited AfterUpdate;
  ApplyLayoutPreferredPageIndex(FPreferredPageIndex);
end;

function TdxChangeActivePieceTableCommand.CanChangePosition(
  const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxChangeActivePieceTableCommand.ChangePosition(
  const APos: TdxDocumentModelPosition): Integer;
begin
  Result := APos.LogPosition;
end;

constructor TdxChangeActivePieceTableCommand.Create(AControl: IdxRichEditControl; ANewActivePieceTable: TdxPieceTable;
  ASection: TdxSection; APreferredPageIndex: Integer);
begin
  inherited Create(AControl);
  FNewActivePieceTable := ANewActivePieceTable;
  Assert(ANewActivePieceTable.DocumentModel = DocumentModel);
  if not ANewActivePieceTable.IsMain then
    FSection := ASection;
  FPreferredPageIndex := APreferredPageIndex;
end;

function TdxChangeActivePieceTableCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxChangeActivePieceTableCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxChangeActivePieceTableCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxChangeActivePieceTableCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.PageArea;
end;

function TdxChangeActivePieceTableCommand.PerformChangeSelection: Boolean;
begin
  ActivatePieceTable(FNewActivePieceTable, FSection);
  Result := True;
end;

procedure TdxChangeActivePieceTableCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  AState.Visible := True;
  AState.Checked := True;
  if FNewActivePieceTable.IsHeaderFooter then
  begin
Assert(False, 'not implenmented');
  end
  else
    if FNewActivePieceTable.IsTextBox then
    begin
      AState.Enabled := True;
      ApplyCommandRestrictionOnEditableControl(AState,
        InnerControl.DocumentModel.DocumentCapabilities.FloatingObjects, 
        AState.Enabled);
    end;
end;

{ TdxPreviousCharacterCommand }

function TdxPreviousCharacterCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxPreviousCharacterCommand.ChangePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
begin
  if not ExtendSelection and (DocumentModel.Selection.Length > 0) then
  begin
    if not DocumentModel.Selection.IsSelectFieldPictureResult then
      Exit(APos.LogPosition);
  end;
  if APos.LogPosition > ActivePieceTable.DocumentStartLogPosition then
    Result := GetPrevVisibleLogPosition(APos)
  else
    Result := APos.LogPosition;
end;

function TdxPreviousCharacterCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxPreviousCharacterCommand.GetPrevVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
var
  ATextFilter: IVisibleTextFilter;
begin
  ATextFilter := ActivePieceTable.NavigationVisibleTextFilter;
  Result := ATextFilter.GetPrevVisibleLogPosition(APos, True);
  if Result < ActivePieceTable.DocumentStartLogPosition then
    Result := APos.LogPosition;
end;

function TdxPreviousCharacterCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

function TdxPreviousCharacterCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxPreviousCharacterCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

class function TdxPreviousCharacterCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.PreviousCharacter;
end;

{ TdxPrevNextWordCommand }

function TdxPrevNextWordCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxPrevNextWordCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxPrevNextWordCommand.GetModelPosition(const APos: TdxDocumentLogPosition): TdxDocumentModelPosition;
begin
Assert(False);
end;

function TdxPrevNextWordCommand.GetNextVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
begin
  Result := VisibleTextFilter.GetNextVisibleLogPosition(APos, True);
  if Result < ActivePieceTable.DocumentEndLogPosition then
    Result := Result - 1;
end;

function TdxPrevNextWordCommand.GetPrevVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
begin
  Result := VisibleTextFilter.GetPrevVisibleLogPosition(APos, True);
  if Result > ActivePieceTable.DocumentStartLogPosition then
    Result := Result + 1;
end;

function TdxPrevNextWordCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

function TdxPrevNextWordCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxPrevNextWordCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

function TdxPrevNextWordCommand.GetVisibleTextFilter: IVisibleTextFilter;
begin
  Result := ActivePieceTable.NavigationVisibleTextFilter;
end;

{ TdxPreviousWordCommand }

function TdxPreviousWordCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  AIterator: TdxPieceTableIterator;
begin
  AIterator := TdxVisibleWordsIterator.Create(ActivePieceTable);
  try
    if not ExtendSelection and (DocumentModel.Selection.Length > 0) then
      Result := APos.LogPosition
    else
      Result := AIterator.MoveBack(APos).LogPosition;
  finally
    AIterator.Free;
  end;
end;

class function TdxPreviousWordCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.PreviousWord;
end;

{ TdxNextCharacterCommand }

function TdxNextCharacterCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxNextCharacterCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
begin
  if not ExtendSelection and (DocumentModel.Selection.Length > 0) then
  begin
    if not DocumentModel.Selection.IsSelectFieldPictureResult then
      Exit(APos.LogPosition);
  end;
  if APos.LogPosition < GetMaxPosition then
    Result := GetNextVisibleLogPosition(APos)
  else
    Result := APos.LogPosition;
end;

function TdxNextCharacterCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxNextCharacterCommand.GetMaxPosition: TdxDocumentLogPosition;
begin
  Result := ActivePieceTable.DocumentEndLogPosition;
end;

function TdxNextCharacterCommand.GetNextVisibleLogPosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
var
  ATextFilter: IVisibleTextFilter;
begin
  ATextFilter := ActivePieceTable.NavigationVisibleTextFilter;
  Result := ATextFilter.GetNextVisibleLogPosition(APos, True);
end;

function TdxNextCharacterCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxNextCharacterCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxNextCharacterCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

class function TdxNextCharacterCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.NextCharacter;
end;

{ TdxNextWordCommand }

function TdxNextWordCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  AIterator: TdxPieceTableIterator;
begin
  AIterator := TdxVisibleWordsIterator.Create(ActivePieceTable);
  try
    if not ExtendSelection and (DocumentModel.Selection.Length > 0) then
      Exit(APos.LogPosition);
    Result := AIterator.MoveForward(APos).LogPosition;
  finally
    AIterator.Free;
  end;
end;

class function TdxNextWordCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.NextWord;
end;

{ TdxLineUpDownCommandBase }

function TdxLineUpDownCommandBase.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxLineUpDownCommandBase.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  ACalculator: TdxNextCaretPositionVerticalDirectionCalculator;
  ANewPosition: TdxDocumentModelPosition;
begin
  ACalculator := CreateNextCaretPositionCalculator;
  try
    ANewPosition := ACalculator.CalculateNextPosition(CaretPosition);
    if not ANewPosition.IsValid then
      Result := APos.LogPosition
    else
      Result := GetLeftVisibleLogPosition(ANewPosition);
  finally
    ACalculator.Free;
  end;
end;

function TdxLineUpDownCommandBase.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.Row;
end;

{ TdxPreviousLineCommand }

function TdxPreviousLineCommand.CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator;
begin
  Result := TdxNextCaretPositionLineUpCalculator.Create(RichEditControl)
end;

function TdxPreviousLineCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxPreviousLineCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

function TdxPreviousLineCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := True;
end;

class function TdxPreviousLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.PreviousLine;
end;

{ TdxNextLineCommand }

function TdxNextLineCommand.CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator;
begin
  Result := TdxNextCaretPositionLineDownCalculator.Create(RichEditControl);
end;

function TdxNextLineCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxNextLineCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxNextLineCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := True;
end;

class function TdxNextLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.NextLine;
end;

{ TdxExtendPreviousCharacterCommand }

function TdxExtendPreviousCharacterCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  ACalculator: TdxExtendKeybordSelectionHorizontalDirectionCalculator;
begin
  Result := inherited ChangePosition(APos);
  ACalculator := TdxExtendKeybordSelectionHorizontalDirectionCalculator.Create(DocumentModel);
  try
    Result := ACalculator.CalculatePrevPosition(Result);
  finally
    ACalculator.Free;
  end;
end;

function TdxExtendPreviousCharacterCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendPreviousCharacterCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendPreviousCharacter;
end;

procedure TdxExtendPreviousCharacterCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
begin
  DocumentModel.Selection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxExtendNextCharacterCommand }

function TdxExtendNextCharacterCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  ACalculator: TdxExtendKeybordSelectionHorizontalDirectionCalculator;
begin
  Result := inherited ChangePosition(APos);
  ACalculator := TdxExtendKeybordSelectionHorizontalDirectionCalculator.Create(DocumentModel);
  try
    Result := ACalculator.CalculateNextPosition(Result);
  finally
    ACalculator.Free;
  end;
end;

function TdxExtendNextCharacterCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

function TdxExtendNextCharacterCommand.GetMaxPosition: Integer;
begin
  Result := ActivePieceTable.DocumentEndLogPosition + 1;
end;

class function TdxExtendNextCharacterCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendNextCharacter;
end;

procedure TdxExtendNextCharacterCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
begin
  DocumentModel.Selection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxExtendPreviousWordCommand }

function TdxExtendPreviousWordCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  ACalculator: TdxExtendKeybordSelectionHorizontalDirectionCalculator;
begin
  Result := inherited ChangePosition(APos);
  ACalculator := TdxExtendKeybordSelectionHorizontalDirectionCalculator.Create(DocumentModel);
  try
    Result := ACalculator.CalculatePrevPosition(Result);
  finally
    ACalculator.Free;
  end;
end;

function TdxExtendPreviousWordCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendPreviousWordCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendPreviousWord;
end;

procedure TdxExtendPreviousWordCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
begin
  DocumentModel.Selection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxExtendNextWordCommand }

function TdxExtendNextWordCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  ACalculator: TdxExtendKeybordSelectionHorizontalDirectionCalculator;
begin
  Result := inherited ChangePosition(APos);
  ACalculator := TdxExtendKeybordSelectionHorizontalDirectionCalculator.Create(DocumentModel);
  try
    Result := ACalculator.CalculateNextPosition(Result);
  finally
    ACalculator.Free;
  end;
end;

function TdxExtendNextWordCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendNextWordCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendNextWord;;
end;

procedure TdxExtendNextWordCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
begin
  DocumentModel.Selection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxStartOfLineCommand }

function TdxStartOfLineCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxStartOfLineCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  APosition: TdxDocumentModelPosition;
begin
  APosition := CaretPosition.LayoutPosition.Row.GetFirstPosition(ActivePieceTable);
  Result := GetLeftVisibleLogPosition(APosition);
end;

function TdxStartOfLineCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxStartOfLineCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

function TdxStartOfLineCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxStartOfLineCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.Row;
end;

class function TdxStartOfLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.StartOfLine;
end;

{ TdxEndOfLineCommand }

function TdxEndOfLineCommand.ApplyNewPositionToSelectionEnd(ASelection: TdxSelection; ALogPosition: Integer): Boolean;
var
  ASelectionIntervalEnd: TdxDocumentModelPosition;
  ARun: TdxTextRunBase;
  AChar: Char;
begin
  inherited ApplyNewPositionToSelectionEnd(ASelection, ALogPosition);
  if ExtendSelection then
  begin
    if ASelection.&End < ActivePieceTable.DocumentEndLogPosition then
    begin
      ASelection.&End := ASelection.&End + 1;
      Result := True;
    end
    else
      Result := False;
  end
  else
  begin
    ASelectionIntervalEnd := ASelection.Interval.&End;
    ARun := ActivePieceTable.Runs[ASelectionIntervalEnd.RunIndex];
    if ARun is TdxParagraphRun then
      Result := False
    else
    begin
      AChar := ActivePieceTable.TextBuffer[ARun.StartIndex + ASelectionIntervalEnd.RunOffset];
      if AChar = TdxCharacters.LineBreak then
        Result := False
      else
      begin
        ASelection.&End := ASelection.&End + 1;
        Result := True;
      end;
    end;
  end;
end;

function TdxEndOfLineCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxEndOfLineCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
begin
  Result := CaretPosition.LayoutPosition.Row.GetLastPosition(ActivePieceTable).LogPosition;
end;

function TdxEndOfLineCommand.ExtendSelectionEndToParagraphMark(
  ASelection: TdxSelection; ALogPosition: Integer): Integer;
begin
  Result := ALogPosition;
end;

function TdxEndOfLineCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxEndOfLineCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxEndOfLineCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxEndOfLineCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.Row;
end;

class function TdxEndOfLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.EndOfLine;
end;

{ TdxExtendStartOfLineCommand }

function TdxExtendStartOfLineCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendStartOfLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendStartOfLine;
end;

{ TdxExtendEndOfLineCommand }

function TdxExtendEndOfLineCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendEndOfLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendEndOfLine;
end;

{ TdxExtendPreviousLineCommand }

function TdxExtendPreviousLineCommand.CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator;
begin
  Result := TdxExtendNextCaretPositionLineUpCalculator.Create(RichEditControl)
end;

function TdxExtendPreviousLineCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendPreviousLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendPreviousLine;
end;

procedure TdxExtendPreviousLineCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
var
  ASelection: TdxSelection;
begin
  ASelection := DocumentModel.Selection;
  ASelection.UpdateTableSelectionStart(ALogPosition);
  ASelection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxExtendNextLineCommand }

function TdxExtendNextLineCommand.CreateNextCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator;
begin
  Result := TdxExtendNextCaretPositionLineDownCalculator.Create(RichEditControl);
end;

function TdxExtendNextLineCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendNextLineCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendNextLine;
end;

procedure TdxExtendNextLineCommand.UpdateTableSelectionAfterSelectionUpdated(ALogPosition: Integer);
var
  ASelection: TdxSelection;
begin
  ASelection := DocumentModel.Selection;
  ASelection.UpdateTableSelectionEnd(ALogPosition);
  ASelection.UpdateTableSelectionStart(ALogPosition);
end;

{ TdxPrevNextParagraphCommandBase }

function TdxPrevNextParagraphCommandBase.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxPrevNextParagraphCommandBase.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxPrevNextParagraphCommandBase.GetNextVisiblePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
begin
  Result := VisibleTextFilter.GetNextVisibleLogPosition(APos, False);
  if Result < ActivePieceTable.DocumentEndLogPosition then
    Result := Result - 1;
end;

function TdxPrevNextParagraphCommandBase.GetPrevVisiblePosition(const APos: TdxDocumentModelPosition): TdxDocumentLogPosition;
begin
  Result := VisibleTextFilter.GetPrevVisibleLogPosition(APos, False);
  if Result > ActivePieceTable.DocumentStartLogPosition then
    Result := Result + 1;
end;

function TdxPrevNextParagraphCommandBase.GetNextVisiblePosition(ARunIndex: TdxRunIndex): TdxDocumentModelPosition;
var
  AIndex: TdxRunIndex;
begin
  AIndex := VisibleTextFilter.GetNextVisibleRunIndex(ARunIndex);
  Result := TdxDocumentModelPosition.FromRunStart(ActivePieceTable, AIndex);
end;

function TdxPrevNextParagraphCommandBase.GetPrevVisiblePosition(ARunIndex: TdxRunIndex): TdxDocumentModelPosition;
var
  AIndex: TdxRunIndex;
begin
  AIndex := VisibleTextFilter.GetPrevVisibleRunIndex(ARunIndex);
  Result := TdxDocumentModelPosition.FromRunStart(ActivePieceTable, AIndex);
end;

function TdxPrevNextParagraphCommandBase.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxPrevNextParagraphCommandBase.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxPrevNextParagraphCommandBase.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

function TdxPrevNextParagraphCommandBase.GetValidLogPosition(const ANewPos: TdxDocumentModelPosition;
  AParagraph: TdxParagraph): TdxDocumentLogPosition;
begin
  Result := ANewPos.LogPosition;
end;

function TdxPrevNextParagraphCommandBase.GetVisibleLogPosition(AParagraph: TdxParagraph): TdxDocumentLogPosition;
var
  ARunIndex: TdxRunIndex;
begin
  ARunIndex := AParagraph.FirstRunIndex;
  if VisibleTextFilter.IsRunVisible(ARunIndex) then
    Result := AParagraph.LogPosition
  else
    Result := GetValidLogPosition(GetNextVisiblePosition(ARunIndex), AParagraph);
end;

function TdxPrevNextParagraphCommandBase.VisibleTextFilter: IVisibleTextFilter;
begin
  Result := ActivePieceTable.NavigationVisibleTextFilter;
end;

{ TdxPreviousParagraphCommand }

function TdxPreviousParagraphCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  AParagraphIndex: TdxParagraphIndex;
  AParagraph: TdxParagraph;
begin
  AParagraphIndex := APos.ParagraphIndex;
  AParagraph := ActivePieceTable.Paragraphs[AParagraphIndex];
  if APos.LogPosition = AParagraph.LogPosition then
    Result := GetPrevPosition(AParagraphIndex)
  else
  begin
    Result := GetVisibleLogPosition(AParagraph);
    if Result = APos.LogPosition then
      Result := GetPrevPosition(AParagraphIndex);
  end;
end;

function TdxPreviousParagraphCommand.GetMinPosition: TdxDocumentLogPosition;
var
  APos: TdxDocumentModelPosition;
begin
  APos := TdxDocumentModelPosition.FromParagraphStart(ActivePieceTable, 0);
  if VisibleTextFilter.IsRunVisible(APos.RunIndex) then
    Result := APos.LogPosition
  else
    Result := GetNextVisiblePosition(APos);
end;

function TdxPreviousParagraphCommand.GetPrevPosition(AParagraphIndex: TdxParagraphIndex): TdxDocumentLogPosition;
var
  APrevParagraphIndex: TdxParagraphIndex;
begin
  APrevParagraphIndex := AParagraphIndex - 1;
  if APrevParagraphIndex >= 0 then
    Result := GetVisibleLogPosition(ActivePieceTable.Paragraphs[APrevParagraphIndex])
  else
    Result := GetMinPosition;
end;

function TdxPreviousParagraphCommand.GetValidLogPosition(
  const ANewPos: TdxDocumentModelPosition; AParagraph: TdxParagraph): Integer;
begin
  if ANewPos.ParagraphIndex <= AParagraph.Index then
    Result := ANewPos.LogPosition
  else
    Result := GetPrevVisiblePosition(AParagraph.FirstRunIndex).LogPosition;
end;

class function TdxPreviousParagraphCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.PreviousParagraph;
end;

{ TdxExtendPreviousParagraphCommand }

function TdxExtendPreviousParagraphCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  APosition: TdxDocumentLogPosition;
  ACalculator: TdxExtendKeybordSelectionHorizontalDirectionCalculator;
begin
  APosition := inherited ChangePosition(APos);
  ACalculator := TdxExtendKeybordSelectionHorizontalDirectionCalculator.Create(DocumentModel);
  try
    Result := ACalculator.CalculatePrevPosition(APosition);
  finally
    ACalculator.Free;
  end;
end;

function TdxExtendPreviousParagraphCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendPreviousParagraphCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendPreviousParagraph;
end;

procedure TdxExtendPreviousParagraphCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
begin
  DocumentModel.Selection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxNextParagraphCommand }

function TdxNextParagraphCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  AParagraphIndex: TdxParagraphIndex;
  AParagraphs: TdxParagraphCollection;
begin
  AParagraphIndex := APos.ParagraphIndex + 1;
  AParagraphs := ActivePieceTable.Paragraphs;
  if AParagraphIndex < AParagraphs.Count then
    Result := GetVisibleLogPosition(AParagraphs[AParagraphIndex])
  else
    Result := GetMaxPosition;
end;

function TdxNextParagraphCommand.GetMaxPosition: TdxDocumentLogPosition;
var
  APos: TdxDocumentModelPosition;
begin
  APos := TdxDocumentModelPosition.FromParagraphEnd(ActivePieceTable, ActivePieceTable.Paragraphs.Count - 1);
  if VisibleTextFilter.IsRunVisible(APos.RunIndex) then
    Result := APos.LogPosition
  else
    Result := GetPrevVisiblePosition(APos);
end;

class function TdxNextParagraphCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.NextParagraph;
end;

{ TdxExtendNextParagraphCommand }

function TdxExtendNextParagraphCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  APosition: TdxDocumentLogPosition;
  ACalculator: TdxExtendKeybordSelectionHorizontalDirectionCalculator;
begin
  APosition := inherited ChangePosition(APos);
  ACalculator := TdxExtendKeybordSelectionHorizontalDirectionCalculator.Create(DocumentModel);
  try
    Result := ACalculator.CalculateNextPosition(APosition);
  finally
    ACalculator.Free;
  end;
end;

function TdxExtendNextParagraphCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

function TdxExtendNextParagraphCommand.GetMaxPosition: Integer;
begin
  Result := ActivePieceTable.DocumentEndLogPosition + 1;
end;

class function TdxExtendNextParagraphCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendNextParagraph;
end;

procedure TdxExtendNextParagraphCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
begin
  DocumentModel.Selection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxPrevNextPageCommandBase }

function TdxPrevNextPageCommandBase.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxPrevNextPageCommandBase.CreateEnsureCaretVisibleVerticallyCommand: TdxRichEditCommand;
var
  AResult: TdxEnsureCaretVisibleVerticallyForMovePrevNextPageCommand;
begin
  AResult := TdxEnsureCaretVisibleVerticallyForMovePrevNextPageCommand.Create(RichEditControl);
  AResult.PhysicalOffsetAboveTargetRow := Round(DocumentModel.LayoutUnitConverter.PointsToFontUnitsF(72) / ActiveView.ZoomFactor);
  Result := AResult;
end;

function TdxPrevNextPageCommandBase.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxPrevNextPageCommandBase.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.Page;
end;

{ TdxPreviousPageCommand }

function TdxPreviousPageCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  APages: TdxPageCollection;
  APageIndex: Integer;
begin
  APages := ActiveView.FormattingController.PageController.Pages;
  APageIndex := APages.IndexOf(CaretPosition.LayoutPosition.Page);
  Assert(APageIndex >= 0);
  if APageIndex > 0 then
    Dec(APageIndex);
  Result := GetLeftVisibleLogPosition(APages[APageIndex].GetFirstPosition(ActivePieceTable));
end;

function TdxPreviousPageCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxPreviousPageCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

class function TdxPreviousPageCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.PreviousPage;
end;

{ TdxExtendPreviousPageCommand }

function TdxExtendPreviousPageCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendPreviousPageCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendPreviousPage;
end;

{ TdxNextPageCommand }

function TdxNextPageCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
var
  APages: TdxPageCollection;
  APageIndex: Integer;
begin
  APages := ActiveView.FormattingController.PageController.Pages;
  APageIndex := APages.IndexOf(CaretPosition.LayoutPosition.Page);
  Assert(APageIndex >= 0);
  if APageIndex + 1 < APages.Count then
  begin
    Inc(APageIndex);
    Result := APages[APageIndex].GetFirstPosition(ActivePieceTable).LogPosition;
  end
  else
    Result := GetLastPosition(APageIndex);
end;

function TdxNextPageCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxNextPageCommand.GetLastPosition(APageIndex: Integer): TdxDocumentLogPosition;
var
  APages: TdxPageCollection;
begin
  APages := ActiveView.FormattingController.PageController.Pages;
  Result := APages[APageIndex].GetFirstPosition(ActivePieceTable).LogPosition;
end;

function TdxNextPageCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

class function TdxNextPageCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.NextPage;
end;

{ TdxExtendNextPageCommand }

function TdxExtendNextPageCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

function TdxExtendNextPageCommand.GetLastPosition(APageIndex: Integer): Integer;
begin
  Result := ActivePieceTable.DocumentEndLogPosition;
end;

class function TdxExtendNextPageCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendNextPage;
end;

{ TdxPrevNextScreenCommandBase }

function TdxPrevNextScreenCommandBase.CalculateCaretNewLogPosition(const APhysicalPoint: TPoint): TdxDocumentLogPosition;
var
  APageViewInfoRow: TdxPageViewInfoRow;
  APageViewInfo: TdxPageViewInfo;
  X: Integer;
  ACalculator: TdxNextCaretPositionVerticalDirectionCalculator;
  ALogicalCaretPoint: TPoint;
  AColumn: TdxColumn;
  ATargetRow: TdxRow;
  ACharacter: TdxCharacterBox;
begin
  APageViewInfoRow := ActiveView.PageViewInfoGenerator.GetPageRowAtPoint(APhysicalPoint, True);
  if APageViewInfoRow = nil then
    Result := MaxInt
  else
  begin
    APageViewInfo := APageViewInfoRow.GetPageAtPoint(APhysicalPoint, False);
    if APageViewInfo = nil then
      Result := MaxInt
    else
    begin
      X := ActiveView.CreateLogicalPoint(APageViewInfo.ClientBounds, APhysicalPoint).X;
      ACalculator := CreateCaretPositionCalculator;
      try
        ALogicalCaretPoint := ActiveView.CreateLogicalPoint(APageViewInfo.ClientBounds, APhysicalPoint);
			  AColumn := ACalculator.GetTargetColumn(APageViewInfo.Page, ALogicalCaretPoint.X);
        if ShouldUseAnotherPage(AColumn, ALogicalCaretPoint) then
        begin
          ATargetRow := ACalculator.ObtainTargetRowFromCurrentPageViewInfoAndCaretX(APageViewInfo, nil, X);
          if ATargetRow <> nil then
          begin
            ACharacter := ACalculator.GetTargetCharacter(ATargetRow, X);
            Result := ACharacter.GetFirstPosition(ActivePieceTable).LogPosition;
          end
          else
            Result := GetDefaultLogPosition;
			  end
  			else
        begin
          ATargetRow := GetTargetRowAtColumn(AColumn);
          ACharacter := ACalculator.GetTargetCharacter(ATargetRow, X);
          Result := ACharacter.GetFirstPosition(ActivePieceTable).LogPosition;
        end;
      finally
        ACalculator.Free;
      end;
    end;
  end;
end;

function TdxPrevNextScreenCommandBase.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := APos.PieceTable.IsMain;
end;

procedure TdxPrevNextScreenCommandBase.ChangeSelection(ASelection: TdxSelection);
var
  APhysicalPoint: TPoint;
  APos, ACurrentLogPosition: TdxDocumentLogPosition;
  ACommand: TdxPlaceCaretToPhysicalPointCommand;
  ACalculator: TdxNextCaretPositionVerticalDirectionCalculator;
  ANewPosition: TdxDocumentModelPosition;
begin
  APhysicalPoint := ActiveView.CreatePhysicalPoint(CaretPosition.PageViewInfo,
    CaretPosition.CalculateCaretBounds.TopLeft);
  if ScrollScreen then
  begin
    ACurrentLogPosition := ASelection.&End;
    ACommand := CreatePlaceCaretToPhysicalPointCommand;
    try
      ACommand.UpdateCaretX := False;
      ACommand.PhysicalPoint := APhysicalPoint;
      ACommand.Execute;
    finally
      ACommand.Free;
    end;
    if ShouldCalculateNewCaretLogPosition(ACurrentLogPosition, ASelection.&End) then
    begin
      APos := CalculateCaretNewLogPosition(APhysicalPoint);
      if APos <> MaxInt then
      begin
        if not IsNewPositionInCorrectDirection(ACurrentLogPosition, APos) then
        begin
          UpdateCaretPosition(TdxDocumentLayoutDetailsLevel.Row);
          ACalculator := CreateCaretPositionCalculator;
          try
            ANewPosition := ACalculator.CalculateNextPosition(CaretPosition);
            if ANewPosition.IsValid then
              APos := ANewPosition.LogPosition;
          finally
            ACalculator.Free;
          end;
        end;
        ASelection.&End := APos;
        if not ExtendSelection then
          ASelection.Start := APos;
      end;
    end;
  end
  else
    inherited ChangeSelection(ASelection);
end;

function TdxPrevNextScreenCommandBase.CreatePlaceCaretToPhysicalPointCommand: TdxPlaceCaretToPhysicalPointCommand;
begin
  Result := TdxPlaceCaretToPhysicalPointCommand.Create(RichEditControl);
end;

function TdxPrevNextScreenCommandBase.GetTryToKeepCaretX: Boolean;
begin
  Result := True;
end;

function TdxPrevNextScreenCommandBase.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.Character;
end;

{ TdxPreviousScreenCommand }

function TdxPreviousScreenCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
begin
  Result := ActivePieceTable.DocumentStartLogPosition;
end;

function TdxPreviousScreenCommand.CreateCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator;
begin
  Result := TdxNextCaretPositionLineUpCalculator.Create(RichEditControl);
end;

function TdxPreviousScreenCommand.GetDefaultLogPosition: Integer;
begin
  Result := ActivePieceTable.DocumentStartLogPosition;
end;

function TdxPreviousScreenCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxPreviousScreenCommand.GetTargetRowAtColumn(AColumn: TdxColumn): TdxRow;
begin
  Result := AColumn.Rows.Last;
end;

function TdxPreviousScreenCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

class function TdxPreviousScreenCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.PreviousScreen;
end;

function TdxPreviousScreenCommand.IsNewPositionInCorrectDirection(
  APreviousLogPosition, ACurrentLogPosition: Integer): Boolean;
begin
  Result := APreviousLogPosition > ACurrentLogPosition;
end;

function TdxPreviousScreenCommand.ScrollScreen: Boolean;
begin
  Result := ActiveView.VerticalScrollController.ScrollPageUp;
  if Result then
    ActiveView.OnVerticalScroll;
end;

function TdxPreviousScreenCommand.ShouldCalculateNewCaretLogPosition(
  APreviousLogPosition, ACurrentLogPosition: Integer): Boolean;
begin
  Result := (APreviousLogPosition = ACurrentLogPosition) and (APreviousLogPosition <> 0);
end;

function TdxPreviousScreenCommand.ShouldUseAnotherPage(AColumn: TdxColumn;
  const ALogicalCaretPoint: TPoint): Boolean;
begin
  Result := AColumn.Rows.First.Bounds.Bottom > ALogicalCaretPoint.Y;
end;

{ TdxExtendPreviousScreenCommand }

function TdxExtendPreviousScreenCommand.CreatePlaceCaretToPhysicalPointCommand: TdxPlaceCaretToPhysicalPointCommand;
begin
  Result := TdxExtendSelectionToPhysicalPointCommand.Create(RichEditControl);
end;

function TdxExtendPreviousScreenCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendPreviousScreenCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendPreviousScreen;
end;

{ TdxNextScreenCommand }

function TdxNextScreenCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
begin
  Result := ActivePieceTable.DocumentEndLogPosition;
end;

function TdxNextScreenCommand.CreateCaretPositionCalculator: TdxNextCaretPositionVerticalDirectionCalculator;
begin
  Result := TdxNextCaretPositionLineDownCalculator.Create(RichEditControl);
end;

function TdxNextScreenCommand.GetDefaultLogPosition: Integer;
begin
  Result := ActivePieceTable.DocumentEndLogPosition;
end;

function TdxNextScreenCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxNextScreenCommand.GetTargetRowAtColumn(AColumn: TdxColumn): TdxRow;
begin
  Result := AColumn.Rows.First;
end;

function TdxNextScreenCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

class function TdxNextScreenCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.NextScreen;
end;

function TdxNextScreenCommand.IsNewPositionInCorrectDirection(
  APreviousLogPosition, ACurrentLogPosition: Integer): Boolean;
begin
  Result := APreviousLogPosition < ACurrentLogPosition;
end;

function TdxNextScreenCommand.ScrollScreen: Boolean;
begin
  Result := ActiveView.VerticalScrollController.ScrollPageDown;
  if Result then
    ActiveView.OnVerticalScroll;
end;

function TdxNextScreenCommand.ShouldCalculateNewCaretLogPosition(
  APreviousLogPosition, ACurrentLogPosition: Integer): Boolean;
begin
  Result := APreviousLogPosition = ACurrentLogPosition;
end;

function TdxNextScreenCommand.ShouldUseAnotherPage(AColumn: TdxColumn; const ALogicalCaretPoint: TPoint): Boolean;
begin
  Result := AColumn.Rows.Last.Bounds.Top < ALogicalCaretPoint.Y;
end;

{ TdxExtendNextScreenCommand }

function TdxExtendNextScreenCommand.CreatePlaceCaretToPhysicalPointCommand: TdxPlaceCaretToPhysicalPointCommand;
begin
  Result := TdxExtendSelectionToPhysicalPointCommand.Create(RichEditControl);
end;

function TdxExtendNextScreenCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendNextScreenCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendNextScreen;
end;

{ TdxStartOfDocumentCommand }

function TdxStartOfDocumentCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxStartOfDocumentCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
begin
  Result := ActivePieceTable.DocumentStartLogPosition;
end;

function TdxStartOfDocumentCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxStartOfDocumentCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := True;
end;

function TdxStartOfDocumentCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxStartOfDocumentCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

class function TdxStartOfDocumentCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.StartOfDocument;
end;

{ TdxExtendStartOfDocumentCommand }

function TdxExtendStartOfDocumentCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendStartOfDocumentCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendStartOfDocument;
end;

{ TdxEndOfDocumentCommand }

function TdxEndOfDocumentCommand.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxEndOfDocumentCommand.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
begin
  Result := ActivePieceTable.DocumentEndLogPosition;
end;

function TdxEndOfDocumentCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxEndOfDocumentCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxEndOfDocumentCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxEndOfDocumentCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

class function TdxEndOfDocumentCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.EndOfDocument;
end;

{ TdxExtendEndOfDocumentCommand }

function TdxExtendEndOfDocumentCommand.GetExtendSelection: Boolean;
begin
  Result := True;
end;

class function TdxExtendEndOfDocumentCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ExtendEndOfDocument;
end;

procedure TdxExtendEndOfDocumentCommand.UpdateTableSelectionAfterSelectionUpdated(
  ALogPosition: Integer);
begin
  DocumentModel.Selection.UpdateTableSelectionStart(ALogPosition);
  DocumentModel.Selection.UpdateTableSelectionEnd(ALogPosition);
end;

{ TdxExtendSelectionByRangesCommand }

function TdxExtendSelectionByRangesCommand.ChangeSelectionEnd(
  ASelection: TdxSelection; ALogPosition: Integer;
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  if AHitTestResult.Character.GetFirstPosition(ActivePieceTable).LogPosition >= InitialLogPosition then
    Result := ExtendEnd1(ASelection, AHitTestResult)
  else
  begin
    ASelection.&End := ExtendEnd2(AHitTestResult);
    Result := False;
  end;
end;

procedure TdxExtendSelectionByRangesCommand.ChangeSelectionStart(
  ASelection: TdxSelection; ALogPosition: Integer;
  AHitTestResult: TdxRichEditHitTestResult);
begin
  if AHitTestResult.Character.GetFirstPosition(ActivePieceTable).LogPosition >= InitialLogPosition then
    ASelection.Start := ExtendStart1
  else
    ASelection.Start := ExtendStart2;
end;

function TdxExtendSelectionByRangesCommand.ExtendEnd1(ASelection: TdxSelection;
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  AIterator: TdxPieceTableIterator;
  APos: TdxDocumentModelPosition;
begin
  AIterator := CreateIterator;
  try
    APos := AHitTestResult.Character.GetFirstPosition(ActivePieceTable);
    ASelection.&End := AIterator.MoveForward(APos).LogPosition;
    Result := False;
  finally
    AIterator.Free;
  end;
end;

function TdxExtendSelectionByRangesCommand.ExtendEnd2(
  AHitTestResult: TdxRichEditHitTestResult): TdxDocumentLogPosition;
var
  AIterator: TdxPieceTableIterator;
  APos: TdxDocumentModelPosition;
begin
  AIterator := CreateIterator;
  try
    APos := AHitTestResult.Character.GetFirstPosition(ActivePieceTable);
    Result := AIterator.MoveBack(APos).LogPosition;
  finally
    AIterator.Free;
  end;
end;

function TdxExtendSelectionByRangesCommand.ExtendStart1: TdxDocumentLogPosition;
var
  AIterator: TdxPieceTableIterator;
  APos: TdxDocumentModelPosition;
begin
  AIterator := CreateIterator;
  try
    APos := InitialBox.GetFirstPosition(ActivePieceTable);
    if AIterator.IsNewElement(APos) then
      Result := APos.LogPosition
    else
      Result := AIterator.MoveBack(APos).LogPosition;
  finally
    AIterator.Free;
  end;
end;

function TdxExtendSelectionByRangesCommand.ExtendStart2: TdxDocumentLogPosition;
var
  AIterator: TdxPieceTableIterator;
  APos: TdxDocumentModelPosition;
begin
  AIterator := CreateIterator;
  try
    APos := InitialBox.GetFirstPosition(ActivePieceTable);
    Result := AIterator.MoveForward(APos).LogPosition;
  finally
    AIterator.Free;
  end;
end;

{ TdxExtendSelectionByWordsCommand }

function TdxExtendSelectionByWordsCommand.CreateIterator: TdxPieceTableIterator;
begin
  Result := TdxWordsDocumentModelIterator.Create(ActivePieceTable);
end;

{ TdxSelectAllCommand }

function TdxSelectAllCommand.CanChangePosition(
  const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxSelectAllCommand.ChangePosition(
  const APos: TdxDocumentModelPosition): Integer;
begin
  Result := APos.LogPosition;
end;

procedure TdxSelectAllCommand.ChangeSelection(ASelection: TdxSelection);
var
  AStartSelection, AEndSelection: TdxDocumentLogPosition;
begin
  DocumentModel.Selection.ClearMultiSelection;
  AStartSelection := ActivePieceTable.DocumentStartLogPosition;
  ASelection.SetStartCell(AStartSelection);
  ASelection.Start := AStartSelection;
  AEndSelection := ActivePieceTable.DocumentEndLogPosition + 1;
  ASelection.&End := AEndSelection;
  ASelection.UpdateTableSelectionEnd(AEndSelection);
end;

procedure TdxSelectAllCommand.EnsureCaretVisibleVertically;
begin
end;

function TdxSelectAllCommand.GetExtendSelection: Boolean;
begin
  Result := False;
end;

class function TdxSelectAllCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.SelectAll;
end;

function TdxSelectAllCommand.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxSelectAllCommand.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxSelectAllCommand.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

{ TdxExtendSelectionByParagraphsCommand }

function TdxExtendSelectionByParagraphsCommand.CreateIterator: TdxPieceTableIterator;
begin
  Result := TdxParagraphsDocumentModelIterator.Create(ActivePieceTable);
end;

{ TdxSelectLineCommand }

procedure TdxSelectLineCommand.ChangeSelection(ASelection: TdxSelection;
  ALogPosition: Integer; AHitTestResult: TdxRichEditHitTestResult);
var
  AValidator: TdxFieldIsSelectValidator;
begin
  DocumentModel.Selection.ClearMultiSelection;
  inherited ChangeSelection(ASelection, ALogPosition, AHitTestResult);
  AValidator := TdxFieldIsSelectValidator.Create(ActivePieceTable);
  try
    AValidator.ValidateSelection(ASelection);
  finally
    AValidator.Free;
  end;
end;

function TdxSelectLineCommand.ChangeSelectionEnd(ASelection: TdxSelection;
  ALogPosition: Integer; AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := SelectToTheEndOfRow(ASelection, AHitTestResult.Row, True);
end;

procedure TdxSelectLineCommand.ChangeSelectionStart(ASelection: TdxSelection;
  ALogPosition: Integer; AHitTestResult: TdxRichEditHitTestResult);
var
  APos: TdxDocumentLogPosition;
begin
  APos := AHitTestResult.Row.GetFirstPosition(ActivePieceTable).LogPosition;
  if APos > 0 then
    ASelection.Start := APos - 1
  else
    ASelection.Start := APos;
  ASelection.ClearMultiSelection;
  ASelection.SetStartCell(ALogPosition);
  ASelection.Start := APos;
end;

function TdxSelectLineCommand.ExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxSelectLineCommand.HitTestOnlyInPageClientBounds: Boolean;
begin
  Result := False;
end;

{ TdxExtendSelectionByLinesCommand }

function TdxExtendSelectionByLinesCommand.ChangeSelectionEnd(
  ASelection: TdxSelection; ALogPosition: Integer;
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  if AHitTestResult.Character.GetFirstPosition(ActivePieceTable).LogPosition >= InitialLogPosition then
    Result := ExtendEnd1(ASelection, AHitTestResult)
  else
  begin
    ASelection.&End := ExtendEnd2(AHitTestResult);
    Result := False;
  end;
end;

procedure TdxExtendSelectionByLinesCommand.ChangeSelectionStart(
  ASelection: TdxSelection; ALogPosition: Integer;
  AHitTestResult: TdxRichEditHitTestResult);
begin
  if AHitTestResult.Character.GetFirstPosition(ActivePieceTable).LogPosition >= InitialLogPosition then
    ASelection.Start := ExtendStart1
  else
    ASelection.Start := ExtendStart2;
end;

function TdxExtendSelectionByLinesCommand.ExtendEnd1(ASelection: TdxSelection;
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := SelectToTheEndOfRow(ASelection, AHitTestResult.Row, True);
end;

function TdxExtendSelectionByLinesCommand.ExtendEnd2(
  AHitTestResult: TdxRichEditHitTestResult): TdxDocumentLogPosition;
begin
  Result := AHitTestResult.Row.GetFirstPosition(ActivePieceTable).LogPosition;
end;

function TdxExtendSelectionByLinesCommand.ExtendStart1: TdxDocumentLogPosition;
begin
  Result := InitialLogPosition;
end;

function TdxExtendSelectionByLinesCommand.ExtendStart2: TdxDocumentLogPosition;
begin
  Result := InitialBox.GetLastPosition(ActivePieceTable).LogPosition + 1;
end;

{ TdxSelectFieldNextToCaretCommand }

function TdxSelectFieldNextToCaretCommand.CanChangePosition(
  const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := True;
end;

function TdxSelectFieldNextToCaretCommand.ChangePosition(
  const APos: TdxDocumentModelPosition): Integer;
begin
  Result := APos.LogPosition;
end;

{ TdxSelectionBasedCommandBase }

function TdxSelectionBasedCommandBase.CalculateStartPosition(AItem: TdxSelectionItem; AllowSelectionExpanding: Boolean): TdxDocumentModelPosition;
begin
  Result := AItem.CalculateStartPosition(AllowSelectionExpanding);
end;

function TdxSelectionBasedCommandBase.CalculateEndPosition(AItem: TdxSelectionItem; AllowSelectionExpanding: Boolean): TdxDocumentModelPosition;
begin
  Result := AItem.CalculateEndPosition(AllowSelectionExpanding);
end;

{ TdxExtendSelectionToPhysicalPointCommand }

function TdxExtendSelectionToPhysicalPointCommand.ExtendSelection: Boolean;
begin
  Result := True;
end;

{ TdxSelectionBasedPropertyChangeCommandBase }

function TdxSelectionBasedPropertyChangeCommandBase.CanEditSelection: Boolean;
var
  AItems: TdxSelectionItems;
  I: Integer;
  AItem: TdxSelectionItem;
  AStart, AEnd: TdxDocumentModelPosition;
begin
  AItems := GetSelectionItems;
  Result := True;
  for I := 0 to AItems.Count -1 do
  begin
    AItem := AItems[I];
    AStart := CalculateStartPosition(AItem, True);
    AEnd := CalculateEndPosition(AItem, True);
    if not ActivePieceTable.CanEditRange(AStart.LogPosition, AEnd.LogPosition) then
    begin
      Result := False;
      Break;
    end;
  end;
end;

procedure TdxSelectionBasedPropertyChangeCommandBase.ForceExecute(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  if not ValidateUIState(AState) then
    Exit;
  NotifyBeginCommandExecution(AState);
  try
    RichEditControl.BeginUpdate;
    try
		  ModifyDocumentModel(AState);
      ActiveView.EnsureCaretVisible;
    finally
      RichEditControl.EndUpdate;
    end;
  finally
    NotifyEndCommandExecution(AState);
  end;
end;

function TdxSelectionBasedPropertyChangeCommandBase.GetSelectionItems: TdxSelectionItems;
begin
  Result := DocumentModel.Selection.Items;
end;

procedure TdxSelectionBasedPropertyChangeCommandBase.ModifyDocumentModel(
  AState: IdxCommandUIState);
var
  ADocumentModel: TdxDocumentModel;
begin
  ADocumentModel := DocumentModel;
  ADocumentModel.BeginUpdate;
  try
    ModifyDocumentModelCore(AState);
  finally
    ADocumentModel.EndUpdate;
  end;
end;

procedure TdxSelectionBasedPropertyChangeCommandBase.ModifyDocumentModelCore(
  AState: IdxCommandUIState);
var
  AList: TdxSelectionItems;
  ACount, I: Integer;
  Actions: TdxDocumentModelChangeActions;
  AItem: TdxSelectionItem;
  ASelectionValid: Boolean;
  AStart, AEnd: TdxDocumentModelPosition;
begin
  AList := GetSelectionItems;
  ACount := AList.Count;
  Actions := [];
  for I := 0 to ACount - 1 do
  begin
    AItem := AList[I];
    ASelectionValid := ValidateSelectionInterval(AItem);
    AStart := CalculateStartPosition(AItem, True);
    AEnd := CalculateEndPosition(AItem, True);
    if ASelectionValid then
      Actions := Actions + ChangeProperty(AStart, AEnd, AState);
  end;
  ActivePieceTable.ApplyChangesCore(Actions, -1, -1);
end;

function TdxSelectionBasedPropertyChangeCommandBase.ValidateSelectionInterval(
  AItem: TdxSelectionItem): Boolean;
begin
  Result := True;
end;

function TdxSelectionBasedPropertyChangeCommandBase.ValidateUIState(
  AState: IdxCommandUIState): Boolean;
begin
  Result := True;
end;

function TdxSelectionBasedPropertyChangeCommandBase.ActivePieceTableObtainParagraphsPropertyValue<T>(ALogPositionStart: TdxDocumentLogPosition;
  ALength: Integer; AModifier: TdxParagraphPropertyModifier<T>; out AValue: T): Boolean;
var
  AInfo: TdxRunInfo;
  I: TdxParagraphIndex;
  AParagraphValue: T;
  AActivePieceTable: TdxPieceTable;
begin
  AActivePieceTable := ActivePieceTable;
  AInfo := AActivePieceTable.FindRunInfo(ALogPositionStart, ALength);
  try
    AValue := AModifier.GetParagraphPropertyValue(AActivePieceTable.Paragraphs[AInfo.Start.ParagraphIndex]);
    Result := True;
    for I := AInfo.Start.ParagraphIndex + 1 to AInfo.&End.ParagraphIndex do
    begin
      AParagraphValue := AModifier.GetParagraphPropertyValue(AActivePieceTable.Paragraphs[I]);
      if not AModifier.IsValueEquals(AParagraphValue, AValue) then
      begin
        Result := False;
        Break;
      end;
    end;
  finally
    AInfo.Free;
  end;
end;

function TdxSelectionBasedPropertyChangeCommandBase.ActivePieceTableObtainRunsPropertyValue<T>(
  const ALogPositionStart: TdxDocumentLogPosition; ALength: Integer; AModifier: TdxRunPropertyModifier<T>;
  out AValue: T): Boolean;
var
  AInfo: TdxRunInfo;
  I: TdxRunIndex;
  ARunValue: T;
  AActivePieceTable: TdxPieceTable;
begin
  AActivePieceTable := ActivePieceTable;
  AInfo := AActivePieceTable.FindRunInfo(ALogPositionStart, ALength);
  try
    Result := True;
    AValue := AModifier.GetRunPropertyValue(AActivePieceTable.Runs[AInfo.Start.RunIndex]);
    for I := AInfo.Start.RunIndex + 1 to AInfo.&End.RunIndex do
    begin
      ARunValue := AModifier.GetRunPropertyValue(AActivePieceTable.Runs[I]);
      Result := AModifier.IsValueEquals(AValue, ARunValue);
      if not Result then
        Break;
    end;
  finally
    AInfo.Free;
  end;
end;

{ TdxSimpleSetSelectionCommand }

procedure TdxSimpleSetSelectionCommand.ExecuteCore;
var
  ADocumentModel: TdxDocumentModel;
  ASelection: TdxSelection;
begin
  ADocumentModel := DocumentModel;
  ASelection := ADocumentModel.Selection;
  ADocumentModel.BeginUpdate;
  try
    ASelection.Start := FPos.LogPosition;
    ASelection.&End := FPos.LogPosition;
  finally
    ADocumentModel.EndUpdate;
  end;
end;

procedure TdxSimpleSetSelectionCommand.UpdateUIStateCore(
  const AState: IdxCommandUIState);
begin
  AState.Checked := False;
  AState.Enabled := True;
  AState.Visible := True;
end;

end.
