{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.TextRange;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.LayoutEngine.BoxMeasurer, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.Utils.ChunkedStringBuilder;

type

  { TdxLayoutDependentTextRun }

  TdxLayoutDependentTextRun = class(TdxTextRun)
  private
    FFieldResultFormatting: TdxFieldResultFormatting;
  protected
    procedure CopyContentCore(ACopyManager: TdxDocumentModelCopyManager); override;
    function CalculateRowProcessingFlags: TdxRowProcessingFlags; override;
    procedure InheritRowProcessgFlags(ARun: TdxTextRunBase); override;
  public
    function CreateRun(AParagraph: TdxParagraph; AStartIndex, ALength: Integer): TdxTextRun; override;
    function CanPlaceCaretBefore: Boolean; override;
    function CanJoinWith(ARun: TdxTextRunBase): Boolean; override;
    function GetText(ABuffer: TdxChunkedStringBuilder; AFrom: Integer; ATo: Integer): string; override;
    procedure Export(const AExporter: IdxDocumentModelExporter); override;
    procedure Measure(ABoxInfo: TdxBoxInfo; const AMeasurer: IdxObjectMeasurer); override;

    property FieldResultFormatting: TdxFieldResultFormatting read FFieldResultFormatting
      write FFieldResultFormatting;
  end;

  TdxSpecialTextRun = class abstract(TdxTextRun)
  end;

implementation

uses
  RTLConsts, dxRichEdit.Platform.Font, dxRichEdit.DocumentModel.Core;

{ TdxLayoutDependentTextRun }

function TdxLayoutDependentTextRun.CanPlaceCaretBefore: Boolean;
begin
  Result := True;
end;

function TdxLayoutDependentTextRun.CanJoinWith(ARun: TdxTextRunBase): Boolean;
begin
  Result := False;
end;

procedure TdxLayoutDependentTextRun.Measure(ABoxInfo: TdxBoxInfo; const AMeasurer: IdxObjectMeasurer);
var
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := DocumentModel.FontCache[FontCacheIndex];
  AMeasurer.MeasureText(ABoxInfo, (TdxLayoutDependentTextBox(ABoxInfo.Box)).CalculatedText, AFontInfo);
end;

function TdxLayoutDependentTextRun.GetText(ABuffer: TdxChunkedStringBuilder; AFrom: Integer;
  ATo: Integer): string;
begin
  Result := '#';
end;

procedure TdxLayoutDependentTextRun.Export(const AExporter: IdxDocumentModelExporter);
begin
  AExporter.Export(Self);
end;

function TdxLayoutDependentTextRun.CreateRun(AParagraph: TdxParagraph; AStartIndex, ALength: Integer): TdxTextRun;
begin
  raise Exception.Create('');
  Result := nil;
end;

procedure TdxLayoutDependentTextRun.CopyContentCore(ACopyManager: TdxDocumentModelCopyManager);
var
  AParagraphIndex: TdxParagraphIndex;
  ALogPosition: TdxDocumentLogPosition;
begin
  ALogPosition := ACopyManager.TargetPosition.LogPosition;
  AParagraphIndex := ACopyManager.TargetPosition.ParagraphIndex;
  ACopyManager.TargetPieceTable.InsertLayoutDependentTextRun(AParagraphIndex, ALogPosition, FFieldResultFormatting);
end;

function TdxLayoutDependentTextRun.CalculateRowProcessingFlags: TdxRowProcessingFlags;
begin
  Result := inherited CalculateRowProcessingFlags + [TdxRowProcessingFlag.ProcessLayoutDependentText];
end;

procedure TdxLayoutDependentTextRun.InheritRowProcessgFlags(ARun: TdxTextRunBase);
begin
  RowProcessingFlags := ARun.RowProcessingFlags + [TdxRowProcessingFlag.ProcessLayoutDependentText];
end;

end.
