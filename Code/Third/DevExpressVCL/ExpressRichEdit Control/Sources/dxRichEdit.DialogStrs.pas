{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DialogStrs;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxCore, cxClasses, dxRichEdit.DocumentModel.ParagraphFormatting;

resourcestring
  sdxRichEditUnitsInches = '"';
  sdxRichEditUnitsCentimeters = 'cm';
  sdxRichEditUnitsMillimeters = 'mm';
  sdxRichEditUnitsPoints = 'pt';
  sdxRichEditUnitsPicas = 'pi';

  sdxRichEditTabForm_All = 'All';

  // Paragraph Dialog localize

  sdxRichEditParagraphDialogAfter = 'Aft&er:';
  sdxRichEditParagraphDialogAlignment = 'Ali&gnment:';
  sdxRichEditParagraphDialogAt = '&At:';
  sdxRichEditParagraphDialogBefore = '&Before:';
  sdxRichEditParagraphDialogButtonCancel = 'Cancel';
  sdxRichEditParagraphDialogButtonOK = 'OK';
  sdxRichEditParagraphDialogButtonTabs = '&Tabs...';
  sdxRichEditParagraphDialogBy = 'B&y:';
  sdxRichEditParagraphDialogDontAddSpace = 'Don''t add spa&ce between paragraphs of the same style';
  sdxRichEditParagraphDialogForm = 'Paragraph';
  sdxRichEditParagraphDialogGeneral = 'General';
  sdxRichEditParagraphDialogIndentation = 'Indentation';
  sdxRichEditParagraphDialogIndentsAndSpacing = '&Indents and Spacing';
  sdxRichEditParagraphDialogKeepLinesTogether = '&Keep lines together';
  sdxRichEditParagraphDialogLeft = '&Left:';
  sdxRichEditParagraphDialogLineAndPageBreaks = 'Line and &Page Breaks';
  sdxRichEditParagraphDialogLineSpacing = 'Li&ne Spacing:';
  sdxRichEditParagraphDialogOutlinelevel = '&Outline level:';
  sdxRichEditParagraphDialogPageBreakBefore = 'Page &break before';
  sdxRichEditParagraphDialogPagination = 'Pagination';
  sdxRichEditParagraphDialogRight = '&Right:';
  sdxRichEditParagraphDialogSpacing = 'Spacing';
  sdxRichEditParagraphDialogSpecial = '&Special:';

  // Paragraph Dialog - List box items

  sdxParagraphAlignmentLeft = 'Left';
  sdxParagraphAlignmentRight = 'Right';
  sdxParagraphAlignmentCenter = 'Center';
  sdxParagraphAlignmentJustify = 'Justify';

  sdxParagraphLineSpacingSingle = 'Single';
  sdxParagraphLineSpacingSesquialteral = '1.5 lines';
  sdxParagraphLineSpacingDouble = 'Double';
  sdxParagraphLineSpacingMultiple = 'Multiple';
  sdxParagraphLineSpacingExactly = 'Exactly';
  sdxParagraphLineSpacingAtLeast = 'AtLeast';

  sdxParagraphFirstLineIndentNone = '(none)';
  sdxParagraphFirstLineIndentIndented = 'First line';
  sdxParagraphFirstLineIndentHanging = 'Hanging';

  sdxParagraphOutlineLeve0 = 'Body Text';
  sdxParagraphOutlineLeve1 = 'Level 1';
  sdxParagraphOutlineLeve2 = 'Level 2';
  sdxParagraphOutlineLeve3 = 'Level 3';
  sdxParagraphOutlineLeve4 = 'Level 4';
  sdxParagraphOutlineLeve5 = 'Level 5';
  sdxParagraphOutlineLeve6 = 'Level 6';
  sdxParagraphOutlineLeve7 = 'Level 7';
  sdxParagraphOutlineLeve8 = 'Level 8';
  sdxParagraphOutlineLeve9 = 'Level 9';

  // Tabs Dialog localize

  sdxRichEditTabsDialogAlignment = 'Alignment';
  sdxRichEditTabsDialogButtonCancel = 'Cancel';
  sdxRichEditTabsDialogButtonClear = 'Cl&ear';
  sdxRichEditTabsDialogButtonClearAll = 'Clear &All';
  sdxRichEditTabsDialogButtonOk = 'OK';
  sdxRichEditTabsDialogButtonSet = '&Set';
  sdxRichEditTabsDialogCenter = '&Center';
  sdxRichEditTabsDialogDecimal = '&Decimal';
  sdxRichEditTabsDialogDefaultTabStops = 'De&fault tab stops:';
  sdxRichEditTabsDialogDots = '&Dots';
  sdxRichEditTabsDialogEqualSign = '&EqualSign';
  sdxRichEditTabsDialogForm = 'Tabs';
  sdxRichEditTabsDialogHyphens = '&Hyphens';
  sdxRichEditTabsDialogLeader = 'Leader';
  sdxRichEditTabsDialogLeft = '&Left';
  sdxRichEditTabsDialogMiddleDots = '&MiddleDots';
  sdxRichEditTabsDialogNone = '&(None)';
  sdxRichEditTabsDialogRight = '&Right';
  sdxRichEditTabsDialogTabStopPosition = '&Tab stop position:';
  sdxRichEditTabsDialogTabStopsToBeCleared = 'Tab stops to be cleared:';
  sdxRichEditTabsDialogThickLine = '&ThickLine';
  sdxRichEditTabsDialogUnderline = '&Underline';

  // Bullets and Numbering Dialogs

  sdxNumberingListDialogBulleted = '&Bulleted';
  sdxNumberingListDialogButtonCancel = 'Cancel';
  sdxNumberingListDialogButtonCustomize = 'Customize...';
  sdxNumberingListDialogButtonOk = 'OK';
  sdxNumberingListDialogForm = 'Bullets and Numbering';
  sdxNumberingListDialogNumbered = '&Numbered';
  sdxNumberingListDialogOutlineNumbered = 'O&utline Numbered';

  // Bullets and Numbering Dialogs

  sdxRichEditFontDialogAllCaps = '&All caps';
  sdxRichEditFontDialogButtonCancel = 'Cancel';
  sdxRichEditFontDialogButtonOk = 'OK';
  sdxRichEditFontDialogDoubleStrikeout = 'Double strikethrou&gh';
  sdxRichEditFontDialogEffects = 'Effects';
  sdxRichEditFontDialogFontColor = 'Font Color:';
  sdxRichEditFontDialogFontName = 'Font:';
  sdxRichEditFontDialogFontSize = 'Size:';
  sdxRichEditFontDialogFontStyle = 'Font style:';
  sdxRichEditFontDialogForm = 'Font';
  sdxRichEditFontDialogHidden = '&Hidden';
  sdxRichEditFontDialogStrikeout = 'Stri&kethrough';
  sdxRichEditFontDialogSubscript = 'Su&bscript';
  sdxRichEditFontDialogSuperscript = 'Su&perscript';
  sdxRichEditFontDialogUnderlineColor = 'Underline color:';
  sdxRichEditFontDialogUnderlineStyle = 'Underline style:';
  sdxRichEditFontDialogUnderlineWordsOnly = '&Underline words only';

  sdxRichEditFontDialogFontStyleRegular = 'Regular';
  sdxRichEditFontDialogFontStyleItalic = 'Italic';
  sdxRichEditFontDialogFontStyleBold = 'Bold';
  sdxRichEditFontDialogFontStyleBoldItalic = 'Bold Italic';

  sdxRichEditFontDialogUnderlineStyleNone = '(none)';
  sdxRichEditFontDialogUnderlineStyleSingle = 'Single';
  sdxRichEditFontDialogUnderlineStyleDouble = 'Double';

  sdxRichEditFontDialogButtonColorAuto = 'Auto';

const
  MinOutlineLevel = 0;
  MaxOutlineLevel = 9;

var
  dxParagraphAlignmentNames: array[TdxParagraphAlignment] of Pointer =
    (@sdxParagraphAlignmentLeft, @sdxParagraphAlignmentRight, @sdxParagraphAlignmentCenter, @sdxParagraphAlignmentJustify);

  dxParagraphLineSpacingNames: array[TdxParagraphLineSpacing] of Pointer =
    (@sdxParagraphLineSpacingSingle, @sdxParagraphLineSpacingSesquialteral, @sdxParagraphLineSpacingDouble,
     @sdxParagraphLineSpacingMultiple, @sdxParagraphLineSpacingExactly, @sdxParagraphLineSpacingAtLeast);

  dxParagraphFirstLineIndentNames: array[TdxParagraphFirstLineIndent] of Pointer =
    (@sdxParagraphFirstLineIndentNone, @sdxParagraphFirstLineIndentIndented, @sdxParagraphFirstLineIndentHanging);

  dxParagraphOutlineLevelNames: array[MinOutlineLevel..MaxOutlineLevel] of Pointer =
    (@sdxParagraphOutlineLeve0, @sdxParagraphOutlineLeve1, @sdxParagraphOutlineLeve2, @sdxParagraphOutlineLeve3,
     @sdxParagraphOutlineLeve4, @sdxParagraphOutlineLeve5, @sdxParagraphOutlineLeve6, @sdxParagraphOutlineLeve7,
     @sdxParagraphOutlineLeve8, @sdxParagraphOutlineLeve9);

procedure AddRichEditDialogsResourceStringNames(AProduct: TdxProductResourceStrings);

implementation

procedure AddRichEditDialogsResourceStringNames(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxRichEditUnitsInches', @sdxRichEditUnitsInches);
  AProduct.Add('sdxRichEditUnitsCentimeters', @sdxRichEditUnitsCentimeters);
  AProduct.Add('sdxRichEditUnitsMillimeters', @sdxRichEditUnitsMillimeters);
  AProduct.Add('sdxRichEditUnitsPoints', @sdxRichEditUnitsMillimeters);
  AProduct.Add('sdxRichEditUnitsPicas', @sdxRichEditUnitsPicas);
  AProduct.Add('sdxRichEditTabForm_All', @sdxRichEditTabForm_All);

  // Paragraph Dialog
  AProduct.Add('sdxRichEditParagraphDialogAfter', @sdxRichEditParagraphDialogAfter);
  AProduct.Add('sdxRichEditParagraphDialogAlignment', @sdxRichEditParagraphDialogAlignment);
  AProduct.Add('sdxRichEditParagraphDialogAt', @sdxRichEditParagraphDialogAt);
  AProduct.Add('sdxRichEditParagraphDialogBefore', @sdxRichEditParagraphDialogBefore);
  AProduct.Add('sdxRichEditParagraphDialogButtonCancel', @sdxRichEditParagraphDialogButtonCancel);
  AProduct.Add('sdxRichEditParagraphDialogButtonOK', @sdxRichEditParagraphDialogButtonOK);
  AProduct.Add('sdxRichEditParagraphDialogButtonTabs', @sdxRichEditParagraphDialogButtonTabs);
  AProduct.Add('sdxRichEditParagraphDialogBy', @sdxRichEditParagraphDialogBy);
  AProduct.Add('sdxRichEditParagraphDialogDontAddSpace', @sdxRichEditParagraphDialogDontAddSpace);
  AProduct.Add('sdxRichEditParagraphDialogForm', @sdxRichEditParagraphDialogForm);
  AProduct.Add('sdxRichEditParagraphDialogGeneral', @sdxRichEditParagraphDialogGeneral);
  AProduct.Add('sdxRichEditParagraphDialogIndentation', @sdxRichEditParagraphDialogIndentation);
  AProduct.Add('sdxRichEditParagraphDialogIndentsAndSpacing', @sdxRichEditParagraphDialogIndentsAndSpacing);
  AProduct.Add('sdxRichEditParagraphDialogKeepLinesTogether', @sdxRichEditParagraphDialogKeepLinesTogether);
  AProduct.Add('sdxRichEditParagraphDialogLeft', @sdxRichEditParagraphDialogLeft);
  AProduct.Add('sdxRichEditParagraphDialogLineAndPageBreaks', @sdxRichEditParagraphDialogLineAndPageBreaks);
  AProduct.Add('sdxRichEditParagraphDialogLineSpacing', @sdxRichEditParagraphDialogLineSpacing);
  AProduct.Add('sdxRichEditParagraphDialogOutlinelevel', @sdxRichEditParagraphDialogOutlinelevel);
  AProduct.Add('sdxRichEditParagraphDialogPageBreakBefore', @sdxRichEditParagraphDialogPageBreakBefore);
  AProduct.Add('sdxRichEditParagraphDialogPagination', @sdxRichEditParagraphDialogPagination);
  AProduct.Add('sdxRichEditParagraphDialogRight', @sdxRichEditParagraphDialogRight);
  AProduct.Add('sdxRichEditParagraphDialogSpacing', @sdxRichEditParagraphDialogSpacing);
  AProduct.Add('sdxRichEditParagraphDialogSpecial', @sdxRichEditParagraphDialogSpecial);

  //Tabs Dialog
  AProduct.Add('sdxRichEditTabsDialogAlignment', @sdxRichEditTabsDialogAlignment);
  AProduct.Add('sdxRichEditTabsDialogButtonCancel', @sdxRichEditTabsDialogButtonCancel);
  AProduct.Add('sdxRichEditTabsDialogButtonClear', @sdxRichEditTabsDialogButtonClear);
  AProduct.Add('sdxRichEditTabsDialogButtonClearAll', @sdxRichEditTabsDialogButtonClearAll);
  AProduct.Add('sdxRichEditTabsDialogButtonOk', @sdxRichEditTabsDialogButtonOk);
  AProduct.Add('sdxRichEditTabsDialogButtonSet', @sdxRichEditTabsDialogButtonSet);
  AProduct.Add('sdxRichEditTabsDialogCenter', @sdxRichEditTabsDialogCenter);
  AProduct.Add('sdxRichEditTabsDialogDecimal', @sdxRichEditTabsDialogDecimal);
  AProduct.Add('sdxRichEditTabsDialogDefaultTabStops', @sdxRichEditTabsDialogDefaultTabStops);
  AProduct.Add('sdxRichEditTabsDialogDots', @sdxRichEditTabsDialogDots);
  AProduct.Add('sdxRichEditTabsDialogEqualSign', @sdxRichEditTabsDialogEqualSign);
  AProduct.Add('sdxRichEditTabsDialogForm', @sdxRichEditTabsDialogForm);
  AProduct.Add('sdxRichEditTabsDialogHyphens', @sdxRichEditTabsDialogHyphens);
  AProduct.Add('sdxRichEditTabsDialogLeader', @sdxRichEditTabsDialogLeader);
  AProduct.Add('sdxRichEditTabsDialogLeft', @sdxRichEditTabsDialogLeft);
  AProduct.Add('sdxRichEditTabsDialogMiddleDots', @sdxRichEditTabsDialogMiddleDots);
  AProduct.Add('sdxRichEditTabsDialogNone', @sdxRichEditTabsDialogNone);
  AProduct.Add('sdxRichEditTabsDialogRight', @sdxRichEditTabsDialogRight);
  AProduct.Add('sdxRichEditTabsDialogTabStopPosition', @sdxRichEditTabsDialogTabStopPosition);
  AProduct.Add('sdxRichEditTabsDialogTabStopsToBeCleared', @sdxRichEditTabsDialogTabStopsToBeCleared);
  AProduct.Add('sdxRichEditTabsDialogThickLine', @sdxRichEditTabsDialogThickLine);
  AProduct.Add('sdxRichEditTabsDialogUnderline', @sdxRichEditTabsDialogUnderline);

  // Bullets and Numbering Dialogs

  AProduct.Add('sdxNumberingListDialogBulleted', @sdxNumberingListDialogBulleted);
  AProduct.Add('sdxNumberingListDialogButtonCancel', @sdxNumberingListDialogButtonCancel);
  AProduct.Add('sdxNumberingListDialogButtonCustomize', @sdxNumberingListDialogButtonCustomize);
  AProduct.Add('sdxNumberingListDialogButtonOk', @sdxNumberingListDialogButtonOk);
  AProduct.Add('sdxNumberingListDialogForm', @sdxNumberingListDialogForm);
  AProduct.Add('sdxNumberingListDialogNumbered', @sdxNumberingListDialogNumbered);
  AProduct.Add('sdxNumberingListDialogOutlineNumbered', @sdxNumberingListDialogOutlineNumbered);

  // Font Dialog

  AProduct.Add('sdxRichEditFontDialogAllCaps', @sdxRichEditFontDialogAllCaps);
  AProduct.Add('sdxRichEditFontDialogButtonCancel', @sdxRichEditFontDialogButtonCancel);
  AProduct.Add('sdxRichEditFontDialogButtonOk', @sdxRichEditFontDialogButtonOk);
  AProduct.Add('sdxRichEditFontDialogDoubleStrikeout', @sdxRichEditFontDialogDoubleStrikeout);
  AProduct.Add('sdxRichEditFontDialogEffects', @sdxRichEditFontDialogEffects);
  AProduct.Add('sdxRichEditFontDialogFontColor', @sdxRichEditFontDialogFontColor);
  AProduct.Add('sdxRichEditFontDialogFontName', @sdxRichEditFontDialogFontName);
  AProduct.Add('sdxRichEditFontDialogFontSize', @sdxRichEditFontDialogFontSize);
  AProduct.Add('sdxRichEditFontDialogFontStyle', @sdxRichEditFontDialogFontStyle);
  AProduct.Add('sdxRichEditFontDialogForm', @sdxRichEditFontDialogForm);
  AProduct.Add('sdxRichEditFontDialogHidden', @sdxRichEditFontDialogHidden);
  AProduct.Add('sdxRichEditFontDialogStrikeout', @sdxRichEditFontDialogStrikeout);
  AProduct.Add('sdxRichEditFontDialogSubscript', @sdxRichEditFontDialogSubscript);
  AProduct.Add('sdxRichEditFontDialogSuperscript', @sdxRichEditFontDialogSuperscript);
  AProduct.Add('sdxRichEditFontDialogUnderlineColor', @sdxRichEditFontDialogUnderlineColor);
  AProduct.Add('sdxRichEditFontDialogUnderlineStyle', @sdxRichEditFontDialogUnderlineStyle);
  AProduct.Add('sdxRichEditFontDialogUnderlineWordsOnly', @sdxRichEditFontDialogUnderlineWordsOnly);

  AProduct.Add('sdxRichEditFontDialogFontStyleRegular', @sdxRichEditFontDialogFontStyleRegular);
  AProduct.Add('sdxRichEditFontDialogFontStyleItalic', @sdxRichEditFontDialogFontStyleItalic);
  AProduct.Add('sdxRichEditFontDialogFontStyleBold', @sdxRichEditFontDialogFontStyleBold);
  AProduct.Add('sdxRichEditFontDialogFontStyleBoldItalic', @sdxRichEditFontDialogFontStyleBoldItalic);

  AProduct.Add('sdxRichEditFontDialogUnderlineStyleNone', @sdxRichEditFontDialogUnderlineStyleNone);
  AProduct.Add('sdxRichEditFontDialogUnderlineStyleSingle', @sdxRichEditFontDialogUnderlineStyleSingle);
  AProduct.Add('sdxRichEditFontDialogUnderlineStyleDouble', @sdxRichEditFontDialogUnderlineStyleDouble);

  AProduct.Add('sdxRichEditFontDialogButtonColorAuto', @sdxRichEditFontDialogButtonColorAuto);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('RichEdit', @AddRichEditDialogsResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('RichEdit');
end.
