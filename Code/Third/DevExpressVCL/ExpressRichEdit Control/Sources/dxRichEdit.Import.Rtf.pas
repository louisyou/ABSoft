{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Generics.Collections, dxRichEdit.Options,
  dxCoreClasses, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Import, dxCore,
  dxRichEdit.DocumentModel.Core, dxRichEdit.DocumentModel.Section,
  dxRichEdit.DocumentModel.CharacterFormatting, dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.TabFormatting,
  dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.Styles, dxRichEdit.Utils.OfficeImage, dxRichEdit.Utils.Types,
  dxRichEdit.DocumentModel.Numbering, dxRichEdit.Import.Rtf.TableReader,
  dxRichEdit.DocumentModel.TableFormatting;

type
  TdxRichEditDocumentModelRtfImporter = class;
  TdxRtfInputPosition = class;

  TdxRichEditRtfParsingState = (Normal, BinData, HexData);

  TdxRichEditPopRtfState = (StackNonEmpty, StackEmpty);

  { TdxRtfImageInfo }

  TdxRtfImageInfo = class
  strict private
    FDocumentModel: TdxDocumentModel;
    FRtfImage: TdxOfficeImageReference;
    FSizeInModelUnits: TSize;
    FScaleX: Integer;
    FScaleY: Integer;
    FPseudoInline: Boolean;
  private
    procedure SetRtfImage(const Value: TdxOfficeImageReference);
  protected
    procedure CreateImage(ANativeImage: TdxOfficeImage);
    property DocumentModel: TdxDocumentModel read FDocumentModel;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);
    destructor Destroy; override;

    procedure LoadMetafileFromStream(AStream: TMemoryStream; AMapMode: TdxMapMode; APictureWidth, APictureHeight: Integer);
    procedure LoadImageFromStream(AStream: TStream);
    procedure LoadDibFromStream(AStream: TStream; AWidth, AHeight, ABytesInLine: Integer);
    procedure LoadBitmapFromStream(AStream: TMemoryStream; AWidth, AHeight, AColorPlanesCount, ABitsPerPixel, ABytesInLine: Integer);

    property RtfImage: TdxOfficeImageReference read FRtfImage write SetRtfImage;
    property SizeInModelUnits: TSize read FSizeInModelUnits write FSizeInModelUnits;
    property ScaleX: Integer read FScaleX write FScaleX;
    property ScaleY: Integer read FScaleY write FScaleY;
    property PseudoInline: Boolean read FPseudoInline write FPseudoInline;
  end;

  { TdxRtfFontInfo }

  TdxRtfFontInfo = class
  private
    FCharset: Integer;
    FID: Integer;
    FName: string;
  public
    constructor Create;

    property Charset: Integer read FCharset write FCharset;
    property ID: Integer read FID write FID;
    property Name: string read FName write FName;
  end;

  { TdxRtfFontInfoCollection }

  TdxRtfFontInfoCollection = class(TdxFastObjectList)
  private
    FDefaultRtfFontInfo: TdxRtfFontInfo;
    function CreateDefaultRtfFontInfo(ADocumentModel: TdxDocumentModel): TdxRtfFontInfo;
    function GetItem(Index: Integer): TdxRtfFontInfo;
    procedure SetItem(Index: Integer; const Value: TdxRtfFontInfo);
  public
    constructor Create(ADocumentModel: TdxDocumentModel);
    destructor Destroy; override;
    function GetRtfFontInfoById(AID: Integer): TdxRtfFontInfo;

    property DefaultRtfFontInfo: TdxRtfFontInfo read FDefaultRtfFontInfo;
    property Items[Index: Integer]: TdxRtfFontInfo read GetItem write SetItem; default;
  end;

  { TdxRtfParagraphFormattingInfo }

  TdxRtfParagraphFormattingInfo = class(TdxParagraphFormattingInfo)
  private
    FInTableParagraph: Boolean;
    FListLevelIndex: Integer;
    FNestingLevel: Integer;
    FNextStyle: Integer;
    FNumberingListIndex: TdxNumberingListIndex;
    FParentStyleIndex: Integer;
    FRtfLineSpacingMultiplier: Integer;
    FRtfLineSpacingType: Integer;
    FStyleIndex: Integer;
    FStyleLink: Integer;
    FTabAlignment: TdxTabAlignmentType;
    FTabLeader: TdxTabLeaderType;
    FTabs: TdxTabFormattingInfo;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure CopyFrom(const Source: TdxParagraphFormattingInfo); override;

    function CalcLineSpacing(AUnitConverter: TdxDocumentModelUnitConverter): Double;
    function CalcLineSpacingType: TdxParagraphLineSpacing;

    property InTableParagraph: Boolean read FInTableParagraph write FInTableParagraph;
    property ListLevelIndex: Integer read FListLevelIndex write FListLevelIndex;
    property NestingLevel: Integer read FNestingLevel write FNestingLevel;
    property NextStyle: Integer read FNextStyle write FNextStyle;
    property NumberingListIndex: TdxNumberingListIndex read FNumberingListIndex write FNumberingListIndex;
    property ParentStyleIndex: Integer read FParentStyleIndex write FParentStyleIndex;
    property RtfLineSpacingMultiplier: Integer read FRtfLineSpacingMultiplier write FRtfLineSpacingMultiplier;
    property RtfLineSpacingType: Integer read FRtfLineSpacingType write FRtfLineSpacingType;
    property StyleIndex: Integer read FStyleIndex write FStyleIndex;
    property StyleLink: Integer read FStyleLink write FStyleLink;
    property TabAlignment: TdxTabAlignmentType read FTabAlignment write FTabAlignment;
    property TabLeader: TdxTabLeaderType read FTabLeader write FTabLeader;
    property Tabs: TdxTabFormattingInfo read FTabs;
  end;

  { TdxKeywordTranslatorTable }

  TdxTranslateKeywordHandler = procedure(AImporter: TdxRichEditDocumentModelRtfImporter;
    AParameterValue: Integer; AHasParameter: Boolean);

  TdxKeywordTranslatorTable = class(TDictionary<string, TdxTranslateKeywordHandler>);

  { TdxControlCharTranslatorTable }

  TdxTranslateControlCharHandler = procedure(AImporter: TdxRichEditDocumentModelRtfImporter;
    var AChar: Char);

  TdxControlCharTranslatorTable = class(TDictionary<Char, TdxTranslateControlCharHandler>);

  { TdxRichEditRtfDestinationBase }

  TdxRichEditRtfDestinationBase = class
  const
    macCodePage = 10000;
    pcCodePage  = 437;
    pcaCodePage = 850;
  private
    FControlCharHT: TdxControlCharTranslatorTable;
    FDefaultControlCharHT: TdxControlCharTranslatorTable;
    FDefaultKeywordHT: TdxKeywordTranslatorTable;
    FImporter: TdxRichEditDocumentModelRtfImporter;
    FKeywordHT: TdxKeywordTranslatorTable;
    FNonEmpty: Boolean;
    FPieceTable: TdxPieceTable;

    // Keywords handlers
    class procedure ColorTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure FontTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure UserTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure AnsiKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure MacKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PcKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PcaKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure AnsiCodePageKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure HyphAutoKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;

    // ControlChar handlers
    class procedure OptionalGroupCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter; var AChar: Char); static;

    function CreateDefaultControlCharTable: TdxControlCharTranslatorTable;
    function CreateDefaultKeywordTable: TdxKeywordTranslatorTable;
  protected
    function CanAppendText: Boolean; virtual;
    function CanProcessDefaultKeyword: Boolean; virtual;
    function CreateClone: TdxRichEditRtfDestinationBase;
    function CreateControlCharTable: TdxControlCharTranslatorTable;
    function CreateKeywordTable: TdxKeywordTranslatorTable;
    function GetControlCharHT: TdxControlCharTranslatorTable; virtual;
    function GetKeywordHT: TdxKeywordTranslatorTable; virtual;
    procedure IncreaseGroupLevel; virtual;
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); virtual;
    procedure PopulateControlCharTable(ATable: TdxControlCharTranslatorTable); virtual;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); virtual;

    procedure ProcessBinCharCore(AChar: Char); virtual;
    procedure ProcessCharCore(AChar: Char); virtual;
    procedure ProcessControlCharCore(AChar: Char); virtual;
    function ProcessKeywordCore(const AKeyword: string; AParameterValue: Integer; AHasParameter: Boolean): Boolean; virtual;
    procedure ProcessTextCore(const AText: string); virtual;

    procedure BeforeNestedGroupFinishedCore(ADestination: TdxRichEditRtfDestinationBase); virtual;

    // handlers
    class procedure BinKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
      AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure EscapedCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter; var AChar: Char); static;
    class procedure SwitchToHexCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter; var AChar: Char); static;
    class procedure UnicodeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
      AParameterValue: Integer; AHasParameter: Boolean); static;

    property ControlCharHT: TdxControlCharTranslatorTable read GetControlCharHT;
    property DefaultControlCharHT: TdxControlCharTranslatorTable read FDefaultControlCharHT;
    property DefaultKeywordHT: TdxKeywordTranslatorTable read FDefaultKeywordHT;
    property Importer: TdxRichEditDocumentModelRtfImporter read FImporter;
    property KeywordHT: TdxKeywordTranslatorTable read GetKeywordHT;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); virtual;
    destructor Destroy; override;

    procedure ProcessBinChar(AChar: Char);
    procedure ProcessChar(AChar: Char);
    procedure ProcessControlChar(AChar: Char);
    function ProcessKeyword(const AKeyword: string; AParameterValue: Integer; AHasParameter: Boolean): Boolean;
    procedure ProcessText(const AText: string);

    procedure AfterPopRtfState; virtual;
    procedure BeforePopRtfState; virtual;
    procedure BeforeNestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
    procedure NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase); virtual;

    property NonEmpty: Boolean read FNonEmpty;
    property PieceTable: TdxPieceTable read FPieceTable;
  end;
  TdxRichEditRtfDestinationBaseClass = class of TdxRichEditRtfDestinationBase;

  { TdxRichEditRtfPieceTableInfo }

  TdxRichEditRtfPieceTableInfo = class(TdxRichEditImportPieceTableInfoBase)
  private
    FCharacterFormattingOptions: TdxCharacterFormattingOptions;
    FPosition: TdxRtfInputPosition;
    FTableReader: TdxRtfTableReader;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter; APieceTable: TdxPieceTable);
    destructor Destroy; override;

    property Position: TdxRtfInputPosition read FPosition;
    property TableReader: TdxRtfTableReader read FTableReader;
  end;

  TdxRichEditRtfPieceTableInfos = class(TdxFastObjectStack)
  private
    function GetItem(Index: Integer): TdxRichEditRtfPieceTableInfo;
    procedure SetItem(Index: Integer; const Value: TdxRichEditRtfPieceTableInfo);
  public
    property Items[Index: Integer]: TdxRichEditRtfPieceTableInfo read GetItem write SetItem; default;
  end;

  { TdxCharacterDecoder }

  TdxCharacterDecoder = class
  private
    FEncoding: TEncoding;
  public
    constructor Create(ACodePage: Cardinal); virtual;
    destructor Destroy; override;

    property Encoding: TEncoding read FEncoding;
  end;

  { TdxCodePageCharacterDecoder }

  TdxCodePageCharacterDecoder = class(TdxCharacterDecoder)
  private
    FBytes: TBytes;
    FCapacity: Integer;
    FCount: Integer;
    procedure AddByte(Value: Byte);
    procedure Clear;
    procedure FlushByChar(AImporter: TdxRichEditDocumentModelRtfImporter; const AChars: TCharArray);
    procedure FlushByString(AImporter: TdxRichEditDocumentModelRtfImporter; const AChars: TCharArray);
  public
    constructor Create(ACodePage: Cardinal); override;
    destructor Destroy; override;

    procedure ProcessChar(AImporter: TdxRichEditDocumentModelRtfImporter; AChar: Char); virtual;
    procedure Flush(AImporter: TdxRichEditDocumentModelRtfImporter); virtual;
  end;

  { TdxEmptyCharacterDecoder }

  TdxEmptyCharacterDecoder = class(TdxCodePageCharacterDecoder)
  public
    constructor Create; reintroduce;
    procedure ProcessChar(AImporter: TdxRichEditDocumentModelRtfImporter; AChar: Char); override;
    procedure Flush(AImporter: TdxRichEditDocumentModelRtfImporter); override;
  end;

  { TdxRtfFormattingInfo }

  TdxRtfFormattingInfo = class(TcxIUnknownObject, IdxCloneable<TdxRtfFormattingInfo>, IdxSupportsCopyFrom<TdxRtfFormattingInfo>)
  private
    FCodePage: Cardinal;
    FDecoder: TdxCodePageCharacterDecoder;
    FParentStyleIndex: Integer;
    FUnicodeCharacterByteCount: Integer;
    procedure SetCodePage(Value: Cardinal);
    procedure SetUnicodeCharacterByteCount(Value: Integer);
  protected
    function ChooseDecoder: TdxCodePageCharacterDecoder; virtual;
    procedure SetDecoder(Value: TdxCodePageCharacterDecoder; ANeedDestroyOldDecoder: Boolean = True);
  public
    constructor Create;
    destructor Destroy; override;
    procedure CopyFrom(const Source: TdxRtfFormattingInfo);

    function Clone: TdxRtfFormattingInfo;

    property CodePage: Cardinal read FCodePage write SetCodePage;
    property Decoder: TdxCodePageCharacterDecoder read FDecoder;
    property ParentStyleIndex: Integer read FParentStyleIndex write FParentStyleIndex;
    property UnicodeCharacterByteCount: Integer read FUnicodeCharacterByteCount write SetUnicodeCharacterByteCount;
  end;
  TdxRtfFormattingInfoClass = class of TdxRtfFormattingInfo;

  { TdxRtfOldListLevelInfo }

  TdxRtfOldListLevelInfo = class(TcxIUnknownObject, IdxCloneable<TdxRtfOldListLevelInfo>, IdxSupportsCopyFrom<TdxRtfOldListLevelInfo>)
  private
    FCharacterProperties: TdxCharacterFormattingInfo;
    FDocumentModel: TdxDocumentModel;
    FIncludeInformationFromPreviousLevel: Boolean;
    FIndent: Integer;
    FListLevelProperties: TdxListLevelInfo;
    FSkipNumbering: Boolean;
    FTextAfter: string;
    FTextBefore: string;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);
    destructor Destroy; override;
    procedure CopyFrom(const Source: TdxRtfOldListLevelInfo);
    function Clone: TdxRtfOldListLevelInfo;

    property CharacterProperties: TdxCharacterFormattingInfo read FCharacterProperties;
    property IncludeInformationFromPreviousLevel: Boolean read FIncludeInformationFromPreviousLevel write FIncludeInformationFromPreviousLevel;
    property Indent: Integer read FIndent write FIndent;
    property ListLevelProperties: TdxListLevelInfo read FListLevelProperties;
    property SkipNumbering: Boolean read FSkipNumbering write FSkipNumbering;
    property TextAfter: string read FTextAfter write FTextAfter;
    property TextBefore: string read FTextBefore write FTextBefore;
  end;
  TdxRtfOldListLevelInfoClass = class of TdxRtfOldListLevelInfo;

  { TdxRtfOldListLevelInfoCollection }

  TdxRtfOldListLevelInfoCollection = class(TdxFastObjectList)
  private
    FDocumentModel: TdxDocumentModel;
    function GetItem(Index: Integer): TdxRtfOldListLevelInfo;
    procedure SetItem(Index: Integer; const Value: TdxRtfOldListLevelInfo);
  public
    constructor Create(ADocumentModel: TdxDocumentModel);

    function Clone: TdxRtfOldListLevelInfoCollection;
    procedure CopyFrom(ASource: TdxRtfOldListLevelInfoCollection);

    property Items[Index: Integer]: TdxRtfOldListLevelInfo read GetItem write SetItem; default;
  end;
  TdxRtfOldListLevelInfoCollectionClass = class of TdxRtfOldListLevelInfoCollection;

  { TdxRtfInputPositionState }

  TdxRtfInputPositionState = class
  private
    FCharacterFormatting: TdxCharacterFormattingBase;
    FCharacterStyleIndex: Integer;
    FCurrentOldListLevelNumber: Integer;
    FCurrentOldListSkipNumbering: Boolean;
    FCurrentOldMultiLevelListIndex: TdxNumberingListIndex;
    FCurrentOldSimpleList: Boolean;
    FCurrentOldSimpleListIndex: TdxNumberingListIndex;
    FDoubleByteCharactersFontName: string;
    FIsDoubleByteCharactersFont: Boolean;
    FParagraphFormattingInfo: TdxRtfParagraphFormattingInfo;
    FRtfFormattingInfo: TdxRtfFormattingInfo;
    FOldListLevelInfo: TdxRtfOldListLevelInfo;
    FOldListLevelInfoCollection: TdxRtfOldListLevelInfoCollection;
    FTableStyleIndex: Integer;
  public
    constructor Create(APosition: TdxRtfInputPosition);
    destructor Destroy; override;

    property CharacterFormatting: TdxCharacterFormattingBase read FCharacterFormatting;
    property CharacterStyleIndex: Integer read FCharacterStyleIndex;
    property CurrentOldListLevelNumber: Integer read FCurrentOldListLevelNumber;
    property CurrentOldListSkipNumbering: Boolean read FCurrentOldListSkipNumbering;
    property CurrentOldMultiLevelListIndex: TdxNumberingListIndex read FCurrentOldMultiLevelListIndex;
    property CurrentOldSimpleList: Boolean read FCurrentOldSimpleList write FCurrentOldSimpleList;
    property CurrentOldSimpleListIndex: TdxNumberingListIndex read FCurrentOldSimpleListIndex;
    property DoubleByteCharactersFontName: string read FDoubleByteCharactersFontName;
    property IsDoubleByteCharactersFont: Boolean read FIsDoubleByteCharactersFont;
    property ParagraphFormattingInfo: TdxRtfParagraphFormattingInfo read FParagraphFormattingInfo;
    property RtfFormattingInfo: TdxRtfFormattingInfo read FRtfFormattingInfo;
    property OldListLevelInfo: TdxRtfOldListLevelInfo read FOldListLevelInfo;
    property OldListLevelInfoCollection: TdxRtfOldListLevelInfoCollection read FOldListLevelInfoCollection;
    property TableStyleIndex: Integer read FTableStyleIndex;
  end;

  { TdxRtfSectionFormattingInfo }

  TdxRtfSectionFormattingInfo = class(TPersistent)
  private
    FColumns: TdxColumnsInfo;
    FGeneralSectionInfo: TdxGeneralSectionInfo;
    FFootNote: TdxSectionFootNote;
    FEndNote: TdxSectionFootNote;
    FLineNumbering: TdxLineNumberingInfo;
    FMargins: TdxMarginsInfo;
    FPage: TdxPageInfo;
    FPageNumbering: TdxPageNumberingInfo;
    FRestartPageNumbering: Boolean;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    property Columns: TdxColumnsInfo read FColumns;
    property EndNote: TdxSectionFootNote read FEndNote;
    property GeneralSectionInfo: TdxGeneralSectionInfo read FGeneralSectionInfo;
    property FootNote: TdxSectionFootNote read FFootNote;
    property LineNumbering: TdxLineNumberingInfo read FLineNumbering;
    property Margins: TdxMarginsInfo read FMargins;
    property Page: TdxPageInfo read FPage;
    property PageNumbering: TdxPageNumberingInfo read FPageNumbering;
    property RestartPageNumbering: Boolean read FRestartPageNumbering write FRestartPageNumbering;
  end;

  { TdxRtfInputPosition }

  TdxRtfInputPosition = class(TdxInputPosition)
  private
    FCurrentOldListLevelNumber: Integer;
    FCurrentOldListSkipNumbering: Boolean;
    FCurrentOldMultiLevelListIndex: TdxNumberingListIndex;
    FCurrentOldSimpleList: Boolean;
    FCurrentOldSimpleListIndex: TdxNumberingListIndex;
    FDoubleByteCharactersFontName: string;
    FIsDoubleByteCharactersFont: Boolean;
    FOldListLevelInfo: TdxRtfOldListLevelInfo;
    FOldListLevelInfoCollection: TdxRtfOldListLevelInfoCollection;
    FParagraphFormattingInfo: TdxRtfParagraphFormattingInfo;
    FRtfFormattingInfo: TdxRtfFormattingInfo;
    FSectionFormattingInfo: TdxRtfSectionFormattingInfo;
    FTableStyleIndex: Integer;
    procedure SetParagraphFormattingInfo(const Value: TdxRtfParagraphFormattingInfo);
    procedure SetRtfFormattingInfo(const Value: TdxRtfFormattingInfo);
  public
    constructor Create(APieceTable: TdxPieceTable); override;
    destructor Destroy; override;

    function GetState: TdxRtfInputPositionState;
    procedure SetState(AState: TdxRtfInputPositionState);

    property CurrentOldMultiLevelListIndex: TdxNumberingListIndex read FCurrentOldMultiLevelListIndex write FCurrentOldMultiLevelListIndex;
    property CurrentOldSimpleListIndex: TdxNumberingListIndex read FCurrentOldSimpleListIndex write FCurrentOldSimpleListIndex;
    property CurrentOldListSkipNumbering: Boolean read FCurrentOldListSkipNumbering write FCurrentOldListSkipNumbering;
    property CurrentOldSimpleList: Boolean read FCurrentOldSimpleList write FCurrentOldSimpleList;
    property CurrentOldListLevelNumber: Integer read FCurrentOldListLevelNumber write FCurrentOldListLevelNumber;
    property DoubleByteCharactersFontName: string read FDoubleByteCharactersFontName write FDoubleByteCharactersFontName;
    property OldListLevelInfo: TdxRtfOldListLevelInfo read FOldListLevelInfo write FOldListLevelInfo;
    property OldListLevelInfoCollection: TdxRtfOldListLevelInfoCollection read FOldListLevelInfoCollection;
    property ParagraphFormattingInfo: TdxRtfParagraphFormattingInfo read FParagraphFormattingInfo write SetParagraphFormattingInfo;
    property RtfFormattingInfo: TdxRtfFormattingInfo read FRtfFormattingInfo write SetRtfFormattingInfo;
    property SectionFormattingInfo: TdxRtfSectionFormattingInfo read FSectionFormattingInfo;
    property TableStyleIndex: Integer read FTableStyleIndex write FTableStyleIndex;
    property IsDoubleByteCharactersFont: Boolean read FIsDoubleByteCharactersFont write FIsDoubleByteCharactersFont;
  end;

  { TdxRichEditRtfParserStateItem }

  TdxRichEditRtfParserStateItem = class
  private
    FDestination: TdxRichEditRtfDestinationBase;
    FInputPositionState: TdxRtfInputPositionState;
  public
    constructor Create(AInputPositionState: TdxRtfInputPositionState; ADestination: TdxRichEditRtfDestinationBase);

    property Destination: TdxRichEditRtfDestinationBase read FDestination;
    property InputPositionState: TdxRtfInputPositionState read FInputPositionState;
  end;

  { TdxRichEditRtfParserStateItems }

  TdxRichEditRtfParserStateItems = class(TdxFastObjectStack)
  private
    function GetItem(Index: Integer): TdxRichEditRtfParserStateItem;
    procedure SetItem(Index: Integer; const Value: TdxRichEditRtfParserStateItem);
  public
    property Items[Index: Integer]: TdxRichEditRtfParserStateItem read GetItem write SetItem; default;
  end;

  { TdxRichEditRtfParserStateManager }

  TdxRichEditRtfParserStateManager = class
  private
    FDestination: TdxRichEditRtfDestinationBase;
    FSuperfluousDestinations: TdxFastObjectList;
    FImporter: TdxRichEditDocumentModelRtfImporter;
    FParsingState: TdxRichEditRtfParsingState;
    FPieceTables: TdxRichEditRtfPieceTableInfos;
    FStates: TdxRichEditRtfParserStateItems;
    function GetPieceTableInfo: TdxRichEditRtfPieceTableInfo;
    procedure SetDestination(const Value: TdxRichEditRtfDestinationBase);
  protected
    procedure CheckSuperfluousDestinations;
    function CreateDefaultDestination: TdxRichEditRtfDestinationBase;
    procedure Initialize;
    procedure SetDestinationCore(Value: TdxRichEditRtfDestinationBase; AUpdatePieceTableInfo: Boolean);

    property PieceTables: TdxRichEditRtfPieceTableInfos read FPieceTables;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter);
    destructor Destroy; override;

    procedure PopState;
    procedure PushState;

    property Destination: TdxRichEditRtfDestinationBase read FDestination write SetDestination;
    property Importer: TdxRichEditDocumentModelRtfImporter read FImporter;
    property ParsingState: TdxRichEditRtfParsingState read FParsingState write FParsingState;
    property PieceTableInfo: TdxRichEditRtfPieceTableInfo read GetPieceTableInfo;
    property States: TdxRichEditRtfParserStateItems read FStates;
  end;

  { TdxListLevelDisplayTextHelper }

  TdxListLevelDisplayTextHelper = class
  public
    class function CreateDisplayFormatStringCore(APlaceholderIndices: TList<Integer>; const AText: string): string; static;
    class function GetTextRange(APlaceholderIndices: TList<Integer>; AStartPlaceHolderIndex: Integer; const AText: string): string; static;
  end;

  { TdxRtfNumberingList }

  TdxRtfListID = Integer;
  TdxRtfNumberingListType = (Unknown, Hybrid, Simple);

  TdxRtfListLevel = class(TdxListLevel)
  private
    FText: string;
    FNumber: string;
  protected
    function CreatePlaceholderIndices: TList<Integer>; virtual;
    function CreateDisplayFormatStringCore(APlaceholderIndices: TList<Integer>): string; virtual;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); override;
    function CreateDisplayFormatString: string;

    procedure CopyFrom(AListLevel: TdxRtfListLevel); reintroduce;
    function Clone: TdxRtfListLevel; reintroduce;

    property Text: string read FText write FText;
    property Number: string read FNumber write FNumber;
  end;

  TdxRtfListLevels = class(TObjectList<TdxRtfListLevel>)
  public
    procedure CopyFrom(Value: TdxRtfListLevels);
  end;

  TdxRtfNumberingList = class(TPersistent)
  private
    FID: TdxRtfListID;
    FLevels: TdxRtfListLevels;
    FName: string;
    FNumberingListType: TdxRtfNumberingListType;
    FParentStyleID: TdxRtfListID;
    FStyleName: string;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    procedure Reset;

    property ID: TdxRtfListID read FID write FID;
    property Levels: TdxRtfListLevels read FLevels;
    property Name: string read FName write FName;
    property NumberingListType: TdxRtfNumberingListType read FNumberingListType write FNumberingListType;
    property ParentStyleID: TdxRtfListID read FParentStyleID write FParentStyleID;
    property StyleName: string read FStyleName write FStyleName;
  end;

  TdxRtfListTable = TObjectList<TdxRtfNumberingList>;

  TdxRtfListOverrideId = Integer;

  TdxRtfListOverrideLevel = class
  private
    FDocumentModel: TdxDocumentModel;
    FLevel: TdxRtfListLevel;
    FOverrideFormat: Boolean;
    FOverrideStartAt: Boolean;
    FStartAt: Integer;
    procedure SetLevel(const Value: TdxRtfListLevel);
  public
    constructor Create(ADocumentModel: TdxDocumentModel);

    procedure CopyFrom(ALevel: TdxRtfListOverrideLevel);
    function Clone: TdxRtfListOverrideLevel;

    property Level: TdxRtfListLevel read FLevel write SetLevel;
    property OverrideFormat: Boolean read FOverrideFormat write FOverrideFormat;
    property OverrideStartAt: Boolean read FOverrideStartAt write FOverrideStartAt;
    property StartAt: Integer read FStartAt write FStartAt;
  end;

  TdxRtfListOverrideLevels = TObjectList<TdxRtfListOverrideLevel>;

  TdxRtfNumberingListOverride = class
  strict private
    FID: TdxRtfListOverrideId;
    FListId: TdxRtfListId;
    FLevels: TdxRtfListOverrideLevels;
  public
    constructor Create;
    destructor Destroy; override;

    property ID: TdxRtfListOverrideId read FID write FID;
    property Levels: TdxRtfListOverrideLevels read FLevels;
    property ListId: TdxRtfListId read FListId write FListId;
  end;

  TdxListOverrideTable = TObjectList<TdxRtfNumberingListOverride>;

  { TdxRtfDocumentProperties }

  TdxRtfDocumentProperties = class
  private
    FColors: TList<TColor>;
    FDefaultCodePage: Integer;
    FDefaultFontNumber: Integer;
    FDefaultSectionProperties: TdxRtfSectionFormattingInfo;
    FFonts: TdxRtfFontInfoCollection;
    FListOverrideTable: TdxListOverrideTable;
    FListTable: TdxRtfListTable;
    FListTableComplete: Boolean;
    FListOverrideTableComplete: Boolean;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);
    destructor Destroy; override;

    property Colors: TList<TColor> read FColors;
    property DefaultCodePage: Integer read FDefaultCodePage write FDefaultCodePage;
    property DefaultFontNumber: Integer read FDefaultFontNumber write FDefaultFontNumber;
    property DefaultSectionProperties: TdxRtfSectionFormattingInfo read FDefaultSectionProperties;
    property Fonts: TdxRtfFontInfoCollection read FFonts;
    property ListOverrideTable: TdxListOverrideTable read FListOverrideTable;
    property ListTable: TdxRtfListTable read FListTable;
    property ListTableComplete: Boolean read FListTableComplete write FListTableComplete;
    property ListOverrideTableComplete: Boolean read FListOverrideTableComplete write FListOverrideTableComplete;
  end;

  TdxIntegerDictionary = TDictionary<Integer, Integer>;

  { TdxRtfNumberingListInfo }

  TdxRtfNumberingListInfo = class
  private
    FRtfNumberingListIndex: Integer;
    FListLevelIndex: Integer;
  public
    constructor Create(ARtfNumberingListIndex, AListLevelIndex: Integer);
    property RtfNumberingListIndex: Integer read FRtfNumberingListIndex;
    property ListLevelIndex: Integer read FListLevelIndex;
  end;

  { TdxRichEditDocumentModelRtfImporter }

  TdxRichEditDocumentModelRtfImporter = class(TdxRichEditDocumentModelImporter)
  private
    FBinCharCount: Integer;
    FCharacterStyleCollectionIndex: TdxIntegerDictionary;
    FDocumentProperties: TdxRtfDocumentProperties;
    FKeyword: TStringBuilder;
    FLastParagraphFormattingCacheIndex: Integer;
    FLastParagraphPropertiesCacheIndex: Integer;
    FLinkParagraphStyleIndexToCharacterStyleIndex: TdxIntegerDictionary;
    FListOverrideIndexToNumberingListIndexMap: TDictionary<Integer, TdxNumberingListIndex>;
    FNextParagraphStyleIndexTable: TdxIntegerDictionary;
    FOptionalGroupLevel: Integer;
    FParagraphStyleCollectionIndex: TdxIntegerDictionary;
    FParagraphStyleListOverrideIndexMap: TObjectDictionary<TdxParagraphStyle, TdxRtfNumberingListInfo>;
    FParameterValueString: TStringBuilder;
    FSkipCount: Integer;
    FStateManager: TdxRichEditRtfParserStateManager;
    FStream: TStream;
    FTableStyleCollectionIndex: TdxIntegerDictionary;

    function CheckSignature(var AStream: TStream): Boolean;
    procedure ClearNumberingList;
    procedure DecreaseSkipCount;
    function ReadByte(AStream: TStream; out AByte: Byte): Boolean; inline;
    function ReadChar(AStream: TStream; out AChar: Char): Boolean; inline;
    procedure SkipDataBeyondOuterBrace(AStream: TStream);

    procedure ParseBinChar(AChar: Char);
    procedure ParseChar(AChar: Char);
    procedure ParseHexChar(AStream: TStream; AChar: Char);

    function GetDestination: TdxRichEditRtfDestinationBase;
    function GetPieceTable: TdxPieceTable;
    function GetPieceTableInfo: TdxRichEditRtfPieceTableInfo;
    function GetPosition: TdxRtfInputPosition;
    procedure SetDestination(const Value: TdxRichEditRtfDestinationBase);
    function GetOptions: TdxRtfDocumentImporterOptions;
    function GetTableReader: TdxRtfTableReader;
  protected
    procedure AddNumberingListToParagraph(AParagraph: TdxParagraph; AListIndex: TdxNumberingListIndex; ALevelIndex: Integer);
    procedure ApplyParagraphFormattingCore(AParagraph: TdxParagraph; AParagraphFormatting: TdxRtfParagraphFormattingInfo);
    procedure ConvertListLevelsToAnotherType(AListIndex: TdxNumberingListIndex);
    procedure ConvertListLevelsToAnotherTypeCore(ALevels: IdxReadOnlyIListLevelCollection; const ADisplayFormat, AFontName: string; AFormat: TdxNumberingFormat); 
    procedure LinkParagraphStyleWithNumberingLists(AStyle: TdxParagraphStyle); virtual;
    procedure InitializeStateManager;
    procedure ImportCore(AStream: TStream); override;
    procedure ImportRtfStream(AStream: TStream);
    class function IsAlpha(C: Char): Boolean; static; inline;
    class function IsDigit(C: Char): Boolean; static; inline;
    procedure ParseCR;
    procedure ParseLF;
    function ParseRtfKeyword(AStream: TStream): Char;
    function PopRtfState: TdxRichEditPopRtfState; virtual;
    procedure PushRtfState;
    procedure TranslateControlChar(AChar: Char); virtual;
    procedure TranslateKeyword(const AKeyword: string; AParameterValue: Integer; AHasParameter: Boolean);

    property OptionalGroupLevel: Integer read FOptionalGroupLevel write FOptionalGroupLevel;
    property StateManager: TdxRichEditRtfParserStateManager read FStateManager;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AOptions: TdxDocumentImporterOptions); override;
    destructor Destroy; override;

    class procedure ThrowInvalidRtfFile;

    procedure ApplyCharacterProperties(ACharacterProperties: TdxCharacterProperties;
      ACharacterFormattingInfo: TdxCharacterFormattingInfo; AParentCharacterFormatting: TdxMergedCharacterProperties;
      AResetUse: Boolean = True);
    procedure ApplyFormattingToLastParagraph;
    procedure ApplyParagraphProperties(AParagraphProperties: TdxParagraphProperties;
      AParagraphFormatting: TdxParagraphFormattingInfo;
      AParentParagraphProperties: TdxMergedParagraphProperties;
      AResetUse: Boolean = True); overload;
    procedure ApplyParagraphProperties(AParagraphProperties: TdxParagraphProperties;
      AParentParagraphRtfStyleIndex: Integer;
      AParagraphFormatting: TdxRtfParagraphFormattingInfo); overload;
    procedure ApplyTableProperties(ATableProperties: TdxTableProperties; AParentTableProperties: TdxMergedTableProperties);
    procedure ApplyTableRowProperties(ARowProperties: TdxTableRowProperties; AParentRowProperties: TdxMergedTableRowProperties);
    procedure ApplyTableCellProperties(ACellProperties: TdxTableCellProperties; AParentCellProperties: TdxMergedTableCellProperties);
    procedure ApplyParagraphFormatting(AParagraphIndex: TdxParagraphIndex);
    procedure ApplySectionFormatting(ASkipNumbering: Boolean);
    procedure FlushDecoder;
    procedure LinkParagraphStylesWithNumberingLists; virtual;
    procedure ParseUnicodeChar(AChar: WideChar);
    procedure ParseCharWithoutDecoding(AChar: WideChar);
    procedure SetCodePage(ACodePage: Cardinal);
    procedure SetFont(AFontInfo: TdxRtfFontInfo);

    function GetCharacterStyleIndex(ARtfCharacterStyleIndex: Integer): Integer;
    function GetParagraphStyleIndex(ARtfParagraphStyleIndex: Integer): Integer;
    function GetStyleMergedCharacterProperties(ARtfStyleIndex: Integer): TdxMergedCharacterProperties; overload;
    function GetStyleMergedCharacterProperties(AParentMergedProperties: TdxMergedCharacterProperties): TdxMergedCharacterProperties; overload;
    function GetStyleMergedParagraphCharacterProperties(ARtfCharacterStyleIndex, ARtfParagraphStyleIndex: Integer): TdxMergedCharacterProperties; overload;
    function GetStyleMergedParagraphProperties(ARtfStyleIndex: Integer): TdxMergedParagraphProperties; overload;
    function GetStyleMergedParagraphProperties(AParentMergedProperties: TdxMergedParagraphProperties): TdxMergedParagraphProperties; overload;
    function GetStyleMergedTableRowProperties(AParentMergedProperties: TdxMergedTableRowProperties): TdxMergedTableRowProperties;
    function GetStyleMergedTableCellProperties(AParentMergedProperties: TdxMergedTableCellProperties): TdxMergedTableCellProperties;
    function GetTableStyleIndex(ARtfTableStyleIndex: Integer): Integer;
    procedure InsertParagraph;

    property CharacterStyleCollectionIndex: TdxIntegerDictionary read FCharacterStyleCollectionIndex;
    property Destination: TdxRichEditRtfDestinationBase read GetDestination write SetDestination;
    property DocumentProperties: TdxRtfDocumentProperties read FDocumentProperties;
    property LinkParagraphStyleIndexToCharacterStyleIndex: TdxIntegerDictionary read FLinkParagraphStyleIndexToCharacterStyleIndex;
    property ListOverrideIndexToNumberingListIndexMap: TDictionary<Integer, TdxNumberingListIndex> read FListOverrideIndexToNumberingListIndexMap;
    property NextParagraphStyleIndexTable: TdxIntegerDictionary read FNextParagraphStyleIndexTable;
    property Options: TdxRtfDocumentImporterOptions read GetOptions;
    property ParagraphStyleCollectionIndex: TdxIntegerDictionary read FParagraphStyleCollectionIndex;
    property ParagraphStyleListOverrideIndexMap: TObjectDictionary<TdxParagraphStyle, TdxRtfNumberingListInfo> read FParagraphStyleListOverrideIndexMap;
    property PieceTable: TdxPieceTable read GetPieceTable;
    property PieceTableInfo: TdxRichEditRtfPieceTableInfo read GetPieceTableInfo;
    property Position: TdxRtfInputPosition read GetPosition;
    property TableReader: TdxRtfTableReader read GetTableReader;
    property TableStyleCollectionIndex: TdxIntegerDictionary read FTableStyleCollectionIndex;
  end;

function dxHexToInt(AChar: Char): Integer;

implementation

  {.$DEFINE AQTIME}

uses
{$IFDEF AQTIME}
  AQtimeHelpers,
{$ENDIF}
  dxRichEdit.Utils.BatchUpdateHelper, RTLConsts, 
  Windows, Math, dxRichEdit.Import.Rtf.DestinationSkip, dxRichEdit.Import.Rtf.DestinationDefault,
  dxRichEdit.Import.Rtf.DestinationFontTable, dxRichEdit.Import.Rtf.DestinationColorTable,
  dxRichEdit.Platform.Font, dxRichEdit.DocumentModel.ParagraphRange, cxClasses, Character,
  dxRichEdit.Utils.Characters, dxRichEdit.Utils.Encoding, dxRichEdit.Utils.StringHelper,
  dxRichEdit.DocumentModel.Borders;

function dxHexToInt(AChar: Char): Integer;
begin
  if CharInSet(AChar, ['0'..'9']) then
    Result := Ord(AChar) - Ord('0')
  else
  begin
    if CharInSet(AChar, ['a'..'z']) then
      Result := 10 + Ord(AChar) - Ord('a')
    else
      if CharInSet(AChar, ['A'..'Z']) then
        Result := 10 + Ord(AChar) - Ord('A')
      else
        Result := 0;
  end;
end;

{ TdxRtfImageInfo }

constructor TdxRtfImageInfo.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
end;

procedure TdxRtfImageInfo.LoadMetafileFromStream(AStream: TMemoryStream;
  AMapMode: TdxMapMode; APictureWidth, APictureHeight: Integer);
begin
  CreateImage(TdxMetafileHelper.CreateMetafile(AStream, AMapMode, APictureWidth, APictureHeight));
end;

procedure TdxRtfImageInfo.SetRtfImage(const Value: TdxOfficeImageReference);
begin
  FRtfImage.Free;
  FRtfImage := Value;
end;

procedure TdxRtfImageInfo.LoadImageFromStream(AStream: TStream);
var
  AImage: TdxOfficeImage;
begin
  AImage := TdxOfficeImage.Create;
  try
    AImage.LoadFromStream(AStream);
  except
    FreeAndNil(AImage);
  end;
  if AImage.Empty then
    FreeAndNil(AImage);
  if AImage <> nil then
    CreateImage(AImage);
end;

procedure TdxRtfImageInfo.LoadDibFromStream(AStream: TStream;
  AWidth, AHeight, ABytesInLine: Integer);
begin
  Assert(False, 'not implemented');
end;

procedure TdxRtfImageInfo.CreateImage(ANativeImage: TdxOfficeImage);
begin
  RtfImage := DocumentModel.CreateImage(ANativeImage);
end;

destructor TdxRtfImageInfo.Destroy;
begin
  FreeAndNil(FRtfImage);
  inherited Destroy;
end;

procedure TdxRtfImageInfo.LoadBitmapFromStream(AStream: TMemoryStream;
  AWidth, AHeight, AColorPlanesCount, ABitsPerPixel, ABytesInLine: Integer);
begin
  Assert(False, 'not implemented');
end;

{ TdxRtfFontInfo }

constructor TdxRtfFontInfo.Create;
begin
  inherited Create;
  FCharset := -1;
end;

{ TdxRtfFontInfoCollection }

constructor TdxRtfFontInfoCollection.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create(True);
  FDefaultRtfFontInfo := CreateDefaultRtfFontInfo(ADocumentModel);
end;

function TdxRtfFontInfoCollection.GetRtfFontInfoById(AID: Integer): TdxRtfFontInfo;
var
  I: Integer;
begin
  Result := FDefaultRtfFontInfo;
  for I := 0 to Count - 1 do
  begin
    if Items[I].ID = AID then
    begin
      Result := Items[I];
      Break;
    end;
  end;

end;

function TdxRtfFontInfoCollection.CreateDefaultRtfFontInfo(ADocumentModel: TdxDocumentModel): TdxRtfFontInfo;
begin
  Result := TdxRtfFontInfo.Create;
  Result.Name := 'Times New Roman';
  Result.ID := MaxInt;
end;

destructor TdxRtfFontInfoCollection.Destroy;
begin
  FreeAndNil(FDefaultRtfFontInfo);
  inherited Destroy;
end;

function TdxRtfFontInfoCollection.GetItem(Index: Integer): TdxRtfFontInfo;
begin
  Result := TdxRtfFontInfo(inherited Items[Index]);
end;

procedure TdxRtfFontInfoCollection.SetItem(Index: Integer;
  const Value: TdxRtfFontInfo);
begin
  inherited Items[Index] := Value;
end;

{ TdxRtfParagraphFormattingInfo }

constructor TdxRtfParagraphFormattingInfo.Create;
begin
  inherited Create;
  FTabs := TdxTabFormattingInfo.Create;
  FRtfLineSpacingMultiplier := 1;
  FStyleLink := -1;
  FNextStyle := -1;
  FNumberingListIndex := NumberingListIndexNoNumberingList;
  WidowOrphanControl := True;
end;

destructor TdxRtfParagraphFormattingInfo.Destroy;
begin
  FreeAndNil(FTabs);
  inherited Destroy;
end;

procedure TdxRtfParagraphFormattingInfo.CopyFrom(const Source: TdxParagraphFormattingInfo);
var
  ASource: TdxRtfParagraphFormattingInfo;
begin
  if Source is TdxRtfParagraphFormattingInfo then
  begin
    ASource := TdxRtfParagraphFormattingInfo(Source);
    InTableParagraph := ASource.InTableParagraph;
    ListLevelIndex := ASource.ListLevelIndex;
    NestingLevel := ASource.NestingLevel;
    NextStyle := ASource.NextStyle;
    NumberingListIndex := ASource.NumberingListIndex;
    RtfLineSpacingType := ASource.RtfLineSpacingType;
    RtfLineSpacingMultiplier := ASource.RtfLineSpacingMultiplier;
    ParentStyleIndex := ASource.ParentStyleIndex;
    StyleIndex := ASource.StyleIndex;
    StyleLink := ASource.StyleLink;
    TabAlignment := ASource.TabAlignment;
    TabLeader := ASource.TabLeader;
    Tabs.CopyFrom(ASource.Tabs);
  end;
  inherited CopyFrom(Source);
end;

function TdxRtfParagraphFormattingInfo.CalcLineSpacing(AUnitConverter: TdxDocumentModelUnitConverter): Double;
begin
  if RtfLineSpacingMultiplier < 0 then
    Result := 0
  else
    if RtfLineSpacingMultiplier = 0 then
      Result := IfThen(RtfLineSpacingType <> 0, Max(AUnitConverter.TwipsToModelUnits(Abs(RtfLineSpacingType)), 1), 0)
    else
      if RtfLineSpacingType < 0 then
        Result := 0
      else
        Result := RtfLineSpacingType / 240;
end;

function TdxRtfParagraphFormattingInfo.CalcLineSpacingType: TdxParagraphLineSpacing;
begin
  if RtfLineSpacingMultiplier < 0 then
    Result := TdxParagraphLineSpacing.Single
  else
  begin
    if RtfLineSpacingMultiplier = 0 then
    begin
      if RtfLineSpacingType = 0 then
        Result := TdxParagraphLineSpacing.Single
      else
        if RtfLineSpacingType > 0 then
          Result := TdxParagraphLineSpacing.AtLeast
        else
          Result := TdxParagraphLineSpacing.Exactly;
    end
    else
    begin
      case RtfLineSpacingType of
        240: Result := TdxParagraphLineSpacing.Single;
        360: Result := TdxParagraphLineSpacing.Sesquialteral;
        480: Result := TdxParagraphLineSpacing.Double;
      else
        if RtfLineSpacingType <= 0 then
          Result := TdxParagraphLineSpacing.Single
        else
          Result := TdxParagraphLineSpacing.Multiple;
      end;
    end;
  end;
end;

{ TdxRichEditRtfDestinationBase }

constructor TdxRichEditRtfDestinationBase.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create;
  FImporter := AImporter;
  FPieceTable := Importer.PieceTable;

  FDefaultKeywordHT := CreateDefaultKeywordTable;
  FKeywordHT := CreateKeywordTable;
  FDefaultControlCharHT := CreateDefaultControlCharTable;
  FControlCharHT := CreateControlCharTable;
end;

destructor TdxRichEditRtfDestinationBase.Destroy;
begin
  FreeAndNil(FControlCharHT);
  FreeAndNil(FDefaultControlCharHT);
  FreeAndNil(FKeywordHT);
  FreeAndNil(FDefaultKeywordHT);
  inherited Destroy;
end;

procedure TdxRichEditRtfDestinationBase.ProcessBinChar(AChar: Char);
begin
  ProcessBinCharCore(AChar);
  FNonEmpty := True;
end;

procedure TdxRichEditRtfDestinationBase.ProcessChar(AChar: Char);
begin
  ProcessCharCore(AChar);
  FNonEmpty := True;
end;

procedure TdxRichEditRtfDestinationBase.ProcessControlChar(AChar: Char);
begin
  ProcessControlCharCore(AChar);
  FNonEmpty := True;
end;

function TdxRichEditRtfDestinationBase.ProcessKeyword(const AKeyword: string; AParameterValue: Integer; AHasParameter: Boolean): Boolean;
begin
  Result := ProcessKeywordCore(AKeyword, AParameterValue, AHasParameter);
  FNonEmpty := True;
end;

procedure TdxRichEditRtfDestinationBase.ProcessText(const AText: string);
begin
  ProcessTextCore(AText);
  FNonEmpty := True;
end;

procedure TdxRichEditRtfDestinationBase.AfterPopRtfState;
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.BeforePopRtfState;
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.BeforeNestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
begin
  BeforeNestedGroupFinishedCore(ADestination);
  FNonEmpty := True;
end;

procedure TdxRichEditRtfDestinationBase.NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
begin
//do nothing
end;

function TdxRichEditRtfDestinationBase.CanAppendText: Boolean;
begin
  Result := False;
end;

function TdxRichEditRtfDestinationBase.CanProcessDefaultKeyword: Boolean;
begin
  Result := True;
end;

function TdxRichEditRtfDestinationBase.CreateClone: TdxRichEditRtfDestinationBase;
begin
  Result := TdxRichEditRtfDestinationBaseClass(ClassType).Create(Importer);
  InitializeClone(Result);
end;

function TdxRichEditRtfDestinationBase.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := FControlCharHT;
end;

function TdxRichEditRtfDestinationBase.GetKeywordHT: TdxKeywordTranslatorTable;
begin
  Result := FKeywordHT;
end;

procedure TdxRichEditRtfDestinationBase.IncreaseGroupLevel;
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.InitializeClone(AClone: TdxRichEditRtfDestinationBase);
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.PopulateControlCharTable(ATable: TdxControlCharTranslatorTable);
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.ProcessBinCharCore(AChar: Char);
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.ProcessCharCore(AChar: Char);
begin
//do nothing
end;

procedure TdxRichEditRtfDestinationBase.ProcessControlCharCore(AChar: Char);
var
  ATranslator: TdxTranslateControlCharHandler;
begin
  if ((ControlCharHT <> nil) and ControlCharHT.TryGetValue(AChar, ATranslator)) or
     DefaultControlCharHT.TryGetValue(AChar, ATranslator) then
    ATranslator(Importer, AChar);
end;

function TdxRichEditRtfDestinationBase.ProcessKeywordCore(const AKeyword: string; AParameterValue: Integer; AHasParameter: Boolean): Boolean;
var
  ATranslator: TdxTranslateKeywordHandler;
begin
  Result := FKeywordHT.TryGetValue(AKeyword, ATranslator) or
    (CanProcessDefaultKeyword and FDefaultKeywordHT.TryGetValue(AKeyword, ATranslator));
  if Result then
    ATranslator(Importer, AParameterValue, AHasParameter);
end;

procedure TdxRichEditRtfDestinationBase.ProcessTextCore(const AText: string);
begin
  Assert(CanAppendText);
end;

procedure TdxRichEditRtfDestinationBase.BeforeNestedGroupFinishedCore(ADestination: TdxRichEditRtfDestinationBase);
begin
//do nothing
end;

class procedure TdxRichEditRtfDestinationBase.BinKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter and (AParameterValue <> 0) then
  begin
    AImporter.StateManager.ParsingState := TdxRichEditRtfParsingState.BinData;
  end
  else
    AImporter.DecreaseSkipCount;
end;

class procedure TdxRichEditRtfDestinationBase.UnicodeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
var
  AChar: WideChar;
begin
  AChar := WideChar(AParameterValue and $FFFF);
  if (Ord(AChar) >= $F020) and (Ord(AChar) <= $F0FF) then
    AChar := WideChar(Ord(AChar) - $F000);
  AImporter.ParseUnicodeChar(AChar);
end;

class procedure TdxRichEditRtfDestinationBase.ColorTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxColorTableDestination.Create(AImporter);
end;

class procedure TdxRichEditRtfDestinationBase.FontTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Destination := TdxFontTableDestination.Create(AImporter);
end;

class procedure TdxRichEditRtfDestinationBase.UserTableKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
end;

class procedure TdxRichEditRtfDestinationBase.AnsiKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.SetCodePage(TEncoding.Default.CodePage);
end;

class procedure TdxRichEditRtfDestinationBase.MacKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.DocumentProperties.DefaultCodePage := macCodePage;
  AImporter.SetCodePage(macCodePage);
end;

class procedure TdxRichEditRtfDestinationBase.PcKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.DocumentProperties.DefaultCodePage := pcCodePage;
  AImporter.SetCodePage(pcCodePage);
end;

class procedure TdxRichEditRtfDestinationBase.PcaKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.DocumentProperties.DefaultCodePage := pcaCodePage;
  AImporter.SetCodePage(pcaCodePage);
end;

class procedure TdxRichEditRtfDestinationBase.AnsiCodePageKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  if AHasParameter then
  begin
    AImporter.DocumentProperties.DefaultCodePage := AParameterValue;
    AImporter.SetCodePage(AParameterValue);
  end;
end;

class procedure TdxRichEditRtfDestinationBase.HyphAutoKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  AParameterValue: Integer; AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 1;
  AImporter.DocumentModel.DocumentProperties.HyphenateDocument := AParameterValue <> 0;
end;

class procedure TdxRichEditRtfDestinationBase.EscapedCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter; var AChar: Char);
begin
  AImporter.FlushDecoder;
  AImporter.Destination.ProcessChar(AChar);
end;

class procedure TdxRichEditRtfDestinationBase.SwitchToHexCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  var AChar: Char);
begin
  AImporter.StateManager.ParsingState := TdxRichEditRtfParsingState.HexData;
end;

class procedure TdxRichEditRtfDestinationBase.OptionalGroupCharHandler(AImporter: TdxRichEditDocumentModelRtfImporter;
  var AChar: Char);
begin
  if AImporter.Destination.NonEmpty then
    Exit;
  AImporter.FlushDecoder;
  AImporter.OptionalGroupLevel := AImporter.StateManager.States.Count;
end;

function TdxRichEditRtfDestinationBase.CreateControlCharTable: TdxControlCharTranslatorTable;
begin
  Result := TdxControlCharTranslatorTable.Create;
  PopulateControlCharTable(Result);
end;

function TdxRichEditRtfDestinationBase.CreateDefaultControlCharTable: TdxControlCharTranslatorTable;
begin
  Result := TdxControlCharTranslatorTable.Create;
  Result.Add('''', SwitchToHexCharHandler);
  Result.Add('*', OptionalGroupCharHandler);
end;

function TdxRichEditRtfDestinationBase.CreateDefaultKeywordTable: TdxKeywordTranslatorTable;
begin
  Result := TdxKeywordTranslatorTable.Create;
  Result.Add('bin', BinKeywordHandler);
  Result.Add('colortbl', ColorTableKeywordHandler);
  Result.Add('fonttbl', FontTableKeywordHandler);
  Result.Add('protusertbl', UserTableKeywordHandler);
  Result.Add('ansi', AnsiKeywordHandler);
  Result.Add('mac', MacKeywordHandler);
  Result.Add('pc', PcKeywordHandler);
  Result.Add('pca', PcaKeywordHandler);
  Result.Add('ansicpg', AnsiCodePageKeywordHandler);
  Result.Add('hyphauto', HyphAutoKeywordHandler);
end;

function TdxRichEditRtfDestinationBase.CreateKeywordTable: TdxKeywordTranslatorTable;
begin
  Result := TdxKeywordTranslatorTable.Create;
  PopulateKeywordTable(Result);
end;

{ TdxRichEditRtfPieceTableInfo }

constructor TdxRichEditRtfPieceTableInfo.Create(AImporter: TdxRichEditDocumentModelRtfImporter;
  APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FCharacterFormattingOptions := TdxCharacterFormattingOptions.Create(TdxCharacterFormattingOptions.MaskUseAll);
  FPosition := TdxRtfInputPosition.Create(PieceTable);
  FPosition.CharacterFormatting.ReplaceInfo(APieceTable.DocumentModel.Cache.CharacterFormattingInfoCache.DefaultItem, FCharacterFormattingOptions);
  FPosition.CharacterFormatting.BeginUpdate;
  FPosition.CharacterFormatting.DoubleFontSize := 24;
  FPosition.CharacterFormatting.FontName := 'Times New Roman';
  FPosition.CharacterFormatting.EndUpdate;
  FTableReader := TdxRtfTableReader.Create(AImporter);
end;

destructor TdxRichEditRtfPieceTableInfo.Destroy;
begin
  FreeAndNil(FTableReader);
  FreeAndNil(FPosition);
  FreeAndNil(FCharacterFormattingOptions);
  inherited Destroy;
end;

{ TdxRichEditRtfPieceTableInfos }

function TdxRichEditRtfPieceTableInfos.GetItem(Index: Integer): TdxRichEditRtfPieceTableInfo;
begin
  Result := TdxRichEditRtfPieceTableInfo(inherited Items[Index]);
end;

procedure TdxRichEditRtfPieceTableInfos.SetItem(Index: Integer; const Value: TdxRichEditRtfPieceTableInfo);
begin
  inherited Items[Index] := Value;
end;

{ TdxCharacterDecoder }

constructor TdxCharacterDecoder.Create(ACodePage: Cardinal);
begin
  inherited Create;
  FEncoding := TEncoding.GetEncoding(ACodePage)
end;

destructor TdxCharacterDecoder.Destroy;
begin
  FreeAndNil(FEncoding);
  inherited Destroy;
end;

{ TdxCodePageCharacterDecoder }

constructor TdxCodePageCharacterDecoder.Create(ACodePage: Cardinal);
begin
  inherited Create(ACodePage);
  FCapacity := 4096;
  SetLength(FBytes, FCapacity);
end;

destructor TdxCodePageCharacterDecoder.Destroy;
begin
  FCapacity := 0;
  SetLength(FBytes, 0);
  cxClearObjectLinks(Self);
  inherited Destroy;
end;

procedure TdxCodePageCharacterDecoder.ProcessChar(AImporter: TdxRichEditDocumentModelRtfImporter; AChar: Char);
begin
  if FCount = FCapacity then
    Flush(AImporter);
  AddByte(Byte(AChar));
end;

procedure TdxCodePageCharacterDecoder.Flush(AImporter: TdxRichEditDocumentModelRtfImporter);
var
  ACount: Integer;
  AChars: TCharArray;
  ALink: TcxObjectLink;
begin
  if (FCount > 0) then
  begin
    ALink := cxAddObjectLink(Self);
    try
      AChars := Encoding.GetChars(FBytes, 0, FCount);
      try
        ACount := Length(AChars);
        if (not AImporter.Destination.CanAppendText) or (ACount <= 1) then
          FlushByChar(AImporter, AChars)
        else
          FlushByString(AImporter, AChars);
      finally
        SetLength(AChars, 0);
      end;
      if ALink.Ref <> nil then
        Clear;
    finally
      cxRemoveObjectLink(ALink);
    end;
  end;
end;

procedure TdxCodePageCharacterDecoder.AddByte(Value: Byte);
begin
  if FCount >= FCapacity then
  begin
    Inc(FCapacity, 4096);
    SetLength(FBytes, FCapacity);
  end;
  FBytes[FCount] := Value;
  Inc(FCount);
end;

procedure TdxCodePageCharacterDecoder.Clear;
begin
  FCount := 0;
  FCapacity := 0;
  SetLength(FBytes, FCapacity);
end;

procedure TdxCodePageCharacterDecoder.FlushByChar(AImporter: TdxRichEditDocumentModelRtfImporter; const AChars: TCharArray);
var
  I: Integer;
  ADestination: TdxRichEditRtfDestinationBase;
begin
  ADestination := AImporter.Destination;
  for I := Low(AChars) to High(AChars) do
    ADestination.ProcessChar(AChars[I]);
end;

procedure TdxCodePageCharacterDecoder.FlushByString(AImporter: TdxRichEditDocumentModelRtfImporter; const AChars: TCharArray);
var
  S: string;
begin
  SetString(S, PChar(@AChars[0]), Length(AChars));
  AImporter.Destination.ProcessText(S);
end;

{ TdxEmptyCharacterDecoder }

constructor TdxEmptyCharacterDecoder.Create;
begin
  inherited Create(TdxEncoding.GetEncodingCodePage(TEncoding.Default));
end;

procedure TdxEmptyCharacterDecoder.Flush(
  AImporter: TdxRichEditDocumentModelRtfImporter);
begin
//do nothing
end;

procedure TdxEmptyCharacterDecoder.ProcessChar(AImporter: TdxRichEditDocumentModelRtfImporter; AChar: Char);
begin
  AImporter.Destination.ProcessChar(AChar);
end;

{ TdxRtfFormattingInfo }

constructor TdxRtfFormattingInfo.Create;
begin
  inherited Create;
  FUnicodeCharacterByteCount := 1;
  FParentStyleIndex := -1;
  CodePage := TEncoding.Default.CodePage;
end;

destructor TdxRtfFormattingInfo.Destroy;
begin
  FreeAndNil(FDecoder);
  inherited Destroy;
end;

procedure TdxRtfFormattingInfo.CopyFrom(const Source: TdxRtfFormattingInfo);
begin
  if Source is TdxRtfFormattingInfo then
  begin
    CodePage := Source.CodePage;
    ParentStyleIndex := Source.ParentStyleIndex;
    UnicodeCharacterByteCount := Source.UnicodeCharacterByteCount;
  end;
end;

function TdxRtfFormattingInfo.Clone: TdxRtfFormattingInfo;
begin
  Result := TdxRtfFormattingInfoClass(ClassType).Create;
  Result.CopyFrom(Self);
end;

function TdxRtfFormattingInfo.ChooseDecoder: TdxCodePageCharacterDecoder;
begin
  Result := TdxCodePageCharacterDecoder.Create(CodePage);
end;

procedure TdxRtfFormattingInfo.SetDecoder(Value: TdxCodePageCharacterDecoder; ANeedDestroyOldDecoder: Boolean = True);
begin
  if Decoder <> Value then
  begin
    Assert(Value <> nil);
    if ANeedDestroyOldDecoder then
      FDecoder.Free;
    FDecoder := Value;
  end;
end;

procedure TdxRtfFormattingInfo.SetCodePage(Value: Cardinal);
begin
  if FCodePage <> Value then
  begin
    FCodePage := Value;
    SetDecoder(ChooseDecoder)
  end;
end;

procedure TdxRtfFormattingInfo.SetUnicodeCharacterByteCount(Value: Integer);
begin
  if Value < 0 then
    TdxRichEditDocumentModelRtfImporter.ThrowInvalidRtfFile;
  FUnicodeCharacterByteCount := value;
end;

{ TdxRtfOldListLevelInfo }

constructor TdxRtfOldListLevelInfo.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
  FCharacterProperties := TdxCharacterFormattingInfo.Create;
  FListLevelProperties := TdxListLevelInfo.Create;
end;

destructor TdxRtfOldListLevelInfo.Destroy;
begin
  FreeAndNil(FCharacterProperties);
  FreeAndNil(FListLevelProperties);
  inherited Destroy;
end;

procedure TdxRtfOldListLevelInfo.CopyFrom(const Source: TdxRtfOldListLevelInfo);
begin
  if Source is TdxRtfOldListLevelInfo then
  begin
    CharacterProperties.CopyFrom(Source.CharacterProperties);
    IncludeInformationFromPreviousLevel := Source.IncludeInformationFromPreviousLevel;
    Indent := Source.Indent;
    ListLevelProperties.CopyFrom(Source.ListLevelProperties);
    SkipNumbering := Source.SkipNumbering;
    TextAfter := Source.TextAfter;
    TextBefore := Source.TextBefore;
  end;
end;

function TdxRtfOldListLevelInfo.Clone: TdxRtfOldListLevelInfo;
begin
  Result := TdxRtfOldListLevelInfoClass(ClassType).Create(FDocumentModel);
  Result.CopyFrom(Self);
end;

{ TdxRtfOldListLevelInfoCollection }

constructor TdxRtfOldListLevelInfoCollection.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
end;

function TdxRtfOldListLevelInfoCollection.Clone: TdxRtfOldListLevelInfoCollection;
begin
  Result := TdxRtfOldListLevelInfoCollectionClass(ClassType).Create(FDocumentModel);
  Result.CopyFrom(Self);
end;

procedure TdxRtfOldListLevelInfoCollection.CopyFrom(ASource: TdxRtfOldListLevelInfoCollection);
var
  I: Integer;
begin
  Clear;
  for I := 0 to ASource.Count - 1 do
    Add(ASource.Items[I].Clone);
end;

function TdxRtfOldListLevelInfoCollection.GetItem(Index: Integer): TdxRtfOldListLevelInfo;
begin
  Result := TdxRtfOldListLevelInfo(inherited Items[Index]);
end;

procedure TdxRtfOldListLevelInfoCollection.SetItem(Index: Integer; const Value: TdxRtfOldListLevelInfo);
begin
  inherited Items[Index] := Value;
end;

{ TdxRtfInputPositionState }

constructor TdxRtfInputPositionState.Create(APosition: TdxRtfInputPosition);
begin
  inherited Create;
  FCharacterFormatting := APosition.CharacterFormatting.Clone;
  FCharacterStyleIndex := APosition.CharacterStyleIndex;
  FParagraphFormattingInfo := APosition.ParagraphFormattingInfo.Clone as TdxRtfParagraphFormattingInfo;
  FRtfFormattingInfo := APosition.RtfFormattingInfo.Clone;
  FCurrentOldMultiLevelListIndex := APosition.CurrentOldMultiLevelListIndex;
  FCurrentOldSimpleListIndex := APosition.CurrentOldSimpleListIndex;
  FCurrentOldSimpleList := APosition.CurrentOldSimpleList;
  FCurrentOldListLevelNumber := APosition.CurrentOldListLevelNumber;
  FCurrentOldListSkipNumbering := APosition.CurrentOldListSkipNumbering;
  FTableStyleIndex := APosition.TableStyleIndex;
  FIsDoubleByteCharactersFont := APosition.IsDoubleByteCharactersFont;
  FDoubleByteCharactersFontName := APosition.DoubleByteCharactersFontName;
  if APosition.OldListLevelInfo <> nil then
    FOldListLevelInfo := APosition.OldListLevelInfo.Clone;
  FOldListLevelInfoCollection := APosition.OldListLevelInfoCollection.Clone;
end;

destructor TdxRtfInputPositionState.Destroy;
begin
  FreeAndNil(FOldListLevelInfoCollection);
  FreeAndNil(FOldListLevelInfo);
  FreeAndNil(FRtfFormattingInfo);
  FreeAndNil(FParagraphFormattingInfo);
  FreeAndNil(FCharacterFormatting);
  inherited Destroy;
end;

{ TdxRtfInputPosition }

constructor TdxRtfInputPosition.Create(APieceTable: TdxPieceTable);
begin
  inherited Create(APieceTable);
  FRtfFormattingInfo := TdxRtfFormattingInfo.Create;
  FParagraphFormattingInfo := TdxRtfParagraphFormattingInfo.Create;
  FCurrentOldMultiLevelListIndex := NumberingListIndexNoNumberingList;
  FCurrentOldSimpleListIndex := NumberingListIndexNoNumberingList;
  FOldListLevelInfoCollection := TdxRtfOldListLevelInfoCollection.Create(DocumentModel);
  FSectionFormattingInfo := TdxRtfSectionFormattingInfo.Create(DocumentModel);
end;

destructor TdxRtfInputPosition.Destroy;
begin
  FreeAndNil(FOldListLevelInfo);
  FreeAndNil(FSectionFormattingInfo);
  FreeAndNil(FOldListLevelInfoCollection);
  FreeAndNil(FParagraphFormattingInfo);
  FreeAndNil(FRtfFormattingInfo);
  inherited Destroy;
end;

function TdxRtfInputPosition.GetState: TdxRtfInputPositionState;
begin
  Result := TdxRtfInputPositionState.Create(Self);
end;

procedure TdxRtfInputPosition.SetState(AState: TdxRtfInputPositionState);
begin
  CharacterFormatting.CopyFrom(AState.CharacterFormatting);
  CharacterStyleIndex := AState.CharacterStyleIndex;
  ParagraphFormattingInfo.CopyFrom(AState.ParagraphFormattingInfo);
  RtfFormattingInfo := AState.RtfFormattingInfo;
  CurrentOldMultiLevelListIndex := AState.CurrentOldMultiLevelListIndex;
  CurrentOldSimpleListIndex := AState.CurrentOldSimpleListIndex;
  CurrentOldListLevelNumber := AState.CurrentOldListLevelNumber;
  CurrentOldListSkipNumbering := AState.CurrentOldListSkipNumbering;
  CurrentOldSimpleList := AState.CurrentOldSimpleList;
  TableStyleIndex := AState.TableStyleIndex;
  IsDoubleByteCharactersFont := AState.IsDoubleByteCharactersFont;
  DoubleByteCharactersFontName := AState.DoubleByteCharactersFontName;
  if AState.OldListLevelInfo <> nil then
    OldListLevelInfo.CopyFrom(AState.OldListLevelInfo)
  else
    FreeAndNil(FOldListLevelInfo);
  OldListLevelInfoCollection.CopyFrom(AState.OldListLevelInfoCollection);
end;

procedure TdxRtfInputPosition.SetParagraphFormattingInfo(const Value: TdxRtfParagraphFormattingInfo);
begin
  ParagraphFormattingInfo.CopyFrom(Value);
end;

procedure TdxRtfInputPosition.SetRtfFormattingInfo(const Value: TdxRtfFormattingInfo);
begin
  RtfFormattingInfo.CopyFrom(Value);
end;

{ TdxRichEditRtfParserStateItem }

constructor TdxRichEditRtfParserStateItem.Create(AInputPositionState: TdxRtfInputPositionState; ADestination: TdxRichEditRtfDestinationBase);
begin
  inherited Create;
  FDestination := ADestination;
  FInputPositionState := AInputPositionState;
end;

{ TdxRichEditRtfParserStateItems }

function TdxRichEditRtfParserStateItems.GetItem(Index: Integer): TdxRichEditRtfParserStateItem;
begin
  Result := TdxRichEditRtfParserStateItem(inherited Items[Index]);
end;

procedure TdxRichEditRtfParserStateItems.SetItem(Index: Integer; const Value: TdxRichEditRtfParserStateItem);
begin
  inherited Items[Index] := Value;
end;

{ TdxRichEditRtfParserStateManager }

constructor TdxRichEditRtfParserStateManager.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create;
  FImporter := AImporter;
  FStates := TdxRichEditRtfParserStateItems.Create;
  FPieceTables := TdxRichEditRtfPieceTableInfos.Create;
  FSuperfluousDestinations := TdxFastObjectList.Create;
end;

destructor TdxRichEditRtfParserStateManager.Destroy;
begin
  FreeAndNil(FPieceTables);
  FreeAndNil(FStates);
  FreeAndNil(FDestination);
  FreeAndNil(FSuperfluousDestinations);
  inherited Destroy;
end;

procedure TdxRichEditRtfParserStateManager.PopState;
var
  ANestedDestination, ANewDestination: TdxRichEditRtfDestinationBase;
  AState: TdxRichEditRtfParserStateItem;
  ASamePieceTable: Boolean;
  APieceTableInfo: TdxRichEditRtfPieceTableInfo;
begin
  ANestedDestination := Destination;
  AState := States.Pop as TdxRichEditRtfParserStateItem;
  try
    ANewDestination := AState.Destination;

    ANewDestination.BeforeNestedGroupFinished(ANestedDestination);
    Destination.BeforePopRtfState;
    ASamePieceTable := Destination.PieceTable = ANewDestination.PieceTable;
    APieceTableInfo := PieceTables.Pop as TdxRichEditRtfPieceTableInfo;
    SetDestinationCore(ANewDestination, False);
    PieceTables.Push(APieceTableInfo);

    Destination.NestedGroupFinished(ANestedDestination);
    if (AState.InputPositionState <> nil) and ASamePieceTable then
      Importer.Position.SetState(AState.InputPositionState);
  finally
    AState.InputPositionState.Free;
    AState.Free;
  end;
  PieceTables.Pop;

  if PieceTables.Count <= 0 then
    PieceTables.Push(APieceTableInfo);
end;

procedure TdxRichEditRtfParserStateManager.PushState;
begin
  FStates.Push(TdxRichEditRtfParserStateItem.Create(Importer.Position.GetState, Destination));
  SetDestinationCore(Destination.CreateClone, True);
  PieceTables.Push(PieceTableInfo);
  Destination.IncreaseGroupLevel;
end;

procedure TdxRichEditRtfParserStateManager.CheckSuperfluousDestinations;
begin
  FSuperfluousDestinations.Clear; 
end;

function TdxRichEditRtfParserStateManager.CreateDefaultDestination: TdxRichEditRtfDestinationBase;
begin
  Result := TdxDefaultDestination.Create(Importer);
end;

procedure TdxRichEditRtfParserStateManager.Initialize;
begin
  FPieceTables.Push(TdxRichEditRtfPieceTableInfo.Create(Importer, Importer.DocumentModel.MainPieceTable));
  FDestination := CreateDefaultDestination;
  FStates.Push(TdxRichEditRtfParserStateItem.Create(nil, Destination));
end;

procedure TdxRichEditRtfParserStateManager.SetDestinationCore(Value: TdxRichEditRtfDestinationBase; AUpdatePieceTableInfo: Boolean);
begin
  if (Value.PieceTable <> Destination.PieceTable) and AUpdatePieceTableInfo and (PieceTables.Count > 0) then
  begin
    PieceTables.Pop.Free;
    PieceTables.Push(TdxRichEditRtfPieceTableInfo.Create(Importer, Value.PieceTable));
  end;
  FDestination := Value;
end;

function TdxRichEditRtfParserStateManager.GetPieceTableInfo: TdxRichEditRtfPieceTableInfo;
begin
  Result := TdxRichEditRtfPieceTableInfo(PieceTables.Peek);
end;

procedure TdxRichEditRtfParserStateManager.SetDestination(const Value: TdxRichEditRtfDestinationBase);
begin
  if Destination <> Value then
  begin
    if Destination <> nil then
      FSuperfluousDestinations.Add(Destination);
    SetDestinationCore(Value, True);
  end;
end;

{ TdxListLevelDisplayTextHelper }

class function TdxListLevelDisplayTextHelper.CreateDisplayFormatStringCore(APlaceholderIndices: TList<Integer>; const AText: string): string;
var
  ASb: TStringBuilder;
  I, ACount: Integer;
begin
  ASb := TStringBuilder.Create;
  try
    ASb.Append(GetTextRange(APlaceholderIndices, 0, AText));
    ACount := APlaceholderIndices.Count - 1;
    for I := 1 to ACount - 1 do
    begin
      ASb.Append('%');
      ASb.Append(Ord(AText[APlaceholderIndices[I] + 1]));
      ASb.Append(':s');
      ASb.Append(GetTextRange(APlaceholderIndices, I, AText));
    end;
    Result := ASb.ToString;
  finally
    ASb.Free;
  end;
end;

class function TdxListLevelDisplayTextHelper.GetTextRange(APlaceholderIndices: TList<Integer>; AStartPlaceHolderIndex: Integer; const AText: string): string;
var
  AIndex: Integer;
begin
  AIndex := APlaceholderIndices[AStartPlaceHolderIndex] + 1;
  Result := Copy(AText, AIndex + 1, APlaceholderIndices[AStartPlaceHolderIndex + 1] - AIndex);
  Result := StringReplace(Result, '%', '%%', [rfReplaceAll]);
end;

{ TdxRtfListLevel }

constructor TdxRtfListLevel.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create(ADocumentModel);
  ListLevelProperties.Start := 1;
  ListLevelProperties.Separator := TdxCharacters.TabMark;
  ListLevelProperties.DisplayFormatString := '%s.';
  ListLevelProperties.RelativeRestartLevel := 0;
end;

function TdxRtfListLevel.CreateDisplayFormatString: string;
var
  APlaceholderIndices: TList<Integer>;
begin
  APlaceholderIndices := CreatePlaceholderIndices;
  try
    if Length(Text) = 0 then
      Result := ''
    else
      Result := TdxStringHelper.RemoveSpecialSymbols(CreateDisplayFormatStringCore(APlaceholderIndices));
  finally
    APlaceholderIndices.Free;
  end;
end;

procedure TdxRtfListLevel.CopyFrom(AListLevel: TdxRtfListLevel);
begin
  inherited CopyFrom(AListLevel);
  FNumber := AListLevel.Number;
  FText := AListLevel.Text;
end;

function TdxRtfListLevel.Clone: TdxRtfListLevel;
begin
  Result := TdxRtfListLevel.Create(DocumentModel);
  Result.CopyFrom(Self);
end;

function TdxRtfListLevel.CreatePlaceholderIndices: TList<Integer>;
var
  I, ACount: Integer;
begin
  ACount := Min(9, Length(Number));
  Result := TList<Integer>.Create;
  Result.Add(0);
  for I := 1 to ACount do 
    if Ord(Number[I]) <= Length(Text) then
      Result.Add(Ord(Number[I]));
  Result.Add(Length(Text));
end;

function TdxRtfListLevel.CreateDisplayFormatStringCore(APlaceholderIndices: TList<Integer>): string;
begin
  Result := TdxListLevelDisplayTextHelper.CreateDisplayFormatStringCore(APlaceholderIndices, Text);
end;

{ TdxRtfListLevels }

procedure TdxRtfListLevels.CopyFrom(Value: TdxRtfListLevels);
var
  I: Integer;
begin
  Clear;
  for I := 0 to Value.Count - 1 do
  begin
    Add(Value[I]);
  end;
end;

{ TdxRtfNumberingList }

constructor TdxRtfNumberingList.Create;
begin
  inherited Create;
  FLevels := TdxRtfListLevels.Create;
end;

destructor TdxRtfNumberingList.Destroy;
begin
  FreeAndNil(FLevels);
  inherited Destroy;
end;

procedure TdxRtfNumberingList.Assign(Source: TPersistent);
var
  ASource: TdxRtfNumberingList;
begin
  if Source is TdxRtfNumberingList then
  begin
    ASource := TdxRtfNumberingList(Source);
    FID := ASource.ID;
    FName := ASource.Name;
    FNumberingListType := ASource.NumberingListType;
    FParentStyleID := ASource.ParentStyleID;
    FStyleName := ASource.StyleName;
  end
  else
    inherited Assign(Source);
end;

procedure TdxRtfNumberingList.Reset;
begin
  FID := 0;
  FLevels.Clear;
  FName := '';
  FNumberingListType := TdxRtfNumberingListType.Unknown;
  FParentStyleID := 0;
  FStyleName := '';
end;

{ TdxRtfListOverrideLevel }

constructor TdxRtfListOverrideLevel.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
  FStartAt := MinInt;
end;

procedure TdxRtfListOverrideLevel.CopyFrom(ALevel: TdxRtfListOverrideLevel);
begin
  Level := ALevel.Level;
  OverrideFormat := ALevel.OverrideFormat;
  OverrideStartAt := ALevel.OverrideStartAt;
  StartAt := ALevel.StartAt;
end;

function TdxRtfListOverrideLevel.Clone: TdxRtfListOverrideLevel;
begin
  Result := TdxRtfListOverrideLevel.Create(FDocumentModel);
  Result.CopyFrom(Self);
end;

procedure TdxRtfListOverrideLevel.SetLevel(const Value: TdxRtfListLevel);
begin
  FLevel := Value;
end;

{ TdxRtfNumberingListOverride }

constructor TdxRtfNumberingListOverride.Create;
begin
  inherited Create;
  FLevels := TdxRtfListOverrideLevels.Create;
end;

destructor TdxRtfNumberingListOverride.Destroy;
begin
  FreeAndNil(FLevels);
  inherited Destroy;
end;

{ TdxRtfSectionFormattingInfo }

constructor TdxRtfSectionFormattingInfo.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FPage := TdxPageInfo.Create;
  FMargins := TdxMarginsInfo.Create;
  FPageNumbering := TdxPageNumberingInfo.Create;
  FGeneralSectionInfo := TdxGeneralSectionInfo.Create;
  FLineNumbering := TdxLineNumberingInfo.Create;
  FColumns := TdxColumnsInfo.Create;
  FFootNote := TdxSectionFootNote.Create(ADocumentModel);
  FEndNote := TdxSectionFootNote.Create(ADocumentModel);
end;

destructor TdxRtfSectionFormattingInfo.Destroy;
begin
  FreeAndNil(FEndNote);
  FreeAndNil(FFootNote);
  FreeAndNil(FColumns);
  FreeAndNil(FLineNumbering);
  FreeAndNil(FGeneralSectionInfo);
  FreeAndNil(FPageNumbering);
  FreeAndNil(FMargins);
  FreeAndNil(FPage);
  inherited Destroy;
end;

procedure TdxRtfSectionFormattingInfo.Assign(Source: TPersistent);
var
  ASource: TdxRtfSectionFormattingInfo;
begin
  if Source is TdxRtfSectionFormattingInfo then
  begin
    ASource := TdxRtfSectionFormattingInfo(Source);
    FPage.CopyFrom(ASource.Page);
    FFootNote.CopyFrom(ASource.FootNote);
    FLineNumbering.CopyFrom(ASource.LineNumbering);
    FMargins.CopyFrom(ASource.Margins);
    FColumns.CopyFrom(ASource.Columns);
    FPageNumbering.CopyFrom(ASource.PageNumbering);
    FGeneralSectionInfo.CopyFrom(ASource.GeneralSectionInfo);
  end
  else
    inherited Assign(Source);
end;

{ TdxRtfDocumentProperties }

constructor TdxRtfDocumentProperties.Create(ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FColors := TList<TColor>.Create;
  FDefaultCodePage := TEncoding.Default.CodePage;
  FFonts := TdxRtfFontInfoCollection.Create(ADocumentModel);
  FListTable := TdxRtfListTable.Create;
  FListOverrideTable := TdxListOverrideTable.Create;
  FDefaultSectionProperties := TdxRtfSectionFormattingInfo.Create(ADocumentModel);
end;

destructor TdxRtfDocumentProperties.Destroy;
begin
  FreeAndNil(FDefaultSectionProperties);
  FreeAndNil(FListOverrideTable);
  FreeAndNil(FListTable);
  FreeAndNil(FFonts);
  FreeAndNil(FColors);
  inherited Destroy;
end;

{ TdxRtfNumberingListInfo }

constructor TdxRtfNumberingListInfo.Create(ARtfNumberingListIndex, AListLevelIndex: Integer);
begin
  inherited Create;
  FRtfNumberingListIndex := ARtfNumberingListIndex;
  FListLevelIndex := AListLevelIndex;
end;

{ TdxRichEditDocumentModelRtfImporter }

constructor TdxRichEditDocumentModelRtfImporter.Create(ADocumentModel: TdxDocumentModel;
  AOptions: TdxDocumentImporterOptions);
begin
  inherited Create(ADocumentModel, AOptions);
  Assert(AOptions is TdxRtfDocumentImporterOptions);
  FOptionalGroupLevel := MaxInt;
  FDocumentProperties := TdxRtfDocumentProperties.Create(DocumentModel);
  FParagraphStyleCollectionIndex := TdxIntegerDictionary.Create;
  FParagraphStyleCollectionIndex.Add(0, 0);
  FParagraphStyleListOverrideIndexMap := TObjectDictionary<TdxParagraphStyle, TdxRtfNumberingListInfo>.Create([doOwnsValues]);
  FCharacterStyleCollectionIndex := TdxIntegerDictionary.Create;
  FLinkParagraphStyleIndexToCharacterStyleIndex := TdxIntegerDictionary.Create;
  FListOverrideIndexToNumberingListIndexMap := TDictionary<Integer, TdxNumberingListIndex>.Create;
  FNextParagraphStyleIndexTable := TdxIntegerDictionary.Create;
  FLastParagraphFormattingCacheIndex := -1;
  FLastParagraphPropertiesCacheIndex := -1;
  FKeyword := TStringBuilder.Create(32);
  FParameterValueString := TStringBuilder.Create(32);
  FTableStyleCollectionIndex := TdxIntegerDictionary.Create;
end;

destructor TdxRichEditDocumentModelRtfImporter.Destroy;
begin
  FreeAndNil(FTableStyleCollectionIndex);
  FreeAndNil(FKeyword);
  FreeAndNil(FParameterValueString);
  FreeAndNil(FParagraphStyleListOverrideIndexMap);
  FreeAndNil(FListOverrideIndexToNumberingListIndexMap);
  FreeAndNil(FLinkParagraphStyleIndexToCharacterStyleIndex);
  FreeAndNil(FCharacterStyleCollectionIndex);
  FreeAndNil(FParagraphStyleCollectionIndex);
  FreeAndNil(FNextParagraphStyleIndexTable);
  FreeAndNil(FStateManager);
  FreeAndNil(FDocumentProperties);
  inherited Destroy;
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyCharacterProperties(
  ACharacterProperties: TdxCharacterProperties;
  ACharacterFormattingInfo: TdxCharacterFormattingInfo;
  AParentCharacterFormatting: TdxMergedCharacterProperties;
  AResetUse: Boolean = True);
var
  AParentCharacterInfo: TdxCharacterFormattingInfo;
begin
  ACharacterProperties.BeginUpdate;
  try
    if AResetUse then
      ACharacterProperties.ResetAllUse;
    AParentCharacterInfo := AParentCharacterFormatting.Info;
    if ACharacterFormattingInfo.AllCaps <> AParentCharacterInfo.AllCaps then
      ACharacterProperties.AllCaps := ACharacterFormattingInfo.AllCaps;
    if ACharacterFormattingInfo.Hidden <> AParentCharacterInfo.Hidden then
      ACharacterProperties.Hidden := ACharacterFormattingInfo.Hidden;
    if ACharacterFormattingInfo.FontBold <> AParentCharacterInfo.FontBold then
      ACharacterProperties.FontBold := ACharacterFormattingInfo.FontBold;
    if ACharacterFormattingInfo.FontItalic <> AParentCharacterInfo.FontItalic then
      ACharacterProperties.FontItalic := ACharacterFormattingInfo.FontItalic;
    if ACharacterFormattingInfo.FontName <> AParentCharacterInfo.FontName then
      ACharacterProperties.FontName := ACharacterFormattingInfo.FontName;
    if ACharacterFormattingInfo.DoubleFontSize <> AParentCharacterInfo.DoubleFontSize then
      ACharacterProperties.DoubleFontSize := ACharacterFormattingInfo.DoubleFontSize;
    if ACharacterFormattingInfo.FontUnderlineType <> AParentCharacterInfo.FontUnderlineType then
      ACharacterProperties.FontUnderlineType := ACharacterFormattingInfo.FontUnderlineType;
    if ACharacterFormattingInfo.ForeColor <> AParentCharacterInfo.ForeColor then
      ACharacterProperties.ForeColor := ACharacterFormattingInfo.ForeColor;
    if ACharacterFormattingInfo.BackColor <> AParentCharacterInfo.BackColor then
      ACharacterProperties.BackColor := ACharacterFormattingInfo.BackColor;
    if ACharacterFormattingInfo.Script <> AParentCharacterInfo.Script then
      ACharacterProperties.Script := ACharacterFormattingInfo.Script;
    if ACharacterFormattingInfo.StrikeoutColor <> AParentCharacterInfo.StrikeoutColor then
      ACharacterProperties.StrikeoutColor := ACharacterFormattingInfo.StrikeoutColor;
    if ACharacterFormattingInfo.FontStrikeoutType <> AParentCharacterInfo.FontStrikeoutType then
      ACharacterProperties.FontStrikeoutType := ACharacterFormattingInfo.FontStrikeoutType;
    if ACharacterFormattingInfo.StrikeoutWordsOnly <> AParentCharacterInfo.StrikeoutWordsOnly then
      ACharacterProperties.StrikeoutWordsOnly := ACharacterFormattingInfo.StrikeoutWordsOnly;
    if ACharacterFormattingInfo.UnderlineColor <> AParentCharacterInfo.UnderlineColor then
      ACharacterProperties.UnderlineColor := ACharacterFormattingInfo.UnderlineColor;
    if ACharacterFormattingInfo.UnderlineWordsOnly <> AParentCharacterInfo.UnderlineWordsOnly then
      ACharacterProperties.UnderlineWordsOnly := ACharacterFormattingInfo.UnderlineWordsOnly;
  finally
    ACharacterProperties.EndUpdate;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyFormattingToLastParagraph;
var
  AParagraph: TdxParagraph;
begin
  Position.ParagraphFormattingInfo.LineSpacing := Position.ParagraphFormattingInfo.CalcLineSpacing(UnitConverter);
  Position.ParagraphFormattingInfo.LineSpacingType := Position.ParagraphFormattingInfo.CalcLineSpacingType;
  AParagraph := PieceTable.Paragraphs.Last;
  ApplyParagraphFormatting(AParagraph.Index);
  PieceTable.InheritParagraphRunStyleCore(Position, TdxParagraphRun(PieceTable.Runs[AParagraph.LastRunIndex]));
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyParagraphProperties(AParagraphProperties: TdxParagraphProperties;
  AParagraphFormatting: TdxParagraphFormattingInfo; AParentParagraphProperties: TdxMergedParagraphProperties;
  AResetUse: Boolean = True);
var
  AParentParagraphInfo: TdxParagraphFormattingInfo;
  ActualLeftIndent: Integer;
  AShouldApplyLineSpacing: Boolean;
begin
  AParagraphProperties.BeginUpdate;
  try
    AParentParagraphInfo := AParentParagraphProperties.Info;
    if AResetUse then
      AParagraphProperties.ResetAllUse;
    if AParagraphFormatting.Alignment <> AParentParagraphInfo.Alignment then
      AParagraphProperties.Alignment := AParagraphFormatting.Alignment;
    if AParagraphFormatting.RightIndent <> AParentParagraphInfo.RightIndent then
      AParagraphProperties.RightIndent := AParagraphFormatting.RightIndent;
    if AParagraphFormatting.FirstLineIndentType <> AParentParagraphInfo.FirstLineIndentType then
      AParagraphProperties.FirstLineIndentType := AParagraphFormatting.FirstLineIndentType;
    ActualLeftIndent := AParagraphFormatting.LeftIndent;
    if ActualLeftIndent <> AParentParagraphInfo.LeftIndent then
      AParagraphProperties.LeftIndent := ActualLeftIndent;
    if AParagraphFormatting.FirstLineIndent <> AParentParagraphInfo.FirstLineIndent then
      AParagraphProperties.FirstLineIndent := AParagraphFormatting.FirstLineIndent;
    if AParagraphFormatting.SuppressHyphenation <> AParentParagraphInfo.SuppressHyphenation then
      AParagraphProperties.SuppressHyphenation := AParagraphFormatting.SuppressHyphenation;
    if AParagraphFormatting.SuppressLineNumbers <> AParentParagraphInfo.SuppressLineNumbers then
      AParagraphProperties.SuppressLineNumbers := AParagraphFormatting.SuppressLineNumbers;
    if AParagraphFormatting.ContextualSpacing <> AParentParagraphInfo.ContextualSpacing then
      AParagraphProperties.ContextualSpacing := AParagraphFormatting.ContextualSpacing;
    if AParagraphFormatting.PageBreakBefore <> AParentParagraphInfo.PageBreakBefore then
      AParagraphProperties.PageBreakBefore := AParagraphFormatting.PageBreakBefore;
    if AParagraphFormatting.BeforeAutoSpacing <> AParentParagraphInfo.BeforeAutoSpacing then
      AParagraphProperties.BeforeAutoSpacing := AParagraphFormatting.BeforeAutoSpacing;
    if AParagraphFormatting.AfterAutoSpacing <> AParentParagraphInfo.AfterAutoSpacing then
      AParagraphProperties.AfterAutoSpacing := AParagraphFormatting.AfterAutoSpacing;
    if AParagraphFormatting.KeepWithNext <> AParentParagraphInfo.KeepWithNext then
      AParagraphProperties.KeepWithNext := AParagraphFormatting.KeepWithNext;
    if AParagraphFormatting.KeepLinesTogether <> AParentParagraphInfo.KeepLinesTogether then
      AParagraphProperties.KeepLinesTogether := AParagraphFormatting.KeepLinesTogether;
    if AParagraphFormatting.WidowOrphanControl <> AParentParagraphInfo.WidowOrphanControl then
      AParagraphProperties.WidowOrphanControl := aparagraphFormatting.WidowOrphanControl;
    if AParagraphFormatting.OutlineLevel <> AParentParagraphInfo.OutlineLevel then
      AParagraphProperties.OutlineLevel := AParagraphFormatting.OutlineLevel;
    if AParagraphFormatting.BackColor <> AParentParagraphInfo.BackColor then
      AParagraphProperties.BackColor := AParagraphFormatting.BackColor;
    if AParagraphFormatting.SpacingBefore <> AParentParagraphInfo.SpacingBefore then
      AParagraphProperties.SpacingBefore := AParagraphFormatting.SpacingBefore;
    if AParagraphFormatting.SpacingAfter <> AParentParagraphInfo.SpacingAfter then
      AParagraphProperties.SpacingAfter := AParagraphFormatting.SpacingAfter;
    AShouldApplyLineSpacing := (AParagraphFormatting.LineSpacingType <> AParentParagraphInfo.LineSpacingType) or
      (AParagraphFormatting.LineSpacing <> AParentParagraphInfo.LineSpacing);
    if AShouldApplyLineSpacing then
    begin
      AParagraphProperties.LineSpacingType := AParagraphFormatting.LineSpacingType;
      AParagraphProperties.LineSpacing := AParagraphFormatting.LineSpacing;
    end;
  finally
    AParagraphProperties.EndUpdate;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyParagraphProperties(
  AParagraphProperties: TdxParagraphProperties;
  AParentParagraphRtfStyleIndex: Integer;
  AParagraphFormatting: TdxRtfParagraphFormattingInfo);
var
  AParentParagraphProperties: TdxMergedParagraphProperties;
begin
  AParentParagraphProperties := GetStyleMergedParagraphProperties(AParentParagraphRtfStyleIndex);
  try
    AParagraphFormatting.LineSpacingType := AParagraphFormatting.CalcLineSpacingType;
    AParagraphFormatting.LineSpacing := AParagraphFormatting.CalcLineSpacing(UnitConverter);
    ApplyParagraphProperties(AParagraphProperties, AParagraphFormatting, AParentParagraphProperties);
  finally
    AParentParagraphProperties.Free;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyTableCellProperties(
  ACellProperties: TdxTableCellProperties;
  AParentCellProperties: TdxMergedTableCellProperties);
begin
  Assert(False, 'not implemented');
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyTableProperties(ATableProperties: TdxTableProperties;
  AParentTableProperties: TdxMergedTableProperties);
var
  AParentTablePropertiesInfo: TdxCombinedTablePropertiesInfo;
  ATablePropertiesInfo: TdxCombinedTablePropertiesInfo;
begin
  ATableProperties.ResetAllUse;
  AParentTablePropertiesInfo := AParentTableProperties.Info;
  ATablePropertiesInfo := TdxCombinedTablePropertiesInfo.Create(ATableProperties); 
  try
    if not ATablePropertiesInfo.Borders.TopBorder.Equals(AParentTablePropertiesInfo.Borders.TopBorder) then
      ATableProperties.Borders.TopBorder.CopyFrom(ATablePropertiesInfo.Borders.TopBorder);
    if not ATablePropertiesInfo.Borders.RightBorder.Equals(AParentTablePropertiesInfo.Borders.RightBorder) then
      ATableProperties.Borders.RightBorder.CopyFrom(ATablePropertiesInfo.Borders.RightBorder);
    if not ATablePropertiesInfo.Borders.LeftBorder.Equals(AParentTablePropertiesInfo.Borders.LeftBorder) then
      ATableProperties.Borders.LeftBorder.CopyFrom(ATablePropertiesInfo.Borders.LeftBorder);
    if not ATablePropertiesInfo.Borders.BottomBorder.Equals(AParentTablePropertiesInfo.Borders.BottomBorder) then
      ATableProperties.Borders.BottomBorder.CopyFrom(ATablePropertiesInfo.Borders.BottomBorder);
    if not ATablePropertiesInfo.Borders.InsideHorizontalBorder.Equals(AParentTablePropertiesInfo.Borders.InsideHorizontalBorder) then
      ATableProperties.Borders.InsideHorizontalBorder.CopyFrom(ATablePropertiesInfo.Borders.InsideHorizontalBorder);
    if not ATablePropertiesInfo.Borders.InsideVerticalBorder.Equals(AParentTablePropertiesInfo.Borders.InsideVerticalBorder) then
      ATableProperties.Borders.InsideVerticalBorder.CopyFrom(ATablePropertiesInfo.Borders.InsideVerticalBorder);
    if not ATablePropertiesInfo.CellMargins.Top.Equals(AParentTablePropertiesInfo.CellMargins.Top) then
    begin
      ATableProperties.CellMargins.Top.CopyFrom(ATablePropertiesInfo.CellMargins.Top);
      if ATableProperties.CellMargins.Top.&Type = TdxWidthUnitType.&Nil then 
        ATableProperties.CellMargins.Top.&Type := TdxWidthUnitType.ModelUnits;
    end;
    if not ATablePropertiesInfo.CellMargins.Right.Equals(AParentTablePropertiesInfo.CellMargins.Right) then
    begin
      ATableProperties.CellMargins.Right.CopyFrom(ATablePropertiesInfo.CellMargins.Right);
      if ATableProperties.CellMargins.Right.&Type = TdxWidthUnitType.Nil then 
        ATableProperties.CellMargins.Right.&Type := TdxWidthUnitType.ModelUnits;
    end;
    if not ATablePropertiesInfo.CellMargins.Left.Equals(AParentTablePropertiesInfo.CellMargins.Left) then
    begin
      ATableProperties.CellMargins.Left.CopyFrom(ATablePropertiesInfo.CellMargins.Left);
      if ATableProperties.CellMargins.Left.&Type = TdxWidthUnitType.Nil then 
        ATableProperties.CellMargins.Left.&Type := TdxWidthUnitType.ModelUnits;
    end;
    if not ATablePropertiesInfo.CellMargins.Bottom.Equals(AParentTablePropertiesInfo.CellMargins.Bottom) then
    begin
      ATableProperties.CellMargins.Bottom.CopyFrom(ATablePropertiesInfo.CellMargins.Bottom);
      if ATableProperties.CellMargins.Bottom.&Type = TdxWidthUnitType.Nil then 
        ATableProperties.CellMargins.Bottom.&Type := TdxWidthUnitType.ModelUnits;
    end;
    if not ATablePropertiesInfo.CellSpacing.Equals(AParentTablePropertiesInfo.CellSpacing) then
      ATableProperties.CellSpacing.CopyFrom(ATablePropertiesInfo.CellSpacing);
    if not ATablePropertiesInfo.FloatingPosition.Equals(AParentTablePropertiesInfo.FloatingPosition) then
      ATableProperties.FloatingPosition.CopyFrom(ATablePropertiesInfo.FloatingPosition);
    if ATablePropertiesInfo.GeneralSettings.IsTableOverlap <> AParentTablePropertiesInfo.GeneralSettings.IsTableOverlap then
      ATableProperties.IsTableOverlap := ATablePropertiesInfo.GeneralSettings.IsTableOverlap;
    if ATablePropertiesInfo.GeneralSettings.TableLayout <> AParentTablePropertiesInfo.GeneralSettings.TableLayout then
      ATableProperties.TableLayout := ATablePropertiesInfo.GeneralSettings.TableLayout;
    if ATablePropertiesInfo.GeneralSettings.TableLook <> AParentTablePropertiesInfo.GeneralSettings.TableLook then
      ATableProperties.TableLook := ATablePropertiesInfo.GeneralSettings.TableLook;
    if ATablePropertiesInfo.GeneralSettings.TableStyleColBandSize <> AParentTablePropertiesInfo.GeneralSettings.TableStyleColBandSize then
      ATableProperties.TableStyleColBandSize := ATablePropertiesInfo.GeneralSettings.TableStyleColBandSize;
    if ATablePropertiesInfo.GeneralSettings.TableStyleRowBandSize <> AParentTablePropertiesInfo.GeneralSettings.TableStyleRowBandSize then
      ATableProperties.TableStyleRowBandSize := ATablePropertiesInfo.GeneralSettings.TableStyleRowBandSize;
    if not ATablePropertiesInfo.PreferredWidth.Equals(AParentTablePropertiesInfo.PreferredWidth) then
      ATableProperties.PreferredWidth.CopyFrom(ATablePropertiesInfo.PreferredWidth);
    if not ATablePropertiesInfo.TableIndent.Equals(AParentTablePropertiesInfo.TableIndent) then
      ATableProperties.TableIndent.CopyFrom(ATablePropertiesInfo.TableIndent);
  finally
    ATablePropertiesInfo.Free;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyTableRowProperties(
  ARowProperties: TdxTableRowProperties;
  AParentRowProperties: TdxMergedTableRowProperties);
begin
  Assert(False, 'not implemented');
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyParagraphFormatting(AParagraphIndex: TdxParagraphIndex);
var
  AParagraph: TdxParagraph;
  AParagraphFormatting: TdxRtfParagraphFormattingInfo;
  AListIndex: TdxNumberingListIndex;
begin
  AParagraph := PieceTable.Paragraphs[AParagraphIndex];
  AParagraphFormatting := Position.ParagraphFormattingInfo;
  ApplyParagraphFormattingCore(AParagraph, AParagraphFormatting);
  if TdxDocumentFormatsHelper.ShouldInsertNumbering(DocumentModel) then
  begin
    AListIndex := AParagraphFormatting.NumberingListIndex;
    if AListIndex >= 0 then
      AddNumberingListToParagraph(AParagraph, AListIndex, AParagraphFormatting.ListLevelIndex)
    else
      if not Position.CurrentOldListSkipNumbering then
      begin
        Position.CurrentOldMultiLevelListIndex := NumberingListIndexNoNumberingList;
        Position.CurrentOldSimpleListIndex := NumberingListIndexNoNumberingList
      end;
  end;
  AParagraph.SetOwnTabs(AParagraphFormatting.Tabs);
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplySectionFormatting(ASkipNumbering: Boolean);
var
  ASectionFormatting: TdxRtfSectionFormattingInfo;
  ASections: TdxSectionCollection;
  ASection: TdxSection;
  AParagraphFormatting: TdxRtfParagraphFormattingInfo;
  AListIndex: TdxNumberingListIndex;
  AParagraph: TdxParagraph;
begin
  ASectionFormatting := Position.SectionFormattingInfo;
  ASections := DocumentModel.Sections;
  ASection := ASections.Last;
  ASectionFormatting.Page.ValidatePaperKind(DocumentModel.UnitConverter);
  ASection.Page.CopyFrom(ASectionFormatting.Page);
  ASection.Margins.CopyFrom(ASectionFormatting.Margins);
  ASection.PageNumbering.CopyFrom(ASectionFormatting.PageNumbering);
  ASection.GeneralSettings.CopyFrom(ASectionFormatting.GeneralSectionInfo);
  ASection.LineNumbering.CopyFrom(ASectionFormatting.LineNumbering);
  ASection.Columns.CopyFrom(ASectionFormatting.Columns);
  ASection.FootNote.CopyFrom(ASectionFormatting.FootNote);
  ASection.EndNote.CopyFrom(ASectionFormatting.EndNote);

  AParagraphFormatting := Position.ParagraphFormattingInfo;
  AListIndex := AParagraphFormatting.NumberingListIndex;
  if not ASkipNumbering and (AListIndex >= 0) and TdxDocumentFormatsHelper.ShouldInsertNumbering(DocumentModel) then
  begin
    AParagraph := PieceTable.Paragraphs[Position.ParagraphIndex];
    if not AParagraph.IsInList then
      AddNumberingListToParagraph(AParagraph, AParagraphFormatting.NumberingListIndex, AParagraphFormatting.ListLevelIndex);
  end
  else
  begin
    Position.CurrentOldSimpleListIndex := NumberingListIndexNoNumberingList;
    Position.CurrentOldMultiLevelListIndex := NumberingListIndexNoNumberingList;
  end;
  if (ASections.Count = 1) or ASectionFormatting.RestartPageNumbering then
    ASection.PageNumbering.StartingPageNumber := ASectionFormatting.PageNumbering.StartingPageNumber
  else
    ASection.PageNumbering.StartingPageNumber := -1;
end;

function TdxRichEditDocumentModelRtfImporter.ReadByte(AStream: TStream; out AByte: Byte): Boolean;
begin
  Result := AStream.Read(AByte, 1) > 0;
  if not Result then
    AByte := 0;
end;

function TdxRichEditDocumentModelRtfImporter.ReadChar(AStream: TStream; out AChar: Char): Boolean;
var
  AOrd: Byte;
begin
  Result := ReadByte(AStream, AOrd);
  AChar := Char(AOrd)
end;

procedure TdxRichEditDocumentModelRtfImporter.ParseCharWithoutDecoding(AChar: WideChar);
begin
  FlushDecoder;
  if FSkipCount = 0 then
    Destination.ProcessChar(AChar);
  DecreaseSkipCount;
end;

procedure TdxRichEditDocumentModelRtfImporter.InitializeStateManager;
begin
  Assert(stateManager = nil, 'stateManager already created');
  FStateManager := TdxRichEditRtfParserStateManager.Create(Self); 
  FStateManager.Initialize;
end;

procedure TdxRichEditDocumentModelRtfImporter.ImportCore(AStream: TStream);
var
  AUpdateOptions: TdxFieldUpdateOnLoadOptions;
begin
  InitializeStateManager;
  DocumentModel.BeginSetContent;
  try
    FStream := AStream;
    try
      ImportRtfStream(AStream);
    finally
      FStream := nil;
    end;
  finally
    AUpdateOptions := Options.UpdateField.NativeOptions;
    try
      DocumentModel.EndSetContent(TdxDocumentModelChangeType.LoadNewDocument, True, Options.PasteFromIE, AUpdateOptions);
    finally
      AUpdateOptions.Free;
    end;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.ImportRtfStream(AStream: TStream);
var
  AByte: Byte;
  AChar: Char;
  ANextChar: Integer;
  ALastReadChar: Char;
  AOrdChar: Integer;
  AParsingState: TdxRichEditRtfParsingState;
  ALastDestination: TdxDefaultDestination;
begin
{$IFDEF AQTIME}
{$ENDIF}
  DocumentModel.DefaultCharacterProperties.FontName := 'Times New Roman';
  DocumentModel.DefaultCharacterProperties.DoubleFontSize := 24;
  ClearNumberingList;
  if not CheckSignature(AStream) then
    Exit;
  ReadChar(AStream, AChar); 
  ANextChar := -1;
  while True do
  begin
    if ANextChar = -1 then
    begin
      if not ReadByte(AStream, AByte) then
        AOrdChar := -1
      else
        AOrdChar := AByte;
    end
    else
    begin
      AOrdChar := ANextChar;
      ANextChar := -1;
    end;
    if AOrdChar < 0 then
      Break;
    AChar := Char(AOrdChar);
    AParsingState := StateManager.ParsingState;
    if AParsingState = TdxRichEditRtfParsingState.BinData then
      ParseBinChar(AChar)
    else
    begin
      case AChar of
        '{':
          begin
            FlushDecoder;
            PushRtfState;
          end;
        '}':
          begin
            FlushDecoder;
            if PopRtfState = TdxRichEditPopRtfState.StackEmpty then
              SkipDataBeyondOuterBrace(AStream);
          end;
        '\':
          begin
            ALastReadChar := ParseRtfKeyword(AStream);
            if ALastReadChar <> ' ' then
              ANextChar := Ord(ALastReadChar);
          end;
        #13:
          ParseCR();
        #10:
          ParseLF();
      else
        if AParsingState = TdxRichEditRtfParsingState.Normal then
          ParseChar(AChar)
        else
          ParseHexChar(AStream, AChar);
      end;
    end;
  end;
  if AChar = #0 then
  begin
    while StateManager.States.Count > 0 do
      PopRtfState;
  end;

  Assert(StateManager.States.Count = 0);
  Assert(Destination <> nil);
  ALastDestination := Destination as TdxDefaultDestination;
{$IFDEF AQTIME}
  EnableProfiling(True);
{$ENDIF}
  ALastDestination.FinalizePieceTableCreation;
{$IFDEF AQTIME}
  EnableProfiling(False);
{$ENDIF}
end;

class function TdxRichEditDocumentModelRtfImporter.IsAlpha(C: Char): Boolean;
begin
  Result := ((C >= 'a') and (C <= 'z')) or ((C >= 'A') and (C <= 'Z'));
end;

class function TdxRichEditDocumentModelRtfImporter.IsDigit(C: Char): Boolean;
begin
  Result := (C >= '0') and (C <= '9');
end;

procedure TdxRichEditDocumentModelRtfImporter.ParseCR;
begin
  Assert(StateManager.ParsingState = TdxRichEditRtfParsingState.Normal); 
end;

procedure TdxRichEditDocumentModelRtfImporter.ParseLF;
begin
  Assert(StateManager.ParsingState = TdxRichEditRtfParsingState.Normal); 
end;

function TdxRichEditDocumentModelRtfImporter.ParseRtfKeyword(AStream: TStream): Char;
var
  AIsNegative: Boolean;
  AHasParameter: Boolean;
  AParameterValue: Integer;
begin
  if not ReadChar(AStream, Result) then
  begin
    Result := #0;
    Exit;
  end;
  if not IsAlpha(Result) then
  begin
    TranslateControlChar(Result);
    Result := ' ';
    Exit;
  end
  else
    FlushDecoder;
  FKeyword.Length := 0;
  while IsAlpha(Result) do
  begin
    FKeyword.Append(Result);
    ReadChar(AStream, Result);
  end;
  FParameterValueString.Length := 0;
  AIsNegative := Result = '-';
  if AIsNegative then
  begin
    FParameterValueString.Append(Result);
    ReadChar(AStream, Result);
  end;
  if AIsNegative and not IsDigit(Result) then
    FParameterValueString.Length := 0
  else
    while IsDigit(Result) do
    begin
      FParameterValueString.Append(Result);
      ReadChar(AStream, Result);
    end;
  AHasParameter := FParameterValueString.Length > 0;
  if AHasParameter then
    AParameterValue := StrToInt(FParameterValueString.ToString)
  else
    AParameterValue := 0;
  TranslateKeyword(FKeyword.ToString, AParameterValue, AHasParameter);
end;

procedure TdxRichEditDocumentModelRtfImporter.ParseUnicodeChar(AChar: WideChar);
begin
  FlushDecoder;
  Destination.ProcessChar(AChar);
  FSkipCount := Position.RtfFormattingInfo.UnicodeCharacterByteCount;
end;

function TdxRichEditDocumentModelRtfImporter.PopRtfState: TdxRichEditPopRtfState;
const
  AResultMap: array[Boolean] of TdxRichEditPopRtfState = (TdxRichEditPopRtfState.StackEmpty, TdxRichEditPopRtfState.StackNonEmpty);
var
  AOldDestination: TdxRichEditRtfDestinationBase;
begin
  AOldDestination := Destination;
  if StateManager.States.Count = OptionalGroupLevel then
    OptionalGroupLevel := MaxInt;
  StateManager.PopState;
  if StateManager.States.Count >= OptionalGroupLevel then
    StateManager.Destination := TdxSkipDestination.Create(Self);
  AOldDestination.AfterPopRtfState;
  if AOldDestination <> Destination then
    AOldDestination.Free;
  FSkipCount := 0;
  Result := AResultMap[StateManager.States.Count > 0];
end;

procedure TdxRichEditDocumentModelRtfImporter.SetCodePage(ACodePage: Cardinal);
begin
  Position.RtfFormattingInfo.CodePage := ACodePage;
end;

procedure TdxRichEditDocumentModelRtfImporter.SetFont(AFontInfo: TdxRtfFontInfo);
var
  ACodePage: Integer;
begin
  if Position.IsDoubleByteCharactersFont then
  begin
    Position.DoubleByteCharactersFontName := AFontInfo.Name;
    Position.IsDoubleByteCharactersFont := False;
  end
  else
    Position.CharacterFormatting.FontName := AFontInfo.Name;
  ACodePage := CodePageFromCharset(AFontInfo.Charset);
  SetCodePage(ACodePage);
end;

function TdxRichEditDocumentModelRtfImporter.GetCharacterStyleIndex(ARtfCharacterStyleIndex: Integer): Integer;
begin
  if CharacterStyleCollectionIndex.ContainsKey(ARtfCharacterStyleIndex) then
    Result := CharacterStyleCollectionIndex[ARtfCharacterStyleIndex]
  else
    Result := 0;
end;

function TdxRichEditDocumentModelRtfImporter.GetParagraphStyleIndex(ARtfParagraphStyleIndex: Integer): Integer;
begin
  if ParagraphStyleCollectionIndex.ContainsKey(ARtfParagraphStyleIndex) then
    Result := ParagraphStyleCollectionIndex[ARtfParagraphStyleIndex]
  else
    Result := 0;
end;

function TdxRichEditDocumentModelRtfImporter.GetStyleMergedCharacterProperties(ARtfStyleIndex: Integer): TdxMergedCharacterProperties;
var
  AStyleIndex: Integer;
begin
  AStyleIndex := GetCharacterStyleIndex(ARtfStyleIndex);
  Result := DocumentModel.CharacterStyles[AStyleIndex].GetMergedCharacterProperties;
  GetStyleMergedCharacterProperties(Result);
end;

function TdxRichEditDocumentModelRtfImporter.GetStyleMergedCharacterProperties(AParentMergedProperties: TdxMergedCharacterProperties): TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  AMerger := TdxCharacterPropertiesMerger.Create(AParentMergedProperties);
  try
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxRichEditDocumentModelRtfImporter.GetStyleMergedParagraphCharacterProperties(ARtfCharacterStyleIndex, ARtfParagraphStyleIndex: Integer): TdxMergedCharacterProperties;
var
  AParagraphStyleIndex, ACharacterStyleIndex: Integer;
  ACharacterResult: TdxMergedCharacterProperties;
  AMerger: TdxCharacterPropertiesMerger;
  AParagraphResult: TdxMergedCharacterProperties;
  ADefaultProperties: TdxCharacterProperties;
begin
  AParagraphStyleIndex := GetParagraphStyleIndex(ARtfParagraphStyleIndex);
  ACharacterStyleIndex := GetCharacterStyleIndex(ARtfCharacterStyleIndex);
  ACharacterResult := DocumentModel.CharacterStyles[ACharacterStyleIndex].GetMergedCharacterProperties;
  AMerger := TdxCharacterPropertiesMerger.Create(ACharacterResult);
  try
    AParagraphResult := DocumentModel.ParagraphStyles[AParagraphStyleIndex].GetMergedCharacterProperties;
    try
      AMerger.Merge(AParagraphResult);
    finally
      AParagraphResult.Free;
    end;
    ADefaultProperties := DocumentModel.DefaultCharacterProperties;
    AMerger.Merge(ADefaultProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxRichEditDocumentModelRtfImporter.GetStyleMergedParagraphProperties(
  ARtfStyleIndex: Integer): TdxMergedParagraphProperties;
var
  AStyleIndex: Integer;
begin
  AStyleIndex := GetParagraphStyleIndex(ARtfStyleIndex);
  Result := GetStyleMergedParagraphProperties(DocumentModel.ParagraphStyles[AStyleIndex].GetMergedParagraphProperties);
end;

function TdxRichEditDocumentModelRtfImporter.GetStyleMergedParagraphProperties(
  AParentMergedProperties: TdxMergedParagraphProperties): TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  AMerger := TdxParagraphPropertiesMerger.Create(AParentMergedProperties);
  try
    AMerger.Merge(DocumentModel.DefaultParagraphProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxRichEditDocumentModelRtfImporter.GetStyleMergedTableCellProperties(
  AParentMergedProperties: TdxMergedTableCellProperties): TdxMergedTableCellProperties;
var
  AMerger: TdxTableCellPropertiesMerger;
begin
  AMerger := TdxTableCellPropertiesMerger.Create(AParentMergedProperties);
  try
    AMerger.Merge(DocumentModel.DefaultTableCellProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxRichEditDocumentModelRtfImporter.GetStyleMergedTableRowProperties(
  AParentMergedProperties: TdxMergedTableRowProperties): TdxMergedTableRowProperties;
var
  AMerger: TdxTableRowPropertiesMerger;
begin
  AMerger := TdxTableRowPropertiesMerger.Create(AParentMergedProperties);
  try
    AMerger.Merge(DocumentModel.DefaultTableRowProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxRichEditDocumentModelRtfImporter.GetTableStyleIndex(ARtfTableStyleIndex: Integer): Integer;
begin
  if TableStyleCollectionIndex.ContainsKey(ARtfTableStyleIndex) then
    Result := TableStyleCollectionIndex[ARtfTableStyleIndex]
  else
    Result := 0;
end;

procedure TdxRichEditDocumentModelRtfImporter.InsertParagraph;
var
  AInsertedParagraphIndex: TdxParagraphIndex;
begin
  AInsertedParagraphIndex := Position.ParagraphIndex;
  Position.ParagraphFormattingInfo.LineSpacing := Position.ParagraphFormattingInfo.CalcLineSpacing(UnitConverter);
  Position.ParagraphFormattingInfo.LineSpacingType := Position.ParagraphFormattingInfo.CalcLineSpacingType;
  PieceTable.InsertParagraphCore(Position);
  ApplyParagraphFormatting(AInsertedParagraphIndex);
end;

procedure TdxRichEditDocumentModelRtfImporter.TranslateKeyword(const AKeyword: string;
  AParameterValue: Integer; AHasParameter: Boolean);
var
  AKeywordProcessed: Boolean;
begin
  if (FSkipCount = 0) or (AKeyword = 'bin') then
  begin
    AKeywordProcessed := Destination.ProcessKeyword(AKeyword, AParameterValue, AHasParameter);
    StateManager.CheckSuperfluousDestinations;
    if AKeywordProcessed then
    begin
      if not (StateManager.Destination is TdxSkipDestination) then
        OptionalGroupLevel := MaxInt;
    end
    else
      if OptionalGroupLevel < MaxInt then
        StateManager.Destination := TdxSkipDestination.Create(Self);
  end
  else
    DecreaseSkipCount;
end;

function TdxRichEditDocumentModelRtfImporter.CheckSignature(var AStream: TStream): Boolean;
var
  ASignature: array[0..4] of Byte;
  ABytesRead: Integer;
begin
  AStream.Position := 0;
  ABytesRead := AStream.Read(ASignature, Length(ASignature));
  AStream.Seek(-ABytesRead, soCurrent);
  Result := (ABytesRead = Length(ASignature)) and
    (ASignature[0] = Ord('{')) and
    (ASignature[1] = Ord('\')) and
    (ASignature[2] = Ord('r')) and
    (ASignature[3] = Ord('t')) and
    (ASignature[4] = Ord('f'));
end;

procedure TdxRichEditDocumentModelRtfImporter.ClearNumberingList;
begin
  DocumentModel.NumberingLists.Clear;
end;

procedure TdxRichEditDocumentModelRtfImporter.DecreaseSkipCount;
begin
  FSkipCount := Max(0, FSkipCount - 1);
end;

procedure TdxRichEditDocumentModelRtfImporter.AddNumberingListToParagraph(AParagraph: TdxParagraph; AListIndex: TdxNumberingListIndex; ALevelIndex: Integer);
begin
  if not TdxDocumentFormatsHelper.ShouldInsertMultiLevelNumbering(DocumentModel) then
    ALevelIndex := 0;
  ConvertListLevelsToAnotherType(AListIndex);
  PieceTable.AddNumberingListToParagraph(AParagraph, AListIndex, ALevelIndex);
end;

procedure TdxRichEditDocumentModelRtfImporter.ApplyParagraphFormattingCore(AParagraph: TdxParagraph; AParagraphFormatting: TdxRtfParagraphFormattingInfo);
var
  AParagraphProperties: TdxParagraphProperties;
  AParagraphFormattingCacheIndex: Integer;
  AFormattingBase: TdxParagraphFormattingBase;
begin
  AParagraph.ParagraphStyleIndex := AParagraphFormatting.StyleIndex;
  AParagraphProperties := AParagraph.ParagraphProperties;
  Assert(not AParagraphProperties.IsUpdateLocked);
  AParagraphFormattingCacheIndex := DocumentModel.Cache.ParagraphFormattingInfoCache.GetItemIndex(AParagraphFormatting);
  if AParagraphFormattingCacheIndex = FLastParagraphFormattingCacheIndex then
    AParagraphProperties.SetIndexInitial(FLastParagraphPropertiesCacheIndex)
  else
  begin
    AFormattingBase := TdxParagraphFormattingBase.Create(PieceTable, DocumentModel,
      AParagraphFormattingCacheIndex, TdxParagraphFormattingOptionsCache.RootParagraphFormattingOptionIndex);
    try
      AParagraphProperties.ReplaceInfo(AFormattingBase, AParagraphProperties.GetBatchUpdateChangeActions);
    finally
      AFormattingBase.Free;
    end;
    FLastParagraphFormattingCacheIndex := AParagraphFormattingCacheIndex;
    FLastParagraphPropertiesCacheIndex := AParagraphProperties.Index;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.ConvertListLevelsToAnotherType(AListIndex: TdxNumberingListIndex);
var
  AList: TdxNumberingList;
  AType: TdxNumberingType;
begin
  AList := DocumentModel.NumberingLists[AListIndex];
  AType := TdxNumberingListHelper.GetListType(AList);
  if TdxDocumentFormatsHelper.NeedReplaceSimpleToBulletNumbering(DocumentModel) and (AType = TdxNumberingType.Simple) then
    ConvertListLevelsToAnotherTypeCore(AList.Levels, TdxCharacters.MiddleDot, 'Symbol', TdxNumberingFormat.Bullet)
  else
    if TdxDocumentFormatsHelper.NeedReplaceBulletedLevelsToDecimal(DocumentModel) and (AType = TdxNumberingType.Bullet) then
      ConvertListLevelsToAnotherTypeCore(AList.Levels, '%s.', DocumentModel.DefaultCharacterProperties.FontName, TdxNumberingFormat.Decimal);
end;

procedure TdxRichEditDocumentModelRtfImporter.ConvertListLevelsToAnotherTypeCore(ALevels: IdxReadOnlyIListLevelCollection; const ADisplayFormat, AFontName: string; AFormat: TdxNumberingFormat);
var
  I: Integer;
  ALevel: IdxListLevel;
begin
  for I := 0 to ALevels.Count - 1 do
  begin
    Supports(ALevels[I], IdxListLevel, ALevel);
    ALevel.ListLevelProperties.DisplayFormatString := ADisplayFormat;
    ALevel.CharacterProperties.FontName := AFontName;
    ALevel.ListLevelProperties.Format := AFormat;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.LinkParagraphStyleWithNumberingLists(AStyle: TdxParagraphStyle);
var
  AInfo: TdxRtfNumberingListInfo;
  AIndex: TdxNumberingListIndex;
begin
  if not ParagraphStyleListOverrideIndexMap.TryGetValue(AStyle, AInfo) then
    Exit;
  if not ListOverrideIndexToNumberingListIndexMap.TryGetValue(AInfo.RtfNumberingListIndex, AIndex) then
    Exit;

  if (AIndex < 0) or (AIndex >= DocumentModel.NumberingLists.Count) then
    Exit;
  AStyle.SetNumberingListIndex(AIndex);
  AStyle.SetNumberingListLevelIndex(AInfo.ListLevelIndex);
end;

procedure TdxRichEditDocumentModelRtfImporter.FlushDecoder;
begin
  Position.RtfFormattingInfo.Decoder.Flush(Self);
end;

procedure TdxRichEditDocumentModelRtfImporter.LinkParagraphStylesWithNumberingLists;
var
  I: Integer;
  AStyles: TdxParagraphStyleCollection;
begin
  AStyles := DocumentModel.ParagraphStyles;
  for I := 0 to AStyles.Count - 1 do
    LinkParagraphStyleWithNumberingLists(AStyles[I]);
end;

procedure TdxRichEditDocumentModelRtfImporter.PushRtfState;
begin
  StateManager.PushState;
end;

class procedure TdxRichEditDocumentModelRtfImporter.ThrowInvalidRtfFile;
begin
  Exception.Create('Invalid RTF file');
end;

procedure TdxRichEditDocumentModelRtfImporter.TranslateControlChar(AChar: Char);
begin
  if (FSkipCount = 0) or (AChar = '''') then
    Destination.ProcessControlChar(AChar)
  else
    DecreaseSkipCount;
end;

procedure TdxRichEditDocumentModelRtfImporter.SkipDataBeyondOuterBrace(AStream: TStream);
begin
  AStream.Seek(0, soEnd);
end;

procedure TdxRichEditDocumentModelRtfImporter.ParseBinChar(AChar: Char);
begin
  Destination.ProcessBinChar(AChar);
  Dec(FBinCharCount);
  if FBinCharCount <= 0 then
  begin
    StateManager.ParsingState := TdxRichEditRtfParsingState.Normal;
    DecreaseSkipCount;
  end;
end;

procedure TdxRichEditDocumentModelRtfImporter.ParseChar(AChar: Char);
begin
  if FSkipCount = 0 then
    Position.RtfFormattingInfo.Decoder.ProcessChar(Self, AChar)
  else
    DecreaseSkipCount;
end;

procedure TdxRichEditDocumentModelRtfImporter.ParseHexChar(AStream: TStream; AChar: Char);
var
  AHex: Integer;
  ACh: Byte;
begin
  AHex := dxHexToInt(AChar) shl 4;
  ReadByte(AStream, ACh);
  AHex := AHex + dxHexToInt(Char(ACh));
  StateManager.ParsingState := TdxRichEditRtfParsingState.Normal;
  ParseChar(Char(AHex));
end;

function TdxRichEditDocumentModelRtfImporter.GetDestination: TdxRichEditRtfDestinationBase;
begin
  Result := StateManager.Destination;
end;

function TdxRichEditDocumentModelRtfImporter.GetOptions: TdxRtfDocumentImporterOptions;
begin
  Result := TdxRtfDocumentImporterOptions(inherited Options);
end;

function TdxRichEditDocumentModelRtfImporter.GetTableReader: TdxRtfTableReader;
begin
  Result := PieceTableInfo.TableReader;
end;

function TdxRichEditDocumentModelRtfImporter.GetPieceTable: TdxPieceTable;
begin
  Result := PieceTableInfo.PieceTable;
end;

function TdxRichEditDocumentModelRtfImporter.GetPieceTableInfo: TdxRichEditRtfPieceTableInfo;
begin
  Result := StateManager.PieceTableInfo;
end;

function TdxRichEditDocumentModelRtfImporter.GetPosition: TdxRtfInputPosition;
begin
  Result := PieceTableInfo.Position;
end;

procedure TdxRichEditDocumentModelRtfImporter.SetDestination(const Value: TdxRichEditRtfDestinationBase);
begin
  StateManager.Destination := Value
end;

end.
