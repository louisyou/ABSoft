{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Commands.Insert;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Generics.Collections, dxRichEdit.DocumentModel.Core,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Control.HitTest, dxRichEdit.DocumentModel.PieceTableIterators,
  dxRichEdit.DocumentModel.Commands, dxRichEdit.Commands, dxRichEdit.View.Core, dxRichEdit.Commands.Selection,
  dxRichEdit.Commands.IDs, dxRichEdit.Commands.MultiCommand;

type
  { TdxTransactedInsertObjectCommand }

  TdxTransactedInsertObjectCommand = class abstract(TdxTransactedMultiCommand)
  protected
    procedure CreateCommands; override;
    function ExecutionMode: TdxMultiCommandExecutionMode; override;
    function UpdateUIStateMode: TdxMultiCommandUpdateUIStateMode; override;

    function CreateDeleteCommand: TdxCommand; virtual;
    function CreateInsertObjectCommand: TdxRichEditCommand; virtual; abstract;
    procedure CreateInsertObjectCommands; virtual;
    function InsertObjectCommand: TdxRichEditCommand; virtual;
    function KeepLastParagraphMarkInSelection: Boolean; virtual;
  public
    procedure UpdateUIState(const AState: IdxCommandUIState); override;
  end;

  { TdxInsertParagraphCommand }

  TdxInsertParagraphCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertTextCommand }

  TdxInsertTextCommand = class(TdxTransactedInsertObjectCommand)
  private
    function GetText: string;
    procedure SetText(const Value: string);
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    property Text: string read GetText write SetText;
  end;

  { TdxInsertObjectCommandBase }

  TdxInsertObjectCommandBase = class abstract(TdxRichEditSelectionCommand)
  protected
    function GetExtendSelection: Boolean; override;
    function GetTreatStartPositionAsCurrent: Boolean; override;
    function GetTryToKeepCaretX: Boolean; override;
    function GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
    function CanChangePosition(const APos: TdxDocumentModelPosition): Boolean; override;
    function ChangePosition(const APos: TdxDocumentModelPosition): Integer; override;
    procedure PerformModifyModel; override;
    procedure ModifyModel; virtual; abstract;
  end;

  { TdxInsertParagraphCoreCommand }

  TdxInsertParagraphCoreCommand = class(TdxInsertObjectCommandBase)
  strict private
    FOldInputPosition: TdxInputPosition;
  private
    function AllowAutoCorrect: Boolean;
    function CalculateTableCellToAdjustIndex(AParagraph: TdxParagraph; APos: TdxDocumentLogPosition): TdxTableCell;
  protected
    procedure AfterUpdate; override;
    procedure BeforeUpdate; override;
    procedure ModifyModel; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    destructor Destroy; override;
  end;

  { TdxInsertParagraphIntoNonEmptyParagraphCoreCommand }

  TdxInsertParagraphIntoNonEmptyParagraphCoreCommand = class(TdxInsertParagraphCoreCommand)
  protected
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  end;

    { TdxInsertTextCoreBaseCommand }

  TdxInsertTextCoreBaseCommand = class abstract(TdxInsertObjectCommandBase)
  protected
    procedure ModifyModel; override;
    function AllowAutoCorrect: Boolean; virtual;
    function GetInsertedText: string; virtual; abstract;
    procedure InsertTextCore; virtual;
    function ResetMerging: Boolean; virtual;
    procedure OnTextInserted(APos: TdxDocumentLogPosition; ALength: Integer); virtual;
  end;

  { TdxInsertTextCoreCommand }

  TdxInsertTextCoreCommand = class(TdxInsertTextCoreBaseCommand)
  private
    FText: string;
  protected
    function GetInsertedText: string; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    constructor Create(AControl: IdxRichEditControl; const AText: string); reintroduce;

    property Text: string read FText write FText;
  end;

  { TdxInsertLineBreakCommand }

  TdxInsertLineBreakCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertSpecialCharacterCommandBase }

  TdxInsertSpecialCharacterCommandBase = class abstract(TdxInsertTextCoreBaseCommand)
  protected
    function GetInsertedText: string; override;
    function GetCharacter: Char; virtual; abstract;
    property Character: Char read GetCharacter;
  end;

  { TdxInsertLineBreakCoreCommand }

  TdxInsertLineBreakCoreCommand = class(TdxInsertSpecialCharacterCommandBase)
  protected
    function GetCharacter: Char; override;
  end;

  { TdxInsertPageBreakCommand }

  TdxInsertPageBreakCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function InsertObjectCommand: TdxRichEditCommand; override;
    procedure CreateInsertObjectCommands; override;
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertPageBreakCoreCommand }

  TdxInsertPageBreakCoreCommand = class(TdxInsertSpecialCharacterCommandBase)
  protected
    function GetCharacter: Char; override;
    function ResetMerging: Boolean; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  end;

  { TdxOvertypeTextCommand }

  TdxOvertypeTextCommand = class(TdxInsertTextCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  end;

  { TdxOvertypeTextCoreCommand }

  TdxOvertypeTextCoreCommand = class(TdxInsertTextCoreCommand)
  private
    function CanDeleteRun(ARun: TdxTextRunBase): Boolean;
    function CalculateDeletedLength(AFirstDeletePos: TdxDocumentLogPosition; ALength: Integer): Integer;
  protected
    procedure OnTextInserted(APos: Integer; ALength: Integer); override;
  end;

implementation

uses
  Classes, RTLConsts, dxCoreClasses, dxRichEdit.Utils.BatchUpdateHelper, 
  Math, dxRichEdit.Commands.Delete, dxRichEdit.DocumentModel.Styles, dxRichEdit.Utils.Characters;

{ TdxTransactedInsertObjectCommand }

procedure TdxTransactedInsertObjectCommand.CreateCommands;
begin
  Commands.Add(CreateDeleteCommand);
  CreateInsertObjectCommands;
end;

function TdxTransactedInsertObjectCommand.CreateDeleteCommand: TdxCommand;
var
  ACommand: TdxDeleteNonEmptySelectionCommand;
begin
  if KeepLastParagraphMarkInSelection then
    ACommand := TdxDeleteSelectionKeepLastParagraphCommand.Create(RichEditControl)
  else
    ACommand := TdxDeleteNonEmptySelectionCommand.Create(RichEditControl);
  ACommand.RestoreInputPositionFormatting := True;
  Result := ACommand;
end;

procedure TdxTransactedInsertObjectCommand.CreateInsertObjectCommands;
begin
  Commands.Add(CreateInsertObjectCommand);
end;

function TdxTransactedInsertObjectCommand.ExecutionMode: TdxMultiCommandExecutionMode;
begin
  Result := TdxMultiCommandExecutionMode.ExecuteAllAvailable;
end;

function TdxTransactedInsertObjectCommand.InsertObjectCommand: TdxRichEditCommand;
begin
  if Commands.Count > 1 then
    Result := TdxRichEditCommand(Commands[1])
  else
    Result := nil;
end;

function TdxTransactedInsertObjectCommand.KeepLastParagraphMarkInSelection: Boolean;
begin
  Result := True;
end;

procedure TdxTransactedInsertObjectCommand.UpdateUIState(const AState: IdxCommandUIState);
var
  ACommandState: IdxCommandUIState;
begin
  if InsertObjectCommand <> nil then
  begin
    ACommandState := InsertObjectCommand.CreateDefaultCommandUIState;
    InsertObjectCommand.UpdateUIState(ACommandState);
    AState.Enabled := ACommandState.Enabled;
    AState.Visible := ACommandState.Visible;
  end
  else
    inherited UpdateUIState(AState);
end;

function TdxTransactedInsertObjectCommand.UpdateUIStateMode: TdxMultiCommandUpdateUIStateMode;
begin
  Result := TdxMultiCommandUpdateUIStateMode.EnableIfAnyAvailable;
end;

{ TdxInsertTextCommand }

function TdxInsertTextCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertTextCoreCommand.Create(RichEditControl, '');
end;

function TdxInsertTextCommand.GetText: string;
var
  ACommand: TdxInsertTextCoreCommand;
begin
  ACommand := TdxInsertTextCoreCommand(InsertObjectCommand);
  Result := ACommand.Text;
end;

procedure TdxInsertTextCommand.SetText(const Value: string);
var
  ACommand: TdxInsertTextCoreCommand;
begin
  ACommand := TdxInsertTextCoreCommand(InsertObjectCommand);
  ACommand.Text := Value;
end;

{ TdxInsertParagraphCommand }

function TdxInsertParagraphCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertParagraphCoreCommand.Create(RichEditControl);
end;

class function TdxInsertParagraphCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.InsertParagraph;
end;

{ TdxInsertParagraphCoreCommand }

destructor TdxInsertParagraphCoreCommand.Destroy;
begin
  FreeAndNil(FOldInputPosition);
  inherited Destroy;
end;

procedure TdxInsertParagraphCoreCommand.ModifyModel;
var
  APos, APrevPos: TdxDocumentLogPosition;
  AFilter: IVisibleTextFilter;
  AParagraph, ANewParagraph: TdxParagraph;
  AParagraphStyle, ANextParagraphStyle: TdxParagraphStyle;
  ACell: TdxTableCell;
  APieceTable: TdxPieceTable;
  ASourceRun: TdxTextRunBase;
begin
  if AllowAutoCorrect then
    ActivePieceTable.ApplyChangesCore([TdxDocumentModelChangeAction.ApplyAutoCorrect], -1, -1);
  APos := DocumentModel.Selection.&End;
  AFilter := DocumentModel.ActivePieceTable.VisibleTextFilter;
  if (APos <> 1) and not AFilter.IsRunVisible(0) then
  begin
    APrevPos := AFilter.GetPrevVisibleLogPosition(APos, False);
    if APrevPos = 0 then
      APos := APrevPos;
  end;
  AParagraph := ActivePieceTable.InsertParagraph(APos, GetForceVisible);
  ANewParagraph := ActivePieceTable.Paragraphs[AParagraph.Index + 1];
  if ANewParagraph.Length <= 1 then
  begin
    AParagraphStyle := AParagraph.ParagraphStyle;
    ANextParagraphStyle := AParagraphStyle.NextParagraphStyle;
    if (ANextParagraphStyle <> nil) and (ANextParagraphStyle <> AParagraphStyle) then
    begin
      ANewParagraph.ParagraphProperties.ResetAllUse;
      ANewParagraph.ParagraphStyleIndex := DocumentModel.ParagraphStyles.IndexOf(ANextParagraphStyle);
    end;
  end;
  ACell := CalculateTableCellToAdjustIndex(AParagraph, APos);
  if ACell <> nil then
  begin
    AParagraph.PieceTable.ChangeCellStartParagraphIndex(ACell, AParagraph.Index + 1);
    if AParagraph.IsInList then
      AParagraph.PieceTable.RemoveNumberingFromParagraph(AParagraph);
  end;
  if AParagraph.IsInList then
  begin
    APieceTable := AParagraph.PieceTable;
    ASourceRun := APieceTable.Runs[AParagraph.LastRunIndex];
    FOldInputPosition.CharacterStyleIndex := ASourceRun.CharacterStyleIndex;
    FOldInputPosition.CharacterFormatting.CopyFrom(ASourceRun.CharacterProperties.Info);
  end;
end;

procedure TdxInsertParagraphCoreCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  inherited UpdateUIStateCore(AState);
  ApplyCommandRestrictionOnEditableControl(AState, DocumentModel.DocumentCapabilities.Paragraphs, AState.Enabled);
  ApplyDocumentProtectionToSelectedCharacters(AState);
end;

procedure TdxInsertParagraphCoreCommand.AfterUpdate;
var
  ANewInputPosition: TdxInputPosition;
begin
  inherited AfterUpdate;
  UpdateCaretPosition(TdxDocumentLayoutDetailsLevel.Character);
  ANewInputPosition := CaretPosition.GetInputPosition;
  ANewInputPosition.CopyFormattingFrom(FOldInputPosition);
end;

function TdxInsertParagraphCoreCommand.AllowAutoCorrect: Boolean;
begin
  Result := CommandSourceType <> TdxCommandSourceType.Unknown;
end;

procedure TdxInsertParagraphCoreCommand.BeforeUpdate;
begin
  inherited BeforeUpdate;
  FOldInputPosition := CaretPosition.GetInputPosition.Clone;
end;

function TdxInsertParagraphCoreCommand.CalculateTableCellToAdjustIndex(
  AParagraph: TdxParagraph; APos: TdxDocumentLogPosition): TdxTableCell;
begin
  if APos <> AParagraph.LogPosition then
    Result := nil
  else
  begin
    if AParagraph.Index = 0 then
      Result := AParagraph.GetCell
    else
    begin
			Result := AParagraph.GetCell;
      if Result = nil then
        Exit;
      Assert(False);
    end;
  end;
end;

{ TdxInsertLineBreakCommand }

function TdxInsertLineBreakCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertLineBreakCoreCommand.Create(RichEditControl);
end;

class function TdxInsertLineBreakCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.InsertLineBreak;
end;

{ TdxInsertSpecialCharacterCommandBase }

function TdxInsertSpecialCharacterCommandBase.GetInsertedText: string;
begin
  Result := Character;
end;

{ TdxInsertLineBreakCoreCommand }

function TdxInsertLineBreakCoreCommand.GetCharacter: Char;
begin
  Result := TdxCharacters.LineBreak;
end;

{ TdxInsertPageBreakCommand }

function TdxInsertPageBreakCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertPageBreakCoreCommand.Create(RichEditControl);
end;

procedure TdxInsertPageBreakCommand.CreateInsertObjectCommands;
begin
  Commands.Add(TdxInsertParagraphIntoNonEmptyParagraphCoreCommand.Create(RichEditControl));
  inherited CreateInsertObjectCommands;
end;

class function TdxInsertPageBreakCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.InsertPageBreak;
end;

function TdxInsertPageBreakCommand.InsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxRichEditCommand(Commands[2]);
end;

procedure TdxInsertPageBreakCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  inherited UpdateUIStateCore(AState);
  if AState.Enabled then
    InsertObjectCommand.UpdateUIState(AState);
end;

{ TdxInsertPageBreakCoreCommand }

function TdxInsertPageBreakCoreCommand.GetCharacter: Char;
begin
  Result := TdxCharacters.PageBreak;
end;

function TdxInsertPageBreakCoreCommand.ResetMerging: Boolean;
begin
  Result := True;
end;

procedure TdxInsertPageBreakCoreCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  inherited UpdateUIStateCore(AState);
  if AState.Enabled then
    AState.Enabled := ActivePieceTable.IsMain and not IsSelectionEndInTableCell;
end;

{ TdxInsertParagraphIntoNonEmptyParagraphCoreCommand }

procedure TdxInsertParagraphIntoNonEmptyParagraphCoreCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
var
  AParagraphIndex: TdxParagraphIndex;
begin
  inherited UpdateUIStateCore(AState);
  if AState.Enabled then
  begin
    AParagraphIndex := CaretPosition.GetInputPosition.ParagraphIndex;
    AState.Enabled := ActivePieceTable.Paragraphs[AParagraphIndex].Length > 1;
  end;
end;

{ TdxInsertObjectCommandBase }

function TdxInsertObjectCommandBase.CanChangePosition(const APos: TdxDocumentModelPosition): Boolean;
begin
  Result := IsContentEditable and CanEditSelection;
end;

function TdxInsertObjectCommandBase.ChangePosition(const APos: TdxDocumentModelPosition): Integer;
begin
  Result := APos.LogPosition;
end;

function TdxInsertObjectCommandBase.GetExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxInsertObjectCommandBase.GetTreatStartPositionAsCurrent: Boolean;
begin
  Result := False;
end;

function TdxInsertObjectCommandBase.GetTryToKeepCaretX: Boolean;
begin
  Result := False;
end;

function TdxInsertObjectCommandBase.GetUpdateCaretPositionBeforeChangeSelectionDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

procedure TdxInsertObjectCommandBase.PerformModifyModel;
begin
  DocumentModel.BeginUpdate;
  try
    ModifyModel;
  finally
    DocumentModel.EndUpdate;
  end;
  ActiveView.EnforceFormattingCompleteForVisibleArea;
end;

{ TdxInsertTextCoreBaseCommand }

function TdxInsertTextCoreBaseCommand.AllowAutoCorrect: Boolean;
begin
  Result := CommandSourceType <> TdxCommandSourceType.Unknown;
end;

procedure TdxInsertTextCoreBaseCommand.InsertTextCore;
var
  AInsertedText: string;
  AForceVisible: Boolean;
  APos: TdxInputPosition;
  AInsertPosition: TdxDocumentLogPosition;
begin
  AInsertedText := GetInsertedText;
  if AInsertedText = '' then
    Exit;
  AForceVisible := GetForceVisible;
  if ResetMerging then
    DocumentModel.ResetMerging;
  if DocumentModel.Selection.Length > 0 then
  begin
    AInsertPosition := DocumentModel.Selection.&End;
    ActivePieceTable.InsertText(DocumentModel.Selection.&End, AInsertedText, AForceVisible);
    OnTextInserted(AInsertPosition, Length(AInsertedText));
    Exit;
  end;
  if CommandSourceType = TdxCommandSourceType.Keyboard then
    APos := CaretPosition.GetInputPosition
  else
    APos := CaretPosition.TryGetInputPosition;
  if APos <> nil then
  begin
    AInsertPosition := APos.LogPosition;
    ActivePieceTable.InsertText(APos, AInsertedText, AForceVisible);
    OnTextInserted(AInsertPosition, Length(AInsertedText));
  end
  else
  begin
    AInsertPosition := DocumentModel.Selection.&End;
    ActivePieceTable.InsertText(DocumentModel.Selection.&End, AInsertedText, AForceVisible);
    OnTextInserted(AInsertPosition, Length(AInsertedText));
  end;
  if ResetMerging then
    DocumentModel.ResetMerging;
end;

procedure TdxInsertTextCoreBaseCommand.ModifyModel;
begin
  if not ActivePieceTable.Runs[DocumentModel.Selection.Interval.&End.RunIndex].CanPlaceCaretBefore then
    Exit;
  if AllowAutoCorrect then
    ActivePieceTable.ApplyChangesCore([TdxDocumentModelChangeAction.ApplyAutoCorrect], -1, -1);
  InsertTextCore;
end;

function TdxInsertTextCoreBaseCommand.ResetMerging: Boolean;
begin
  Result := False;
end;

procedure TdxInsertTextCoreBaseCommand.OnTextInserted(APos: TdxDocumentLogPosition; ALength: Integer);
begin
//do nothing
end;

{ TdxInsertTextCoreCommand }

constructor TdxInsertTextCoreCommand.Create(AControl: IdxRichEditControl;
  const AText: string);
begin
  inherited Create(AControl);
  FText := AText;
end;

function TdxInsertTextCoreCommand.GetInsertedText: string;
begin
  Result := Text;
end;

procedure TdxInsertTextCoreCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  inherited UpdateUIStateCore(AState);
  if AState.Enabled then
    AState.Enabled := IsContentEditable and (Text <> '');
  ApplyDocumentProtectionToSelectedCharacters(AState);
end;

{ TdxOvertypeTextCommand }

function TdxOvertypeTextCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxOvertypeTextCoreCommand.Create(RichEditControl, '');
end;

{ TdxOvertypeTextCoreCommand }

function TdxOvertypeTextCoreCommand.CalculateDeletedLength(
  AFirstDeletePos: TdxDocumentLogPosition; ALength: Integer): Integer;
var
  AStartPos: TdxDocumentModelPosition;
  ATotalLength: Integer;
  ARunIndex, AMaxRunIndex: TdxRunIndex;
  ARun: TdxTextRunBase;
begin
  AStartPos := TdxPositionConverter.ToDocumentModelPosition(ActivePieceTable, AFirstDeletePos);
  ATotalLength := -AStartPos.RunOffset;
  ARunIndex := AStartPos.RunIndex;
  AMaxRunIndex := ActivePieceTable.Runs.Count - 1;
  while (ARunIndex <= AMaxRunIndex) and (ATotalLength < ALength) do
  begin
    ARun := ActivePieceTable.Runs[ARunIndex];
    if not CanDeleteRun(ARun) then
      Exit(ATotalLength);
    ATotalLength := ATotalLength + ARun.Length;
    Inc(ARunIndex);
  end;
  Result := Min(ATotalLength, ALength);
end;

function TdxOvertypeTextCoreCommand.CanDeleteRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := (ARun is TdxInlineObjectRun) or (ARun is TdxTextRun);
end;

procedure TdxOvertypeTextCoreCommand.OnTextInserted(APos, ALength: Integer);
var
  ADeleteStart: TdxDocumentLogPosition;
  ADeleteLength: Integer;
begin
  inherited OnTextInserted(APos, ALength);
  ADeleteStart := APos + ALength;
  ADeleteLength := CalculateDeletedLength(ADeleteStart, ALength);
  if ADeleteLength > 0 then
    ActivePieceTable.DeleteContent(APos + ALength, ADeleteLength, False, False, False, True, False);
end;

end.
