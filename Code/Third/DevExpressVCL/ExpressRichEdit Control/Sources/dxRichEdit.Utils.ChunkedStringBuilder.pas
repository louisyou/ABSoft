{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.ChunkedStringBuilder;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Math; 

type

  { TdxChunkedStringBuilder }

  TdxChunkedStringBuilder = class
  strict private const
    DefaultCapacity = $10000;
  strict private
    FCapacity: Integer;
    FBuffer: Pointer;
    FLength: Integer;
    FMinCapacity: Integer;
    procedure CheckLength(ANewLength: Integer); inline;
    function GetChar(AIndex: Integer): Char; inline;
    procedure SetCapacity(ANewCapacity: Integer); inline;
    procedure SetChar(AIndex: Integer; const AValue: Char); inline;
  protected
    property Capacity: Integer read FCapacity write SetCapacity;
    property MinCapacity: Integer read FMinCapacity;
  public
    constructor Create(ACapacity: Integer = 0);
    destructor Destroy; override;
    procedure Append(const ACharacter: Char); overload; inline;
    procedure Append(AValue: Integer); overload; inline;
    procedure Append(AText: PString); overload; inline;
    procedure Append(AStringBuilder: TdxChunkedStringBuilder); overload;
    procedure Clear;
    function ToString(AStartIndex, ALength: Integer): string; reintroduce; overload;
    function ToString: string; reintroduce; overload;

    property Chars[Index: Integer]: Char read GetChar write SetChar; default;
    property Length: Integer read FLength;
  end;

implementation

uses
  SysUtils;

{ TdxChunkedStringBuilder }

procedure TdxChunkedStringBuilder.SetCapacity(ANewCapacity: Integer);
begin
  if ANewCapacity <> FCapacity then
  begin
    FCapacity := ANewCapacity;
    ReallocMem(FBuffer, FCapacity);
  end;
end;

constructor TdxChunkedStringBuilder.Create(ACapacity: Integer = 0);
begin
  inherited Create;
  if ACapacity > 0 then
    FMinCapacity := ACapacity
  else
    FMinCapacity := DefaultCapacity;
end;

destructor TdxChunkedStringBuilder.Destroy;
begin
  Capacity := 0;
  inherited Destroy;
end;

procedure TdxChunkedStringBuilder.CheckLength(ANewLength: Integer);
begin
  while ANewLength * SizeOf(Char) > Capacity do
    Capacity := Max(MinCapacity, Capacity * 2);
end;

procedure TdxChunkedStringBuilder.Append(AText: PString);
var
  P: PChar;
  L: Integer;
begin
  L := System.Length(AText^);
  Assert(L > 0, 'Bad AText parameter: Length(AText^) = 0');
  CheckLength(FLength + L);
  P := FBuffer;
  Inc(P, FLength);
  Move(Pointer(AText^)^, P^, SizeOf(Char) * L); 
  Inc(FLength, L);
end;

procedure TdxChunkedStringBuilder.Append(AStringBuilder: TdxChunkedStringBuilder);
var
  AText: string;
begin
  if AStringBuilder.Length = 0 then
    Exit;

  AText := AStringBuilder.ToString;
  Append(@AText);
end;

procedure TdxChunkedStringBuilder.Append(const ACharacter: Char);
var
  P: PChar;
begin
  CheckLength(FLength + 1);
  P := FBuffer;
  Inc(P, FLength);
  P^ := ACharacter;
  Inc(FLength);
end;

procedure TdxChunkedStringBuilder.Append(AValue: Integer);
var
  AValueAsString: string;
begin
  AValueAsString := IntToStr(AValue);
  Append(@AValueAsString);
end;

procedure TdxChunkedStringBuilder.Clear;
begin
  FLength := 0;
  Capacity := MinCapacity;
end;

function TdxChunkedStringBuilder.GetChar(AIndex: Integer): Char;
var
  P: PChar;
begin
  Assert((AIndex >= 0) and (AIndex < FLength), Format('Bad Index parameter: GetChar(Index = %d) where Self.Length = %d', [AIndex, FLength]));
  P := FBuffer;
  Inc(P, AIndex);
  Result := P^;
end;

procedure TdxChunkedStringBuilder.SetChar(AIndex: Integer; const AValue: Char);
var
  P: PChar;
begin
  Assert((AIndex >= 0) and (AIndex < FLength), Format('Bad Index parameter: SetChar(Index = %d) where Self.Length = %d', [AIndex, FLength]));
  P := FBuffer;
  Inc(P, AIndex);
  P^ := AValue;
end;

function TdxChunkedStringBuilder.ToString(AStartIndex, ALength: Integer): string;
var
  P: PChar;
begin
  Assert((AStartIndex >= 0) and (AStartIndex < FLength), Format('Bad StartIndex parameter: ToString(StartIndex = %d) where Self.Length = %d', [AStartIndex, FLength]));
  Assert(AStartIndex + ALength <= FLength, Format('Bad Length parameter: ToString(StartIndex = %d, Length = %d) where Self.Length = %d', [AStartIndex, ALength, FLength]));
  P := FBuffer;
  Inc(P, AStartIndex);
  SetString(Result, P, ALength);
end;

function TdxChunkedStringBuilder.ToString: string;
var
  P: PChar;
begin
  P := FBuffer;
  SetString(Result, P, FLength);
end;

end.
