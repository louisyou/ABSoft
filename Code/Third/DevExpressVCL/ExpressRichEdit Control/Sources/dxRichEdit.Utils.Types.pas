{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.Types;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, dxCoreClasses, Generics.Collections;

type

  IdxComparable<T> = interface
    function CompareTo(const Value: T): Integer;
  end;

  IdxCloneable<T> = interface
    function Clone: T;
  end;

  IdxSupportsCopyFrom<T> = interface
    procedure CopyFrom(const Source: T);
  end;

  IdxSupportsSizeOf = interface
  end;

  IdxList<T> = interface
    function GetItem(Index: Integer): T;
    function GetCount: Integer;

    property Count: Integer read GetCount;
    property Items[Index: Integer]: T read GetItem; default;
  end;

  TdxAlgorithms = class
  public
    class function BinarySearch<T>(AList: TList<T>; const APredicate: IdxComparable<T>; AStartIndex, AEndIndex: Integer): Integer; overload;
    class function BinarySearch<T>(AList: TList<T>; const APredicate: IdxComparable<T>): Integer; overload;
  end;

implementation

{ TdxAlgoritms }

class function TdxAlgorithms.BinarySearch<T>(AList: TList<T>;
  const APredicate: IdxComparable<T>; AStartIndex, AEndIndex: Integer): Integer;
var
  ALow, AHi, AMedian: Integer;
  ACompareResult: Integer;
begin
  ALow := AStartIndex;
  AHi := AEndIndex;
  while ALow <= AHi do
  begin
    AMedian := (ALow + ((AHi - ALow) shr 1));
    ACompareResult := APredicate.CompareTo(AList[AMedian]);
    if ACompareResult = 0 then
      Exit(AMedian)
    else
      if ACompareResult < 0 then
        ALow := AMedian + 1
      else
        AHi := AMedian - 1;
  end;
  Result := not ALow;
end;

class function TdxAlgorithms.BinarySearch<T>(AList: TList<T>; const APredicate: IdxComparable<T>): Integer;
begin
  Result := TdxAlgorithms.BinarySearch<T>(AList, APredicate, 0, AList.Count - 1);
end;

end.
