{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.Units;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, cxGeometry;

function DocumentsToTwips(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToEmu(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToEmuL(const AValue: Int64): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToEmuF(const AValue: Single): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToTwipsL(const AValue: Int64): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToTwipsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToTwips(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToTwips(const AValue: TRect): TRect; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToTwips(const AValue: TdxRectF): TdxRectF; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPixels(const AValue: Integer; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPixels(const AValue: TPoint; const ADpiX, ADpiY: Single): TPoint; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPixels(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPixels(const AValue: TRect; const ADpiX, ADpiY: Single): TRect; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPixels(const AValue: TdxRectF; const ADpiX, ADpiY: Single): TdxRectF; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPixelsF(const AValue: Single; const ADpi: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPoints(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPointsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPointsFRound(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToHundredthsOfMillimeter(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToHundredthsOfMillimeter(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToHundredthsOfInch(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToHundredthsOfInch(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToCentimetersF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToInchesF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToMillimetersF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function DocumentsToPicasF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

function TwipsToDocuments(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToDocuments(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToEmu(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToEmuL(const AValue: Int64): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToEmuF(const AValue: Single): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToDocumentsL(const AValue: Int64): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToDocuments(const AValue: TRect): TRect; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToDocuments(const AValue: TdxRectF): TdxRectF; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPixels(const AValue: Integer; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPixelsL(const AValue: Int64; const ADpi: Single): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPixels(const AValue: TPoint; const ADpiX, ADpiY: Single): TPoint; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPixels(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPixels(const AValue: TRect; const ADpiX, ADpiY: Single): TRect; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPixelsF(const AValue: Single; const ADpi: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToHundredthsOfMillimeter(const AValue: TSize): TSize; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPointsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToPointsFRound(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToCentimetersF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToInchesF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToMillimetersF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToDocumentsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToHundredthsOfInch(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function TwipsToHundredthsOfInch(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}

function PixelsToPoints(const AValue: Integer; const ADpi: Single): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToPointsF(const AValue: Single; const ADpi: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocuments(const AValue: Integer; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocumentsF(const AValue: Single; const ADpi: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToTwips(const AValue: Integer; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToTwipsL(const AValue: Int64; const ADpi: Single): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToTwipsF(const AValue: Single; const ADpi: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocuments(const AValue: Double; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocuments(const ARect: TRect; ADpiX, ADpiY: Single): TRect; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocuments(const ARect: TdxRectF; ADpiX, ADpiY: Single): TdxRectF; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocuments(const ASize: TSize; ADpiX, ADpiY: Single): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocumentsRound(const ASize: TSize; ADpiX, ADpiY: Single): TSize; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToTwips(const ASize: TSize; ADpiX, ADpiY: Single): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToTwipsRound(const ASize: TSize; ADpiX, ADpiY: Single): TSize; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToTwips(const ARect: TRect; ADpiX, ADpiY: Single): TRect; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToDocuments(APoint: TPoint; ADpiX, ADpiY: Single): TPoint; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToHundredthsOfMillimeter(const AValue: Integer; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToHundredthsOfMillimeter(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToHundredthsOfInch(const AValue: Integer; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function PixelsToHundredthsOfInch(const AValue: TSize; const ADpi: Single): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}

function HundredthsOfMillimeterToDocuments(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfMillimeterToDocuments(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfMillimeterToDocumentsRound(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfMillimeterToTwips(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfMillimeterToTwips(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfMillimeterToTwipsRound(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfMillimeterToPixels(const AValue: Integer; const ADpi: Single): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfMillimeterToPixels(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}

function PointsToDocuments(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function PointsToDocumentsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function PointsToTwipsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function PointsToTwips(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function PointsToPixels(const AValue: Integer; const ADpi: Single): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function PointsToPixelsF(const AValue: Single; const ADpi: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

function EmuToDocuments(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function EmuToDocumentsL(const AValue: Int64): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function EmuToDocumentsF(const AValue: Integer): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function EmuToTwips(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function EmuToTwipsL(const AValue: Int64): Int64; {$IFDEF DELPHI9} inline; {$ENDIF}
function EmuToTwipsF(const AValue: Integer): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

function HundredthsOfInchToDocuments(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfInchToDocuments(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfInchToTwips(const AValue: Integer): Integer; overload; {$IFDEF DELPHI9} inline; {$ENDIF}
function HundredthsOfInchToTwips(const AValue: TSize): TSize; overload; {$IFDEF DELPHI9} inline; {$ENDIF}

function CentimetersToDocumentsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function CentimetersToTwipsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

function InchesToDocumentsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function InchesToTwipsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function InchesToPointsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

function MillimetersToDocumentsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function MillimetersToTwipsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

function PicasToDocumentsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}
function PicasToTwipsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

function MillimetersToPoints(const AValue: Integer): Integer; {$IFDEF DELPHI9} inline; {$ENDIF}
function MillimetersToPointsF(const AValue: Single): Single; {$IFDEF DELPHI9} inline; {$ENDIF}

implementation

uses
  Windows, Math;

function MulDiv(AValue, AMul, ADiv: Integer): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Trunc(AValue * AMul / ADiv);
end;

function MulDiv(AValue, AMul: Integer; ADiv: Single): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Trunc(AMul * AValue / ADiv);
end;

function MulDivL(AValue, AMul, ADiv: Int64): Int64; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Trunc(Int64(AValue * AMul) / ADiv);
end;

function MulDivL(AValue, AMul: Int64; ADiv: Single): Int64; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Trunc(Int64(AValue * AMul) / ADiv);
end;

function MulDivF(AValue, AMul, ADiv: Single): Single;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := (AMul * AValue) / ADiv;
end;

function PixelsToPointsCore(const AValue: Integer; const ADpi: Single): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := MulDiv(AValue, 72, ADpi);
end;

function PixelsToPointsCore(const AValue: Single; const ADpi: Single): Single; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := MulDivF(AValue, 72, ADpi);
end;

function PixelsToDocumentsCore(const AValue: Integer; const ADpi: Single): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := MulDiv(AValue, 300, ADpi);
end;

function PixelsToDocumentsCoreRound(const AValue: Integer; const ADpi: Single): Integer;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Round(MulDivF(AValue, 300, ADpi));
end;

function PixelsToTwipsCore(const AValue: Integer; const ADpi: Single): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := MulDiv(AValue, 1440, ADpi);
end;

function PixelsToTwipsCoreRound(const AValue: Integer; const ADpi: Single): Integer;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Round(MulDivF(AValue, 1440, ADpi));
end;

function PixelsToTwipsCore(const AValue: Int64; const ADpi: Single): Int64; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := MulDivL(AValue, 1440, ADpi);
end;

function PixelsToTwipsCore(const AValue: Single; const ADpi: Single): Single; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := MulDivF(AValue, 1440, ADpi);
end;

function PixelsToDocumentsCore(const AValue: Double; const ADpi: Single): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Round(300 * AValue / ADpi);
end;

function PixelsToDocumentsCoreF(const AValue: Single; const ADpi: Single): Single;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := MulDivF(AValue, 300, ADpi);
end;


function DocumentsToTwips(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 24, 5);
end;

function DocumentsToEmu(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 9144, 3);
end;

function DocumentsToEmuL(const AValue: Int64): Int64;
begin
  Result := MulDivL(AValue, 9144, 3);
end;

function DocumentsToEmuF(const AValue: Single): Integer;
begin
  Result := Trunc(MulDivF(AValue, 9144, 3));
end;

function DocumentsToTwipsL(const AValue: Int64): Int64;
begin
  Result := MulDivL(AValue, 24, 5);
end;

function DocumentsToTwipsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 24, 5);
end;

function DocumentsToTwips(const AValue: TSize): TSize;
begin
  Result := cxSize(DocumentsToTwips(AValue.cx), DocumentsToTwips(AValue.cy));
end;

function DocumentsToTwips(const AValue: TRect): TRect;
begin
  Result := Rect(DocumentsToTwips(AValue.Left), DocumentsToTwips(AValue.Top),
    DocumentsToTwips(AValue.Right), DocumentsToTwips(AValue.Bottom));
end;

function DocumentsToTwips(const AValue: TdxRectF): TdxRectF;
begin
  Result := dxRectF(DocumentsToTwipsF(AValue.Left), DocumentsToTwipsF(AValue.Top),
    DocumentsToTwipsF(AValue.Right), DocumentsToTwipsF(AValue.Bottom));
end;

function DocumentsToPixels(const AValue: Integer; const ADpi: Single): Integer;
begin
  if AValue >= 0 then
    Result := Trunc(ADpi * AValue / 300 + 0.99)
  else
    Result := Trunc(ADpi * AValue / 300 - 0.99);
end;

function DocumentsToPixels(const AValue: TPoint; const ADpiX, ADpiY: Single): TPoint;
begin
  Result := cxPoint(DocumentsToPixels(AValue.X, ADpiX), DocumentsToPixels(AValue.Y, ADpiY));
end;

function DocumentsToPixels(const AValue: TSize; const ADpiX, ADpiY: Single): TSize;
begin
  Result := cxSize(DocumentsToPixels(AValue.cx, ADpiX), DocumentsToPixels(AValue.cy, ADpiY));
end;

function DocumentsToPixels(const AValue: TRect; const ADpiX, ADpiY: Single): TRect;
begin
  Result := cxRect(DocumentsToPixels(AValue.Left, ADpiX), DocumentsToPixels(AValue.Top, ADpiY),
    DocumentsToPixels(AValue.Right, ADpiX), DocumentsToPixels(AValue.Bottom, ADpiY));
end;

function DocumentsToPixels(const AValue: TdxRectF; const ADpiX, ADpiY: Single): TdxRectF;
begin
  Result := dxRectF(DocumentsToPixelsF(AValue.Left, ADpiX), DocumentsToPixelsF(AValue.Top, ADpiY),
    DocumentsToPixelsF(AValue.Right, ADpiX), DocumentsToPixelsF(AValue.Bottom, ADpiY));
end;

function DocumentsToPixelsF(const AValue: Single; const ADpi: Single): Single;
begin
  Result := MulDivF(AValue, ADpi, 300);
end;

function DocumentsToPoints(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 6, 25);
end;

function DocumentsToPointsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 6, 25);
end;

function DocumentsToPointsFRound(const AValue: Single): Single;
begin
  Result := Round(DocumentsToPointsF(AValue));
end;

function DocumentsToHundredthsOfMillimeter(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 127, 15);
end;

function DocumentsToHundredthsOfMillimeter(const AValue: TSize): TSize;
begin
  Result := cxSize(DocumentsToHundredthsOfMillimeter(AValue.cx),
    DocumentsToHundredthsOfMillimeter(AValue.cy));
end;

function DocumentsToHundredthsOfInch(const AValue: Integer): Integer;
begin
  Result := Trunc(AValue / 3);
end;

function DocumentsToHundredthsOfInch(const AValue: TSize): TSize;
begin
  Result := cxSize(DocumentsToHundredthsOfInch(AValue.cx), DocumentsToHundredthsOfInch(AValue.cy));
end;

function DocumentsToCentimetersF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 127, 15000);
end;

function DocumentsToInchesF(const AValue: Single): Single;
begin
  Result := AValue / 300;
end;

function DocumentsToMillimetersF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 127, 1500);
end;

function DocumentsToPicasF(const AValue: Single): Single;
begin
  Result := AValue / 50;
end;

function TwipsToDocuments(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 5, 24);
end;

function TwipsToDocuments(const AValue: TSize): TSize;
begin
  Result := cxSize(TwipsToDocuments(AValue.cx), TwipsToDocuments(AValue.cy));
end;

function TwipsToEmu(const AValue: Integer): Integer;
begin
  Result := AValue * 635;
end;

function TwipsToEmuL(const AValue: Int64): Int64;
begin
  Result := AValue * 635;
end;

function TwipsToEmuF(const AValue: Single): Integer;
begin
  Result := Trunc(AValue * 635);
end;

function TwipsToDocumentsL(const AValue: Int64): Int64;
begin
  Result := MulDivL(AValue, 5, 24);
end;

function TwipsToDocuments(const AValue: TRect): TRect;
begin
  Result := cxRect(TwipsToDocuments(AValue.Left), TwipsToDocuments(AValue.Top),
    TwipsToDocuments(AValue.Right), TwipsToDocuments(AValue.Bottom));
end;

function TwipsToDocuments(const AValue: TdxRectF): TdxRectF;
begin
  Result := dxRectF(TwipsToDocumentsF(AValue.Left), TwipsToDocumentsF(AValue.Top),
    TwipsToDocumentsF(AValue.Right), TwipsToDocumentsF(AValue.Bottom));
end;

function TwipsToPixels(const AValue: Integer; const ADpi: Single): Integer;
begin
  Result := Trunc(ADpi * AValue / 1440 + 0.99);
end;

function TwipsToPixelsL(const AValue: Int64; const ADpi: Single): Int64;
begin
  Result := Trunc(ADpi * AValue / 1440 + 0.99);
end;

function TwipsToPixels(const AValue: TPoint; const ADpiX, ADpiY: Single): TPoint;
begin
  Result := cxPoint(TwipsToPixels(AValue.X, ADpiX), TwipsToPixels(AValue.Y, ADpiY));
end;

function TwipsToPixels(const AValue: TSize; const ADpiX, ADpiY: Single): TSize;
begin
  Result := cxSize(TwipsToPixels(AValue.cx, ADpiX), TwipsToPixels(AValue.cy, ADpiY));
end;

function TwipsToPixels(const AValue: TRect; const ADpiX, ADpiY: Single): TRect;
begin
  Result := cxRect(TwipsToPixels(AValue.Left, ADpiX), TwipsToPixels(AValue.Top, ADpiY),
    TwipsToPixels(AValue.Right, ADpiX), TwipsToPixels(AValue.Bottom, ADpiY));
end;

function TwipsToPixelsF(const AValue: Single; const ADpi: Single): Single;
begin
  Result := MulDivF(AValue, ADpi, 1440);
end;

function TwipsToHundredthsOfMillimeter(const AValue: TSize): TSize;
begin
  Result := cxSize(MulDiv(AValue.cx, 127, 72), MulDiv(AValue.cy, 127, 72));
end;

function TwipsToPointsF(const AValue: Single): Single;
begin
  Result := AValue / 20;
end;

function TwipsToPointsFRound(const AValue: Single): Single;
begin
  Result := Round(TwipsToPointsF(AValue));
end;

function TwipsToCentimetersF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 2.54, 1440);
end;

function TwipsToInchesF(const AValue: Single): Single;
begin
  Result := AValue / 1440;
end;

function TwipsToMillimetersF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 25.4, 1440);
end;

function TwipsToDocumentsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 5, 24);
end;

function TwipsToHundredthsOfInch(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 5, 72);
end;

function TwipsToHundredthsOfInch(const AValue: TSize): TSize;
begin
  Result := cxSize(TwipsToHundredthsOfInch(AValue.cx), TwipsToHundredthsOfInch(AValue.cy));
end;

function PixelsToPoints(const AValue: Integer; const ADpi: Single): Integer;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := PixelsToPointsCore(AValue, ADpi);
end;

function PixelsToPointsF(const AValue: Single; const ADpi: Single): Single;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := PixelsToPointsCore(AValue, ADpi);
end;

function PixelsToDocuments(const AValue: Integer; const ADpi: Single): Integer;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := PixelsToDocumentsCore(AValue, ADpi);
end;

function PixelsToDocumentsF(const AValue: Single; const ADpi: Single): Single;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := MulDivF(AValue, 300, ADpi);
end;

function PixelsToTwips(const AValue: Integer; const ADpi: Single): Integer;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := PixelsToTwipsCore(AValue, ADpi);
end;

function PixelsToTwipsL(const AValue: Int64; const ADpi: Single): Int64;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := PixelsToTwipsCore(AValue, ADpi);
end;

function PixelsToTwipsF(const AValue: Single; const ADpi: Single): Single;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := PixelsToTwipsCore(AValue, ADpi);
end;

function PixelsToDocuments(const AValue: Double; const ADpi: Single): Integer;
begin
  if ADpi = 0 then
    Result := 0
  else
    Result := PixelsToDocumentsCore(AValue, ADpi);
end;

function PixelsToDocuments(const ARect: TRect; ADpiX, ADpiY: Single): TRect;
begin
  if ADpiX = 0 then
    ADpiX := 300;
  if ADpiY = 0 then
    ADpiY := 300;
  Result := cxRect(PixelsToDocumentsCore(ARect.Left, ADpiX), PixelsToDocumentsCore(ARect.Top, ADpiY),
    PixelsToDocumentsCore(ARect.Right, ADpiX), PixelsToDocumentsCore(ARect.Bottom, ADpiY));
end;

function PixelsToDocuments(const ARect: TdxRectF; ADpiX, ADpiY: Single): TdxRectF;
begin
  if ADpiX = 0 then
    ADpiX := 300;
  if ADpiY = 0 then
    ADpiY := 300;
  Result := dxRectF(PixelsToDocumentsCoreF(ARect.Left, ADpiX), PixelsToDocumentsCoreF(ARect.Top, ADpiY),
    PixelsToDocumentsCoreF(ARect.Right, ADpiX), PixelsToDocumentsCoreF(ARect.Bottom, ADpiY));
end;

function PixelsToDocuments(const ASize: TSize; ADpiX, ADpiY: Single): TSize;
begin
  if ADpiX = 0 then
    ADpiX := 300;
  if ADpiY = 0 then
    ADpiY := 300;
  Result := cxSize(PixelsToDocumentsCore(ASize.cx, ADpiX), PixelsToDocumentsCore(ASize.cy, ADpiY));
end;

function PixelsToDocumentsRound(const ASize: TSize; ADpiX, ADpiY: Single): TSize;
begin
  if ADpiX = 0 then
    ADpiX := 300;
  if ADpiY = 0 then
    ADpiY := 300;
  Result := cxSize(PixelsToDocumentsCoreRound(ASize.cx, ADpiX), PixelsToDocumentsCoreRound(ASize.cy, ADpiY));
end;

function PixelsToTwips(const ASize: TSize; ADpiX, ADpiY: Single): TSize;
begin
  if ADpiX = 0 then
    ADpiX := 1440;
  if ADpiY = 0 then
    ADpiY := 1440;
  Result := cxSize(PixelsToTwipsCore(ASize.cx, ADpiX), PixelsToTwipsCore(ASize.cy, ADpiY));
end;

function PixelsToTwipsRound(const ASize: TSize; ADpiX, ADpiY: Single): TSize;
begin
  if ADpiX = 0 then
    ADpiX := 1440;
  if ADpiY = 0 then
    ADpiY := 1440;
  Result := cxSize(PixelsToTwipsCoreRound(ASize.cx, ADpiX), PixelsToTwipsCoreRound(ASize.cy, ADpiY));
end;

function PixelsToTwips(const ARect: TRect; ADpiX, ADpiY: Single): TRect;
begin
  if ADpiX = 0 then
    ADpiX := 1440;
  if ADpiY = 0 then
    ADpiY := 1440;
  Result := cxRect(PixelsToTwipsCore(ARect.Left, ADpiX), PixelsToTwipsCore(ARect.Top, ADpiY),
    PixelsToTwipsCore(ARect.Right, ADpiX), PixelsToTwipsCore(ARect.Bottom, ADpiY));
end;

function PixelsToDocuments(APoint: TPoint; ADpiX, ADpiY: Single): TPoint;
begin
  if ADpiX = 0 then
    ADpiX := 300;
  if ADpiY = 0 then
    ADpiY := 300;
  Result := cxPoint(PixelsToDocumentsCore(APoint.X, ADpiX), PixelsToDocumentsCore(APoint.Y, ADpiY));
end;

function PixelsToHundredthsOfMillimeter(const AValue: Integer; const ADpi: Single): Integer;
begin
  Result := Round(2540 * (AValue / ADpi));
end;

function PixelsToHundredthsOfMillimeter(const AValue: TSize; const ADpiX, ADpiY: Single): TSize;
begin
  Result := cxSize(PixelsToHundredthsOfMillimeter(AValue.cx, ADpiX),
    PixelsToHundredthsOfMillimeter(AValue.cy, ADpiY));
end;

function PixelsToHundredthsOfInch(const AValue: Integer; const ADpi: Single): Integer;
begin
  Result := MulDiv(AValue, 100, ADpi);
end;

function PixelsToHundredthsOfInch(const AValue: TSize; const ADpi: Single): TSize;
begin
  Result := cxSize(PixelsToHundredthsOfInch(AValue.cx, ADpi),
    PixelsToHundredthsOfInch(AValue.cy, ADpi));
end;

function HundredthsOfMillimeterToDocuments(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 15, 127);
end;

function HundredthsOfMillimeterToDocuments(const AValue: TSize): TSize;
begin
  Result := cxSize(HundredthsOfMillimeterToDocuments(AValue.cx),
    HundredthsOfMillimeterToDocuments(AValue.cy));
end;

function HundredthsOfMillimeterToDocumentsRound(const AValue: Integer): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Round(MulDivF(AValue, 15, 127));
end;

function HundredthsOfMillimeterToDocumentsRound(const AValue: TSize): TSize; overload;
begin
  Result := cxSize(HundredthsOfMillimeterToDocumentsRound(AValue.cx),
    HundredthsOfMillimeterToDocumentsRound(AValue.cy));
end;

function HundredthsOfMillimeterToTwips(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 72, 127);
end;

function HundredthsOfMillimeterToTwips(const AValue: TSize): TSize;
begin
  Result := cxSize(HundredthsOfMillimeterToTwips(AValue.cx),
    HundredthsOfMillimeterToTwips(AValue.cy));
end;

function HundredthsOfMillimeterToTwipsRound(const AValue: Integer): Integer; overload;{$IFDEF DELPHI9} inline; {$ENDIF}
begin
  Result := Round(MulDivF(AValue, 72, 127));
end;

function HundredthsOfMillimeterToTwipsRound(const AValue: TSize): TSize; overload;
begin
  Result := cxSize(HundredthsOfMillimeterToTwipsRound(AValue.cx),
    HundredthsOfMillimeterToTwipsRound(AValue.cy));
end;

function HundredthsOfMillimeterToPixels(const AValue: Integer; const ADpi: Single): Integer; overload;
begin
  Result := Round(MulDivF(AValue, ADpi, 2540));
end;

function HundredthsOfMillimeterToPixels(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload;
begin
  Result := cxSize(HundredthsOfMillimeterToPixels(AValue.cx, ADpiX),
    HundredthsOfMillimeterToPixels(AValue.cy, ADpiY));
end;

function PointsToDocuments(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 25, 6);
end;

function PointsToDocumentsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 25, 6);
end;

function PointsToTwipsF(const AValue: Single): Single;
begin
  Result := AValue * 20;
end;

function PointsToTwips(const AValue: Integer): Integer;
begin
  Result := AValue * 20;
end;

function PointsToPixels(const AValue: Integer; const ADpi: Single): Integer;
begin
  if AValue >= 0 then
    Result := Trunc((ADpi * ((AValue / 72) + 0.99)))
  else
    Result := Trunc((ADpi * ((AValue / 72) - 0.99)));
end;

function PointsToPixelsF(const AValue: Single; const ADpi: Single): Single;
begin
  Result := MulDivF(AValue, ADpi, 72);
end;

function EmuToDocuments(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 3, 9144);
end;

function EmuToDocumentsL(const AValue: Int64): Int64;
begin
  Result := MulDivL(AValue, 3, 9144);
end;

function EmuToDocumentsF(const AValue: Integer): Single;
begin
  Result := MulDivF(AValue, 3, 9144);
end;

function EmuToTwips(const AValue: Integer): Integer;
begin
  Result := Trunc(AValue / 635);
end;

function EmuToTwipsL(const AValue: Int64): Int64;
begin
  Result := Trunc(AValue / 635);
end;

function EmuToTwipsF(const AValue: Integer): Single;
begin
  Result := AValue / 635.0;
end;

function HundredthsOfInchToDocuments(const AValue: Integer): Integer;
begin
  Result := AValue * 3;
end;

function HundredthsOfInchToDocuments(const AValue: TSize): TSize;
begin
  Result := cxSize(HundredthsOfInchToDocuments(AValue.cx), HundredthsOfInchToDocuments(AValue.cy));
end;

function HundredthsOfInchToTwips(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 72, 5);
end;

function HundredthsOfInchToTwips(const AValue: TSize): TSize;
begin
  Result := cxSize(HundredthsOfInchToTwips(AValue.cx), HundredthsOfInchToTwips(AValue.cy));
end;

function CentimetersToDocumentsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 15000, 127);
end;

function CentimetersToTwipsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 1440, 2.54);
end;

function InchesToDocumentsF(const AValue: Single): Single;
begin
  Result := 300 * AValue;
end;

function InchesToTwipsF(const AValue: Single): Single;
begin
  Result := 1440 * AValue;
end;

function InchesToPointsF(const AValue: Single): Single;
begin
  Result := 72 * AValue;
end;

function MillimetersToDocumentsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 1500, 127);
end;

function MillimetersToTwipsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 1440, 25.4);
end;

function PicasToDocumentsF(const AValue: Single): Single;
begin
  Result := 50 * AValue;
end;

function PicasToTwipsF(const AValue: Single): Single;
begin
  Result := 240 * AValue;
end;

function MillimetersToPoints(const AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 360, 127);
end;

function MillimetersToPointsF(const AValue: Single): Single;
begin
  Result := MulDivF(AValue, 72, 25.4);
end;


end.
