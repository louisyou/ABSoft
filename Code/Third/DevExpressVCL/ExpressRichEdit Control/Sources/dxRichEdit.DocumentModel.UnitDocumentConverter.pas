{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.UnitDocumentConverter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, dxRichEdit.DocumentLayout.UnitConverter, dxRichEdit.DocumentModel.UnitConverter,
  dxRichEdit.DocumentModel.UnitToLayoutUnitConverter;

type

  { TdxDocumentModelUnitDocumentConverter }

  TdxDocumentModelUnitDocumentConverter = class(TdxDocumentModelUnitConverter)
  public
    function CreateConverterToLayoutUnits(const AUnit: TdxDocumentLayoutUnit; const ADpi: Single): TdxDocumentModelUnitToLayoutUnitConverter; override;
    function TwipsToModelUnits(const AValue: Integer): Integer; overload; override;
    function TwipsToModelUnits(const AValue: TSize): TSize; overload; override;
    function MillimetersToModelUnitsF(const AValue: Single): Single; override;
    function PointsToModelUnits(const AValue: Integer): Integer; override;
    function PointsToModelUnitsF(const AValue: Single): Single; override;
    function PixelsToModelUnits(const AValue: Integer; const ADpi: Single): Integer; overload; override;
    function PixelsToModelUnits(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; override;
    function HundredthsOfInchToModelUnits(const AValue: Integer): Integer; overload; override;
    function HundredthsOfInchToModelUnits(const AValue: TSize): TSize; overload; override;
    function HundredthsOfMillimeterToModelUnits(const AValue: Integer): Integer; overload; override;
    function HundredthsOfMillimeterToModelUnits(const AValue: TSize): TSize; overload; override;
    function HundredthsOfMillimeterToModelUnitsRound(const AValue: Integer): Integer; override;
    function CentimetersToModelUnitsF(const AValue: Single): Single; override;
    function InchesToModelUnitsF(const AValue: Single): Single; override;
    function PicasToModelUnitsF(const AValue: Single): Single; override;
    function DocumentsToModelUnits(const AValue: Integer): Integer; overload; override;
    function DocumentsToModelUnits(const AValue: TSize): TSize; overload; override;
    function DocumentsToModelUnitsF(const AValue: Single): Single; override;
    function ModelUnitsToTwips(const AValue: Integer): Integer; overload; override;
    function ModelUnitsToTwipsF(const AValue: Single): Single; override;
    function ModelUnitsToTwips(const AValue: TSize): TSize; overload; override;
    function ModelUnitsToHundredthsOfMillimeter(const AValue: TSize): TSize; override;
    function ModelUnitsToPointsF(const AValue: Single): Single; override;
    function ModelUnitsToPointsFRound(const AValue: Single): Single; override;
    function ModelUnitsToPixels(const AValue: Integer; const ADpi: Single): Integer; override;
    function ModelUnitsToPixelsF(const AValue: Single; const ADpi: Single): Single; override;
    function ModelUnitsToCentimetersF(const AValue: Single): Single; override;
    function ModelUnitsToInchesF(const AValue: Single): Single; override;
    function ModelUnitsToMillimetersF(const AValue: Single): Single; override;
    function ModelUnitsToDocumentsF(const AValue: Single): Single; override;
    function ModelUnitsToHundredthsOfInch(const AValue: Integer): Integer; overload; override;
    function ModelUnitsToHundredthsOfInch(const AValue: TSize): TSize; overload; override;
    function EmuToModelUnits(const AValue: Integer): Integer; override;
    function EmuToModelUnitsL(const AValue: Int64): Int64; override;
    function EmuToModelUnitsF(const AValue: Integer): Single; override;
    function ModelUnitsToEmu(const AValue: Integer): Integer; override;
    function ModelUnitsToEmuL(const AValue: Int64): Int64; override;
    function ModelUnitsToEmuF(const AValue: Single): Integer; override;
    function FDToModelUnits(const AValue: Integer): Integer; override;
    function ModelUnitsToFD(const AValue: Integer): Integer; override;
  end;

implementation

uses
  cxGeometry, dxRichEdit.Utils.Units, dxRichEdit.DocumentModel.DocumentsToLayoutDocumentsConverter,
  dxRichEdit.DocumentModel.DocumentsToLayoutPixelsConverter, dxRichEdit.DocumentModel.DocumentsToLayoutTwipsConverter;

{ TdxDocumentModelUnitDocumentConverter }

function TdxDocumentModelUnitDocumentConverter.CreateConverterToLayoutUnits(const AUnit: TdxDocumentLayoutUnit;
  const ADpi: Single): TdxDocumentModelUnitToLayoutUnitConverter;
begin
  case AUnit of
    TdxDocumentLayoutUnit.Pixel:
      Result := TdxDocumentModelDocumentsToLayoutPixelsConverter.Create(ADpi);
    TdxDocumentLayoutUnit.Document:
      Result := TdxDocumentModelDocumentsToLayoutDocumentsConverter.Create;
  else
    Result := TdxDocumentModelDocumentsToLayoutTwipsConverter.Create;
  end;
end;

function TdxDocumentModelUnitDocumentConverter.TwipsToModelUnits(const AValue: Integer): Integer;
begin
  Result := TwipsToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.TwipsToModelUnits(const AValue: TSize): TSize;
begin
  Result := TwipsToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.MillimetersToModelUnitsF(const AValue: Single): Single;
begin
  Result := MillimetersToDocumentsF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.PointsToModelUnits(const AValue: Integer): Integer;
begin
  Result := PointsToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.PointsToModelUnitsF(const AValue: Single): Single;
begin
  Result := PointsToDocumentsF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.PixelsToModelUnits(const AValue: Integer; const ADpi: Single): Integer;
begin
  Result := PixelsToDocuments(AValue, ADpi);
end;

function TdxDocumentModelUnitDocumentConverter.PixelsToModelUnits(const AValue: TSize; const ADpiX, ADpiY: Single): TSize;
begin
  Result := PixelsToDocuments(AValue, ADpiX, ADpiY);
end;

function TdxDocumentModelUnitDocumentConverter.HundredthsOfInchToModelUnits(const AValue: Integer): Integer;
begin
  Result := HundredthsOfInchToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.HundredthsOfInchToModelUnits(const AValue: TSize): TSize;
begin
  Result := HundredthsOfInchToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.HundredthsOfMillimeterToModelUnits(const AValue: Integer): Integer;
begin
  Result := HundredthsOfMillimeterToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.HundredthsOfMillimeterToModelUnits(const AValue: TSize): TSize;
begin
  Result := HundredthsOfMillimeterToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.HundredthsOfMillimeterToModelUnitsRound(const AValue: Integer): Integer;
begin
  Result := Round(300 * (AValue / 2540));
end;

function TdxDocumentModelUnitDocumentConverter.CentimetersToModelUnitsF(const AValue: Single): Single;
begin
  Result := CentimetersToDocumentsF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.InchesToModelUnitsF(const AValue: Single): Single;
begin
  Result := InchesToDocumentsF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.PicasToModelUnitsF(const AValue: Single): Single;
begin
  Result := PicasToDocumentsF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.DocumentsToModelUnits(const AValue: Integer): Integer;
begin
  Result := AValue;
end;

function TdxDocumentModelUnitDocumentConverter.DocumentsToModelUnits(const AValue: TSize): TSize;
begin
  Result := AValue;
end;

function TdxDocumentModelUnitDocumentConverter.DocumentsToModelUnitsF(const AValue: Single): Single;
begin
  Result := AValue;
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToTwips(const AValue: Integer): Integer;
begin
  Result := DocumentsToTwips(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToTwipsF(const AValue: Single): Single;
begin
  Result := DocumentsToTwipsF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToTwips(const AValue: TSize): TSize;
begin
  Result := DocumentsToTwips(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToHundredthsOfMillimeter(const AValue: TSize): TSize;
begin
  Result := DocumentsToHundredthsOfMillimeter(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToPointsF(const AValue: Single): Single;
begin
  Result := DocumentsToPointsF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToPointsFRound(const AValue: Single): Single;
begin
  Result := DocumentsToPointsFRound(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToPixels(const AValue: Integer; const ADpi: Single): Integer;
begin
  Result := DocumentsToPixels(AValue, ADpi);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToPixelsF(const AValue: Single; const ADpi: Single): Single;
begin
  Result := DocumentsToPixelsF(AValue, ADpi);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToCentimetersF(const AValue: Single): Single;
begin
  Result := DocumentsToCentimetersF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToInchesF(const AValue: Single): Single;
begin
  Result := DocumentsToInchesF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToMillimetersF(const AValue: Single): Single;
begin
  Result := DocumentsToMillimetersF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToDocumentsF(const AValue: Single): Single;
begin
  Result := AValue;
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToHundredthsOfInch(const AValue: Integer): Integer;
begin
  Result := DocumentsToHundredthsOfInch(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToHundredthsOfInch(const AValue: TSize): TSize;
begin
  Result := DocumentsToHundredthsOfInch(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.EmuToModelUnits(const AValue: Integer): Integer;
begin
  Result := EmuToDocuments(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.EmuToModelUnitsL(const AValue: Int64): Int64;
begin
  Result := EmuToDocumentsL(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.EmuToModelUnitsF(const AValue: Integer): Single;
begin
  Result := EmuToDocumentsL(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToEmu(const AValue: Integer): Integer;
begin
  Result := DocumentsToEmu(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToEmuL(const AValue: Int64): Int64;
begin
  Result := DocumentsToEmuL(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToEmuF(const AValue: Single): Integer;
begin
  Result := DocumentsToEmuF(AValue);
end;

function TdxDocumentModelUnitDocumentConverter.FDToModelUnits(const AValue: Integer): Integer;
begin
  Result := Trunc(AValue * (60000 / 65536));
end;

function TdxDocumentModelUnitDocumentConverter.ModelUnitsToFD(const AValue: Integer): Integer;
begin
  Result := Trunc(AValue * (65536 / 60000));
end;

end.
