{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Platform.Font;

interface

{$I cxVer.inc}

{$SCOPEDENUMS ON}

uses
  Types, Windows, Graphics, dxCoreClasses, Classes, dxRichEdit.DocumentLayout.UnitConverter,
  Generics.Collections;

type
  TdxFontInfo = class;
  TdxFontCache = class;
  TdxFontInfoMeasurer = class;

  TdxTextDirection = (
    LeftToRightTopToBottom        = 0,
    TopToBottomRightToLeft        = 1,
    TopToBottomLeftToRightRotated = 2,
    BottomToTopLeftToRight        = 3,
    LeftToRightTopToBottomRotated = 4,
    TopToBottomRightToLeftRotated = 5
  );

  TdxVerticalAlignment = (Default, Bottom, Center, Top);

  { TdxFontCharacterSet }

  TdxFontCharacterRange = record
    Low: Integer;
    Hi: Integer;
    procedure CopyFrom(ALow, AHi: Integer); overload;
    procedure CopyFrom(const ARange: TWCRange); overload;
  end;

  TdxFontCharacterRangeArray = array of TdxFontCharacterRange;

  TdxFontCharacterSet = class
  private
    FBits: TBits;
    procedure AddRange(const AFontCharacterRange: TdxFontCharacterRange);
  protected
    FPanose: TPanose;
    property Bits: TBits read FBits;
  public
    constructor Create(const ARanges: TdxFontCharacterRangeArray; const APanose: TPanose);
    destructor Destroy; override;
    class function CalculatePanoseDistance(const AFontCharacterSet1, AFontCharacterSet2: TdxFontCharacterSet): Integer;
    function ContainsChar(C: Char): Boolean; inline;
  end;

  { TdxFontInfo }

  TdxFontInfo = class
  private
    FAscent: Integer;
    FBaseFontIndex: Integer;
    FCharset: Integer;
    FDashWidth: Integer;
    FDescent: Integer;
    FDotWidth: Integer;
    FEqualSignWidth: Integer;
    FIsCJKFont: Boolean;
    FLineSpacing: Integer;
    FMacAscent: Integer;
    FMacDescent: Integer;
    FMaxDigitWidth: Single;
    FMiddleDotWidth: Integer;
    FNonBreakingSpaceWidth: Integer;
    FPilcrowSignWidth: Integer;
    FSizeInPoints: Single;
    FSpaceWidth: Integer;
    FStrikeoutPosition: Integer;
    FStrikeoutThickness: Integer;
    FSubscriptOffset: TPoint;
    FSubscriptSize: TSize;
    FSuperscriptOffset: TPoint;
    FSuperscriptSize: TSize;
    FUnderlinePosition: Integer;
    FUnderlineThickness: Integer;
    FUnderscoreWidth: Integer;
  protected
    procedure CalculateFontParameters(AMeasurer: TdxFontInfoMeasurer); virtual;
    function CalculateMaxDigitWidth(AMeasurer: TdxFontInfoMeasurer): Single;
    procedure CalculateFontVerticalParameters(AMeasurer: TdxFontInfoMeasurer); virtual; abstract;
    function CalculateFontCharset(AMeasurer: TdxFontInfoMeasurer): Integer; virtual; abstract;
    procedure CreateFont(AMeasurer: TdxFontInfoMeasurer; const AName: string; ASize: Integer; const AFontStyle: TFontStyles); virtual; abstract;
    function CalculateFontSizeInPoints: Single; virtual; abstract;
    function GetBaseFontInfo(const ADocumentModel: TObject): TdxFontInfo; virtual; 
    function GetBold: Boolean; virtual; abstract;
    function GetItalic: Boolean; virtual; abstract;
    function GetName: string; virtual; abstract;
    function GetSize: Single; virtual; abstract;
    function GetUnderline: Boolean; virtual; abstract;
    procedure Initialize(AMeasurer: TdxFontInfoMeasurer); virtual; abstract;
    procedure SetCJKFont(AValue: Boolean);
  public
    constructor Create(AMeasurer: TdxFontInfoMeasurer; const AName: string; ADoubleSize: Integer; const AFontStyle: TFontStyles);
    procedure CalculateSuperscriptOffset(ABaseFontInfo: TdxFontInfo); virtual; abstract;
    procedure CalculateSubscriptOffset(ABaseFontInfo: TdxFontInfo); virtual; abstract;
    function GetBaseAscentAndFree(const ADocumentModel: TObject): Integer; 
    function GetBaseDescent(const ADocumentModel: TObject): Integer; 

    property Bold: Boolean read GetBold;
    property Italic: Boolean read GetItalic;
    property Underline: Boolean read GetUnderline;
    property Size: Single read GetSize;
    property Name: string read GetName;

    property Ascent: Integer read FAscent write FAscent;
    property Descent: Integer read FDescent write FDescent;
    property IsCJKFont: Boolean read FIsCJKFont;
    property LineSpacing: Integer read FLineSpacing write FLineSpacing;
    property SpaceWidth: Integer read FSpaceWidth;
    property PilcrowSignWidth: Integer read FPilcrowSignWidth;
    property NonBreakingSpaceWidth: Integer read FNonBreakingSpaceWidth write FNonBreakingSpaceWidth;
    property UnderscoreWidth: Integer read FUnderscoreWidth write FUnderscoreWidth;
    property MacAscent: Integer read FMacAscent write FMacAscent;
    property MacDescent: Integer read FMacDescent write FMacDescent;
    property MiddleDotWidth: Integer read FMiddleDotWidth write FMiddleDotWidth;
    property DotWidth: Integer read FDotWidth write FDotWidth;
    property DashWidth: Integer read FDashWidth write FDashWidth;
    property EqualSignWidth: Integer read FEqualSignWidth write FEqualSignWidth;
    property UnderlineThickness: Integer read FUnderlineThickness write FUnderlineThickness;
    property UnderlinePosition: Integer read FUnderlinePosition write FUnderlinePosition;
    property StrikeoutThickness: Integer read FStrikeoutThickness write FStrikeoutThickness;
    property StrikeoutPosition: Integer read FStrikeoutPosition write FStrikeoutPosition;
    property SubscriptSize: TSize read FSubscriptSize write FSubscriptSize;
    property SubscriptOffset: TPoint read FSubscriptOffset write FSubscriptOffset;
    property SuperscriptSize: TSize read FSuperscriptSize write FSuperscriptSize;
    property SuperscriptOffset: TPoint read FSuperscriptOffset write FSuperscriptOffset;
    property SizeInPoints: Single read FSizeInPoints;
    property BaseFontIndex: Integer read FBaseFontIndex write FBaseFontIndex;
    property Charset: Integer read FCharset write FCharset;
    property MaxDigitWidth: Single read FMaxDigitWidth;
  end;

  { TdxFontInfoMeasurer }

  TdxFontInfoMeasurer = class
  private
    FUnitConverter: TdxDocumentLayoutUnitConverter;
  protected
    procedure Initialize; virtual; abstract;
  public
    constructor Create(AUnitConverter: TdxDocumentLayoutUnitConverter); virtual;
    function MeasureCharacterWidth(ACharacter: WideChar; AFontInfo: TdxFontInfo): Integer; virtual;
    function MeasureCharacterWidthF(ACharacter: WideChar; AFontInfo: TdxFontInfo): Single; virtual; abstract;
    function MeasureString(const AText: string; AFontInfo: TdxFontInfo): TSize; virtual; abstract;
    function MeasureMaxDigitWidthF(AFontInfo: TdxFontInfo): Single; virtual; abstract;

    property UnitConverter: TdxDocumentLayoutUnitConverter read FUnitConverter;
  end;

  { TdxFontCache }

  TdxCharacterFormattingScript = (Normal = 0, Subscript = 1, Superscript = 2);

  TdxFontCache = class
  strict protected class var
    FNameToCharacterSetMap: TObjectDictionary<string, TdxFontCharacterSet>;
  private
    FItems: TdxFastObjectList;
    FMeasurer: TdxFontInfoMeasurer;
    FUnitConverter: TdxDocumentLayoutUnitConverter;
    FIndexHash: TDictionary<Int64, Integer>;
    FCharsets: TDictionary<string, Integer>;
    function GetCount: Integer; inline;
    function GetItem(AIndex: Integer): TdxFontInfo; inline;
  protected
    function CreateFontInfoMeasurer(AUnitConverter: TdxDocumentLayoutUnitConverter): TdxFontInfoMeasurer; virtual; abstract;
    function CreateFontInfoCore(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): TdxFontInfo; virtual; abstract;
    function CalcSubscriptFontSize(ABaseFontIndex: Integer): Integer;
    function CalcSuperscriptFontSize(ABaseFontIndex: Integer): Integer;
    function CalcSuperscriptFontIndex(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer;
    function CalcSubscriptFontIndex(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer;
    function CalcFontIndexCore(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles; AScript: TdxCharacterFormattingScript): Integer;
    function CalcHash(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles; AScript: TdxCharacterFormattingScript): Int64;
    function CreateFontInfo(const AHash: Int64; const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer;
    function GetFontCharacterRanges(AFontInfo: TdxFontInfo): TdxFontCharacterRangeArray; virtual; abstract;

    property IndexHash: TDictionary<Int64, Integer> read FIndexHash;
    class property NameToCharacterSetMap: TObjectDictionary<string, TdxFontCharacterSet> read FNameToCharacterSetMap;
  public
    constructor Create(AUnitConverter: TdxDocumentLayoutUnitConverter); virtual;
    destructor Destroy; override;
    function CalcFontIndex(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles; AScript: TdxCharacterFormattingScript): Integer; virtual;
    function CalcNormalFontIndex(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer; virtual;
    function FindSubstituteFont(const ASourceFontName: string; ACharacter: WideChar; var AFontCharacterSet: TdxFontCharacterSet): string; virtual; abstract;
    function GetCharsetByFontName(const AFontName: string): Integer;
    function GetFontCharacterSet(const AFontName: string): TdxFontCharacterSet; virtual; abstract;
    function ShouldUseDefaultFontToDrawInvisibleCharacter(AFontInfo: TdxFontInfo; ACharacter: Char): Boolean; virtual;

    property Count: Integer read GetCount;
    property Items[Index: Integer]: TdxFontInfo read GetItem; default;
    property Measurer: TdxFontInfoMeasurer read FMeasurer;
    property UnitConverter: TdxDocumentLayoutUnitConverter read FUnitConverter;
  end;

  { TdxFontCacheManager }

  TdxFontCacheManager = class
  private
    FUnitConverter: TdxDocumentLayoutUnitConverter;
  public
    constructor Create(AUnitConverter: TdxDocumentLayoutUnitConverter);
    class function CreateDefault(AUnitConverter: TdxDocumentLayoutUnitConverter): TdxFontCacheManager;
    procedure ReleaseFontCache(ACache: TdxFontCache); virtual;
    function CreateFontCache: TdxFontCache; virtual; abstract;

    property UnitConverter: TdxDocumentLayoutUnitConverter read FUnitConverter;
  end;

  { TdxRichEditControlCompatibility }

  TdxRichEditControlCompatibility = class
  const
    DefaultFontNameValue = 'Calibri';
    DefaultFontSizeValue = 11;
  strict private
    class var FDefaultFontName: string;
    class var FDefaultFontSize: Double;

    class constructor Initialize;

    class procedure SetDefaultFontName(const Value: string); static;
    class procedure SetDefaultFontSize(const Value: Double); static;
  public
    class function DefaultDoubleFontSize: Integer;

    class property DefaultFontName: string read FDefaultFontName write SetDefaultFontName;
    class property DefaultFontSize: Double read FDefaultFontSize write SetDefaultFontSize;
  end;

function CodePageFromCharset(ACharSet: Cardinal): Cardinal;

implementation

uses
  RTLConsts, Math, SysUtils, dxCore, dxHashUtils, Character, dxRichEdit.Utils.Characters, dxRichEdit.Platform.Win.FontCache,
  dxRichEdit.DocumentModel.Core;

const
  dxUnicodeCharacterCount = 65536;

function CodePageFromCharset(ACharSet: Cardinal): Cardinal;
var
  lpCs: TCharsetInfo;
begin
  if (ACharSet <> 2) and TranslateCharsetInfo(ACharSet, lpCs, TCI_SRCCHARSET) then
    Result := lpCs.ciACP
  else
    Result := ANSI_CHARSET;
end;

{ TdxFontCharacterRange }

procedure TdxFontCharacterRange.CopyFrom(ALow, AHi: Integer);
begin
  Low := ALow;
  Hi := AHi;
end;

procedure TdxFontCharacterRange.CopyFrom(const ARange: TWCRange);
begin
  Low := Ord(ARange.wcLow);
  Hi := Low + ARange.cGlyphs - 1;
end;

{ TdxFontCharacterSet }

constructor TdxFontCharacterSet.Create(const ARanges: TdxFontCharacterRangeArray; const APanose: TPanose);
var
  I, AMaxIndex: Integer;
  ARange: TdxFontCharacterRange;
begin
  inherited Create;
  FBits := TBits.Create;
  FPanose := APanose;
  AMaxIndex := 0;
  for I := Low(ARanges) to High(ARanges) do
  begin
    ARange := ARanges[I];
    AddRange(ARange);
    if ARange.Hi > AMaxIndex then
      AMaxIndex := ARange.Hi;
  end;
  FBits.Size := AMaxIndex + 1;
end;

destructor TdxFontCharacterSet.Destroy;
begin
  FBits.Free;
  inherited Destroy;
end;

class function TdxFontCharacterSet.CalculatePanoseDistance(const AFontCharacterSet1, AFontCharacterSet2: TdxFontCharacterSet): Integer;
var
  I, ADist: Integer;
  P1, P2: PByte;
begin
  Result := 0;
  P1 := @AFontCharacterSet1.FPanose;
  P2 := @AFontCharacterSet2.FPanose;
  for I := 0 to 9 do
  begin
    ADist := P1^ - P2^;
    Result := Result + Sqr(ADist);
    Inc(P1);
    Inc(P2);
  end;
end;

function TdxFontCharacterSet.ContainsChar(C: Char): Boolean;
begin
  Result := (Ord(C) < FBits.Size) and FBits[Ord(C)];
end;

procedure TdxFontCharacterSet.AddRange(const AFontCharacterRange: TdxFontCharacterRange);
var
  I: Integer;
begin
  for I := AFontCharacterRange.Low to AFontCharacterRange.Hi do
    FBits[I] := True;
end;

{ TdxFontInfo }

constructor TdxFontInfo.Create(AMeasurer: TdxFontInfoMeasurer; const AName: string; ADoubleSize: Integer; const AFontStyle: TFontStyles);
begin
  inherited Create;
  FBaseFontIndex := -1;
  CreateFont(AMeasurer, AName, ADoubleSize, AFontStyle);

  Initialize(AMeasurer);
  CalculateFontParameters(AMeasurer);
end;

procedure TdxFontInfo.CalculateFontParameters(AMeasurer: TdxFontInfoMeasurer);
begin
  CalculateFontVerticalParameters(AMeasurer);
  FSizeInPoints := CalculateFontSizeInPoints;
  FSpaceWidth := Round(AMeasurer.MeasureCharacterWidthF(TdxCharacters.Space, Self));
  FPilcrowSignWidth := AMeasurer.MeasureCharacterWidth(TdxCharacters.PilcrowSign, Self);
  FNonBreakingSpaceWidth := AMeasurer.MeasureCharacterWidth(TdxCharacters.NonBreakingSpace, Self);
  FUnderscoreWidth := AMeasurer.MeasureCharacterWidth(TdxCharacters.Underscore, Self);
  FMiddleDotWidth := AMeasurer.MeasureCharacterWidth(TdxCharacters.MiddleDot, Self);
  FDotWidth := AMeasurer.MeasureCharacterWidth(TdxCharacters.Dot, Self);
  FDashWidth := AMeasurer.MeasureCharacterWidth(TdxCharacters.Dash, Self);
  FEqualSignWidth := AMeasurer.MeasureCharacterWidth(TdxCharacters.EqualSign, Self);
  FCharset := CalculateFontCharset(AMeasurer);
  FMaxDigitWidth := CalculateMaxDigitWidth(AMeasurer);
end;

function TdxFontInfo.GetBaseAscentAndFree(const ADocumentModel: TObject): Integer; 
var
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := GetBaseFontInfo(ADocumentModel);
  Result := AFontInfo.LineSpacing - Descent;
end;

function TdxFontInfo.GetBaseDescent(const ADocumentModel: TObject): Integer; 
begin
  Result := GetBaseFontInfo(ADocumentModel).Descent;
end;

function TdxFontInfo.GetBaseFontInfo(const ADocumentModel: TObject): TdxFontInfo; 
begin
  if FBaseFontIndex >= 0 then
    Result := TdxCustomDocumentModel(ADocumentModel).FontCache[FBaseFontIndex]
  else
    Result := Self;
end;

function TdxFontInfo.CalculateMaxDigitWidth(AMeasurer: TdxFontInfoMeasurer): Single;
begin
  Result := AMeasurer.MeasureMaxDigitWidthF(Self);
end;

procedure TdxFontInfo.SetCJKFont(AValue: Boolean);
begin
  FIsCJKFont := AValue;
end;

{ TdxFontInfoMeasurer }

constructor TdxFontInfoMeasurer.Create(AUnitConverter: TdxDocumentLayoutUnitConverter);
begin
  inherited Create;
  FUnitConverter := AUnitConverter;
  Initialize;
end;

function TdxFontInfoMeasurer.MeasureCharacterWidth(ACharacter: Char; AFontInfo: TdxFontInfo): Integer;
begin
  Result := Ceil(MeasureCharacterWidthF(ACharacter, AFontInfo));
end;

{ TdxFontCache }

constructor TdxFontCache.Create(AUnitConverter: TdxDocumentLayoutUnitConverter);
begin
  inherited Create;
  Assert(AUnitConverter <> nil);
  FUnitConverter := AUnitConverter;
  FIndexHash := TDictionary<Int64, Integer>.Create;
  FItems := TdxFastObjectList.Create;
  FMeasurer := CreateFontInfoMeasurer(AUnitConverter);
  FCharsets := TDictionary<string, Integer>.Create;
end;

destructor TdxFontCache.Destroy;
begin
  FIndexHash.Free;
  FCharsets.Free;
  FMeasurer.Free;
  FItems.Free;
  inherited Destroy;
end;


function TdxFontCache.CalcFontIndex(const AFontName: string; ADoubleFontSize: Integer;
  const AFontStyle: TFontStyles; AScript: TdxCharacterFormattingScript): Integer;
var
  ADoubleFontSizeInLayoutFontUnits: Integer;
begin
  ADoubleFontSizeInLayoutFontUnits := UnitConverter.PointsToFontUnits(ADoubleFontSize);
  case AScript of
    TdxCharacterFormattingScript.Normal:
      Result := CalcNormalFontIndex(AFontName, ADoubleFontSizeInLayoutFontUnits, AFontStyle);
    TdxCharacterFormattingScript.Subscript:
      Result := CalcSubscriptFontIndex(AFontName, ADoubleFontSizeInLayoutFontUnits, AFontStyle);
  else
    Result := CalcSuperscriptFontIndex(AFontName, ADoubleFontSizeInLayoutFontUnits, AFontStyle);
  end;
end;

function TdxFontCache.ShouldUseDefaultFontToDrawInvisibleCharacter(AFontInfo: TdxFontInfo;
  ACharacter: WideChar): Boolean;
begin
  Result := True;
end;

function TdxFontCache.CalcSubscriptFontSize(ABaseFontIndex: Integer): Integer;
var
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := Items[ABaseFontIndex];
  Result := Trunc((AFontInfo.SubscriptSize.cy * UnitConverter.FontSizeScale));
end;

function TdxFontCache.CalcSuperscriptFontSize(ABaseFontIndex: Integer): Integer;
var
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := Items[ABaseFontIndex];
  Result := Trunc((AFontInfo.SuperscriptSize.cy * UnitConverter.FontSizeScale));
end;

function TdxFontCache.CalcNormalFontIndex(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer;
begin
  Result := CalcFontIndexCore(AFontName, ADoubleFontSize, AFontStyle, TdxCharacterFormattingScript.Normal);
end;

function TdxFontCache.CalcSuperscriptFontIndex(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer;
var
  AFontInfo: TdxFontInfo;
  ASuperscriptFontSize: Integer;
  ABaseFontIndex: Integer;
begin
  ABaseFontIndex := CalcFontIndexCore(AFontName, ADoubleFontSize, AFontStyle, TdxCharacterFormattingScript.Normal);
  ASuperscriptFontSize := CalcSuperscriptFontSize(ABaseFontIndex);
  Result := CalcFontIndexCore(AFontName, ASuperscriptFontSize * 2, AFontStyle, TdxCharacterFormattingScript.Superscript);
  AFontInfo := Items[Result];
  AFontInfo.CalculateSuperscriptOffset(Items[ABaseFontIndex]);
  AFontInfo.BaseFontIndex := ABaseFontIndex;
end;

function TdxFontCache.CalcSubscriptFontIndex(const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer;
var
  AFontInfo: TdxFontInfo;
  ASubscriptFontSize: Integer;
  ABaseFontIndex: Integer;
begin
  ABaseFontIndex := CalcFontIndexCore(AFontName, ADoubleFontSize, AFontStyle, TdxCharacterFormattingScript.Normal);
  ASubscriptFontSize := CalcSubscriptFontSize(ABaseFontIndex);
  Result := CalcFontIndexCore(AFontName, ASubscriptFontSize * 2, AFontStyle, TdxCharacterFormattingScript.Subscript);
  AFontInfo := Items[Result];
  AFontInfo.CalculateSubscriptOffset(Items[ABaseFontIndex]);
  AFontInfo.BaseFontIndex := ABaseFontIndex;
end;

function TdxFontCache.CalcFontIndexCore(const AFontName: string; ADoubleFontSize: Integer;
  const AFontStyle: TFontStyles; AScript: TdxCharacterFormattingScript): Integer;
var
  AHash: Int64;
  AUpperFontName: string;
begin
{$IFDEF DELPHI18}
  AUpperFontName := AFontName.ToUpper;
{$ELSE}
  AUpperFontName := ToUpper(AFontName);
{$ENDIF}
  AHash := CalcHash(AUpperFontName, ADoubleFontSize, AFontStyle, AScript);
  if not IndexHash.TryGetValue(AHash, Result) then
  begin
    TMonitor.Enter(Self);
    try
      if not IndexHash.TryGetValue(AHash, Result) then 
        Result := CreateFontInfo(AHash, AUpperFontName, ADoubleFontSize, AFontStyle);
    finally
      TMonitor.Exit(Self);
    end;
  end;
end;

function TdxFontCache.CalcHash(const AFontName: string; ADoubleFontSize: Integer;
  const AFontStyle: TFontStyles; AScript: TdxCharacterFormattingScript): Int64;
var
  AStyleValue: Integer;
  AFontStyleByte: Byte absolute AFontStyle;
begin
  AStyleValue := Ord(AScript) or (AFontStyleByte shl 2); // 6 bits
  Int64Rec(Result).Lo := (ADoubleFontSize shl 6) or AStyleValue;
  Int64Rec(Result).Hi := dxElfHash(PWideChar(AFontName), Length(AFontName), nil, 0);
end;

function TdxFontCache.CreateFontInfo(const AHash: Int64; const AFontName: string; ADoubleFontSize: Integer; const AFontStyle: TFontStyles): Integer;
var
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := CreateFontInfoCore(AFontName, ADoubleFontSize, AFontStyle);
  Result := FItems.Add(AFontInfo);
  if not FCharsets.ContainsKey(AFontName) then
    FCharsets.Add(AFontName, AFontInfo.Charset);
  IndexHash.Add(AHash, Result);
end;

function TdxFontCache.GetCharsetByFontName(const AFontName: string): Integer;
begin
  if not FCharsets.TryGetValue(AFontName, Result) then
    Result := CalcFontIndex(AFontName, 24, [], TdxCharacterFormattingScript.Normal);
end;

function TdxFontCache.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TdxFontCache.GetItem(AIndex: Integer): TdxFontInfo;
begin
  Result := TdxFontInfo(FItems[AIndex]);
end;

{ TdxFontCacheManager }

constructor TdxFontCacheManager.Create(AUnitConverter: TdxDocumentLayoutUnitConverter);
begin
  inherited Create;
  FUnitConverter := AUnitConverter;
end;

class function TdxFontCacheManager.CreateDefault(AUnitConverter: TdxDocumentLayoutUnitConverter): TdxFontCacheManager;
begin
  Result := TdxGdiFontCacheManager.Create(AUnitConverter);
end;

procedure TdxFontCacheManager.ReleaseFontCache(ACache: TdxFontCache);
begin
  FreeAndNil(ACache);
end;

{ TdxRichEditControlCompatibility }

class function TdxRichEditControlCompatibility.DefaultDoubleFontSize: Integer;
begin
  Result := Trunc(DefaultFontSize * 2);
end;

class constructor TdxRichEditControlCompatibility.Initialize;
begin
  FDefaultFontName := DefaultFontNameValue;
  FDefaultFontSize := DefaultFontSizeValue;
end;

class procedure TdxRichEditControlCompatibility.SetDefaultFontName(
  const Value: string);
begin
  if Value = '' then
    raise Exception.Create('Invalid argument: DefaultFontName');
  FDefaultFontName := Value;
end;

class procedure TdxRichEditControlCompatibility.SetDefaultFontSize(
  const Value: Double);
begin
  if Value < 0 then
    raise Exception.Create('Invalid argument: DefaultFontSize');
  FDefaultFontSize := Value;
end;

end.

