{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.PieceTableModifiers;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Generics.Collections, dxCore, dxCoreClasses,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.Utils.FastComparer, dxRichEdit.DocumentModel.Styles,
  dxRichEdit.Utils.PredefinedFontSizeCollection, dxRichEdit.DocumentModel.TabFormatting, dxRichEdit.Platform.Font,
  dxRichEdit.DocumentModel.History.Run, dxRichEdit.DocumentModel.Core;

type


  { IdxRectangularObject }

  IdxRectangularObject = interface
  ['{4EB11B18-4E65-474D-AE07-0A00CD7CF8E3}']
  end;

  { TdxInlinePictureRunPropertyModifierBase }

  TdxInlinePictureRunPropertyModifierBase = class 
  public
    procedure ModifyPictureRun(ARun: TdxInlinePictureRun; ARunIndex: TdxRunIndex); virtual; abstract; 
  end;

  { TdxRectangularObjectPropertyModifierBase }

  TdxRectangularObjectPropertyModifierBase = class 
  public
    procedure ModifyRectangularObject(ARectangularObject: IdxRectangularObject; ARunIndex: TdxRunIndex); virtual; abstract; 
  end;

  { TdxRunChangeCaseModifierBase }

  TdxRunChangeCaseModifierBase = class(TdxRunPropertyModifierBase) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    procedure ChangeRunCase(ATextRun: TdxTextRunBase{TODO: TdxTextRun}; ARunIndex: TdxRunIndex); virtual; 
    function CreateHistoryItem(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex): TdxTextRunChangeCaseHistoryItem; virtual; abstract; 
  end;

  { TdxRunMakeUpperCaseModifier }

  TdxRunMakeUpperCaseModifier = class(TdxRunChangeCaseModifierBase) 
  public
    function CreateHistoryItem(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex): TdxTextRunChangeCaseHistoryItem; override; 
  end;

  { TdxRunMakeLowerCaseModifier }

  TdxRunMakeLowerCaseModifier = class(TdxRunChangeCaseModifierBase) 
  public
    function CreateHistoryItem(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex): TdxTextRunChangeCaseHistoryItem; override; 
  end;

  { TdxRunToggleCaseModifier }

  TdxRunToggleCaseModifier = class(TdxRunChangeCaseModifierBase) 
  public
    function CreateHistoryItem(APieceTable: TdxPieceTable; ARunIndex: TdxRunIndex): TdxTextRunChangeCaseHistoryItem; override; 
  end;

  { TdxRunPropertyModifier<T> }

  TdxRunPropertyModifier<T> = class(TdxRunPropertyModifierBase) 
  strict private
    FComparerType: TdxFastComparerType;
  private
    FNewValue: T; 
  protected
    function ValidateNewValue(ANewValue: T): T; virtual; 
  public
    constructor Create(ANewValue: T); 
    procedure ModifyInputPosition(APos: TdxInputPosition); virtual; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): T; virtual; abstract; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); virtual; abstract; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): T; virtual; abstract; 
    function IsValueEquals(const AValue1, AValue2: T): Boolean; virtual;

    property NewValue: T read FNewValue; 
  end;

  { TdxInlinePictureRunPropertyModifier<T> }

  TdxInlinePictureRunPropertyModifier<T> = class(TdxInlinePictureRunPropertyModifierBase) 
  private
    FNewValue: T; 
  protected
    procedure InlinePictureRunPropertyModifier(ANewValue: T); 
    function ValidateNewValue(ANewValue: T): T; virtual; 
  public
    property NewValue: T read FNewValue; 
  end;

  { TdxRectangularObjectPropertyModifier<T> }

  TdxRectangularObjectPropertyModifier<T> = class(TdxRectangularObjectPropertyModifierBase) 
  private
    FNewValue: T; 
  protected
    procedure RectangularObjectPropertyModifier(ANewValue: T); 
    function ValidateNewValue(ANewValue: T): T; 
  public
    property NewValue: T read FNewValue; 
  end;

  { TdxFloatingObjectRunPropertyModifierBase }

  TdxFloatingObjectRunPropertyModifierBase = class 
  public
    procedure ModifyFloatingObjectRun(ARun: TdxFloatingObjectAnchorRun; ARunIndex: TdxRunIndex); virtual; abstract; 
  end;


  { TdxMergedRunPropertyModifier<T> }

  TdxMergedRunPropertyModifier<T> = class(TdxRunPropertyModifier<T>) 
  protected
    function CanModifyRun(ARun: TdxTextRunBase): Boolean; virtual; 
  public
    function Merge(ALeftValue, ARightValue: T): T; virtual; abstract; 
  end;

  { TdxParagraphPropertyModifier<T> }

  TdxParagraphPropertyModifier<T> = class(TdxParagraphPropertyModifierBase) 
  strict private
    FComparerType: TdxFastComparerType;
  private
    FNewValue: T; 
  protected
  public
    constructor Create(ANewValue: T); 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): T; virtual; abstract; 
    function IsValueEquals(const AValue1, AValue2: T): Boolean;

    property NewValue: T read FNewValue; 
  end;

  { TdxMergedParagraphPropertyModifier<T> }

  TdxMergedParagraphPropertyModifier<T> = class(TdxParagraphPropertyModifier<T>) 
  public
    function Merge(ALeftValue, ARightValue: T): T; virtual; abstract; 
  end;

  { TdxRunCharacterStyleModifier }

  TdxRunCharacterStyleModifier = class(TdxRunPropertyModifier<Integer>) 
  private
    FResetProperties: Boolean; 
  public
    constructor Create(AStyleIndex: Integer; AResetProperties: Boolean = True); 
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Integer; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Integer; override; 
  end;

  { TdxRunCharacterStyleKeepOldStylePropertiesModifier }

  TdxRunCharacterStyleKeepOldStylePropertiesModifier = class(TdxRunCharacterStyleModifier) 
  private
    FApplyDefaultHyperlinkStyle: Boolean;
    FIgnoredOptions: TdxUsedCharacterFormattingOptions;
  public
    constructor Create(AStyleIndex: Integer; AApplyDefaultHyperlinkStyle: Boolean; const AIgnoredOptions: TdxUsedCharacterFormattingOptions); overload;
    constructor Create(AStyleIndex: Integer; AApplyDefaultHyperlinkStyle: Boolean); overload;
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    procedure ModifyTextRunCore(ARun: TdxTextRunBase); 
  end;

  { TdxReplaceRunCharacterStylePropertiesModifier }

  TdxReplaceRunCharacterStylePropertiesModifier = class(TdxRunCharacterStyleModifier) 
  private
    FProperties: TdxMergedCharacterProperties; 
    function LeaveUseInStyleProperties(AStyle: TdxCharacterStyle;
      AProperties: TdxMergedCharacterProperties): TdxMergedCharacterProperties; 
  public
    constructor Create(AStyleIndex: Integer; ASourceProperties: TdxMergedCharacterProperties); 
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
  end;

  { TdxRunFontNamePropertyModifier }

  TdxRunFontNamePropertyModifier = class(TdxRunPropertyModifier<string>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): string; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): string; override; 
    function IsValueEquals(const AValue1, AValue2: string): Boolean; override;
  end;

  { TdxRunDoubleFontSizePropertyModifier }

  TdxRunDoubleFontSizePropertyModifier = class(TdxRunPropertyModifier<Integer>) 
  protected
    function ValidateNewValue(ANewValue: Integer): Integer; override; 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Integer; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Integer; override; 
  end;

  { TdxRunFontSizePropertyModifier }

  TdxRunFontSizePropertyModifier = class(TdxRunPropertyModifier<Single>) 
  protected
    function GetDoubleFontSize(AFontSize: Single): Integer;
    function ValidateNewValue(ANewValue: Single): Single; override; 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Single; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Single; override; 
  end;

  { TdxRunFontBoldModifier }

  TdxRunFontBoldModifier = class(TdxRunPropertyModifier<Boolean>)
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunFontItalicModifier }

  TdxRunFontItalicModifier = class(TdxRunPropertyModifier<Boolean>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunClearCharacterFormattingModifier }

  TdxRunClearCharacterFormattingModifier = class(TdxRunPropertyModifier<Boolean>) 
  private
    procedure SplitRunByCharset(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunHiddenModifier }

  TdxRunHiddenModifier = class(TdxRunPropertyModifier<Boolean>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunAllCapsModifier }

  TdxRunAllCapsModifier = class(TdxRunPropertyModifier<Boolean>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunStrikeoutWordsOnlyModifier }

  TdxRunStrikeoutWordsOnlyModifier = class(TdxRunPropertyModifier<Boolean>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunUnderlineWordsOnlyModifier }

  TdxRunUnderlineWordsOnlyModifier = class(TdxRunPropertyModifier<Boolean>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunFontUnderlineTypeModifier }

  TdxRunFontUnderlineTypeModifier = class(TdxRunPropertyModifier<TdxUnderlineType>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TdxUnderlineType; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TdxUnderlineType; override; 
  end;

  { TdxRunFontStrikeoutTypeModifier }

  TdxRunFontStrikeoutTypeModifier = class(TdxRunPropertyModifier<TdxStrikeoutType>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TdxStrikeoutType; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TdxStrikeoutType; override; 
  end;

  { TdxRunFontCodeModifier }

  TdxRunFontCodeModifier = class(TdxRunPropertyModifier<Boolean>) 
  const
    CodeFontSize = 10;
    CodeFontName = 'Courier New';
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxRunBackColorModifier }

  TdxRunBackColorModifier = class(TdxRunPropertyModifier<TColor>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TColor; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TColor; override; 
  end;

  { TdxRunForeColorModifier }

  TdxRunForeColorModifier = class(TdxRunPropertyModifier<TColor>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TColor; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TColor; override; 
  end;

  { TdxRunStrikeoutColorModifier }

  TdxRunStrikeoutColorModifier = class(TdxRunPropertyModifier<TColor>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TColor; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TColor; override; 
  end;

  { TdxRunUnderlineColorModifier }

  TdxRunUnderlineColorModifier = class(TdxRunPropertyModifier<TColor>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TColor; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TColor; override; 
  end;

  { TdxRunScriptModifier }

  TdxRunScriptModifier = class(TdxRunPropertyModifier<TdxCharacterFormattingScript>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TdxCharacterFormattingScript; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TdxCharacterFormattingScript; override; 
  end;

  { TdxRunFontModifier }

  TdxRunFontModifier = class(TdxRunPropertyModifier<TdxFont>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TdxFont; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TdxFont; override; 
  end;

  { TdxRunIncrementFontSizeModifier }

  TdxRunIncrementFontSizeModifier = class(TdxRunPropertyModifier<Integer>) 
  public
    constructor Create;
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Integer; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Integer; override; 
  end;

  { TdxRunDecrementFontSizeModifier }

  TdxRunDecrementFontSizeModifier = class(TdxRunPropertyModifier<Integer>) 
  public
    constructor Create;
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Integer; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Integer; override; 
  end;

  { TdxRunIncreaseFontSizeModifier }

  TdxRunIncreaseFontSizeModifier = class(TdxRunPropertyModifier<Integer>) 
  private
    FPredefinedFontSizeCollection: TdxPredefinedFontSizeCollection; 
  public
    constructor Create(APredefinedFontSizeCollection: TdxPredefinedFontSizeCollection); 
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Integer; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Integer; override; 
  end;

  { TdxRunDecreaseFontSizeModifier }

  TdxRunDecreaseFontSizeModifier = class(TdxRunPropertyModifier<Integer>) 
  private
    FPredefinedFontSizeCollection: TdxPredefinedFontSizeCollection; 
  public
    constructor Create(APredefinedFontSizeCollection: TdxPredefinedFontSizeCollection); 
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Integer; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Integer; override; 
  end;

  { TdxRunResetUseModifier }

  TdxRunResetUseModifier = class(TdxRunPropertyModifier<Boolean>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): Boolean; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean; override; 
  end;

  { TdxParagraphAlignmentModifier }

  TdxParagraphAlignmentModifier = class(TdxParagraphPropertyModifier<TdxParagraphAlignment>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): TdxParagraphAlignment; override; 
  end;

  { TdxParagraphLeftIndentModifier }

  TdxParagraphLeftIndentModifier = class(TdxParagraphPropertyModifier<Integer>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer; override; 
  end;

  { TdxParagraphRightIndentModifier }

  TdxParagraphRightIndentModifier = class(TdxParagraphPropertyModifier<Integer>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer; override; 
  end;

  { TdxAssignParagraphLeftIndentModifier }

  TdxAssignParagraphLeftIndentModifier = class(TdxParagraphPropertyModifier<Integer>) 
  private
    FMaxValue: Integer; 
  public
    constructor Create(ALeftIndent, AMaxValue: Integer); 
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer; override; 
  end;

  { TdxParagraphFirstLineIndentModifier }

  TdxParagraphFirstLineIndentModifier = class(TdxParagraphPropertyModifier<Integer>) 
  private
    FMaxIndent: Integer; 
  public
    constructor Create(AFirstLineIndent, AMaxIndent: Integer); overload; 
    constructor Create(AFirstLineIndent: Integer); overload; 

    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer; override; 
  end;

  { TdxParagraphSpacingModifier }

  TdxParagraphSpacingModifier = class(TdxParagraphPropertyModifier<TdxParagraphLineSpacing>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): TdxParagraphLineSpacing; override; 
  end;

  { TdxParagraphSpacingBeforeModifier }

  TdxParagraphSpacingBeforeModifier = class(TdxParagraphPropertyModifier<Integer>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer; override; 
  end;

  { TdxParagraphSpacingAfterModifier }

  TdxParagraphSpacingAfterModifier = class(TdxParagraphPropertyModifier<Integer>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer; override; 
  end;

  { TdxParagraphClearParagraphFormattingModifier }

  TdxParagraphClearParagraphFormattingModifier = class(TdxParagraphPropertyModifier<Boolean>) 
  public
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Boolean; override; 
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
  end;

  { TdxParagraphSuppressLineNumbersModifier }

  TdxParagraphSuppressLineNumbersModifier = class(TdxParagraphPropertyModifier<Boolean>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Boolean; override; 
  end;

  { TdxParagraphSuppressHyphenationModifier }

  TdxParagraphSuppressHyphenationModifier = class(TdxParagraphPropertyModifier<Boolean>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): Boolean; override; 
  end;

  { TdxParagraphBackColorModifier }

  TdxParagraphBackColorModifier = class(TdxParagraphPropertyModifier<TColor>) 
  public
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): TColor; override; 
  end;

  { TdxRunFontColorPropertyModifier }

  TdxRunFontColorPropertyModifier = class(TdxRunPropertyModifier<TColor>) 
  public
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    function GetRunPropertyValue(ARun: TdxTextRunBase): TColor; override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TColor; override; 
  end;

  { TdxFontPropertiesModifier }

  TdxFontPropertiesModifier = class(TdxMergedRunPropertyModifier<TdxMergedCharacterProperties>) 
  protected
    function CanModifyRun(ARun: TdxTextRunBase): Boolean; override; 
    procedure ApplyCharacterProperties(AProperties: IdxCharacterProperties); virtual; 
  public
    function GetRunPropertyValue(ARun: TdxTextRunBase): TdxMergedCharacterProperties; override; 
    procedure ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex); override; 
    procedure ModifyInputPositionCore(APos: TdxInputPosition); override; 
    function GetInputPositionPropertyValue(APos: TdxInputPosition): TdxMergedCharacterProperties; override; 
    function Merge(ALeftValue, ARightValue: TdxMergedCharacterProperties): TdxMergedCharacterProperties; override; 
  end;

  { TdxParagraphPropertiesModifier }

  TdxParagraphPropertiesModifier = class(TdxMergedParagraphPropertyModifier<TdxMergedParagraphProperties>) 
  public
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): TdxMergedParagraphProperties; override; 
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function Merge(ALeftValue, ARightValue: TdxMergedParagraphProperties): TdxMergedParagraphProperties; override; 
  end;

  { TdxTabFormattingInfoModifier }

  TdxTabFormattingInfoModifier = class(TdxMergedParagraphPropertyModifier<TdxTabFormattingInfo>) 
  private
    FNewDefaultTabWidth: Integer; 
  protected
    function GetMergedTabInfo(ANewTabInfo, AOldOwnTabInfo, AStyleTabInfo: TdxTabFormattingInfo): TdxTabFormattingInfo; 
  public
    constructor Create(ANewValue: TdxTabFormattingInfo; ADefaultTabWidth: Integer); 
    function GetParagraphPropertyValue(AParagraph: TdxParagraph): TdxTabFormattingInfo; override; 
    procedure ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex); override; 
    function Merge(ALeftValue, ARightValue: TdxTabFormattingInfo): TdxTabFormattingInfo; override; 

		property NewDefaultTabWidth: Integer read FNewDefaultTabWidth write FNewDefaultTabWidth; 
  end;


implementation

uses
  dxRichEdit.Utils.BatchUpdateHelper, RTLConsts, 
  Math, dxRichEdit.DocumentModel.TextRange, dxRichEdit.LayoutEngine.Formatter, dxRichEdit.Utils.Colors;

{ TdxRunChangeCaseModifierBase }

procedure TdxRunChangeCaseModifierBase.ChangeRunCase(ATextRun: TdxTextRunBase{TODO:TdxTextRun}; ARunIndex: TdxRunIndex);
var
  AItem: TdxTextRunChangeCaseHistoryItem;
  AHistory: TdxDocumentHistory;
begin
  AItem := CreateHistoryItem(ATextRun.Paragraph.PieceTable, ARunIndex);
  AHistory := ATextRun.Paragraph.DocumentModel.History;
  AHistory.Add(AItem);
  AItem.Execute;
end;

procedure TdxRunChangeCaseModifierBase.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  if ARun is TdxTextRun then
    ChangeRunCase(TdxTextRun(ARun), ARunIndex);
end;

{ TdxRunMakeUpperCaseModifier }

function TdxRunMakeUpperCaseModifier.CreateHistoryItem(APieceTable: TdxPieceTable;
  ARunIndex: TdxRunIndex): TdxTextRunChangeCaseHistoryItem;
begin
{TODO:
  Result := TdxTextRunMakeUpperCaseHistoryItem.Create(APieceTable, ARunIndex);}
  Result := NotImplemented;
end;

{ TdxRunMakeLowerCaseModifier }

function TdxRunMakeLowerCaseModifier.CreateHistoryItem(APieceTable: TdxPieceTable;
  ARunIndex: TdxRunIndex): TdxTextRunChangeCaseHistoryItem;
begin
  Result := TdxTextRunMakeLowerCaseHistoryItem.Create(APieceTable, ARunIndex);
end;

{ TdxRunToggleCaseModifier }

function TdxRunToggleCaseModifier.CreateHistoryItem(APieceTable: TdxPieceTable;
  ARunIndex: TdxRunIndex): TdxTextRunChangeCaseHistoryItem;
begin
  Result := TdxTextRunToggleCaseHistoryItem.Create(APieceTable, ARunIndex);
end;

{ TdxRunPropertyModifier<T> }

constructor TdxRunPropertyModifier<T>.Create(ANewValue: T);
begin
  FNewValue := ValidateNewValue(ANewValue);
end;

function TdxRunPropertyModifier<T>.IsValueEquals(const AValue1, AValue2: T): Boolean;
begin
  Result := TdxFastComparer<T>.IsValueEquals(FComparerType, AValue1, AValue2);
end;

procedure TdxRunPropertyModifier<T>.ModifyInputPosition(APos: TdxInputPosition);
begin
  ModifyInputPositionCore(APos);
end;

function TdxRunPropertyModifier<T>.ValidateNewValue(ANewValue: T): T;
begin
  Result := ANewValue;
end;

{ TdxInlinePictureRunPropertyModifier<T> }

procedure TdxInlinePictureRunPropertyModifier<T>.InlinePictureRunPropertyModifier(ANewValue: T);
begin
  FNewValue := ValidateNewValue(ANewValue);
end;

function TdxInlinePictureRunPropertyModifier<T>.ValidateNewValue(ANewValue: T): T;
begin
  Result := ANewValue;
end;

{ TdxRectangularObjectPropertyModifier<T> }

procedure TdxRectangularObjectPropertyModifier<T>.RectangularObjectPropertyModifier(ANewValue: T);
begin
  FNewValue := ValidateNewValue(ANewValue);
end;

function TdxRectangularObjectPropertyModifier<T>.ValidateNewValue(ANewValue: T): T;
begin
  Result := ANewValue;
end;

{ TdxMergedRunPropertyModifier<T> }

function TdxMergedRunPropertyModifier<T>.CanModifyRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := True;
end;

{ TdxParagraphPropertyModifier<T> }

constructor TdxParagraphPropertyModifier<T>.Create(ANewValue: T);
begin
  inherited Create;
  FNewValue := ANewValue;
end;

function TdxParagraphPropertyModifier<T>.IsValueEquals(const AValue1, AValue2: T): Boolean;
begin
  Result := TdxFastComparer<T>.IsValueEquals(FComparerType, AValue1, AValue2);
end;

{ TdxRunCharacterStyleModifier }

constructor TdxRunCharacterStyleModifier.Create(AStyleIndex: Integer; AResetProperties: Boolean = True);
begin
  inherited Create(AStyleIndex);
  FResetProperties := AResetProperties;
end;

function TdxRunCharacterStyleModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Integer;
begin
  Result := APos.CharacterStyleIndex;
end;

function TdxRunCharacterStyleModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Integer;
begin
  Result := ARun.CharacterStyleIndex;
end;

procedure TdxRunCharacterStyleModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
end;

procedure TdxRunCharacterStyleModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
var
  ADocumentModel: TdxDocumentModel;
begin
  ADocumentModel := ARun.Paragraph.DocumentModel;
  ADocumentModel.History.BeginTransaction;
  try
    if FResetProperties then
      ARun.ResetCharacterProperties;
    ARun.CharacterStyleIndex := NewValue;
  finally
    ADocumentModel.History.EndTransaction;
  end;
end;

{ TdxRunCharacterStyleKeepOldStylePropertiesModifier }

constructor TdxRunCharacterStyleKeepOldStylePropertiesModifier.Create(AStyleIndex: Integer;
  AApplyDefaultHyperlinkStyle: Boolean; const AIgnoredOptions: TdxUsedCharacterFormattingOptions);
begin
  inherited Create(AStyleIndex, False);
  FApplyDefaultHyperlinkStyle := AApplyDefaultHyperlinkStyle;
  FIgnoredOptions := AIgnoredOptions;
end;

constructor TdxRunCharacterStyleKeepOldStylePropertiesModifier.Create(AStyleIndex: Integer; AApplyDefaultHyperlinkStyle: Boolean);
begin
  Create(AStyleIndex, AApplyDefaultHyperlinkStyle, []);
end;

procedure TdxRunCharacterStyleKeepOldStylePropertiesModifier.ModifyTextRun(ARun: TdxTextRunBase;
  ARunIndex: TdxRunIndex);
begin
  if ARun.CharacterStyleIndex <> NewValue then
    ModifyTextRunCore(ARun);
end;

procedure TdxRunCharacterStyleKeepOldStylePropertiesModifier.ModifyTextRunCore(ARun: TdxTextRunBase);
var
  ADocumentModel: TdxDocumentModel;
  AProperties: TdxMergedCharacterProperties;
  AStypeProperties: TdxMergedCharacterProperties;
  AStyleOptions: TdxUsedCharacterFormattingOptions;
  AMerger: TdxCharacterPropertiesMerger;
  AMergedProperties: TdxMergedCharacterProperties;
begin
  ADocumentModel := ARun.Paragraph.DocumentModel;
  ADocumentModel.History.BeginTransaction;
  try
    AProperties := ARun.CharacterStyle.GetMergedCharacterProperties;
    if FApplyDefaultHyperlinkStyle then
    begin
      AStypeProperties := ADocumentModel.CharacterStyles[NewValue].GetMergedCharacterProperties;

      AStyleOptions := AStypeProperties.Options.Value - FIgnoredOptions;
      AProperties.Options.Value := AProperties.Options.Value - AStyleOptions;

      ARun.CharacterProperties.ResetUse(AStyleOptions);
    end;
    AMerger := TdxCharacterPropertiesMerger.Create(ARun.CharacterProperties);
    try
      AMerger.Merge(AProperties);
      AMergedProperties := AMerger.MergedProperties;
      ARun.CharacterProperties.CopyFrom(AMergedProperties);
      ARun.CharacterStyleIndex := NewValue;
    finally
      AMerger.Free;
    end;
  finally
    ADocumentModel.History.EndTransaction;
  end;
end;

{ TdxReplaceRunCharacterStylePropertiesModifier }

constructor TdxReplaceRunCharacterStylePropertiesModifier.Create(AStyleIndex: Integer;
  ASourceProperties: TdxMergedCharacterProperties);
begin
  inherited Create(AStyleIndex, False);
  Assert(ASourceProperties <> nil, 'ASourceProperties = nil');
  FProperties := ASourceProperties;
end;

function TdxReplaceRunCharacterStylePropertiesModifier.LeaveUseInStyleProperties(AStyle: TdxCharacterStyle;
  AProperties: TdxMergedCharacterProperties): TdxMergedCharacterProperties;
var
  AStyleProperties: TdxMergedCharacterProperties;
begin
  Result := TdxMergedCharacterProperties.Create(AProperties.Info, AProperties.Options);
  AStyleProperties := AStyle.GetMergedCharacterProperties;
  Result.Options.Value := AStyleProperties.Options.Value;
end;

procedure TdxReplaceRunCharacterStylePropertiesModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
var
  ADocumentModel: TdxDocumentModel;
  AStyle: TdxCharacterStyle;
  ANewProperties: TdxMergedCharacterProperties;
  AMerger: TdxCharacterPropertiesMerger;
begin
  ADocumentModel := ARun.Paragraph.DocumentModel;
  ADocumentModel.History.BeginTransaction;
  try
    if ARun.CharacterStyleIndex <> NewValue then
      Exit;
    AStyle := ADocumentModel.CharacterStyles[NewValue];
    ANewProperties := LeaveUseInStyleProperties(AStyle, FProperties);
    AMerger := TdxCharacterPropertiesMerger.Create(ARun.CharacterProperties);
    try
      AMerger.Merge(ANewProperties);
      ARun.CharacterProperties.CopyFrom(AMerger.MergedProperties);
    finally
      FreeAndNil(AMerger);
    end;
    ARun.CharacterStyleIndex := TdxCharacterStyleCollection.EmptyCharacterStyleIndex;
  finally
    ADocumentModel.History.EndTransaction;
  end;
end;

{ TdxRunFontNamePropertyModifier }

function TdxRunFontNamePropertyModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): string;
begin
  Result := APos.MergedCharacterFormatting.FontName;
end;

function TdxRunFontNamePropertyModifier.IsValueEquals(const AValue1, AValue2: string): Boolean;
begin
  Result := AValue1 = AValue2;
end;

function TdxRunFontNamePropertyModifier.GetRunPropertyValue(ARun: TdxTextRunBase): string;
begin
  Result := ARun.FontName;
end;

procedure TdxRunFontNamePropertyModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.FontName := NewValue;
  APos.MergedCharacterFormatting.FontName := NewValue;
end;

procedure TdxRunFontNamePropertyModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.FontName := NewValue;
end;

{ TdxRunDoubleFontSizePropertyModifier }

function TdxRunDoubleFontSizePropertyModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Integer;
begin
  Result := APos.MergedCharacterFormatting.DoubleFontSize;
end;

function TdxRunDoubleFontSizePropertyModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Integer;
begin
  Result := ARun.DoubleFontSize;
end;

procedure TdxRunDoubleFontSizePropertyModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.DoubleFontSize := NewValue;
  APos.MergedCharacterFormatting.DoubleFontSize := NewValue;
end;

procedure TdxRunDoubleFontSizePropertyModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.DoubleFontSize := NewValue;
end;

function TdxRunDoubleFontSizePropertyModifier.ValidateNewValue(ANewValue: Integer): Integer;
begin
  Result := TdxPredefinedFontSizeCollection.ValidateFontSize(ANewValue);
end;

{ TdxRunFontSizePropertyModifier }

function TdxRunFontSizePropertyModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Single;
begin
  Result := APos.MergedCharacterFormatting.DoubleFontSize / 2.0;
end;

function TdxRunFontSizePropertyModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Single;
begin
  Result := ARun.DoubleFontSize / 2.0;
end;

procedure TdxRunFontSizePropertyModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.DoubleFontSize := GetDoubleFontSize(NewValue);
  APos.MergedCharacterFormatting.DoubleFontSize := GetDoubleFontSize(NewValue);
end;

procedure TdxRunFontSizePropertyModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.DoubleFontSize := GetDoubleFontSize(NewValue);
end;

function TdxRunFontSizePropertyModifier.GetDoubleFontSize(AFontSize: Single): Integer;
begin
  Result := Round(AFontSize * 2.0);
end;

function TdxRunFontSizePropertyModifier.ValidateNewValue(ANewValue: Single): Single;
begin
  Result := TdxPredefinedFontSizeCollection.ValidateFontSize(GetDoubleFontSize(ANewValue)) / 2.0;
end;

{ TdxRunFontBoldModifier }

function TdxRunFontBoldModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := APos.MergedCharacterFormatting.FontBold;
end;

function TdxRunFontBoldModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun.FontBold;
end;

procedure TdxRunFontBoldModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.FontBold := NewValue;
  APos.MergedCharacterFormatting.FontBold := NewValue;
end;

procedure TdxRunFontBoldModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.FontBold := NewValue;
end;

{ TdxRunFontItalicModifier }

function TdxRunFontItalicModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := APos.MergedCharacterFormatting.FontItalic;
end;

function TdxRunFontItalicModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun.FontItalic;
end;

procedure TdxRunFontItalicModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.FontItalic := NewValue;
  APos.MergedCharacterFormatting.FontItalic := NewValue;
end;

procedure TdxRunFontItalicModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.FontItalic := NewValue;
end;

{ TdxRunClearCharacterFormattingModifier }

function TdxRunClearCharacterFormattingModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := APos.CharacterFormatting.InfoIndex <> APos.DocumentModel.DefaultCharacterProperties.Info.InfoIndex;
end;

function TdxRunClearCharacterFormattingModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := True;
end;

procedure TdxRunClearCharacterFormattingModifier.ModifyInputPositionCore(APos: TdxInputPosition);
var
  AEmptyInfo: TdxCharacterFormattingBase;
begin
  AEmptyInfo := APos.DocumentModel.Cache.CharacterFormattingCache[TdxCharacterFormattingCache.EmptyCharacterFormattingIndex];
  APos.CharacterFormatting.CopyFrom(AEmptyInfo.Info, AEmptyInfo.Options);
  APos.MergedCharacterFormatting.CopyFrom(APos.DocumentModel.DefaultCharacterProperties.Info.Info);
end;

procedure TdxRunClearCharacterFormattingModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.CharacterProperties.Reset;
  SplitRunByCharset(ARun, ARunIndex);
  ARun.CharacterStyleIndex := 0;
end;

type
  TdxTextRunBaseAccess = class(TdxTextRunBase);

procedure TdxRunClearCharacterFormattingModifier.SplitRunByCharset(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  TdxTextRunBaseAccess(ARun).DocumentModel.DeferredChanges.ApplyChanges(TdxTextRunBaseAccess(ARun).PieceTable,
    TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.FontName), ARunIndex, ARunIndex);
end;

{ TdxRunHiddenModifier }

function TdxRunHiddenModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := APos.MergedCharacterFormatting.Hidden;
end;

function TdxRunHiddenModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun.Hidden;
end;

procedure TdxRunHiddenModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.Hidden := NewValue;
  APos.MergedCharacterFormatting.Hidden := NewValue;
end;

procedure TdxRunHiddenModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  if ARunIndex < ARun.Paragraph.PieceTable.Runs.Count - 1 then
    ARun.Hidden := NewValue;
end;

{ TdxRunAllCapsModifier }

function TdxRunAllCapsModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := APos.MergedCharacterFormatting.AllCaps;
end;

function TdxRunAllCapsModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun.AllCaps;
end;

procedure TdxRunAllCapsModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.AllCaps := NewValue;
  APos.MergedCharacterFormatting.AllCaps := NewValue;
end;

procedure TdxRunAllCapsModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.AllCaps := NewValue;
end;

{ TdxRunStrikeoutWordsOnlyModifier }

function TdxRunStrikeoutWordsOnlyModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := APos.MergedCharacterFormatting.StrikeoutWordsOnly;
end;

function TdxRunStrikeoutWordsOnlyModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun.StrikeoutWordsOnly;
end;

procedure TdxRunStrikeoutWordsOnlyModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.StrikeoutWordsOnly := NewValue;
  APos.MergedCharacterFormatting.StrikeoutWordsOnly := NewValue;
end;

procedure TdxRunStrikeoutWordsOnlyModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.StrikeoutWordsOnly := NewValue;
end;

{ TdxRunUnderlineWordsOnlyModifier }

function TdxRunUnderlineWordsOnlyModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := APos.MergedCharacterFormatting.UnderlineWordsOnly;
end;

function TdxRunUnderlineWordsOnlyModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun.UnderlineWordsOnly;
end;

procedure TdxRunUnderlineWordsOnlyModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.UnderlineWordsOnly := NewValue;
  APos.MergedCharacterFormatting.UnderlineWordsOnly := NewValue;
end;

procedure TdxRunUnderlineWordsOnlyModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.UnderlineWordsOnly := NewValue;
end;

{ TdxRunFontUnderlineTypeModifier }

function TdxRunFontUnderlineTypeModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TdxUnderlineType;
begin
  Result := APos.MergedCharacterFormatting.FontUnderlineType;
end;

function TdxRunFontUnderlineTypeModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TdxUnderlineType;
begin
  Result := ARun.FontUnderlineType;
end;

procedure TdxRunFontUnderlineTypeModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.FontUnderlineType := NewValue;
  APos.MergedCharacterFormatting.FontUnderlineType := NewValue;
end;

procedure TdxRunFontUnderlineTypeModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.FontUnderlineType := NewValue;
end;

{ TdxRunFontStrikeoutTypeModifier }

function TdxRunFontStrikeoutTypeModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TdxStrikeoutType;
begin
  Result := APos.MergedCharacterFormatting.FontStrikeoutType;
end;

function TdxRunFontStrikeoutTypeModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TdxStrikeoutType;
begin
  Result := ARun.FontStrikeoutType;
end;

procedure TdxRunFontStrikeoutTypeModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.FontStrikeoutType := NewValue;
  APos.MergedCharacterFormatting.FontStrikeoutType := NewValue;
end;

procedure TdxRunFontStrikeoutTypeModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.FontStrikeoutType := NewValue;
end;

{ TdxRunFontCodeModifier }

function TdxRunFontCodeModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := (APos.MergedCharacterFormatting.DoubleFontSize = CodeFontSize) and (APos.CharacterFormatting.FontName = CodeFontName);
end;

function TdxRunFontCodeModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := (ARun.DoubleFontSize = CodeFontSize * 2) and (ARun.FontName = CodeFontName);
end;

procedure TdxRunFontCodeModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  if NewValue then
  begin
    APos.CharacterFormatting.DoubleFontSize := CodeFontSize * 2;
    APos.CharacterFormatting.FontName := CodeFontName;
    APos.MergedCharacterFormatting.DoubleFontSize := CodeFontSize * 2;
    APos.MergedCharacterFormatting.FontName := CodeFontName;
  end
  else
  begin
    APos.CharacterFormatting.DoubleFontSize := TdxRichEditControlCompatibility.DefaultDoubleFontSize;
    APos.CharacterFormatting.FontName := TdxRichEditControlCompatibility.DefaultFontName;
    APos.MergedCharacterFormatting.DoubleFontSize := TdxRichEditControlCompatibility.DefaultDoubleFontSize;
    APos.MergedCharacterFormatting.FontName := TdxRichEditControlCompatibility.DefaultFontName;
  end;
end;

procedure TdxRunFontCodeModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  if NewValue then
  begin
    ARun.DoubleFontSize := CodeFontSize * 2;
    ARun.FontName := CodeFontName;
  end
  else
  begin
    ARun.CharacterProperties.DoubleFontSize := TdxRichEditControlCompatibility.DefaultDoubleFontSize;
    ARun.CharacterProperties.FontName := TdxRichEditControlCompatibility.DefaultFontName;
  end;
end;

{ TdxRunBackColorModifier }

function TdxRunBackColorModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TColor;
begin
  Result := APos.MergedCharacterFormatting.BackColor;
end;

function TdxRunBackColorModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TColor;
begin
  Result := ARun.BackColor;
end;

procedure TdxRunBackColorModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.BackColor := NewValue;
  APos.MergedCharacterFormatting.BackColor := NewValue;
end;

procedure TdxRunBackColorModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.BackColor := NewValue;
end;

{ TdxRunForeColorModifier }

function TdxRunForeColorModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TColor;
begin
  Result := APos.MergedCharacterFormatting.ForeColor;
end;

function TdxRunForeColorModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TColor;
begin
  Result := ARun.ForeColor;
end;

procedure TdxRunForeColorModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.ForeColor := NewValue;
  APos.MergedCharacterFormatting.ForeColor := NewValue;
end;

procedure TdxRunForeColorModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.ForeColor := NewValue;
end;

{ TdxRunStrikeoutColorModifier }

function TdxRunStrikeoutColorModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TColor;
begin
  Result := APos.MergedCharacterFormatting.StrikeoutColor;
end;

function TdxRunStrikeoutColorModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TColor;
begin
  Result := ARun.StrikeoutColor;
end;

procedure TdxRunStrikeoutColorModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.StrikeoutColor := NewValue;
  APos.MergedCharacterFormatting.StrikeoutColor := NewValue;
end;

procedure TdxRunStrikeoutColorModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.StrikeoutColor := NewValue;
end;

{ TdxRunUnderlineColorModifier }

function TdxRunUnderlineColorModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TColor;
begin
  Result := APos.MergedCharacterFormatting.UnderlineColor;
end;

function TdxRunUnderlineColorModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TColor;
begin
  Result := ARun.UnderlineColor;
end;

procedure TdxRunUnderlineColorModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.UnderlineColor := NewValue;
  APos.MergedCharacterFormatting.UnderlineColor := NewValue;
end;

procedure TdxRunUnderlineColorModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.UnderlineColor := NewValue;
end;

{ TdxRunScriptModifier }

function TdxRunScriptModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TdxCharacterFormattingScript;
begin
  Result := APos.MergedCharacterFormatting.Script;
end;

function TdxRunScriptModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TdxCharacterFormattingScript;
begin
  Result := ARun.Script;
end;

procedure TdxRunScriptModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.Script := NewValue;
  APos.MergedCharacterFormatting.Script := NewValue;
end;

procedure TdxRunScriptModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.Script := NewValue;
end;

{ TdxRunFontModifier }

function TdxRunFontModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TdxFont;
begin
  Result := nil;
end;

function TdxRunFontModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TdxFont;
begin
  Result := nil;
end;

procedure TdxRunFontModifier.ModifyInputPositionCore(APos: TdxInputPosition);
var
  AFormatting: TdxCharacterFormattingBase;
begin
  AFormatting := APos.CharacterFormatting;
  AFormatting.BeginUpdate;
  try
    TdxCharacterPropertiesFontAssignmentHelper.AssignFont(AFormatting, NewValue);
  finally
    AFormatting.EndUpdate;
  end;
end;

procedure TdxRunFontModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
var
  ADocumentModel: TdxDocumentModel;
begin
  ADocumentModel := ARun.Paragraph.DocumentModel;
  ADocumentModel.History.BeginTransaction;
  try
    TdxCharacterPropertiesFontAssignmentHelper.AssignFont(ARun, NewValue);
  finally
    ADocumentModel.History.EndTransaction;
  end;
end;

{ TdxRunIncrementFontSizeModifier }

constructor TdxRunIncrementFontSizeModifier.Create;
begin
  inherited Create(0);
end;

function TdxRunIncrementFontSizeModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Integer;
begin
  Result := APos.MergedCharacterFormatting.DoubleFontSize;
end;

function TdxRunIncrementFontSizeModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Integer;
begin
  Result := ARun.DoubleFontSize;
end;

procedure TdxRunIncrementFontSizeModifier.ModifyInputPositionCore(APos: TdxInputPosition);
var
  AValue: Integer;
begin
  AValue := TdxPredefinedFontSizeCollection.ValidateFontSize(Trunc(APos.CharacterFormatting.DoubleFontSize / 2 + 1)) * 2;
  APos.CharacterFormatting.DoubleFontSize := AValue;
  APos.MergedCharacterFormatting.DoubleFontSize := AValue;
end;

procedure TdxRunIncrementFontSizeModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.DoubleFontSize := TdxPredefinedFontSizeCollection.ValidateFontSize(Trunc(ARun.DoubleFontSize / 2 + 1)) * 2;
end;

{ TdxRunDecrementFontSizeModifier }

constructor TdxRunDecrementFontSizeModifier.Create;
begin
  inherited Create(0);
end;

function TdxRunDecrementFontSizeModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Integer;
begin
  Result := APos.MergedCharacterFormatting.DoubleFontSize;
end;

function TdxRunDecrementFontSizeModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Integer;
begin
  Result := ARun.DoubleFontSize;
end;

procedure TdxRunDecrementFontSizeModifier.ModifyInputPositionCore(APos: TdxInputPosition);
var
  AValue: Integer;
begin
  AValue := TdxPredefinedFontSizeCollection.ValidateFontSize(APos.CharacterFormatting.DoubleFontSize - 1);
  APos.CharacterFormatting.DoubleFontSize := AValue;
  APos.MergedCharacterFormatting.DoubleFontSize := AValue;
end;

procedure TdxRunDecrementFontSizeModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.DoubleFontSize := TdxPredefinedFontSizeCollection.ValidateFontSize(ARun.DoubleFontSize - 1);
end;

{ TdxRunIncreaseFontSizeModifier }

constructor TdxRunIncreaseFontSizeModifier.Create(APredefinedFontSizeCollection: TdxPredefinedFontSizeCollection);
begin
  inherited Create(0);
  FPredefinedFontSizeCollection := APredefinedFontSizeCollection;
end;

function TdxRunIncreaseFontSizeModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Integer;
begin
  Result := APos.MergedCharacterFormatting.DoubleFontSize;
end;

function TdxRunIncreaseFontSizeModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Integer;
begin
  Result := ARun.DoubleFontSize;
end;

procedure TdxRunIncreaseFontSizeModifier.ModifyInputPositionCore(APos: TdxInputPosition);
var
  AValue: Integer;
begin
  AValue := FPredefinedFontSizeCollection.CalculateNextFontSize(APos.CharacterFormatting.DoubleFontSize);
  APos.CharacterFormatting.DoubleFontSize := AValue;
  APos.MergedCharacterFormatting.DoubleFontSize := AValue;
end;

procedure TdxRunIncreaseFontSizeModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.DoubleFontSize := FPredefinedFontSizeCollection.CalculateNextFontSize(ARun.DoubleFontSize);
end;

{ TdxRunDecreaseFontSizeModifier }

constructor TdxRunDecreaseFontSizeModifier.Create(APredefinedFontSizeCollection: TdxPredefinedFontSizeCollection);
begin
  inherited Create(0);
	FPredefinedFontSizeCollection := APredefinedFontSizeCollection;
end;

function TdxRunDecreaseFontSizeModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Integer;
begin
  Result := APos.MergedCharacterFormatting.DoubleFontSize;
end;

function TdxRunDecreaseFontSizeModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Integer;
begin
  Result := ARun.DoubleFontSize;
end;

procedure TdxRunDecreaseFontSizeModifier.ModifyInputPositionCore(APos: TdxInputPosition);
var
  AValue: Integer;
begin
  AValue := FPredefinedFontSizeCollection.CalculatePreviousFontSize(APos.CharacterFormatting.DoubleFontSize);
  APos.CharacterFormatting.DoubleFontSize := AValue;
  APos.MergedCharacterFormatting.DoubleFontSize := AValue;
end;

procedure TdxRunDecreaseFontSizeModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.DoubleFontSize := FPredefinedFontSizeCollection.CalculatePreviousFontSize(ARun.DoubleFontSize);
end;

{ TdxRunResetUseModifier }

function TdxRunResetUseModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): Boolean;
begin
  Result := False;
end;

function TdxRunResetUseModifier.GetRunPropertyValue(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun.CharacterProperties.Info.Options.Value = [];
end;

procedure TdxRunResetUseModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
end;

procedure TdxRunResetUseModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  if ARunIndex < ARun.Paragraph.PieceTable.Runs.Count - 1 then
    ARun.CharacterProperties.ResetAllUse;
end;

{ TdxParagraphAlignmentModifier }

function TdxParagraphAlignmentModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): TdxParagraphAlignment;
begin
  Result := AParagraph.Alignment;
end;

procedure TdxParagraphAlignmentModifier.ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.Alignment := NewValue;
end;

{ TdxParagraphLeftIndentModifier }

function TdxParagraphLeftIndentModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer;
begin
  Result := AParagraph.LeftIndent;
end;

procedure TdxParagraphLeftIndentModifier.ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.LeftIndent := NewValue;
end;

{ TdxParagraphRightIndentModifier }

function TdxParagraphRightIndentModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer;
begin
  Result := AParagraph.RightIndent;
end;

procedure TdxParagraphRightIndentModifier.ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.RightIndent := NewValue;
end;

{ TdxAssignParagraphLeftIndentModifier }

constructor TdxAssignParagraphLeftIndentModifier.Create(ALeftIndent, AMaxValue: Integer);
begin
  inherited Create(ALeftIndent);
  FMaxValue := AMaxValue;
end;

function TdxAssignParagraphLeftIndentModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer;
begin
  Result := AParagraph.LeftIndent;
end;

procedure TdxAssignParagraphLeftIndentModifier.ModifyParagraph(AParagraph: TdxParagraph;
  AParagraphIndex: TdxParagraphIndex);
var
  ANewLeftIndent, AFirstLineLeftIndent, ADistanceToRight: Integer;
begin
  ANewLeftIndent := AParagraph.LeftIndent + NewValue;
  if ANewLeftIndent >= 0 then
  begin
    if AParagraph.FirstLineIndentType = TdxParagraphFirstLineIndent.Hanging then
    begin
      AFirstLineLeftIndent := ANewLeftIndent - AParagraph.FirstLineIndent;
      if AFirstLineLeftIndent < 0 then
        Dec(ANewLeftIndent, AFirstLineLeftIndent);
    end;
    AParagraph.LeftIndent := Min(FMaxValue, ANewLeftIndent);
    if AParagraph.FirstLineIndentType = TdxParagraphFirstLineIndent.Indented then
    begin
      ADistanceToRight := FMaxValue - (AParagraph.LeftIndent + AParagraph.FirstLineIndent);
      if ADistanceToRight < 0 then
        AParagraph.FirstLineIndent := AParagraph.FirstLineIndent + ADistanceToRight;
    end;
  end;
end;

{ TdxParagraphFirstLineIndentModifier }

constructor TdxParagraphFirstLineIndentModifier.Create(AFirstLineIndent, AMaxIndent: Integer);
begin
  inherited Create(AFirstLineIndent);
  FMaxIndent := AMaxIndent;
end;

constructor TdxParagraphFirstLineIndentModifier.Create(AFirstLineIndent: Integer);
begin
  Create(AFirstLineIndent, MaxInt div 4);
end;

function TdxParagraphFirstLineIndentModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer;
begin
  Result := AParagraph.FirstLineIndent;
end;

procedure TdxParagraphFirstLineIndentModifier.ModifyParagraph(AParagraph: TdxParagraph;
  AParagraphIndex: TdxParagraphIndex);
var
  AValue, ADistanceToRight: Integer;
begin
  if NewValue > 0 then
  begin
    AParagraph.FirstLineIndentType := TdxParagraphFirstLineIndent.Indented;
    AValue := NewValue;
    ADistanceToRight := FMaxIndent - (AParagraph.LeftIndent + AValue);
    if ADistanceToRight < 0 then
      Inc(AValue, ADistanceToRight);
    AParagraph.FirstLineIndent := AValue;
  end
  else
  begin
    AParagraph.FirstLineIndentType := TdxParagraphFirstLineIndent.Hanging;
    AParagraph.FirstLineIndent := -NewValue;
  end;
end;

{ TdxParagraphSpacingModifier }

function TdxParagraphSpacingModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): TdxParagraphLineSpacing;
begin
  Result := AParagraph.LineSpacingType;
end;

procedure TdxParagraphSpacingModifier.ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.LineSpacingType := NewValue;
end;

{ TdxParagraphSpacingBeforeModifier }

function TdxParagraphSpacingBeforeModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer;
begin
  Result := AParagraph.SpacingBefore;
end;

procedure TdxParagraphSpacingBeforeModifier.ModifyParagraph(AParagraph: TdxParagraph;
  AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.SpacingBefore := NewValue;
end;

{ TdxParagraphSpacingAfterModifier }

function TdxParagraphSpacingAfterModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Integer;
begin
  Result := AParagraph.SpacingAfter;
end;

procedure TdxParagraphSpacingAfterModifier.ModifyParagraph(AParagraph: TdxParagraph;
  AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.SpacingAfter := NewValue;
end;

{ TdxParagraphClearParagraphFormattingModifier }

function TdxParagraphClearParagraphFormattingModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Boolean;
begin
  Result := True;
end;

procedure TdxParagraphClearParagraphFormattingModifier.ModifyParagraph(AParagraph: TdxParagraph;
  AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.ParagraphProperties.Reset;
end;

{ TdxParagraphSuppressLineNumbersModifier }

function TdxParagraphSuppressLineNumbersModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Boolean;
begin
  Result := AParagraph.SuppressLineNumbers;
end;

procedure TdxParagraphSuppressLineNumbersModifier.ModifyParagraph(AParagraph: TdxParagraph;
  AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.SuppressLineNumbers := NewValue;
end;

{ TdxParagraphSuppressHyphenationModifier }

function TdxParagraphSuppressHyphenationModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): Boolean;
begin
  Result := AParagraph.SuppressHyphenation;
end;

procedure TdxParagraphSuppressHyphenationModifier.ModifyParagraph(AParagraph: TdxParagraph;
  AParagraphIndex: TdxParagraphIndex);
begin
  AParagraph.SuppressHyphenation := NewValue;
end;

{ TdxParagraphBackColorModifier }

function TdxParagraphBackColorModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): TColor;
begin
  Result := AParagraph.BackColor;
end;

procedure TdxParagraphBackColorModifier.ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex);
begin
  if (AParagraph.GetCell <> nil) and TdxColor.IsEmpty(NewValue) then
    AParagraph.BackColor := TdxColor.White
  else
    AParagraph.BackColor := NewValue;
end;

{ TdxRunFontColorPropertyModifier }

function TdxRunFontColorPropertyModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TColor;
begin
  Result := APos.MergedCharacterFormatting.ForeColor;
end;

function TdxRunFontColorPropertyModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TColor;
begin
  Result := ARun.ForeColor;
end;

procedure TdxRunFontColorPropertyModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  APos.CharacterFormatting.ForeColor := NewValue;
  APos.MergedCharacterFormatting.ForeColor := NewValue;
end;

procedure TdxRunFontColorPropertyModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ARun.ForeColor := NewValue;
end;

{ TdxFontPropertiesModifier }

procedure TdxFontPropertiesModifier.ApplyCharacterProperties(AProperties: IdxCharacterProperties);
var
  AInfo: TdxCharacterFormattingInfo;
  AOptions: TdxCharacterFormattingOptions;
begin
  AInfo := NewValue.Info;
  AOptions := NewValue.Options;
  if AOptions.UseFontName then
    AProperties.FontName := AInfo.FontName;
  if AOptions.UseFontBold then
    AProperties.FontBold := AInfo.FontBold;
  if AOptions.UseFontItalic then
    AProperties.FontItalic := AInfo.FontItalic;
  if AOptions.UseDoubleFontSize then
    AProperties.DoubleFontSize := AInfo.DoubleFontSize;
  if AOptions.UseForeColor then
    AProperties.ForeColor := AInfo.ForeColor;
  if AOptions.UseFontUnderlineType then
    AProperties.FontUnderlineType := AInfo.FontUnderlineType;
  if AOptions.UseUnderlineColor then
    AProperties.UnderlineColor := AInfo.UnderlineColor;
  if AOptions.UseFontStrikeoutType then
    AProperties.FontStrikeoutType := AInfo.FontStrikeoutType;
  if AOptions.UseScript then
    AProperties.Script := AInfo.Script;
  if AOptions.UseAllCaps then
    AProperties.AllCaps := AInfo.AllCaps;
  if AOptions.UseUnderlineWordsOnly then
    AProperties.UnderlineWordsOnly := AInfo.UnderlineWordsOnly;
  if AOptions.UseHidden then
    AProperties.Hidden := AInfo.Hidden;
end;

function TdxFontPropertiesModifier.CanModifyRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := not (ARun is TdxSeparatorTextRun);
end;

function TdxFontPropertiesModifier.GetInputPositionPropertyValue(APos: TdxInputPosition): TdxMergedCharacterProperties;
begin
  Result := TdxMergedCharacterProperties.Create(APos.MergedCharacterFormatting, TdxCharacterFormattingOptions.Create(TdxCharacterFormattingOptions.MaskUseAll));
end;

function TdxFontPropertiesModifier.GetRunPropertyValue(ARun: TdxTextRunBase): TdxMergedCharacterProperties;
var
  AFormattingInfo: TdxCharacterFormattingInfo;
  AOptions: TdxCharacterFormattingOptions;
begin
  AFormattingInfo := ARun.MergedCharacterFormatting;
  AOptions := TdxCharacterFormattingOptions.Create(TdxCharacterFormattingOptions.MaskUseAll);
  Result := TdxMergedCharacterProperties.Create(AFormattingInfo, AOptions);
end;

function TdxFontPropertiesModifier.Merge(ALeftValue,
  ARightValue: TdxMergedCharacterProperties): TdxMergedCharacterProperties;
var
  ATargetOptions: TdxCharacterFormattingOptions;
  ATargetInfo, ARunInfo: TdxCharacterFormattingInfo;
begin
  Result := TdxMergedCharacterProperties.Create(ALeftValue.Info, ALeftValue.Options);
  ATargetOptions := Result.Options;
  ATargetInfo := Result.Info;
  ARunInfo := ARightValue.Info;
  ATargetOptions.UseAllCaps := ATargetOptions.UseAllCaps and (ATargetInfo.AllCaps = ARunInfo.AllCaps);
  ATargetOptions.UseBackColor := ATargetOptions.UseBackColor and (ATargetInfo.BackColor = ARunInfo.BackColor);
  ATargetOptions.UseFontBold := ATargetOptions.UseFontBold and (ATargetInfo.FontBold = ARunInfo.FontBold);
  ATargetOptions.UseFontItalic := ATargetOptions.UseFontItalic and (ATargetInfo.FontItalic = ARunInfo.FontItalic);
  ATargetOptions.UseFontName := ATargetOptions.UseFontName and (ATargetInfo.FontName = ARunInfo.FontName);
  ATargetOptions.UseDoubleFontSize := ATargetOptions.UseDoubleFontSize and (ATargetInfo.DoubleFontSize = ARunInfo.DoubleFontSize);
  ATargetOptions.UseFontStrikeoutType := ATargetOptions.UseFontStrikeoutType and (ATargetInfo.FontStrikeoutType = ARunInfo.FontStrikeoutType);
  ATargetOptions.UseFontUnderlineType := ATargetOptions.UseFontUnderlineType and (ATargetInfo.FontUnderlineType = ARunInfo.FontUnderlineType);
  ATargetOptions.UseForeColor := ATargetOptions.UseForeColor and (ATargetInfo.ForeColor = ARunInfo.ForeColor);
  ATargetOptions.UseScript := ATargetOptions.UseScript and (ATargetInfo.Script = ARunInfo.Script);
  ATargetOptions.UseStrikeoutColor := ATargetOptions.UseStrikeoutColor and (ATargetInfo.StrikeoutColor = ARunInfo.StrikeoutColor);
  ATargetOptions.UseStrikeoutWordsOnly := ATargetOptions.UseStrikeoutWordsOnly and (ATargetInfo.StrikeoutWordsOnly = ARunInfo.StrikeoutWordsOnly);
  ATargetOptions.UseUnderlineColor := ATargetOptions.UseUnderlineColor and (ATargetInfo.UnderlineColor = ARunInfo.UnderlineColor);
  ATargetOptions.UseUnderlineWordsOnly := ATargetOptions.UseUnderlineWordsOnly and (ATargetInfo.UnderlineWordsOnly = ARunInfo.UnderlineWordsOnly);
  ATargetOptions.UseHidden := ATargetOptions.UseHidden and (ATargetInfo.Hidden = ARunInfo.Hidden);
end;

procedure TdxFontPropertiesModifier.ModifyInputPositionCore(APos: TdxInputPosition);
begin
  ApplyCharacterProperties(APos.CharacterFormatting);
  ApplyCharacterProperties(APos.MergedCharacterFormatting);
end;

procedure TdxFontPropertiesModifier.ModifyTextRun(ARun: TdxTextRunBase; ARunIndex: TdxRunIndex);
begin
  ApplyCharacterProperties(ARun);
end;

{ TdxParagraphPropertiesModifier }

function TdxParagraphPropertiesModifier.GetParagraphPropertyValue(
  AParagraph: TdxParagraph): TdxMergedParagraphProperties;
begin
  Result := AParagraph.GetMergedParagraphProperties;
end;

function TdxParagraphPropertiesModifier.Merge(ALeftValue,
  ARightValue: TdxMergedParagraphProperties): TdxMergedParagraphProperties;
var
  ATargetOptions: TdxParagraphFormattingOptions;
  ATargetInfo: TdxParagraphFormattingInfo;
  ARunInfo: TdxParagraphFormattingInfo;
begin
  Result := TdxMergedParagraphProperties.Create(ALeftValue.Info, ALeftValue.Options);
  ATargetOptions := Result.Options;
  ATargetInfo := Result.Info;
  ARunInfo := ARightValue.Info;
  ATargetOptions.UseLeftIndent := ATargetOptions.UseLeftIndent and (ATargetInfo.LeftIndent = ARunInfo.LeftIndent);
  ATargetOptions.UseRightIndent := ATargetOptions.UseRightIndent and (ATargetInfo.RightIndent = ARunInfo.RightIndent);
  ATargetOptions.UseFirstLineIndent := ATargetOptions.UseFirstLineIndent and (ATargetInfo.FirstLineIndent = ARunInfo.FirstLineIndent) and
    (ATargetInfo.FirstLineIndentType = ARunInfo.FirstLineIndentType);
  ATargetOptions.UseAlignment := ATargetOptions.UseAlignment and (ATargetInfo.Alignment = ARunInfo.Alignment);
  ATargetOptions.UseSpacingBefore := ATargetOptions.UseSpacingBefore and (ATargetInfo.SpacingBefore = ARunInfo.SpacingBefore);
  ATargetOptions.UseSpacingAfter := ATargetOptions.UseSpacingAfter and (ATargetInfo.SpacingAfter = ARunInfo.SpacingAfter);
  ATargetOptions.UseLineSpacing := ATargetOptions.UseLineSpacing and (ATargetInfo.LineSpacing = ARunInfo.LineSpacing) and
    (ATargetInfo.LineSpacingType = ARunInfo.LineSpacingType);
  ATargetOptions.UseSuppressHyphenation := ATargetOptions.UseSuppressHyphenation and (ATargetInfo.SuppressHyphenation = ARunInfo.SuppressHyphenation);
  ATargetOptions.UseSuppressLineNumbers := ATargetOptions.UseSuppressLineNumbers and (ATargetInfo.SuppressLineNumbers = ARunInfo.SuppressLineNumbers);
  ATargetOptions.UseContextualSpacing := ATargetOptions.UseContextualSpacing and (ATargetInfo.ContextualSpacing = ARunInfo.ContextualSpacing);
  ATargetOptions.UsePageBreakBefore := ATargetOptions.UsePageBreakBefore and (ATargetInfo.PageBreakBefore = ARunInfo.PageBreakBefore);
  ATargetOptions.UseBeforeAutoSpacing := ATargetOptions.UseBeforeAutoSpacing and (ATargetInfo.BeforeAutoSpacing = ARunInfo.BeforeAutoSpacing);
  ATargetOptions.UseAfterAutoSpacing := ATargetOptions.UseAfterAutoSpacing and (ATargetInfo.AfterAutoSpacing = ARunInfo.AfterAutoSpacing);
  ATargetOptions.UseKeepWithNext := ATargetOptions.UseKeepWithNext and (ATargetInfo.KeepWithNext = ARunInfo.KeepWithNext);
  ATargetOptions.UseKeepLinesTogether := ATargetOptions.UseKeepLinesTogether and (ATargetInfo.KeepLinesTogether = ARunInfo.KeepLinesTogether);
  ATargetOptions.UseWidowOrphanControl := ATargetOptions.UseWidowOrphanControl and (ATargetInfo.WidowOrphanControl = ARunInfo.WidowOrphanControl);
  ATargetOptions.UseOutlineLevel := ATargetOptions.UseOutlineLevel and (ATargetInfo.OutlineLevel = ARunInfo.OutlineLevel);
  ATargetOptions.UseBackColor := ATargetOptions.UseBackColor and (ATargetInfo.BackColor = ARunInfo.BackColor);
end;

procedure TdxParagraphPropertiesModifier.ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex);
var
  AInfo: TdxParagraphFormattingInfo;
  AOptions: TdxParagraphFormattingOptions;
begin
  AInfo := NewValue.Info;
  AOptions := NewValue.Options;
  if AOptions.UseRightIndent then
    AParagraph.RightIndent := AInfo.RightIndent;
  if AOptions.UseLeftIndent then
    AParagraph.LeftIndent := AInfo.LeftIndent;
  if AOptions.UseFirstLineIndent then
  begin
    AParagraph.FirstLineIndentType := AInfo.FirstLineIndentType;
    AParagraph.FirstLineIndent := AInfo.FirstLineIndent;
  end;
  if AOptions.UseAlignment then
    AParagraph.Alignment := AInfo.Alignment;
  if AOptions.UseSpacingAfter then
    AParagraph.SpacingAfter := AInfo.SpacingAfter;
  if AOptions.UseSpacingBefore then
    AParagraph.SpacingBefore := AInfo.SpacingBefore;
  if AOptions.UseLineSpacing then
  begin
    AParagraph.LineSpacingType := AInfo.LineSpacingType;
    AParagraph.LineSpacing := AInfo.LineSpacing;
  end;
  if AOptions.UseSuppressHyphenation then
    AParagraph.SuppressHyphenation := AInfo.SuppressHyphenation;
  if AOptions.UseSuppressLineNumbers then
    AParagraph.SuppressLineNumbers := AInfo.SuppressLineNumbers;
  if AOptions.UseContextualSpacing then
    AParagraph.ContextualSpacing := AInfo.ContextualSpacing;
  if AOptions.UsePageBreakBefore then
    AParagraph.PageBreakBefore := AInfo.PageBreakBefore;
  if AOptions.UseBeforeAutoSpacing then
    AParagraph.BeforeAutoSpacing := AInfo.BeforeAutoSpacing;
  if AOptions.UseAfterAutoSpacing then
    AParagraph.AfterAutoSpacing := AInfo.AfterAutoSpacing;
  if AOptions.UseKeepWithNext then
    AParagraph.KeepWithNext := AInfo.KeepWithNext;
  if AOptions.UseKeepLinesTogether then
    AParagraph.KeepLinesTogether := AInfo.KeepLinesTogether;
  if AOptions.UseWidowOrphanControl then
    AParagraph.WidowOrphanControl := AInfo.WidowOrphanControl;
  if AOptions.UseOutlineLevel then
    AParagraph.OutlineLevel := AInfo.OutlineLevel;
  if AOptions.UseBackColor then
    AParagraph.BackColor := AInfo.BackColor;
end;

{ TdxTabFormattingInfoModifier }

constructor TdxTabFormattingInfoModifier.Create(ANewValue: TdxTabFormattingInfo; ADefaultTabWidth: Integer);
begin
  inherited Create(ANewValue);
  NewDefaultTabWidth := ADefaultTabWidth;
end;

function TdxTabFormattingInfoModifier.GetMergedTabInfo(ANewTabInfo, AOldOwnTabInfo,
  AStyleTabInfo: TdxTabFormattingInfo): TdxTabFormattingInfo;
begin
  Result := NotImplemented;
end;

function TdxTabFormattingInfoModifier.GetParagraphPropertyValue(AParagraph: TdxParagraph): TdxTabFormattingInfo;
begin
  Result := AParagraph.GetTabs;
end;

function TdxTabFormattingInfoModifier.Merge(ALeftValue, ARightValue: TdxTabFormattingInfo): TdxTabFormattingInfo;
begin
{TODO:
  if ALeftValue.Equals(ARightValue) then
    Exit(ALeftValue.Clone);
  Result := TdxTabFormattingInfo.Create;}
  Result := NotImplemented;
end;

procedure TdxTabFormattingInfoModifier.ModifyParagraph(AParagraph: TdxParagraph; AParagraphIndex: TdxParagraphIndex);
var
  AResult: TdxTabFormattingInfo;
begin
  AResult := GetMergedTabInfo(NewValue, AParagraph.GetOwnTabs, AParagraph.ParagraphStyle.GetTabs);
  AParagraph.SetOwnTabs(AResult);
  AParagraph.DocumentModel.DocumentProperties.DefaultTabWidth := NewDefaultTabWidth;
end;

end.
