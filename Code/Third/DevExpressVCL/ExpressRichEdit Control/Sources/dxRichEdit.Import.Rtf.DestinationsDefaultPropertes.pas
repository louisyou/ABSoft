{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationsDefaultPropertes;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.Import.Rtf, dxRichEdit.Import.Rtf.DestinationPieceTable,
  dxRichEdit.DocumentModel.PieceTable;

type
  TdxDefaultCharacterPropertiesDestination = class(TdxDestinationPieceTable)
  protected
    function CanAppendText: Boolean; override;
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessCharCore(AChar: Char); override;
  public
    procedure BeforePopRtfState; override;
    procedure FinalizePieceTableCreation; override;
  end;

  TdxDefaultParagraphPropertiesDestination = class(TdxDestinationPieceTable)
  protected
    function CanAppendText: Boolean; override;
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessCharCore(AChar: Char); override;
  public
    procedure BeforePopRtfState; override;
    procedure FinalizePieceTableCreation; override;
  end;

implementation

uses
  dxRichEdit.DocumentModel.Core, dxRichEdit.Utils.BatchUpdateHelper, RTLConsts, 
  dxRichEdit.DocumentModel.CharacterFormatting, dxRichEdit.DocumentModel.ParagraphFormatting;

{ TdxDefaultCharacterPropertiesDestination }

procedure TdxDefaultCharacterPropertiesDestination.BeforePopRtfState;
var
  AInfo, AFormattingInfo: TdxCharacterFormattingBase;
  AMergedCharacterProperties: TdxMergedCharacterProperties;
begin
  AFormattingInfo := Importer.Position.CharacterFormatting;
  AInfo := Importer.DocumentModel.DefaultCharacterProperties.Info;
  AMergedCharacterProperties := TdxMergedCharacterProperties.Create(AInfo.Info, AInfo.Options);
  try
    Importer.ApplyCharacterProperties(Importer.DocumentModel.DefaultCharacterProperties,
      AFormattingInfo.Info, AMergedCharacterProperties, False);
  finally
    AMergedCharacterProperties.Free;
  end;
end;

procedure TdxDefaultCharacterPropertiesDestination.FinalizePieceTableCreation;
begin
//do nothing
end;

function TdxDefaultCharacterPropertiesDestination.CanAppendText: Boolean;
begin
  Result := False;
end;

function TdxDefaultCharacterPropertiesDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxDefaultCharacterPropertiesDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  AddCharacterPropertiesKeywords(ATable);
end;

procedure TdxDefaultCharacterPropertiesDestination.ProcessCharCore(AChar: Char);
begin
//do nothing
end;

{ TdxDefaultParagraphPropertiesDestination }

procedure TdxDefaultParagraphPropertiesDestination.BeforePopRtfState;
var
  AFormattingInfo: TdxRtfParagraphFormattingInfo;
  AInfo: TdxParagraphFormattingBase;
  AMergedProperties: TdxMergedParagraphProperties;
begin
  AFormattingInfo := Importer.Position.ParagraphFormattingInfo;
  AInfo := Importer.DocumentModel.DefaultParagraphProperties.Info;
  AMergedProperties := TdxMergedParagraphProperties.Create(AInfo.Info, AInfo.Options);
  try
    Importer.ApplyParagraphProperties(
      Importer.DocumentModel.DefaultParagraphProperties,
      AFormattingInfo, AMergedProperties, False);
  finally
    AMergedProperties.Free;
  end;
end;

procedure TdxDefaultParagraphPropertiesDestination.FinalizePieceTableCreation;
begin
// do nothing
end;

function TdxDefaultParagraphPropertiesDestination.CanAppendText: Boolean;
begin
  Result := False;
end;

function TdxDefaultParagraphPropertiesDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxDefaultParagraphPropertiesDestination.PopulateKeywordTable(
  ATable: TdxKeywordTranslatorTable);
begin
  AddParagraphPropertiesKeywords(ATable);
end;

procedure TdxDefaultParagraphPropertiesDestination.ProcessCharCore(AChar: Char);
begin
// do noting
end;

end.
