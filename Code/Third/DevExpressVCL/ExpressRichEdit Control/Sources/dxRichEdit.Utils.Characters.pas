{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.Characters;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

type

  { TdxCharacters }

  TdxCharacters = class sealed
  const
    Dot                        = '.';
    Colon                      = ':';
    Underscore                 = '_';
    EqualSign                  = '=';
    MiddleDot                  = #$00B7;
    Dash                       = '-';
    ParagraphMark              = #$000D;
    SectionMark                = #$001D;
    Hyphen                     = #$001F;
    TabMark                    = #$0009;
    NonBreakingSpace           = #$00A0;
    Space                      = ' ';
    EmSpace                    = #$2003;
    EnSpace                    = #$2002;
    QmSpace                    = #$2005;
    LineBreak                  = #$000B;
    PageBreak                  = #$000C;
    ColumnBreak                = #$000E;
    ObjectMark                 = #$FFFC;
    FloatingObjectMark         = #$0008;
    EmDash                     = #$2014;
    EnDash                     = #$2013;
    Bullet                     = #$2022;
    LeftSingleQuote            = #$2018;
    RightSingleQuote           = #$2019;
    LeftDoubleQuote            = #$201C;
    RightDoubleQuote           = #$201D;
    PilcrowSign                = #$00B6;
    CurrencySign               = #$00A4;
    CopyrightSymbol            = #$00A9;
    TrademarkSymbol            = #$2122;
    OptionalHyphen             = #$00AD;
    RegisteredTrademarkSymbol  = #$00AE;
    Ellipsis                   = #$2026;
    OpeningSingleQuotationMark = #$2018;
    ClosingSingleQuotationMark = #$2019;
    OpeningDoubleQuotationMark = #$201C;
    ClosingDoubleQuotationMark = #$201D;
    SeparatorMark              = '|';
  public
    constructor Create;
    class function IsCharDash(C: Char): Boolean; inline; static;
    class function IsCharSpace(C: Char): Boolean; inline; static;
  end;

implementation

uses
  RTLConsts, SysUtils;

constructor TdxCharacters.Create;
begin
  raise ENoConstructException.CreateResFmt(@sNoConstruct, [ClassName]);
end;

class function TdxCharacters.IsCharDash(C: Char): Boolean;
begin
  Result := (C = Dash) or (C = EmDash) or (C = EnDash);
end;

class function TdxCharacters.IsCharSpace(C: Char): Boolean;
begin
  Result := (C = Space) or (C = EmSpace) or (C = EnSpace);
end;

end.
