{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.DocumentProperties;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, dxCoreClasses, dxRichEdit.Utils.Types,
  dxRichEdit.DocumentModel.IndexBasedObject, dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.Core;

type

  TdxDocumentPropertiesChangeType = (
    None = 0,
    DefaultTabWidth,
    HyphenateDocument,
    DifferentOddAndEvenPages,
    PageBackColor,
    DisplayBackgroundShape,
    BatchUpdate
  );

  { TdxDocumentInfo }

  TdxDocumentInfo = class(TcxIUnknownObject, IdxCloneable<TdxDocumentInfo>, IdxSupportsCopyFrom<TdxDocumentInfo>,
    IdxSupportsSizeOf)
  public const
    HyphenateDocumentDefaultValue = False;
  private
    FDefaultTabWidth: Integer;
    FHyphenateDocument: Boolean;
    FDifferentOddAndEvenPages: Boolean;
    FPageBackColor: TColor;
    FDisplayBackgroundShape: Boolean;
  public
    constructor Create;

    function Clone: TdxDocumentInfo;
    function Equals(AObject: TObject): Boolean; override;
    procedure CopyFrom(const AInfo: TdxDocumentInfo);

    property DefaultTabWidth: Integer read FDefaultTabWidth write FDefaultTabWidth;
    property HyphenateDocument: Boolean read FHyphenateDocument write FHyphenateDocument;
    property DifferentOddAndEvenPages: Boolean read FDifferentOddAndEvenPages write FDifferentOddAndEvenPages;
    property PageBackColor: TColor read FPageBackColor write FPageBackColor;
    property DisplayBackgroundShape: Boolean read FDisplayBackgroundShape write FDisplayBackgroundShape;
  end;

  { TdxDocumentProperties }

  TdxDocumentProperties = class(TdxRichEditIndexBasedObject<TdxDocumentInfo>)
  private
    FOnPageBackgroundChanged: TdxEventHandler;
    function GetDefaultTabWidth: Integer;
    procedure SetDefaultTabWidth(const Value: Integer);
    function GetHyphenateDocument: Boolean;
    procedure SetHyphenateDocument(const Value: Boolean);
    function GetDifferentOddAndEvenPages: Boolean;
    procedure SetDifferentOddAndEvenPages(const Value: Boolean);
    function GetPageBackColor: TColor;
    procedure SetPageBackColor(const Value: TColor);
    function GetDisplayBackgroundShape: Boolean;
    procedure SetDisplayBackgroundShape(const Value: Boolean);
  protected
    function SetDefaultTabWidthCore(const AInfo: TdxDocumentInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; virtual;
    function SetHyphenateDocumentCore(const AInfo: TdxDocumentInfo;
      const AValue: Boolean): TdxDocumentModelChangeActions; virtual;
    function SetDifferentOddAndEvenPagesCore(const AInfo: TdxDocumentInfo;
      const AValue: Boolean): TdxDocumentModelChangeActions; virtual;
    function SetPageBackColorCore(const AInfo: TdxDocumentInfo;
      const AValue: TColor): TdxDocumentModelChangeActions; virtual;
    function SetDisplayBackgroundShapeCore(const AInfo: TdxDocumentInfo;
      const AValue: Boolean): TdxDocumentModelChangeActions; virtual;
    procedure RaisePageBackgroundChanged; virtual;
    procedure OnIndexChanged; override;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxDocumentInfo>; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel); reintroduce;

    property DefaultTabWidth: Integer read GetDefaultTabWidth write SetDefaultTabWidth;
    property HyphenateDocument: Boolean read GetHyphenateDocument write SetHyphenateDocument;
    property DifferentOddAndEvenPages: Boolean read GetDifferentOddAndEvenPages write SetDifferentOddAndEvenPages;
    property PageBackColor: TColor read GetPageBackColor write SetPageBackColor;
    property DisplayBackgroundShape: Boolean read GetDisplayBackgroundShape write SetDisplayBackgroundShape;
    property OnPageBackgroundChanged: TdxEventHandler read FOnPageBackgroundChanged write FOnPageBackgroundChanged;
  end;

  { TdxDocumentPropertiesChangeActionsCalculator }

  TdxDocumentPropertiesChangeActionsCalculator = class
  public
    class function CalculateChangeActions(AChange: TdxDocumentPropertiesChangeType): TdxDocumentModelChangeActions;
  end;

  { TdxDocumentInfoCache }

  TdxDocumentInfoCache = class(TdxUniqueItemsCache<TdxDocumentInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxDocumentInfo; override;
  end;

implementation

uses
  RTLConsts, dxRichEdit.Utils.BatchUpdateHelper,
  dxRichEdit.DocumentModel.PieceTable;

{ TdxDocumentInfo }

constructor TdxDocumentInfo.Create;
begin
  inherited Create;
  FHyphenateDocument := HyphenateDocumentDefaultValue;
end;

function TdxDocumentInfo.Clone: TdxDocumentInfo;
begin
  Result := TdxDocumentInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxDocumentInfo.CopyFrom(const AInfo: TdxDocumentInfo);
begin
  DefaultTabWidth := AInfo.DefaultTabWidth;
  HyphenateDocument := AInfo.HyphenateDocument;
  DifferentOddAndEvenPages := AInfo.DifferentOddAndEvenPages;
  PageBackColor := AInfo.PageBackColor;
  DisplayBackgroundShape := AInfo.DisplayBackgroundShape;
end;

function TdxDocumentInfo.Equals(AObject: TObject): Boolean;
var
  AInfo: TdxDocumentInfo absolute AObject;
begin
  Assert(AObject is TdxDocumentInfo);
  Result := (DefaultTabWidth = AInfo.DefaultTabWidth) and
    (HyphenateDocument = AInfo.HyphenateDocument) and
    (DifferentOddAndEvenPages = AInfo.DifferentOddAndEvenPages) and
    (PageBackColor = AInfo.PageBackColor) and
    (DisplayBackgroundShape = AInfo.DisplayBackgroundShape);
end;

{ TdxDocumentProperties }

constructor TdxDocumentProperties.Create(ADocumentModel: TdxCustomDocumentModel);

  function GetMainPieceTable(ADocumentModel: TdxDocumentModel): TdxPieceTable;
  begin
    Assert(ADocumentModel <> nil);
    Result := ADocumentModel.MainPieceTable;
  end;

begin
  inherited Create(GetMainPieceTable(TdxDocumentModel(ADocumentModel)));
end;

function TdxDocumentProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxDocumentPropertiesChangeActionsCalculator.CalculateChangeActions(TdxDocumentPropertiesChangeType.BatchUpdate);
end;

function TdxDocumentProperties.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxDocumentInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.DocumentInfoCache;
end;

function TdxDocumentProperties.GetDefaultTabWidth: Integer;
begin
  Result := Info.DefaultTabWidth;
end;

function TdxDocumentProperties.GetDifferentOddAndEvenPages: Boolean;
begin
  Result := Info.DifferentOddAndEvenPages;
end;

function TdxDocumentProperties.GetDisplayBackgroundShape: Boolean;
begin
  Result := Info.DisplayBackgroundShape;
end;

function TdxDocumentProperties.GetHyphenateDocument: Boolean;
begin
  Result := Info.HyphenateDocument;
end;

function TdxDocumentProperties.GetPageBackColor: TColor;
begin
  Result := Info.PageBackColor;
end;

procedure TdxDocumentProperties.OnIndexChanged;
begin
  inherited OnIndexChanged;
  RaisePageBackgroundChanged; 
end;

procedure TdxDocumentProperties.RaisePageBackgroundChanged;
begin
  if Assigned(FOnPageBackgroundChanged) then
    FOnPageBackgroundChanged(Self, nil); 
end;

procedure TdxDocumentProperties.SetDefaultTabWidth(const Value: Integer);
begin
  Assert(Value > 0);
  if DefaultTabWidth = Value then
    Exit;
  SetPropertyValue<Integer>(SetDefaultTabWidthCore, Value);
end;

function TdxDocumentProperties.SetDefaultTabWidthCore(const AInfo: TdxDocumentInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.DefaultTabWidth := AValue;
  Result := TdxDocumentPropertiesChangeActionsCalculator.CalculateChangeActions(TdxDocumentPropertiesChangeType.DefaultTabWidth);
end;

procedure TdxDocumentProperties.SetDifferentOddAndEvenPages(const Value: Boolean);
begin
  if DifferentOddAndEvenPages = Value then
    Exit;
  SetPropertyValue<Boolean>(SetDifferentOddAndEvenPagesCore, Value);
end;

function TdxDocumentProperties.SetDifferentOddAndEvenPagesCore(const AInfo: TdxDocumentInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.DifferentOddAndEvenPages := AValue;
  Result := TdxDocumentPropertiesChangeActionsCalculator.CalculateChangeActions(TdxDocumentPropertiesChangeType.DifferentOddAndEvenPages);
end;

procedure TdxDocumentProperties.SetDisplayBackgroundShape(const Value: Boolean);
begin
  if DisplayBackgroundShape = Value then
    Exit;
  SetPropertyValue<Boolean>(SetDisplayBackgroundShapeCore, Value);
end;

function TdxDocumentProperties.SetDisplayBackgroundShapeCore(const AInfo: TdxDocumentInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.DisplayBackgroundShape := AValue;
  Result := TdxDocumentPropertiesChangeActionsCalculator.CalculateChangeActions(TdxDocumentPropertiesChangeType.DisplayBackgroundShape);
end;

procedure TdxDocumentProperties.SetHyphenateDocument(const Value: Boolean);
begin
  if HyphenateDocument = Value then
    Exit;
  SetPropertyValue<Boolean>(SetHyphenateDocumentCore, Value);
end;

function TdxDocumentProperties.SetHyphenateDocumentCore(const AInfo: TdxDocumentInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.HyphenateDocument := AValue;
  Result := TdxDocumentPropertiesChangeActionsCalculator.CalculateChangeActions(TdxDocumentPropertiesChangeType.HyphenateDocument);
end;

procedure TdxDocumentProperties.SetPageBackColor(const Value: TColor);
begin
  if PageBackColor = Value then
    Exit;
  SetPropertyValue<TColor>(SetPageBackColorCore, Value);
end;

function TdxDocumentProperties.SetPageBackColorCore(const AInfo: TdxDocumentInfo;
  const AValue: TColor): TdxDocumentModelChangeActions;
begin
  AInfo.PageBackColor := AValue;
  Result := TdxDocumentPropertiesChangeActionsCalculator.CalculateChangeActions(TdxDocumentPropertiesChangeType.PageBackColor);
end;

{ TdxDocumentPropertiesChangeActionsCalculator }

class function TdxDocumentPropertiesChangeActionsCalculator.CalculateChangeActions(
  AChange: TdxDocumentPropertiesChangeType): TdxDocumentModelChangeActions;
const
  ADocumentPropertiesChangeActionsTable: array[TdxDocumentPropertiesChangeType] of TdxDocumentModelChangeActions = (
    [], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSpellingCheck], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSpellingCheck], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSpellingCheck] 
  );
begin
  Result := ADocumentPropertiesChangeActionsTable[AChange];
end;

{ TdxDocumentInfoCache }

function TdxDocumentInfoCache.CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxDocumentInfo;
begin
  Result := TdxDocumentInfo.Create;
  Result.DefaultTabWidth := AUnitConverter.TwipsToModelUnits(720);
  Result.HyphenateDocument := TdxDocumentInfo.HyphenateDocumentDefaultValue;
end;

end.
