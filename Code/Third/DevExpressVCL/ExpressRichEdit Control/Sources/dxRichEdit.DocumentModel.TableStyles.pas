{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.TableStyles;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Windows, SysUtils, Graphics, Generics.Collections,
  dxCoreClasses,
  dxRichEdit.DocumentModel.Styles, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentModel.Core, dxRichEdit.DocumentModel.IndexBasedObject,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.TableFormatting,
  dxRichEdit.DocumentModel.History.IndexChangedHistoryItem, dxRichEdit.DocumentModel.Borders,
  dxRichEdit.DocumentModel.TabFormatting;

type
  TdxTableStyle = class;
  TdxTableConditionalStyle = class;
  TdxTableConditionalStyleProperties = class;

  { IdxTableStyle }

  IdxTableStyle = interface
  ['{0747EB4A-AAC0-44B4-9A30-8F711DC88686}']
    function GetTableProperties: TdxTableProperties;
    function GetTableRowProperties: TdxTableRowProperties;
    function GetTableCellProperties: TdxTableCellProperties;
    function GetCharacterProperties: TdxCharacterProperties;
    function GetParagraphProperties: TdxParagraphProperties;
    function GetParent: TdxTableStyle;

    property TableProperties: TdxTableProperties read GetTableProperties;
    property TableRowProperties: TdxTableRowProperties read GetTableRowProperties;
    property TableCellProperties: TdxTableCellProperties read GetTableCellProperties;
    property CharacterProperties: TdxCharacterProperties read GetCharacterProperties;
    property ParagraphProperties: TdxParagraphProperties read GetParagraphProperties;
    property Parent: TdxTableStyle read GetParent;
  end;

  { TdxTableStyle }

  TdxTableStyle = class(TdxParagraphPropertiesBasedStyle, IdxCharacterPropertiesContainer, IdxCellPropertiesOwner, IdxTableStyle)
  protected
    type
      TdxMaskPrediacate = reference to function(AConditionalStyle: TdxTableConditionalStyle): Boolean; 
  private
    FTableProperties: TdxTableProperties;
    FTableRowProperties: TdxTableRowProperties;
    FTableCellProperties: TdxTableCellProperties;
    FCharacterProperties: TdxCharacterProperties;

    FConditionalStyleProperties: TdxTableConditionalStyleProperties;

    function GetTableProperties: TdxTableProperties; overload;
    function GetConditionalStyleProperties: TdxTableConditionalStyleProperties;
    function GetTableRowProperties: TdxTableRowProperties; overload;
    function GetTableCellProperties: TdxTableCellProperties; overload;
    function GetCharacterProperties: TdxCharacterProperties; overload;
    function GetHasColumnBandingStyleProperties: Boolean;
    function GetHasRowBandingStyleProperties: Boolean;
    function GetHasConditionalStyleProperties: Boolean;
    function GetParent: TdxTableStyle;
    procedure SetParent(const Value: TdxTableStyle);
  protected
    function GetType: TdxStyleType; override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel;
      AParent: TdxTableStyle = nil; const AStyleName: string = ''); reintroduce;
    destructor Destroy; override;

    procedure SubscribeCharacterPropertiesEvents; virtual;
    procedure SubscribeTablePropertiesPropertiesEvents; virtual;
    procedure SubscribeTableRowPropertiesEvents; virtual;
    procedure SubscribeTableCellPropertiesEvents; virtual;
    procedure OnObtainAffectedRange(ASender: TObject; E: TdxObtainAffectedRangeEventArgs);
    procedure MergeConditionalProperties(AMerger: TdxCharacterPropertiesMerger; ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType);
    procedure TryGetConditionalStyle(AConditionalStyles: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
      AStyleType: TdxConditionalTableStyleFormattingTypes; out AResult: TdxTableConditionalStyle; AMaskPredicate: TdxMaskPrediacate); overload;
    function GetConditionalPropertiesSource(ARowType: TdxConditionalRowType;
      AColumnType: TdxConditionalColumnType; AMask: Integer;
      AIsBorderCell: Boolean; out AInsideBorder: Boolean): TdxTableConditionalStyle; overload;
    procedure TryGetConditionalStyle(AConditionalStyles: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
      AStyleType: TdxConditionalTableStyleFormattingTypes; out AResult: TdxTableConditionalStyle; AMask: Integer); overload;
    function GetConditionalPropertiesSource(ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType;
      AMaskPredicate: TdxMaskPrediacate): TdxTableConditionalStyle; overload;
    function GetConditionalPropertiesMask(ARowType: TdxConditionalRowType;
      AColumnType: TdxConditionalColumnType): TdxConditionalTableStyleFormattingTypes;
    function GetTableProperties(AMask: Integer; ARowType: TdxConditionalRowType; 
      AColumnType: TdxConditionalColumnType): TdxTableProperties; overload;
    function GetTableProperties(AMask: Integer; ARowType: TdxConditionalRowType; 
      AColumnType: TdxConditionalColumnType; AIsBorderCell: Boolean;
        out AInsideBorder: Boolean): TdxTableProperties; overload; virtual;
    function GetMergedCharacterProperties(ARowType: TdxConditionalRowType;
      AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties; overload; virtual;
    function GetMergedCharacterProperties: TdxMergedCharacterProperties; overload; virtual;
    function GetMergedWithDefaultCharacterProperties(ARowType: TdxConditionalRowType;
      AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties; virtual;
    function GetMergedTableProperties: TdxMergedTableProperties; virtual;
    function GetMergedWithDefaultTableProperties: TdxMergedTableProperties; virtual;
    function GetMergedWithDefaultTableRowProperties: TdxMergedTableRowProperties; virtual;
    function GetMergedWithDefaultTableCellProperties: TdxMergedTableCellProperties; virtual;
    function GetTableCellProperties(AMask: Integer; ARowType: TdxConditionalRowType; 
      AColumnType: TdxConditionalColumnType): TdxTableCellProperties; overload; virtual;
    function GetCharacterProperties(AMask: TdxUsedCharacterFormattingOption; ARowType: TdxConditionalRowType; 
      AColumnType: TdxConditionalColumnType): TdxCharacterProperties; overload; virtual;
    function GetParagraphProperties(AMask: TdxUsedParagraphFormattingOption; ARowType: TdxConditionalRowType; 
      AColumnType: TdxConditionalColumnType): TdxParagraphProperties; virtual;
    function GetMergedParagraphProperties(ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType): TdxMergedParagraphProperties; reintroduce; overload; virtual;
    function GetMergedParagraphProperties: TdxMergedParagraphProperties; overload; override;
    function GetTableRowProperties(AMask: Integer; 
      ARowType: TdxConditionalRowType): TdxTableRowProperties; overload; virtual;
    procedure MergePropertiesWithParent; override;
    procedure CopyProperties(ASource: TdxStyleBase); override;
    function Copy(ATargetModel: TdxCustomDocumentModel): Integer; override;
    function CopyTo(ATargetModel: TdxCustomDocumentModel): TdxTableStyle; virtual;
    procedure ApplyPropertiesDiff(AStyle: TdxTableStyle);
    function CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnCharacterPropertiesChanged;
    function CreateCellPropertiesChangedHistoryItem(AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;

    procedure ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType); override;
    function HasColumnStyle(AConditionalColumnType: TdxConditionalColumnType): Boolean;
    function HasRowStyle(AConditionalRowType: TdxConditionalRowType): Boolean;
    function GetMergedCharacterProperties(AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedCharacterProperties; overload;
    function GetMergedParagraphProperties(AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedParagraphProperties; reintroduce; overload;
    function GetMergedTableRowProperties: TdxMergedTableRowProperties; overload; virtual;
    function GetMergedTableRowProperties(ARowType: TdxConditionalRowType): TdxMergedTableRowProperties; overload;
    function GetMergedTableRowProperties(AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableRowProperties; overload;
    function GetMergedTableCellProperties: TdxMergedTableCellProperties; overload; virtual;
    function GetMergedTableCellProperties(ARowType: TdxConditionalRowType; AColType: TdxConditionalColumnType): TdxMergedTableCellProperties; overload;
    function GetMergedTableCellProperties(AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableCellProperties; overload;

    property Parent: TdxTableStyle read GetParent write SetParent;
    property HasConditionalStyleProperties: Boolean read GetHasConditionalStyleProperties;
    property HasRowBandingStyleProperties: Boolean read GetHasRowBandingStyleProperties;
    property HasColumnBandingStyleProperties: Boolean read GetHasColumnBandingStyleProperties;
    property ConditionalStyleProperties: TdxTableConditionalStyleProperties read GetConditionalStyleProperties;
    property TableProperties: TdxTableProperties read GetTableProperties;
    property TableRowProperties: TdxTableRowProperties read GetTableRowProperties;
    property TableCellProperties: TdxTableCellProperties read GetTableCellProperties;
    property CharacterProperties: TdxCharacterProperties read GetCharacterProperties;
  end;

  { TdxTableStyleCollection }

  TdxTableStyleCollection = class(TdxStyleCollectionBase)
  public const
    DefaultTableStyleIndex = 0;
    TableSimpleStyleName = 'Table Simple 1';
    DefaultTableStyleName = 'Normal Table';
  private const
    FDefaultLeftMargin  = 108;
    FDefaultRightMargin = 108;
    FDefaultBorderWidth = 10;
    FDefaultBorderLineStyle = TdxBorderLineStyle.Single;
    function GetItem(Index: Integer): TdxTableStyle;
    function GetDocumentModel: TdxCustomDocumentModel;
    function GetPieceTable: TObject;
  protected
    function CreateNormalTableStyle(AChangeDefaultTableStyle: Boolean): TdxTableStyle; reintroduce; virtual;
    function CreateTableSimpleStyle: TdxTableStyle; virtual;
    function CreateDefaultItem: TdxStyleBase; override;
    procedure SetDefaultBorder(ABorder: TdxBorderBase; AStyle: TdxBorderLineStyle; AWidthInTwips: Integer);
    procedure SetDefaultMargin(AMargin: TdxWidthUnit; AValueInTwips: Integer);
    procedure NotifyPieceTableStyleDeleting(APieceTable: TObject; AStyle: TdxStyleBase); override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; AChangeDefaultTableStyle: Boolean); reintroduce;

    property Self[Index: Integer]: TdxTableStyle read GetItem; default;
    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
    property PieceTable: TObject read GetPieceTable;
  end;

  { TdxTableConditionalStyleProperties }

  TdxTableConditionalStyleProperties = class
  protected
    class var StyleTypes: TArray<TdxConditionalTableStyleFormattingTypes>;
    class constructor Initialize;
  private
    FItems: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
    FOwner: TdxTableStyle;
    function GetDocumentModel: TdxCustomDocumentModel;
    function GetItem(ACondition: TdxConditionalTableStyleFormattingTypes): TdxTableConditionalStyle;
  protected
    procedure CopyConditionalStyle(ACondition: TdxConditionalTableStyleFormattingTypes;
      AConditionalProperties: TdxTableConditionalStyleProperties);
    procedure InitializeItems(ADocumentModel: TdxCustomDocumentModel);
    procedure AddConditionalStyle(ADocumentModel: TdxCustomDocumentModel; ACondition: TdxConditionalTableStyleFormattingTypes);
  public
    constructor Create(AOwner: TdxTableStyle);
    function GetStyleSafe(ACondition: TdxConditionalTableStyleFormattingTypes): TdxTableConditionalStyle;
    procedure AddStyle(AStyle: TdxTableConditionalStyle);
    procedure CopyFrom(AConditionalProperties: TdxTableConditionalStyleProperties);
    function ContainsStyle(AConditionalTableStyleFormattingType: TdxConditionalTableStyleFormattingTypes): Boolean;
    function HasNonNullStyle: Boolean;

    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
    property Self[ACondition: TdxConditionalTableStyleFormattingTypes]: TdxTableConditionalStyle read GetItem; default;
    property Items: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle> read FItems;
    property Owner: TdxTableStyle read FOwner;
  end;

  { TdxTableConditionalStyle }

  TdxTableConditionalStyle = class(TcxIUnknownObject, IdxCellPropertiesOwner, IdxCharacterPropertiesContainer,
    IdxParagraphPropertiesContainer, IdxTableStyle)
  private
    FConditionType: TdxConditionalTableStyleFormattingTypes;
    FOwner: TdxTableStyle;
    FTableProperties: TdxTableProperties;
    FTableRowProperties: TdxTableRowProperties;
    FTableCellProperties: TdxTableCellProperties;
    FParagraphProperties: TdxParagraphProperties;
    FTabs: TdxTabProperties;
    FCharacterProperties: TdxCharacterProperties;
    function GetDocumentModel: TdxCustomDocumentModel;
    function GetInnerPieceTable: TObject; overload;
    function GetPieceTable: TObject; overload; 
    function GetTableProperties: TdxTableProperties; overload;
    function GetTableRowProperties: TdxTableRowProperties; overload;
    function GetTableCellProperties: TdxTableCellProperties; overload;
    function GetCharacterProperties: TdxCharacterProperties;
    function GetParagraphProperties: TdxParagraphProperties;
    function GetTabs: TdxTabProperties;
    function GetParent: TdxTableStyle;
  protected
    property Tabs: TdxTabProperties read GetTabs;
  public
    constructor Create(AOwner: TdxTableStyle; AConditionType: TdxConditionalTableStyleFormattingTypes);
    destructor Destroy; override;

    function GetTableProperties(AMask: Integer): TdxTableProperties; overload; virtual; 
    function GetTableRowProperties(AMask: Integer): TdxTableRowProperties; overload; virtual; 
    function GetTableCellProperties(AMask: Integer): TdxTableCellProperties; overload; virtual; 
    procedure CopyFrom(ACondition: TdxTableConditionalStyle);
    function CreateCellPropertiesChangedHistoryItem(AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;

    function CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnCharacterPropertiesChanged;
    function CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnParagraphPropertiesChanged;
    function GetMergedCharacterProperties: TdxMergedCharacterProperties;
    function GetMergedWithDefaultCharacterProperties: TdxMergedCharacterProperties; virtual;
    function GetMergedParagraphProperties: TdxMergedParagraphProperties; virtual;
    function GetMergedWithDefaultParagraphProperties: TdxMergedParagraphProperties; virtual;
    function GetMergedTableProperties: TdxMergedTableProperties; virtual;
    function GetMergedTableRowProperties: TdxMergedTableRowProperties; virtual;
    function GetMergedTableCellProperties: TdxMergedTableCellProperties; virtual;

    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
    property PieceTable: TObject read GetInnerPieceTable;
    property ConditionType: TdxConditionalTableStyleFormattingTypes read FConditionType;

    property TableProperties: TdxTableProperties read GetTableProperties;
    property TableRowProperties: TdxTableRowProperties read GetTableRowProperties;
    property TableCellProperties: TdxTableCellProperties read GetTableCellProperties;
    property CharacterProperties: TdxCharacterProperties read GetCharacterProperties;
    property ParagraphProperties: TdxParagraphProperties read GetParagraphProperties;

    property Parent: TdxTableStyle read GetParent;
  end;

  { IdxTableCellStyle }

  TdxTableCellStyle = class;

  IdxTableCellStyle = interface
  ['{B93BD106-3B85-4100-A8D1-8D988C0796DC}']
    function GetTableCellProperties: TdxTableCellProperties;
    function GetCharacterProperties: TdxCharacterProperties;
    function GetParagraphProperties: TdxParagraphProperties;
    function GetParent: TdxTableCellStyle;

    property TableCellProperties: TdxTableCellProperties read GetTableCellProperties;
    property CharacterProperties: TdxCharacterProperties read GetCharacterProperties;
    property ParagraphProperties: TdxParagraphProperties read GetParagraphProperties;
    property Parent: TdxTableCellStyle read GetParent;
  end;

  { TdxTableCellStyle }

  TdxTableCellStyle = class(TdxParagraphPropertiesBasedStyle, IdxCharacterPropertiesContainer,
    IdxCellPropertiesOwner, IdxTableCellStyle)
  private
    FTableCellProperties: TdxTableCellProperties;
    FCharacterProperties: TdxCharacterProperties;
    function GetTableCellProperties: TdxTableCellProperties; overload;
    function GetParentStyle: TdxTableCellStyle;
    procedure SetParentStyle(const Value: TdxTableCellStyle);
  strict protected
    function GetCharacterProperties: TdxCharacterProperties; overload;
    function GetParagraphProperties: TdxParagraphProperties; overload;
    function GetParent: TdxTableCellStyle;
  protected
    function GetType: TdxStyleType; override;

    procedure SubscribeCharacterPropertiesEvents; virtual;
    procedure SubscribeTableCellPropertiesEvents; virtual;
    procedure OnObtainAffectedRange(ASender: TObject; E: TdxObtainAffectedRangeEventArgs); virtual;
    procedure MergePropertiesWithParent; override;
    function CopyTo(ATargetModel: TdxCustomDocumentModel): TdxTableCellStyle; virtual;
    procedure ApplyPropertiesDiff(AStyle: TdxTableCellStyle);
    function CreateCellPropertiesChangedHistoryItem(AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    function GetMergedTableRowProperties(AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableRowProperties;
    function GetMergedTableCellProperties(ARowType: TdxConditionalRowType; AColType: TdxConditionalColumnType): TdxMergedTableCellProperties; overload;
    function GetMergedTableCellProperties(AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableCellProperties; overload;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel;
      AParent: TdxTableCellStyle = nil; const AStyleName: string = ''); reintroduce;
    destructor Destroy; override;

    procedure CopyProperties(ASource: TdxStyleBase); override;

    function GetMergedCharacterProperties(ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties; virtual;
    function GetMergedWithDefaultCharacterProperties(ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties; virtual;
    function GetMergedWithDefaultTableCellProperties: TdxMergedTableCellProperties; virtual;
    function GetTableCellProperties(AMask: Integer): TdxTableCellProperties; overload; virtual; 
    function GetCharacterProperties(AMask: TdxUsedCharacterFormattingOption; ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType): TdxCharacterProperties; overload; virtual; 
    function GetParagraphProperties(AMask: TdxUsedParagraphFormattingOption; ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType): TdxParagraphProperties; overload; virtual; 
    function GetMergedParagraphProperties(ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType): TdxMergedParagraphProperties; reintroduce; overload; virtual;
    function GetMergedParagraphProperties: TdxMergedParagraphProperties; overload; override;
    function Copy(ATargetModel: TdxCustomDocumentModel): Integer; override;

    property Parent: TdxTableCellStyle read GetParentStyle write SetParentStyle;
    //IdxCharacterPropertiesContainer
    function CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnCharacterPropertiesChanged;
    procedure ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType); override;
    function GetMergedTableCellProperties: TdxMergedTableCellProperties; overload; virtual;

    property TableCellProperties: TdxTableCellProperties read GetTableCellProperties;
    property CharacterProperties: TdxCharacterProperties read FCharacterProperties;
  end;

  { TdxTableCellStyleCollection }

  TdxTableCellStyleCollection = class(TdxStyleCollectionBase)
  strict private
    const FDefaultLeftMargin = 108;
    const FDefaultRightMargin = 108;
    const FDefaultBorderWidth = 10;
    const FDefaultBorderLineStyle = TdxBorderLineStyle.Single;
  public
    const DefaultTableCellStyleIndex = 0;
    class var TableCellSimpleStyleName: string;
    class var DefaultTableCellStyleName: string;
  strict protected
    class constructor Initialize;
  private
    procedure SetDefaultBorder(ABorder: TdxBorderBase; AStyle: TdxBorderLineStyle; AWidthInTwips: Integer);
    procedure SetDefaultMargin(AMargin: TdxWidthUnit; AValueInTwips: Integer);
    function GetDocumentModel: TdxCustomDocumentModel;
    function GetItem(Index: Integer): TdxTableCellStyle;
  protected
    function CreateNormalTableCellStyle(AChangeDefaultTableCellStyle: Boolean): TdxTableCellStyle; virtual;
    function CreateTableSimpleStyle: TdxTableCellStyle; virtual;
    function CreateDefaultItem: TdxStyleBase; override;
    procedure NotifyPieceTableStyleDeleting(APieceTable: TObject; AStyle: TdxStyleBase); override;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel; AChangeDefaultTableCellStyle: Boolean); reintroduce;

    property Self[Index: Integer]: TdxTableCellStyle read GetItem; default;
    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
  end;

implementation

uses
  Classes, RTLConsts, dxRichEdit.Utils.BatchUpdateHelper, 
  dxRichEdit.DocumentModel.PieceTable;

type
  TdxTablePropertiesAccess = class(TdxTableProperties);
  TdxTableCellPropertiesAccess = class(TdxTableCellProperties);

  { TdxStyleBaseHelper }

  TdxStyleBaseHelper = class helper for TdxTableStyle
  private
    function GetDocumentModel: TdxDocumentModel;
    function GetPieceTable: TdxPieceTable;
  public
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property PieceTable: TdxPieceTable read GetPieceTable;
  end;

  { TdxTableConditionalStyleHelper }

  TdxTableConditionalStyleHelper = class helper for TdxTableConditionalStyle
  private
    function GetDocumentModel: TdxDocumentModel;
    function GetPieceTable: TdxPieceTable;
  public
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property PieceTable: TdxPieceTable read GetPieceTable;
  end;

  { TdxTableCellStyleHelper }

  TdxTableCellStyleHelper = class helper for TdxTableCellStyle
  private
    function GetDocumentModel: TdxDocumentModel;
    function GetPieceTable: TdxPieceTable;
  public
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property PieceTable: TdxPieceTable read GetPieceTable;
  end;

{ TdxStyleBaseHelper }

function TdxStyleBaseHelper.GetDocumentModel: TdxDocumentModel;
begin
  Result := TdxDocumentModel(inherited DocumentModel);
end;

function TdxStyleBaseHelper.GetPieceTable: TdxPieceTable;
begin
  Result := TdxPieceTable(inherited PieceTable);
end;

{ TdxTableConditionalStyleHelper }

function TdxTableConditionalStyleHelper.GetDocumentModel: TdxDocumentModel;
begin
  Result := TdxDocumentModel(inherited DocumentModel);
end;

function TdxTableConditionalStyleHelper.GetPieceTable: TdxPieceTable;
begin
  Result := TdxPieceTable(inherited PieceTable);
end;

{ TdxTableCellStyleHelper }

function TdxTableCellStyleHelper.GetDocumentModel: TdxDocumentModel;
begin
  Result := TdxDocumentModel(inherited DocumentModel);
end;

function TdxTableCellStyleHelper.GetPieceTable: TdxPieceTable;
begin
  Result := TdxPieceTable(inherited PieceTable);
end;

{ TdxTableStyle }

constructor TdxTableStyle.Create(ADocumentModel: TdxCustomDocumentModel;
  AParent: TdxTableStyle = nil; const AStyleName: string = '');
begin
  inherited Create(ADocumentModel, AParent, AStyleName);
  FCharacterProperties := TdxCharacterProperties.Create(Self);
  SubscribeCharacterPropertiesEvents;
end;

destructor TdxTableStyle.Destroy;
begin
  FreeAndNil(FTableProperties);
  FreeAndNil(FTableRowProperties);
  FreeAndNil(FTableCellProperties);
  FreeAndNil(FCharacterProperties);
  inherited Destroy;
end;

procedure TdxTableStyle.ApplyPropertiesDiff(AStyle: TdxTableStyle);
begin
  ParagraphProperties.ApplyPropertiesDiff(AStyle.ParagraphProperties,
    AStyle.GetMergedWithDefaultParagraphProperties.Info, GetMergedWithDefaultParagraphProperties.Info);
  CharacterProperties.ApplyPropertiesDiff(AStyle.CharacterProperties,
    AStyle.GetMergedWithDefaultCharacterProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal).Info,
    GetMergedWithDefaultCharacterProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal).Info); 
end;

function TdxTableStyle.Copy(ATargetModel: TdxCustomDocumentModel): Integer;
var
  I: Integer;
  ATableStyles: TdxTableStyleCollection;
  ATargetDocumentModel: TdxDocumentModel absolute ATargetModel;
begin
  Assert(ATargetModel is TdxDocumentModel);
  ATableStyles := ATargetDocumentModel.TableStyles;
  for I := 0 to ATableStyles.Count - 1 do
    if StyleName = ATableStyles[I].StyleName then
      Exit(I);
  Result := ATargetDocumentModel.TableStyles.AddNewStyle(CopyTo(ATargetDocumentModel));
end;

procedure TdxTableStyle.CopyProperties(ASource: TdxStyleBase);
var
  AInnerSource: TdxTableStyle absolute ASource;
begin
  Assert(ASource is TdxTableStyle);
  inherited CopyProperties(ASource);
  TdxTableConditionalFormattingController.ResetTablesCachedProperties(DocumentModel); 
  TableProperties.CopyFrom(AInnerSource.TableProperties);
  TableRowProperties.CopyFrom(AInnerSource.TableRowProperties);
  TableCellProperties.CopyFrom(AInnerSource.TableCellProperties);
  CharacterProperties.CopyFrom(AInnerSource.CharacterProperties.Info);
end;

function TdxTableStyle.CopyTo(ATargetModel: TdxCustomDocumentModel): TdxTableStyle;
var
  AStyle: TdxTableStyle;
  AModel: TdxDocumentModel absolute ATargetModel;
begin
  AStyle := TdxTableStyle.Create(ATargetModel);
  AStyle.StyleName := StyleName;
  AStyle.CopyProperties(Self);
  if HasConditionalStyleProperties then
    AStyle.ConditionalStyleProperties.CopyFrom(ConditionalStyleProperties);
  if Parent <> nil then
    AStyle.Parent := AModel.TableStyles[Parent.Copy(ATargetModel)];
  ApplyPropertiesDiff(AStyle);
  Result := AStyle;
end;

function TdxTableStyle.CreateCellPropertiesChangedHistoryItem(
  AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Assert(AProperties = TableCellProperties);
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(AProperties.PieceTable as TdxPieceTable, AProperties);
end;

function TdxTableStyle.CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable, CharacterProperties);
end;

function TdxTableStyle.GetConditionalPropertiesSource(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType; AMask: Integer; AIsBorderCell: Boolean;
  out AInsideBorder: Boolean): TdxTableConditionalStyle;
begin
  Result := NotImplemented
end;

function TdxTableStyle.GetCharacterProperties(AMask: TdxUsedCharacterFormattingOption; ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxCharacterProperties;
var
  AConditionalStyle: TdxTableConditionalStyle;
begin
  if Deleted then
    raise Exception.Create(''); 
  AConditionalStyle := GetConditionalPropertiesSource(ARowType, AColumnType, function(AStyle: TdxTableConditionalStyle): Boolean
    begin
      Result := AStyle.CharacterProperties.UseVal(AMask);
    end);
  if AConditionalStyle <> nil then
    Exit(AConditionalStyle.CharacterProperties);
  if (FCharacterProperties <> nil) and FCharacterProperties.UseVal(AMask) then
    Exit(FCharacterProperties);
  if Parent <> nil then
    Result := Parent.GetCharacterProperties(AMask, ARowType, AColumnType)
  else
    Result := nil;
end;

function TdxTableStyle.GetCharacterProperties: TdxCharacterProperties;
begin
  Result := FCharacterProperties;
end;

function TdxTableStyle.GetConditionalPropertiesMask(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxConditionalTableStyleFormattingTypes;
var  
  AResult: Integer;
  ACondStyle: TdxTableConditionalStyle;
  AConditionalStyles: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
begin
  AResult := 0;
  if FConditionalStyleProperties = nil then
    Exit(TdxConditionalTableStyleFormattingTypes(AResult));
  AConditionalStyles := FConditionalStyleProperties.Items;
  if AColumnType = TdxConditionalColumnType.FirstColumn then
  begin
    if ARowType = TdxConditionalRowType.FirstRow then
    begin
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.TopLeftCell, ACondStyle) and (ACondStyle <> nil) then
        AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.TopLeftCell);
    end
    else
      if ARowType = TdxConditionalRowType.LastRow then
        if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.BottomLeftCell, ACondStyle) and (ACondStyle <> nil) then
          AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.BottomLeftCell);
  end
  else
    if AColumnType = TdxConditionalColumnType.LastColumn then
    begin
      if ARowType = TdxConditionalRowType.FirstRow then
      begin
        if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.TopRightCell, ACondStyle) and (ACondStyle <> nil) then
          AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.TopRightCell);
      end
      else
        if ARowType = TdxConditionalRowType.LastRow then
          if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.BottomRightCell, ACondStyle) and (ACondStyle <> nil) then
            AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.BottomRightCell);
    end;
  if ARowType = TdxConditionalRowType.FirstRow then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.FirstRow, ACondStyle) and (ACondStyle <> nil) then
      AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.FirstRow);
  end
  else
    if ARowType = TdxConditionalRowType.LastRow then
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.LastRow, ACondStyle) and (ACondStyle <> nil) then
        AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.LastRow);
  if AColumnType = TdxConditionalColumnType.FirstColumn then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.FirstColumn, ACondStyle) and (ACondStyle <> nil) then
      AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.FirstColumn);
  end
  else
    if AColumnType = TdxConditionalColumnType.LastColumn then
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.LastColumn, ACondStyle) and (ACondStyle <> nil) then
        AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.LastColumn);
  if AColumnType = TdxConditionalColumnType.EvenColumnBand then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.EvenColumnBanding, ACondStyle) and (ACondStyle <> nil) then
      AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.EvenColumnBanding);
  end
  else
    if AColumnType = TdxConditionalColumnType.OddColumnBand then
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.OddColumnBanding, ACondStyle) and (ACondStyle <> nil) then
        AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.OddColumnBanding);
  if ARowType = TdxConditionalRowType.EvenRowBand then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.EvenRowBanding, ACondStyle) and (ACondStyle <> nil) then
      AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.EvenRowBanding);
  end
  else
    if ARowType = TdxConditionalRowType.OddRowBand then
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.OddRowBanding, ACondStyle) and (ACondStyle <> nil) then
        AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.OddRowBanding);
  if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.WholeTable, ACondStyle) and (ACondStyle <> nil) then
    AResult := AResult or Ord(TdxConditionalTableStyleFormattingTypes.WholeTable);
  Result := TdxConditionalTableStyleFormattingTypes(AResult);
end;

function TdxTableStyle.GetConditionalPropertiesSource(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType; AMaskPredicate: TdxMaskPrediacate): TdxTableConditionalStyle;
var
  AConditionalStyles: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
begin
  Result := nil;
  if FConditionalStyleProperties = nil then
    Exit;
  AConditionalStyles := FConditionalStyleProperties.Items;
  if AColumnType = TdxConditionalColumnType.FirstColumn then
  begin
    if ARowType = TdxConditionalRowType.FirstRow then
    begin
      TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.TopLeftCell, Result, AMaskPredicate);
      if Result <> nil then
        Exit;
    end
    else
      if ARowType = TdxConditionalRowType.LastRow then
      begin
          TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.BottomLeftCell, Result, AMaskPredicate);
          if Result <> nil then
            Exit;
      end;
  end
  else
    if AColumnType = TdxConditionalColumnType.LastColumn then
    begin
      if ARowType = TdxConditionalRowType.FirstRow then
      begin
        TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.TopRightCell, Result, AMaskPredicate);
        if Result <> nil then
          Exit;
      end
      else
        if ARowType = TdxConditionalRowType.LastRow then
        begin
          TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.BottomRightCell, Result, AMaskPredicate);
          if Result <> nil then
            Exit;
        end;
    end;
  if ARowType = TdxConditionalRowType.FirstRow then
  begin
    TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.FirstRow, Result, AMaskPredicate);
    if Result <> nil then
      Exit;
  end
  else
    if ARowType = TdxConditionalRowType.LastRow then
    begin
      TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.LastRow, Result, AMaskPredicate);
      if Result <> nil then
        Exit;
    end;
  if AColumnType = TdxConditionalColumnType.FirstColumn then
  begin
    TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.FirstColumn, Result, AMaskPredicate);
    if Result <> nil then
      Exit;
  end
  else
    if AColumnType = TdxConditionalColumnType.LastColumn then
    begin
      TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.LastColumn, Result, AMaskPredicate);
      if Result <> nil then
        Exit;
    end;
  if AColumnType = TdxConditionalColumnType.EvenColumnBand then
  begin
    TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.EvenColumnBanding, Result, AMaskPredicate);
    if Result <> nil then
      Exit;
  end
  else
    if AColumnType = TdxConditionalColumnType.OddColumnBand then
    begin
      TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.OddColumnBanding, Result, AMaskPredicate);
      if Result <> nil then
        Exit;
    end;
  if ARowType = TdxConditionalRowType.EvenRowBand then
  begin
    TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.EvenRowBanding, Result, AMaskPredicate);
    if Result <> nil then
      Exit;
  end
  else
    if ARowType = TdxConditionalRowType.OddRowBand then
    begin
      TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.OddRowBanding, Result, AMaskPredicate);
      if Result <> nil then
        Exit;
    end;
  TryGetConditionalStyle(AConditionalStyles, TdxConditionalTableStyleFormattingTypes.WholeTable, Result, AMaskPredicate);
end;

function TdxTableStyle.GetConditionalStyleProperties: TdxTableConditionalStyleProperties;
begin
  if FConditionalStyleProperties = nil then
    FConditionalStyleProperties := TdxTableConditionalStyleProperties.Create(Self);
  Result := FConditionalStyleProperties;
end;

function TdxTableStyle.GetHasColumnBandingStyleProperties: Boolean;
begin
  Result := (FConditionalStyleProperties <> nil) and
    (FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.OddColumnBanding) or
      FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.EvenColumnBanding));
end;

function TdxTableStyle.GetHasConditionalStyleProperties: Boolean;
begin
  Result := (FConditionalStyleProperties <> nil) and FConditionalStyleProperties.HasNonNullStyle;
end;

function TdxTableStyle.GetHasRowBandingStyleProperties: Boolean;
begin
  Result := (FConditionalStyleProperties <> nil) and
    (FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.OddRowBanding) or
      FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.EvenRowBanding));
end;

function TdxTableStyle.GetMergedCharacterProperties: TdxMergedCharacterProperties;
begin
  Result := GetMergedCharacterProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal);
end;

function TdxTableStyle.GetMergedCharacterProperties(
  AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedCharacterProperties;
begin
  if (FConditionalStyleProperties <> nil) and FConditionalStyleProperties.ContainsStyle(AConditionType) then
    Exit(FConditionalStyleProperties.Items[AConditionType].GetMergedCharacterProperties);
  if Parent <> nil then
    Result := Parent.GetMergedCharacterProperties(AConditionType)
  else
    Result := TdxMergedCharacterProperties.Create(TdxCharacterFormattingInfo.Create, TdxCharacterFormattingOptions.Create);
end;

function TdxTableStyle.GetMergedParagraphProperties(
  AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedParagraphProperties;
begin
  if (FConditionalStyleProperties <> nil) and FConditionalStyleProperties.ContainsStyle(AConditionType) then
    Exit(FConditionalStyleProperties.Items[AConditionType].GetMergedParagraphProperties);
  if Parent <> nil then
    Result := Parent.GetMergedParagraphProperties(AConditionType)
  else
    Result := TdxMergedParagraphProperties.Create(TdxParagraphFormattingInfo.Create, TdxParagraphFormattingOptions.Create);
end;

function TdxTableStyle.GetMergedParagraphProperties: TdxMergedParagraphProperties;
begin
  Result := GetMergedParagraphProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal);
end;

function TdxTableStyle.GetMergedParagraphProperties(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxParagraphPropertiesMerger.Create(TdxMergedParagraphProperties.Create(TdxParagraphFormattingInfo.Create, TdxParagraphFormattingOptions.Create));
  try
    GetConditionalPropertiesSource(ARowType, AColumnType, function(AStyle: TdxTableConditionalStyle): Boolean
      begin
        AMerger.Merge(AStyle.ParagraphProperties);
        Result := False;
      end);
    if ParagraphProperties <> nil then
      AMerger.Merge(ParagraphProperties);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedParagraphProperties(ARowType, AColumnType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetMergedTableCellProperties: TdxMergedTableCellProperties;
begin
  Result := GetMergedTableCellProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal);
end;

function TdxTableStyle.GetMergedTableCellProperties(ARowType: TdxConditionalRowType;
  AColType: TdxConditionalColumnType): TdxMergedTableCellProperties;
var
  AMerger: TdxTableCellPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTableCellPropertiesMerger.Create(TableCellProperties);
  try
    GetConditionalPropertiesSource(ARowType, AColType, function(AStyle: TdxTableConditionalStyle): Boolean
      begin
        AMerger.Merge(AStyle.TableCellProperties);
        Result := False;
      end);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedTableCellProperties(ARowType, AColType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetMergedTableProperties: TdxMergedTableProperties;
var
  AMerger: TdxTablePropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTablePropertiesMerger.Create(TableProperties);
  try
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedTableProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetMergedTableRowProperties(
  AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableRowProperties;
begin
  if (FConditionalStyleProperties <> nil) and FConditionalStyleProperties.ContainsStyle(AConditionType) then
    Exit(FConditionalStyleProperties.Items[AConditionType].GetMergedTableRowProperties);
  if Parent <> nil then
    Result := Parent.GetMergedTableRowProperties(AConditionType)
  else
    Result := TdxMergedTableRowProperties.Create(TdxCombinedTableRowPropertiesInfo.Create, TdxTableRowPropertiesOptions.Create);
end;

function TdxTableStyle.GetMergedTableRowProperties(ARowType: TdxConditionalRowType): TdxMergedTableRowProperties;
var
  AMerger: TdxTableRowPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTableRowPropertiesMerger.Create(TableRowProperties);
  try
    GetConditionalPropertiesSource(ARowType, TdxConditionalColumnType.Normal, function(AStyle: TdxTableConditionalStyle): Boolean
      begin
        AMerger.Merge(AStyle.TableRowProperties);
        Result := False;
      end);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedTableRowProperties(ARowType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetMergedTableRowProperties: TdxMergedTableRowProperties;
begin
  Result := GetMergedTableRowProperties(TdxConditionalRowType.Normal);
end;

function TdxTableStyle.GetMergedWithDefaultCharacterProperties(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxCharacterPropertiesMerger.Create(TdxMergedCharacterProperties.Create(TdxCharacterFormattingInfo.Create, TdxCharacterFormattingOptions.Create));
  try
    MergeConditionalProperties(AMerger, ARowType, AColumnType);
    AMerger.Merge(CharacterProperties);
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetMergedWithDefaultTableCellProperties: TdxMergedTableCellProperties;
var
  AMerger: TdxTableCellPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTableCellPropertiesMerger.Create(GetMergedTableCellProperties);
  try
    AMerger.Merge(DocumentModel.DefaultTableCellProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetMergedWithDefaultTableProperties: TdxMergedTableProperties;
var
  AMerger: TdxTablePropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTablePropertiesMerger.Create(GetMergedTableProperties);
  try
    AMerger.Merge(DocumentModel.DefaultTableProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetMergedWithDefaultTableRowProperties: TdxMergedTableRowProperties;
var
  AMerger: TdxTableRowPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTableRowPropertiesMerger.Create(GetMergedTableRowProperties);
  try
    AMerger.Merge(DocumentModel.DefaultTableRowProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetParagraphProperties(AMask: TdxUsedParagraphFormattingOption; ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxParagraphProperties;
var
  AConditionalStyle: TdxTableConditionalStyle;
begin
  if Deleted then
    raise Exception.Create(''); 
  AConditionalStyle := GetConditionalPropertiesSource(ARowType, AColumnType, function(AStyle: TdxTableConditionalStyle): Boolean
    begin
      Result := AStyle.ParagraphProperties.UseVal(AMask);
    end);
  if AConditionalStyle <> nil then
    Exit(AConditionalStyle.ParagraphProperties);
  if (ParagraphProperties <> nil) and ParagraphProperties.UseVal(AMask) then
    Exit(ParagraphProperties);
  if Parent <> nil then
    Result := Parent.GetParagraphProperties(AMask, ARowType, AColumnType)
  else
    Result := nil;
end;

function TdxTableStyle.GetParent: TdxTableStyle;
begin
  Result := TdxTableStyle(inherited Parent);
end;

function TdxTableStyle.GetMergedCharacterProperties(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxCharacterPropertiesMerger.Create(TdxMergedCharacterProperties.Create(TdxCharacterFormattingInfo.Create, TdxCharacterFormattingOptions.Create));
  try
    MergeConditionalProperties(AMerger, ARowType, AColumnType);
    AMerger.Merge(CharacterProperties);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedCharacterProperties(ARowType, AColumnType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableStyle.GetTableCellProperties: TdxTableCellProperties;
begin
  if FTableCellProperties = nil then
  begin
    FTableCellProperties := TdxTableCellProperties.Create(DocumentModel.MainPieceTable, Self);
    SubscribeTableCellPropertiesEvents;
  end;
  Result := FTableCellProperties;
end;

function TdxTableStyle.GetTableProperties(AMask: Integer; ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxTableProperties;
var
  AInsideBorder: Boolean;
begin
  Result := GetTableProperties(AMask, ARowType, AColumnType, True, AInsideBorder);
end;

function TdxTableStyle.GetTableProperties: TdxTableProperties;
begin
  if FTableProperties = nil then
  begin
    FTableProperties := TdxTableProperties.Create(DocumentModel.MainPieceTable);
    SubscribeTablePropertiesPropertiesEvents;
  end;
  Result := FTableProperties;
end;

function TdxTableStyle.GetTableRowProperties: TdxTableRowProperties;
begin
  if FTableRowProperties = nil then
  begin
    FTableRowProperties := TdxTableRowProperties.Create(DocumentModel.MainPieceTable);
    SubscribeTableRowPropertiesEvents;
  end;
  Result := FTableRowProperties;
end;

procedure TdxTableStyle.MergeConditionalProperties(AMerger: TdxCharacterPropertiesMerger;
  ARowType: TdxConditionalRowType; AColumnType: TdxConditionalColumnType);
var
  AConditionalStyle: TdxTableConditionalStyle;
  AConditionalStyles: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
begin
  if FConditionalStyleProperties = nil then
    Exit;

  AConditionalStyles := FConditionalStyleProperties.Items;
  AConditionalStyle := nil;
  if AColumnType = TdxConditionalColumnType.FirstColumn then
  begin
    if ARowType = TdxConditionalRowType.FirstRow then
    begin
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.TopLeftCell, AConditionalStyle) and (AConditionalStyle <> nil) then
        AMerger.Merge(AConditionalStyle.CharacterProperties);
    end
    else
      if ARowType = TdxConditionalRowType.LastRow then
        if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.BottomLeftCell, AConditionalStyle) and (AConditionalStyle <> nil) then
          AMerger.Merge(AConditionalStyle.CharacterProperties);
  end
  else
    if AColumnType = TdxConditionalColumnType.LastColumn then
    begin
      if ARowType = TdxConditionalRowType.FirstRow then
      begin
        if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.TopRightCell, AConditionalStyle) and (AConditionalStyle <> nil) then
          AMerger.Merge(AConditionalStyle.CharacterProperties);
      end
      else
        if ARowType = TdxConditionalRowType.LastRow then
          if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.BottomRightCell, AConditionalStyle) and (AConditionalStyle <> nil) then
            AMerger.Merge(AConditionalStyle.CharacterProperties);
    end;
  if ARowType = TdxConditionalRowType.FirstRow then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.FirstRow, AConditionalStyle) and (AConditionalStyle <> nil) then
      AMerger.Merge(AConditionalStyle.CharacterProperties);
  end
  else
    if ARowType = TdxConditionalRowType.LastRow then
    begin
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.LastRow, AConditionalStyle) and (AConditionalStyle <> nil) then
        AMerger.Merge(AConditionalStyle.CharacterProperties);
    end;
  if AColumnType = TdxConditionalColumnType.FirstColumn then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.FirstColumn, AConditionalStyle) and (AConditionalStyle <> nil) then
      AMerger.Merge(AConditionalStyle.CharacterProperties);
  end
  else
    if AColumnType = TdxConditionalColumnType.LastColumn then
    begin
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.LastColumn, AConditionalStyle) and (AConditionalStyle <> nil) then
        AMerger.Merge(AConditionalStyle.CharacterProperties);
    end;
  if AColumnType = TdxConditionalColumnType.EvenColumnBand then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.EvenColumnBanding, AConditionalStyle) and (AConditionalStyle <> nil) then
      AMerger.Merge(AConditionalStyle.CharacterProperties);
  end
  else
    if AColumnType = TdxConditionalColumnType.OddColumnBand then
    begin
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.OddColumnBanding, AConditionalStyle) and (AConditionalStyle <> nil) then
        AMerger.Merge(AConditionalStyle.CharacterProperties);
    end;
  if ARowType = TdxConditionalRowType.EvenRowBand then
  begin
    if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.EvenRowBanding, AConditionalStyle) and (AConditionalStyle <> nil) then
      AMerger.Merge(AConditionalStyle.CharacterProperties);
  end
  else
    if ARowType = TdxConditionalRowType.OddRowBand then
    begin
      if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.OddRowBanding, AConditionalStyle) and (AConditionalStyle <> nil) then
        AMerger.Merge(AConditionalStyle.CharacterProperties);
    end;
  if AConditionalStyles.TryGetValue(TdxConditionalTableStyleFormattingTypes.WholeTable, AConditionalStyle) and (AConditionalStyle <> nil) then
    AMerger.Merge(AConditionalStyle.CharacterProperties);
end;

procedure TdxTableStyle.MergePropertiesWithParent;
begin
  inherited MergePropertiesWithParent;
  TableProperties.Merge(Parent.TableProperties);
  TableRowProperties.Merge(Parent.TableRowProperties);
  TableCellProperties.Merge(Parent.TableCellProperties);
  CharacterProperties.Merge(Parent.CharacterProperties);
end;

procedure TdxTableStyle.OnCharacterPropertiesChanged;
begin
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.Character);
end;

procedure TdxTableStyle.OnObtainAffectedRange(ASender: TObject; E: TdxObtainAffectedRangeEventArgs);
begin
  E.Start := 0;
  E.&End := MaxInt; 
end;

procedure TdxTableStyle.ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType);
begin
end;

procedure TdxTableStyle.SetParent(const Value: TdxTableStyle);
begin
  inherited Parent := Value;
end;

procedure TdxTableStyle.SubscribeCharacterPropertiesEvents;
begin
  CharacterProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

procedure TdxTableStyle.SubscribeTableCellPropertiesEvents;
begin
  FTableCellProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

procedure TdxTableStyle.SubscribeTablePropertiesPropertiesEvents;
begin
  FTableProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

procedure TdxTableStyle.SubscribeTableRowPropertiesEvents;
begin
  FTableRowProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

procedure TdxTableStyle.TryGetConditionalStyle(
  AConditionalStyles: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
  AStyleType: TdxConditionalTableStyleFormattingTypes; out AResult: TdxTableConditionalStyle; AMask: Integer);
begin
  TryGetConditionalStyle(AConditionalStyles, AStyleType, AResult, function(AStyle: TdxTableConditionalStyle): Boolean
    begin
      Result := AStyle.TableProperties.GetUse(AMask);
    end);
end;

procedure TdxTableStyle.TryGetConditionalStyle(
  AConditionalStyles: TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>;
  AStyleType: TdxConditionalTableStyleFormattingTypes; out AResult: TdxTableConditionalStyle;
  AMaskPredicate: TdxMaskPrediacate);
begin
  AConditionalStyles.TryGetValue(AStyleType, AResult);
  if ((AResult = nil) or not AMaskPredicate(AResult)) and (Parent <> nil) then
    Parent.TryGetConditionalStyle(Parent.ConditionalStyleProperties.Items, AStyleType, AResult, AMaskPredicate);
end;

function TdxTableStyle.GetTableCellProperties(AMask: Integer; ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxTableCellProperties;
var
  AConditionalStyle: TdxTableConditionalStyle;
begin
  if Deleted then
    raise Exception.Create(''); 
  AConditionalStyle := GetConditionalPropertiesSource(ARowType, AColumnType, function(AStyle: TdxTableConditionalStyle): Boolean
    begin
      Result := AStyle.TableCellProperties.GetUse(AMask);
    end);
  if AConditionalStyle <> nil then
    Exit(AConditionalStyle.TableCellProperties);
  if (FTableCellProperties <> nil) and FTableCellProperties.GetUse(AMask) then
    Exit(FTableCellProperties);
  if Parent <> nil then
    Result := Parent.GetTableCellProperties(AMask, ARowType, AColumnType)
  else
    Result := nil;
end;

function TdxTableStyle.GetTableProperties(AMask: Integer; ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType; AIsBorderCell: Boolean; out AInsideBorder: Boolean): TdxTableProperties;
var
  AConditionalStyle: TdxTableConditionalStyle;
begin
  if Deleted then
    raise Exception.Create(''); 

  AConditionalStyle := GetConditionalPropertiesSource(ARowType, AColumnType, AMask, AIsBorderCell, AInsideBorder);
  if AConditionalStyle <> nil then
    Exit(AConditionalStyle.TableProperties);

  AInsideBorder := not AIsBorderCell;

  if (FTableProperties <> nil) and FTableProperties.GetUse(TdxTable.OuterOrInside(AMask, AIsBorderCell)) then
    Exit(TableProperties);
  if Parent <> nil then
    Result := Parent.GetTableProperties(AMask, ARowType, AColumnType, AIsBorderCell, AInsideBorder)
  else
    Result := nil;
end;

function TdxTableStyle.GetTableRowProperties(AMask: Integer; ARowType: TdxConditionalRowType): TdxTableRowProperties;
var
  AConditionalStyle: TdxTableConditionalStyle;
begin
  if Deleted then
    raise Exception.Create(''); 
  AConditionalStyle := GetConditionalPropertiesSource(ARowType, TdxConditionalColumnType.Normal, function(AStyle: TdxTableConditionalStyle): Boolean
    begin
      Result := AStyle.TableRowProperties.GetUse(AMask);
    end);
  if AConditionalStyle <> nil then
    Exit(AConditionalStyle.TableRowProperties);
  if (FTableRowProperties <> nil) and FTableRowProperties.GetUse(AMask) then
    Exit(FTableRowProperties);
  if Parent <> nil then
    Result := Parent.GetTableRowProperties(AMask, ARowType)
  else
    Result := nil;
end;

function TdxTableStyle.GetType: TdxStyleType;
begin
  Result := TdxStyleType.TableStyle;
end;

function TdxTableStyle.HasColumnStyle(AConditionalColumnType: TdxConditionalColumnType): Boolean;
begin
  if FConditionalStyleProperties <> nil then
  begin
    if AConditionalColumnType = TdxConditionalColumnType.FirstColumn then
      Exit(FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.FirstColumn) or
        FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.TopLeftCell) or
        FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.BottomLeftCell))
    else
      if AConditionalColumnType = TdxConditionalColumnType.LastColumn then
        Exit(FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.LastColumn) or
          FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.TopRightCell) or
          FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.BottomRightCell));
  end;
  if Parent <> nil then
    Result := Parent.HasColumnStyle(AConditionalColumnType)
  else
    Result := False;
end;

function TdxTableStyle.HasRowStyle(AConditionalRowType: TdxConditionalRowType): Boolean;
begin
  if FConditionalStyleProperties <> nil then
  begin
    if AConditionalRowType = TdxConditionalRowType.FirstRow then
      Exit(FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.FirstRow) or
        FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.TopLeftCell) or
        FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.TopRightCell))
    else
      if AConditionalRowType = TdxConditionalRowType.LastRow then
        Exit(FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.LastRow) or
          FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.BottomLeftCell) or
          FConditionalStyleProperties.ContainsStyle(TdxConditionalTableStyleFormattingTypes.BottomRightCell));
  end;
  if Parent <> nil then
    Result := Parent.HasRowStyle(AConditionalRowType)
  else
    Result := False;
end;

function TdxTableStyle.GetMergedTableCellProperties(
  AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableCellProperties;
begin
  if (FConditionalStyleProperties <> nil) and FConditionalStyleProperties.ContainsStyle(AConditionType) then
    Exit(FConditionalStyleProperties.Items[AConditionType].GetMergedTableCellProperties);
  if Parent <> nil then
    Result := Parent.GetMergedTableCellProperties(AConditionType)
  else
    Result := TdxMergedTableCellProperties.Create(TdxCombinedCellPropertiesInfo.Create, TdxTableCellPropertiesOptions.Create);
end;

{ TdxTableStyleCollection }

constructor TdxTableStyleCollection.Create(ADocumentModel: TdxCustomDocumentModel; AChangeDefaultTableStyle: Boolean);
begin
  inherited Create(ADocumentModel);
  Items.Add(CreateNormalTableStyle(AChangeDefaultTableStyle));
  Items.Add(CreateTableSimpleStyle);
end;

function TdxTableStyleCollection.CreateDefaultItem: TdxStyleBase;
begin
  Result := nil;
end;

function TdxTableStyleCollection.CreateNormalTableStyle(AChangeDefaultTableStyle: Boolean): TdxTableStyle;
var
  AStyle: TdxTableStyle;
  AOptions: TdxTablePropertiesOptions;
  AIsDeferred: Boolean;
begin
  AStyle := TdxTableStyle.Create(DocumentModel, nil, DefaultTableStyleName);
  if AChangeDefaultTableStyle then
  begin
    AStyle.TableProperties.BeginInit;
    try
      SetDefaultMargin(AStyle.TableProperties.CellMargins.Left, FDefaultLeftMargin);
      SetDefaultMargin(AStyle.TableProperties.CellMargins.Top, 0);
      SetDefaultMargin(AStyle.TableProperties.CellMargins.Right, FDefaultRightMargin);
      SetDefaultMargin(AStyle.TableProperties.CellMargins.Bottom, 0);

      AOptions := TdxTablePropertiesAccess(AStyle.TableProperties).GetInfoForModification(AIsDeferred);
      AOptions.UseLeftMargin := True;
      AOptions.UseTopMargin := True;
      AOptions.UseRightMargin := True;
      AOptions.UseBottomMargin := True;
      AStyle.TableProperties.ReplaceInfo(AOptions, []);
      if not AIsDeferred then
        AOptions.Free;
    finally
      AStyle.TableProperties.EndInit;
    end;
  end;
  Result := AStyle;
end;

function TdxTableStyleCollection.CreateTableSimpleStyle: TdxTableStyle;
var
  AIsDeferred: Boolean;
  AStyle: TdxTableStyle;
  AOptions: TdxTablePropertiesOptions;
begin
  AStyle := TdxTableStyle.Create(DocumentModel, Self[DefaultTableStyleIndex], TableSimpleStyleName);
  AStyle.TableProperties.BeginInit;
  try
    SetDefaultMargin(AStyle.TableProperties.CellMargins.Left, FDefaultLeftMargin);
    SetDefaultMargin(AStyle.TableProperties.CellMargins.Right, FDefaultRightMargin);
    SetDefaultBorder(AStyle.TableProperties.Borders.LeftBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableProperties.Borders.RightBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableProperties.Borders.TopBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableProperties.Borders.BottomBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableProperties.Borders.InsideHorizontalBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableProperties.Borders.InsideVerticalBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);

    AOptions := TdxTablePropertiesAccess(AStyle.TableProperties).GetInfoForModification(AIsDeferred);
    AOptions.UseLeftMargin := True;
    AOptions.UseRightMargin := True;
    AOptions.UseTopMargin := True;
    AOptions.UseBottomMargin := True;
    AOptions.UseLeftBorder := True;
    AOptions.UseRightBorder := True;
    AOptions.UseTopBorder := True;
    AOptions.UseBottomBorder := True;
    AOptions.UseInsideHorizontalBorder := True;
    AOptions.UseInsideVerticalBorder := True;
    AStyle.TableProperties.ReplaceInfo(AOptions, []);
    if not AIsDeferred then
      AOptions.Free;
  finally
    AStyle.TableProperties.EndInit;
  end;
  Result := AStyle;
end;

function TdxTableStyleCollection.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := TdxDocumentModel(inherited DocumentModel);
end;

function TdxTableStyleCollection.GetItem(Index: Integer): TdxTableStyle;
begin
  Result := TdxTableStyle(inherited Items[Index])
end;

function TdxTableStyleCollection.GetPieceTable: TObject;
begin
  Result := TdxPieceTable(inherited PieceTable);
end;

procedure TdxTableStyleCollection.NotifyPieceTableStyleDeleting(APieceTable: TObject; AStyle: TdxStyleBase);
var
  I: Integer;
  ATables: TdxTableCollection;
  ATableStyle: TdxTableStyle absolute AStyle;
begin
  Assert(APieceTable is TdxPieceTable);
  ATables := TdxPieceTable(APieceTable).Tables;
  for I := 0 to ATables.Count - 1 do
    if ATables[I].TableStyle = ATableStyle then
      ATables[I].StyleIndex := DefaultItemIndex;
end;

procedure TdxTableStyleCollection.SetDefaultBorder(ABorder: TdxBorderBase; AStyle: TdxBorderLineStyle;
  AWidthInTwips: Integer);
begin
  ABorder.BeginInit;
  try
    ABorder.Style := TdxBorderLineStyle.Single;
    ABorder.Width := DocumentModel.UnitConverter.TwipsToModelUnits(AWidthInTwips);
  finally
    ABorder.EndInit;
  end;
end;

procedure TdxTableStyleCollection.SetDefaultMargin(AMargin: TdxWidthUnit; AValueInTwips: Integer);
begin
  AMargin.BeginInit;
  try
    AMargin.&Type := TdxWidthUnitType.ModelUnits;
    AMargin.Value := DocumentModel.UnitConverter.TwipsToModelUnits(AValueInTwips);
  finally
    AMargin.EndInit;
  end;
end;

{ TdxTableConditionalStyleProperties }

procedure TdxTableConditionalStyleProperties.AddConditionalStyle(ADocumentModel: TdxCustomDocumentModel;
  ACondition: TdxConditionalTableStyleFormattingTypes);
begin
  Items.Add(ACondition, nil);
end;

procedure TdxTableConditionalStyleProperties.AddStyle(AStyle: TdxTableConditionalStyle);
var
  I: Integer;
  AIsColumnBinding, AIsRowBinding: Boolean;
  AOldStyle: TdxTableConditionalStyle;
  AOwnerTableProperties: TdxTableProperties;
  AStyleType: TdxConditionalTableStyleFormattingTypes;
  ATables: TdxTableCollection;
  AController: TdxTableConditionalFormattingController;
begin
  AStyleType := AStyle.ConditionType;
  AOldStyle := Items[AStyleType];
  if Items[AStyleType] <> nil then
  begin
    AOldStyle.CharacterProperties.OnObtainAffectedRange.Remove(FOwner.OnObtainAffectedRange);
    AOldStyle.ParagraphProperties.OnObtainAffectedRange.Remove(FOwner.OnObtainAffectedRange);
    AOldStyle.TableCellProperties.OnObtainAffectedRange.Remove(FOwner.OnObtainAffectedRange);
    AOldStyle.TableProperties.OnObtainAffectedRange.Remove(FOwner.OnObtainAffectedRange);
    AOldStyle.TableRowProperties.OnObtainAffectedRange.Remove(FOwner.OnObtainAffectedRange);
  end;
  AStyle.CharacterProperties.OnObtainAffectedRange.Add(FOwner.OnObtainAffectedRange);
  AStyle.ParagraphProperties.OnObtainAffectedRange.Add(FOwner.OnObtainAffectedRange);
  AStyle.TableCellProperties.OnObtainAffectedRange.Add(FOwner.OnObtainAffectedRange);
  AStyle.TableProperties.OnObtainAffectedRange.Add(FOwner.OnObtainAffectedRange);
  AStyle.TableRowProperties.OnObtainAffectedRange.Add(FOwner.OnObtainAffectedRange);
  Items[AStyleType] := AStyle;
  AOwnerTableProperties := Owner.TableProperties;
  AIsColumnBinding := (AStyleType = TdxConditionalTableStyleFormattingTypes.EvenColumnBanding) or (AStyleType = TdxConditionalTableStyleFormattingTypes.OddColumnBanding);
  if AIsColumnBinding and not AOwnerTableProperties.UseTableStyleColBandSize then
    AOwnerTableProperties.TableStyleColBandSize := 1;
  AIsRowBinding := (AStyleType = TdxConditionalTableStyleFormattingTypes.EvenRowBanding) or (AStyleType = TdxConditionalTableStyleFormattingTypes.OddRowBanding);
  if AIsRowBinding and not AOwnerTableProperties.UseTableStyleRowBandSize then
    AOwnerTableProperties.TableStyleRowBandSize := 1;
  ATables := AStyle.DocumentModel.ActivePieceTable.Tables;
  for I := 0 to ATables.Count - 1 do
  begin
    AController := TdxTableConditionalFormattingController.Create(ATables[I]);
    try
      AController.ResetCachedProperties(0);
    finally
      AController.Free;
    end;
  end;
end;

function TdxTableConditionalStyleProperties.ContainsStyle(
  AConditionalTableStyleFormattingType: TdxConditionalTableStyleFormattingTypes): Boolean;
begin
  Result := Items[AConditionalTableStyleFormattingType] <> nil;
end;

procedure TdxTableConditionalStyleProperties.CopyConditionalStyle(ACondition: TdxConditionalTableStyleFormattingTypes;
  AConditionalProperties: TdxTableConditionalStyleProperties);
var
  ASourceStyle: TdxTableConditionalStyle;
  ATargetStyle: TdxTableConditionalStyle;
begin
  ASourceStyle := AConditionalProperties[ACondition];
  if ASourceStyle <> nil then
  begin
    ATargetStyle := GetStyleSafe(ACondition);
    ATargetStyle.CopyFrom(ASourceStyle);
  end
  else
    Items[ACondition] := nil;
end;

procedure TdxTableConditionalStyleProperties.CopyFrom(AConditionalProperties: TdxTableConditionalStyleProperties);
var
  ACondition: TdxConditionalTableStyleFormattingTypes;
begin
  for ACondition in StyleTypes do
    CopyConditionalStyle(ACondition, AConditionalProperties);
end;

constructor TdxTableConditionalStyleProperties.Create(AOwner: TdxTableStyle);
begin
  inherited Create;
  FItems := TDictionary<TdxConditionalTableStyleFormattingTypes, TdxTableConditionalStyle>.Create;
  FOwner := AOwner;
  InitializeItems(DocumentModel);
end;

function TdxTableConditionalStyleProperties.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := TdxDocumentModel(FOwner.DocumentModel);
end;

function TdxTableConditionalStyleProperties.GetItem(
  ACondition: TdxConditionalTableStyleFormattingTypes): TdxTableConditionalStyle;
begin
  Result := FItems[ACondition];
end;

function TdxTableConditionalStyleProperties.GetStyleSafe(
  ACondition: TdxConditionalTableStyleFormattingTypes): TdxTableConditionalStyle;
begin
  Result := Self[ACondition];
  if Result <> nil then
    Exit;

  Result := TdxTableConditionalStyle.Create(Owner, ACondition);
  Items[ACondition] := Result;
end;

function TdxTableConditionalStyleProperties.HasNonNullStyle: Boolean;
var
  ACondition: TdxConditionalTableStyleFormattingTypes;
begin
  for ACondition in StyleTypes do
    if Items[ACondition] <> nil then
      Exit(True);
  Result := False;
end;

class constructor TdxTableConditionalStyleProperties.Initialize;
var
  I: TdxConditionalTableStyleFormattingTypes;
  AIndex: Integer;
begin
  AIndex := 0;
  for I := Low(TdxConditionalTableStyleFormattingTypes) to High(TdxConditionalTableStyleFormattingTypes) do
  begin
    SetLength(StyleTypes, AIndex + 1);
    StyleTypes[AIndex] := I;
    Inc(AIndex);
  end;
end;

procedure TdxTableConditionalStyleProperties.InitializeItems(ADocumentModel: TdxCustomDocumentModel);
var
  ACondition: TdxConditionalTableStyleFormattingTypes;
begin
  for ACondition in StyleTypes do 
    AddConditionalStyle(ADocumentModel, ACondition);
end;

{ TdxTableConditionalStyle }

procedure TdxTableConditionalStyle.CopyFrom(ACondition: TdxTableConditionalStyle);
begin
  TableProperties.CopyFrom(ACondition.TableProperties);
  TableRowProperties.CopyFrom(ACondition.TableRowProperties);
  TableCellProperties.CopyFrom(ACondition.TableCellProperties);
  if DocumentModel = ACondition.DocumentModel then
  begin
    ParagraphProperties.CopyFrom(ACondition.ParagraphProperties);
    CharacterProperties.CopyFrom(ACondition.CharacterProperties);
  end
  else
  begin
    ParagraphProperties.CopyFrom(ACondition.ParagraphProperties.Info);
    CharacterProperties.CopyFrom(ACondition.CharacterProperties.Info);
  end;
end;

constructor TdxTableConditionalStyle.Create(AOwner: TdxTableStyle;
  AConditionType: TdxConditionalTableStyleFormattingTypes);
begin
  inherited Create;
  Assert(AOwner <> nil);
  FOwner := AOwner;
  FConditionType := AConditionType;
end;

destructor TdxTableConditionalStyle.Destroy;
begin
  FreeAndNil(FTableProperties);
  FreeAndNil(FTableRowProperties);
  FreeAndNil(FTableCellProperties);
  FreeAndNil(FParagraphProperties);
  FreeAndNil(FTabs);
  FreeAndNil(FCharacterProperties);
  inherited Destroy;
end;

function TdxTableConditionalStyle.CreateCellPropertiesChangedHistoryItem(
  AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Assert(AProperties = TableCellProperties);
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable, AProperties);
end;

function TdxTableConditionalStyle.CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable, CharacterProperties);
end;

function TdxTableConditionalStyle.CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable, ParagraphProperties);
end;

function TdxTableConditionalStyle.GetCharacterProperties: TdxCharacterProperties;
begin
  if FConditionType = TdxConditionalTableStyleFormattingTypes.WholeTable then
    Exit(FOwner.CharacterProperties);
  if FCharacterProperties = nil then
    FCharacterProperties := TdxCharacterProperties.Create(Self);
  Result := FCharacterProperties;
end;

function TdxTableConditionalStyle.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := TdxDocumentModel(FOwner.DocumentModel);
end;

function TdxTableConditionalStyle.GetParagraphProperties: TdxParagraphProperties;
begin
  if FConditionType = TdxConditionalTableStyleFormattingTypes.WholeTable then
    Exit(FOwner.ParagraphProperties);
  if FParagraphProperties = nil then
    FParagraphProperties := TdxParagraphProperties.Create(Self);
  Result := FParagraphProperties;
end;

function TdxTableConditionalStyle.GetParent: TdxTableStyle;
begin
  Result := FOwner.Parent;
end;

function TdxTableConditionalStyle.GetPieceTable: TObject;
begin
  Result := GetInnerPieceTable;
end;

function TdxTableConditionalStyle.GetInnerPieceTable: TObject;
begin
  Result := DocumentModel.MainPieceTable;
end;

function TdxTableConditionalStyle.GetMergedCharacterProperties: TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  if FOwner.Deleted then
    Assert(False); 
  AMerger := TdxCharacterPropertiesMerger.Create(TdxMergedCharacterProperties.Create(TdxCharacterFormattingInfo.Create, TdxCharacterFormattingOptions.Create));
  try
    AMerger.Merge(CharacterProperties);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedCharacterProperties(FConditionType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableConditionalStyle.GetMergedParagraphProperties: TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  if FOwner.Deleted then
    Assert(False); 
  AMerger := TdxParagraphPropertiesMerger.Create(TdxMergedParagraphProperties.Create(TdxParagraphFormattingInfo.Create, TdxParagraphFormattingOptions.Create));
  try
    AMerger.Merge(ParagraphProperties);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedParagraphProperties(FConditionType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableConditionalStyle.GetMergedTableCellProperties: TdxMergedTableCellProperties;
var
  AMerger: TdxTableCellPropertiesMerger;
begin
  if FOwner.Deleted then
    Assert(False); 
  AMerger := TdxTableCellPropertiesMerger.Create(TableCellProperties);
  try
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedTableCellProperties(FConditionType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableConditionalStyle.GetMergedTableProperties: TdxMergedTableProperties;
var
  AMerger: TdxTablePropertiesMerger;
begin
  if FOwner.Deleted then
    Assert(False); 
  AMerger := TdxTablePropertiesMerger.Create(TableProperties);
  try
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedTableProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableConditionalStyle.GetMergedTableRowProperties: TdxMergedTableRowProperties;
var
  AMerger: TdxTableRowPropertiesMerger;
begin
  if FOwner.Deleted then
    Assert(False); 
  AMerger := TdxTableRowPropertiesMerger.Create(TableRowProperties);
  try
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedTableRowProperties(FConditionType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableConditionalStyle.GetMergedWithDefaultCharacterProperties: TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  AMerger := TdxCharacterPropertiesMerger.Create(TdxMergedCharacterProperties.Create(TdxCharacterFormattingInfo.Create, TdxCharacterFormattingOptions.Create));
  try
    AMerger.Merge(CharacterProperties);
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableConditionalStyle.GetMergedWithDefaultParagraphProperties: TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  AMerger := TdxParagraphPropertiesMerger.Create(TdxMergedParagraphProperties.Create(TdxParagraphFormattingInfo.Create, TdxParagraphFormattingOptions.Create));
  try
    AMerger.Merge(ParagraphProperties);
    AMerger.Merge(DocumentModel.DefaultParagraphProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableConditionalStyle.GetTableCellProperties: TdxTableCellProperties;
begin
  if FConditionType = TdxConditionalTableStyleFormattingTypes.WholeTable then
    Exit(FOwner.TableCellProperties);
  if FTableCellProperties = nil then
    FTableCellProperties := TdxTableCellProperties.Create(DocumentModel.MainPieceTable, Self);
  Result := FTableCellProperties;
end;

function TdxTableConditionalStyle.GetTableCellProperties(AMask: Integer): TdxTableCellProperties;
begin
  if (FTableCellProperties <> nil) and FTableCellProperties.GetUse(AMask) then
    Result := TableCellProperties
  else
    Result := nil;
end;

function TdxTableConditionalStyle.GetTableProperties(AMask: Integer): TdxTableProperties;
begin
  if (FTableProperties <> nil) and FTableProperties.GetUse(AMask) then
    Result := TableProperties
  else
    Result := nil;
end;

function TdxTableConditionalStyle.GetTableRowProperties(AMask: Integer): TdxTableRowProperties;
begin
  if (FTableRowProperties <> nil) and FTableRowProperties.GetUse(AMask) then
    Result := TableRowProperties
  else
    Result := nil;
end;

function TdxTableConditionalStyle.GetTableProperties: TdxTableProperties;
begin
  if FConditionType = TdxConditionalTableStyleFormattingTypes.WholeTable then
    Exit(FOwner.TableProperties);
  if FTableProperties = nil then
    FTableProperties := TdxTableProperties.Create(DocumentModel.MainPieceTable);
  Result := FTableProperties;
end;

function TdxTableConditionalStyle.GetTableRowProperties: TdxTableRowProperties;
begin
  if FTableRowProperties = nil then
    FTableRowProperties := TdxTableRowProperties.Create(DocumentModel.MainPieceTable);
  Result := FTableRowProperties;
end;

function TdxTableConditionalStyle.GetTabs: TdxTabProperties;
begin
  if FTabs = nil then
    FTabs := TdxTabProperties.Create(Self);
  Result := FTabs;
end;

procedure TdxTableConditionalStyle.OnCharacterPropertiesChanged;
begin
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.Character);
end;

procedure TdxTableConditionalStyle.OnParagraphPropertiesChanged;
begin
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.Paragraph);
end;

{ TdxTableCellStyle }

constructor TdxTableCellStyle.Create(ADocumentModel: TdxCustomDocumentModel;
  AParent: TdxTableCellStyle = nil; const AStyleName: string = '');
begin
  inherited Create(ADocumentModel, AParent, AStyleName);
  FCharacterProperties := TdxCharacterProperties.Create(Self);
  SubscribeCharacterPropertiesEvents;
end;

destructor TdxTableCellStyle.Destroy;
begin
  FreeAndNil(FTableCellProperties);
  FreeAndNil(FCharacterProperties);
  inherited Destroy;
end;

procedure TdxTableCellStyle.ApplyPropertiesDiff(AStyle: TdxTableCellStyle);
begin
  ParagraphProperties.ApplyPropertiesDiff(AStyle.ParagraphProperties,
    AStyle.GetMergedWithDefaultParagraphProperties.Info, GetMergedWithDefaultParagraphProperties.Info);
  CharacterProperties.ApplyPropertiesDiff(AStyle.CharacterProperties,
    AStyle.GetMergedWithDefaultCharacterProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal).Info,
    GetMergedWithDefaultCharacterProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal).Info);
end;

function TdxTableCellStyle.Copy(ATargetModel: TdxCustomDocumentModel): Integer;
var
  I: Integer;
  ATableCellStyles: TdxTableCellStyleCollection;
  ATargetDocumentModel: TdxDocumentModel absolute ATargetModel;
begin
  ATableCellStyles := ATargetDocumentModel.TableCellStyles;
  for I := 0 to ATableCellStyles.Count do
    if StyleName = ATableCellStyles[I].StyleName then
      Exit(I);
  Result := ATargetDocumentModel.TableCellStyles.AddNewStyle(CopyTo(ATargetDocumentModel));
end;

procedure TdxTableCellStyle.CopyProperties(ASource: TdxStyleBase);
var
  ATableCellStyle: TdxTableCellStyle absolute ASource;
begin
  inherited CopyProperties(ATableCellStyle);
  TableCellProperties.CopyFrom(ATableCellStyle.TableCellProperties);
  CharacterProperties.CopyFrom(ATableCellStyle.CharacterProperties.Info);
end;

function TdxTableCellStyle.CopyTo(ATargetModel: TdxCustomDocumentModel): TdxTableCellStyle;
var
  AStyle: TdxTableCellStyle;
  AModel: TdxDocumentModel absolute ATargetModel;
begin
  AStyle := TdxTableCellStyle.Create(ATargetModel);
  AStyle.CopyProperties(Self);
  AStyle.StyleName := StyleName;
  if Parent <> nil then
    AStyle.Parent := AModel.TableCellStyles[Parent.Copy(ATargetModel)];
  ApplyPropertiesDiff(AStyle);
  Result := AStyle;
end;

function TdxTableCellStyle.CreateCellPropertiesChangedHistoryItem(
  AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Assert(AProperties = TableCellProperties);
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(AProperties.PieceTable as TdxPieceTable, AProperties);
end;

function TdxTableCellStyle.CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := TdxIndexChangedHistoryItem<TdxDocumentModelChangeActions>.Create(DocumentModel.MainPieceTable, CharacterProperties);
end;

function TdxTableCellStyle.GetCharacterProperties(AMask: TdxUsedCharacterFormattingOption; ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxCharacterProperties;
begin
  if Deleted then
    raise Exception.Create(''); 
  if (FCharacterProperties <> nil) and FCharacterProperties.UseVal(AMask) then
    Exit(FCharacterProperties);
  if Parent <> nil then
    Result := Parent.GetCharacterProperties(AMask, ARowType, AColumnType)
  else
    Result := nil;
end;

function TdxTableCellStyle.GetCharacterProperties: TdxCharacterProperties;
begin
  Result := CharacterProperties;
end;

function TdxTableCellStyle.GetMergedCharacterProperties(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxCharacterPropertiesMerger.Create(TdxMergedCharacterProperties.Create(TdxCharacterFormattingInfo.Create, TdxCharacterFormattingOptions.Create));
  try
    AMerger.Merge(CharacterProperties);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedCharacterProperties(ARowType, AColumnType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableCellStyle.GetMergedParagraphProperties: TdxMergedParagraphProperties;
begin
  Result := GetMergedParagraphProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal);
end;

function TdxTableCellStyle.GetMergedParagraphProperties(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxMergedParagraphProperties;
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxParagraphPropertiesMerger.Create(TdxMergedParagraphProperties.Create(TdxParagraphFormattingInfo.Create, TdxParagraphFormattingOptions.Create));
  try
    if ParagraphProperties <> nil then
      AMerger.Merge(ParagraphProperties);
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedParagraphProperties(ARowType, AColumnType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableCellStyle.GetMergedTableCellProperties: TdxMergedTableCellProperties;
begin
  Result := GetMergedTableCellProperties(TdxConditionalRowType.Normal, TdxConditionalColumnType.Normal);
end;

function TdxTableCellStyle.GetMergedTableCellProperties(ARowType: TdxConditionalRowType;
  AColType: TdxConditionalColumnType): TdxMergedTableCellProperties;
var
  AMerger: TdxTableCellPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTableCellPropertiesMerger.Create(TableCellProperties);
  try
    if Parent <> nil then
      AMerger.Merge(Parent.GetMergedTableCellProperties(ARowType, AColType));
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableCellStyle.GetMergedTableCellProperties(
  AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableCellProperties;
begin
  if Parent <> nil then
    Result := Parent.GetMergedTableCellProperties(AConditionType)
  else
    Result := TdxMergedTableCellProperties.Create(TdxCombinedCellPropertiesInfo.Create, TdxTableCellPropertiesOptions.Create);
end;

function TdxTableCellStyle.GetMergedTableRowProperties(
  AConditionType: TdxConditionalTableStyleFormattingTypes): TdxMergedTableRowProperties;
begin
  if Parent <> nil then
    Result := Parent.GetMergedTableRowProperties(AConditionType)
  else
    Result := TdxMergedTableRowProperties.Create(TdxCombinedTableRowPropertiesInfo.Create, TdxTableRowPropertiesOptions.Create);
end;

function TdxTableCellStyle.GetMergedWithDefaultCharacterProperties(ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxMergedCharacterProperties;
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxCharacterPropertiesMerger.Create(TdxMergedCharacterProperties.Create(TdxCharacterFormattingInfo.Create, TdxCharacterFormattingOptions.Create));
  try
    AMerger.Merge(CharacterProperties);
    AMerger.Merge(DocumentModel.DefaultCharacterProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableCellStyle.GetMergedWithDefaultTableCellProperties: TdxMergedTableCellProperties;
var
  AMerger: TdxTableCellPropertiesMerger;
begin
  if Deleted then
    raise Exception.Create(''); 
  AMerger := TdxTableCellPropertiesMerger.Create(GetMergedTableCellProperties);
  try
    AMerger.Merge(DocumentModel.DefaultTableCellProperties);
    Result := AMerger.MergedProperties;
  finally
    AMerger.Free;
  end;
end;

function TdxTableCellStyle.GetParagraphProperties(AMask: TdxUsedParagraphFormattingOption; ARowType: TdxConditionalRowType;
  AColumnType: TdxConditionalColumnType): TdxParagraphProperties;
begin
  if Deleted then
    raise Exception.Create(''); 
  if (ParagraphProperties <> nil) and ParagraphProperties.UseVal(AMask) then
    Exit(ParagraphProperties);
  if Parent <> nil then
    Result := Parent.GetParagraphProperties(AMask, ARowType, AColumnType)
  else
    Result := nil;
end;

function TdxTableCellStyle.GetParagraphProperties: TdxParagraphProperties;
begin
  Result := ParagraphProperties;
end;

function TdxTableCellStyle.GetParent: TdxTableCellStyle;
begin
  Result := Parent;
end;

function TdxTableCellStyle.GetParentStyle: TdxTableCellStyle;
begin
  Result := TdxTableCellStyle(inherited GetParentStyle);
end;

function TdxTableCellStyle.GetTableCellProperties: TdxTableCellProperties;
begin
  if FTableCellProperties = nil then
  begin
    FTableCellProperties := TdxTableCellProperties.Create(DocumentModel.MainPieceTable, Self);
    SubscribeTableCellPropertiesEvents;
  end;
  Result := FTableCellProperties;
end;

function TdxTableCellStyle.GetTableCellProperties(AMask: Integer): TdxTableCellProperties;
begin
  if Deleted then
    raise Exception.Create(''); 
  if (FTableCellProperties <> nil) and FTableCellProperties.GetUse(AMask) then
    Exit(FTableCellProperties);
  if Parent <> nil then
    Result := Parent.GetTableCellProperties(AMask)
  else
    Result := nil;
end;

function TdxTableCellStyle.GetType: TdxStyleType;
begin
  Result := TdxStyleType.TableCellStyle;
end;

procedure TdxTableCellStyle.MergePropertiesWithParent;
begin
  inherited MergePropertiesWithParent;
  TableCellProperties.Merge(Parent.TableCellProperties);
  CharacterProperties.Merge(Parent.CharacterProperties);
end;

procedure TdxTableCellStyle.OnCharacterPropertiesChanged;
begin
  DocumentModel.ResetDocumentFormattingCaches(TdxResetFormattingCacheType.Character);
end;

procedure TdxTableCellStyle.OnObtainAffectedRange(ASender: TObject; E: TdxObtainAffectedRangeEventArgs);
begin
  E.Start := 0;
  E.&End := MaxInt;
end;

procedure TdxTableCellStyle.ResetCachedIndices(AResetFormattingCacheType: TdxResetFormattingCacheType);
begin
end;

procedure TdxTableCellStyle.SetParentStyle(const Value: TdxTableCellStyle);
begin
  inherited SetParentStyle(Value);
end;

procedure TdxTableCellStyle.SubscribeCharacterPropertiesEvents;
begin
  CharacterProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

procedure TdxTableCellStyle.SubscribeTableCellPropertiesEvents;
begin
  FTableCellProperties.OnObtainAffectedRange.Add(OnObtainAffectedRange);
end;

{ TdxTableCellStyleCollection }

constructor TdxTableCellStyleCollection.Create(
  ADocumentModel: TdxCustomDocumentModel; AChangeDefaultTableCellStyle: Boolean);
begin
  inherited Create(ADocumentModel);
  Items.Add(CreateNormalTableCellStyle(AChangeDefaultTableCellStyle));
  Items.Add(CreateTableSimpleStyle);
end;

function TdxTableCellStyleCollection.CreateDefaultItem: TdxStyleBase;
begin
  Result := nil;
end;

function TdxTableCellStyleCollection.CreateNormalTableCellStyle(
  AChangeDefaultTableCellStyle: Boolean): TdxTableCellStyle;
var
  AStyle: TdxTableCellStyle;
begin
  AStyle := TdxTableCellStyle.Create(DocumentModel, nil, DefaultTableCellStyleName);
  if AChangeDefaultTableCellStyle then
  begin
    AStyle.TableCellProperties.BeginInit;
    try
    finally
      AStyle.TableCellProperties.EndInit;
    end;
  end;
  Result := AStyle;
end;

function TdxTableCellStyleCollection.CreateTableSimpleStyle: TdxTableCellStyle;
var
  AIsDeferred: Boolean;
  AStyle: TdxTableCellStyle;
  AOptions: TdxTableCellPropertiesOptions;
begin
  AStyle := TdxTableCellStyle.Create(DocumentModel, Self[DefaultTableCellStyleIndex], TableCellSimpleStyleName);
  AStyle.TableCellProperties.BeginInit();
  try
    SetDefaultMargin(AStyle.TableCellProperties.CellMargins.Left, FDefaultLeftMargin);
    SetDefaultMargin(AStyle.TableCellProperties.CellMargins.Right, FDefaultRightMargin);
    SetDefaultBorder(AStyle.TableCellProperties.Borders.LeftBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableCellProperties.Borders.RightBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableCellProperties.Borders.TopBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableCellProperties.Borders.BottomBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableCellProperties.Borders.InsideHorizontalBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);
    SetDefaultBorder(AStyle.TableCellProperties.Borders.InsideVerticalBorder, FDefaultBorderLineStyle, FDefaultBorderWidth);

    AOptions := TdxTableCellPropertiesAccess(AStyle.TableCellProperties).GetInfoForModification(AIsDeferred);
    AOptions.UseLeftMargin := True;
    AOptions.UseRightMargin := True;
    AOptions.UseTopMargin := True;
    AOptions.UseBottomMargin := True;
    AOptions.UseLeftBorder := True;
    AOptions.UseRightBorder := True;
    AOptions.UseTopBorder := True;
    AOptions.UseBottomBorder := True;
    AOptions.UseInsideHorizontalBorder := True;
    AOptions.UseInsideVerticalBorder := True;
    AStyle.TableCellProperties.ReplaceInfo(AOptions, []);
    if not AIsDeferred then
      AOptions.Free;
  finally
    AStyle.TableCellProperties.EndInit;
  end;
  Result := AStyle;
end;

function TdxTableCellStyleCollection.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := TdxDocumentModel(inherited DocumentModel);
end;

function TdxTableCellStyleCollection.GetItem(Index: Integer): TdxTableCellStyle;
begin
  Result := TdxTableCellStyle(inherited Self[Index]);
end;

class constructor TdxTableCellStyleCollection.Initialize;
begin
  TableCellSimpleStyleName := 'Table Cell Simple 1';
  DefaultTableCellStyleName := 'Normal Table Cell';
end;

procedure TdxTableCellStyleCollection.NotifyPieceTableStyleDeleting(APieceTable: TObject;
  AStyle: TdxStyleBase);
var
  I, ACount: Integer;
  ATables: TdxTableCollection;
  ATableCellStyle: TdxTableCellStyle absolute AStyle;
begin
  ATables := TdxPieceTable(APieceTable).Tables;
  ACount := ATables.Count;
  for I := 0 to ACount - 1 do
    ATables[I].ForEachCell(procedure(ACell: TdxTableCell)
      begin
        if ACell.TableCellStyle = ATableCellStyle then
          ACell.StyleIndex := DefaultItemIndex;
      end);
end;

procedure TdxTableCellStyleCollection.SetDefaultBorder(ABorder: TdxBorderBase; AStyle: TdxBorderLineStyle;
  AWidthInTwips: Integer);
begin
  ABorder.BeginInit;
  try
    ABorder.Style := TdxBorderLineStyle.Single;
    ABorder.Width := DocumentModel.UnitConverter.TwipsToModelUnits(AWidthInTwips);
  finally
    ABorder.EndInit;
  end;
end;

procedure TdxTableCellStyleCollection.SetDefaultMargin(AMargin: TdxWidthUnit; AValueInTwips: Integer);
begin
  AMargin.BeginInit;
  try
    AMargin.&Type := TdxWidthUnitType.ModelUnits;
    AMargin.Value := DocumentModel.UnitConverter.TwipsToModelUnits(AValueInTwips);
  finally
    AMargin.EndInit;
  end;
end;

end.
