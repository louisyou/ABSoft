{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationPicture;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, Windows, SysUtils, Generics.Collections, dxRichEdit.Utils.CheckSumStream,
  dxCoreClasses, dxRichEdit.Import.Rtf, dxRichEdit.Import.Rtf.DestinationHexContent, dxRichEdit.Utils.OfficeImage,
  dxRichEdit.DocumentModel.UnitConverter;

type
  TdxPictureSourceType = (Emf, Png, Jpeg, Mac, PmmMetafile, Wmf, WindowsDib, WindowsBmp);

  { TdxRtfShapeProperties }

  TdxRtfShapeProperties = class(TDictionary<string, Variant>)
  public
    procedure Assign(const Source: TdxRtfShapeProperties);

    function HasBoolProperty(const AName: string): Boolean;
    function HasIntegerProperty(const AName: string): Boolean;
    function GetIntegerPropertyValue(const AName: string): Integer;
    function GetBoolPropertyValue(const AName: string): Boolean;
  end;

  { TdxPictureDestinationInfo }

  TdxPictureDestinationInfo = class(TPersistent)
  strict private
    FDataStream: TdxCrc32Stream;
    FPictureSourceType: TdxPictureSourceType;
    FPictureWidth: Integer;
    FPictureHeight: Integer;
    FDesiredPictureWidth: Integer;
    FDesiredPictureHeight: Integer;
    FPictureStream: TBytesStream;
    FProperties: TdxRtfShapeProperties;
    FBmpBitsPerPixel: Integer;
    FBmpColorPlanes: Integer;
    FBmpBytesInLine: Integer;
    FScaleX: Integer;
    FScaleY: Integer;
    FImageUri: string;
    FWmfMapMode: TdxMapMode;
  private
    function GetDataStream: TdxCrc32Stream;
    function GetDataCrc32: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    procedure ClosePictureStream;

    property BmpBitsPerPixel: Integer read FBmpBitsPerPixel write FBmpBitsPerPixel;
    property BmpColorPlanes: Integer read FBmpColorPlanes write FBmpColorPlanes;
    property BmpBytesInLine: Integer read FBmpBytesInLine write FBmpBytesInLine;
    property DataCrc32: Integer read GetDataCrc32;
    property ScaleX: Integer read FScaleX write FScaleX;
    property ScaleY: Integer read FScaleY write FScaleY;
    property ImageUri: string read FImageUri write FImageUri;
    property PictureSourceType: TdxPictureSourceType read FPictureSourceType write FPictureSourceType;
    property PictureWidth: Integer read FPictureWidth write FPictureWidth;
    property PictureHeight: Integer read FPictureHeight write FPictureHeight;
    property Properties: TdxRtfShapeProperties read FProperties;
    property DataStream: TdxCrc32Stream read GetDataStream;
    property DesiredPictureWidth: Integer read FDesiredPictureWidth write FDesiredPictureWidth;
    property DesiredPictureHeight: Integer read FDesiredPictureHeight write FDesiredPictureHeight;
    property PictureStream: TBytesStream read FPictureStream;
    property WmfMapMode: TdxMapMode read FWmfMapMode write FWmfMapMode;
  end;

  { TdxPictureDestination }

  TdxPictureDestination = class(TdxHexContentDestination)
  type
    { TdxRtfPictureUnitsConverter }

    TdxRtfPictureUnitsConverter = class abstract
    public
      function UnitsToTwips(Value: Integer): Integer; virtual; abstract;
      function UnitsToModelUnits(Value: Integer; AUnitConverter: TdxDocumentModelUnitConverter): Integer; virtual; abstract;
    end;

    { TdxRtfPixelsToTwipsConverter }

    TdxRtfPixelsToTwipsConverter = class(TdxRtfPictureUnitsConverter)
    strict private
      FDpi: Integer;
    public
      constructor Create(ADpi: Integer);
      function UnitsToTwips(Value: Integer): Integer; override;
      function UnitsToModelUnits(Value: Integer; AUnitConverter: TdxDocumentModelUnitConverter): Integer; override;
    end;

    { TdxRtfHundredthsOfMillimeterConverter }

    TdxRtfHundredthsOfMillimeterConverter = class(TdxRtfPictureUnitsConverter)
    public
      function UnitsToTwips(Value: Integer): Integer; override;
      function UnitsToModelUnits(Value: Integer; AUnitConverter: TdxDocumentModelUnitConverter): Integer; override;
    end;

  private
    class var FRtfHundredthsOfMillimeterConverter: TdxRtfPictureUnitsConverter;
    class var FRtfPixelsConverter: TdxRtfPictureUnitsConverter;

    class constructor Initialize;
    class destructor Finalize;
  strict private
    FInfo: TdxPictureDestinationInfo;
    FOldDecoder: TdxCodePageCharacterDecoder;
    class procedure EmfFileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PngFileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure JpegFileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure MacFileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure WindowsMetafileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DeviceIndependentBitmapFileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DeviceDependentBitmapFileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure BitmapBitsPerPixelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure BitmapPlanesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure BitmapBytesInLineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PictureWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PictureHeightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure PictureGoalWidthKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PictureGoalHeightKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure HorizontalScalingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure VerticalScalingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PicScaledKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static; //Only for macpict pictures
    class procedure TopCropKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure BottomCropKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LeftCropKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure RightCropKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure BitmapMetafileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure BitsPerPixelBitmapMetafileKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DxImageUriHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ShapePropertiesKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;

    class function FillBytesToConvertFromShortIntToLongInt(AParameterValue: Word): Integer; inline; static;
    procedure LoadBitmap(AInfo: TdxRtfImageInfo);
    procedure LoadDib(AInfo: TdxRtfImageInfo);
    function LoadPicture: TdxRtfImageInfo;
    procedure LoadMetafile(AInfo: TdxRtfImageInfo);
    procedure LoadImage(AInfo: TdxRtfImageInfo);
    procedure LoadImageInUnits(AImageInfo: TdxRtfImageInfo; AUnitsConverter: TdxRtfPictureUnitsConverter);
    procedure LoadMetafileImageInUnits(AImageInfo: TdxRtfImageInfo; AUnitsConverter: TdxRtfPictureUnitsConverter);
    procedure ValidateImageSize(AImage: TdxOfficeImageReference);

    function GetDataStream: TdxCrc32Stream;
    function GetPictureSourceType: TdxPictureSourceType;
    function GetWmfMapMode: TdxMapMode;
    procedure SetPictureSourceType(const Value: TdxPictureSourceType); inline;
    procedure SetWmfMapMode(const Value: TdxMapMode); inline;
  protected
    function GetKeywordHT: TdxKeywordTranslatorTable; override;
    procedure InitializeClone(AClone: TdxRichEditRtfDestinationBase); override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    procedure ProcessBinCharCore(AChar: Char); override;
    procedure LoadPictureCore(AInfo: TdxRtfImageInfo);

    property DataStream: TdxCrc32Stream read GetDataStream;
    property PictureSourceType: TdxPictureSourceType read GetPictureSourceType write SetPictureSourceType;
    property WmfMapMode: TdxMapMode read GetWmfMapMode write SetWmfMapMode;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
    destructor Destroy; override;
    procedure BeforePopRtfState; override;

    function GetImageInfo: TdxRtfImageInfo;

    property Info: TdxPictureDestinationInfo read FInfo;
  end;

implementation

uses
  Variants, Math, cxVariants, cxGeometry;

type
  TdxRtfFormattingInfoAccess = class(TdxRtfFormattingInfo);

{ TdxRtfShapeProperties }

procedure TdxRtfShapeProperties.Assign(const Source: TdxRtfShapeProperties);
var
  AKey: string;
begin
  Clear;
  for AKey in Source.Keys do
    AddOrSetValue(AKey, Source[AKey]);
end;

function TdxRtfShapeProperties.GetBoolPropertyValue(
  const AName: string): Boolean;
begin
  Result := GetIntegerPropertyValue(AName) <> 0;
end;

function TdxRtfShapeProperties.GetIntegerPropertyValue(
  const AName: string): Integer;
begin
  Result := Self[AName];
end;

function TdxRtfShapeProperties.HasBoolProperty(const AName: string): Boolean;
begin
  Result := HasIntegerProperty(AName);
end;

function TdxRtfShapeProperties.HasIntegerProperty(const AName: string): Boolean;
begin
  Result := ContainsKey(AName) and VarIsNumeric(Self[AName]);
end;

{ TdxPictureDestinationInfo }

constructor TdxPictureDestinationInfo.Create;
begin
  inherited Create;
  FPictureSourceType := TdxPictureSourceType.WindowsBmp;
  FWmfMapMode := TdxMapMode.Text;
  FPictureWidth := -1;
  FPictureHeight := -1;
  FDesiredPictureWidth := -1;
  FDesiredPictureHeight := -1;
  FPictureStream := TBytesStream.Create;
  FDataStream := TdxCrc32Stream.Create(FPictureStream);
  FBmpBitsPerPixel := 1;
  FBmpColorPlanes := 1;
  FScaleX := 100;
  FScaleY := 100;
  FProperties := TdxRtfShapeProperties.Create;
end;

procedure TdxPictureDestinationInfo.Assign(Source: TPersistent);
var
  ASource: TdxPictureDestinationInfo;
begin
  if Source is TdxPictureDestinationInfo then
  begin
    ASource := TdxPictureDestinationInfo(Source);
    FPictureSourceType := ASource.PictureSourceType;
    FWmfMapMode := ASource.WmfMapMode;
    FPictureWidth := ASource.PictureWidth;
    FPictureHeight := ASource.PictureHeight;
    FDesiredPictureWidth := ASource.DesiredPictureWidth;
    FDesiredPictureHeight := ASource.DesiredPictureHeight;
    DataStream.Position := 0;
    DataStream.CopyFrom(ASource.DataStream, 0);
    PictureStream.Position := 0;
    PictureStream.CopyFrom(ASource.PictureStream, 0);
    Properties.Assign(ASource.Properties);
    FBmpBitsPerPixel := ASource.BmpBitsPerPixel;
    FBmpColorPlanes := ASource.BmpColorPlanes;
    FBmpBytesInLine := ASource.BmpBytesInLine;
    FScaleX := ASource.ScaleX;
    FScaleY := ASource.ScaleY;
    FImageUri := ASource.ImageUri;
  end
  else
    inherited Assign(Source);
end;

procedure TdxPictureDestinationInfo.ClosePictureStream;
begin
  FreeAndNil(FDataStream);
  FreeAndNil(FPictureStream);
end;

destructor TdxPictureDestinationInfo.Destroy;
begin
  FreeAndNil(FProperties);
  FreeAndNil(FPictureStream);
  FreeAndNil(FDataStream);
  inherited Destroy;
end;

function TdxPictureDestinationInfo.GetDataCrc32: Integer;
begin
  Result :=  DataStream.WriteCheckSum;
end;

function TdxPictureDestinationInfo.GetDataStream: TdxCrc32Stream;
begin
  Result := FDataStream;
end;

{ TdxPictureDestination }

constructor TdxPictureDestination.Create(
  AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FInfo := TdxPictureDestinationInfo.Create;
  FOldDecoder := Importer.Position.RtfFormattingInfo.Decoder;
  TdxRtfFormattingInfoAccess(Importer.Position.RtfFormattingInfo).SetDecoder(TdxEmptyCharacterDecoder.Create, False);
end;

destructor TdxPictureDestination.Destroy;
begin
  FreeAndNil(FInfo);
  inherited Destroy;
end;

procedure TdxPictureDestination.BeforePopRtfState;
begin
  TdxRtfFormattingInfoAccess(Importer.Position.RtfFormattingInfo).SetDecoder(FOldDecoder);
  inherited BeforePopRtfState;
end;

class procedure TdxPictureDestination.BitmapBitsPerPixelKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
assert(false, 'not implemented');
end;

class procedure TdxPictureDestination.BitmapBytesInLineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
assert(false, 'not implemented');
end;

class procedure TdxPictureDestination.BitmapMetafileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxPictureDestination.BitmapPlanesKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
assert(false, 'not implemented');
end;

class procedure TdxPictureDestination.BitsPerPixelBitmapMetafileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxPictureDestination.BottomCropKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxPictureDestination.DeviceDependentBitmapFileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  if AHasParameter and (AParameterValue <> 0) then
    AImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.PictureSourceType := TdxPictureSourceType.WindowsBmp;
end;

class procedure TdxPictureDestination.DeviceIndependentBitmapFileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  if AHasParameter and (AParameterValue <> 0) then
    AImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.PictureSourceType := TdxPictureSourceType.WindowsDib;
end;

class procedure TdxPictureDestination.DxImageUriHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
assert(false, 'not implemented');
end;

class procedure TdxPictureDestination.EmfFileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.PictureSourceType := TdxPictureSourceType.Emf;
end;

class function TdxPictureDestination.FillBytesToConvertFromShortIntToLongInt(
  AParameterValue: Word): Integer;
begin
  Result := $FFFF and AParameterValue;
end;

function TdxPictureDestination.GetDataStream: TdxCrc32Stream;
begin
  Result := Info.DataStream;
end;

function TdxPictureDestination.GetImageInfo: TdxRtfImageInfo;
begin
  if Info.PictureStream.Size <= 0 then
    Result := nil
  else
  begin
    Info.PictureStream.Seek(0, soBeginning);
    Result := LoadPicture;
    if Result <> nil then
    begin
      if Info.Properties.HasBoolProperty('fPseudoInline') then
        Result.PseudoInline := Info.Properties.GetBoolPropertyValue('fPseudoInline');
    end;
    Info.ClosePictureStream;
  end;
end;

function TdxPictureDestination.GetKeywordHT: TdxKeywordTranslatorTable;
begin
  Result := CreateKeywordTable;
end;

function TdxPictureDestination.GetPictureSourceType: TdxPictureSourceType;
begin
  Result := FInfo.PictureSourceType;
end;

function TdxPictureDestination.GetWmfMapMode: TdxMapMode;
begin
  Result := Info.WmfMapMode;
end;

class procedure TdxPictureDestination.HorizontalScalingKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  if not AHasParameter then
    AImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.Info.ScaleX := AParameterValue;
end;

class constructor TdxPictureDestination.Initialize;
begin
  FRtfHundredthsOfMillimeterConverter := TdxRtfHundredthsOfMillimeterConverter.Create;
  FRtfPixelsConverter := TdxRtfPixelsToTwipsConverter.Create(96);
end;

class destructor TdxPictureDestination.Finalize;
begin
  FreeAndNil(FRtfHundredthsOfMillimeterConverter);
  FreeAndNil(FRtfPixelsConverter);
end;

procedure TdxPictureDestination.InitializeClone(
  AClone: TdxRichEditRtfDestinationBase);
begin
  inherited InitializeClone(AClone);
  TdxPictureDestination(AClone).Info.Assign(Info);
end;

class procedure TdxPictureDestination.JpegFileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.PictureSourceType := TdxPictureSourceType.Jpeg;
end;

class procedure TdxPictureDestination.LeftCropKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

procedure TdxPictureDestination.LoadImage(AInfo: TdxRtfImageInfo);
begin
  if AInfo.RtfImage = nil then
  begin
    try
      AInfo.LoadImageFromStream(Info.PictureStream);
    except
      Exit;
    end;
  end;
  ValidateImageSize(AInfo.RtfImage);
  LoadImageInUnits(AInfo, FRtfHundredthsOfMillimeterConverter);
end;

procedure TdxPictureDestination.LoadImageInUnits(AImageInfo: TdxRtfImageInfo;
  AUnitsConverter: TdxRtfPictureUnitsConverter);
var
  AModelUnitConverter: TdxDocumentModelUnitConverter;
  AWidthInModelUnits, AHeightInModelUnits: Integer;
begin
  if AImageInfo.RtfImage.RawFormat in [TdxOfficeImageFormat.Emf, TdxOfficeImageFormat.Wmf] then
  begin
    LoadMetafileImageInUnits(AImageInfo, AUnitsConverter);
    Exit;
  end;
  Info.PictureWidth := AImageInfo.RtfImage.Image.SizeInTwips.cx;
  Info.PictureHeight := AImageInfo.RtfImage.Image.SizeInTwips.cy;
  if Info.DesiredPictureWidth <= 0 then
    Info.DesiredPictureWidth := Info.PictureWidth;
  if Info.DesiredPictureHeight <= 0 then
    Info.DesiredPictureHeight := Info.PictureHeight;
  if Info.ScaleX <= 0 then
    Info.ScaleX := 100;
  if Info.ScaleY <= 0 then
    Info.ScaleY := 100;
  if (Info.PictureWidth > 0) and (Info.DesiredPictureWidth > 0) then
    AImageInfo.ScaleX := Round(Info.ScaleX * Info.DesiredPictureWidth / Info.PictureWidth);
  if (Info.PictureHeight > 0) and (Info.DesiredPictureHeight > 0) then
    AImageInfo.ScaleY := Round(Info.ScaleY * Info.DesiredPictureHeight / Info.PictureHeight);

  AModelUnitConverter := Importer.UnitConverter;
  AWidthInModelUnits := Max(1, AModelUnitConverter.TwipsToModelUnits(Info.PictureWidth));
  AHeightInModelUnits := Max(1, AModelUnitConverter.TwipsToModelUnits(Info.PictureHeight));
  AImageInfo.SizeInModelUnits := cxSize(AWidthInModelUnits, AHeightInModelUnits);
end;

procedure TdxPictureDestination.LoadMetafile(AInfo: TdxRtfImageInfo);
var
  AActualFormat: TdxOfficeImageFormat;
begin
  if AInfo.RtfImage = nil then
  begin
    try
      AInfo.LoadMetafileFromStream(Info.PictureStream, WmfMapMode, Info.PictureWidth, Info.PictureHeight);
    except
      Exit;
    end;
  end;
  ValidateImageSize(AInfo.RtfImage);
  AActualFormat := AInfo.RtfImage.RawFormat;
  if AActualFormat in [TdxOfficeImageFormat.Wmf, TdxOfficeImageFormat.Emf] then
    LoadMetafileImageInUnits(AInfo, FRtfHundredthsOfMillimeterConverter)
  else
    LoadImageInUnits(AInfo, FRtfPixelsConverter);
end;

procedure TdxPictureDestination.LoadMetafileImageInUnits(
  AImageInfo: TdxRtfImageInfo; AUnitsConverter: TdxRtfPictureUnitsConverter);
var
  AModelUnitConverter: TdxDocumentModelUnitConverter;
  AWidthInModelUnits, AHeightInModelUnits: Integer;
begin
  if Info.DesiredPictureWidth <= 0 then
    Info.DesiredPictureWidth := AUnitsConverter.UnitsToTwips(Info.PictureWidth);
  if Info.DesiredPictureHeight <= 0 then
    Info.DesiredPictureHeight := AUnitsConverter.UnitsToTwips(Info.PictureHeight);
  if Info.ScaleX <= 0 then
    Info.ScaleX := 100;
  if Info.ScaleY <= 0 then
    Info.ScaleY := 100;
  if (Info.PictureWidth > 0) and (Info.DesiredPictureWidth > 0) then
    AImageInfo.ScaleX := Round(Info.ScaleX * Info.DesiredPictureWidth / AUnitsConverter.UnitsToTwips(Info.PictureWidth));
  if (Info.PictureHeight > 0) and (Info.DesiredPictureHeight > 0) then
    AImageInfo.ScaleY := Round(Info.ScaleY * Info.DesiredPictureHeight / AUnitsConverter.UnitsToTwips(Info.PictureHeight));

  AModelUnitConverter := Importer.UnitConverter;
  AWidthInModelUnits := Max(1, AUnitsConverter.UnitsToModelUnits(Info.PictureWidth, AModelUnitConverter));
  AHeightInModelUnits := Max(1, AUnitsConverter.UnitsToModelUnits(Info.PictureHeight, AModelUnitConverter));
  AImageInfo.SizeInModelUnits := cxSize(AWidthInModelUnits, AHeightInModelUnits);
end;

procedure TdxPictureDestination.LoadBitmap(AInfo: TdxRtfImageInfo);
begin
  if AInfo.RtfImage = nil then
  begin
    try
      AInfo.LoadBitmapFromStream(Info.PictureStream, Info.PictureWidth,
        Info.PictureHeight, Info.BmpColorPlanes, Info.BmpBitsPerPixel, Info.BmpBytesInLine);
    except
      Exit;
    end;
  end;
  LoadImageInUnits(AInfo, FRtfPixelsConverter);
end;

procedure TdxPictureDestination.LoadDib(AInfo: TdxRtfImageInfo);
begin
  if AInfo.RtfImage = nil then
  begin
    try
      AInfo.LoadDibFromStream(Info.PictureStream, Info.PictureWidth, Info.PictureHeight,
        Info.BmpBytesInLine);
    except
      Exit;
    end;
  end;
  LoadImageInUnits(AInfo, FRtfPixelsConverter);
end;

function TdxPictureDestination.LoadPicture: TdxRtfImageInfo;
begin
  Result := TdxRtfImageInfo.Create(Importer.DocumentModel);
  LoadPictureCore(Result);
  if Result.RtfImage = nil then
    FreeAndNil(Result)
  else
  begin
    if Result.RtfImage.Uri = '' then
      Result.RtfImage.Uri := Info.ImageUri;
  end;
end;

procedure TdxPictureDestination.LoadPictureCore(AInfo: TdxRtfImageInfo);
var
  AImageCache: TdxImageCache;
  ACrc32: Integer;
  ACachedImage: TdxOfficeImage;
begin
  ACrc32 := Info.DataCrc32;
  AImageCache := Importer.DocumentModel.ImageCache;
  ACachedImage := AImageCache.GetImage(ACrc32);
  if ACachedImage <> nil then
  begin
    Assert(AInfo.RtfImage = nil);
    AInfo.RtfImage := TdxOfficeImageReference.Create(Importer.DocumentModel.ImageCache, ACachedImage);
  end;

  case PictureSourceType of
    TdxPictureSourceType.Emf, TdxPictureSourceType.Wmf:
      LoadMetafile(AInfo);
    TdxPictureSourceType.WindowsBmp:
      LoadBitmap(AInfo);
    TdxPictureSourceType.WindowsDib:
      LoadDib(AInfo);
  else
    LoadImage(AInfo);
  end;
  if (ACachedImage = nil) and (AInfo.RtfImage <> nil) then
    AImageCache.AddImage(AInfo.RtfImage.Image, ACrc32);
end;

class procedure TdxPictureDestination.MacFileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.PictureSourceType := TdxPictureSourceType.Mac;
end;

class procedure TdxPictureDestination.PicScaledKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
assert(false, 'not implemented');
end;

class procedure TdxPictureDestination.PictureGoalHeightKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  if not AHasParameter then
    TdxRichEditDocumentModelRtfImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.Info.DesiredPictureHeight := AParameterValue;
end;

class procedure TdxPictureDestination.PictureGoalWidthKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  if not AHasParameter then
    TdxRichEditDocumentModelRtfImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.Info.DesiredPictureWidth := AParameterValue;
end;

class procedure TdxPictureDestination.PictureHeightKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
  ACorrectedValue: Integer;
begin
  if not AHasParameter then
    TdxRichEditDocumentModelRtfImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ACorrectedValue := AParameterValue;
  if (AParameterValue < 0) and (AParameterValue = ShortInt(AParameterValue)) then //B208956
    ACorrectedValue := FillBytesToConvertFromShortIntToLongInt(Word(AParameterValue));
  ADestination.Info.PictureHeight := ACorrectedValue;
end;

class procedure TdxPictureDestination.PictureWidthKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
  ACorrectedValue: Integer;
begin
  if not AHasParameter then
    TdxRichEditDocumentModelRtfImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ACorrectedValue := AParameterValue;
  if (AParameterValue < 0) and (AParameterValue = ShortInt(AParameterValue)) then //B208956
    ACorrectedValue := FillBytesToConvertFromShortIntToLongInt(Word(AParameterValue));
  ADestination.Info.PictureWidth := ACorrectedValue;
end;

class procedure TdxPictureDestination.PngFileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.PictureSourceType := TdxPictureSourceType.Png;
end;

procedure TdxPictureDestination.PopulateKeywordTable(
  ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('emfblip', EmfFileKeywordHandler);
  ATable.Add('pngblip', PngFileKeywordHandler);
  ATable.Add('jpegblip', JpegFileKeywordHandler);
  ATable.Add('macpict', MacFileKeywordHandler);
  ATable.Add('wmetafile', WindowsMetafileKeywordHandler);
  ATable.Add('dibitmap', DeviceIndependentBitmapFileKeywordHandler);
  ATable.Add('wbitmap', DeviceDependentBitmapFileKeywordHandler);

  //bitmapinfo(only for dibitmap and wbitmap
  ATable.Add('wbmbitspixel', BitmapBitsPerPixelKeywordHandler);
  ATable.Add('wbmplanes', BitmapPlanesKeywordHandler);
  ATable.Add('wbmwidthbytes', BitmapBytesInLineKeywordHandler);

  //pictsize
  ATable.Add('picw', PictureWidthKeywordHandler);
  ATable.Add('pich', PictureHeightKeywordHandler);
  ATable.Add('picwgoal', PictureGoalWidthKeywordHandler);
  ATable.Add('pichgoal', PictureGoalHeightKeywordHandler);
  ATable.Add('picscalex', HorizontalScalingKeywordHandler);
  ATable.Add('picscaley', VerticalScalingKeywordHandler);
  ATable.Add('picscaled', PicScaledKeywordHandler);//Only for macpict pictures
  ATable.Add('piccropt', TopCropKeywordHandler);
  ATable.Add('piccropb', BottomCropKeywordHandler);
  ATable.Add('piccropr', LeftCropKeywordHandler);
  ATable.Add('piccropl', RightCropKeywordHandler);

  //metafileinfo
  ATable.Add('picbmp', BitmapMetafileKeywordHandler);
  ATable.Add('picbpp', BitsPerPixelBitmapMetafileKeywordHandler);


  ATable.Add('dximageuri', DxImageUriHandler);

  ATable.Add('picprop', ShapePropertiesKeywordHandler);
end;

procedure TdxPictureDestination.ProcessBinCharCore(AChar: Char);
begin
  DataStream.WriteByte(Ord(AChar));
end;

class procedure TdxPictureDestination.RightCropKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

procedure TdxPictureDestination.SetPictureSourceType(
  const Value: TdxPictureSourceType);
begin
  FInfo.PictureSourceType := Value;
end;

procedure TdxPictureDestination.SetWmfMapMode(const Value: TdxMapMode);
begin
  Info.WmfMapMode := Value;
end;

class procedure TdxPictureDestination.ShapePropertiesKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
assert(false, 'not implemented');
end;

class procedure TdxPictureDestination.TopCropKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

procedure TdxPictureDestination.ValidateImageSize(AImage: TdxOfficeImageReference);
{$IFDEF RICH_IMAGE}
var
  AImageSize: TSize;
{$ENDIF}
begin
  if (Info.PictureWidth <= 0) or (Info.PictureHeight <= 0) then
  begin
  {$IFDEF RICH_IMAGE}
    AImageSize := AImage.SizeInHundredthsOfMillimeter;
    if Info.PictureWidth <= 0 then
      Info.PictureWidth := AImageSize.cx;
    if Info.PictureHeight <= 0 then
      Info.PictureHeight := AImageSize.cy;
  {$ENDIF}
  end;
end;

class procedure TdxPictureDestination.VerticalScalingKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  if not AHasParameter then
    AImporter.ThrowInvalidRtfFile;
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.Info.ScaleY := AParameterValue;
end;

class procedure TdxPictureDestination.WindowsMetafileKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  ADestination: TdxPictureDestination;
begin
  ADestination := TdxPictureDestination(AImporter.Destination);
  ADestination.PictureSourceType := TdxPictureSourceType.Wmf;
  if AHasParameter then
    ADestination.WmfMapMode := TdxMapMode(AParameterValue);
end;

{ TdxPictureDestination.TdxRtfPixelsToTwipsConverter }

constructor TdxPictureDestination.TdxRtfPixelsToTwipsConverter.Create(ADpi: Integer);
begin
  inherited Create;
  FDpi := ADpi;
end;

function TdxPictureDestination.TdxRtfPixelsToTwipsConverter.UnitsToModelUnits(Value: Integer;
  AUnitConverter: TdxDocumentModelUnitConverter): Integer;
begin
  Result := AUnitConverter.PixelsToModelUnits(Value, FDpi);
end;

function TdxPictureDestination.TdxRtfPixelsToTwipsConverter.UnitsToTwips(Value: Integer): Integer;
begin
  Result := Round(1440 * Value / FDpi);
end;

{ TdxPictureDestination.TdxRtfHundredthsOfMillimeterConverter }

function TdxPictureDestination.TdxRtfHundredthsOfMillimeterConverter.UnitsToModelUnits(Value: Integer;
  AUnitConverter: TdxDocumentModelUnitConverter): Integer;
begin
  Result := AUnitConverter.HundredthsOfMillimeterToModelUnitsRound(Value);
end;

function TdxPictureDestination.TdxRtfHundredthsOfMillimeterConverter.UnitsToTwips(
  Value: Integer): Integer;
begin
  Result := Round(1440 * Value / 2540.0);
end;

end.
