{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Dialogs.Fonts;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, ActnList, Menus,
  Controls, Forms, Dialogs, dxRichEdit.Dialog.CustomDialog, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxLayoutControlAdapters, dxLayoutcxEditAdapters, cxContainer, cxEdit, cxDropDownEdit,
  cxTextEdit, cxMaskEdit, cxFontNameComboBox, dxLayoutContainer, Vcl.StdCtrls, dxLayoutControl, cxColorComboBox, cxLabel,
  cxCheckBox, cxButtons, dxRichEdit.DocumentModel.CharacterFormatting;

type
  TdxFontDialogFontStyle = (dfsRegular, dfsBold, dfsItalic, dfsBoldItalic);

  TdxRichEditFontDialogForm = class(TdxRichEditCustomDialogForm)
    alActions: TActionList;
    aAllCaps: TAction;
    aDoubleStrikeout: TAction;
    aHidden: TAction;
    aStrikeout: TAction;
    aSubscript: TAction;
    aSuperscript: TAction;
    aUnderlineWordsOnly: TAction;
    btnCancel: TcxButton;
    btnOk: TcxButton;
    cbAllCaps: TcxCheckBox;
    cbDoubleStrikeout: TcxCheckBox;
    cbHidden: TcxCheckBox;
    cbStrikeout: TcxCheckBox;
    cbSubscript: TcxCheckBox;
    cbSuperscript: TcxCheckBox;
    cbUnderlineWordsOnly: TcxCheckBox;
    cmbFontColor: TcxColorComboBox;
    cmbFontName: TcxFontNameComboBox;
    cmbFontSize: TcxComboBox;
    cmbFontStyle: TcxComboBox;
    cmbUnderlineColor: TcxColorComboBox;
    cmbUnderlineStyle: TcxComboBox;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1Group1: TdxLayoutGroup;
    dxLayoutControl1Group2: TdxLayoutAutoCreatedGroup;
    dxLayoutControl1Group3: TdxLayoutGroup;
    dxLayoutControl1Group4: TdxLayoutGroup;
    dxLayoutControl1Group5: TdxLayoutGroup;
    dxLayoutControl1Group6: TdxLayoutGroup;
    dxLayoutControl1Group7: TdxLayoutGroup;
    dxLayoutControl1Item1: TdxLayoutItem;
    dxLayoutControl1Item10: TdxLayoutItem;
    dxLayoutControl1Item2: TdxLayoutItem;
    dxLayoutControl1Item3: TdxLayoutItem;
    dxLayoutControl1Item4: TdxLayoutItem;
    dxLayoutControl1Item5: TdxLayoutItem;
    dxLayoutControl1Item6: TdxLayoutItem;
    dxLayoutControl1Item7: TdxLayoutItem;
    dxLayoutControl1Item8: TdxLayoutItem;
    dxLayoutControl1Item9: TdxLayoutItem;
    lblEffects: TcxLabel;
    lciFontColor: TdxLayoutItem;
    lciFontName: TdxLayoutItem;
    lciFontSize: TdxLayoutItem;
    lciFontStyle: TdxLayoutItem;
    lciUnderlineColor: TdxLayoutItem;
    lciUnderlineStyle: TdxLayoutItem;
    lcMainGroup_Root: TdxLayoutGroup;
    procedure aEffectsExecute(Sender: TObject);
    procedure cmbColorPropertiesNamingConvention(Sender: TObject; const AColor: TColor;
      var AColorDescription: string);
    procedure FormCreate(Sender: TObject);
  private
    procedure AddAutoButton(AColorComboBox: TcxColorComboBox);
    procedure cbColorComboBoxButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure DoSetFontStyleNames;
    procedure DoSetUnderlineStyles;
  protected
    procedure ApplyLocalization; override;
    procedure InitializeForm; override;
  public
  end;

var
  dxRichEditFontDialogForm: TdxRichEditFontDialogForm;

implementation

uses
  dxCore, cxDrawTextUtils, dxRichEdit.DialogStrs;

var
  dxFontStyleNames: array[TdxFontDialogFontStyle] of Pointer =
    (@sdxRichEditFontDialogFontStyleRegular, @sdxRichEditFontDialogFontStyleItalic,
     @sdxRichEditFontDialogFontStyleBold, @sdxRichEditFontDialogFontStyleBoldItalic);

{$R *.dfm}

{ TdxRichEditFontDialogForm }

procedure TdxRichEditFontDialogForm.cmbColorPropertiesNamingConvention(Sender: TObject; const AColor: TColor;
  var AColorDescription: string);
begin
  if AColor = clDefault then
    AColorDescription := cxGetResourceString(@sdxRichEditFontDialogButtonColorAuto)
  else
    if AColorDescription = '' then
      AColorDescription := '$' + IntToHex(AColor, 6);
end;

procedure TdxRichEditFontDialogForm.FormCreate(Sender: TObject);
begin
  inherited;
  ApplyLocalization;
  InitializeForm;
end;

procedure TdxRichEditFontDialogForm.aEffectsExecute(Sender: TObject);
begin
// do nothing
end;

procedure TdxRichEditFontDialogForm.AddAutoButton(AColorComboBox: TcxColorComboBox);
var
  AItem: TcxEditButton;
begin
  AItem := AColorComboBox.Properties.Buttons.Add;
  AItem.Kind := bkText;
  AItem.Caption := cxGetResourceString(@sdxRichEditFontDialogButtonColorAuto);
  AItem.Width := cxTextWidth(AColorComboBox.Style.GetVisibleFont, AItem.Caption) + 8 * cxTextSpace;
  AColorComboBox.Properties.OnButtonClick := cbColorComboBoxButtonClick;
end;

procedure TdxRichEditFontDialogForm.cbColorComboBoxButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  if AButtonIndex = 2 then
    (Sender as TcxColorComboBox).ColorValue := clDefault;
end;

procedure TdxRichEditFontDialogForm.DoSetFontStyleNames;
var
  S: Pointer;
begin
  cmbFontStyle.Clear;
  cmbFontStyle.Properties.Items.BeginUpdate;
  for S in dxFontStyleNames do
    cmbFontStyle.Properties.Items.Add(cxGetResourceString(S));
  cmbFontStyle.Properties.Items.EndUpdate;
end;


procedure TdxRichEditFontDialogForm.DoSetUnderlineStyles;
begin
  cmbUnderlineStyle.Clear;
  cmbUnderlineStyle.Properties.Items.BeginUpdate;
  cmbUnderlineStyle.Properties.Items.AddObject(cxGetResourceString(@sdxRichEditFontDialogUnderlineStyleNone), TObject(TdxUnderlineType.None));

  cmbUnderlineStyle.Properties.Items.AddObject(cxGetResourceString(@sdxRichEditFontDialogUnderlineStyleSingle), TObject(TdxUnderlineType.Single));
  cmbUnderlineStyle.Properties.Items.AddObject(cxGetResourceString(@sdxRichEditFontDialogUnderlineStyleDouble), TObject(TdxUnderlineType.Double));

  cmbUnderlineStyle.Properties.Items.EndUpdate;
end;

procedure TdxRichEditFontDialogForm.ApplyLocalization;
begin
  Caption := cxGetResourceString(@sdxRichEditFontDialogForm);
  lblEffects.Caption := cxGetResourceString(@sdxRichEditFontDialogEffects);
  btnOk.Caption := cxGetResourceString(@sdxRichEditFontDialogButtonOk);
  btnCancel.Caption := cxGetResourceString(@sdxRichEditFontDialogButtonCancel);
  lciFontName.CaptionOptions.Text := cxGetResourceString(@sdxRichEditFontDialogFontName);
  lciFontStyle.CaptionOptions.Text := cxGetResourceString(@sdxRichEditFontDialogFontStyle);
  lciFontSize.CaptionOptions.Text := cxGetResourceString(@sdxRichEditFontDialogFontSize);
  lciFontColor.CaptionOptions.Text := cxGetResourceString(@sdxRichEditFontDialogFontColor);
  lciUnderlineStyle.CaptionOptions.Text := cxGetResourceString(@sdxRichEditFontDialogUnderlineStyle);
  lciUnderlineColor.CaptionOptions.Text := cxGetResourceString(@sdxRichEditFontDialogUnderlineColor);
  aStrikeout.Caption := cxGetResourceString(@sdxRichEditFontDialogStrikeout);
  aDoubleStrikeout.Caption := cxGetResourceString(@sdxRichEditFontDialogDoubleStrikeout);
  aUnderlineWordsOnly.Caption := cxGetResourceString(@sdxRichEditFontDialogUnderlineWordsOnly);
  aSuperscript.Caption := cxGetResourceString(@sdxRichEditFontDialogSuperscript);
  aSubscript.Caption := cxGetResourceString(@sdxRichEditFontDialogSubscript);
  aAllCaps.Caption := cxGetResourceString(@sdxRichEditFontDialogAllCaps);
  aHidden.Caption := cxGetResourceString(@sdxRichEditFontDialogHidden);

  DoSetFontStyleNames;
  DoSetUnderlineStyles;
end;

procedure TdxRichEditFontDialogForm.InitializeForm;
begin
  inherited;
  AddAutoButton(cmbFontColor);
  AddAutoButton(cmbUnderlineColor);
end;

end.
