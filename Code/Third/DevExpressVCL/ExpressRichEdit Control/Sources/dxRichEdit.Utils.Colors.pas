{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.Colors;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Graphics, Windows, cxGraphics, dxCoreGraphics;

type
  { TdxColor }

  TdxColor = class sealed
  const
    Empty = clNone;
    Transparent = clNone;
    Black = clBlack;
    Gray = clGray;
    White = clWhite;
  public
    class function FromArgb(R, G, B: Integer): TColor; overload; static; inline;
    class function FromArgb(Alpha, R, G, B: Integer): TdxAlphaColor; overload; static; inline;

    class function Blend(AColor, ABackgroundColor: TdxAlphaColor): TdxAlphaColor; overload; static; inline;
    class function Blend(AColor, ABackgroundColor: TColor): TColor; overload; static; inline;
    class function IsEmpty(AColor: TColor): Boolean; 
    class function IsTransparentOrEmpty(AColor: TColor): Boolean; static; inline;
  end;

implementation

{ TdxColor }

class function TdxColor.FromArgb(R, G, B: Integer): TColor;
begin
  Result := RGB(R, G, B);
end;

class function TdxColor.Blend(AColor, ABackgroundColor: TdxAlphaColor): TdxAlphaColor;
var
  Alpha, AOneAlpha: Single;
begin
  if dxGetAlpha(AColor) = 255 then
    Result := AColor
  else
  begin
    Alpha := dxGetAlpha(AColor) / 255;
    AOneAlpha := 1 - Alpha;

    Result := FromArgb(Round(dxGetRed(AColor) * Alpha + dxGetRed(ABackgroundColor) * AOneAlpha),
      Round(dxGetGreen(AColor) * Alpha + dxGetGreen(ABackgroundColor) * AOneAlpha),
      Round(dxGetBlue(AColor) * Alpha + dxGetBlue(ABackgroundColor) * AOneAlpha));
  end;
end;

class function TdxColor.Blend(AColor, ABackgroundColor: TColor): TColor;
begin
  Result := dxAlphaColorToColor(Blend(dxColorToAlphaColor(AColor), dxColorToAlphaColor(ABackgroundColor)));
end;

class function TdxColor.FromArgb(Alpha, R, G, B: Integer): TdxAlphaColor;
begin
  Result := dxColorToAlphaColor(RGB(R, G, B), Alpha);
end;

class function TdxColor.IsEmpty(AColor: TColor): Boolean;
begin
  Result := AColor = Empty; 
end;

class function TdxColor.IsTransparentOrEmpty(AColor: TColor): Boolean;
begin
  Result := (AColor = Empty) or (AColor = Transparent);
end;

end.
