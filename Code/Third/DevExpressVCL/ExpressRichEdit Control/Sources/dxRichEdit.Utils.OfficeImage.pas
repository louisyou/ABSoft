{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.OfficeImage;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Windows, Graphics, Controls, Generics.Collections, cxGraphics,
  dxCoreClasses, dxRichEdit.Utils.Types,
  dxGDIPlusClasses, dxGDIPlusApi, dxRichEdit.DocumentModel.UnitConverter, cxGeometry, dxRichEdit.DocumentModel.Core;

type
  TdxOfficeImageFormat = (
    None = 0,
    Bmp = 1,
    Emf = 2,
    Exif = 3,
    Gif = 4,
    Icon = 5,
    Jpeg = 6,
    MemoryBmp = 7,
    Png = 8,
    Tiff = 9,
    Wmf = 10);

  TdxMapMode = (
    Text = 1,
    LowMetric = 2,
    HighMetric = 3,
    LowEnglish = 4,
    HighEnglish = 5,
    Twips = 6,
    Isotropic = 7,
    Anisotropic = 8);

  { TdxOfficeImage }

  TdxOfficeImage = class(TdxSmartImage) 
  private
    FIsLoaded: Boolean;
    FSuppressStore: Boolean;
    FSuppressStorePlaceholder: Boolean;
    FUri: string;
    function GetRawFormat: TdxOfficeImageFormat;
    function GetClientRectInImageUnits: TdxRectF;
    function GetSizeInImageUnits: TdxSizeF;
    function GetSuppressStore: Boolean;
    function GetSizeInTwips: TSize;
    function GetSizeInHundredthsOfMillimeter: TSize;
  protected
    function GetHorizontalResolution: Single; virtual;
    function GetPhysicalDimension: TdxGpSizeF; virtual;
    function GetSizeInOriginalUnits: TSize; virtual;
    function GetVerticalResolution: Single; virtual;

    function GetEmfImageBytes: TBytes;
    function GetWmfImageBytes: TBytes;

    function UnitsToTwips(const ASizeInUnits: TSize): TSize;
    function UnitsToHundredthsOfMillimeter(const ASizeInUnits: TSize): TSize;
    function EnsureNonZeroSize(const ASize: TSize): TSize; 

    property IsLoaded: Boolean read FIsLoaded;
  public
    function CalculateImageSizeInModelUnits(AUnitConverter: TdxDocumentModelUnitConverter): TSize; 
    function CanGetImageBytes(AImageFormat: TdxOfficeImageFormat): Boolean;
    procedure EnsureLoadComplete;
    function GetImageBytesStream(AImageFormat: TdxOfficeImageFormat): TStream;
    function IsExportSupported(ARawFormat: TdxOfficeImageFormat): Boolean;

    function CalculateCrc32: Integer; virtual;
    function Clone: TdxOfficeImage; reintroduce;
    function GetEmfImageBytesStream: TStream;
    function GetWmfImageBytesStream: TStream;

    property ClientRectInImageUnits: TdxRectF read GetClientRectInImageUnits;
    property RawFormat: TdxOfficeImageFormat read GetRawFormat;
    property Uri: string read FUri write FUri;
    property HorizontalResolution: Single read GetHorizontalResolution;
    property PhysicalDimension: TdxGpSizeF read GetPhysicalDimension;
    property SizeInOriginalUnits: TSize read GetSizeInOriginalUnits;
    property SizeInImageUnits: TdxSizeF read GetSizeInImageUnits;
    property SizeInTwips: TSize read GetSizeInTwips;
    property SizeInHundredthsOfMillimeter: TSize read GetSizeInHundredthsOfMillimeter;
    property SuppressStore: Boolean read GetSuppressStore write FSuppressStore;
    property SuppressStorePlaceholder: Boolean read FSuppressStorePlaceholder write FSuppressStorePlaceholder;
    property VerticalResolution: Single read GetVerticalResolution;
  end;

  { TdxOfficeImageReference }

  TdxImageCache = class;

  TdxOfficeImageReference = class
  strict private
    FImage: TdxOfficeImage;
    FOwner: TdxImageCache;
    function GetUri: string;
    procedure SetUri(const Value: string);
  private
    function GetRawFormat: TdxOfficeImageFormat;
    function GetSuppressStore: Boolean;
    procedure SetSuppressStore(const Value: Boolean);
    function GetSize: TSize;
    function GetHorizontalResolution: Single;
    function GetVerticalResolution: Single;
    function GetDocumentModel: TdxCustomDocumentModel;
  public
    constructor Create(AOwner: TdxImageCache; ABaseImage: TdxOfficeImage);
    destructor Destroy; override;

    function CalculateImageSizeInModelUnits(AUnitConverter: TdxDocumentModelUnitConverter): TSize; 
    function CanGetImageBytes(AImageFormat: TdxOfficeImageFormat): Boolean;
    procedure EnsureLoadComplete;
    function GetImageBytesStream(AImageFormat: TdxOfficeImageFormat): TStream;
    function IsExportSupported(ARawFormat: TdxOfficeImageFormat): Boolean;

    function GetEmfImageBytesStream: TStream;
    function GetWmfImageBytesStream: TStream;

    function Clone(AOwner: TdxImageCache): TdxOfficeImageReference;

    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;
    property HorizontalResolution: Single read GetHorizontalResolution;
    property Image: TdxOfficeImage read FImage;
    property RawFormat: TdxOfficeImageFormat read GetRawFormat;
    property Size: TSize read GetSize;
    property SuppressStore: Boolean read GetSuppressStore write SetSuppressStore;
    property Uri: string read GetUri write SetUri;
    property VerticalResolution: Single read GetVerticalResolution;
  end;

  { TdxImageCache }

  TdxImageCache = class
  private
    FImages: TDictionary<Integer, TdxOfficeImage>;
    FOwner: TdxCustomDocumentModel;
  protected
    function GetCrc32(AImage: TdxOfficeImage): Integer;
  public
    constructor Create(AOwner: TdxCustomDocumentModel);
    destructor Destroy; override;

    procedure AddImage(AImage: TdxOfficeImage; ACrc32: Integer);
    procedure Clear;
    function GetImage(ACrc32: Integer): TdxOfficeImage;

    property Owner: TdxCustomDocumentModel read FOwner;
  end;

  { TdxMetafileHelper }

  TdxMetafileHelper = class
  const
    MetafileResolution = 96;
  protected
    class function TryCreateMetafileCore(AHandle: THandle): TMetafile;
  public
    class function CreateMetafile(AStream: TMemoryStream; AMapMode: TdxMapMode; APictureWidth, APictureHeight: Integer): TdxOfficeImage;
    class function TryCreateMetafile(AHandle: THandle): TMetafile;
    class procedure DeleteMetafileHandle(AHandle: THandle);

  end;

implementation

uses
  Math, dxTypeHelpers, dxRichEdit.Utils.CheckSumStream;

type
  TdxGPImageDataAccess = class(TdxGPImageData);

function PixelsToHundredthsOfMillimeter(AValue: Integer; ADpi: Single): Integer; overload;
begin
  Result := Round(2540 * AValue / ADpi);
end;

function PixelsToHundredthsOfMillimeter(const ASize: TSize; ADpiX, ADpiY: Double): TSize; overload;
begin
  Result := cxSize(PixelsToHundredthsOfMillimeter(ASize.cx, ADpiX), PixelsToHundredthsOfMillimeter(ASize.cy, ADpiY));
end;

function PixelsToTwips(const ASize: TSize; ADpiX, ADpiY: Single): TSize;
begin
  if ADpiX = 0 then
    ADpiX := 1440;
  if ADpiY = 0 then
    ADpiY := 1440;

  Result := cxSize(MulDiv(ASize.cx, 1440, Trunc(ADpiX)), MulDiv(ASize.cy, 1440, Trunc(ADpiY)));
end;

{ TdxOfficeImage }

function TdxOfficeImage.CalculateCrc32: Integer;
var
  AStream: TStream;
  ADataStream: TdxCrc32Stream;
begin
  AStream := TMemoryStream.Create;
  try
    ADataStream := TdxCrc32Stream.Create(AStream);
    try
      SaveToStream(ADataStream);
      Result := ADataStream.WriteCheckSum;
    finally
      ADataStream.Free;
    end;
  finally
    AStream.Free;
  end;
end;

function TdxOfficeImage.CalculateImageSizeInModelUnits(AUnitConverter: TdxDocumentModelUnitConverter): TSize;
begin
  Result := EnsureNonZeroSize(AUnitConverter.PixelsToModelUnits(SizeInOriginalUnits, HorizontalResolution, VerticalResolution));
end;

function TdxOfficeImage.CanGetImageBytes(AImageFormat: TdxOfficeImageFormat): Boolean;
begin
  Result := True;
end;

function TdxOfficeImage.Clone: TdxOfficeImage;
begin
  Result := TdxOfficeImage(inherited Clone);
end;

procedure TdxOfficeImage.EnsureLoadComplete;
begin
end;

function TdxOfficeImage.UnitsToHundredthsOfMillimeter(
  const ASizeInUnits: TSize): TSize;
begin
  Result := EnsureNonZeroSize(PixelsToHundredthsOfMillimeter(ASizeInUnits, HorizontalResolution, VerticalResolution));
end;

function TdxOfficeImage.UnitsToTwips(const ASizeInUnits: TSize): TSize;
begin
  Result := EnsureNonZeroSize(PixelsToTwips(ASizeInUnits, HorizontalResolution, VerticalResolution));
end;

function TdxOfficeImage.EnsureNonZeroSize(const ASize: TSize): TSize;
begin
  Result := cxSize(Math.Max(1, ASize.Width), Math.Max(1, ASize.Height));
end;

function TdxOfficeImage.GetClientRectInImageUnits: TdxRectF;
var
  ASize: TdxSizeF;
begin
  ASize := SizeInImageUnits;
  Result := cxRectF(0, 0, ASize.cx, ASize.cy);
end;

function TdxOfficeImage.GetEmfImageBytes: TBytes;
var
  AMetafile: TMetafile;
  ALength: Integer;
begin
  AMetafile := TMetafile.Create;
  try
    AMetafile.Assign(Self);
    ALength := GetEnhMetaFileBits(AMetafile.Handle, 0, nil);
    if ALength = 0 then
      Exit;
    SetLength(Result, ALength);
    GetEnhMetaFileBits(AMetafile.Handle, ALength, @Result[0]);
  finally
    AMetafile.Free;
  end;
end;

function TdxOfficeImage.GetWmfImageBytes: TBytes;
var
  AMetafile: TMetafile;
  ALength: Integer;
begin
  AMetafile := TMetafile.Create;
  try
    AMetafile.Enhanced := False;
    AMetafile.Assign(Self);
    ALength := GdipEmfToWmfBits(AMetafile.Handle, 0, nil, MM_ANISOTROPIC, 0);
    if ALength = 0 then
      Exit;
    SetLength(Result, ALength);
    GdipEmfToWmfBits(AMetafile.Handle, ALength, @Result[0], MM_ANISOTROPIC, 0);
  finally
    AMetafile.Free;
  end;
end;

function TdxOfficeImage.GetEmfImageBytesStream: TStream;
var
  ABytes: TBytes;
begin
  ABytes := GetEmfImageBytes;
  try
    Result := TBytesStream.Create(ABytes);
  finally
    SetLength(ABytes, 0);
  end;
end;

function TdxOfficeImage.GetHorizontalResolution: Single;
begin
  if (Handle = nil) or (GdipGetImageHorizontalResolution(Handle, Result) <> Ok) then
    Result := 96;
end;

function TdxOfficeImage.GetImageBytesStream(
  AImageFormat: TdxOfficeImageFormat): TStream;
const
  AImageDataFormatMap: array[TdxOfficeImageFormat] of TdxImageDataFormat =
    (dxImageUnknown, dxImageBitmap, dxImageEmf, dxImageExif, dxImageGif,
    dxImageIcon, dxImageJpeg, dxImageMemoryBmp, dxImagePng, dxImageTiff,
    dxImageWmf);
var
  AImage: TdxOfficeImage;
begin
  if AImageFormat = TdxOfficeImageFormat.Wmf then
    Result := GetWmfImageBytesStream
  else
  if AImageFormat = TdxOfficeImageFormat.Emf then
    Result := GetEmfImageBytesStream
  else
  begin
    AImage := TdxOfficeImage.Create;
    try
      AImage.Assign(Self);
      AImage.ImageDataFormat := AImageDataFormatMap[AImageFormat];
      Result := TBytesStream.Create;
      Result.CopyFrom(TdxGPImageDataAccess(AImage.ImageData).FData, 0);
    finally
      AImage.Free;
    end;
  end;
end;

function TdxOfficeImage.GetPhysicalDimension: TdxGpSizeF;
begin
  if (Handle = nil) or (GdipGetImageDimension(Handle, Result.Width, Result.Height) <> Ok) then
    Result := MakeSize(0.0, 0.0);
end;

function TdxOfficeImage.GetRawFormat: TdxOfficeImageFormat;
const
  ResultMap: array[TdxImageDataFormat] of TdxOfficeImageFormat = (
    TdxOfficeImageFormat.None, TdxOfficeImageFormat.Bmp, TdxOfficeImageFormat.Jpeg, TdxOfficeImageFormat.Png,
    TdxOfficeImageFormat.Tiff, TdxOfficeImageFormat.Gif, TdxOfficeImageFormat.Emf, TdxOfficeImageFormat.Exif,
    TdxOfficeImageFormat.Icon, TdxOfficeImageFormat.MemoryBmp, TdxOfficeImageFormat.Wmf);
begin
  Result := ResultMap[ImageDataFormat];
end;

function TdxOfficeImage.GetSizeInOriginalUnits: TSize;
begin
  Result := cxSize(Trunc(PhysicalDimension.Width), Trunc(PhysicalDimension.Height));
end;

function TdxOfficeImage.GetSizeInTwips: TSize;
begin
  Result := UnitsToTwips(SizeInOriginalUnits);
end;

function TdxOfficeImage.GetSuppressStore: Boolean;
begin
  Result := FSuppressStore or (SuppressStorePlaceholder and not IsLoaded);
end;

function TdxOfficeImage.GetSizeInHundredthsOfMillimeter: TSize;
begin
  Result := UnitsToHundredthsOfMillimeter(SizeInOriginalUnits);
end;

function TdxOfficeImage.GetSizeInImageUnits: TdxSizeF;
var
  ASrcRect: TdxGpRectF;
  ASrcUnit: GpUNIT;
begin
  if GdipGetImageBounds(Handle, @ASrcRect, ASrcUnit) = Ok then
  begin
    Result.cx := ASrcRect.Width;
    Result.cy := ASrcRect.Height;
  end
  else
    Result := dxSizeF(Size.cx, Size.cy);
end;

function TdxOfficeImage.GetVerticalResolution: Single;
begin
  if (Handle = nil) or (GdipGetImageVerticalResolution(Handle, Result) <> Ok) then
    Result := 96;
end;

function TdxOfficeImage.GetWmfImageBytesStream: TStream;
var
  ABytes: TBytes;
begin
  ABytes := GetWmfImageBytes;
  try
    Result := TBytesStream.Create(ABytes);
  finally
    SetLength(ABytes, 0);
  end;
end;

function TdxOfficeImage.IsExportSupported(
  ARawFormat: TdxOfficeImageFormat): Boolean;
begin
  Result := True;
end;

{ TdxOfficeImageReference }

function TdxOfficeImageReference.CalculateImageSizeInModelUnits(
  AUnitConverter: TdxDocumentModelUnitConverter): TSize;
begin
  Result := Image.CalculateImageSizeInModelUnits(AUnitConverter);
end;

function TdxOfficeImageReference.CanGetImageBytes(
  AImageFormat: TdxOfficeImageFormat): Boolean;
begin
  Result := Image.CanGetImageBytes(AImageFormat);
end;

function TdxOfficeImageReference.Clone(AOwner: TdxImageCache): TdxOfficeImageReference;
var
  AImage: TdxOfficeImage;
  ACrc32: Integer;
begin
  if FOwner = AOwner then
    AImage := Image
  else
  begin
    ACrc32 := FOwner.GetCrc32(FImage);
    AImage := AOwner.GetImage(ACrc32);
    if AImage = nil then
    begin
      AImage := FImage.Clone;
      AOwner.AddImage(AImage, ACrc32);
    end;
  end;
  Result := TdxOfficeImageReference.Create(AOwner, AImage);
end;

constructor TdxOfficeImageReference.Create(AOwner: TdxImageCache;
  ABaseImage: TdxOfficeImage);
begin
  inherited Create;
  FImage := ABaseImage;
  FOwner := AOwner;
end;

destructor TdxOfficeImageReference.Destroy;
begin

  inherited;
end;

procedure TdxOfficeImageReference.EnsureLoadComplete;
begin
  Image.EnsureLoadComplete;
end;

function TdxOfficeImageReference.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := FOwner.Owner;
end;

function TdxOfficeImageReference.GetEmfImageBytesStream: TStream;
begin
  Result := Image.GetEmfImageBytesStream;
end;

function TdxOfficeImageReference.GetHorizontalResolution: Single;
begin
  Result := Image.HorizontalResolution;
end;

function TdxOfficeImageReference.GetImageBytesStream(
  AImageFormat: TdxOfficeImageFormat): TStream;
begin
  Result := Image.GetImageBytesStream(AImageFormat);
end;

function TdxOfficeImageReference.GetRawFormat: TdxOfficeImageFormat;
begin
  Result := FImage.RawFormat;
end;

function TdxOfficeImageReference.GetSize: TSize;
begin
  Result := Image.Size;
end;

function TdxOfficeImageReference.GetSuppressStore: Boolean;
begin
  Result := Image.SuppressStore;
end;

function TdxOfficeImageReference.GetUri: string;
begin
  Result := FImage.Uri;
end;

function TdxOfficeImageReference.GetVerticalResolution: Single;
begin
  Result := Image.VerticalResolution;
end;

function TdxOfficeImageReference.GetWmfImageBytesStream: TStream;
begin
  Result := Image.GetWmfImageBytesStream;
end;

function TdxOfficeImageReference.IsExportSupported(
  ARawFormat: TdxOfficeImageFormat): Boolean;
begin
  Result := Image.IsExportSupported(ARawFormat);
end;

procedure TdxOfficeImageReference.SetSuppressStore(const Value: Boolean);
begin
  Image.SuppressStore := Value;
end;

procedure TdxOfficeImageReference.SetUri(const Value: string);
begin
  FImage.Uri := Value;
end;

{ TdxImageCache }

constructor TdxImageCache.Create(AOwner: TdxCustomDocumentModel);
begin
  inherited Create;
  FOwner := AOwner;
  FImages := TDictionary<Integer, TdxOfficeImage>.Create;
end;

destructor TdxImageCache.Destroy;
begin
  Clear;
  FreeAndNil(FImages);
  inherited Destroy;
end;

procedure TdxImageCache.AddImage(AImage: TdxOfficeImage; ACrc32: Integer);
begin
  FImages.Add(ACrc32, AImage);
end;

function TdxImageCache.GetCrc32(AImage: TdxOfficeImage): Integer;
begin
  for Result in FImages.Keys do
    if FImages[Result] = AImage then
      Exit;
  Result := -1;
end;

function TdxImageCache.GetImage(ACrc32: Integer): TdxOfficeImage;
begin
  Result := nil;
  if not FImages.TryGetValue(ACrc32, Result) then
    Exit;
  if Result = nil then
  begin
    FImages.Remove(ACrc32);
    Exit;
  end;
end;

procedure TdxImageCache.Clear;
var
  AKey: Integer;
  AImage: TdxOfficeImage;
begin
  for AKey in FImages.Keys do
  begin
    AImage := FImages[AKey];
    AImage.Free;
  end;
  FImages.Clear;
end;

{ TdxMetafileHelper }

class function TdxMetafileHelper.CreateMetafile(AStream: TMemoryStream;
  AMapMode: TdxMapMode; APictureWidth, APictureHeight: Integer): TdxOfficeImage;
var
  AHandle: THandle;
  AMfp: TMetafilePict;
  AMetafile: TMetafile;
begin
  AHandle := SetMetaFileBitsEx(AStream.Size, AStream.Memory);
  AMetafile := TryCreateMetafile(AHandle);
  try
    if AMetafile = nil then
    begin
      TdxMetafileHelper.DeleteMetafileHandle(AHandle);
      AHandle := SetEnhMetaFileBits(AStream.Size, AStream.Memory);
      AMetafile := TryCreateMetafile(AHandle);
      if AMetafile = nil then
      begin
        TdxMetafileHelper.DeleteMetafileHandle(AHandle);

        AMfp.mm := Ord(AMapMode);
        AMfp.xExt := APictureWidth;
        AMfp.yExt := APictureHeight;
        AMfp.hMF := 0;

        AHandle := SetWinMetaFileBits(AStream.Size, AStream.Memory, 0, AMfp);
        AMetafile := TryCreateMetafile(AHandle);
        if AMetafile = nil then
        begin
          TdxMetafileHelper.DeleteMetafileHandle(AHandle);
          AMetafile := TMetafile.Create;
          try
            AStream.Position := 0;
            AMetafile.LoadFromStream(AStream);
          except
            FreeAndNil(AMetafile);
          end;
        end
        else
          AMetafile.Enhanced := False;
      end;
    end;
    if AMetafile <> nil then
    begin
      Result := TdxOfficeImage.Create;
      Result.AssignFromGraphic(AMetafile);
    end
    else
      Result := nil;
  finally
    AMetafile.Free;
  end;
end;

class procedure TdxMetafileHelper.DeleteMetafileHandle(AHandle: THandle);
begin
  if not DeleteEnhMetaFile(AHandle) then
    DeleteMetaFile(AHandle);
end;

class function TdxMetafileHelper.TryCreateMetafile(AHandle: THandle): TMetafile;
begin
  if AHandle = 0 then
    Result := nil
  else
    Result := TryCreateMetafileCore(AHandle);
end;

class function TdxMetafileHelper.TryCreateMetafileCore(
  AHandle: THandle): TMetafile;
begin
  Result := TMetafile.Create;
  try
    Result.Handle := AHandle;
  except
    FreeAndNil(Result);
  end;
end;

end.
