inherited dxRichEditFontDialogForm: TdxRichEditFontDialogForm
  Caption = 'Font'
  ClientHeight = 242
  ClientWidth = 416
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 416
    Height = 242
    Align = alClient
    TabOrder = 0
    object cmbFontName: TcxFontNameComboBox
      Left = 10
      Top = 28
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Width = 191
    end
    object cmbFontStyle: TcxComboBox
      Left = 207
      Top = 28
      Properties.DropDownListStyle = lsFixedList
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 1
      Width = 106
    end
    object cmbFontSize: TcxComboBox
      Left = 319
      Top = 28
      Properties.Items.Strings = (
        '8'
        '9'
        '10'
        '11'
        '12'
        '14'
        '16'
        '18'
        '20'
        '22'
        '24'
        '26'
        '28'
        '36'
        '48'
        '72')
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 2
      Width = 75
    end
    object cmbFontColor: TcxColorComboBox
      Left = 10
      Top = 73
      Properties.AllowSelectColor = True
      Properties.ColorDialogShowFull = True
      Properties.ColorDialogType = cxcdtAdvanced
      Properties.CustomColors = <>
      Properties.DefaultColorStyle = cxdcClear
      Properties.DropDownSizeable = True
      Properties.MaxMRUColors = 0
      Properties.OnNamingConvention = cmbColorPropertiesNamingConvention
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 3
      Width = 136
    end
    object cmbUnderlineStyle: TcxComboBox
      Left = 152
      Top = 73
      Properties.DropDownListStyle = lsFixedList
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 4
      Width = 106
    end
    object cmbUnderlineColor: TcxColorComboBox
      Left = 264
      Top = 73
      Properties.AllowSelectColor = True
      Properties.ColorDialogShowFull = True
      Properties.ColorDialogType = cxcdtAdvanced
      Properties.CustomColors = <>
      Properties.DefaultColorStyle = cxdcClear
      Properties.DropDownSizeable = True
      Properties.MaxMRUColors = 0
      Properties.OnNamingConvention = cmbColorPropertiesNamingConvention
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 5
      Width = 136
    end
    object lblEffects: TcxLabel
      Left = 10
      Top = 100
      AutoSize = False
      Caption = 'Effects'
      Style.HotTrack = False
      Properties.LineOptions.Visible = True
      Transparent = True
      Height = 17
      Width = 396
    end
    object cbStrikeout: TcxCheckBox
      Left = 20
      Top = 123
      Action = aStrikeout
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 7
      Transparent = True
      Width = 143
    end
    object cbDoubleStrikeout: TcxCheckBox
      Left = 20
      Top = 146
      Action = aDoubleStrikeout
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 8
      Transparent = True
      Width = 143
    end
    object cbUnderlineWordsOnly: TcxCheckBox
      Left = 20
      Top = 169
      Action = aUnderlineWordsOnly
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 9
      Transparent = True
      Width = 143
    end
    object cbSuperscript: TcxCheckBox
      Left = 172
      Top = 123
      Action = aSuperscript
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 10
      Transparent = True
      Width = 107
    end
    object cbSubscript: TcxCheckBox
      Left = 172
      Top = 146
      Action = aSubscript
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 11
      Transparent = True
      Width = 107
    end
    object cbAllCaps: TcxCheckBox
      Left = 285
      Top = 123
      Action = aAllCaps
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 12
      Transparent = True
      Width = 121
    end
    object cbHidden: TcxCheckBox
      Left = 285
      Top = 146
      Action = aHidden
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 13
      Transparent = True
      Width = 121
    end
    object btnOk: TcxButton
      Left = 250
      Top = 207
      Width = 75
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 14
    end
    object btnCancel: TcxButton
      Left = 331
      Top = 207
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 15
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object lcMainGroup_Root: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group1: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignHorz = ahLeft
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object lciFontName: TdxLayoutItem
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = 'Font:'
      CaptionOptions.Layout = clTop
      Control = cmbFontName
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciFontStyle: TdxLayoutItem
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = 'Font style:'
      CaptionOptions.Layout = clTop
      Control = cmbFontStyle
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciFontSize: TdxLayoutItem
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = 'Size:'
      CaptionOptions.Layout = clTop
      Control = cmbFontSize
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Group3: TdxLayoutGroup
      Parent = lcMainGroup_Root
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object lciFontColor: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      CaptionOptions.Text = 'Font Color:'
      CaptionOptions.Layout = clTop
      Control = cmbFontColor
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciUnderlineStyle: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      CaptionOptions.Text = 'Underline style:'
      CaptionOptions.Layout = clTop
      Control = cmbUnderlineStyle
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciUnderlineColor: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      CaptionOptions.Text = 'Underline color:'
      CaptionOptions.Layout = clTop
      Control = cmbUnderlineColor
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item1: TdxLayoutItem
      Parent = lcMainGroup_Root
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblEffects
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Group4: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 3
    end
    object dxLayoutControl1Group5: TdxLayoutGroup
      Parent = dxLayoutControl1Group4
      CaptionOptions.Text = 'New Group'
      Offsets.Left = 10
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item4: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cbStrikeout
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item5: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      CaptionOptions.Text = 'cxCheckBox2'
      CaptionOptions.Visible = False
      Control = cbDoubleStrikeout
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item6: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      CaptionOptions.Text = 'cxCheckBox3'
      CaptionOptions.Visible = False
      Control = cbUnderlineWordsOnly
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Group6: TdxLayoutGroup
      Parent = dxLayoutControl1Group4
      AlignHorz = ahRight
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group7: TdxLayoutGroup
      Parent = dxLayoutControl1Group4
      AlignHorz = ahRight
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item7: TdxLayoutItem
      Parent = dxLayoutControl1Group6
      CaptionOptions.Text = 'cxCheckBox4'
      CaptionOptions.Visible = False
      Control = cbSuperscript
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item8: TdxLayoutItem
      Parent = dxLayoutControl1Group6
      CaptionOptions.Text = 'cxCheckBox5'
      CaptionOptions.Visible = False
      Control = cbSubscript
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item9: TdxLayoutItem
      Parent = dxLayoutControl1Group7
      CaptionOptions.Text = 'cxCheckBox6'
      CaptionOptions.Visible = False
      Control = cbAllCaps
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item10: TdxLayoutItem
      Parent = dxLayoutControl1Group7
      CaptionOptions.Text = 'cxCheckBox7'
      CaptionOptions.Visible = False
      Control = cbHidden
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item2: TdxLayoutItem
      Parent = dxLayoutControl1Group2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Button1'
      CaptionOptions.Visible = False
      Control = btnOk
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item3: TdxLayoutItem
      Parent = dxLayoutControl1Group2
      CaptionOptions.Text = 'Button2'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group2: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahRight
      LayoutDirection = ldHorizontal
      Index = 1
      AutoCreated = True
    end
  end
  object alActions: TActionList
    Left = 384
    object aStrikeout: TAction
      AutoCheck = True
      Caption = 'Stri&kethrough'
      GroupIndex = 1
      OnExecute = aEffectsExecute
    end
    object aDoubleStrikeout: TAction
      Tag = 1
      AutoCheck = True
      Caption = 'Double strikethrou&gh'
      GroupIndex = 1
      OnExecute = aEffectsExecute
    end
    object aUnderlineWordsOnly: TAction
      Caption = '&Underline words only'
      OnExecute = aEffectsExecute
    end
    object aSuperscript: TAction
      AutoCheck = True
      Caption = 'Su&perscript'
      GroupIndex = 2
      OnExecute = aEffectsExecute
    end
    object aSubscript: TAction
      Tag = 1
      AutoCheck = True
      Caption = 'Su&bscript'
      GroupIndex = 2
      OnExecute = aEffectsExecute
    end
    object aAllCaps: TAction
      Caption = '&All caps'
      OnExecute = aEffectsExecute
    end
    object aHidden: TAction
      Caption = '&Hidden'
      OnExecute = aEffectsExecute
    end
  end
end
