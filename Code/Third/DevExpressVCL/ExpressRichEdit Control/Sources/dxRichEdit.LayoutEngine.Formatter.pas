{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.LayoutEngine.Formatter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Contnrs, Graphics, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.LayoutEngine.BoxMeasurer, dxCoreClasses,
  dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.UnitToLayoutUnitConverter, dxRichEdit.DocumentModel.TabFormatting, dxCore, dxRichEdit.DocumentModel.Section,
  Generics.Collections, dxRichEdit.DocumentModel.Core, dxRichEdit.Control.HitTest, Generics.Defaults, dxRichEdit.Utils.Types,
  dxRichEdit.DocumentModel.TableCalculator, dxRichEdit.Options, dxRichEdit.DocumentModel.Numbering,
  dxRichEdit.Utils.SortedList, dxRichEdit.DocumentModel.Borders;

type
  TdxCharacterFormatterState = (
    Start,
    Spaces,
    Tab,
    FirstDash,
    Dash,
    Separator,
    LineBreak,
    PageBreak,
    ColumnBreak,
    Text,
    InlineObject,
    LayoutDependentText,
    FloatingObject,
    Final
  );

  TdxParagraphBoxFormatterState = ( 
    ParagraphStart,
    ParagraphStartFromTheMiddle,
    ParagraphStartAfterFloatingObject,
    ParagraphStartFromTheMiddleAfterFloatingObject,
    RowEmpty,
    RowEmptyAfterFloatingObject,
    RowWithSpacesOnly,
    RowWithTextOnly,
    RowWithTextOnlyAfterFloatingObject,
    RowWithDashOnly,
    RowWithDashOnlyAfterFloatingObject,
    RowWithTextOnlyAfterFirstLeadingTab,
    RowWithTextOnlyAfterFirstLeadingTabAfterFloatingObject,
    RowSpaces,
    RowText,
    RowTextAfterFloatingObject,
    RowDashAfterText,
    RowDash,
    RowDashAfterFloatingObject,
    RowWithDashAfterTextOnly,
    RowTextSplit,
    RowTextSplitAfterFloatingObject,
    RowTextSplitFloatingObject,
    RowTextSplitAfterFirstLeadingTab,
    RowTextSplitAfterFirstLeadingTabAfterFloatingObject,

    RowDashSplit,
    RowDashSplitAfterFloatingObject,

    RowEmptyHyphenation,
    RowEmptyHyphenationAfterFloatingObject,
    RowTextHyphenationFirstSyllable,
    RowTextHyphenationFirstSyllableAfterFloatingObject,
    RowTextHyphenationFirstSyllableAfterFirstLeadingTab,
    RowTextHyphenationFirstSyllableAfterFirstLeadingTabAfterFloatingObject,
    RowTextHyphenation,
    RowTextHyphenationAfterFloatingObject,
    RowFirstLeadingTab,
    RowLeadingTab,
    RowLeadingTabAfterFloatingObject,
    RowTab,
    RowTabAfterFloatingObject,
    RowLineBreak,
    RowPageBreak,
    RowColumnBreak,

    SectionBreakAfterParagraphMark,
    FloatingObject,


    ContinueFromParagraph,

    Final
  );

  TdxFinalizeHandler = procedure (ABoxInfo: TdxBoxInfo) of object;

  TdxFormattingProcess = (
    ContinueFromParagraph,
    Continue,
    RestartFromTheStartOfRow,
    Finish);

  TdxAddBoxResult = (
    Success,
    HorizontalIntersect,
    LeaveColumnPlain,
    LeaveColumnFirstCellRow,
    IntersectWithFloatObject,
    RestartDueFloatingObject
  );

  TdxSplitBoxResult = (
    Success,
    SuccessSuppressedHorizontalOverfull,
    FailedHorizontalOverfull
  );

  TdxCanFitCurrentRowToColumnResult = (
    RowFitted,
    PlainRowNotFitted,
    FirstCellRowNotFitted,
    TextAreasRecreated,      
    RestartDueFloatingObject 
  );

  TdxAdjustEndPositionResult = (
    Success,
    Failure
  );

  TdxStateContinueFormatResult = (
    Success,
    RestartDueFloatingObject,
    RestartDueOrphanedObjects
  );

  TdxClearInvalidatedContentResult = (
    Restart,
    RestartFromTheStartOfSection,
    NoRestart,
    ClearOnlyTableCellRows
  );

  TdxLayoutUnit = Integer;

  TdxParagraphBoxFormatter = class;
  TdxParagraphBoxIterator = class;
  TdxRowsController = class;
  TdxPageAreaController = class;
  TdxTablesController = class;
  TdxTablesControllerStateBase = class;
  TdxFloatingObjectSizeAndPositionController = class;
  TdxFloatingObjectsLayout = class;

  IColumnController = interface 
  ['{659D53CA-61CD-4C47-B14D-BB243C06C9A5}']
    procedure AddInnerTable(ATableViewInfo: TdxTableViewInfo);
    function CompleteCurrentColumnFormatting(AColumn: TdxColumn): TdxCompleteFormattingResult;
    function CreateRow: TdxRow;
    function GetCurrentPageBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
    function GetCurrentPageClientBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
    function GetMeasurer: TdxBoxMeasurer;
    function GetNextColumn(AColumn: TdxColumn; AKeepFloatingObjects: Boolean): TdxColumn; 
    function GetPageAreaController: TdxPageAreaController;
    function GetPageLastRunIndex: TdxRunIndex;
    function GetPreviousColumn(AColumn: TdxColumn): TdxColumn; 
    function GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean;
    procedure ResetToFirstColumn; 

    property Measurer: TdxBoxMeasurer read GetMeasurer;
    property PageAreaController: TdxPageAreaController read GetPageAreaController;
    property PageLastRunIndex: TdxRunIndex read GetPageLastRunIndex;
    property ShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean read GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn; 
  end;


  { TdxTextArea }

  TdxTextArea = record  
  strict private
  public
    Start: Integer;
    &End: Integer;
    constructor Create(AStart, AEnd: Integer);
    function GetEmpty: TdxTextArea;
    function GetWidth: Integer;

    function IntersectsWith(const AInterval: TdxTextArea): Boolean;
    function IntersectsWithExcludingBounds(const AInterval: TdxTextArea): Boolean;
    class function Union(const AInterval1, AInterval2: TdxTextArea): TdxTextArea; static;
    function Subtract(const AInterval: TdxTextArea): TList<TdxTextArea>;
    function Contains(const AInterval: TdxTextArea): Boolean;
    function ToString: string;

    property Empty: TdxTextArea read GetEmpty;
    property Width: Integer read GetWidth;
  end;

  { TdxColumnList }

  TdxColumnList = class(TdxFastObjectList) 
  private
    function GetItems(Index: Integer): TdxColumn;
  public
    property Items[Index: Integer]: TdxColumn read GetItems; default;
  end;

  { TdxTableViewInfoList }

  TdxTableViewInfoList = class(TdxFastObjectList) 
  private
    function GetItems(Index: Integer): TdxTableViewInfo;
  public
    property Items[Index: Integer]: TdxTableViewInfo read GetItems; default;
  end;

  { TdxTableCellColumn }

  TdxTableCellColumn = class(TdxColumn) 
  private
    FParent: TdxColumn;
    FCell: TdxTableCell;
  protected
    function GetRows: TdxRowCollection; override;
    function GetTopLevelColumn: TdxColumn; override;
  public
    constructor Create(AParent: TdxColumn; ACell: TdxTableCell); 
    function GetOwnRows: TdxRowCollection; override; 
    procedure AddParagraphFrame(AParagraph: TdxParagraph); override; 

    property Parent: TdxColumn read FParent;
    property Cell: TdxTableCell read FCell;
  end;

  { TdxTableCellColumnController }

  TdxTableCellColumnController = class(TcxIUnknownObject, IColumnController) 
  private
    FParent: IColumnController;
    FCurrentParentColumn: TdxColumn;
    FCurrentCellViewInfo: TdxTableCellViewInfo;
    FTop: Integer;
    FLeft: Integer;
    FWidth: Integer;
    FGeneratedColumns: TdxColumnList;
    FGeneratedTableViewInfo: TdxTableViewInfoList;
    FCurrentColumnIndex: Integer;
    FLastCreatedRow: TdxTableCellRow;
    FCurrentCell: TdxTableCell;
    function GetMeasurer: TdxBoxMeasurer;
    function GetParentColumn: TdxColumn;
    function GetLastParentColumn: TdxColumn;
    function GetFirstParentColumn: TdxColumn;
    function GetCurrentTopLevelColumn: TdxColumn;
    function GetViewInfo: TdxTableViewInfo;
  protected
    procedure AddTableViewInfo(ACurrentTableViewInfo: TdxTableViewInfo);
    procedure RemoveTableViewInfo(ATableViewInfo: TdxTableViewInfo);
    function GetCurrentPageBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
    function GetCurrentPageClientBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
    function GetNextColumn(AColumn: TdxColumn; AKeepFloatingObjects: Boolean): TdxColumn; 
    function GetPageAreaController: TdxPageAreaController;
    function GetPageLastRunIndex: TdxRunIndex;
    function GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean;
    procedure SetCurrentTableCellViewInfo(AValue: TdxTableCellViewInfo);

    property ParentColumn: TdxColumn read GetParentColumn;
    property LastParentColumn: TdxColumn read GetLastParentColumn;
    property FirstParentColumn: TdxColumn read GetFirstParentColumn;
    property CurrentTopLevelColumn: TdxColumn read GetCurrentTopLevelColumn;
    property ViewInfo: TdxTableViewInfo read GetViewInfo;
  public
    constructor Create(AParent: IColumnController; ACurrentParentColumn: TdxColumn; ALeft, ATop, AWidth: Integer; ATableViewInfo: TdxTableViewInfo; ACurrentCell: TdxTableCell); 
    destructor Destroy; override;

    function CompleteCurrentColumnFormatting(AColumn: TdxColumn): TdxCompleteFormattingResult;
    procedure ResetToFirstColumn; 
    procedure MoveToNextColumn; 
    function GetStartColumn: TdxColumn;
    procedure StartNewCell(ACurrentParentColumn: TdxColumn; ALeft, ATop, AWidth: Integer; ACurrentCell: TdxTableCell); 
    function GetMaxAnchor(AAnchor1, AAnchor2: TdxTableCellVerticalAnchor): TdxTableCellVerticalAnchor;
    procedure SetCurrentParentColumn(AColumn: TdxColumn); 
    function CreateRow: TdxRow; 
    procedure AddInnerTable(ATableViewInfo: TdxTableViewInfo);
    procedure RemoveGeneratedColumn(AColumn: TdxColumn);
    function GetPreviousColumn(AColumn: TdxColumn): TdxColumn; 

    property Parent: IColumnController read FParent;
    property Measurer: TdxBoxMeasurer read GetMeasurer;
    property PageAreaController: TdxPageAreaController read GetPageAreaController;
    property ShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean read GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn; 
    property CurrentCell: TdxTableCellViewInfo read FCurrentCellViewInfo;
  end;

  { TdxFormattingProcessResult }

  TdxFormattingProcessResult = record 
    RestartPosition: TdxDocumentModelPosition;
    FormattingProcess: TdxFormattingProcess;
    ParagraphIndex: TdxParagraphIndex;
    procedure Init(AFormattingProcess: TdxFormattingProcess); overload; 
    procedure Init(AParagraphIndex: TdxParagraphIndex); overload; 
    procedure Init(const ARestartPosition: TdxDocumentModelPosition); overload; 
  end;

  { TdxParagraphBoxFormatterTableStateBase }

  TdxParagraphBoxFormatterTableStateBase = class abstract 
  private
    FFormatter: TdxParagraphBoxFormatter; 
  protected
    procedure OnColumnOverfull; virtual; abstract; 

    property Formatter: TdxParagraphBoxFormatter read FFormatter; 
  public
    constructor Create(AFormatter: TdxParagraphBoxFormatter); 
  end;

  { TdxParagraphBoxFormatterTextState }

  TdxParagraphBoxFormatterTextState = class(TdxParagraphBoxFormatterTableStateBase) 
  protected
    procedure OnColumnOverfull; override; 
  end;

  { TdxParagraphBoxFormatterTableState }

  TdxParagraphBoxFormatterTableState = class(TdxParagraphBoxFormatterTableStateBase) 
  protected
    procedure OnColumnOverfull; override; 
  end;

  TdxParagraphBoxFormatterTableStateBaseStack = class(TObjectStack<TdxParagraphBoxFormatterTableStateBase>);

  IHyphenationService = interface
  ['{CAC07938-101A-4138-AC21-C2AE7616D4D5}']
    function Hyphenize(const AOriginalWord: string): TList<Integer>;
  end;

  { TdxBoxInfoList }

  TdxBoxInfoList = class(TdxFastObjectList) 
  private
    function GetItem(Index: Integer): TdxBoxInfo;
  public
    property Items[Index: Integer]: TdxBoxInfo read GetItem; default;
  end;

  TdxSeparatorTextRun = class(TdxTextRunBase); 
  TdxFieldResultEndRun = class(TdxTextRunBase); 

  { TdxEmptyTextFilter }

  TdxEmptyTextFilter = class(TdxVisibleTextFilterBase) 
  public
    function Clone(APieceTable: TdxPieceTable): IVisibleTextFilter; override; 
  end;

  { TdxEmptyTextFilterSkipFloatingObjects }

  TdxEmptyTextFilterSkipFloatingObjects = class(TdxEmptyTextFilter) 
  public
    function IsRunVisible(ARunIndex: TdxRunIndex): Boolean; override; 
  end;

  { TdxVisibleTextFilter }

  TdxVisibleTextFilter = class(TdxVisibleTextFilterBase) 
  public
    function Clone(APieceTable: TdxPieceTable): IVisibleTextFilter; override; 
    function IsRunVisible(ARunIndex: TdxRunIndex): Boolean; override; 
    function IsRunVisibleCore(ARunIndex: TdxRunIndex): TdxRunVisibility; override; 
  end;

  { TdxVisibleTextFilterSkipFloatingObjects }

  TdxVisibleTextFilterSkipFloatingObjects = class(TdxVisibleTextFilter) 
  public
    function IsRunVisible(ARunIndex: TdxRunIndex): Boolean; override; 
  end;

  { TdxVisibleOnlyTextFilter }

  TdxVisibleOnlyTextFilter = class(TdxVisibleTextFilterBase) 
  public
    function Clone(APieceTable: TdxPieceTable): IVisibleTextFilter; override; 
    function IsRunVisible(ARunIndex: TdxRunIndex): Boolean; override; 
    function IsRunVisibleCore(ARunIndex: TdxRunIndex): TdxRunVisibility; override; 
  end;

  { TdxParagraphIteratorBase }

  TdxParagraphIteratorBase = class 
  private
    FPieceTable: TdxPieceTable;
    FParagraph: TdxParagraph;
    FVisibleTextFilter: TdxVisibleTextFilterBase;
    FMaxOffset: Integer;
    FRunStartIndex: TdxRunIndex;
    FEndRunIndex: TdxRunIndex;
    FPosition: TdxFormatterPosition;
  protected
    function GetIsEnd: Boolean; virtual;
    function GetCurrentChar: Char; virtual;

    property Table: TdxPieceTable read FPieceTable;
    property PieceTable: TdxPieceTable read FPieceTable;
  public
    constructor Create(AParagraph: TdxParagraph; APieceTable: TdxPieceTable; AVisibleTextFilter: TdxVisibleTextFilterBase); 
    function CreatePosition: TdxFormatterPosition; virtual; abstract; 
    function IsParagraphMarkRun: Boolean; virtual; 
    function IsParagraphFrame: Boolean; virtual; 
    function IsFloatingObjectAnchorRun: Boolean; 
    procedure SetRunIndexCore(ARunIndex: TdxRunIndex); 
    procedure SetPosition(APos: TdxFormatterPosition); overload; virtual; 
    procedure SetPositionCore(APos: TdxFormatterPosition); 
    procedure SetPosition(ARunIndex: TdxRunIndex; AOffset: Integer); overload; virtual; 
    function Next: TdxParagraphIteratorResult; virtual; 
    procedure NextOffset; virtual; 
    procedure NextRun; virtual; 
    function GetCurrentPosition: TdxFormatterPosition; virtual; 
    function GetPreviousPosition: TdxFormatterPosition; virtual; 
    function GetPreviousVisibleRunPosition: TdxFormatterPosition; virtual; 
    function GetPreviousOffsetPosition: TdxFormatterPosition; virtual; 

    property CurrentChar: Char read GetCurrentChar;
    property IsEnd: Boolean read GetIsEnd;
    property Offset: Integer read FPosition.Offset;
    property Paragraph: TdxParagraph read FParagraph;
    property RunIndex: TdxRunIndex read FPosition.RunIndex;
    property VisibleTextFilter: TdxVisibleTextFilterBase read FVisibleTextFilter;
  end;

  { TdxParagraphCharacterIterator }

  TdxParagraphCharacterIterator = class(TdxParagraphIteratorBase)  
  public
    function CreatePosition: TdxFormatterPosition; override; 
  end;

  TdxIteratorClass = class of TdxParagraphIteratorBase;

  TdxTypeEnum = Integer;

  TdxParagraphCharacterFormatter = class;

  { TdxRichEditFormattingProcessResult }

  TdxRichEditFormattingProcessResult = record
    FormattingProcess: TdxFormattingProcess;
    ParagraphIndex: TdxParagraphIndex;
    RestartPosition: TdxDocumentModelPosition;
    procedure Init(AFormattingProcess: TdxFormattingProcess); overload;
    procedure Init(AParagraphIndex: TdxParagraphIndex); overload;
    procedure Init(const ARestartPosition: TdxDocumentModelPosition); overload;
  end;

  { TdxFormatterStateBase }

  TdxFormatterStateBase = class(TcxIUnknownObject)
  protected
    function GetIteratorClass: TdxIteratorClass; virtual; abstract;

    function GetMeasurer: TdxBoxMeasurer; virtual; abstract;
    function GetFormattingComplete: Boolean; virtual; abstract;
    function GetIterator: TdxParagraphIteratorBase; virtual; abstract;

    function IsTerminatorChar(ACh: Char): Boolean; virtual; abstract;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; virtual; abstract; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); virtual; abstract; 
    function FinishParagraph: TdxCompleteFormattingResult; virtual; abstract; 
    function FinishSection: TdxCompleteFormattingResult; virtual; abstract; 

    procedure FinishParagraphCore(AIterator: TdxParagraphIteratorBase); virtual; abstract; 

    function ContinueFormatByCharacter(var ABoxInfo: TdxBoxInfo; ALayoutDependentTextBox: TdxLayoutDependentTextBox): TdxStateContinueFormatResult; 
  public
    property Measurer: TdxBoxMeasurer read GetMeasurer;
    property FormattingComplete: Boolean read GetFormattingComplete;
    property Iterator: TdxParagraphIteratorBase read GetIterator;
  end;


  { TdxCharacterFormatterStateBase }

  TdxCharacterFormatterStateBase = class(TdxFormatterStateBase)
  private
    FFormatter: TdxParagraphCharacterFormatter;
  protected
    function GetDashState: TdxCharacterFormatterState; virtual; 
    function GetIteratorClass: TdxIteratorClass; override;
    function GetIterator: TdxParagraphIteratorBase; override;
    function GetFormattingComplete: Boolean; override;
    function GetMeasurer: TdxBoxMeasurer; override;
    function GetType: TdxCharacterFormatterState; virtual; abstract; 
    function ContinueFormat: Boolean; virtual; 
    procedure FinishParagraphCore(AIterator: TdxParagraphIteratorBase); override; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    procedure ChangeState(AStateType: TdxCharacterFormatterState); virtual; 

    function GetParagraph: TdxParagraph; virtual;
    function GetCharacterFormatterState: TdxCharacterFormatterState;
  public
    constructor Create(AFormatter: TdxParagraphCharacterFormatter); 

    procedure AppendBoxCore(AType: TdxBoxClass; var ABoxInfo: TdxBoxInfo); 
    procedure SwitchToNextState; 
    procedure AddBox(AType: TdxBoxClass; var ABoxInfo: TdxBoxInfo; AMeasured: Boolean); 
    function CreateParagraphMarkBoxInfo: TdxBoxInfo; 
    function CreateSectionMarkBoxInfo: TdxBoxInfo; 
    function GetNextState: TdxCharacterFormatterState; virtual; 
    function IsParagraphMarkRun(ARun: TdxTextRunBase): Boolean;
    function IsInlineObjectRun(ARun: TdxTextRunBase): Boolean;
    function IsSeparatorRun(ARun: TdxTextRunBase): Boolean;
    function IsFloatingObjectRun(ARun: TdxTextRunBase): Boolean;
    function IsLayoutDependentTextRun(ARun: TdxTextRunBase): Boolean;

    property DashState: TdxCharacterFormatterState read GetDashState;
    property Paragraph: TdxParagraph read GetParagraph;
    property State: TdxCharacterFormatterState read GetCharacterFormatterState;
    property &Type: TdxCharacterFormatterState read GetType;
    property Formatter: TdxParagraphCharacterFormatter read FFormatter;
  end;
  TdxCharacterFormatterStateBaseClass = class of TdxCharacterFormatterStateBase;

  { TdxCharacterFormatterStartState }

  TdxCharacterFormatterStartState = class(TdxCharacterFormatterStateBase)
  protected
    function ContinueFormat: Boolean; override; 
    function GetType: TdxCharacterFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterSpacesState }

  TdxCharacterFormatterSpacesState = class(TdxCharacterFormatterStateBase)
  protected
    function GetType: TdxCharacterFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
  end;

  { TdxCharacterFormatterTextState }

  TdxCharacterFormatterTextState = class(TdxCharacterFormatterStateBase)
  protected
    function GetType: TdxCharacterFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
  end;

  { TdxCharacterFormatterDashState }

  TdxCharacterFormatterDashState = class(TdxCharacterFormatterStateBase)
  protected
    function GetType: TdxCharacterFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
  end;

  { TdxCharacterFormatterFirstDashState }

  TdxCharacterFormatterFirstDashState = class(TdxCharacterFormatterDashState)
  protected
    function GetType: TdxCharacterFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function GetDashState: TdxCharacterFormatterState; override;
  end;

  { TdxCharacterFormatterLayoutDependentTextState }

  TdxCharacterFormatterLayoutDependentTextState = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterInlineObjectState }

  TdxCharacterFormatterInlineObjectState = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterSeparatorState }

  TdxCharacterFormatterSeparatorState = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterFloatingObjectState }

  TdxCharacterFormatterFloatingObjectState = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterTabState }

  TdxCharacterFormatterTabState = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterLineBreak }

  TdxCharacterFormatterLineBreak = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterPageBreak }

  TdxCharacterFormatterPageBreak = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterColumnBreak }

  TdxCharacterFormatterColumnBreak = class(TdxCharacterFormatterStateBase) 
  protected
    function GetType: TdxCharacterFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;

  { TdxCharacterFormatterFinalState }

  TdxCharacterFormatterFinalState = class(TdxCharacterFormatterStateBase) 
  protected
    function GetFormattingComplete: Boolean; override; 
    function GetType: TdxCharacterFormatterState; override;
    function ContinueFormat: Boolean; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
  end;


  { TdxBoxFormatterStateBase }

  TdxBoxFormatterStateBase = class(TdxFormatterStateBase) 
  private
    FFormatter: TdxParagraphBoxFormatter;
    FPreviousState: TdxBoxFormatterStateBase;
    function GetCurrentRow: TdxRow;
    function GetForceFormattingComplete: Boolean;
    procedure SetForceFormattingComplete(const Value: Boolean);
    function GetRowsController: TdxRowsController;
  protected
    function CanUseBox: Boolean; virtual;

    function FinishParagraphOrSecton: TdxCompleteFormattingResult; virtual; 
    procedure FinishParagraphCore(AIterator: TdxParagraphIteratorBase); override; 
    function GetIterator: TdxParagraphIteratorBase; override;
    function GetIterator_: TdxParagraphBoxIterator; 
    function GetFormattingComplete: Boolean; override;
    function GetMeasurer: TdxBoxMeasurer; override;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; virtual; 
    function GetType: TdxParagraphBoxFormatterState; virtual; abstract; 

    procedure AddSuccess(ABoxInfo: TdxBoxInfo); virtual; abstract; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; virtual; abstract;
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; virtual; abstract;
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; virtual; abstract; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; virtual; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; virtual; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; virtual; 
    function CanAddBoxWithoutHyphen(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; virtual; 
    procedure ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo); virtual; 
    procedure ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo); virtual; 
    procedure ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo); virtual; 
    procedure ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo); virtual; 
    procedure FinalizeParagraph(ABoxInfo: TdxBoxInfo); 
    procedure FinalizeSection(ABoxInfo: TdxBoxInfo); 
    procedure FinalizeLine(ABoxInfo: TdxBoxInfo); 
    function FinalizePage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
    function FinalizeColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
    procedure AddNumberingListBox; virtual; 
    function CanInsertNumberingListBox: Boolean; virtual; 
    function CreateParagraphMarkBoxInfo: TdxBoxInfo; 
    function CreateSectionMarkBoxInfo: TdxBoxInfo; virtual; 
    function CreateLineBreakBoxInfo: TdxBoxInfo; 
    function CreatePageBreakBoxInfo: TdxBoxInfo; 
    function CreateColumnBreakBoxInfo: TdxBoxInfo; 
    function BinarySearchFittedBox(ABoxInfo: TdxBoxInfo): Integer; virtual; 
    procedure SetCurrentRowHeightToLastBoxHeight; 
    procedure EndRow; 
    procedure CalculateAndMeasureLayoutDependentTextBox(ALayoutDependentTextBox: TdxLayoutDependentTextBox; ABoxInfo: TdxBoxInfo); virtual; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
    procedure MeasureBoxContentCore(ABoxInfo: TdxBoxInfo); 
    procedure ChangeState(AStateType: TdxParagraphBoxFormatterState); virtual; 
    procedure AddTextBox(ABoxInfo: TdxBoxInfo); 
    function GetAddBoxResultColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult): TdxAddBoxResult; virtual; 
    function CanAddBoxCore(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; 
    function CanAddBoxWithHyphenCore(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; 
    function GetContinueFormatResult(AResult: TdxCompleteFormattingResult): TdxStateContinueFormatResult; 
    function SplitBox(ABoxInfo: TdxBoxInfo): TdxSplitBoxResult; 
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
  public
    constructor Create(AFormatter: TdxParagraphBoxFormatter); 
    destructor Destroy; override;

    function ContinueFormat: TdxStateContinueFormatResult; virtual; 
    function AddExistedBox(ABoxInfo: TdxBoxInfo; AIterator: TdxParagraphBoxIterator): TdxStateContinueFormatResult; 
    function ObtainAddBoxResult(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; 
    function DispatchAddBoxResult(ABoxInfo: TdxBoxInfo; AResult: TdxAddBoxResult): TdxStateContinueFormatResult;
    function GetCanFitCurrentRowToColumn(AAddBoxResult: TdxAddBoxResult): TdxCanFitCurrentRowToColumnResult; 
    function CalcBoxSizeWithHyphen(ABoxInfo: TdxBoxInfo): TSize; 
    function IsCurrentRowEmpty: Boolean; 
    function AdjustEndPositionToFit(ABoxInfo: TdxBoxInfo): TdxAdjustEndPositionResult; 

    property Formatter: TdxParagraphBoxFormatter read FFormatter;
    property CurrentRow: TdxRow read GetCurrentRow;
    property ForceFormattingComplete: Boolean read GetForceFormattingComplete write SetForceFormattingComplete;
    property RowsController: TdxRowsController read GetRowsController;
    property StateAfterFloatingObject: TdxParagraphBoxFormatterState read GetStateAfterFloatingObject;
    property &Type: TdxParagraphBoxFormatterState read GetType;
    property Iterator: TdxParagraphBoxIterator read GetIterator_;
    property PreviousState: TdxBoxFormatterStateBase read FPreviousState write FPreviousState;
  end;
  TdxBoxFormatterStateBaseClass = class of TdxBoxFormatterStateBase;

  { ISupportsChangeStateManually }

  ISupportsChangeStateManually = interface
  ['{F5605619-439A-4000-9907-D3F8D025E241}']
    procedure ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult); 
  end;

  { TdxStateRowTextSplit }

  TdxStateRowTextSplit = class(TdxBoxFormatterStateBase, ISupportsChangeStateManually) 
  protected
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;  
    function GetType: TdxParagraphBoxFormatterState; override;
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function HandleUnsuccessfulSplitting: TdxCompleteFormattingResult; virtual; 
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function ShouldChangeStateManually(AIteratorResult: TdxParagraphIteratorResult; ACurrentCharacter: Char): Boolean; 
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; virtual;
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
  public
    procedure ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult); 
    function NextSplitState: TdxParagraphBoxFormatterState; virtual;
  end;

  { TdxStateRowTextSplitAfterFloatingObject }

  TdxStateRowTextSplitAfterFloatingObject = class(TdxStateRowTextSplit)
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateRowDashSplit }

  TdxStateRowDashSplit = class(TdxStateRowTextSplit)
  protected
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; override;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;  
    function GetType: TdxParagraphBoxFormatterState; override;
    function HandleUnsuccessfulSplitting: TdxCompleteFormattingResult; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
  public
    function NextSplitState: TdxParagraphBoxFormatterState; override;
  end;

  { TdxStateRowDashSplitAfterFloatingObject }

  TdxStateRowDashSplitAfterFloatingObject = class(TdxStateRowDashSplit)
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateRowEmptyHyphenation }

  TdxStateRowEmptyHyphenation = class(TdxBoxFormatterStateBase) 
  private
    FSwitchToRowEmpty: Boolean;
  protected
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function IsHyphenationOfWordComplete: Boolean; 
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function CanAddBoxWithoutHyphen(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function GetIterator: TdxParagraphIteratorBase; override;
    function GetNextState: TdxParagraphBoxFormatterState; virtual;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override; 
    function GetType: TdxParagraphBoxFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
  public
    function CalcFinalState(ACurrentChar: Char): TdxParagraphBoxFormatterState; 

    property NextState: TdxParagraphBoxFormatterState read GetNextState;
  end;

  { TdxStateRowEmptyHyphenationAfterFloatingObject }

  TdxStateRowEmptyHyphenationAfterFloatingObject = class(TdxStateRowEmptyHyphenation)
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateRowTextHyphenation }

  TdxStateRowTextHyphenation = class(TdxStateRowEmptyHyphenation) 
  protected
    function GetNextState: TdxParagraphBoxFormatterState; override; 
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override; 
    function GetType: TdxParagraphBoxFormatterState; override; 
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function CanAddBoxWithoutHyphen(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    procedure InsertHyphenBox; 
    function CreateHyphenBoxInfo: TdxBoxInfo; 
  end;

  { TdxStateRowWithTextOnlyBase }

  TdxStateRowWithTextOnlyBase = class(TdxBoxFormatterStateBase) 
  protected
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; virtual;
    function GetHyphenationState: TdxParagraphBoxFormatterState; virtual; abstract;
    function GetNoHyphenationNextState: TdxParagraphBoxFormatterState; virtual; abstract;
    function CanUseBox: Boolean; override;
    function DashAfterTextState: TdxParagraphBoxFormatterState; virtual;  
    function IsTerminatorChar(ACh: Char): Boolean; override;
  public
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function ShouldChangeStateManually(ACurrentCharacter: Char): Boolean; 
    procedure ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult); 

    property HyphenationState: TdxParagraphBoxFormatterState read GetHyphenationState;
    property NoHyphenationNextState: TdxParagraphBoxFormatterState read GetNoHyphenationNextState;
  end;

  { TdxStateRowWithTextOnly }

  TdxStateRowWithTextOnly = class(TdxStateRowWithTextOnlyBase) 
  protected
    function GetHyphenationState: TdxParagraphBoxFormatterState; override;
    function GetNoHyphenationNextState: TdxParagraphBoxFormatterState; override;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function DashAfterTextState: TdxParagraphBoxFormatterState; override;
  end;

  { TdxStateRowText }

  TdxStateRowText = class(TdxStateRowWithTextOnly) 
  protected
    function DashAfterTextState: TdxParagraphBoxFormatterState; override;
    function GetHyphenationState: TdxParagraphBoxFormatterState; override;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
  public
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function WrapLine: TdxCompleteFormattingResult;
  end;

  { TdxStateRowDashAfterText }

  TdxStateRowDashAfterText = class(TdxStateRowText)
  protected
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
  end;

  { TdxStateRowWithDashAfterTextOnly }

  TdxStateRowWithDashAfterTextOnly = class(TdxStateRowWithTextOnly)
  protected
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
  public
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
  end;

  { TdxStateRowDash }

  TdxStateRowDash = class(TdxStateRowText)
  protected
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; override;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
  public
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
  end;

  { TdxStateRowWithDashOnly }

  TdxStateRowWithDashOnly = class(TdxStateRowWithTextOnly)
  protected
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; override;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
  public
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
  end;

  { TdxStateRowSpaces }

  TdxStateRowSpaces = class(TdxBoxFormatterStateBase) 
  protected
    function CanUseBox: Boolean; override;
    function GetType: TdxParagraphBoxFormatterState; override;
  public
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; virtual; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    procedure ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    function ShouldChangeStateManually(ACurrentCharacter: Char): Boolean; 
    procedure ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult); 
  end;

  { TdxStateRowWithSpacesOnly }

  TdxStateRowWithSpacesOnly = class(TdxStateRowSpaces)
  strict private type
    TFinalizeHandler = procedure (ABoxInfo: TdxBoxInfo) of object;
    function InternalFinishParagraphCore(ABoxInfo: TdxBoxInfo; AFinalizeHandler: TFinalizeHandler): TdxCompleteFormattingResult;
  protected
    function GetType: TdxParagraphBoxFormatterState; override;
  public
    procedure ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo); override;
    procedure ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo); override;
    procedure ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo); override;
    procedure ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo); override;
    function CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState; override;
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
    function FinishParagraph: TdxCompleteFormattingResult; override;
    function FinishSection: TdxCompleteFormattingResult; override;
  end;

  { TdxStateRowEmptyBase }

  TdxStateRowEmptyBase = class(TdxBoxFormatterStateBase) 
  strict private type
    TFinalizeHandler = procedure (ABoxInfo: TdxBoxInfo) of object;
    function InternalFinishParagraphCore(ABoxInfo: TdxBoxInfo; AFinalizeHandler: TFinalizeHandler): TdxCompleteFormattingResult;
  protected
    function CanFitCurrentRowToColumn(ABoxInfo: TdxBoxInfo): TdxCanFitCurrentRowToColumnResult; virtual; 
    procedure ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
  public
    function ContinueFormat: TdxStateContinueFormatResult; override; 
  end;

  { TdxStateRowEmpty }

  TdxStateRowEmpty = class(TdxStateRowEmptyBase) 
  protected
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
  end;

  { TdxStateRowEmptyAfterFloatingObject }

  TdxStateRowEmptyAfterFloatingObject = class(TdxStateRowEmpty)
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateParagraphStart }

  TdxStateParagraphStart = class(TdxStateRowEmpty) 
  protected
    function CanFitCurrentRowToColumn(ABoxInfo: TdxBoxInfo): TdxCanFitCurrentRowToColumnResult; override; 
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    procedure InsertNumberingListBox; virtual; 
  public
    function ContinueFormat: TdxStateContinueFormatResult; override; 
  end;

  { TdxStateParagraphStartAfterFloatingObject }

  TdxStateParagraphStartAfterFloatingObject = class(TdxStateParagraphStart)
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateParagraphStartFromTheMiddle }

  TdxStateParagraphStartFromTheMiddle = class(TdxStateParagraphStart)
  protected
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override; 
    procedure InsertNumberingListBox; override; 
  end;

  { TdxStateParagraphStartFromTheMiddleAfterFloatingObject }

  TdxStateParagraphStartFromTheMiddleAfterFloatingObject = class(TdxStateParagraphStartFromTheMiddle)
  protected
    function GetType: TdxParagraphBoxFormatterState; override;
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
  end;

  { TdxStateRowWithTextOnlyAfterFirstLeadingTab}

  TdxStateRowWithTextOnlyAfterFirstLeadingTab = class(TdxStateRowWithTextOnly) 
  protected
    function GetHyphenationState: TdxParagraphBoxFormatterState; override; 
    function GetNoHyphenationNextState: TdxParagraphBoxFormatterState; override; 
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateRowTabBase }

  TdxStateRowTabBase = class(TdxBoxFormatterStateBase) 
  private
    FCurrentTab: TdxTabInfo;
  protected
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    procedure SwitchToNextState; virtual; abstract; 
    procedure MeasureBoxContent(ABoxInfo: TdxBoxInfo); override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function CanUseBox: Boolean; override; 
    function CurrentTabPosition: Integer;

    property CurrentTab: TdxTabInfo read FCurrentTab write FCurrentTab;
  public
    procedure ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult); 
  end;

  { TdxStateRowTab }

  TdxStateRowTab = class(TdxStateRowTabBase) 
  protected
    procedure SwitchToNextState; override; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
  end;

  { TdxStateRowLeadingTab }

  TdxStateRowLeadingTab = class(TdxStateRowTabBase) 
  strict private type
    TFinalizeHandler = procedure (ABoxInfo: TdxBoxInfo) of object;
    function InternalFinishParagraphCore(ABoxInfo: TdxBoxInfo; AFinalizeHandler: TFinalizeHandler): TdxCompleteFormattingResult;
  protected
    procedure SwitchToNextState; override; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    procedure ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo); override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    procedure ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    procedure ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo); override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateRowFirstLeadingTab }

  TdxStateRowFirstLeadingTab = class(TdxStateRowLeadingTab) 
  protected
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    function GetType: TdxParagraphBoxFormatterState; override; 
    procedure SwitchToNextState; override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
  end;

  { TdxStateRowTextSplitAfterFirstLeadingTab }

  TdxStateRowTextSplitAfterFirstLeadingTab = class(TdxStateRowTextSplit) 
  private
    function CompleteRowProcessing: TdxCompleteFormattingResult; 
  protected
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override; 
    function HandleUnsuccessfulSplitting: TdxCompleteFormattingResult; override;
  end;

  { TdxStateRowTextHyphenationFirstSyllable }

  TdxStateRowTextHyphenationFirstSyllable = class(TdxStateRowEmptyHyphenation) 
  protected
    function GetNextState: TdxParagraphBoxFormatterState; override; 
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
  end;

  { TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab }

  TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab = class(TdxStateRowTextHyphenationFirstSyllable) 
  private
    function CompleteRowProcessing: TdxCompleteFormattingResult;
  protected
    function GetStateAfterFloatingObject: TdxParagraphBoxFormatterState; override;
    function GetType: TdxParagraphBoxFormatterState; override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
  end;

  { TdxStateFloatingObject }

  TdxStateFloatingObject = class(TdxBoxFormatterStateBase) 
  protected
    function CanUseBox: Boolean; override; 
    function GetType: TdxParagraphBoxFormatterState; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    procedure SwitchToNextState; virtual; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishParagraphOrSecton: TdxCompleteFormattingResult; override; 
  public
    function ContinueFormat: TdxStateContinueFormatResult; override; 
  end;

  { TdxFormatterStartEndState }

  TdxFormatterStartEndState = class(TdxBoxFormatterStateBase) 
  public
    procedure AddSuccess(ABoxInfo: TdxBoxInfo); override; 
    function HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function IsTerminatorChar(ACh: Char): Boolean; override;
    function AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult; override; 
    function CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult; override; 
  end;

  { TdxStateContinueFormattingFromParagraph }

  TdxStateContinueFormattingFromParagraph = class(TdxFormatterStartEndState) 
  private
    FParagraphIndex: TdxParagraphIndex;
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  public
    constructor Create(AFormatter: TdxParagraphBoxFormatter; AParagraphIndex: TdxParagraphIndex); 

    property ParagraphIndex: TdxParagraphIndex read FParagraphIndex; 
  end;

  { TdxStateSectionBreakAfterParagraphMark }

  TdxStateSectionBreakAfterParagraphMark = class(TdxFormatterStartEndState) 
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function CreateSectionMarkBoxInfo: TdxBoxInfo; override; 
  public
    function ContinueFormat: TdxStateContinueFormatResult; override; 
  end;

  { TdxStateRowBreakBase }

  TdxStateRowBreakBase = class(TdxFormatterStartEndState)
  public
    function FinishParagraph: TdxCompleteFormattingResult; override;
    function FinishSection: TdxCompleteFormattingResult; override;
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override;
    function IsTerminatorChar(ACh: Char): Boolean; override;
  end;

  { TdxStateRowPageBreak }

  TdxStateRowPageBreak = class(TdxStateRowBreakBase) 
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  public
    function ContinueFormat: TdxStateContinueFormatResult; override; 
  end;

  { TdxStateRowColumnBreak }

  TdxStateRowColumnBreak = class(TdxStateRowBreakBase) 
  public
    function ContinueFormat: TdxStateContinueFormatResult; override; 
    function GetType: TdxParagraphBoxFormatterState; override; 
  end;

  { TdxStateRowLineBreak }

  TdxStateRowLineBreak = class(TdxStateRowBreakBase) 
  protected
    function GetType: TdxParagraphBoxFormatterState; override; 
  public
    function ContinueFormat: TdxStateContinueFormatResult; override; 
  end;

  { TdxStateFinal }

  TdxStateFinal = class(TdxFormatterStartEndState)
  protected
    function GetFormattingComplete: Boolean; override;
    function GetType: TdxParagraphBoxFormatterState; override;
    function FinishParagraph: TdxCompleteFormattingResult; override; 
    function FinishSection: TdxCompleteFormattingResult; override; 
    function FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
    function FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult; override; 
  end;

  { TdxParagraphFormatterBase }

  TdxParagraphFormatterBase<TState: TdxFormatterStateBase> = class
  private
    FPieceTable: TdxPieceTable;
    FMeasurer: TdxBoxMeasurer;
    FIterator: TdxParagraphIteratorBase;
    FState: TState;
    function GetDocumentModel: TdxDocumentModel;
    procedure SetIterator(const Value: TdxParagraphIteratorBase);
  protected
    procedure CreateStates; virtual;
    procedure DestroyStates; virtual;
    function GetIteratorClass: TdxIteratorClass; virtual; abstract;

    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property Measurer: TdxBoxMeasurer read FMeasurer;
    property PieceTable: TdxPieceTable read FPieceTable;
    property State: TState read FState write FState;
  public
    constructor Create(APieceTable: TdxPieceTable; AMeasurer: TdxBoxMeasurer);
    destructor Destroy; override;

    procedure BeginParagraph(ABeginFromParagraphStart: Boolean); virtual; abstract;
    class function GetSpaceBoxTemplate(ABoxInfo: TdxBoxInfo): TdxBoxClass;
    procedure OnNewMeasurementAndDrawingStrategyChanged(AMeasurer: TdxBoxMeasurer); virtual;

    property Iterator: TdxParagraphIteratorBase read FIterator write SetIterator;
  end;

  { TdxParagraphBoxIterator }

  TdxParagraphBoxIterator = class(TdxParagraphIteratorBase) 
  private
    FBoxIndex: Integer;
    function GetCurrentBox: TdxBox;
  public
    function CreatePosition: TdxFormatterPosition; override; 

    procedure NextBox; 
    function Next: TdxParagraphIteratorResult; override; 
    procedure SetPosition(APos: TdxFormatterPosition); override; 
    function GetPreviousVisibleRunPosition: TdxFormatterPosition; override; 
    function GetPreviousOffsetPosition: TdxFormatterPosition; override; 
    function GetCurrentPosition: TdxFormatterPosition; override; 
    procedure NextBoxCore; 
    procedure SetNextPosition(APrevPosition: TdxFormatterPosition); 

    property CurrentBox: TdxBox read GetCurrentBox;
    property BoxIndex: Integer read FBoxIndex write FBoxIndex;
  end;

  { TdxSyllableBoxIterator }

  TdxFormatterPositionCollection = class(TList<TdxFormatterPosition>);

  TdxSyllableBoxIterator = class(TdxParagraphBoxIterator) 
  private
    FIterator: TdxParagraphBoxIterator;
    FHyphenationService: IHyphenationService;
    FHyphenPositions: TdxFormatterPositionCollection;
    FHyphenPos: TdxFormatterPosition;
    FHyphenPosIndex: Integer;
    FHyphenAtCurrentPosition: Boolean;
    FEndPos: TdxFormatterPosition;
    function GetHyphenChar: Char;
  protected
    function GetCurrentChar: Char; override;
    function GetIsEnd: Boolean; override;
  public
    constructor Create(AIterator: TdxParagraphBoxIterator; AHyphenationService: IHyphenationService ); 
    destructor Destroy; override;

    procedure SetInvalidHyphenPos; 
    procedure SetPosition(ARunIndex: TdxRunIndex; AOffset: Integer); override; 
    procedure SetPosition(APos: TdxFormatterPosition); override; 
    function GetCurrentWord: string; 
    procedure HyphenizeCurrentWord; virtual; 
    function IsEndOfWord(ACh: Char): Boolean; 
    function IsBreak(ACh: Char): Boolean; 
    function IsWhiteSpace(ACh: Char): Boolean; 
    function Next: TdxParagraphIteratorResult; override; 

    property Hyphenizer: IHyphenationService read FHyphenationService;
    property Iterator: TdxParagraphBoxIterator read FIterator;
    property HyphenPositions: TdxFormatterPositionCollection read FHyphenPositions;
    property HyphenChar: Char read GetHyphenChar;
  end;

  { TdxParagraphBoxFormatter }

  TdxParagraphBoxFormatter = class(TdxParagraphFormatterBase<TdxBoxFormatterStateBase>)
  var
    InvalidPosition: TdxFormatterPosition;
  private
    FSyllableIterator: TdxParagraphBoxIterator;
    FParagraphStartPos: TdxFormatterPosition;
    FLastRestartDueToFloatingObjectParagraphStartPos: TdxFormatterPosition;
    FRowsController: TdxRowsController;
    FWordStartPos: TdxFormatterPosition;
    FRowStartPos: TdxFormatterPosition;
    FSuppressHyphenation: Boolean;
    FMaxHeight: Integer;
    FForceFormattingComplete: Boolean;
    FPageNumberSource: TdxPage;
    FStates: array[TdxParagraphBoxFormatterState] of TdxBoxFormatterStateBase;
    FHasDeferredNumberingListBox: Boolean;
    FHyphenationService: IHyphenationService;
    FParagraphStartRowCount: Integer;
    FLastTabStartPos: TdxFormatterPosition;
    FUnapprovedFloatingObjectsStartPos: TdxFormatterPosition;
    FHasUnapprovedFloatingObjects: Boolean;
    FTableStates: TdxParagraphBoxFormatterTableStateBaseStack;
    function GetCurrentRow: TdxRow; 
    function GetNewState(AStateType: TdxParagraphBoxFormatterState): TdxBoxFormatterStateBase; inline;
  protected
    procedure CreateStates; override;
    procedure DestroyStates; override;
    function GetIteratorClass: TdxIteratorClass; override;

    procedure ApproveFloatingObjects;
    procedure RollbackUnapprovedFloatingObjects;
    procedure StartFloatingObjects;
  public
    constructor Create(APieceTable: TdxPieceTable; AMeasurer: TdxBoxMeasurer; ARowsController: TdxRowsController); 
    destructor Destroy; override;

    procedure ChangeStateContinueFromParagraph(AParagraphIndex: TdxParagraphIndex); 
    procedure ChangeState(AStateType: TdxParagraphBoxFormatterState); virtual; 
    function GetInitialStateType(ABeginFromParagraphStart: Boolean): TdxParagraphBoxFormatterState; 
    procedure Initialize(ARowsController: TdxRowsController); virtual; 
    procedure SubscribeRowsControllerEvents; virtual; 
    procedure UnsubscribeRowsControllerEvents; virtual; 
    procedure OnTableStart(ASender: TObject; AE: TdxEventArgs); 
    procedure OnTableEnd(ASender: TObject; AE: TdxEventArgs); 
    function GetActiveIterator: TdxParagraphBoxIterator; 
    procedure UpdateSyllableIterator; 
    procedure ClearSyllableIterator; 
    procedure BeginParagraphFormatting(AIterator: TdxParagraphBoxIterator; ABeginFromParagraphStart: Boolean); 
    function FormatNextRow: TdxFormattingProcessResult; 
    procedure EndParagraphFormatting; 
    procedure BeginParagraph(ABeginFromParagraphStart: Boolean); override; 
    function CanBreakPageBefore(AParagraph: TdxParagraph): Boolean; 
    function ShouldPageBreakBeforeParagraph(AParagraph: TdxParagraph): Boolean; virtual; 
    procedure EndParagraph; 
    procedure StartNewRow;
    procedure StartNewTab; 
    procedure StartNewWord; 
    procedure RollbackToPositionAndClearLastRow(APos: TdxFormatterPosition); 
    function RollbackToParagraphStart(APos: TdxFormatterPosition; AInitialRowCount: Integer): TdxCompleteFormattingResult;
    function RollbackToStartOfRowCore(ACanFit: TdxCanFitCurrentRowToColumnResult; ANextState: TdxParagraphBoxFormatterState): TdxCompleteFormattingResult; 
    function RollbackToStartOfRowTable(ACanFit: TdxCanFitCurrentRowToColumnResult): TdxParagraphIndex; 
    function RollbackToStartOfRow(ACanFit: TdxCanFitCurrentRowToColumnResult): TdxCompleteFormattingResult; 
    procedure ResetLastTabPosition; 
    procedure RollbackToLastTab; overload; 
    procedure RollbackToLastTab(AIter: TdxParagraphBoxIterator); overload; 
    procedure RollbackToStartOfWord; overload; 
    procedure RollbackToStartOfWord(AIter: TdxParagraphBoxIterator); overload; 

    property SyllableIterator: TdxParagraphBoxIterator read FSyllableIterator;
    property CurrentRow: TdxRow read GetCurrentRow;
    property ParagraphStartPos: TdxFormatterPosition read FParagraphStartPos;
    property RowStartPos: TdxFormatterPosition read FRowStartPos;
    property WordStartPos: TdxFormatterPosition read FWordStartPos;
    property RowsController: TdxRowsController read FRowsController;
    property SuppressHyphenation: Boolean read FSuppressHyphenation;
    property MaxHeight: Integer read FMaxHeight write FMaxHeight;
    property ForceFormattingComplete: Boolean read FForceFormattingComplete write FForceFormattingComplete;
    property PageNumberSource: TdxPage read FPageNumberSource write FPageNumberSource;
    property HasDeferredNumberingListBox: Boolean read FHasDeferredNumberingListBox write FHasDeferredNumberingListBox;
  end;

  { TdxParagraphCharacterFormatter }

  TdxParagraphCharacterFormatter = class(TdxParagraphFormatterBase<TdxCharacterFormatterStateBase>)
  private
    FStates: array[TdxCharacterFormatterState] of TdxCharacterFormatterStateBase;
    FTextBoxInfos: TdxBoxInfoList;
    FTextBoxes: TdxBoxList;
    function GetIterator: TdxParagraphCharacterIterator; {$IFDEF DELPHI9} inline; {$ENDIF}
  protected
    procedure CreateStates; override;
    procedure DestroyStates; override;
    procedure FormatParagraph(AIterator: TdxParagraphIteratorBase); virtual;
    function GetInitialStateType(ABeginFromParagraphStart: Boolean): TdxCharacterFormatterState;  
    function GetIteratorClass: TdxIteratorClass; override;

    property Iterator: TdxParagraphCharacterIterator read GetIterator;
  public
    constructor Create(APieceTable: TdxPieceTable; AMeasurer: TdxBoxMeasurer);
    destructor Destroy; override;

    procedure ChangeState(AStateType: TdxCharacterFormatterState); virtual; 
    procedure BeginParagraph(ABeginFromParagraphStart: Boolean); override; 
    procedure FormatNumberingListBoxes; virtual; 
    function CreateNumberingListBox(ASeparatorChar: Char; APosition: TdxFormatterPosition): TdxNumberingListBox; 
    function CreateSeparatorBox(ASeparator: Char; APosition: TdxFormatterPosition): TdxBox; 
    procedure Format(AIterator: TdxParagraphCharacterIterator); virtual; 
    procedure AddTextBoxToQueue(ABox: TdxBox; ABoxInfo: TdxBoxInfo); 
    procedure MeasureTextBoxes; 
  end;


  { TdxCurrentHorizontalPositionController }

  TdxCurrentHorizontalPositionController = class 
  private
    FRowsController: TdxRowsController;
    FCurrentHorizontalPosition: Integer;
  protected
    procedure IncrementCurrentHorizontalPosition(ADelta: Integer); virtual; 
    procedure RollbackCurrentHorizontalPositionTo(AValue: Integer); virtual; 
    procedure SetCurrentRowInitialHorizontalPosition; virtual; 
    procedure OnCurrentRowHeightChanged(AKeepTextAreas: Boolean); virtual; 
    function CalculateBoxBounds(ABoxInfo: TdxBoxInfo): TRect; virtual; 
    function CanFitCurrentRowToColumn(ANewRowHeight: Integer): TdxCanFitCurrentRowToColumnResult; virtual; 
    function CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean; virtual; 
    function CanFitNumberingListBoxToCurrentRow(const ABoxSize: TSize): Boolean; virtual; 
    procedure SetCurrentHorizontalPosition(AValue: Integer); virtual; 
    procedure OnCurrentRowFinished; virtual; 
    function MoveCurrentRowDownToFitTable(ATableWidth, ATableTop: Integer): Integer; virtual; 
    function CalculateFloatingObjectColumnBounds(ACurrentColumn: TdxColumn): TRect; virtual; 

    property InnerCurrentHorizontalPosition: Integer read FCurrentHorizontalPosition write FCurrentHorizontalPosition;
  public
    constructor Create(ARowsController: TdxRowsController); overload; 
    constructor Create(ARowsController: TdxRowsController; APosition: Integer); overload; 
    function GetTextAreaForTable: TdxTextArea; virtual; 

    property RowsController: TdxRowsController read FRowsController;
    property CurrentHorizontalPosition: Integer read FCurrentHorizontalPosition;
  end;

  { TdxFloatingObjectsCurrentHorizontalPositionController }

  TdxFloatingObjectsCurrentHorizontalPositionController = class(TdxCurrentHorizontalPositionController) 
  private
    FTextAreas: TList<TdxTextArea>; 
    FCurrentTextAreaIndex: Integer; 
    FLastRowHeight: Integer; 
    function AreTextAreasChanged(APrevTextAreas, ATextAreas: TList<TdxTextArea>): Boolean; 
    function UpdateCurrentTextAreaIndex: Integer; 
    function DoCurrentRowHeightChanged(AHeight: Integer; const ACurrentRowBounds: TRect; AKeepTextAreas,
      AIgnoreFloatingObjects: Boolean): Boolean; overload; 
  protected
    procedure OnCurrentRowHeightChanged(AKeepTextAreas: Boolean); overload; override; 
    procedure SetCurrentRowInitialHorizontalPosition; override; 
    procedure IncrementCurrentHorizontalPosition(ADelta: Integer); override; 
    procedure RollbackCurrentHorizontalPositionTo(AValue: Integer); override; 
    procedure SetCurrentHorizontalPosition(AValue: Integer); override; 
    function MoveCurrentRowDownToFitTable(ATableWidth, ATableTop: Integer): Integer; override; 
    function CanFitCurrentRowToColumn(ANewRowHeight: Integer): TdxCanFitCurrentRowToColumnResult; override; 
    function CanFitNumberingListBoxToCurrentRow(const ABoxSize: TSize): Boolean; override; 
    function CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean; override; 
    function CalculateBoxBounds(ABoxInfo: TdxBoxInfo): TRect; override; 
    procedure AppendRowBoxRange(ABoxRanges: TdxRowBoxRangeCollection; AFirstBoxIndex, ALastBoxIndex, ATextAreaIndex: Integer); 
    procedure OnCurrentRowFinished; override; 
    procedure TryCreateDefaultBoxRange(ACurrentRow: TdxRow); 
    function ShouldBeMovedToNextTextArea(ABox: TdxBox): Boolean; virtual; 
  public
    function CalculateTextAreas(AParagraphFrameItems: TList<TdxParagraphFrameBox>;
      AFloatingObjectItems: TList<TdxFloatingObjectBox>; const ABounds: TRect): TList<TdxTextArea>; 
    function GetTextAreaForTable: TdxTextArea; override; 
    function AdvanceHorizontalPositionToNextTextArea: Boolean; 
  end;

  { TdxIntegerStack }

  TdxIntegerStack = class(TStack)
  private
    procedure Clear;
    function Push(AItem: TdxNativeInt): Pointer;
    function Pop: TdxNativeInt;
    function Peek: TdxNativeInt;
  end;

  { TdxTabsController }

  TdxTabsController = class 
  const
    DefaultDecimalChars: array[0..1] of Char = ('.', ','); 
  private
    FColumnLeft: Integer;
    FParagraphRight: Integer;
    FDocumentModel: TdxDocumentModel;
    FTabs: TdxTabFormattingInfo;
    FTabStack: TdxIntegerStack; 
    FBoxes: TdxBoxCollection;
    FPieceTable: TdxPieceTable;
    FParagraph: TdxParagraph;
    FSingleDecimalTabInTable: Boolean;
  public
    constructor Create;
    destructor Destroy; override;

    procedure ClearLastTabPosition; 
    procedure UpdateLastTabPosition(ABoxesCount: Integer); 
    procedure SaveLastTabPosition(APos: Integer); 
    function CalcLastTabWidth(ARow: TdxRow; AFormatter: TdxParagraphBoxFormatter): Integer; 

    class function CalculateLeaderCount(ABox: TdxTabSpaceBox; APieceTable: TdxPieceTable): Integer; static;
    class function GetTabLeaderCharacterWidth(ABox: TdxTabSpaceBox; APieceTable: TdxPieceTable): Integer; static;
    class procedure CacheLeader(ABox: TdxTabSpaceBox; AFormatter: TdxParagraphBoxFormatter); static;
    class function GetTabLeaderCharacter(ALeaderType: TdxTabLeaderType): Char; static;

    procedure BeginParagraph(AParagraph: TdxParagraph); 
    procedure ObtainTabs; 
    function GetNextTab(APos: Integer): TdxTabInfo; 

    function CalcLastRightTabWidth(ATab: TdxTabInfo; AStartIndex: Integer): Integer; 
    function CalcLastCenterTabWidth(ATab: TdxTabInfo; AStartIndex: Integer): Integer; 
    function CalcLastDecimalTabWidth(ATab: TdxTabInfo; AStartIndex: Integer; AFormatter: TdxParagraphBoxFormatter; ARowLeft: Integer): Integer; 
    function AdjustAlignedTabWidth(ATab: TdxTabInfo; ATabWidth, ALastNonSpaceBoxRight: Integer): Integer; 
    function IsTabPositionBehindParagraphRight(ATab: TdxTabInfo): Boolean; 
    function FindDecimalPointPosition(ABoxes: TdxBoxCollection; AFrom, ATo: Integer; AFormatter: TdxParagraphBoxFormatter; ARowLeft: Integer): Integer; 

    property ColumnLeft: Integer read FColumnLeft write FColumnLeft;
    property ParagraphRight: Integer read FParagraphRight write FParagraphRight;
    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property Tabs: TdxTabFormattingInfo read FTabs;
    property SingleDecimalTabInTable: Boolean read FSingleDecimalTabInTable write FSingleDecimalTabInTable;
  end;

  { TdxRowsController }

  TdxRowsController = class 
  strict private
    FSavedCurrentColumnBounds: TRect;
  private
    FBoxes: TdxFastObjectList;
    FColumnController: IColumnController;
    FCurrentRow: TdxRow;
    FCurrentColumn: TdxColumn;
    FDefaultRowHeight: Integer;
    FMaxAscentAndFree: Integer; 
    FCurrentRowIndent: Integer;
    FParagraphLeft: Integer;
    FSuppressHorizontalOverfull: Boolean;
    FTablesController: TdxTablesController;
    FFloatingObjectsLayout: TdxFloatingObjectsLayout;
    FParagraph: TdxParagraph;
    FInitialLineNumber: Integer;
    FRestartModelPosition: TdxDocumentModelPosition;
    FLineNumber: Integer;
    FLineNumberStep: Integer;
    FPieceTable: TdxPieceTable;
    FHorizontalPositionController: TdxCurrentHorizontalPositionController;
    FSupportsColumnAndPageBreaks: Boolean;

    FParagraphRight: Integer;
    FRegularRowIndent: Integer;
    FStartWordBoxIndex: Integer;
    FHeightBeforeWord: Integer;
    FLastRestartDueToFloatingObjectModelPosition: TdxDocumentModelPosition;
    FLineSpacingCalculator: TdxLineSpacingCalculatorBase;
    FMaxAscent: Integer;
    FMaxPictureHeight: Integer;
    FMaxDescent: Integer;
    FMaxAscentBeforeWord: Integer;
    FMaxDescentBeforeWord: Integer;
    FMaxPictureHeightBeforeWord: Integer;
    FRunIndexBeforeWord: TdxRunIndex;
    FCurrentRunIndex: TdxRunIndex;
    FRowProcessingFlagsBeforeWord: TdxRowProcessingFlags;
    FTabsController: TdxTabsController;
    FCurrentParagraphNumberingListBounds: TRect;
    FSpacingAfterPrevParagraph: Integer;
    FIsSpacingAfterPrevParagraphValid: Boolean;
    FInvisibleEmptyParagraphInCellAfterNestedTable: Boolean;

    FSuppressLineNumberingRecalculationForLastPage: Boolean;
    FMatchHorizontalTableIndentsToTextEdge: Boolean;
    FFrameParagraphIndex: Integer;
    FOnBeginNextSectionFormatting: TdxMulticastMethod<TdxEventHandler>;

    FOnTableStarted: TdxMulticastMethod<TdxEventHandler>; 

    procedure SetInitialLineNumber(const Value: Integer);
    procedure SetLineNumberStep(const Value: Integer);
    function GetDocumentModel: TdxDocumentModel;
    procedure SetCurrentRow(const Value: TdxRow);
    function GetCurrentHorizontalPosition: Integer;
  protected
    function CanAddSectionBreakToPrevRow: Boolean; virtual;
    function CompleteCurrentColumnFormatting: TdxCompleteFormattingResult; virtual;
    function CreateCurrentHorizontalPosition: TdxCurrentHorizontalPositionController; overload; virtual;
    function CreateCurrentHorizontalPosition(APosition: Integer): TdxCurrentHorizontalPositionController; overload; virtual;

    property ColumnController: IColumnController read FColumnController;
    property CurrentColumn: TdxColumn read FCurrentColumn write FCurrentColumn;
    property CurrentRow: TdxRow read FCurrentRow write SetCurrentRow;
    property CurrentHorizontalPosition: Integer read  GetCurrentHorizontalPosition;
    property CurrentRowIndent: Integer read FCurrentRowIndent;
    property DefaultRowHeight: Integer read FDefaultRowHeight;
    property ParagraphLeft: Integer read FParagraphLeft;
    property SuppressHorizontalOverfull: Boolean read FSuppressHorizontalOverfull write FSuppressHorizontalOverfull;
    property FloatingObjectsLayout: TdxFloatingObjectsLayout read FFloatingObjectsLayout;
    property Paragraph: TdxParagraph read FParagraph;
    property RestartModelPosition: TdxDocumentModelPosition read FRestartModelPosition write FRestartModelPosition;
    property InitialLineNumber: Integer read FInitialLineNumber write SetInitialLineNumber;
    property LastRestartDueToFloatingObjectModelPosition: TdxDocumentModelPosition read FLastRestartDueToFloatingObjectModelPosition write FLastRestartDueToFloatingObjectModelPosition;
    property LineNumber: Integer read FLineNumber write FLineNumber;
    property LineNumberStep: Integer read FLineNumberStep write SetLineNumberStep;
    property PieceTable: TdxPieceTable read FPieceTable;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property SupportsColumnAndPageBreaks: Boolean read FSupportsColumnAndPageBreaks write FSupportsColumnAndPageBreaks;
    property HorizontalPositionController: TdxCurrentHorizontalPositionController read FHorizontalPositionController write FHorizontalPositionController;
    property MatchHorizontalTableIndentsToTextEdge: Boolean read FMatchHorizontalTableIndentsToTextEdge write FMatchHorizontalTableIndentsToTextEdge;
  public
    constructor Create(APieceTable: TdxPieceTable; AColumnController: IColumnController; AMatchHorizontalTableIndentsToTextEdge: Boolean); 
    destructor Destroy; override;

    procedure RaiseBeginNextSectionFormatting; virtual;
    procedure RaiseTableStarted; virtual; 
    procedure Reset(ASection: TdxSection; AKeepFloatingObjects: Boolean); virtual; 
    procedure SetColumnController(ANewColumnController: IColumnController); 
    procedure ClearInvalidatedContentCore(AColumn: TdxColumn; APos: TdxFormatterPosition; AParagraphIndex: TdxParagraphIndex); 
    procedure ClearInvalidatedContentFromTheStartOfRowAtCurrentPage(AColumn: TdxColumn; APos: TdxFormatterPosition; AParagraphIndex: TdxParagraphIndex); virtual; 
    procedure ClearInvalidatedContent(AColumn: TdxColumn; APos: TdxFormatterPosition; AParagraphIndex: TdxParagraphIndex); virtual; 
    procedure RecreateHorizontalPositionController; virtual; 
    function GetTopLevelColumnController: IColumnController; 
    procedure BeginSectionFormatting(ASection: TdxSection); virtual; 
    procedure RestartFormattingFromTheStartOfSection(ASection: TdxSection; AColumn: TdxColumn); virtual; 
    procedure RestartFormattingFromTheMiddleOfSection(ASection: TdxSection; AColumn: TdxColumn); virtual; 
    procedure RestartFormattingFromTheStartOfRowAtCurrentPage(ASection: TdxSection; AColumn: TdxColumn); virtual; 
    procedure CreateNewCurrentRowAfterRestart(ASection: TdxSection; AColumn: TdxColumn); virtual; 
    procedure AssignCurrentRowLineNumber; 
    function CreateTablesController: TdxTablesController; virtual; 
    procedure StartNewWord; virtual; 
    procedure AddSingleDecimalTabInTable; virtual; 
    procedure AddTabBox(ATab: TdxTabInfo; ABoxInfo: TdxBoxInfo); virtual; 
    procedure ExpandLastTab(AFormatter: TdxParagraphBoxFormatter); 
    function AddBox(ABoxType: TdxBoxClass; ABoxInfo: TdxBoxInfo): TdxBox; virtual; 
    procedure AddNumberingListBox(ABox: TdxNumberingListBox; AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter); virtual; 
    function CalculateNumberingListBoxBounds(ABox: TdxNumberingListBox; AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): TRect; 
    function CalculateNumberingListBoxBoundsCore(ABox: TdxNumberingListBox; AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): TRect; 
    function GetNumberingListBoxSeparatorWidth(ABox: TdxNumberingListBoxWithSeparator; ABounds: TRect; AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): Integer; 
    function GetLegacyNumberingListBoxSeparatorWidth(ABox: TdxNumberingListBoxWithSeparator; ABounds: TRect; AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): Integer; 
    function GetActualTabInfo(ATabInfo: TdxTabInfo; AProperties: TdxListLevelProperties): TdxTabInfo; 
    function CalculateNumberingListBoxLeft(ABoxWidth: Integer; AAlignment: TdxListNumberAlignment): Integer; 
    procedure AddSectionBreakBoxToPrevRow(ABoxType: TdxBoxClass; ABoxInfo: TdxBoxInfo); virtual; 
    function GetBox(ABoxType: TdxBoxClass; ABoxInfo: TdxBoxInfo): TdxBox; virtual; 
    procedure UpdateCurrentRowHeight(ABoxInfo: TdxBoxInfo); 
    procedure UpdateCurrentRowHeightFromInlineObjectRun(ARun: TdxTextRunBase; ABox: TdxBox; AMeasurer: IdxObjectMeasurer); 
    procedure UpdateCurrentRowHeightFromFloatingObject(ARun: TdxFloatingObjectAnchorRun; AMeasurer: IdxObjectMeasurer); 
    function CanFitCurrentRowToColumn: TdxCanFitCurrentRowToColumnResult; overload; 
    function MoveCurrentRowDownToFitTable(ATableWidth, ATableTop: Integer): Integer; 
    function GetTextAreaForTable: TdxTextArea; 
    function CanFitCurrentRowToColumn(ANewBoxInfo: TdxBoxInfo): TdxCanFitCurrentRowToColumnResult; overload; 
    function CreateFloatingObjectSizeAndPositionController: TdxFloatingObjectSizeAndPositionController; virtual; 
    function CanRestartDueFloatingObject: Boolean; virtual; 
    procedure ApplyFloatingObjectBoxProperties(AFloatingObjectBox: TdxFloatingObjectBox; AFloatingObjectProperties: TdxFloatingObjectProperties); virtual; 
    procedure ApplyFloatingObjectBoxBounds(AFloatingObjectBox: TdxFloatingObjectBox; AAnchorBox: TdxFloatingObjectAnchorBox); virtual; 
    procedure UpdateCurrentTableCellBottom(AFloatingObjectBox: TdxFloatingObjectBox); virtual; 
    procedure AddFloatingObjectToLayoutCore(AFloatingObjectBox: TdxFloatingObjectBox; ABoxInfo: TdxBoxInfo); 
    function ShouldChangeExistingFloatingObjectBounds(AFloatingObjectBox: TdxFloatingObjectBox; AAnchorBox: TdxFloatingObjectAnchorBox): Boolean; 
    function AddFloatingObjectToLayout(AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox; ABoxInfo: TdxBoxInfo): Boolean; 
    procedure FormatFloatingObjectTextBox(AFloatingObjectBox: TdxFloatingObjectBox; AContent: TdxTextBoxFloatingObjectContent; AAnchorBox: TdxFloatingObjectAnchorBox); virtual; 
    procedure ResizeFloatingObjectBoxToFitText(AFloatingObjectBox: TdxFloatingObjectBox; AContent: TdxTextBoxFloatingObjectContent; AAnchorBox: TdxFloatingObjectAnchorBox; ATextBoxContent: TRect; AActualSize: Integer); 
    function CalculateRestartPositionDueFloatingObject(AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox): TdxDocumentModelPosition; overload; virtual; 
    function CalculateRestartPositionDueFloatingObject(AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox; ACurrentPosition: TPoint; AWrapType: TdxFloatingObjectTextWrapType): TdxDocumentModelPosition; overload; virtual; 
    function CanFitCurrentRowToColumnCore(ANewRowHeight: Integer): TdxCanFitCurrentRowToColumnResult; 
    function CanFitNumberingListBoxToCurrentRow(const ABoxSize: TSize): Boolean; 
    function CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean; 
    function IsPositionOutsideRightParagraphBound(APos: Integer): Boolean; 
    function CalcNewCurrentRowHeight(ANewBoxInfo: TdxBoxInfo): Integer; 
    procedure OnCellStart; virtual; 
    procedure UpdateMaxAscentAndDescent(ABoxInfo: TdxBoxInfo); 
    function GetFontAscentAndFree(ANewBoxInfo: TdxBoxInfo): Integer; overload; virtual; 
    function GetFontDescent(ANewBoxInfo: TdxBoxInfo): Integer; overload; virtual; 
    function GetFontAscentAndFree(ARun: TdxTextRunBase): Integer; overload; virtual; 
    function GetFontDescent(ARun: TdxInlinePictureRun): Integer; overload; virtual; 
    procedure RemoveLastTextBoxes; 
    procedure TryToRemoveLastTabBox; 
    function CreateLineSpacingCalculator: TdxLineSpacingCalculatorBase; virtual; 
    procedure PrepareCurrentRowBounds(AParagraph: TdxParagraph; ABeginFromParagraphStart: Boolean); 
    procedure BeginParagraph(AParagraph: TdxParagraph; ABeginFromParagraphStart: Boolean); 
    procedure EnsureSpacingAfterPrevParagraphValid(ACurrentParagraph: TdxParagraph); 
    function GetActualSpacingAfter(AParagraphIndex: TdxParagraphIndex): Integer; 
    function ShouldIgnoreParagraphHeight(AParagraph: TdxParagraph): Boolean; 
    procedure EndParagraph; 
    procedure EndSection; 
    procedure ClearRow(AKeepTextAreas: Boolean); 
    procedure InitCurrentRow(AKeepTextAreas: Boolean); 
    procedure MoveRowToNextColumn; 
    procedure MoveRowToNextPage; 
    procedure OnPageBreakBeforeParagraph; 
    procedure MoveCurrentRowToNextColumnCore; virtual; 
    function GetEffectiveLineNumberingRestartType(ASection: TdxSection): TdxLineNumberingRestart; virtual; 
    procedure OnPageFormattingComplete(ASection: TdxSection; APage: TdxPage); virtual; 
    procedure CreateLineNumberBoxes(ASection: TdxSection; APage: TdxPage); overload; virtual; 
    procedure CreateLineNumberBoxes(ASection: TdxSection; APageArea: TdxPageArea); overload;virtual; 
    procedure CreateLineNumberBoxes(ASection: TdxSection; AColumn: TdxColumn; ALineNumbers: TdxLineNumberBoxCollection); overload; virtual; 
    procedure EndRow(AFormatter: TdxParagraphBoxFormatter); 
    procedure EndRowCore(AParagraph: TdxParagraph; AFormatter: TdxParagraphBoxFormatter); 
    function ShouldAddParagraphFrameBox(AParagraph: TdxParagraph): Boolean; 
    function ShouldUseMaxDescent: Boolean; virtual; 
    procedure CreateNewCurrentRow(APrevCurrentRow: TdxRow; AAfterRestart: Boolean); virtual; 
    function CalcPrevBottom(APrevRow: TdxRow; AAfterRestart: Boolean; ANewRow: TdxRow): Integer; 
    procedure ApplyCurrentColumnBounds(const ABounds: TRect); 
    procedure OnDeferredBeginParagraph; virtual; 
    procedure RecalcRowBounds; virtual; 
    procedure ObtainRowIndents; virtual; 
    function GetNextTab(AFormatter: TdxParagraphBoxFormatter): TdxTabInfo; 
    function CalcDefaultRowBounds(Y: Integer): TRect; 
    procedure OffsetCurrentRow(AOffset: Integer); 
    function IncreaseLastRowHeight(ADelta: Integer): TdxRow; 
    procedure EnforceNextRowToStartFromNewPage; virtual; 
    procedure ApplySectionStart(ASection: TdxSection); virtual; 
    procedure NewTableStarted; 
    function CreateTableGridCalculator(ADocumentModel: TdxDocumentModel; AWidthsCalculator: TdxTableWidthsCalculator; AMaxTableWidth: Integer): TdxTableGridCalculator; virtual; 
    procedure AddPageBreakBeforeRow; 
    procedure SaveCurrentInfo; 

    property TablesController: TdxTablesController read FTablesController;
    property BeginNextSectionFormatting: TdxMulticastMethod<TdxEventHandler> read FOnBeginNextSectionFormatting; 
    property TableStarted: TdxMulticastMethod<TdxEventHandler> read FOnTableStarted; 
  end;

  TdxCalculatePageOrdinalResult = record 
    PageOrdinal: Integer; 
    SkippedPageCount: Integer; 

    constructor Create(APageOrdinal, ASkippedPageCount: Integer); 
  end;

  { TdxPageFormattingCompleteEventArgs }

  TdxPageFormattingCompleteEventArgs = class(TdxEventArgs) 
  private
    FSkipAddingFloatingObjects: Boolean; 
    FDocumentFormattingComplete: Boolean; 
    FPage: TdxPage; 
  public
    constructor Create(APage: TdxPage; ADocumentFormattingComplete, ASkipAddingFloatingObjects: Boolean); 

    property Page: TdxPage read FPage; 
    property DocumentFormattingComplete: Boolean read FDocumentFormattingComplete; 
    property SkipAddingFloatingObjects: Boolean read FSkipAddingFloatingObjects; 
  end;

  { TdxPageFormattingCompleteEventHandler }

  TdxPageFormattingCompleteEventHandler = TdxMulticastMethod<
    procedure (ASender: TObject; E: TdxPageFormattingCompleteEventArgs) of object>;

  { TdxResetSecondaryFormattingForPageArgs }

  TdxResetSecondaryFormattingForPageArgs = class(TdxEventArgs) 
  private
    FPage: TdxPage; 
    FPageIndex: Integer; 
  public
    constructor Create(APage: TdxPage; APageIndex: Integer); 

    property Page: TdxPage read FPage; 
    property PageIndex: Integer read FPageIndex; 
  end;

  { TdxResetSecondaryFormattingForPageHandler }

  TdxResetSecondaryFormattingForPageHandler = TdxMulticastMethod<
    procedure (ASender: TObject; AArgs: TdxResetSecondaryFormattingForPageArgs) of object>;

  { TdxColumnsBoundsCalculator }

  TdxColumnsBoundsCalculator = class 
  private
    FUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
  protected
    procedure PopulateColumnsBounds(const ARects: TList<TRect>; const ABounds: TRect; const AColumnInfoCollection: TdxColumnInfoCollection); virtual;
    procedure PopulateEqualWidthColumnsBounds(const AColumnBounds: TList<TRect>; const ABounds: TRect; AColumnCount, ASpaceBetweenColumns: Integer); virtual;
    procedure PopulateEqualWidthColumnsBoundsCore(const ARects: TList<TRect>; const AColumnRects: TRects; ASpaceBetweenColumns: Integer); virtual;
  public
    constructor Create(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter); 
    function Calculate(ASection: TdxSection; ABounds: TRect): TList<TRect>; virtual; 

    property UnitConverter: TdxDocumentModelUnitToLayoutUnitConverter read FUnitConverter;
  end;

  { TdxColumnController }

  TdxColumnController = class(TInterfacedPersistent, IColumnController) 
  private
    FPageAreaController: TdxPageAreaController;
    FColumnsBounds: TList<TRect>;
    FNextColumnIndex: Integer;
    FOnGenerateNewColumn: TdxMulticastMethod<TdxEventHandler>; 
  protected
    function CompleteCurrentColumnFormatting(AColumn: TdxColumn): TdxCompleteFormattingResult; virtual;
    function GetCurrentPageBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
    function GetCurrentPageClientBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
    function GetMeasurer: TdxBoxMeasurer;
    function GetColumns: TdxColumnCollection; virtual;
    function GetPageAreaController: TdxPageAreaController;
    function GetPageLastRunIndex: TdxRunIndex;
    function GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean; virtual;
  public
    constructor Create(APageAreaController: TdxPageAreaController); 
    destructor Destroy; override;
    procedure RaiseGenerateNewColumn; virtual; 
    procedure Reset(ASection: TdxSection); virtual; 
    procedure ClearInvalidatedContent(APos: TdxFormatterPosition); virtual; 
    procedure ClearInvalidatedContentCore(APos: TdxFormatterPosition; AColumns: TdxColumnCollection); 
    procedure CleanupEmptyBoxes(ALastColumn: TdxColumn); virtual; 
    procedure ResetToFirstColumn; 
    procedure BeginSectionFormatting(ASection: TdxSection); virtual; 
    procedure RestartFormattingFromTheStartOfSection(ASection: TdxSection; ACurrentColumnIndex: Integer); virtual; 
    procedure RestartFormattingFromTheMiddleOfSection(ASection: TdxSection; ACurrentColumnIndex: Integer); virtual; 
    procedure RestartFormattingFromTheStartOfRowAtCurrentPage; virtual; 
    function GetNextColumn(AColumn: TdxColumn; AKeepFloatingObjects: Boolean): TdxColumn; virtual; 
    procedure RemoveGeneratedColumn(AColumn: TdxColumn);
    procedure AddColumn(AColumn: TdxColumn); virtual; 
    function GetNextColumnCore(AColumn: TdxColumn): TdxColumn; virtual; 
    function CalculateColumnBounds(AColumnIndex: Integer): TRect; 
    function CalculateColumnBoundsCore(AColumnIndex: Integer): TRect; virtual; abstract; 
    procedure ApplySectionStart(ASection: TdxSection); virtual; 
    procedure CreateColumnBounds; virtual; 
    function CreateColumnBoundsCalculator: TdxColumnsBoundsCalculator; virtual; 
    function CreateRow: TdxRow; 
    procedure AddInnerTable(ATableViewInfo: TdxTableViewInfo); virtual;
    function GetPreviousColumn(AColumn: TdxColumn): TdxColumn; 
    property PageAreaController: TdxPageAreaController read FPageAreaController;
    property Columns: TdxColumnCollection read GetColumns;
    property ColumnsBounds: TList<TRect> read FColumnsBounds; 
    property Measurer: TdxBoxMeasurer read GetMeasurer;
    property ShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean read GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn; 
    property GenerateNewColumn: TdxMulticastMethod<TdxEventHandler> read FOnGenerateNewColumn; 
  end;

  { TdxPageBoundsCalculator }

  TdxPageBoundsCalculator = class 
  private
    FUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
  protected
    function CalculatePageBounds(ASection: TdxSection): TRect; virtual; 
    function CalculatePageClientBounds(ASection: TdxSection): TRect; virtual; 
    function CalculatePageClientBoundsCore(APageWidth, APageHeight, AMarginLeft, AMarginTop, AMarginRight,
      AMarginBottom: Integer): TRect; virtual; 
  public
    constructor Create(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter);

    property UnitConverter: TdxDocumentModelUnitToLayoutUnitConverter read FUnitConverter;
  end;

  { TdxPageController }

  TdxPageController = class abstract
  private
    FCurrentSection: TdxSection;
    FDocumentLayout: TdxDocumentLayout;
    FFloatingObjectsLayout: TdxFloatingObjectsLayout;
    FPageClientBounds: TRect;
    FNextPageOrdinalType: TdxSectionStartType;
    FFirstPageOfSection: Boolean;
    FTableStarted: Boolean;
    FCurrentSectionIndex: TdxSectionIndex;
    FPageBoundsCalculator: TdxPageBoundsCalculator;
    FCurrentPageClientBounds: TRect;
    FPageBounds: TRect;
    FOnPageFormattingStarted: TdxPageFormattingCompleteEventHandler; 
    FOnPageFormattingComplete: TdxPageFormattingCompleteEventHandler; 
    FOnPageCountChanged: TdxMulticastMethod<TdxEventHandler>; 
    FPageLastRunIndex: TdxRunIndex;
    FPagesBeforeTable: Integer;
    FNextSection: Boolean;
    function ContainsOrphanedItems(AItems: TList<TdxFloatingObjectBox>; AMaxRunIndex: TdxRunIndex): Boolean; 
    function GetPageCount: Integer;
  protected
    class var FFloatingObjectBoxZOrderComparer: TdxFloatingObjectBoxZOrderComparer; 
    class constructor Initialize;
    class destructor Finalize;
  protected
    function GetCurrentSection: TdxSection; virtual;
    function GetPages: TdxPageCollection; virtual;
    function GetPieceTable: TdxPieceTable; virtual;
    function GetDocumentModel: TdxDocumentModel; virtual;
    function IsCurrentPageEven: Boolean;
    procedure AppendFloatingObjects(AFrom, ATo: TList<TdxFloatingObjectBox>; AMaxRunIndex: TdxRunIndex); virtual; 
  public
    constructor Create(ADocumentLayout: TdxDocumentLayout; AFloatingObjectsLayout: TdxFloatingObjectsLayout); 
    destructor Destroy; override;
    property PageFormattingStarted: TdxPageFormattingCompleteEventHandler read FOnPageFormattingStarted; 
    procedure RaisePageFormattingStarted(APage: TdxPage); virtual; 
    property PageFormattingComplete: TdxPageFormattingCompleteEventHandler read FOnPageFormattingComplete; 
    procedure RaisePageFormattingComplete(APage: TdxPage; ASkipAddingFloatingObjects: Boolean); virtual; 

    property PageCountChanged: TdxMulticastMethod<TdxEventHandler> read FOnPageCountChanged; 
    procedure RaisePageCountChanged; virtual; 

    procedure Reset(AKeepFloatingObjects: Boolean); virtual; 
    function ClearInvalidatedContentFromTheStartOfRowAtCurrentPage(APos: TdxFormatterPosition;
      AParagraphIndex: TdxParagraphIndex; AKeepFloatingObjects: Boolean): TdxClearInvalidatedContentResult; 

    function ClearInvalidatedContent(APos: TdxFormatterPosition; AParagraphIndex: TdxParagraphIndex; AKeepFloatingObjects: Boolean): TdxClearInvalidatedContentResult; virtual; 
    function CompleteCurrentPageFormatting: TdxCompleteFormattingResult; virtual;
    procedure CorrectLastPageOrdinal(AParagraphIndex: TdxParagraphIndex; APageIndex: Integer); virtual; 
    procedure BeginNextSectionFormatting; virtual; 
    procedure RestartFormattingFromTheStartOfSection; virtual; 
    function CalculatePageBounds(ASection: TdxSection): TRect; 
    procedure RestartFormattingFromTheMiddleOfSectionCore(ASection: TdxSection); virtual; 
    procedure PopulateFloatingObjectsLayout(ALastPage: TdxPage); 
    procedure PopulateFloatingObjects(AFloatingObjects: TList<TdxFloatingObjectBox>); 
    procedure RestartFormattingFromTheMiddleOfSection(ASection: TdxSection); virtual; 
    procedure RestartFormattingFromTheStartOfRowAtCurrentPage; virtual; 
    function GetNextPage(AKeepFloatingObjects: Boolean): TdxPage; virtual; 
    function CalculatePageOrdinal(AFirstPageOfSection: Boolean): TdxCalculatePageOrdinalResult; virtual; 
    function CalculatePageOrdinalCore(APreviousPageOrdinal, ANumSkippedPages: Integer; AFirstPageOfSection: Boolean): TdxCalculatePageOrdinalResult; virtual; 
    function GetNextPageCore: TdxPage; 
    procedure ApplySectionStart(ASection: TdxSection); virtual; 
    function CalculateNextPageOrdinalType(ASection: TdxSection): TdxSectionStartType; virtual; 
    procedure FinalizePagePrimaryFormatting(APage: TdxPage; ASkipAddingFloatingObjects: Boolean); virtual; 
    function AppendFloatingObjectsToPage(APage: TdxPage): Boolean; virtual; 
    procedure ClearFloatingObjectsLayout; virtual; 
    procedure ApplyExistingHeaderAreaBounds(APage: TdxPage); virtual; 
    procedure ApplyExistingFooterAreaBounds(APage: TdxPage); virtual; 
    function CreatePageBoundsCalculator: TdxPageBoundsCalculator; virtual; abstract; 
    procedure RemoveLastPage;
    procedure SetPageLastRunIndex(ARunIndex: TdxRunIndex);
    procedure BeginTableFormatting; 
    procedure EndTableFormatting; 
    function CreateHitTestCalculator(ARequest: TdxRichEditHitTestRequest;
      AResult: TdxRichEditHitTestResult): TdxBoxHitTestCalculator; virtual; abstract;

    property PageBounds: TRect read FPageBounds;
    property PageClientBounds: TRect read FCurrentPageClientBounds;
    property CurrentSectionIndex: TdxSectionIndex read FCurrentSectionIndex write FCurrentSectionIndex; 
    property CurrentSection: TdxSection read GetCurrentSection;
    property DocumentLayout: TdxDocumentLayout read FDocumentLayout;
    property PageCount: Integer read GetPageCount;
    property Pages: TdxPageCollection read GetPages;
    property NextPageOrdinalType: TdxSectionStartType read FNextPageOrdinalType; 
    property PageBoundsCalculator: TdxPageBoundsCalculator read FPageBoundsCalculator; 
    property PieceTable: TdxPieceTable read GetPieceTable; 
    property DocumentModel: TdxDocumentModel read GetDocumentModel; 
    property FirstPageOfSection: Boolean read FFirstPageOfSection; 
    property FloatingObjectsLayout: TdxFloatingObjectsLayout read FFloatingObjectsLayout;
    property NextSection: Boolean read FNextSection write FNextSection; 
    property PageLastRunIndex: TdxRunIndex read FPageLastRunIndex;
  end;

  { TdxPageAreaControllerState }

  TdxPageAreaControllerState = class 
  private
    FOwner: TdxPageAreaController;
    FCurrentAreaBounds: TRect;
    function GetPageController: TdxPageController;
  protected
    function GetAreas: TdxPageAreaCollection; virtual;
    procedure RestoreCurrentAreaBounds(const AOldBounds: TRect); virtual;
  public
    constructor Create(AOwner: TdxPageAreaController); 
    function CompleteCurrentAreaFormatting: TdxCompleteFormattingResult; virtual; abstract;  
    function GetNextPageArea(AKeepFloatingObjects: Boolean): TdxPageArea; virtual; abstract; 
    procedure Reset(ASection: TdxSection); virtual; 
    procedure BeginSectionFormatting(ASection: TdxSection); virtual; 
    procedure RestartFormattingFromTheStartOfSection(ASection: TdxSection; ACurrentAreaIndex: Integer); virtual; 
    procedure RestartFormattingFromTheMiddleOfSection(ASection: TdxSection; ACurrentAreaIndex: Integer); virtual; 
    procedure RestartFormattingFromTheStartOfRowAtCurrentPage; virtual; 
    procedure ClearInvalidatedContent(APos: TdxFormatterPosition); 
    function GetNextPageAreaCore: TdxPageArea; 
    procedure CreateCurrentAreaBounds; virtual; 
    function CreateCurrentAreaBoundsCore: TRect; virtual; abstract; 
    procedure ApplySectionStart(ASection: TdxSection); virtual; abstract; 

    property Owner: TdxPageAreaController read FOwner;
    property CurrentAreaBounds: TRect read FCurrentAreaBounds;
    property PageController: TdxPageController read GetPageController;
    property Areas: TdxPageAreaCollection read GetAreas;
  end;

  { TdxMulticastMethodHandler }

  TdxMulticastMethodHandler = TdxMulticastMethod<
    procedure(ASender: TObject; AArgs: TdxEventArgs) of object>;

  { TdxPageAreaController }

  TdxPageAreaController = class 
  private
    FPageController: TdxPageController; 
    FState: TdxPageAreaControllerState; 
    FOnResetFormattingFromCurrentArea: TdxMulticastMethodHandler; 
    function GetCurrentAreaBounds: TRect;
  protected
    function GetAreas: TdxPageAreaCollection; virtual;
    procedure RaiseResetFormattingFromCurrentArea; virtual; 
  public
    constructor Create(APageController: TdxPageController); 
    destructor Destroy; override;

    function CreateDefaultState(ACurrentAreaIndex: Integer): TdxPageAreaControllerState; virtual; 
    function GetNextPageArea(AKeepFloatingObjects: Boolean): TdxPageArea; virtual; 
    procedure Reset(ASection: TdxSection); 
    procedure BeginSectionFormatting(ASection: TdxSection); virtual; 
    procedure RestartFormattingFromTheStartOfSection(ASection: TdxSection; ACurrentAreaIndex: Integer); virtual; 
    procedure RestartFormattingFromTheMiddleOfSection(ASection: TdxSection; ACurrentAreaIndex: Integer); virtual; 
    procedure RestartFormattingFromTheStartOfRowAtCurrentPage; virtual; 
    procedure ClearInvalidatedContent(APos: TdxFormatterPosition); virtual; 
    function CompleteCurrentAreaFormatting: TdxCompleteFormattingResult; virtual;
    procedure RestartFormattingFromBeginOfCurrentArea; virtual; 
    procedure SwitchToState(AState: TdxPageAreaControllerState); virtual; 
    procedure RemoveLastPageArea; virtual;

    property PageController: TdxPageController read FPageController;
    property Areas: TdxPageAreaCollection read GetAreas;
    property State: TdxPageAreaControllerState read FState;
    property CurrentAreaBounds: TRect read GetCurrentAreaBounds;
    property ResetFormattingFromCurrentArea: TdxMulticastMethodHandler read FOnResetFormattingFromCurrentArea; 
  end;

  { TdxDefaultPageAreaControllerState }

  TdxDefaultPageAreaControllerState = class(TdxPageAreaControllerState) 
  private
    FNextAreaIndex: Integer;
  protected
    procedure ApplyContinuousSectionStart(ASection: TdxSection); virtual; 
  public
    constructor Create(AOwner: TdxPageAreaController; ANextAreaIndex: Integer); 
    function CompleteCurrentAreaFormatting: TdxCompleteFormattingResult; override;
    function GetNextPageArea(AKeepFloatingObjects: Boolean): TdxPageArea; override; 
    procedure BeginSectionFormatting(ASection: TdxSection); override; 
    procedure RestartFormattingFromTheStartOfSection(ASection: TdxSection; ACurrentAreaIndex: Integer); override; 
    procedure RestartFormattingFromTheMiddleOfSection(ASection: TdxSection; ACurrentAreaIndex: Integer); override; 
    function CreateCurrentAreaBoundsCore: TRect; override; 
    procedure ApplySectionStart(ASection: TdxSection); override; 
  end;

  { TdxTablesControllerStateBaseStack }

  TdxTablesControllerStateBaseStack = class(TStack)
  private
    function GetItem(Index: Integer): TdxTablesControllerStateBase;
  public
    function Push(AItem: TdxTablesControllerStateBase): TdxTablesControllerStateBase;
    function Pop: TdxTablesControllerStateBase;
    function Peek: TdxTablesControllerStateBase;

    property Items[Index: Integer]: TdxTablesControllerStateBase read GetItem; default;
  end;

  TdxTableBreakType = ( 
    NoBreak,
    NextPage,
    InfinityHeight
  );

  TdxTableViewInfoManager = class;

  { TdxTablesController }

  TdxTablesController = class 
  private
    FRowsController: TdxRowsController;
    FStates: TdxTablesControllerStateBaseStack;
    FTableBreaks: TDictionary<TdxTable, TdxSortedList<TPair<Integer, Integer>>>;
    FInfinityHeights: TDictionary<Integer, Integer>;
    function GetState: TdxTablesControllerStateBase;
    function GetIsInsideTable: Boolean;
  protected
    function CreateTableViewInfoManager(AParentTableViewInfoManager: TdxTableViewInfoManager; ARowsController: TdxRowsController): TdxTableViewInfoManager; virtual;
  public
    constructor Create(ARowsController: TdxRowsController); 
    destructor Destroy; override;

    function GetTableBreak(ATable: TdxTable; ATableViewInfoIndex: Integer; out ABottomBounds: Integer): TdxTableBreakType;
    function IsInfinityHeight(ATableViewInfoIndex: Integer; out ABottomAnchorIndex: Integer): Boolean;
    procedure AddTableBreak(ATable: TdxTable; ATableViewInfoIndex, ABottomBounds: Integer);
    procedure AddInfinityTableBreak(ATableViewInfoIndex, ABottomAnchorIndex: Integer);
    procedure RemoveAllTableBreaks;
    procedure BeginParagraph(AParagraph: TdxParagraph); 
    procedure StartNewTable(ANewCell: TdxTableCell);
    procedure StartTopLevelTable(ANewCell: TdxTableCell);
    procedure EndParagraph(ALastRow: TdxRow);
    function CanFitRowToColumn(ALastTextRowBottom: TdxLayoutUnit; AColumn: TdxColumn): TdxCanFitCurrentRowToColumnResult; virtual; 

    procedure BeforeMoveRowToNextColumn;
    procedure AfterMoveRowToNextColumn;
    procedure AfterMoveRowToNextPage;
    procedure BeforeMoveRowToNextPage;
    procedure StartInnerTable(ACell: TdxTableCell);
    procedure UpdateCurrentCellBottom(ABottom: TdxLayoutUnit);
    procedure UpdateCurrentCellHeight;
    procedure ReturnToPrevState;
    procedure LeaveCurrentTable(ANextCell: TdxTableCell);
    procedure ClearInvalidatedContent;
    procedure Reset;
    procedure OnCurrentRowFinished; 
    function RollbackToStartOfRowTableOnFirstCellRowColumnOverfull: TdxParagraphIndex;
    function GetCurrentCell: TdxTableCell;

    property RowsController: TdxRowsController read FRowsController;
    property State: TdxTablesControllerStateBase read GetState;
    property States: TdxTablesControllerStateBaseStack read FStates;
    property IsInsideTable: Boolean read GetIsInsideTable;
  end;

  { TdxLayoutGridRectangle }

  TdxLayoutGridRectangle = record
    Bounds: TRect;
    ColumnIndex: Integer;
    ColumnSpan: Integer;
    RowSpan: Integer;
    RowIndex: Integer;
    constructor Create(const ABounds: TRect; ARowIndex, AColumnIndex, AColumnSpan: Integer);
  end;

  { TdxTableCellLeftComparable }

  TdxTableCellLeftComparable = class(TInterfacedObject, IdxComparable<TdxTableCellViewInfo>)
  private
    FPos: Integer;
  public
    constructor Create(APos: Integer);
    function CompareTo(const ACell: TdxTableCellViewInfo): Integer;
  end;

  { TdxTableViewInfoManager }

  TdxTableViewInfoManager = class
    type
      TCellAction = (None, Split, SetBottomIndex);

      TTableViewInfoAndStartRowSeparatorIndexComparer = class(TInterfacedObject, IdxComparable<TdxTableViewInfo>)
      private
        FRowSeparatorIndex: Integer;
      public
        constructor Create(ARowSeparatorIndex: Integer);
        function CompareTo(const Value: TdxTableViewInfo): Integer;
      end;

      TTableViewInfoAndEndRowSeparatorIndexComparer = class(TInterfacedObject, IdxComparable<TdxTableViewInfo>)
      private
        FRowSeparatorIndex: Integer;
      public
        constructor Create(ARowSeparatorIndex: Integer);
        function CompareTo(const Value: TdxTableViewInfo): Integer;
      end;
  strict private
    FRowsController: TdxRowsController;
    FParentTableViewInfoManager: TdxTableViewInfoManager;
    FTableViewInfos: TObjectList<TdxTableViewInfo>;  
    FCurrentCellBottoms: TList<Integer>;
    FColumnController: TdxTableCellColumnController; 
    FTableGrid: TdxTableGrid;                        
    FCurrentTableViewInfoIndex: Integer;
    FCurrentTableCellViewInfo: TdxTableCellViewInfo;
    FTable: TdxTable;
    procedure SetColumnController(Value: TdxTableCellColumnController);
    function GetCurrentTableViewInfo: TdxTableViewInfo;
    function GetCurrentCellBottom: Integer;
    procedure SetCurrentCellBottom(Value: Integer);
    function CreateTableViewInfo(ATable: TdxTable; ATopRowIndex: Integer; AFirstContentInParentCell: Boolean; AVerticalBorderPositions: TdxVerticalBorderPositions): TdxTableViewInfo;
    function AddTableViewInfo(ATableViewInfo: TdxTableViewInfo): Integer;
    function GetStartTableViewInfoByRowSeparatorIndex(ARowSeparatorIndex: Integer): TdxTableViewInfo;
    function GetStartTableViewInfoIndexByRowSeparatorIndex(ARowSeparatorIndex: Integer): Integer;
    function GetEndTableViewInfoIndexByRowSeparatorIndex(ARowSeparatorIndex: Integer): Integer;
    procedure SetRowSeparatorCore(ATableViewInfo: TdxTableViewInfo; ARowSeparatorIndex: Integer; AAnchor: TdxTableCellVerticalAnchor);
    function CreateTableRowViewInfo(ATableViewInfo: TdxTableViewInfo; ARow: TdxTableRow; ARowIndex: Integer): TdxTableRowViewInfoBase;
    function GetRowStartAnchorIndex(ATableViewInfo: TdxTableViewInfo; ARowIndex: Integer): Integer;
    function GetRowStartAnchor(ATableViewInfo: TdxTableViewInfo; ARowIndex: Integer): IdxTableCellVerticalAnchor; overload;
    function AddFirstCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACell: TdxTableCell;
      const ABounds: TRect; const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect): TdxTableCellViewInfo;
    procedure AddMiddleCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACell: TdxTableCell;
      const ABounds: TRect; const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect);
    procedure AddLastCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACell: TdxTableCell; const ABounds: TRect;
      const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect);
    function GetLastTableViewInfo: TdxTableViewInfo;
    procedure AddTableCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACellViewInfo: TdxTableCellViewInfo; AStartRowViewInfoIndex, AEndRowViewInfoIndex: Integer);
    procedure MoveRowAndAnchors_(ASource, ATarget: TdxTableViewInfo; AStartAnchorIndex: Integer);
    function ShouldSplitCell(ASource, ATarget: TdxTableViewInfo; ACellViewInfo: TdxTableCellViewInfo; ASplitAnchorIndex, ASourceAnchorsCount, AInitialSourceBottomRowIndex, AInitialTargetTopRowIndex: Integer): TCellAction;
    procedure MoveRowAndAnchor(ASource, ATarget: TdxTableViewInfo; AStartAnchorIndex: Integer);
    procedure SplitCellsByAncor(ACurrentTableViewInfo, ANextTableViewInfo: TdxTableViewInfo; AAnchorIndex: Integer);
    procedure SplitCellByAnchor(ACurrentTableViewInfo, ANextTableViewInfo: TdxTableViewInfo; AAnchorIndex: Integer; ATableCellViewInfo: TdxTableCellViewInfo);
    function ShouldRemoveColumn(ATableViewInfoIndex: Integer; ATableViewInfo: TdxTableViewInfo): Boolean;
    function GetTopLevelTableViewInfoManager: TdxTableViewInfoManager;
  protected
    function GetParentTableCellViewInfo: TdxTableCellViewInfo; virtual;
    procedure SetCurrentTableCellViewInfo(ATableCellViewInfo: TdxTableCellViewInfo); virtual;
    procedure ValidateTopLevelColumn; virtual;
    procedure SetCurrentParentColumn(ATableViewInfo: TdxTableViewInfo); virtual;
    function FindSuitableTableCellViewInfo(ACells: TdxTableCellViewInfoCollection; ACell: TdxTableCell): TdxTableCellViewInfo;
    function GetSplitAnchorHorizontalCellBorders(ASplitAnchor: TdxTableCellVerticalAnchor): TList<TdxHorizontalCellBordersInfo>; virtual;
  public
    constructor Create(AParentTableViewInfoManager: TdxTableViewInfoManager; ARowsController: TdxRowsController);
    destructor Destroy; override;

    function StartNewTable(ATable: TdxTable; ACellBounds: TDictionary<TdxTableCell, TdxLayoutGridRectangle>;
      ALeftOffset, ARightCellMargin: TdxLayoutUnit; AFirstContentInParentCell: Boolean;
      ATableTextArea: TdxTextArea; out AMaxRight: TdxLayoutUnit): TdxTableViewInfo;
    function GetTableGrid: TdxTableGrid;
    function GetTableViewInfos: TList<TdxTableViewInfo>;
    procedure SetRowSeparator(ARowSeparatorIndex: Integer; AAnchor: TdxTableCellVerticalAnchor);
    procedure SetRowSeparatorForCurrentTableViewInfo(ARowSeparatorIndex: Integer; AAnchor: TdxTableCellVerticalAnchor);
    procedure EnsureTableRowViewInfo(ATableViewInfo: TdxTableViewInfo; ARow: TdxTableRow; ARowIndex: Integer);
    function GetRowStartAnchor(ARowIndex: Integer): IdxTableCellVerticalAnchor; overload;
    function GetRowStartColumn(ARowIndex: Integer): TdxColumn;
    function GetRowStartTableViewInfo(ATableRow: TdxTableRow): TdxTableViewInfo;
    procedure StartNextCell(ACell: TdxTableCell; const ABounds: TRect; const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect);
    function GetLastTableBottom: Integer;
    function GetBottomCellAnchor(ACurrentCell: TdxTableCell): TdxTableCellVerticalAnchor;
    function GetBottomCellRowSeparatorIndex(ACurrentCell: TdxTableCell; ARowSpan: Integer): Integer;
    procedure BeforeMoveRowToNextColumn(ACurrentCellBottom: TdxLayoutUnit);
    procedure AfterMoveRowToNextColumn;
    function GetCurrentCellTop: Integer;
    function GetCurrentCellTopAnchorIndex: Integer;
    function GetCurrentCellBottomAnchorIndex: Integer;
    procedure LeaveCurrentTable(ABeforeRestart: Boolean); virtual;
    procedure RemoveAllInvalidRowsOnColumnOverfull(AFirstInvalidRowIndex: Integer);
    procedure FixColumnOverflow; virtual; 
    function IsCurrentCellViewInfoFirst: Boolean;
    function IsFirstTableRowViewInfoInColumn: Boolean;
    procedure SetCurrentCellHasContent;
    function GetCurrentTableViewInfoIndex: Integer;
    function GetCurrentTopLevelTableViewInfoIndex: Integer;
    function IsFirstContentInParentCell: Boolean;
    function GetParentTableViewInfoManager: TdxTableViewInfoManager;

    property ColumnController: TdxTableCellColumnController read FColumnController write SetColumnController;
    property CurrentTableViewInfo: TdxTableViewInfo read GetCurrentTableViewInfo;
    property CurrentCellBottom: Integer read GetCurrentCellBottom write SetCurrentCellBottom;
    property CurrentTableCellViewInfo: TdxTableCellViewInfo read FCurrentTableCellViewInfo write SetCurrentTableCellViewInfo;
    property RowsController: TdxRowsController read FRowsController;
  end;

  { TdxTablesControllerStateBase }

  TdxTablesControllerStateBase = class abstract 
  private
    FTablesController: TdxTablesController;
  public
    constructor Create(ATablesController: TdxTablesController); 

    procedure EnsureCurrentCell(ACell: TdxTableCell); virtual; abstract; 
    procedure UpdateCurrentCellBottom(ABottom: TdxLayoutUnit); virtual; abstract; 
    procedure EndParagraph(ALastRow: TdxRow); virtual; abstract; 
    procedure BeforeMoveRowToNextColumn; virtual; abstract; 
    procedure AfterMoveRowToNextColumn; virtual; abstract; 
    procedure UpdateCurrentCellHeight(ARow: TdxRow); virtual; abstract; 
    function CanFitRowToColumn(ALastTextRowBottom: TdxLayoutUnit; AColumn: TdxColumn): TdxCanFitCurrentRowToColumnResult; virtual; abstract; 
    procedure OnCurrentRowFinished; virtual; abstract; 
    function RollbackToStartOfRowTableOnFirstCellRowColumnOverfull(AFirstTableRowViewInfoInColumn, AInnerMostTable: Boolean): TdxParagraphIndex; virtual; abstract; 

    property TablesController: TdxTablesController read FTablesController;
  end;

  { TdxTablesControllerNoTableState }

  TdxTablesControllerNoTableState = class(TdxTablesControllerStateBase) 
  public
    procedure EnsureCurrentCell(ACell: TdxTableCell); override; 
    procedure EndParagraph(ALastRow: TdxRow); override; 
    procedure BeforeMoveRowToNextColumn; override; 
    procedure AfterMoveRowToNextColumn; override; 
    procedure UpdateCurrentCellHeight(ARow: TdxRow); override; 
    procedure UpdateCurrentCellBottom(ABottom: TdxLayoutUnit); override; 
    function CanFitRowToColumn(ALastTextRowBottom: Integer; AColumn: TdxColumn): TdxCanFitCurrentRowToColumnResult; override; 
    procedure OnCurrentRowFinished; override; 
    function RollbackToStartOfRowTableOnFirstCellRowColumnOverfull(AFirstTableRowViewInfoInColumn, AInnerMostTable: Boolean): TdxParagraphIndex; override; 
  end;

  { TdxTableCellVerticalBorderCalculator }

  TdxTableCellVerticalBorderCalculator = class 
  private
    FTable: TdxTable; 
  protected
    function GetLeftBorder(ABorderCalculator: TdxTableBorderCalculator; ACell: TdxTableCell): TdxBorderInfo; 
    function GetRightBorder(ABorderCalculator: TdxTableBorderCalculator; ACell: TdxTableCell): TdxBorderInfo; 
  public
    constructor Create(ATable: TdxTable); 
    class function GetStartColumnIndex(ACell: TdxTableCell; ALayoutIndex: Boolean): Integer; 
    class function GetVerticalSpanCells(ACell: TdxTableCell; AStartColumnIndex: Integer; ALayoutIndex: Boolean): TList<TdxTableCell>; overload; 
    class function GetVerticalSpanCells(ACell: TdxTableCell; ALayoutIndex: Boolean): TList<TdxTableCell>; overload; 
    class function GetCellByStartColumnIndex(ARow: TdxTableRow; AStartColumnIndex: Integer; ALayoutIndex: Boolean): TdxTableCell; 
    class function GetCellByColumnIndex(ARow: TdxTableRow; AStartColumnIndex: Integer): TdxTableCell; 
    class function GetCellByEndColumnIndex(ARow: TdxTableRow; AEndColumnIndex: Integer): TdxTableCell; 
    class function GetCellsByIntervalColumnIndex(ARow: TdxTableRow; AStartColumnIndex, AEndColumnIndex: Integer): TList<TdxTableCell>; 
    function GetLeftBorderWidth(ABorderCalculator: TdxTableBorderCalculator; ACell: TdxTableCell; ALayoutIndex: Boolean): TdxModelUnit; 
    function GetRightBorderWidth(ABorderCalculator: TdxTableBorderCalculator; ACell: TdxTableCell): TdxModelUnit; 
  end;

  { TdxTableCellIteratorBase }

  TdxTableCellIteratorBase = class 
  protected
    function GetCurrentStartColumnIndex: Integer; virtual; abstract;
    function GetCurrentEndColumnIndex: Integer; virtual; abstract;
    function GetCurrentCell: TdxTableCell; virtual; abstract;
    function GetEndOfRow: Boolean; virtual; abstract;
  public
    procedure SetStartColumnIndex(ANewStartColumnIndex: Integer); virtual; abstract; 
    function MoveNextCell: Boolean; virtual; abstract; 

    property CurrentStartColumnIndex: Integer read GetCurrentStartColumnIndex; 
    property CurrentEndColumnIndex: Integer read GetCurrentEndColumnIndex; 
    property CurrentCell: TdxTableCell read GetCurrentCell; 
    property EndOfRow: Boolean read GetEndOfRow; 
  end;

  { TdxTableCellIterator }

  TdxTableCellIterator = class(TdxTableCellIteratorBase) 
  private
    FRow: TdxTableRow; 
    FCurrentStartColumnIndex: Integer; 
    FCurrentEndColumnIndex: Integer; 
    FCurrentCellIndex: Integer; 
    function GetCellCount: Integer;
  protected
    function GetCurrentStartColumnIndex: Integer; override; 
    function GetCurrentEndColumnIndex: Integer; override; 
    function GetCurrentCell: TdxTableCell; override; 
    function GetEndOfRow: Boolean; override; 

    property Row: TdxTableRow read FRow; 
    property CellCount: Integer read GetCellCount; 
  public
    constructor Create(ARow: TdxTableRow); 
    procedure SetStartColumnIndex(ANewStartColumnIndex: Integer); override; 
    function MoveNextCell: Boolean; override; 
  end;

  { TdxTableCellEmptyIterator }

  TdxTableCellEmptyIterator = class(TdxTableCellIteratorBase) 
  protected
    function GetCurrentStartColumnIndex: Integer; override; 
    function GetCurrentEndColumnIndex: Integer; override; 
    function GetCurrentCell: TdxTableCell; override; 
    function GetEndOfRow: Boolean; override; 
  public
    function MoveNextCell: Boolean; override; 
    procedure SetStartColumnIndex(ANewStartColumnIndex: Integer); override; 
  end;

  { TdxTableCellBorderIterator }

  TdxTableCellBorderIterator = class
  private
    FCurrentCellAbove: TdxTableCell; 
    FCurrentCellBelow: TdxTableCell; 
    FAboveRowIterator: TdxTableCellIteratorBase; 
    FBelowRowIterator: TdxTableCellIteratorBase; 
    FCurrentStartColumnIndex: Integer; 
    FCurrentEndColumnIndex: Integer; 
    function GetCurrentAboveInfo: TdxHorizontalCellBordersInfo;
    function GetCurrentMergedInfo: TdxHorizontalCellBordersInfo;
    function GetCurrentBelowInfo: TdxHorizontalCellBordersInfo; 
  public
    constructor Create(AAboveRow, ABelowRow: TdxTableRow); 
    function MoveNext: Boolean; 
    function IsVerticallyMerged: Boolean; 

    property CurrentAboveInfo: TdxHorizontalCellBordersInfo read GetCurrentAboveInfo; 
    property CurrentMergedInfo: TdxHorizontalCellBordersInfo read GetCurrentMergedInfo; 
    property CurrentBelowInfo: TdxHorizontalCellBordersInfo read GetCurrentBelowInfo; 
  end;

  { TdxTableCellHorizontalBorderCalculator }

  TdxTableCellHorizontalBorderCalculator = class 
  private
    FTable: TdxTable; 
    function GetAnchorBorders(AAnchorIndex: Integer): TList<TdxHorizontalCellBordersInfo>; 
    function ResolveBorder(ATable: TdxTable; ABorder1, ABorder2: TdxHorizontalCellBordersInfo): TdxHorizontalCellBordersInfo; 
  public
    constructor Create(ATable: TdxTable); 
    function GetBottomBorders(ARow: TdxTableRow): TList<TdxHorizontalCellBordersInfo>;
    function GetTopBorders(ARow: TdxTableRow): TList<TdxHorizontalCellBordersInfo>; 
  end;

  TdxRestartFrom = ( 
    NoRestart,
    CellStart,
    TableStart
  );

  { TdxTablesControllerTableState }

  TdxTablesControllerTableState = class(TdxTablesControllerStateBase) 
  private
    FTableViewInfoManager: TdxTableViewInfoManager;
    FCellsBounds: TDictionary<TdxTableCell, TdxLayoutGridRectangle> ; 
    FUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter; 
    FVerticalBordersCalculator: TdxTableCellVerticalBorderCalculator; 
    FCurrentRow: TdxTableRow; 
    FCurrentCell: TdxTableCell; 
    FCurrentCellRowCount: Integer; 
    FRestartFrom: TdxRestartFrom; 
    FRowBoundsBeforeTableStart: TRect; 
    FMaxTableWidth: TdxLayoutUnit; 
    FTableRight: TdxLayoutUnit; 

    function GetRowSpan(AGridPosition: TdxLayoutGridRectangle): Integer; 
    function GetCellBounds(ANewCell: TdxTableCell): TdxLayoutRectangle; 
    function CalcFirstInvalidRowIndex: Integer; 

    function GetCurrentTable: TdxTable;
    function GetRowsController: TdxRowsController;
    function GetCurrentCellViewInfoEmpty: Boolean;
  protected
    FHorizontalBordersCalculator: TdxTableCellHorizontalBorderCalculator; 
    FBorderCalculator: TdxTableBorderCalculator; 
    procedure StartNewTable(ATable: TdxTable; AFirstContentInParentCell: Boolean); 
    procedure EnsureCurrentTableRow(ARow: TdxTableRow); 
    function CalculateTableActualLeftRelativeOffset(ATable: TdxTable): TdxModelUnit; 
    function GetTextBounds(ACell: TdxTableCell; ACellBounds: TdxLayoutRectangle): TdxLayoutRectangle; 
    function GetActualCellBottomMargin(ACell: TdxTableCell): TdxModelUnit; 
    function GetActualCellLeftMargin(ACell: TdxTableCell): TdxModelUnit; 
    function GetActualCellRightMargin(ACell: TdxTableCell): TdxModelUnit; 
    function CalculateBottomTextIndent(ARowSeparatorIndex: Integer; out ACellBordersInfo: TList<TdxHorizontalCellBordersInfo>): TdxLayoutUnit; 
    procedure ChangeCurrentCellInTheSameTable(ANewCell: TdxTableCell); 
    procedure PrepareGridCellBounds; 
    function PrepareCellsBounds(ALeftOffset: TdxLayoutUnit): TdxVerticalBorderPositions; 
    function GetWidth(AGrid: TdxTableGrid; AFirstColumn, ACount: Integer): TdxLayoutUnit; 
    procedure FinishCurrentTable; 
    procedure ProcessPendingCells(ATableViewInfo: TdxTableViewInfo; APendingCells: TList<TdxTableCellViewInfo>); 
    procedure FinishCurrentCell; 
    function CalcFirstInvalidRowIndexCore(AStartRowIndex: Integer): Integer; 

    procedure ShiftRow(APositions: TdxSortedList<TdxLayoutUnit>; ARow: TdxTableRow; AOffset: Integer); virtual; 
    procedure AddTopAnchor; virtual; 
    function GetRowOffset(ARow: TdxTableRow): Integer; virtual; 
  public
    constructor Create(ATablesController: TdxTablesController; AStartCell: TdxTableCell; AFirstContentInParentCell: Boolean); 

    class function GetActualCellSpacing(ARow: TdxTableRow): TdxModelUnit; 
    function GetActualCellTopMargin(ACell: TdxTableCell): TdxModelUnit; 

    function CanFitRowToColumn(ALastTextRowBottom: Integer; AColumn: TdxColumn): TdxCanFitCurrentRowToColumnResult; override; 
    function IsFirstTableRowViewInfoInColumn: Boolean; 
    procedure EnsureCurrentCell(ACell: TdxTableCell); override; 
    procedure OnCurrentRowFinished; override; 
    procedure LeaveCurrentTable(ABeforeRestart, ARoolbackParent: Boolean); 
    procedure EndParagraph(ALastRow: TdxRow); override; 
    procedure UpdateCurrentCellBottom(ABottom: TdxLayoutUnit); override; 
    procedure UpdateCurrentCellHeight(ARow: TdxRow); override; 
    procedure BeforeMoveRowToNextColumn; override; 
    procedure AfterMoveRowToNextColumn; override; 
    function RollbackToStartOfRowTableOnFirstCellRowColumnOverfull(AFirstTableRowViewInfoInColumn,
      AInnerMostTable: Boolean): TdxParagraphIndex; override; 
    function GetCurrentCell: TdxTableCell; 

    property CurrentCell: TdxTableCell read FCurrentCell; 
    property CurrentRow: TdxTableRow read FCurrentRow; 
    property CurrentTable: TdxTable read GetCurrentTable; 
    property RowsController: TdxRowsController read GetRowsController; 
    property UnitConverter: TdxDocumentModelUnitToLayoutUnitConverter read FUnitConverter; 
    property VerticalBordersCalculator: TdxTableCellVerticalBorderCalculator read FVerticalBordersCalculator; 
    property TableViewInfoManager: TdxTableViewInfoManager read FTableViewInfoManager; 
    property CurrentCellViewInfoEmpty: Boolean read GetCurrentCellViewInfoEmpty; 
  end;

  { TdxFloatingObjectSizeController }

  TdxFloatingObjectSizeController = class 
  private
    FPieceTable: TdxPieceTable; 
    function GetDocumentModel: TdxDocumentModel;
  protected
    function CalculateAbsoluteFloatingObjectBounds(ALocation: IdxFloatingObjectLocation): TRect; virtual;
    function ValidateRotatedShapeHorizontalPosition(const ARect: TRect; AProperties: TdxFloatingObjectProperties): TRect; virtual;
    function ValidateRotatedShapeVerticalPosition(const ARect: TRect; AProperties: TdxFloatingObjectProperties): TRect; virtual;

    property DocumentModel: TdxDocumentModel read GetDocumentModel; 
    property PieceTable: TdxPieceTable read FPieceTable; 
  public
    constructor Create(APieceTable: TdxPieceTable); 
    procedure UpdateFloatingObjectBox(AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox); 
    function CreateRotatedFloatingObjectLocation(AProperties: IdxFloatingObjectLocation; AAngle: Integer): IdxFloatingObjectLocation; 
    function CalculateAbsoluteFloatingObjectShapeBounds(ALocation: IdxFloatingObjectLocation; AShape: TdxShape;
      AIsTextBox: Boolean): TRect; virtual; 
    function CenterRectangleHorizontally(const ARectangle, AWhere: TRect): TRect; 
    function CenterRectangleVertically(const ARectangle, AWhere: TRect): TRect; 
    function CalculateAbsoluteFloatingObjectContentBounds(AFloatingObjectProperties: IdxFloatingObjectLocation;
      AShape: TdxShape; const AShapeBounds: TRect): TRect; virtual; 
    function CalculateActualAbsoluteFloatingObjectBounds(AFloatingObjectProperties: TdxFloatingObjectProperties; const ABounds: TRect): TRect; virtual; 
  end;

  { TdxFloatingObjectTargetPlacementInfo }

  TdxFloatingObjectTargetPlacementInfo = class 
  private
    FColumnBounds: TRect; 
    FOriginalColumnBounds: TRect; 
    FOriginX: Integer; 
    FOriginY: Integer; 
    FPageBounds: TRect; 
    FPageClientBounds: TRect; 
    procedure SetColumnBounds(const Value: TRect);
  public
    property ColumnBounds: TRect read FColumnBounds write SetColumnBounds; 
    property OriginalColumnBounds: TRect read FOriginalColumnBounds write FOriginalColumnBounds; 
    property OriginX: Integer read FOriginX write FOriginX; 
    property OriginY: Integer read FOriginY write FOriginY; 
    property PageBounds: TRect read FPageBounds write FPageBounds; 
    property PageClientBounds: TRect read FPageClientBounds write FPageClientBounds; 
  end;

  { TdxFloatingObjectSizeAndPositionController }

  TdxFloatingObjectSizeAndPositionController = class(TdxFloatingObjectSizeController) 
  private
    FRowsController: TdxRowsController; 
    function GetCurrentColumn: TdxColumn;
    function GetColumnController: IColumnController;
    function GetHorizontalPositionController: TdxCurrentHorizontalPositionController;
    function GetCurrentRow: TdxRow;
    function GetCurrentHorizontalPosition: Integer;
  protected
    function CalculateAbsoluteFloatingObjectBounds(ALocation: IdxFloatingObjectLocation): TRect; override;
    function CalculatePlacementInfo: TdxFloatingObjectTargetPlacementInfo; 
    function CalculateAbsoluteFloatingObjectX(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo; AActualWidth: Integer): Integer; virtual; 
    function CalculateAbsoluteFloatingObjectY(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo; AActualHeight: Integer): Integer; virtual; 
    function CalculateAbsoluteFloatingObjectWidth(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; virtual; 
    function CalculateAbsoluteFloatingObjectHeight(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; virtual; 
    function ValidateRotatedShapeHorizontalPosition(const AShapeBounds: TRect; AProperties: TdxFloatingObjectProperties): TRect; override;
    function ValidateRotatedShapeVerticalPosition(const AShapeBounds: TRect; AProperties: TdxFloatingObjectProperties): TRect; override;

    property RowsController: TdxRowsController read FRowsController; 
    property CurrentColumn: TdxColumn read GetCurrentColumn; 
    property ColumnController: IColumnController read GetColumnController; 
    property HorizontalPositionController: TdxCurrentHorizontalPositionController read GetHorizontalPositionController; 
    property CurrentRow: TdxRow read GetCurrentRow; 
    property CurrentHorizontalPosition: Integer read GetCurrentHorizontalPosition; 
  public
    constructor Create(ARowsController: TdxRowsController); 
  end;

  { TdxFloatingObjectHorizontalPositionCalculator }

  TdxFloatingObjectHorizontalPositionCalculator = class 
  private
    FUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter; 
    function CalculateAbsoluteFloatingObjectWidthCore(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
    function GetPercentBaseWidth(AFrom: TdxFloatingObjectRelativeFromHorizontal;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
    function CalculateFloatingObjectOffsetPercentBase(AType: TdxFloatingObjectHorizontalPositionType;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
    function CalculateAbsoluteFloatingObjectXCore(AHorizontalPositionType: TdxFloatingObjectHorizontalPositionType;
      AOffsetX: Integer; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
  protected
    function CalculateAlignBounds(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): TRect; 
    function CalculateAbsoluteFloatingObjectHorizontalAlignmentPosition(
      AAlignment: TdxFloatingObjectHorizontalPositionAlignment; AAlignBounds: TRect; AActualWidth: Integer): Integer; virtual; 
  public
    constructor Create(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter); 
    function CalculateAbsoluteFloatingObjectWidth(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; virtual; 
    function CalculateAbsoluteFloatingObjectX(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo; AActualWidth: Integer): Integer; virtual; 
    function CalculateFloatingObjectOffsetX(AHorizontalPositionType: TdxFloatingObjectHorizontalPositionType; X: Integer;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; virtual; 
  end;

  { TdxFloatingObjectVerticalPositionCalculator }

  TdxFloatingObjectVerticalPositionCalculator = class 
  private
    FUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter; 
    function CalculateAbsoluteFloatingObjectHeightCore(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
    function GetPercentBaseHeight(AFrom: TdxFloatingObjectRelativeFromVertical;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
    function CalculateFloatingObjectOffsetPercentBase(AType: TdxFloatingObjectVerticalPositionType;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
    function CalculateAbsoluteFloatingObjectYCore(AType: TdxFloatingObjectVerticalPositionType; AOffsetY: Integer;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; 
  protected
    function ValidateY(Y, AActualHeight: Integer; const ATargetBounds: TRect): Integer; virtual; 
    function CalculateAlignBounds(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): TRect; 
    function CalculateAbsoluteFloatingObjectVerticalAlignmentPosition(
      AAlignment: TdxFloatingObjectVerticalPositionAlignment; const AAlignBounds: TRect; AActualHeight: Integer): Integer; virtual; 
  public
    constructor Create(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter); 

    function CalculateAbsoluteFloatingObjectHeight(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; virtual; 
    function CalculateAbsoluteFloatingObjectY(ALocation: IdxFloatingObjectLocation;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo; AActualHeight: Integer): Integer; virtual; 
    function CalculateFloatingObjectOffsetY(AVerticalPositionType: TdxFloatingObjectVerticalPositionType; Y: Integer;
      APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer; virtual; 
  end;

  { TdxTextAreaCollectionEx }

  TdxTextAreaCollectionEx = class 
  private
    FInnerList: TList<TdxTextArea>; 
    procedure RemoveCore(AToRemove: TList<TdxTextArea>); 
    procedure AddCore(AToAdd: TList<TdxTextArea>); overload; 
    function GetCount: Integer;
    function GetItem(Index: Integer): TdxTextArea;
  protected
    function AddCore(const AInterval: TdxTextArea): Integer; overload; 
  public
    constructor Create;
    destructor Destroy; override;

    procedure Add(const AInterval: TdxTextArea); 
    function Remove(const AInterval: TdxTextArea): Boolean; 
    function Contains(const AInterval: TdxTextArea): Boolean; 
    procedure Sort; 

    property InnerList: TList<TdxTextArea> read FInnerList; 
    property Count: Integer read GetCount; 
    property Self[Index: Integer]: TdxTextArea read GetItem; default; 
  end;

  { TdxTextAreaAndXComparable }

  TdxTextAreaAndXComparable = class(TInterfacedObject, IdxComparable<TdxTextArea>) 
  private
    FX: Integer; 
  public
    constructor Create(X: Integer); 
    function CompareTo(const AOther: TdxTextArea): Integer; 
  end;

  { TdxFloatingObjectsLayout }

  TdxFloatingObjectsLayout = class 
  private
    FItems: TList<TdxFloatingObjectBox>; 
    FForegroundItems: TList<TdxFloatingObjectBox>; 
    FBackgroundItems: TList<TdxFloatingObjectBox>; 
    FRuns: TList<TdxFloatingObjectAnchorRun>; 
    FObjectsTable: TDictionary<TdxFloatingObjectAnchorRun, TdxFloatingObjectBox>; 
    FObjectToRunMapTable: TDictionary<TdxFloatingObjectBox, TdxFloatingObjectAnchorRun>; 
    function FindLeftMostX(AProcessedObjects: TList<TdxFloatingObjectBox>; AInitialX: Integer;
      const ABounds: TRect): Integer; 
    function FindRightMostX(AProcessedObjects: TList<TdxFloatingObjectBox>; AInitialX: Integer; const ABounds: TRect): Integer; 
  protected
    class var FHorizontalObjectComparer: IComparer<TdxFloatingObjectBox>; 
    class var FVerticalObjectComparer: IComparer<TdxFloatingObjectBox>; 
    class constructor Initialize;
  protected
    procedure Add(AFloatingObject: TdxFloatingObjectBox); overload;  
    procedure ClearFloatingObjects(ARunIndex: TdxRunIndex; AObjects: TList<TdxFloatingObjectBox>); overload;

    property Runs: TList<TdxFloatingObjectAnchorRun> read FRuns; 
  public
    constructor Create;
    destructor Destroy; override;
    function ContainsRun(AObjectAnchorRun: TdxFloatingObjectAnchorRun): Boolean; 
    function GetFloatingObject(AObjectAnchorRun: TdxFloatingObjectAnchorRun): TdxFloatingObjectBox; 
    procedure Add(AObjectAnchorRun: TdxFloatingObjectAnchorRun; AFloatingObject: TdxFloatingObjectBox); overload;
    function GetObjectsInRectangle(const ABounds: TRect): TList<TdxFloatingObjectBox>; overload; 
    function GetAllObjectsInRectangle(const ABounds: TRect): TList<TdxFloatingObjectBox>; 
    procedure GetObjectsInRectangle(AWhere: TList<TdxFloatingObjectBox>; ATo: TList<TdxFloatingObjectBox>;
      const ABounds: TRect); overload; 
    function CalculateTextAreas(AItems: TList<TdxFloatingObjectBox>; const ABounds: TRect): TList<TdxTextArea>; 
    procedure ProcessFloatingObject(AFloatingObject: TdxFloatingObjectBox; AProcessedObjects: TList<TdxFloatingObjectBox>;
      AResult: TdxTextAreaCollectionEx; const AInitialBounds: TRect); 
  public
    procedure Clear; 
    procedure ClearFloatingObjects(ARunIndex: TdxRunIndex); overload;
    function GetFloatingObjects(APieceTable: TdxPieceTable): TList<TdxFloatingObjectBox>; overload; 
    procedure GetFloatingObjects(AWhere: TList<TdxFloatingObjectBox>; APieceTable: TdxPieceTable;
      AObjects: TList<TdxFloatingObjectBox>); overload; 
    procedure MoveFloatingObjectsVertically(ADeltaY: Integer; APieceTable: TdxPieceTable); 

    property Items: TList<TdxFloatingObjectBox> read FItems; 
    property ForegroundItems: TList<TdxFloatingObjectBox> read FForegroundItems; 
    property BackgroundItems: TList<TdxFloatingObjectBox> read FBackgroundItems; 
  end;

implementation

uses
  dxRichEdit.Utils.BatchUpdateHelper, 
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  cxGeometry, dxRichEdit.DocumentModel.ParagraphRange, Math, dxRichEdit.DocumentModel.TextRange, Forms,
  RTLConsts, dxTypeHelpers, dxRichEdit.Utils.Characters,
  dxRichEdit.Utils.ChunkedStringBuilder, dxRichEdit.Platform.Font, dxRichEdit.Utils.Colors,
  dxRichEdit.DocumentLayout.BottomTextIndentCalculator, dxRichEdit.DocumentModel.TableFormatting,
  dxRichEdit.DocumentModel.SectionRange;

{ TdxTextArea }

constructor TdxTextArea.Create(AStart, AEnd: Integer);
begin
  Start := AStart;
  &End  := AEnd;
end;

function TdxTextArea.GetEmpty: TdxTextArea;
begin
  Result.Start := 0;
  Result.&End := 0;
end;

function TdxTextArea.GetWidth: Integer;
begin
  Result := &End - Start;
end;

function TdxTextArea.IntersectsWith(const AInterval: TdxTextArea): Boolean;
begin
  Result := (AInterval.&End >= Start) and (AInterval.Start <= &End);
end;

function TdxTextArea.IntersectsWithExcludingBounds(const AInterval: TdxTextArea): Boolean;
begin
  Result := (AInterval.&End > Start) and (AInterval.Start < &End);
end;

class function TdxTextArea.Union(const AInterval1, AInterval2: TdxTextArea): TdxTextArea;
begin
  Result := TdxTextArea.Create(Min(AInterval1.Start, AInterval2.Start), Max(AInterval1.&End, AInterval2.&End));
end;

function TdxTextArea.Subtract(const AInterval: TdxTextArea): TList<TdxTextArea>;
var
  _result: TdxTextAreaCollectionEx;
begin
  _result := TdxTextAreaCollectionEx.Create;
  if not IntersectsWithExcludingBounds(AInterval) then
  begin
    _result.Add(Self);
    Result := _result.InnerList;
    Exit;
  end;
  if AInterval.Contains(Self) then
    begin
      Result := _result.InnerList;
      Exit;
    end;
  if Self.Contains(AInterval) then
  begin
    if (Self.Start <> AInterval.Start) then
      _result.Add(TdxTextArea.Create(Self.Start, AInterval.Start));
    if (Self.&End <> AInterval.&End) then
      _result.Add(TdxTextArea.Create(AInterval.&End, Self.&End));
    Result := _result.InnerList;
    Exit;
  end;
  if (Self.Start >= AInterval.Start) then
    if (Self.&End <> AInterval.&End) then
      _result.Add(TdxTextArea.Create(AInterval.&End, Self.&End))
  else
    if (Self.Start <> AInterval.Start) then
      _result.Add(TdxTextArea.Create(Self.Start, AInterval.Start));
  Result := _result.InnerList;
end;

function TdxTextArea.Contains(const AInterval: TdxTextArea): Boolean;
begin
  Result := (AInterval.Start >= Start) and (AInterval.&End <= &End);
end;

function TdxTextArea.ToString: string;
begin
  Result := Format('[%d - %d]', [Start, &End]);
end;

{ TdxRichEditFormattingProcessResult }

procedure TdxRichEditFormattingProcessResult.Init(AFormattingProcess: TdxFormattingProcess);
begin
  FormattingProcess := AFormattingProcess;
end;

procedure TdxRichEditFormattingProcessResult.Init(AParagraphIndex: TdxParagraphIndex);
begin
  Init(TdxFormattingProcess.ContinueFromParagraph);
  ParagraphIndex := AParagraphIndex;
end;

procedure TdxRichEditFormattingProcessResult.Init(const ARestartPosition: TdxDocumentModelPosition);
begin
  Init(TdxFormattingProcess.RestartFromTheStartOfRow);
  RestartPosition := ARestartPosition;
end;

{ TdxParagraphBoxIterator }

function TdxParagraphBoxIterator.CreatePosition: TdxFormatterPosition;
begin
  Result.CopyFrom(0, 0, 0);
end;

function TdxParagraphBoxIterator.GetCurrentBox: TdxBox;
begin
  Result := Paragraph.BoxCollection[FBoxIndex];
end;

function TdxParagraphBoxIterator.GetCurrentPosition: TdxFormatterPosition;
begin
  Result := inherited GetCurrentPosition;
  Result.BoxIndex := BoxIndex;
end;

function TdxParagraphBoxIterator.GetPreviousOffsetPosition: TdxFormatterPosition;
var
  APos: TdxFormatterPosition;
begin
  APos := inherited GetPreviousOffsetPosition;
  if (APos.Offset < CurrentBox.StartPos.Offset) then
  begin
    APos.BoxIndex := BoxIndex - 1;
  end
  else
    APos.BoxIndex := BoxIndex;
  Result := APos;
end;

function TdxParagraphBoxIterator.GetPreviousVisibleRunPosition: TdxFormatterPosition;
var
  APos: TdxFormatterPosition;
begin
  APos := inherited GetPreviousVisibleRunPosition;
  APos.BoxIndex := FBoxIndex - 1;
  Result := APos;
end;

function TdxParagraphBoxIterator.Next: TdxParagraphIteratorResult;
begin
  Result := inherited Next;
  if (Offset > CurrentBox.EndPos.Offset) or (Result <> TdxParagraphIteratorResult.Success) then
    NextBoxCore;
end;

procedure TdxParagraphBoxIterator.NextBox;
begin
  NextBoxCore;
  inherited SetPosition(CurrentBox.StartPos);
end;

procedure TdxParagraphBoxIterator.NextBoxCore;
begin
  if BoxIndex < Paragraph.BoxCollection.Count - 1 then
    BoxIndex := BoxIndex + 1
end;

procedure TdxParagraphBoxIterator.SetNextPosition(APrevPosition: TdxFormatterPosition);
begin
  SetPosition(APrevPosition.RunIndex, APrevPosition.Offset + 1);
  BoxIndex := APrevPosition.BoxIndex;
end;

procedure TdxParagraphBoxIterator.SetPosition(APos: TdxFormatterPosition);
begin
  inherited SetPosition(APos);
  BoxIndex := APos.BoxIndex;
end;

{ TdxParagraphBoxFormatter }

procedure TdxParagraphBoxFormatter.BeginParagraph(ABeginFromParagraphStart: Boolean);
var
  AParagraph: TdxParagraph;
  AShouldAddPageBreak: Boolean;
begin
  AParagraph := Iterator.Paragraph;
  FParagraphStartPos.CopyFrom(AParagraph.FirstRunIndex, 0, 0);
  FParagraphStartRowCount := RowsController.CurrentColumn.Rows.Count;
  AShouldAddPageBreak := ABeginFromParagraphStart and ShouldPageBreakBeforeParagraph(AParagraph) and
    (FParagraphStartRowCount > 0) and PieceTable.IsMain and CanBreakPageBefore(AParagraph);
  if AShouldAddPageBreak then
    RowsController.AddPageBreakBeforeRow;
  RowsController.BeginParagraph(AParagraph, ABeginFromParagraphStart);
  FSuppressHyphenation := True; 
  if AShouldAddPageBreak then
    RowsController.OnPageBreakBeforeParagraph;
end;

procedure TdxParagraphBoxFormatter.BeginParagraphFormatting(AIterator: TdxParagraphBoxIterator;
  ABeginFromParagraphStart: Boolean);
begin
  Iterator := AIterator;
  HasDeferredNumberingListBox := False;
  BeginParagraph(ABeginFromParagraphStart);
  ChangeState(GetInitialStateType(ABeginFromParagraphStart));
end;

function TdxParagraphBoxFormatter.CanBreakPageBefore(AParagraph: TdxParagraph): Boolean;
var
  ACell: TdxTableCell;
begin
  if not RowsController.TablesController.IsInsideTable then
    Exit(True);
  ACell := AParagraph.GetCell;
  if ACell = nil then
    Exit(True);
  while ACell.Table.ParentCell <> nil do
    ACell := ACell.Table.ParentCell;
  Result := ACell.IsFirstCellInRow and (ACell.StartParagraphIndex = AParagraph.Index);
end;

function TdxParagraphBoxFormatter.GetNewState(AStateType: TdxParagraphBoxFormatterState): TdxBoxFormatterStateBase;
var
  APreviousState: TdxBoxFormatterStateBase;
begin
  APreviousState := State;
  Result := FStates[AStateType];
  Result.PreviousState := APreviousState;
end;

procedure TdxParagraphBoxFormatter.ChangeState(AStateType: TdxParagraphBoxFormatterState);
begin
  case AStateType of
    TdxParagraphBoxFormatterState.RowEmpty:
      begin
        StartNewRow;
        State := GetNewState(TdxParagraphBoxFormatterState.RowEmpty);
      end;
    TdxParagraphBoxFormatterState.RowEmptyAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowEmptyAfterFloatingObject);
    TdxParagraphBoxFormatterState.ParagraphStart:
      begin
        StartNewRow;
        State := GetNewState(TdxParagraphBoxFormatterState.ParagraphStart);
      end;
    TdxParagraphBoxFormatterState.ParagraphStartAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.ParagraphStartAfterFloatingObject);
    TdxParagraphBoxFormatterState.ParagraphStartFromTheMiddle:
      begin
        StartNewRow;
        State := GetNewState(TdxParagraphBoxFormatterState.ParagraphStartFromTheMiddle);
      end;
    TdxParagraphBoxFormatterState.ParagraphStartFromTheMiddleAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.ParagraphStartFromTheMiddleAfterFloatingObject);
    TdxParagraphBoxFormatterState.RowWithSpacesOnly:
      State := GetNewState(TdxParagraphBoxFormatterState.RowWithSpacesOnly);
    TdxParagraphBoxFormatterState.RowWithTextOnly:
      begin
        State := GetNewState(TdxParagraphBoxFormatterState.RowWithTextOnly);
        StartNewWord;
      end;
    TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowWithTextOnly); 
    TdxParagraphBoxFormatterState.RowWithDashOnly:
      begin
        StartNewWord;
        State := GetNewState(TdxParagraphBoxFormatterState.RowWithDashOnly);
      end;
    TdxParagraphBoxFormatterState.RowWithDashAfterTextOnly:
      begin
        StartNewWord;
        State := GetNewState(TdxParagraphBoxFormatterState.RowWithDashAfterTextOnly);
      end;
    TdxParagraphBoxFormatterState.RowWithDashOnlyAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowWithDashOnly); 
    TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab:
      begin
        State := GetNewState(TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab);
        StartNewWord;
      end;
    TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTabAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab); 
    TdxParagraphBoxFormatterState.RowFirstLeadingTab:
      State := GetNewState(TdxParagraphBoxFormatterState.RowFirstLeadingTab);
    TdxParagraphBoxFormatterState.RowLeadingTab:
      begin
        StartNewTab;
        State := GetNewState(TdxParagraphBoxFormatterState.RowLeadingTab);
      end;
    TdxParagraphBoxFormatterState.RowLeadingTabAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowLeadingTab); 
    TdxParagraphBoxFormatterState.RowTab:
      begin
        StartNewTab;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTab);
      end;
    TdxParagraphBoxFormatterState.RowTabAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowTab); 
    TdxParagraphBoxFormatterState.RowSpaces:
      begin
        ResetLastTabPosition;
        State := GetNewState(TdxParagraphBoxFormatterState.RowSpaces);
      end;
    TdxParagraphBoxFormatterState.RowText:
      begin
        StartNewWord;
        State := GetNewState(TdxParagraphBoxFormatterState.RowText);
      end;
    TdxParagraphBoxFormatterState.RowTextAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowText); 
    TdxParagraphBoxFormatterState.RowDashAfterText:
      State := GetNewState(TdxParagraphBoxFormatterState.RowDashAfterText);
    TdxParagraphBoxFormatterState.RowDash:
      begin
        StartNewWord;
        State := GetNewState(TdxParagraphBoxFormatterState.RowDash);
      end;
    TdxParagraphBoxFormatterState.RowDashAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowDash); 
    TdxParagraphBoxFormatterState.RowTextSplit:
      begin
        StartNewRow;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextSplit);
      end;
    TdxParagraphBoxFormatterState.RowTextSplitAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowTextSplitAfterFloatingObject);
    TdxParagraphBoxFormatterState.RowTextSplitAfterFirstLeadingTab:
      begin
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextSplitAfterFirstLeadingTab);
        StartNewWord;
      end;
    TdxParagraphBoxFormatterState.RowTextSplitAfterFirstLeadingTabAfterFloatingObject:
      State := GetNewState(TdxParagraphBoxFormatterState.RowTextSplitAfterFirstLeadingTab); 
    TdxParagraphBoxFormatterState.RowDashSplit:
      begin
        StartNewRow;
        State := GetNewState(TdxParagraphBoxFormatterState.RowDashSplit);
      end;
    TdxParagraphBoxFormatterState.RowDashSplitAfterFloatingObject:
        State := GetNewState(TdxParagraphBoxFormatterState.RowDashSplitAfterFloatingObject);
    TdxParagraphBoxFormatterState.RowEmptyHyphenation:
      begin
        UpdateSyllableIterator;
        StartNewRow;
        State := GetNewState(TdxParagraphBoxFormatterState.RowEmptyHyphenation);
      end;
    TdxParagraphBoxFormatterState.RowEmptyHyphenationAfterFloatingObject:
      begin
        UpdateSyllableIterator;
        State := GetNewState(TdxParagraphBoxFormatterState.RowEmptyHyphenationAfterFloatingObject);
      end;
    TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllable:
      begin
        UpdateSyllableIterator;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllable);
        StartNewWord;
      end;
    TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFloatingObject:
      begin
        UpdateSyllableIterator;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllable); 
      end;
    TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFirstLeadingTab:
      begin
        UpdateSyllableIterator;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFirstLeadingTab);
        StartNewWord;
      end;
    TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFirstLeadingTabAfterFloatingObject:
      begin
        UpdateSyllableIterator;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFirstLeadingTab); 
      end;
    TdxParagraphBoxFormatterState.RowTextHyphenation:
      begin
        UpdateSyllableIterator;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextHyphenation);
        StartNewWord;
      end;
    TdxParagraphBoxFormatterState.RowTextHyphenationAfterFloatingObject:
      begin
        UpdateSyllableIterator;
        State := GetNewState(TdxParagraphBoxFormatterState.RowTextHyphenation); 
      end;
    TdxParagraphBoxFormatterState.RowLineBreak:
      State := GetNewState(TdxParagraphBoxFormatterState.RowLineBreak);
    TdxParagraphBoxFormatterState.RowPageBreak:
      State := GetNewState(TdxParagraphBoxFormatterState.RowPageBreak);
    TdxParagraphBoxFormatterState.RowColumnBreak:
      State := GetNewState(TdxParagraphBoxFormatterState.RowColumnBreak);
    TdxParagraphBoxFormatterState.FloatingObject:
      begin
        StartFloatingObjects;
        State := GetNewState(TdxParagraphBoxFormatterState.FloatingObject);
      end;
    TdxParagraphBoxFormatterState.SectionBreakAfterParagraphMark:
      State := GetNewState(TdxParagraphBoxFormatterState.SectionBreakAfterParagraphMark);
    TdxParagraphBoxFormatterState.Final:
      State := GetNewState(TdxParagraphBoxFormatterState.Final);
  end;
end;

procedure TdxParagraphBoxFormatter.ChangeStateContinueFromParagraph(AParagraphIndex: TdxParagraphIndex);
begin
  TdxStateContinueFormattingFromParagraph(FStates[TdxParagraphBoxFormatterState.ContinueFromParagraph]).FParagraphIndex := AParagraphIndex;
  State := FStates[TdxParagraphBoxFormatterState.ContinueFromParagraph];
end;

procedure TdxParagraphBoxFormatter.ClearSyllableIterator;
begin
  FSyllableIterator := nil;
end;

constructor TdxParagraphBoxFormatter.Create(APieceTable: TdxPieceTable; AMeasurer: TdxBoxMeasurer;
  ARowsController: TdxRowsController);
begin
  inherited Create(APieceTable, AMeasurer);
  InvalidPosition.CopyFrom(-1, -1, -1);
  FMaxHeight := MaxInt;

  Initialize(ARowsController);
end;

destructor TdxParagraphBoxFormatter.Destroy;
begin
  FreeAndNil(FTableStates);
  inherited Destroy;
end;


procedure TdxParagraphBoxFormatter.EndParagraph;
begin
  if Iterator.CurrentChar = TdxCharacters.ParagraphMark then
    RowsController.EndParagraph
  else
    RowsController.EndSection;
end;

procedure TdxParagraphBoxFormatter.EndParagraphFormatting;
begin
  EndParagraph;
end;

function TdxParagraphBoxFormatter.FormatNextRow: TdxFormattingProcessResult;
var
  ARestartPosition: TdxDocumentModelPosition;
  AContinueFormatResult: TdxStateContinueFormatResult;
begin
  repeat
    AContinueFormatResult := State.ContinueFormat;
    if AContinueFormatResult <> TdxStateContinueFormatResult.Success then
    begin
      if AContinueFormatResult = TdxStateContinueFormatResult.RestartDueFloatingObject then
      begin
        FLastRestartDueToFloatingObjectParagraphStartPos := FParagraphStartPos;
        ARestartPosition := RowsController.RestartModelPosition;
        RowsController.RestartModelPosition.Invalidate;
        FHasUnapprovedFloatingObjects := False;
        Result.Init(ARestartPosition);
        Exit;
      end
      else
      begin
        RowsController.ColumnController.PageAreaController.PageController.SetPageLastRunIndex(FLastRestartDueToFloatingObjectParagraphStartPos.RunIndex - 1);
        RowsController.RestartModelPosition.Invalidate;
        Result.Init(RowsController.LastRestartDueToFloatingObjectModelPosition);
        Exit;
      end;
    end;
    case State.&Type of
      TdxParagraphBoxFormatterState.RowEmpty, TdxParagraphBoxFormatterState.RowEmptyHyphenation:
        begin
          Result.Init(TdxFormattingProcess.Continue); 
          Break;
        end;
      TdxParagraphBoxFormatterState.Final:
        begin
          Result.Init(TdxFormattingProcess.Finish);
          Break;
        end;
      TdxParagraphBoxFormatterState.ContinueFromParagraph:
        begin
          Result.Init(TdxStateContinueFormattingFromParagraph(State).ParagraphIndex); 
          Break;
        end;
    end;
  until False;
end;

function TdxParagraphBoxFormatter.GetActiveIterator: TdxParagraphBoxIterator;
var
  AIterator: TdxParagraphIteratorBase;
begin
  Result := TdxParagraphBoxIterator(Iterator);
  if State <> nil then
  begin
    AIterator := State.Iterator;
    if AIterator <> nil then
      Result := TdxParagraphBoxIterator(AIterator);
  end;
end;

function TdxParagraphBoxFormatter.GetCurrentRow: TdxRow;
begin
  Result := RowsController.CurrentRow;
end;

function TdxParagraphBoxFormatter.GetInitialStateType(ABeginFromParagraphStart: Boolean): TdxParagraphBoxFormatterState;
var
  ARuns: TdxTextRunCollection;
  APrevParagraph: TdxParagraph;
begin
  if ABeginFromParagraphStart then
    Result := TdxParagraphBoxFormatterState.ParagraphStart
  else
    Result := TdxParagraphBoxFormatterState.ParagraphStartFromTheMiddle;

  ARuns := PieceTable.Runs;
  if not (ARuns[Iterator.RunIndex] is TdxSectionRun) then
    Exit;

  if ARuns[Iterator.RunIndex] <> ARuns.First then
    if ARuns[Iterator.RunIndex - 1].ClassType = TdxParagraphRun then
    begin
      APrevParagraph := ARuns[Iterator.RunIndex - 1].Paragraph;
      if not APrevParagraph.IsInCell and (ARuns[Iterator.RunIndex - 1].Paragraph.FrameProperties = nil) then
        Exit(TdxParagraphBoxFormatterState.SectionBreakAfterParagraphMark);
    end;
end;

procedure TdxParagraphBoxFormatter.Initialize(ARowsController: TdxRowsController);
begin
  if ARowsController = nil then
    raise Exception.Create('ARowsController = nil'); 
  FRowsController := ARowsController;
  FTableStates := TdxParagraphBoxFormatterTableStateBaseStack.Create;
  FTableStates.Push(TdxParagraphBoxFormatterTextState.Create(Self));
  SubscribeRowsControllerEvents;
end;

procedure TdxParagraphBoxFormatter.OnTableEnd(ASender: TObject; AE: TdxEventArgs);
begin
  FTableStates.Pop;
end;

procedure TdxParagraphBoxFormatter.OnTableStart(ASender: TObject; AE: TdxEventArgs);
begin
  FTableStates.Push(TdxParagraphBoxFormatterTableState.Create(Self));
end;

procedure TdxParagraphBoxFormatter.ResetLastTabPosition;
begin
  FLastTabStartPos := InvalidPosition;
end;

procedure TdxParagraphBoxFormatter.RollbackToLastTab;
var
  AIterator: TdxParagraphBoxIterator;
begin
  AIterator := GetActiveIterator;
  RollbackToLastTab(AIterator);
end;

procedure TdxParagraphBoxFormatter.RollbackToLastTab(AIter: TdxParagraphBoxIterator);
begin
  if not FLastTabStartPos.AreEqual(InvalidPosition) then
    AIter.SetPosition(FLastTabStartPos);
end;

function TdxParagraphBoxFormatter.RollbackToParagraphStart(APos: TdxFormatterPosition; AInitialRowCount: Integer): TdxCompleteFormattingResult;
var
  ARows: TdxRowCollection;
begin
  Result := RowsController.CompleteCurrentColumnFormatting;
  if Result <> TdxCompleteFormattingResult.Success then
    Exit;

  RollbackToPositionAndClearLastRow(APos);
  ARows := RowsController.CurrentColumn.Rows;
  ARows.DeleteRange(AInitialRowCount, ARows.Count - AInitialRowCount);
  RowsController.MoveRowToNextColumn;
  ChangeState(TdxParagraphBoxFormatterState.ParagraphStart);
end;

procedure TdxParagraphBoxFormatter.RollbackToPositionAndClearLastRow(APos: TdxFormatterPosition);
var
  AIterator: TdxParagraphBoxIterator;
begin
  AIterator := GetActiveIterator;
  AIterator.SetPosition(APos);
  RowsController.ClearRow(True);
  ResetLastTabPosition;
end;

function TdxParagraphBoxFormatter.RollbackToStartOfRow(ACanFit: TdxCanFitCurrentRowToColumnResult): TdxCompleteFormattingResult;
var
  ARollbackToParagraphStart: Boolean;
  ANextState: TdxParagraphBoxFormatterState;
begin
  ARollbackToParagraphStart := (RowStartPos.RunIndex = Iterator.Paragraph.FirstRunIndex) and (RowStartPos.Offset = 0);
  if ARollbackToParagraphStart then
    ANextState := TdxParagraphBoxFormatterState.ParagraphStart
  else
    ANextState := TdxParagraphBoxFormatterState.RowEmpty;
  Result := RollbackToStartOfRowCore(ACanFit, ANextState);
end;

function TdxParagraphBoxFormatter.RollbackToStartOfRowCore(ACanFit: TdxCanFitCurrentRowToColumnResult; ANextState: TdxParagraphBoxFormatterState): TdxCompleteFormattingResult;
var
  AStartParagraphIndex: TdxParagraphIndex;
begin
  FHasUnapprovedFloatingObjects := False;
  if ACanFit = TdxCanFitCurrentRowToColumnResult.FirstCellRowNotFitted then
  begin
    RollbackToPositionAndClearLastRow(RowStartPos);
    AStartParagraphIndex := RollbackToStartOfRowTable(ACanFit);
    ChangeStateContinueFromParagraph(AStartParagraphIndex);
  end
  else
    if ACanFit = TdxCanFitCurrentRowToColumnResult.TextAreasRecreated then
    begin
      RollbackToPositionAndClearLastRow(RowStartPos);
      ChangeState(ANextState);
    end
    else
    begin
      if (FParagraphStartRowCount > 0) and RowsController.Paragraph.KeepLinesTogether and PieceTable.IsMain and not RowsController.TablesController.IsInsideTable then
      begin
        Result := RollbackToParagraphStart(ParagraphStartPos, FParagraphStartRowCount);
        if Result <> TdxCompleteFormattingResult.Success then
          Exit;
        FParagraphStartRowCount := -1;
      end
      else
      begin
        Result := RowsController.CompleteCurrentColumnFormatting;
        if Result <> TdxCompleteFormattingResult.Success then
          Exit;
        RollbackToPositionAndClearLastRow(RowStartPos);
        RowsController.MoveRowToNextColumn;
        ChangeState(ANextState);
      end;
    end;
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxParagraphBoxFormatter.RollbackToStartOfRowTable(
  ACanFit: TdxCanFitCurrentRowToColumnResult): TdxParagraphIndex;
begin
  Result := RowsController.TablesController.RollbackToStartOfRowTableOnFirstCellRowColumnOverfull;
end;

procedure TdxParagraphBoxFormatter.RollbackToStartOfWord(AIter: TdxParagraphBoxIterator);
begin
  AIter.SetPosition(WordStartPos);
  RowsController.RemoveLastTextBoxes;
end;

procedure TdxParagraphBoxFormatter.CreateStates;
const
  StateClasses: array[TdxParagraphBoxFormatterState] of TdxBoxFormatterStateBaseClass = (
    TdxStateParagraphStart,                                      
    TdxStateParagraphStartFromTheMiddle,                         
    TdxStateParagraphStartAfterFloatingObject,                   
    TdxStateParagraphStartFromTheMiddleAfterFloatingObject,      
    TdxStateRowEmpty,                                            
    TdxStateRowEmptyAfterFloatingObject,                         

    TdxStateRowWithSpacesOnly,                                   
    TdxStateRowWithTextOnly,                                     
    nil,                                                         
    TdxStateRowWithDashOnly,                                     
    nil,                                                         
    TdxStateRowWithTextOnlyAfterFirstLeadingTab,                 
    nil,                                                         

    TdxStateRowSpaces,                                           
    TdxStateRowText,                                             
    nil,                                                         
    TdxStateRowDashAfterText,                                    
    TdxStateRowDash,                                             
    nil,                                                         
    TdxStateRowWithDashAfterTextOnly,                            
    TdxStateRowTextSplit,                                        
    nil,                                                         
    nil,                                                         
    TdxStateRowTextSplitAfterFirstLeadingTab,                    
    nil,                                                         

    TdxStateRowDashSplit,                                        
    TdxStateRowDashSplitAfterFloatingObject,                     

    TdxStateRowEmptyHyphenation,                                 
    TdxStateRowEmptyHyphenationAfterFloatingObject,              
    TdxStateRowTextHyphenationFirstSyllable,                     
    nil,                                                         
    TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab, 
    nil,                                                         
    TdxStateRowTextHyphenation,                                  
    nil,                                                         
    TdxStateRowFirstLeadingTab,                                  
    TdxStateRowLeadingTab,                                       
    nil,                                                         
    TdxStateRowTab,                                              
    nil,                                                         
    TdxStateRowLineBreak,                                        
    TdxStateRowPageBreak,                                        
    TdxStateRowColumnBreak,                                      

    TdxStateSectionBreakAfterParagraphMark,                      
    TdxStateFloatingObject,                                      

    TdxStateContinueFormattingFromParagraph,                     

    TdxStateFinal                                                
  );
var
  I: TdxParagraphBoxFormatterState;
begin
  for I := Low(TdxParagraphBoxFormatterState) to High(TdxParagraphBoxFormatterState) do
    if StateClasses[I] <> nil then
      FStates[I] := StateClasses[I].Create(Self);
end;

procedure TdxParagraphBoxFormatter.DestroyStates;
var
  I: TdxParagraphBoxFormatterState;
begin
  for I := Low(TdxParagraphBoxFormatterState) to High(TdxParagraphBoxFormatterState) do
    FStates[I].Free;
end;

function TdxParagraphBoxFormatter.GetIteratorClass: TdxIteratorClass;
begin
  Result := TdxParagraphBoxIterator;
end;

procedure TdxParagraphBoxFormatter.ApproveFloatingObjects;
begin
  FHasUnapprovedFloatingObjects := False;
end;

procedure TdxParagraphBoxFormatter.RollbackUnapprovedFloatingObjects;
begin
  if FHasUnapprovedFloatingObjects then
  begin
    GetActiveIterator.SetPosition(FUnapprovedFloatingObjectsStartPos);
    FHasUnapprovedFloatingObjects := False;
  end;
end;

procedure TdxParagraphBoxFormatter.StartFloatingObjects;
begin
  if not FHasUnapprovedFloatingObjects then
  begin
    FUnapprovedFloatingObjectsStartPos := GetActiveIterator.GetCurrentPosition;
    FHasUnapprovedFloatingObjects := True;
  end;
end;

procedure TdxParagraphBoxFormatter.RollbackToStartOfWord;
begin
  RollbackToStartOfWord(GetActiveIterator);
end;

function TdxParagraphBoxFormatter.ShouldPageBreakBeforeParagraph(AParagraph: TdxParagraph): Boolean;
var
  APageBreakBefore: Boolean;
begin
  APageBreakBefore := AParagraph.PageBreakBefore;
  if (not APageBreakBefore) or (AParagraph.Length > 1) then
    Result := APageBreakBefore
  else
    Result := not (PieceTable.Runs[AParagraph.FirstRunIndex] is TdxSectionRun);
end;

procedure TdxParagraphBoxFormatter.StartNewRow;
begin
  FRowStartPos := GetActiveIterator.GetCurrentPosition;
  ResetLastTabPosition;
  StartNewWord;
end;

procedure TdxParagraphBoxFormatter.StartNewTab;
begin
  StartNewWord;
  FLastTabStartPos := GetActiveIterator.GetCurrentPosition;
end;

procedure TdxParagraphBoxFormatter.StartNewWord;
begin
  FWordStartPos := GetActiveIterator.GetCurrentPosition;
  RowsController.StartNewWord;
end;

procedure TdxParagraphBoxFormatter.SubscribeRowsControllerEvents;
begin
  FRowsController.TableStarted.Add(OnTableStart); 
end;

procedure TdxParagraphBoxFormatter.UnsubscribeRowsControllerEvents;
begin
  FRowsController.TableStarted.Remove(OnTableStart); 
end;

procedure TdxParagraphBoxFormatter.UpdateSyllableIterator;
begin
  if FSyllableIterator = nil then
    FSyllableIterator := TdxSyllableBoxIterator.Create(Iterator as TdxParagraphBoxIterator, FHyphenationService);
end;

{ TdxRichEditCharacterFormatterStateBase }

constructor TdxCharacterFormatterStateBase.Create(
  AFormatter: TdxParagraphCharacterFormatter);
begin
  inherited Create;
  Assert(AFormatter <> nil);
  FFormatter := AFormatter;
end;

procedure TdxCharacterFormatterStateBase.AddBox(AType: TdxBoxClass; var ABoxInfo: TdxBoxInfo; AMeasured: Boolean);
var
  ABox: TdxBox;
begin
  ABox := AType.CreateBox;
  ABox.StartPos := ABoxInfo.StartPos;
  ABox.EndPos := ABoxInfo.EndPos;
  if AMeasured then
    ABox.Bounds := TRect.Create(0, 0, ABoxInfo.Size.cx, ABoxInfo.Size.cy)
  else
  begin
    Formatter.AddTextBoxToQueue(ABox, ABoxInfo);
    ABoxInfo := nil;
  end;
  Paragraph.BoxCollection.Add(ABox);
end;

procedure TdxCharacterFormatterStateBase.AppendBoxCore(AType: TdxBoxClass; var ABoxInfo: TdxBoxInfo);
var
  AMeasured: Boolean;
begin
  AMeasured := False;
  if AType <> TdxTextBox then 
  begin
    if AType <> TdxLayoutDependentTextBox then 
      MeasureBoxContent(ABoxInfo);
    AMeasured := True;
  end;
  AddBox(AType, ABoxInfo, AMeasured);
  SwitchToNextState;
end;

procedure TdxCharacterFormatterStateBase.ChangeState(AStateType: TdxCharacterFormatterState);
begin
  Formatter.ChangeState(AStateType);
end;

function TdxCharacterFormatterStateBase.ContinueFormat: Boolean;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  try
    ABoxInfo.StartPos := Iterator.GetCurrentPosition;
    Result := ContinueFormatByCharacter(ABoxInfo, nil) = TdxStateContinueFormatResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxCharacterFormatterStateBase.CreateParagraphMarkBoxInfo: TdxBoxInfo;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.StartPos := Iterator.GetCurrentPosition();
  ABoxInfo.EndPos := ABoxInfo.StartPos;
  Measurer.MeasureParagraphMark(ABoxInfo);
  Result := ABoxInfo;
end;

function TdxCharacterFormatterStateBase.CreateSectionMarkBoxInfo: TdxBoxInfo;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.StartPos := Iterator.GetCurrentPosition;
  ABoxInfo.EndPos := ABoxInfo.StartPos;
  Measurer.MeasureSectionMark(ABoxInfo);
  Result := ABoxInfo;
end;

function TdxCharacterFormatterStateBase.FinishParagraph: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateParagraphMarkBoxInfo;
  try
    AddBox(TdxParagraphMarkBox, ABoxInfo, True);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

procedure TdxCharacterFormatterStateBase.FinishParagraphCore(AIterator: TdxParagraphIteratorBase);
begin
end;

function TdxCharacterFormatterStateBase.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    AddBox(TdxSectionMarkBox, ABoxInfo, True);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxCharacterFormatterStateBase.GetCharacterFormatterState: TdxCharacterFormatterState;
begin
  Result := &Type;
end;

function TdxCharacterFormatterStateBase.GetFormattingComplete: Boolean;
begin
  Result := False;
end;

function TdxCharacterFormatterStateBase.GetIterator: TdxParagraphIteratorBase;
begin
  Result := Formatter.Iterator;
end;

function TdxCharacterFormatterStateBase.GetDashState: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.FirstDash;
end;

function TdxCharacterFormatterStateBase.GetIteratorClass: TdxIteratorClass;
begin
  Result := TdxParagraphCharacterIterator; 
end;

function TdxCharacterFormatterStateBase.GetMeasurer: TdxBoxMeasurer;
begin
  Result := Formatter.Measurer;
end;

function TdxCharacterFormatterStateBase.GetNextState: TdxCharacterFormatterState;
var
  ACurrentRun: TdxTextRunBase;
begin
  ACurrentRun := Formatter.PieceTable.Runs[Iterator.RunIndex];
  if IsInlineObjectRun(ACurrentRun) then
    Result := TdxCharacterFormatterState.InlineObject
  else
    if IsSeparatorRun(ACurrentRun) then
      Result := TdxCharacterFormatterState.Separator
    else
      if IsFloatingObjectRun(ACurrentRun) then
        Result := TdxCharacterFormatterState.FloatingObject
      else
        if IsLayoutDependentTextRun(ACurrentRun) then
          Result := TdxCharacterFormatterState.LayoutDependentText
        else
          case Iterator.CurrentChar of
            TdxCharacters.ParagraphMark:
              begin
                if IsParagraphMarkRun(ACurrentRun) then
                begin
                  FinishParagraph;
                  Result := TdxCharacterFormatterState.Final;
                end
                else
                  Result := TdxCharacterFormatterState.Text;
              end;
            TdxCharacters.SectionMark:
              begin
                if IsParagraphMarkRun(ACurrentRun) then
                begin
                  FinishSection;
                  Result := TdxCharacterFormatterState.Final;
                end
                else
                  Result := TdxCharacterFormatterState.Text;
              end;
            TdxCharacters.Space,
            TdxCharacters.EmSpace,
            TdxCharacters.EnSpace:
    					Result := TdxCharacterFormatterState.Spaces;
            TdxCharacters.Dash,
            TdxCharacters.EmDash,
            TdxCharacters.EnDash:
              Result := DashState;
            TdxCharacters.TabMark:
              Result := TdxCharacterFormatterState.Tab;
            TdxCharacters.LineBreak:
              Result := TdxCharacterFormatterState.LineBreak;
            TdxCharacters.PageBreak:
              begin
              if Paragraph.IsInCell then
                Result := TdxCharacterFormatterState.Spaces
              else
                Result := TdxCharacterFormatterState.PageBreak;
              end;
            TdxCharacters.ColumnBreak:
              begin
              if Paragraph.IsInCell then
                Result := TdxCharacterFormatterState.Spaces
              else
                Result := TdxCharacterFormatterState.ColumnBreak;
              end;
            else
              Result := TdxCharacterFormatterState.Text;
          end;
end;

function TdxCharacterFormatterStateBase.GetParagraph: TdxParagraph;
begin
  Result := TdxParagraphCharacterIterator(Iterator).Paragraph;
end;

function TdxCharacterFormatterStateBase.IsFloatingObjectRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun is TdxFloatingObjectAnchorRun;
end;

function TdxCharacterFormatterStateBase.IsInlineObjectRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := Supports(ARun, IdxInlineObjectRun);
end;

function TdxCharacterFormatterStateBase.IsLayoutDependentTextRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun is TdxLayoutDependentTextRun;
end;

function TdxCharacterFormatterStateBase.IsParagraphMarkRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun is TdxParagraphRun;
end;

function TdxCharacterFormatterStateBase.IsSeparatorRun(ARun: TdxTextRunBase): Boolean;
begin
  Result := ARun is TdxSeparatorTextRun;
end;

procedure TdxCharacterFormatterStateBase.SwitchToNextState;
var
  ANextState: TdxCharacterFormatterState;
begin
  ANextState := GetNextState;
  ChangeState(ANextState);
end;

{ TdxParagraphFormatterBase }

constructor TdxParagraphFormatterBase<TState>.Create(APieceTable: TdxPieceTable; AMeasurer: TdxBoxMeasurer);
begin
  inherited Create;
  Assert(APieceTable <> nil, 'pieceTable = nil');
  Assert(AMeasurer <> nil, 'measurer = nil');
  FPieceTable := APieceTable;
  FMeasurer := AMeasurer;
  CreateStates;
end;

destructor TdxParagraphFormatterBase<TState>.Destroy;
begin
  DestroyStates;
  FreeAndNil(FIterator);
  inherited Destroy;
end;

procedure TdxParagraphFormatterBase<TState>.CreateStates;
begin
end;

procedure TdxParagraphFormatterBase<TState>.DestroyStates;
begin
  FreeAndNil(FState);
end;

function TdxParagraphFormatterBase<TState>.GetDocumentModel: TdxDocumentModel;
begin
  Result := FPieceTable.DocumentModel;
end;

class function TdxParagraphFormatterBase<TState>.GetSpaceBoxTemplate(ABoxInfo: TdxBoxInfo): TdxBoxClass;
begin
  if IsFormatterPositionEquals(ABoxInfo.StartPos, ABoxInfo.EndPos) then
    Result := TdxSingleSpaceBox
  else
    Result := TdxSpaceBoxa;
end;

procedure TdxParagraphFormatterBase<TState>.OnNewMeasurementAndDrawingStrategyChanged(AMeasurer: TdxBoxMeasurer);
begin
  Assert(AMeasurer <> nil);
  FMeasurer := AMeasurer;
end;

procedure TdxParagraphFormatterBase<TState>.SetIterator(const Value: TdxParagraphIteratorBase);
begin
  FreeAndNil(FIterator);
  FIterator := Value;
end;

{ TdxCharacterFormatterStartState }

function TdxCharacterFormatterStartState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  Assert(False);
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxCharacterFormatterStartState.ContinueFormat: Boolean;
begin
  SwitchToNextState;
  Result := True;
end;

function TdxCharacterFormatterStartState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Start;
end;

function TdxCharacterFormatterStartState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Assert(False);
  Result := True;
end;

procedure TdxCharacterFormatterStartState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Assert(False);
end;

{ TdxCharacterFormatterSpacesState }

function TdxCharacterFormatterSpacesState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(Formatter.GetSpaceBoxTemplate(ABoxInfo), ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxCharacterFormatterSpacesState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Spaces;
end;

{ TdxCharacterFormatterTextState }

function TdxCharacterFormatterTextState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  if Paragraph.PieceTable.Runs[ABoxInfo.StartPos.RunIndex] is TdxSpecialTextRun then
    AppendBoxCore(TdxSpecialTextBox, ABoxInfo)
  else
    AppendBoxCore(TdxTextBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxCharacterFormatterTextState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Text;
end;

function TdxCharacterFormatterTextState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := TdxCharacters.IsCharSpace(ACh) or (ACh = TdxCharacters.TabMark) or (ACh = TdxCharacters.LineBreak) or
    (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak) or TdxCharacters.IsCharDash(ACh);
end;

procedure TdxCharacterFormatterTextState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasureText(ABoxInfo); 
end;

{ TdxCharacterFormatterDashState }

function TdxCharacterFormatterDashState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxTextBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxCharacterFormatterDashState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Dash;
end;

function TdxCharacterFormatterDashState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := not TdxCharacters.IsCharDash(ACh);
end;

procedure TdxCharacterFormatterDashState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasureText(ABoxInfo);
end;

{ TdxCharacterFormatterFirstDashState }

function TdxCharacterFormatterFirstDashState.GetDashState: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Dash;
end;

function TdxCharacterFormatterFirstDashState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.FirstDash;
end;

function TdxCharacterFormatterFirstDashState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

{ TdxCharacterFormatterLayoutDependentTextState }

function TdxCharacterFormatterLayoutDependentTextState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.LayoutDependentText;
end;

function TdxCharacterFormatterLayoutDependentTextState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := TdxCharacters.IsCharSpace(ACh) or (ACh = TdxCharacters.TabMark) or (ACh = TdxCharacters.LineBreak) or
     (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak);
end;

function TdxCharacterFormatterLayoutDependentTextState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxLayoutDependentTextBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterLayoutDependentTextState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasureText(ABoxInfo);
end;

{ TdxCharacterFormatterInlineObjectState }

function TdxCharacterFormatterInlineObjectState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.InlineObject;
end;

function TdxCharacterFormatterInlineObjectState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := TdxCharacters.IsCharSpace(ACh) or (ACh = TdxCharacters.TabMark) or (ACh = TdxCharacters.LineBreak) or
     (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak) or TdxCharacters.IsCharDash(ACh);
end;

function TdxCharacterFormatterInlineObjectState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
var
  AInlineObjectRun: IdxInlineObjectRun;
begin
  if not Supports(Formatter.PieceTable.Runs[ABoxInfo.StartPos.RunIndex], IdxInlineObjectRun, AInlineObjectRun) then
    Assert(False);
  AppendBoxCore(AInlineObjectRun.GetBoxClassType, ABoxInfo); 
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterInlineObjectState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
var
  ARun: TdxTextRunBase;
begin
  ARun := Formatter.PieceTable.Runs[ABoxInfo.StartPos.RunIndex];
  ARun.Measure(ABoxInfo, Measurer);
end;

{ TdxCharacterFormatterSeparatorState }

function TdxCharacterFormatterSeparatorState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Separator;
end;

function TdxCharacterFormatterSeparatorState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := TdxCharacters.IsCharSpace(ACh) or (ACh = TdxCharacters.TabMark) or (ACh = TdxCharacters.LineBreak) or
    (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak) or TdxCharacters.IsCharDash(ACh);
end;

function TdxCharacterFormatterSeparatorState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxSeparatorBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterSeparatorState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
var
  ARun: TdxTextRunBase;
begin
  ARun := Formatter.PieceTable.Runs[ABoxInfo.StartPos.RunIndex];
  Assert(ARun is TdxSeparatorTextRun);
  ARun.Measure(ABoxInfo, Measurer);
end;

{ TdxCharacterFormatterFloatingObjectState }

function TdxCharacterFormatterFloatingObjectState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.InlineObject;
end;

function TdxCharacterFormatterFloatingObjectState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := TdxCharacters.IsCharSpace(ACh) or (ACh = TdxCharacters.TabMark) or (ACh = TdxCharacters.LineBreak) or
    (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak) or TdxCharacters.IsCharDash(ACh);
end;

function TdxCharacterFormatterFloatingObjectState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxFloatingObjectAnchorBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterFloatingObjectState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  ABoxInfo.Size := cxNullSize; 
end;

{ TdxCharacterFormatterTabState }

function TdxCharacterFormatterTabState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Tab;
end;

function TdxCharacterFormatterTabState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

function TdxCharacterFormatterTabState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxTabSpaceBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterTabState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasureTab(ABoxInfo);
end;

{ TdxCharacterFormatterLineBreak }

function TdxCharacterFormatterLineBreak.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.LineBreak;
end;

function TdxCharacterFormatterLineBreak.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

function TdxCharacterFormatterLineBreak.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxLineBreakBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterLineBreak.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasureLineBreakMark(ABoxInfo);
end;

{ TdxCharacterFormatterPageBreak }

function TdxCharacterFormatterPageBreak.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.PageBreak;
end;

function TdxCharacterFormatterPageBreak.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

function TdxCharacterFormatterPageBreak.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxPageBreakBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterPageBreak.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasurePageBreakMark(ABoxInfo);
end;

{ TdxCharacterFormatterColumnBreak }

function TdxCharacterFormatterColumnBreak.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.ColumnBreak;
end;

function TdxCharacterFormatterColumnBreak.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

function TdxCharacterFormatterColumnBreak.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  AppendBoxCore(TdxColumnBreakBox, ABoxInfo);
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxCharacterFormatterColumnBreak.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasureColumnBreakMark(ABoxInfo);
end;

{ TdxCharacterFormatterFinalState }

function TdxCharacterFormatterFinalState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  Assert(False);
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxCharacterFormatterFinalState.ContinueFormat: Boolean;
begin
  Assert(False);
  Result := True;
end;

function TdxCharacterFormatterFinalState.GetFormattingComplete: Boolean;
begin
  Result := True;
end;

function TdxCharacterFormatterFinalState.GetType: TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Final;
end;

function TdxCharacterFormatterFinalState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Assert(False);
  Result := True;
end;

procedure TdxCharacterFormatterFinalState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Assert(False);
end;

{ TdxParagraphCharacterFormatter }

constructor TdxParagraphCharacterFormatter.Create(APieceTable: TdxPieceTable; AMeasurer: TdxBoxMeasurer);
begin
  inherited Create(APieceTable, AMeasurer);
  FTextBoxes := TdxBoxList.Create;
  FTextBoxInfos := TdxBoxInfoList.Create;
end;

destructor TdxParagraphCharacterFormatter.Destroy;
begin
  FreeAndNil(FTextBoxInfos);
  FreeAndNil(FTextBoxes);
  inherited Destroy;
end;

procedure TdxParagraphCharacterFormatter.CreateStates;
const
  StateClasses: array[TdxCharacterFormatterState] of TdxCharacterFormatterStateBaseClass = (
    TdxCharacterFormatterStartState,               
    TdxCharacterFormatterSpacesState,              
    TdxCharacterFormatterTabState,                 
    TdxCharacterFormatterFirstDashState,           
    TdxCharacterFormatterDashState,                
    TdxCharacterFormatterSeparatorState,           
    TdxCharacterFormatterLineBreak,                
    TdxCharacterFormatterPageBreak,                
    TdxCharacterFormatterColumnBreak,              
    TdxCharacterFormatterTextState,                
    TdxCharacterFormatterInlineObjectState,        
    TdxCharacterFormatterLayoutDependentTextState, 
    TdxCharacterFormatterFloatingObjectState,      
    TdxCharacterFormatterFinalState                
  );
var
  I: TdxCharacterFormatterState;
begin
  for I := Low(TdxCharacterFormatterState) to High(TdxCharacterFormatterState) do
    FStates[I] := StateClasses[I].Create(Self);
end;

procedure TdxParagraphCharacterFormatter.DestroyStates;
var
  I: TdxCharacterFormatterState;
begin
  for I := Low(TdxCharacterFormatterState) to High(TdxCharacterFormatterState) do
    FStates[I].Free;
end;

procedure TdxParagraphCharacterFormatter.FormatParagraph(AIterator: TdxParagraphIteratorBase);
begin
  FIterator := AIterator;
  BeginParagraph(True);
  ChangeState(GetInitialStateType(True)); 
  repeat
    State.ContinueFormat;
  until State.FormattingComplete;
end;

procedure TdxParagraphCharacterFormatter.AddTextBoxToQueue(ABox: TdxBox; ABoxInfo: TdxBoxInfo);
begin
{$IFDEF DEBUG}
  Assert(PieceTable.Runs[ABoxInfo.StartPos.RunIndex] is TdxTextRun);
  Assert(ABox is TdxTextBox);
{$ENDIF}
  FTextBoxInfos.Add(ABoxInfo);
  FTextBoxes.Add(ABox);
end;

procedure TdxParagraphCharacterFormatter.BeginParagraph(ABeginFromParagraphStart: Boolean);
var
  AParagraph: TdxParagraph;
  ABoxCollection: TdxParagraphBoxCollection;
begin
  AParagraph := Iterator.Paragraph;
  ABoxCollection := AParagraph.BoxCollection;
  ABoxCollection.Clear;
  if AParagraph.IsInList then
    FormatNumberingListBoxes;
end;

procedure TdxParagraphCharacterFormatter.ChangeState(AStateType: TdxCharacterFormatterState);
begin
  State := FStates[AStateType];
end;

function TdxParagraphCharacterFormatter.CreateNumberingListBox(ASeparatorChar: Char;
  APosition: TdxFormatterPosition): TdxNumberingListBox;
begin
  if ASeparatorChar = #0000 then
    Result := TdxNumberingListBox.Create
  else
  begin
    Result := TdxNumberingListBoxWithSeparator.Create;
    Result.StartPos := APosition;
    Result.EndPos := APosition;
    TdxNumberingListBoxWithSeparator(Result).SeparatorBox := CreateSeparatorBox(ASeparatorChar, APosition);
  end;
end;

function TdxParagraphCharacterFormatter.CreateSeparatorBox(ASeparator: Char; APosition: TdxFormatterPosition): TdxBox;
var
  ABoxInfo: TdxBoxInfo;
begin
  if ASeparator = TdxCharacters.TabMark then
  begin
    Result := TdxTabSpaceBox.Create;
    Result.Bounds.Empty;
  end
  else
  begin
    Assert(ASeparator = TdxCharacters.Space);
    ABoxInfo := TdxBoxInfo.Create;
    ABoxInfo.StartPos := APosition;
    ABoxInfo.EndPos := APosition;
    Measurer.MeasureSingleSpace(ABoxInfo);
    Result := TdxSingleSpaceBox.Create;
    Result.Bounds := TRect.CreateSize(ABoxInfo.Size);
  end;
end;

procedure TdxParagraphCharacterFormatter.Format(AIterator: TdxParagraphCharacterIterator);
begin
  FormatParagraph(AIterator);
  MeasureTextBoxes;
end;

procedure TdxParagraphCharacterFormatter.FormatNumberingListBoxes;
var
  AParagraph: TdxParagraph;
  ABoxInfo: TdxBoxInfo;
  ANumberingList: TdxNumberingList;
  APosition: TdxFormatterPosition;
  AListLevelIndex: Integer;
  AListLevel: TdxAbstractListLevel;
  ANumberingListBox: TdxNumberingListBox;
begin
  AParagraph := Iterator.Paragraph;
  ABoxInfo := TdxNumberingListBoxInfo.Create;
  try
    ANumberingList := DocumentModel.NumberingLists[AParagraph.GetNumberingListIndex];
    APosition.CopyFrom(AParagraph.FirstRunIndex, 0, 0);
    ABoxInfo.StartPos := APosition;
    ABoxInfo.EndPos := APosition;
    Measurer.MeasureText(ABoxInfo, AParagraph.GetNumberingListText, AParagraph.GetNumerationFontInfo);
    AListLevelIndex := AParagraph.GetListLevelIndex;
    AListLevel := ANumberingList.Levels[AListLevelIndex];
    ANumberingListBox := CreateNumberingListBox(AListLevel.ListLevelProperties.Separator, APosition);
    ANumberingListBox.StartPos := APosition;
    ANumberingListBox.EndPos := APosition;
    ANumberingListBox.InitialBounds := TRect.CreateSize(ABoxInfo.Size);
    AParagraph.BoxCollection.NumberingListBox := ANumberingListBox;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxParagraphCharacterFormatter.GetInitialStateType(ABeginFromParagraphStart: Boolean): TdxCharacterFormatterState;
begin
  Result := TdxCharacterFormatterState.Start;
end;

function TdxParagraphCharacterFormatter.GetIterator: TdxParagraphCharacterIterator;
begin
  Result := TdxParagraphCharacterIterator(inherited Iterator);
end;

function TdxParagraphCharacterFormatter.GetIteratorClass: TdxIteratorClass;
begin
  Result := TdxParagraphCharacterIterator;
end;

procedure TdxParagraphCharacterFormatter.MeasureTextBoxes;
var
  I, ACount: Integer;
  ARuns: TdxTextRunCollection;
  ABoxInfo: TdxBoxInfo;
  ARun: TdxTextRunBase;
begin
  ACount := FTextBoxInfos.Count;
  Assert(ACount = FTextBoxes.Count);
  ARuns := PieceTable.Runs;
  Measurer.BeginTextMeasure;
  try
    for I := 0 to ACount - 1 do
    begin
      ABoxInfo := FTextBoxInfos[I];
      ARun := ARuns[ABoxInfo.StartPos.RunIndex];
      ARun.Measure(ABoxInfo, Measurer);
    end;
  finally
    Measurer.EndTextMeasure;
  end;
  for I := 0 to ACount - 1 do
  begin
    ABoxInfo := FTextBoxInfos[I];
    FTextBoxes[I].Bounds := TRect.CreateSize(ABoxInfo.Size); 
  end;
end;

{ TdxFormatterStateBase }

function TdxFormatterStateBase.ContinueFormatByCharacter(var ABoxInfo: TdxBoxInfo;
  ALayoutDependentTextBox: TdxLayoutDependentTextBox): TdxStateContinueFormatResult;
var
  AIterator: TdxParagraphIteratorBase;
  AAppendBoxResult: TdxStateContinueFormatResult;
begin
  AIterator := Iterator;
  repeat
    ABoxInfo.IteratorResult := AIterator.Next;
    Assert((ALayoutDependentTextBox = nil) or (ABoxInfo.IteratorResult <> TdxParagraphIteratorResult.Success));
    ABoxInfo.Box := ALayoutDependentTextBox;
    if ABoxInfo.IteratorResult <> TdxParagraphIteratorResult.Success then
    begin
      ABoxInfo.EndPos := AIterator.GetPreviousPosition;
      AAppendBoxResult := AppendBox(ABoxInfo);
      if AAppendBoxResult <> TdxStateContinueFormatResult.Success then
        Exit(AAppendBoxResult);
      FinishParagraphCore(AIterator);
      Exit(TdxStateContinueFormatResult.Success);
    end
    else
      if IsTerminatorChar(AIterator.CurrentChar) then
      begin
        ABoxInfo.EndPos := AIterator.GetPreviousPosition;
        Exit(AppendBox(ABoxInfo));
      end;
  until False;
end;

{ TdxParagraphIteratorBase }

constructor TdxParagraphIteratorBase.Create(AParagraph: TdxParagraph; APieceTable: TdxPieceTable;
  AVisibleTextFilter: TdxVisibleTextFilterBase);
begin
  inherited Create;
  Assert(AParagraph <> nil, 'AParagraph = nil');
  Assert(APieceTable <> nil, 'APieceTable = nil');
  Assert(AVisibleTextFilter <> nil);
  FParagraph := AParagraph;
  FPieceTable := APieceTable;
  FVisibleTextFilter := AVisibleTextFilter;
  FEndRunIndex := AVisibleTextFilter.FindVisibleParagraphRunForward(AParagraph.LastRunIndex);
  FPosition := CreatePosition;
  SetRunIndexCore(AVisibleTextFilter.FindVisibleRunForward(AParagraph.FirstRunIndex));
end;

function TdxParagraphIteratorBase.GetCurrentChar: Char;
begin
  Result := FPieceTable.TextBuffer[FRunStartIndex + Offset];
end;

function TdxParagraphIteratorBase.GetCurrentPosition: TdxFormatterPosition;
begin
  Result.CopyFrom(RunIndex, Offset, 0); 
end;

function TdxParagraphIteratorBase.GetIsEnd: Boolean;
begin
  Result := RunIndex >= FEndRunIndex;
end;

function TdxParagraphIteratorBase.GetPreviousOffsetPosition: TdxFormatterPosition;
begin
  Result.CopyFrom(RunIndex, Offset - 1, 0); 
end;

function TdxParagraphIteratorBase.GetPreviousPosition: TdxFormatterPosition;
begin
  if Offset = 0 then
    Result := GetPreviousVisibleRunPosition
  else
    Result := GetPreviousOffsetPosition;
end;

function TdxParagraphIteratorBase.GetPreviousVisibleRunPosition: TdxFormatterPosition;
var
  ARunIndex: TdxRunIndex;
begin
  ARunIndex := VisibleTextFilter.GetPrevVisibleRunIndex(RunIndex);
  Result.CopyFrom(ARunIndex, FPieceTable.Runs[ARunIndex].Length - 1, -1);
end;

function TdxParagraphIteratorBase.IsFloatingObjectAnchorRun: Boolean;
begin
  Result := PieceTable.Runs[RunIndex] is TdxFloatingObjectAnchorRun;
end;

function TdxParagraphIteratorBase.IsParagraphMarkRun: Boolean;
begin
  Result := PieceTable.Runs[RunIndex] is TdxParagraphRun;
end;

function TdxParagraphIteratorBase.IsParagraphFrame: Boolean;
begin
  Result := PieceTable.Runs[RunIndex].Paragraph.FrameProperties <> nil;
end;

function TdxParagraphIteratorBase.Next: TdxParagraphIteratorResult;
begin
  if Offset < FMaxOffset then
  begin
    NextOffset;
    Result := TdxParagraphIteratorResult.Success;
    Exit;
  end;
  while not IsEnd do
  begin
    NextRun;
    if VisibleTextFilter.IsRunVisible(RunIndex) then
    begin
      if IsEnd then
        Result := TdxParagraphIteratorResult.Finished
      else
        Result := TdxParagraphIteratorResult.RunFinished;
      Exit;
    end;
  end;
  Result := TdxParagraphIteratorResult.Finished;
end;

procedure TdxParagraphIteratorBase.NextOffset;
begin
  Inc(FPosition.Offset)
end;

procedure TdxParagraphIteratorBase.NextRun;
begin
  SetRunIndexCore(RunIndex + 1);
  FPosition.Offset := 0;
end;

procedure TdxParagraphIteratorBase.SetPosition(APos: TdxFormatterPosition);
begin
  SetRunIndexCore(APos.RunIndex);
  FPosition.Offset := APos.Offset;
end;

procedure TdxParagraphIteratorBase.SetPosition(ARunIndex: TdxRunIndex; AOffset: Integer);
begin
  SetRunIndexCore(ARunIndex);
  FPosition.Offset := AOffset;
end;

procedure TdxParagraphIteratorBase.SetPositionCore(APos: TdxFormatterPosition);
begin
  SetRunIndexCore(APos.RunIndex);
  FPosition.Offset := APos.Offset;
end;

procedure TdxParagraphIteratorBase.SetRunIndexCore(ARunIndex: TdxRunIndex);
var
  ARun: TdxTextRunBase;
begin
  FPosition.RunIndex := ARunIndex;
  ARun := FPieceTable.Runs[RunIndex];
  FMaxOffset := ARun.Length - 1;
  FRunStartIndex := ARun.StartIndex;
end;

{ TdxParagraphCharacterIterator }

function TdxParagraphCharacterIterator.CreatePosition: TdxFormatterPosition;
begin
  Result.CopyFrom(0, 0, 0); 
end;

{ TdxEmptyTextFilter }

function TdxEmptyTextFilter.Clone(APieceTable: TdxPieceTable): IVisibleTextFilter;
begin
  Result := TdxEmptyTextFilter.Create(APieceTable);
end;

{ TdxEmptyTextFilterSkipFloatingObjects }

function TdxEmptyTextFilterSkipFloatingObjects.IsRunVisible(ARunIndex: TdxRunIndex): Boolean;
begin
  Result := (inherited IsRunVisible(ARunIndex)) and not (PieceTable.Runs[ARunIndex] is TdxFloatingObjectAnchorRun);
end;

{ TdxVisibleTextFilter }

function TdxVisibleTextFilter.Clone(APieceTable: TdxPieceTable): IVisibleTextFilter;
begin
  Result := TdxVisibleTextFilter.Create(APieceTable);
end;

function TdxVisibleTextFilter.IsRunVisible(ARunIndex: TdxRunIndex): Boolean;
begin
  Result := (IsRunVisibleCore(ARunIndex) <> TdxRunVisibility.Hidden) or (ARunIndex = PieceTable.Runs.Count - 1);
end;

function TdxVisibleTextFilter.IsRunVisibleCore(ARunIndex: TdxRunIndex): TdxRunVisibility;
var
  AShouldDisplayHiddenText: Boolean;
begin
  Result := inherited IsRunVisibleCore(ARunIndex);
  if Result <> TdxRunVisibility.Visible then
    Exit;
  AShouldDisplayHiddenText := PieceTable.DocumentModel.FormattingMarkVisibilityOptions.HiddenText = TdxRichEditFormattingMarkVisibility.Visible;
  if (not PieceTable.Runs[ARunIndex].Hidden) or AShouldDisplayHiddenText then
    Result := TdxRunVisibility.Visible
  else
    Result := TdxRunVisibility.Hidden;
end;

{ TdxVisibleTextFilterSkipFloatingObjects }

function TdxVisibleTextFilterSkipFloatingObjects.IsRunVisible(ARunIndex: TdxRunIndex): Boolean;
begin
  Result := (inherited IsRunVisible(ARunIndex)) and not (PieceTable.Runs[ARunIndex] is TdxFloatingObjectAnchorRun);
end;

{ TdxVisibleOnlyTextFilter }

function TdxVisibleOnlyTextFilter.Clone(APieceTable: TdxPieceTable): IVisibleTextFilter;
begin
  Result := TdxVisibleOnlyTextFilter.Create(APieceTable);
end;

function TdxVisibleOnlyTextFilter.IsRunVisible(ARunIndex: TdxRunIndex): Boolean;
begin
  Result := (IsRunVisibleCore(ARunIndex) <> TdxRunVisibility.Hidden) or (ARunIndex = PieceTable.Runs.Count - 1);
end;

function TdxVisibleOnlyTextFilter.IsRunVisibleCore(ARunIndex: TdxRunIndex): TdxRunVisibility;
begin
  Result := inherited IsRunVisibleCore(ARunIndex);
  if Result <> TdxRunVisibility.Visible then
    Exit;
  if not PieceTable.Runs[ARunIndex].Hidden then
    Result := TdxRunVisibility.Visible
  else
    Result := TdxRunVisibility.Hidden;
end;

function TdxCharacterFormatterSpacesState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := not TdxCharacters.IsCharSpace(ACh);
end;

procedure TdxCharacterFormatterSpacesState.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Measurer.MeasureSpaces(ABoxInfo);
end;

{ TdxFormattingProcessResult }

procedure TdxFormattingProcessResult.Init(AFormattingProcess: TdxFormattingProcess);
begin
  FormattingProcess := AFormattingProcess;
end;

procedure TdxFormattingProcessResult.Init(AParagraphIndex: TdxParagraphIndex);
begin
  FormattingProcess := TdxFormattingProcess.ContinueFromParagraph;
  ParagraphIndex := AParagraphIndex;
end;

procedure TdxFormattingProcessResult.Init(const ARestartPosition: TdxDocumentModelPosition);
begin
  FormattingProcess := TdxFormattingProcess.RestartFromTheStartOfRow;
  RestartPosition := ARestartPosition;
end;

{ TdxParagraphBoxFormatterTableStateBase }

constructor TdxParagraphBoxFormatterTableStateBase.Create(AFormatter: TdxParagraphBoxFormatter);
begin
  inherited Create;
  Assert(AFormatter <> nil);
  FFormatter := AFormatter;
end;

{ TdxParagraphBoxFormatterTextState }

procedure TdxParagraphBoxFormatterTextState.OnColumnOverfull;
begin
end;

{ TdxParagraphBoxFormatterTableState }

procedure TdxParagraphBoxFormatterTableState.OnColumnOverfull;
begin
end;

{ TdxBoxFormatterStateBase }

function TdxBoxFormatterStateBase.AddExistedBox(ABoxInfo: TdxBoxInfo; AIterator: TdxParagraphBoxIterator): TdxStateContinueFormatResult;
begin
  ABoxInfo.Box := AIterator.CurrentBox;
  AIterator.NextBox;
  ABoxInfo.EndPos := AIterator.GetPreviousPosition;
  Result := AppendBox(ABoxInfo);
end;

procedure TdxBoxFormatterStateBase.AddNumberingListBox;
var
  ANumberingListBox: TdxNumberingListBox;
  AIndex: TdxNumberingListIndex;
  AListLevelIndex: Integer;
  ABoxInfo: TdxBoxInfo;
  AListLevel: TdxAbstractListLevel;
begin
  if CurrentRow.NumberingListBox <> nil then
    Exit;

  ANumberingListBox := Iterator.Paragraph.BoxCollection.NumberingListBox;
  AIndex := Iterator.Paragraph.GetNumberingListIndex;
  AListLevelIndex := Iterator.Paragraph.GetListLevelIndex;
  AListLevel := Iterator.Paragraph.DocumentModel.NumberingLists[AIndex].Levels[AListLevelIndex];

  ABoxInfo := TdxNumberingListBoxInfo.Create;
  try
    ABoxInfo.Box := ANumberingListBox;
    ABoxInfo.StartPos := ANumberingListBox.StartPos;
    ABoxInfo.EndPos := ANumberingListBox.EndPos;
    ABoxInfo.Size := cxSize(ANumberingListBox.InitialBounds);
    ANumberingListBox.Bounds := ANumberingListBox.InitialBounds;

    RowsController.UpdateCurrentRowHeight(ABoxInfo);
    RowsController.AddNumberingListBox(ANumberingListBox, AListLevel.ListLevelProperties, Formatter);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

procedure TdxBoxFormatterStateBase.AddTextBox(ABoxInfo: TdxBoxInfo);
var
  ARun: TdxTextRunBase;
  AInlineObjectRun: IdxInlineObjectRun;
  ABox: TdxBox;
begin
  ARun := Formatter.PieceTable.Runs[ABoxInfo.StartPos.RunIndex];

  if Supports(ARun, IdxInlineObjectRun, AInlineObjectRun) then
  begin
    ABox := RowsController.AddBox(AInlineObjectRun.GetBoxClassType, ABoxInfo);
    RowsController.UpdateCurrentRowHeightFromInlineObjectRun(ARun, ABox, Formatter.Measurer);
  end
  else
  begin
    if not (ARun is TdxLayoutDependentTextRun) then
      RowsController.AddBox(TdxTextBox, ABoxInfo)
    else
    begin
        RowsController.AddBox(TdxLayoutDependentTextBox, ABoxInfo);
    end;
    RowsController.UpdateCurrentRowHeight(ABoxInfo);
  end;
end;

function TdxBoxFormatterStateBase.AdjustEndPositionToFit(ABoxInfo: TdxBoxInfo): TdxAdjustEndPositionResult;
var
  AOffset: Integer;
begin
  if ABoxInfo.StartPos.AreEqual(ABoxInfo.EndPos) then
  begin
    Result := TdxAdjustEndPositionResult.Failure;
    Exit;
  end;
  ABoxInfo.Box := nil;
  AOffset := BinarySearchFittedBox(ABoxInfo);
  Assert(AOffset >= 0); 
  AOffset := AOffset - 1; 
  if AOffset < ABoxInfo.StartPos.Offset then
  begin
    ABoxInfo.EndPos.CopyFrom(ABoxInfo.EndPos.RunIndex, ABoxInfo.StartPos.Offset, ABoxInfo.EndPos.BoxIndex);
    Result := TdxAdjustEndPositionResult.Failure;
  end
  else
  begin
    ABoxInfo.EndPos.CopyFrom(ABoxInfo.EndPos.RunIndex, AOffset, ABoxInfo.EndPos.BoxIndex);
    Formatter.Measurer.MeasureText(ABoxInfo);
    Result := TdxAdjustEndPositionResult.Success;
  end;
end;

function TdxBoxFormatterStateBase.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := &Type;
end;

function TdxBoxFormatterStateBase.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
var
  ABoxResult: TdxAddBoxResult;
begin
  MeasureBoxContentCore(ABoxInfo);
  ABoxResult := ObtainAddBoxResult(ABoxInfo);
  Result := DispatchAddBoxResult(ABoxInfo, ABoxResult);
end;

procedure TdxBoxFormatterStateBase.ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

procedure TdxBoxFormatterStateBase.ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

procedure TdxBoxFormatterStateBase.ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

procedure TdxBoxFormatterStateBase.ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

function TdxBoxFormatterStateBase.BinarySearchFittedBox(ABoxInfo: TdxBoxInfo): Integer;
var
  AEndPos: TdxFormatterPosition;
  AHi, AMedian: Integer;
begin
  AEndPos.CopyFrom(ABoxInfo.EndPos.RunIndex, ABoxInfo.EndPos.Offset, ABoxInfo.EndPos.BoxIndex);;
  Result := ABoxInfo.StartPos.Offset;
  AHi := ABoxInfo.EndPos.Offset;
  while (Result <= AHi) do
  begin
    AMedian := (Result + AHi) div 2;
    ABoxInfo.EndPos.CopyFrom(AEndPos.RunIndex, AMedian, AEndPos.BoxIndex);
    Formatter.Measurer.MeasureText(ABoxInfo);
    if CanAddBoxWithoutHyphen(ABoxInfo) = TdxAddBoxResult.Success then
      Result := AMedian + 1
    else
      AHi := AMedian - 1;
  end;
end;

function TdxBoxFormatterStateBase.CalcBoxSizeWithHyphen(ABoxInfo: TdxBoxInfo): TSize;
var
  AHyphenWidth: Integer;
  ABoxSize: TSize;
begin
  AHyphenWidth := Formatter.Measurer.MeasureHyphen(ABoxInfo.EndPos, nil).cx; 
  ABoxSize := ABoxInfo.Size;
  ABoxSize.cx := ABoxSize.cx + AHyphenWidth;
  Result := ABoxSize;
end;

procedure TdxBoxFormatterStateBase.CalculateAndMeasureLayoutDependentTextBox(
  ALayoutDependentTextBox: TdxLayoutDependentTextBox; ABoxInfo: TdxBoxInfo);
var
  ARun: TdxLayoutDependentTextRun;
begin
  ARun := TdxLayoutDependentTextRun(Formatter.PieceTable.Runs[ALayoutDependentTextBox.StartPos.RunIndex]);
  ALayoutDependentTextBox.CalculatedText := ARun.FieldResultFormatting.GetValue(Formatter, Formatter.DocumentModel);
  MeasureBoxContent(ABoxInfo);
end;

function TdxBoxFormatterStateBase.CanAddBoxCore(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  if Formatter.Iterator.Paragraph.FrameProperties <> nil then
  begin
    if (ABoxInfo.Box <> nil) and not (ABoxInfo.Box is TdxFloatingObjectAnchorBox) then
    begin
      NotImplemented();
    end;
  end;
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if Formatter.Iterator.Paragraph.FrameProperties <> nil then
  begin
    if (ABoxInfo.Box <> nil) and not (ABoxInfo.Box is TdxFloatingObjectAnchorBox) then
    begin
      if not RowsController.CanFitBoxToCurrentRow(ABoxInfo.Size) then
        Exit(TdxAddBoxResult.HorizontalIntersect);
      Exit(TdxAddBoxResult.Success);
    end;
  end;
  if ACanFit <> TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    if ACanFit = TdxCanFitCurrentRowToColumnResult.TextAreasRecreated then
    begin
      Result := TdxAddBoxResult.IntersectWithFloatObject;
      Exit;
    end
    else
      if ACanFit = TdxCanFitCurrentRowToColumnResult.RestartDueFloatingObject then
      begin
        Result := TdxAddBoxResult.RestartDueFloatingObject;
        Exit;
      end;
    Result := GetAddBoxResultColumnOverfull(ACanFit);
    Exit;
  end;
  if not RowsController.CanFitBoxToCurrentRow(ABoxInfo.Size) then
    Result := TdxAddBoxResult.HorizontalIntersect
  else
    Result := TdxAddBoxResult.Success;
end;

function TdxBoxFormatterStateBase.CanAddBoxWithHyphenCore(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
  ABoxSize: TSize;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit <> TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    if ACanFit = TdxCanFitCurrentRowToColumnResult.TextAreasRecreated then
    begin
      Result := TdxAddBoxResult.IntersectWithFloatObject;
      Exit;
    end
    else
      if ACanFit = TdxCanFitCurrentRowToColumnResult.RestartDueFloatingObject then
      begin
        Result := TdxAddBoxResult.RestartDueFloatingObject;
        Exit;
      end;
    Result := GetAddBoxResultColumnOverfull(ACanFit);
    Exit;
  end;

  ABoxSize := CalcBoxSizeWithHyphen(ABoxInfo);
  if not RowsController.CanFitBoxToCurrentRow(ABoxSize) then
    Result := TdxAddBoxResult.HorizontalIntersect
  else
    Result := TdxAddBoxResult.Success;
end;

function TdxBoxFormatterStateBase.GetContinueFormatResult(AResult: TdxCompleteFormattingResult): TdxStateContinueFormatResult;
begin
  if AResult = TdxCompleteFormattingResult.Success then
    Result := TdxStateContinueFormatResult.Success
  else
    Result := TdxStateContinueFormatResult.RestartDueOrphanedObjects;
end;

function TdxBoxFormatterStateBase.CanAddBoxWithoutHyphen(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBox(ABoxInfo);
end;

function TdxBoxFormatterStateBase.CanInsertNumberingListBox: Boolean;
var
  ACh: Char;
begin
  ACh := Iterator.CurrentChar;
  Result := (ACh <> TdxCharacters.PageBreak) and (ACh <> TdxCharacters.ColumnBreak);
end;

procedure TdxBoxFormatterStateBase.ChangeState(AStateType: TdxParagraphBoxFormatterState);
begin
  if Formatter.HasDeferredNumberingListBox and CanInsertNumberingListBox then
  begin
    Formatter.RowsController.OnDeferredBeginParagraph;
    AddNumberingListBox;
    Formatter.HasDeferredNumberingListBox := False;
  end;
  Formatter.ChangeState(AStateType);
end;

function TdxBoxFormatterStateBase.ContinueFormat: TdxStateContinueFormatResult;
var
  ABoxInfo: TdxBoxInfo;
  AAddExistingBoxResult: TdxStateContinueFormatResult;
begin
  ABoxInfo := TdxBoxInfo.Create;
  try
    ABoxInfo.StartPos := Iterator.GetCurrentPosition;
    if CanUseBox and ABoxInfo.StartPos.AreEqual(Iterator.CurrentBox.StartPos) then
    begin
      AAddExistingBoxResult := AddExistedBox(ABoxInfo, Iterator);
      if AAddExistingBoxResult <> TdxStateContinueFormatResult.Success then
        Exit(AAddExistingBoxResult);
      if Iterator.IsEnd then
      begin
        if Iterator.IsParagraphFrame and (Self is TdxStateFloatingObject) then
          Exit(TdxStateContinueFormatResult.Success);
        if (FFormatter.SyllableIterator = nil) or FFormatter.SyllableIterator.IsEnd then
          Exit(GetContinueFormatResult(FinishParagraphOrSecton));
      end;
      Exit(TdxStateContinueFormatResult.Success);
    end;
    if Iterator.CurrentBox is TdxLayoutDependentTextBox then
      Result := ContinueFormatByCharacter(ABoxInfo, TdxLayoutDependentTextBox(Iterator.CurrentBox))
    else
      Result := ContinueFormatByCharacter(ABoxInfo, nil);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

constructor TdxBoxFormatterStateBase.Create(AFormatter: TdxParagraphBoxFormatter);
begin
  inherited Create;
  Assert(AFormatter <> nil);
  FFormatter := AFormatter;
end;

destructor TdxBoxFormatterStateBase.Destroy;
begin
  inherited;
end;

function TdxBoxFormatterStateBase.CreateColumnBreakBoxInfo: TdxBoxInfo;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.StartPos := Iterator.GetCurrentPosition;
  ABoxInfo.EndPos := ABoxInfo.StartPos;
  Formatter.Measurer.MeasureColumnBreakMark(ABoxInfo);
  Result := ABoxInfo;
end;

function TdxBoxFormatterStateBase.CreateLineBreakBoxInfo: TdxBoxInfo;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.StartPos := Iterator.GetCurrentPosition;
  ABoxInfo.EndPos := ABoxInfo.StartPos;
  Formatter.Measurer.MeasureLineBreakMark(ABoxInfo);
  Result := ABoxInfo;
end;

function TdxBoxFormatterStateBase.CreatePageBreakBoxInfo: TdxBoxInfo;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.StartPos := Iterator.GetCurrentPosition;
  ABoxInfo.EndPos := ABoxInfo.StartPos;
  Formatter.Measurer.MeasurePageBreakMark(ABoxInfo);
  Result := ABoxInfo;
end;

function TdxBoxFormatterStateBase.CreateParagraphMarkBoxInfo: TdxBoxInfo;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.Box := Iterator.CurrentBox;
  ABoxInfo.StartPos := Iterator.GetCurrentPosition();
  ABoxInfo.EndPos := ABoxInfo.StartPos;
  Formatter.Measurer.MeasureParagraphMark(ABoxInfo);
  Result := ABoxInfo;
end;

function TdxBoxFormatterStateBase.CreateSectionMarkBoxInfo: TdxBoxInfo;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.Box := Iterator.CurrentBox;
  ABoxInfo.StartPos := Iterator.GetCurrentPosition();
  ABoxInfo.EndPos := ABoxInfo.StartPos;
  Formatter.Measurer.MeasureSectionMark(ABoxInfo);
  Result := ABoxInfo;
end;

function TdxBoxFormatterStateBase.DispatchAddBoxResult(ABoxInfo: TdxBoxInfo; AResult: TdxAddBoxResult): TdxStateContinueFormatResult;
begin
  case AResult of
    TdxAddBoxResult.Success:
      AddSuccess(ABoxInfo);
    TdxAddBoxResult.HorizontalIntersect:
      Exit(GetContinueFormatResult(HorizontalOverfull(ABoxInfo)));
    TdxAddBoxResult.LeaveColumnFirstCellRow,
    TdxAddBoxResult.LeaveColumnPlain:
      Exit(GetContinueFormatResult(ColumnOverfull(GetCanFitCurrentRowToColumn(AResult), ABoxInfo)));
    TdxAddBoxResult.IntersectWithFloatObject:
      Exit(GetContinueFormatResult(Formatter.RollbackToStartOfRow(TdxCanFitCurrentRowToColumnResult.TextAreasRecreated)));
    TdxAddBoxResult.RestartDueFloatingObject:
      Exit(TdxStateContinueFormatResult.RestartDueFloatingObject);
    else
      Assert(False); 
  end;
  Result := TdxStateContinueFormatResult.Success;
end;

procedure TdxBoxFormatterStateBase.EndRow;
var
  ACurrentBottom: Integer;
begin
  Formatter.RollbackUnapprovedFloatingObjects;
  Formatter.RowsController.EndRow(Formatter);
  ACurrentBottom := Formatter.RowsController.CurrentRow.Bounds.Bottom;
  if (ACurrentBottom >= Formatter.MaxHeight) and not Iterator.Paragraph.IsInCell() then
    ForceFormattingComplete := True;
end;

function TdxBoxFormatterStateBase.FinalizeColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := RowsController.CompleteCurrentColumnFormatting;
  if Result <> TdxCompleteFormattingResult.Success then
    Exit;
  if not Formatter.Iterator.Paragraph.IsInCell and RowsController.SupportsColumnAndPageBreaks then
  begin
    RowsController.AddBox(TdxColumnBreakBox, ABoxInfo);
    Formatter.ApproveFloatingObjects;
    EndRow;
    RowsController.MoveRowToNextColumn;
    ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  end
  else
  begin
    if CurrentRow.Boxes.Count > 0 then
      FinalizeLine(ABoxInfo)
    else
      ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  end;
  Result := TdxCompleteFormattingResult.Success;
end;

procedure TdxBoxFormatterStateBase.FinalizeLine(ABoxInfo: TdxBoxInfo);
begin
  RowsController.AddBox(TdxLineBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
end;

function TdxBoxFormatterStateBase.FinalizePage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := RowsController.CompleteCurrentColumnFormatting;
  if Result <> TdxCompleteFormattingResult.Success then
    Exit;
  RowsController.AddBox(TdxPageBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  RowsController.MoveRowToNextPage;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  Result := TdxCompleteFormattingResult.Success;
end;

procedure TdxBoxFormatterStateBase.FinalizeParagraph(ABoxInfo: TdxBoxInfo);
begin
  RowsController.AddBox(TdxParagraphMarkBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.Final);
end;

procedure TdxBoxFormatterStateBase.FinalizeSection(ABoxInfo: TdxBoxInfo);
begin
  RowsController.AddBox(TdxSectionMarkBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.Final);
end;

function TdxBoxFormatterStateBase.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := FinalizeColumn(ABoxInfo);
end;

function TdxBoxFormatterStateBase.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  FinalizeLine(ABoxInfo);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxBoxFormatterStateBase.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := FinalizePage(ABoxInfo);
end;

function TdxBoxFormatterStateBase.FinishParagraph: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateParagraphMarkBoxInfo;
  try
    ApplyParagraphMarkSize(ABoxInfo);
    FinalizeParagraph(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

procedure TdxBoxFormatterStateBase.FinishParagraphCore(AIterator: TdxParagraphIteratorBase);
begin
  if AIterator.IsEnd then
    if (FFormatter.SyllableIterator = nil) or FFormatter.SyllableIterator.IsEnd then
      FinishParagraph;
end;

function TdxBoxFormatterStateBase.FinishParagraphOrSecton: TdxCompleteFormattingResult;
begin
  if Iterator.CurrentChar = TdxCharacters.ParagraphMark then
    Result := FinishParagraph
  else
    Result := FinishSection;
end;

function TdxBoxFormatterStateBase.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    ApplyParagraphMarkSize(ABoxInfo);
    FinalizeSection(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxBoxFormatterStateBase.GetAddBoxResultColumnOverfull(
  ACanFit: TdxCanFitCurrentRowToColumnResult): TdxAddBoxResult;
begin
  case ACanFit of
    TdxCanFitCurrentRowToColumnResult.PlainRowNotFitted:
      Result := TdxAddBoxResult.LeaveColumnPlain;
    TdxCanFitCurrentRowToColumnResult.FirstCellRowNotFitted:
      Result := TdxAddBoxResult.LeaveColumnFirstCellRow;
  else
    Result := TdxAddBoxResult.LeaveColumnPlain;
    Assert(False);
  end;
end;

function TdxBoxFormatterStateBase.GetCanFitCurrentRowToColumn(
  AAddBoxResult: TdxAddBoxResult): TdxCanFitCurrentRowToColumnResult;
begin
  case AAddBoxResult of
    TdxAddBoxResult.LeaveColumnPlain:
      Result := TdxCanFitCurrentRowToColumnResult.PlainRowNotFitted;
    TdxAddBoxResult.LeaveColumnFirstCellRow:
      Result := TdxCanFitCurrentRowToColumnResult.FirstCellRowNotFitted;
    else
    begin
      Assert(False);
      Result := TdxCanFitCurrentRowToColumnResult.RowFitted;
    end;
  end;
end;

function TdxBoxFormatterStateBase.CanUseBox: Boolean;
begin
  Result := False;
end;

function TdxBoxFormatterStateBase.GetCurrentRow: TdxRow;
begin
  Result := Formatter.CurrentRow;
end;

function TdxBoxFormatterStateBase.GetForceFormattingComplete: Boolean;
begin
  Result := Formatter.ForceFormattingComplete;
end;

function TdxBoxFormatterStateBase.GetFormattingComplete: Boolean;
begin
  Result := ForceFormattingComplete;
end;

function TdxBoxFormatterStateBase.GetIterator: TdxParagraphIteratorBase;
begin
  Result := Formatter.Iterator;
end;

function TdxBoxFormatterStateBase.GetIterator_: TdxParagraphBoxIterator;
begin
  Result := GetIterator as TdxParagraphBoxIterator; 
end;

function TdxBoxFormatterStateBase.GetMeasurer: TdxBoxMeasurer;
begin
  Result := Formatter.Measurer;
end;

function TdxBoxFormatterStateBase.GetRowsController: TdxRowsController;
begin
  Result := FFormatter.RowsController;
end;

function TdxBoxFormatterStateBase.IsCurrentRowEmpty: Boolean;
begin
  Result := RowsController.CurrentRow.Boxes.Count <= 0;
end;

procedure TdxBoxFormatterStateBase.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Formatter.Measurer.MeasureText(ABoxInfo);
end;

procedure TdxBoxFormatterStateBase.MeasureBoxContentCore(ABoxInfo: TdxBoxInfo);
var
  ABox: TdxBox;
  ALayoutDependentTextBox: TdxLayoutDependentTextBox;
begin
  ABox := ABoxInfo.Box;
  ALayoutDependentTextBox := nil;
  if ABox is TdxLayoutDependentTextBox then
    ALayoutDependentTextBox := TdxLayoutDependentTextBox(ABox);
  if (ABox <> nil) and (ALayoutDependentTextBox = nil) then
    ABoxInfo.Size := ABox.Bounds.Size
  else
    if ALayoutDependentTextBox <> nil then
      CalculateAndMeasureLayoutDependentTextBox(ALayoutDependentTextBox, ABoxInfo)
    else
      MeasureBoxContent(ABoxInfo);
end;

function TdxBoxFormatterStateBase.ObtainAddBoxResult(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  if Iterator.CurrentChar = TdxCharacters.Hyphen then
    Result := CanAddBox(ABoxInfo)
  else
    Result := CanAddBoxWithoutHyphen(ABoxInfo);
end;

procedure TdxBoxFormatterStateBase.SetCurrentRowHeightToLastBoxHeight;
var
  ACurrentRow: TdxRow;
  ABoxesCount: Integer;
begin
  ACurrentRow := CurrentRow;
  Assert(ACurrentRow.Height <= 0);
  ABoxesCount := ACurrentRow.Boxes.Count;
  Assert(ABoxesCount > 0);
  ACurrentRow.Height := ACurrentRow.Boxes[ABoxesCount - 1].Bounds.Height;
end;

procedure TdxBoxFormatterStateBase.SetForceFormattingComplete(const Value: Boolean);
begin
  Formatter.ForceFormattingComplete := Value;
end;

function TdxBoxFormatterStateBase.SplitBox(ABoxInfo: TdxBoxInfo): TdxSplitBoxResult;
var
  AStartPos: TdxFormatterPosition;
  AIterator: TdxParagraphBoxIterator;
begin
  AStartPos := ABoxInfo.StartPos;

  Assert(AStartPos.RunIndex = ABoxInfo.EndPos.RunIndex);

  if AdjustEndPositionToFit(ABoxInfo) = TdxAdjustEndPositionResult.Success then
  begin
    Iterator.SetNextPosition(ABoxInfo.EndPos);
    Result := TdxSplitBoxResult.Success;
    Exit;
  end;

  AIterator := Iterator;
  AIterator.SetPosition(AStartPos);
  if IsCurrentRowEmpty then
  begin
    AIterator.Next;
    Result := TdxSplitBoxResult.SuccessSuppressedHorizontalOverfull;
  end
  else
    Result := TdxSplitBoxResult.FailedHorizontalOverfull;
end;

{ TdxCurrentHorizontalPositionController }

constructor TdxCurrentHorizontalPositionController.Create(ARowsController: TdxRowsController; APosition: Integer);
begin
  inherited Create;
  Assert(ARowsController <> nil);
  FRowsController := ARowsController;
  FCurrentHorizontalPosition := APosition;
end;

constructor TdxCurrentHorizontalPositionController.Create(ARowsController: TdxRowsController);
begin
  Create(ARowsController, 0);
end;

function TdxCurrentHorizontalPositionController.CalculateBoxBounds(ABoxInfo: TdxBoxInfo): TRect;
begin
  Result.InitSize(CurrentHorizontalPosition, 0, ABoxInfo.Size); 
end;

function TdxCurrentHorizontalPositionController.CalculateFloatingObjectColumnBounds(
  ACurrentColumn: TdxColumn): TRect;
begin
  Result := ACurrentColumn.Bounds;
end;

function TdxCurrentHorizontalPositionController.CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean;
begin
  Result := (CurrentHorizontalPosition + ABoxSize.cx <= FRowsController.CurrentRow.Bounds.Right) or 
    FRowsController.SuppressHorizontalOverfull;
end;

function TdxCurrentHorizontalPositionController.CanFitCurrentRowToColumn(
  ANewRowHeight: Integer): TdxCanFitCurrentRowToColumnResult;
begin
  Result := RowsController.TablesController.CanFitRowToColumn(RowsController.CurrentRow.Bounds.Top + ANewRowHeight,
    RowsController.CurrentColumn);
end;

function TdxCurrentHorizontalPositionController.CanFitNumberingListBoxToCurrentRow(const ABoxSize: TSize): Boolean;
begin
  Result := True;
end;

function TdxCurrentHorizontalPositionController.GetTextAreaForTable: TdxTextArea;
var
  ABounds: TRect;
begin
  ABounds := RowsController.CurrentColumn.Bounds;
  Result := TdxTextArea.Create(ABounds.Left, ABounds.Right);
end;

procedure TdxCurrentHorizontalPositionController.IncrementCurrentHorizontalPosition(ADelta: Integer);
begin
  FCurrentHorizontalPosition := FCurrentHorizontalPosition + ADelta;
end;

function TdxCurrentHorizontalPositionController.MoveCurrentRowDownToFitTable(ATableWidth, ATableTop: Integer): Integer;
begin
  Result := ATableTop;
end;

procedure TdxCurrentHorizontalPositionController.OnCurrentRowFinished;
begin
end;

procedure TdxCurrentHorizontalPositionController.OnCurrentRowHeightChanged(AKeepTextAreas: Boolean);
begin
end;

procedure TdxCurrentHorizontalPositionController.RollbackCurrentHorizontalPositionTo(AValue: Integer);
begin
  Assert(AValue <= FCurrentHorizontalPosition);
  FCurrentHorizontalPosition := AValue;
end;

procedure TdxCurrentHorizontalPositionController.SetCurrentHorizontalPosition(AValue: Integer);
begin
  FCurrentHorizontalPosition := AValue;
end;

procedure TdxCurrentHorizontalPositionController.SetCurrentRowInitialHorizontalPosition;
begin
  FCurrentHorizontalPosition := FRowsController.ParagraphLeft + FRowsController.CurrentRowIndent;
end;

{ TdxRowsController }

constructor TdxRowsController.Create(APieceTable: TdxPieceTable; AColumnController: IColumnController; AMatchHorizontalTableIndentsToTextEdge: Boolean);
begin
  inherited Create;
  Assert(APieceTable <> nil);
  Assert(AColumnController <> nil);
  FColumnController := AColumnController;
  FTabsController := TdxTabsController.Create;
  FTablesController := CreateTablesController;
  FPieceTable := APieceTable;
  FCurrentRunIndex := -1; 
  FFloatingObjectsLayout := AColumnController.PageAreaController.PageController.FloatingObjectsLayout;
  Assert(FFloatingObjectsLayout <> nil);
  FHorizontalPositionController := CreateCurrentHorizontalPosition;
  FLineNumberStep := 1; 
  FBoxes := TdxFastObjectList.Create(False);
  FMatchHorizontalTableIndentsToTextEdge := AMatchHorizontalTableIndentsToTextEdge;
  FFrameParagraphIndex := -1;
  FLastRestartDueToFloatingObjectModelPosition.Create(APieceTable);
end;

destructor TdxRowsController.Destroy;
begin
  FreeAndNil(FBoxes);
  FreeAndNil(FLineSpacingCalculator);
  FreeAndNil(FHorizontalPositionController);
  FreeAndNil(FTablesController);
  FreeAndNil(FTabsController);
  FreeAndNil(FCurrentRow);
  inherited Destroy;
end;

function TdxRowsController.AddBox(ABoxType: TdxBoxClass; ABoxInfo: TdxBoxInfo): TdxBox;
var
  ABox: TdxBox;
  ASpaceBox: ISpaceBox;
  ARunIndex: TdxRunIndex;
  ARun: TdxTextRunBase;
begin
  ABox := GetBox(ABoxType, ABoxInfo);
  ABox.Bounds := HorizontalPositionController.CalculateBoxBounds(ABoxInfo);
  if Supports(ABox, ISpaceBox, ASpaceBox) then
    ASpaceBox.MinWidth := ABoxInfo.Size.cx; 
  CurrentRow.Boxes.Add(ABox);
  HorizontalPositionController.IncrementCurrentHorizontalPosition(ABoxInfo.Size.cx); 
  ARunIndex := ABoxInfo.StartPos.RunIndex;
  ARun := PieceTable.Runs[ARunIndex];
  CurrentRow.ProcessingFlags := CurrentRow.ProcessingFlags + ARun.RowProcessingFlags;
  Result := ABox;
end;

function TdxRowsController.AddFloatingObjectToLayout(AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox;
  ABoxInfo: TdxBoxInfo): Boolean;
var
  AObjectAnchorRun: TdxFloatingObjectAnchorRun;
  AFloatingObjectBox: TdxFloatingObjectBox;
  AAnchorBox: TdxFloatingObjectAnchorBox;
begin
  AObjectAnchorRun := TdxFloatingObjectAnchorRun(PieceTable.Runs[ABoxInfo.StartPos.RunIndex]);
  if AObjectAnchorRun.ExcludeFromLayout then
    Exit(False);
  Assert((ABoxInfo.Box <> nil) and (ABoxInfo.Box is TdxFloatingObjectAnchorBox) and (ABoxInfo.Box = AFloatingObjectAnchorBox));
  if FloatingObjectsLayout.ContainsRun(AObjectAnchorRun) then
  begin
    AFloatingObjectBox := FloatingObjectsLayout.GetFloatingObject(AObjectAnchorRun);
    AAnchorBox := TdxFloatingObjectAnchorBox(ABoxInfo.Box);
    if ShouldChangeExistingFloatingObjectBounds(AFloatingObjectBox, AAnchorBox) then
    begin
      ApplyFloatingObjectBoxBounds(AFloatingObjectBox, AAnchorBox);
      if AObjectAnchorRun.Content is TdxTextBoxFloatingObjectContent then
        if AObjectAnchorRun.Content is TdxTextBoxFloatingObjectContent then
          FormatFloatingObjectTextBox(AFloatingObjectBox, TdxTextBoxFloatingObjectContent(AObjectAnchorRun.Content), AAnchorBox)
        else
          FormatFloatingObjectTextBox(AFloatingObjectBox, nil, AAnchorBox);
    end;
    Result := False;
  end
  else
  begin
    AFloatingObjectBox := TdxFloatingObjectBox.Create;
    AddFloatingObjectToLayoutCore(AFloatingObjectBox, ABoxInfo);
    Result := True;
  end;
end;

procedure TdxRowsController.AddFloatingObjectToLayoutCore(AFloatingObjectBox: TdxFloatingObjectBox;
  ABoxInfo: TdxBoxInfo);
var
  AContent: TdxTextBoxFloatingObjectContent;
  AAnchorBox: TdxFloatingObjectAnchorBox;
  AObjectAnchorRun: TdxFloatingObjectAnchorRun;
begin
  AObjectAnchorRun := TdxFloatingObjectAnchorRun(PieceTable.Runs[ABoxInfo.StartPos.RunIndex]);
  AFloatingObjectBox.StartPos := ABoxInfo.StartPos;
  ApplyFloatingObjectBoxProperties(AFloatingObjectBox, AObjectAnchorRun.FloatingObjectProperties);
  AAnchorBox := TdxFloatingObjectAnchorBox(ABoxInfo.Box);
  ApplyFloatingObjectBoxBounds(AFloatingObjectBox, AAnchorBox);
  if AObjectAnchorRun.Content is TdxTextBoxFloatingObjectContent then
  begin
    if AObjectAnchorRun.Content is TdxTextBoxFloatingObjectContent then
      AContent := TdxTextBoxFloatingObjectContent(AObjectAnchorRun.Content)
    else
      AContent := nil;
    FormatFloatingObjectTextBox(AFloatingObjectBox, AContent, AAnchorBox);
  end;
  FloatingObjectsLayout.Add(AObjectAnchorRun, AFloatingObjectBox);
  UpdateCurrentTableCellBottom(AFloatingObjectBox);
  RecreateHorizontalPositionController;
end;

procedure TdxRowsController.AddNumberingListBox(ABox: TdxNumberingListBox; AProperties: TdxListLevelProperties;
  AFormatter: TdxParagraphBoxFormatter);
var
  ABounds: TRect;
begin
  FCurrentParagraphNumberingListBounds := ABox.Bounds;
  ABounds := CalculateNumberingListBoxBounds(ABox, AProperties, AFormatter);
  ABox.Bounds := ABounds;
  HorizontalPositionController.SetCurrentHorizontalPosition(ABounds.Right);
end;

procedure TdxRowsController.AddPageBreakBeforeRow;
var
  ATopLevelColumn: TdxColumn;
  R: TRect;
begin
  ATopLevelColumn := CurrentColumn.TopLevelColumn;
  R := ATopLevelColumn.Bounds;
  R.Height := Max(CurrentRow.Bounds.Top - R.Top, 0);
  ATopLevelColumn.Bounds := R;
end;

procedure TdxRowsController.SaveCurrentInfo;
begin
  FSavedCurrentColumnBounds := CurrentColumn.Bounds;
end;

function TdxRowsController.CanAddSectionBreakToPrevRow: Boolean;
begin
  Result := CurrentColumn.Rows.Last <> nil;
end;

function TdxRowsController.CompleteCurrentColumnFormatting: TdxCompleteFormattingResult;
begin
  Result := ColumnController.CompleteCurrentColumnFormatting(CurrentColumn);
end;

function TdxRowsController.CreateCurrentHorizontalPosition: TdxCurrentHorizontalPositionController;
begin
  Result := TdxCurrentHorizontalPositionController.Create(Self);
end;

function TdxRowsController.CreateCurrentHorizontalPosition(APosition: Integer): TdxCurrentHorizontalPositionController;
begin
  Result := TdxCurrentHorizontalPositionController.Create(Self, APosition);
end;

procedure TdxRowsController.AddSectionBreakBoxToPrevRow(ABoxType: TdxBoxClass; ABoxInfo: TdxBoxInfo);
var
  ABox: TdxBox;
  ALastRow: TdxRow;
begin
  ALastRow := CurrentColumn.Rows.Last;
  ABox := GetBox(ABoxType, ABoxInfo);
  ABox.Bounds := cxRect(Point(ALastRow.Boxes.Last.Bounds.Right, 0), cxPoint(ABoxInfo.Size)); 
  ALastRow.Boxes.Add(ABox);
end;

procedure TdxRowsController.AddSingleDecimalTabInTable;
begin
  if CurrentHorizontalPosition > FParagraphRight then
    SuppressHorizontalOverfull := True;
  FTabsController.SaveLastTabPosition(-1);
end;

procedure TdxRowsController.AddTabBox(ATab: TdxTabInfo; ABoxInfo: TdxBoxInfo);
var
  ABox: TdxTabSpaceBox;
  ATabPosition, ANewPosition: Integer;
begin
  if ATab.Alignment = TdxTabAlignmentType.Left then
  begin
    ATabPosition := ATab.GetLayoutPosition(DocumentModel.ToDocumentLayoutUnitConverter);
    ANewPosition := ATabPosition + CurrentColumn.Bounds.Left;
    ABoxInfo.Size := cxSize(ANewPosition - CurrentHorizontalPosition, ABoxInfo.Size.cy);
  end
  else
    ANewPosition := CurrentHorizontalPosition;
  if (ANewPosition > FParagraphRight) and not ATab.IsDefault then
    SuppressHorizontalOverfull := True;
  FTabsController.SaveLastTabPosition(CurrentRow.Boxes.Count);
  ABox := TdxTabSpaceBox(AddBox(TdxTabSpaceBox, ABoxInfo));
  ABox.TabInfo := ATab.Clone;
  ABox.LeaderCount := TdxTabsController.CalculateLeaderCount(ABox, PieceTable);
end;

procedure TdxRowsController.ApplyCurrentColumnBounds(const ABounds: TRect);
var
  AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
begin
  AUnitConverter := DocumentModel.ToDocumentLayoutUnitConverter;
  FParagraphLeft := ABounds.Left + AUnitConverter.ToLayoutUnits(FParagraph.LeftIndent);
  FParagraphRight := ABounds.Right - AUnitConverter.ToLayoutUnits(FParagraph.RightIndent);
  HorizontalPositionController.SetCurrentRowInitialHorizontalPosition;
  FTabsController.ColumnLeft := ABounds.Left;
  FTabsController.FParagraphRight := FParagraphRight;
end;

procedure TdxRowsController.ApplyFloatingObjectBoxBounds(AFloatingObjectBox: TdxFloatingObjectBox;
  AAnchorBox: TdxFloatingObjectAnchorBox);
begin
  AFloatingObjectBox.ExtendedBounds := AAnchorBox.ActualBounds;
  AFloatingObjectBox.Bounds := AAnchorBox.ShapeBounds;
  AFloatingObjectBox.ContentBounds := AAnchorBox.ContentBounds;
  AFloatingObjectBox.SetActualSizeBounds(AAnchorBox.ActualSizeBounds);
end;

procedure TdxRowsController.ApplyFloatingObjectBoxProperties(AFloatingObjectBox: TdxFloatingObjectBox;
  AFloatingObjectProperties: TdxFloatingObjectProperties);
begin
  AFloatingObjectBox.CanPutTextAtLeft := AFloatingObjectProperties.CanPutTextAtLeft;
  AFloatingObjectBox.CanPutTextAtRight := AFloatingObjectProperties.CanPutTextAtRight;
  AFloatingObjectBox.PutTextAtLargestSide := AFloatingObjectProperties.PutTextAtLargestSide;
  AFloatingObjectBox.PieceTable := AFloatingObjectProperties.PieceTable;
end;

procedure TdxRowsController.ApplySectionStart(ASection: TdxSection);
var
  ARestart: TdxLineNumberingRestart;
begin
  if ASection.GeneralSettings.StartType in [TdxSectionStartType.EvenPage, TdxSectionStartType.OddPage,
    TdxSectionStartType.NextPage] then
    EnforceNextRowToStartFromNewPage;
  ARestart := GetEffectiveLineNumberingRestartType(ASection);
  if ARestart = TdxLineNumberingRestart.NewSection then
  begin
    InitialLineNumber := ASection.LineNumbering.StartingLineNumber;
    LineNumberStep := ASection.LineNumbering.Step;
  end
  else
    if ARestart = TdxLineNumberingRestart.NewPage then
    begin
      InitialLineNumber := ASection.LineNumbering.StartingLineNumber;
      LineNumberStep := ASection.LineNumbering.Step;
    end;
end;

procedure TdxRowsController.AssignCurrentRowLineNumber;
begin
  if not FParagraph.SuppressLineNumbers and not TablesController.IsInsideTable and ((LineNumber mod LineNumberStep) = 0) then
    CurrentRow.LineNumber := LineNumber
  else
    CurrentRow.LineNumber := -LineNumber;
end;

procedure TdxRowsController.BeginParagraph(AParagraph: TdxParagraph; ABeginFromParagraphStart: Boolean);
var
  ASpacingBefore: Integer;
begin
  Assert(AParagraph.PieceTable = PieceTable); 
  EnsureSpacingAfterPrevParagraphValid(AParagraph);
  FParagraph := AParagraph;
  PrepareCurrentRowBounds(AParagraph, ABeginFromParagraphStart);
  TablesController.BeginParagraph(AParagraph);
  PrepareCurrentRowBounds(AParagraph, ABeginFromParagraphStart);
  ASpacingBefore := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(AParagraph.ContextualSpacingBefore);
  if ASpacingBefore < FSpacingAfterPrevParagraph then
    ASpacingBefore := 0
  else
    ASpacingBefore := ASpacingBefore - FSpacingAfterPrevParagraph;
  FInvisibleEmptyParagraphInCellAfterNestedTable := ShouldIgnoreParagraphHeight(AParagraph);
  if FInvisibleEmptyParagraphInCellAfterNestedTable then
  begin
    CurrentRow.ProcessingFlags := CurrentRow.ProcessingFlags + [TdxRowProcessingFlag.LastInvisibleEmptyCellRowAfterNestedTable];
    CurrentRow.Height := 0;
    ASpacingBefore := 0;
  end;
  CurrentRow.SpacingBefore := ASpacingBefore;
  CurrentRow.ProcessingFlags := CurrentRow.ProcessingFlags + [TdxRowProcessingFlag.FirstParagraphRow];
  OffsetCurrentRow(ASpacingBefore);
  HorizontalPositionController.OnCurrentRowHeightChanged(False);
  HorizontalPositionController.SetCurrentRowInitialHorizontalPosition;
  AssignCurrentRowLineNumber;
  if FTabsController.SingleDecimalTabInTable then
    AddSingleDecimalTabInTable;
end;

procedure TdxRowsController.BeginSectionFormatting(ASection: TdxSection);
begin
  ApplySectionStart(ASection);
  FSpacingAfterPrevParagraph := 0;
  FIsSpacingAfterPrevParagraphValid := True;
end;

function TdxRowsController.CalcDefaultRowBounds(Y: Integer): TRect;
begin
  Result := Rect(ParagraphLeft + FCurrentRowIndent, Y, FParagraphRight, Y + DefaultRowHeight);
end;

function TdxRowsController.CalcNewCurrentRowHeight(ANewBoxInfo: TdxBoxInfo): Integer;
begin
  Result := FLineSpacingCalculator.CalcRowHeight(CurrentRow.Height, ANewBoxInfo.Size.cy); 
end;

function TdxRowsController.CalcPrevBottom(APrevRow: TdxRow; AAfterRestart: Boolean; ANewRow: TdxRow): Integer;
var
  ATableCellRow: TdxTableCellRow;
begin
  if APrevRow = nil then
    Result := CurrentColumn.Bounds.Top
  else
  begin
    Result := APrevRow.Bounds.Bottom;
    if not AAfterRestart then
      Exit;
    if not (APrevRow is TdxTableCellRow) then
      Exit;
    ATableCellRow := TdxTableCellRow(APrevRow);
    if not (ANewRow is TdxTableCellRow) or (TdxTableCellRow(ANewRow).CellViewInfo.Cell.Table <> ATableCellRow.CellViewInfo.Cell.Table) then
      Result := ATableCellRow.CellViewInfo.TableViewInfo.GetTableBottom;
  end;
end;

function TdxRowsController.CalculateNumberingListBoxBounds(ABox: TdxNumberingListBox;
  AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): TRect;
var
  ALeft: Integer;
  AController: TdxFloatingObjectsCurrentHorizontalPositionController;
begin
  ALeft := CurrentRow.Bounds.Left;
  Result := CalculateNumberingListBoxBoundsCore(ABox, AProperties, AFormatter);
  CurrentRow.TextOffset := Result.Right - ALeft;

  if not (HorizontalPositionController is TdxFloatingObjectsCurrentHorizontalPositionController) then
    Exit;
  AController := TdxFloatingObjectsCurrentHorizontalPositionController(HorizontalPositionController);
  if CanFitNumberingListBoxToCurrentRow(Result.Size) then
    Exit;
  while AController.AdvanceHorizontalPositionToNextTextArea do
  begin
    ALeft := AController.CurrentHorizontalPosition;
    Result := CalculateNumberingListBoxBoundsCore(ABox, AProperties, AFormatter);
    if CanFitNumberingListBoxToCurrentRow(Result.Size) then
      Break;
  end;
  CurrentRow.TextOffset := Result.Right - ALeft;
end;

function TdxRowsController.CalculateNumberingListBoxBoundsCore(ABox: TdxNumberingListBox;
  AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): TRect;
var
  ABounds, ASeparatorBounds: TRect;
  AAlignment: TdxListNumberAlignment;
  ANumberingListBoxLeft, ASeparatorWidth: Integer;
  ABoxWithSeparator: TdxNumberingListBoxWithSeparator;
begin
  ABounds := ABox.Bounds;
  AAlignment := AProperties.Alignment;
  ANumberingListBoxLeft := CalculateNumberingListBoxLeft(ABounds.Width, AAlignment);

  ABounds.InitSize(ANumberingListBoxLeft, 0, ABounds.Width, ABounds.Height);
  CurrentRow.NumberingListBox := ABox;

  if ABox is TdxNumberingListBoxWithSeparator then
  begin
    ABoxWithSeparator := TdxNumberingListBoxWithSeparator(ABox);
    if Supports(ABoxWithSeparator.SeparatorBox, ISpaceBox) then
      ABounds.Width := ABounds.Width + ABoxWithSeparator.SeparatorBox.Bounds.Width
    else
    begin
      Assert(ABoxWithSeparator.SeparatorBox is TdxTabSpaceBox);
      if AProperties.Legacy then
        ASeparatorWidth := GetLegacyNumberingListBoxSeparatorWidth(ABoxWithSeparator, ABounds, AProperties, AFormatter)
      else
        ASeparatorWidth := GetNumberingListBoxSeparatorWidth(ABoxWithSeparator, ABounds, AProperties, AFormatter);
      ABounds.Width := ABounds.Width + ASeparatorWidth;
      ASeparatorBounds := ABoxWithSeparator.SeparatorBox.Bounds;
      ASeparatorBounds.Width := ASeparatorWidth;
      ABoxWithSeparator.SeparatorBox.Bounds := ASeparatorBounds;
    end;
  end;
  Result := ABounds;
end;

function TdxRowsController.CalculateNumberingListBoxLeft(ABoxWidth: Integer;
  AAlignment: TdxListNumberAlignment): Integer;
var
  ABoundsLeft: Integer;
begin
  ABoundsLeft := CurrentHorizontalPosition;
  case AAlignment of
    TdxListNumberAlignment.Left:
      Result := ABoundsLeft;
    TdxListNumberAlignment.Center:
      Result := Trunc(ABoundsLeft - ABoxWidth / 2);
    TdxListNumberAlignment.Right:
      Result := ABoundsLeft - ABoxWidth;
  else
    raise Exception.Create('Error Message'); 
    Result := 0;
  end;
end;

function TdxRowsController.CalculateRestartPositionDueFloatingObject(
  AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox; ACurrentPosition: TPoint;
  AWrapType: TdxFloatingObjectTextWrapType): TdxDocumentModelPosition;
var
  ABounds, ARowBounds: TRect;
  APageController: TdxPageController;
  APages: TdxPageCollection;
  ACurrentPage: TdxPage;
  AHitTestRequest: TdxRichEditHitTestRequest;
  AHitTestResult: TdxRichEditHitTestResult;
  ACalculator: TdxBoxHitTestCalculator;
  ATableCellColumnController: TdxTableCellColumnController;
  ARows: TdxRowCollection;
  ATable: TdxTable;
  ALastTableCell: TdxTableCell;
begin
  if AWrapType = TdxFloatingObjectTextWrapType.None then
    Exit(TdxDocumentModelPosition.Null);
  ABounds := AFloatingObjectAnchorBox.ActualBounds;
  if (ABounds.Left >= ACurrentPosition.X) and (ABounds.Top >= ACurrentPosition.Y) then
    Exit(TdxDocumentModelPosition.Null);
  APageController := ColumnController.PageAreaController.PageController;
  APages := APageController.Pages;
  ACurrentPage := APages.Last;
  if ACurrentPage.IsEmpty then
  begin
    if FloatingObjectsLayout.ContainsRun(AFloatingObjectAnchorBox.GetFloatingObjectRun(PieceTable)) then
      Exit(TdxDocumentModelPosition.Null);
    if CurrentRow.IsFirstParagraphRow then
      Exit(TdxDocumentModelPosition.FromParagraphStart(PieceTable, Paragraph.Index));
    if CurrentRow.Boxes.Count > 0 then
      Exit(CurrentRow.GetFirstPosition(PieceTable))
    else
      Exit(AFloatingObjectAnchorBox.GetFirstPosition(PieceTable));
  end;
  AHitTestRequest := TdxRichEditHitTestRequest.Create(PieceTable);
  AHitTestRequest.LogicalPoint := ABounds.Location;
  AHitTestRequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Row;
  AHitTestRequest.Accuracy := NearestPageArea or NearestColumn or NearestRow;
  AHitTestResult := TdxRichEditHitTestResult.Create(APageController.DocumentLayout, PieceTable);
  AHitTestResult.Page := ACurrentPage;
  AHitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Page);
  ACalculator := APageController.CreateHitTestCalculator(AHitTestRequest, AHitTestResult);
  try
    if ColumnController is TdxTableCellColumnController then
      ATableCellColumnController := TdxTableCellColumnController(ColumnController)
    else
      ATableCellColumnController := nil;

    if ATableCellColumnController <> nil then
    begin
      ATableCellColumnController := TdxTableCellColumnController(ColumnController);
      AHitTestResult.PageArea := ColumnController.PageAreaController.Areas.Last;
      AHitTestResult.Column := ATableCellColumnController.CurrentTopLevelColumn;
      AHitTestResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Column);
      ACalculator.CalcHitTest(CurrentColumn);
      if not AHitTestResult.IsValid(TdxDocumentLayoutDetailsLevel.Row) then
      begin
        if FloatingObjectsLayout.ContainsRun(afloatingObjectAnchorBox.GetFloatingObjectRun(PieceTable)) then
          Exit(TdxDocumentModelPosition.Null)
        else
          Exit(afloatingObjectAnchorBox.GetFirstPosition(PieceTable));
      end;
    end
    else
      ACalculator.CalcHitTest(ACurrentPage);
  finally
    ACalculator.Free;
  end;
  Assert(AHitTestResult.IsValid(TdxDocumentLayoutDetailsLevel.Row));
  Assert(AHitTestResult.Row <> nil);
  ARows := AHitTestResult.Column.Rows;
  if ARows.Last = AHitTestResult.Row then
  begin
    ARowBounds := AHitTestResult.Row.Bounds;
    if (ABounds.Top > ARowBounds.Bottom) and not ARowBounds.IntersectsWith(ABounds) then
      Exit(TdxDocumentModelPosition.Null)
  end;
  if AHitTestResult.TableCell <> nil then
  begin
    if ATableCellColumnController = nil then
    begin
      ATable := AHitTestResult.TableCell.Cell.Table;
      while ATable.ParentCell <> nil do
        ATable := ATable.ParentCell.Table;
      ALastTableCell := ATable.Rows.Last.Cells.Last;
      Exit(TdxDocumentModelPosition.FromParagraphStart(PieceTable, ALastTableCell.EndParagraphIndex + 1));
    end;
  end;
  Result := AHitTestResult.Row.GetFirstPosition(PieceTable);
end;

function TdxRowsController.CalculateRestartPositionDueFloatingObject(
  AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox): TdxDocumentModelPosition;
var
  X, Y: Integer;
  ARun: TdxFloatingObjectAnchorRun;
begin
  X := CurrentHorizontalPosition;
  Y := CurrentRow.Bounds.Top;
  ARun := TdxFloatingObjectAnchorRun(PieceTable.Runs[AFloatingObjectAnchorBox.StartPos.RunIndex]);
  if ARun.ExcludeFromLayout then
    Result.Invalidate
  else
    Result := CalculateRestartPositionDueFloatingObject(AFloatingObjectAnchorBox, Point(X, Y), ARun.FloatingObjectProperties.TextWrapType);
end;

function TdxRowsController.CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean;
begin
  Result := HorizontalPositionController.CanFitBoxToCurrentRow(ABoxSize);
end;

function TdxRowsController.CanFitCurrentRowToColumn(ANewBoxInfo: TdxBoxInfo): TdxCanFitCurrentRowToColumnResult;
var
  ABox: TdxBox;
  ABoxIndex: Integer;
  ARowHeight: Integer;
  AFloatingObjectBox: TdxFloatingObjectBox;
  ARestartPosition: TdxDocumentModelPosition;
  AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox;
  AController: TdxFloatingObjectSizeAndPositionController;
begin
  if ANewBoxInfo.Box = nil then
  begin
    ABoxIndex := ANewBoxInfo.StartPos.BoxIndex;
    if ABoxIndex < Paragraph.BoxCollection.Count then
    begin
      ABox := Paragraph.BoxCollection[ABoxIndex];
      if ABox is TdxFloatingObjectAnchorBox then
        ANewBoxInfo.Box := ABox;
    end;
  end;
  if ANewBoxInfo.Box is TdxFloatingObjectAnchorBox then
  begin
    AFloatingObjectAnchorBox := TdxFloatingObjectAnchorBox(ANewBoxInfo.Box);
    AController := CreateFloatingObjectSizeAndPositionController;
    try
      AController.UpdateFloatingObjectBox(AFloatingObjectAnchorBox);
    finally
      AController.Free;
    end;
    ARestartPosition := CalculateRestartPositionDueFloatingObject(AFloatingObjectAnchorBox);
    if ARestartPosition.IsValid then
      if AddFloatingObjectToLayout(AFloatingObjectAnchorBox, ANewBoxInfo) and CanRestartDueFloatingObject then
      begin
        AFloatingObjectBox := FloatingObjectsLayout.GetFloatingObject(AFloatingObjectAnchorBox.GetFloatingObjectRun(PieceTable));
        AFloatingObjectBox.LockPosition := True;
        RestartModelPosition := ARestartPosition;
        Exit(TdxCanFitCurrentRowToColumnResult.RestartDueFloatingObject);
      end;
  end;
  ARowHeight := CalcNewCurrentRowHeight(ANewBoxInfo);
  Result := CanFitCurrentRowToColumnCore(ARowHeight);
end;

function TdxRowsController.CanFitCurrentRowToColumnCore(ANewRowHeight: Integer): TdxCanFitCurrentRowToColumnResult;
begin
  Result := HorizontalPositionController.CanFitCurrentRowToColumn(ANewRowHeight);
end;

function TdxRowsController.CanFitNumberingListBoxToCurrentRow(const ABoxSize: TSize): Boolean;
begin
  Result := HorizontalPositionController.CanFitNumberingListBoxToCurrentRow(ABoxSize);
end;

function TdxRowsController.CanRestartDueFloatingObject: Boolean;
begin
  Result := True;
end;

function TdxRowsController.CanFitCurrentRowToColumn: TdxCanFitCurrentRowToColumnResult;
var
  ARowHeight: Integer;
begin
  ARowHeight := FLineSpacingCalculator.CalcRowHeight(CurrentRow.Height, CurrentRow.Height);
  Result := CanFitCurrentRowToColumnCore(ARowHeight);
end;

procedure TdxRowsController.ClearInvalidatedContent(AColumn: TdxColumn; APos: TdxFormatterPosition;
  AParagraphIndex: TdxParagraphIndex);
begin
  ClearInvalidatedContentCore(AColumn, APos, AParagraphIndex);
  TablesController.ClearInvalidatedContent;
  FColumnController := GetTopLevelColumnController;
  RecreateHorizontalPositionController;
end;

procedure TdxRowsController.ClearInvalidatedContentCore(AColumn: TdxColumn; APos: TdxFormatterPosition;
  AParagraphIndex: TdxParagraphIndex);
var
  ARows: TdxRowCollection;
  ARowIndex: Integer;
begin
  AColumn.ClearInvalidatedParagraphFrames(AParagraphIndex);
  ARows := AColumn.Rows;
  ARowIndex := ARows.BinarySearchBoxIndex(APos);
  if ARowIndex < 0 then
    ARowIndex := not ARowIndex;
  if ARowIndex < ARows.Count then
    ARows.DeleteRange(ARowIndex, ARows.Count - ARowIndex);
  FParagraph := PieceTable.Paragraphs[AParagraphIndex];
  ApplyCurrentColumnBounds(FSavedCurrentColumnBounds);
end;

procedure TdxRowsController.ClearInvalidatedContentFromTheStartOfRowAtCurrentPage(AColumn: TdxColumn;
  APos: TdxFormatterPosition; AParagraphIndex: TdxParagraphIndex);
begin
  ClearInvalidatedContentCore(AColumn, APos, AParagraphIndex);
  RecreateHorizontalPositionController;
end;

procedure TdxRowsController.ClearRow(AKeepTextAreas: Boolean);
begin
  CurrentRow.Boxes.Clear;
  CurrentRow.ClearBoxRanges;
  InitCurrentRow(AKeepTextAreas);
end;

function TdxRowsController.CreateFloatingObjectSizeAndPositionController: TdxFloatingObjectSizeAndPositionController;
begin
  Result := TdxFloatingObjectSizeAndPositionController.Create(Self);
end;

procedure TdxRowsController.CreateLineNumberBoxes(ASection: TdxSection; APage: TdxPage);
var
  I: Integer;
  AAreas: TdxPageAreaCollection;
begin
  if ASection.LineNumbering.Step <= 0 then
    Exit;
  AAreas := APage.Areas;
  for I := 0 to AAreas.Count - 1 do
    CreateLineNumberBoxes(ASection, AAreas[i]);
end;

procedure TdxRowsController.CreateLineNumberBoxes(ASection: TdxSection; APageArea: TdxPageArea);
var
  I, ACount: Integer;
  AColumns: TdxColumnCollection;
begin
  APageArea.LineNumbers.Clear;
  AColumns := APageArea.Columns;
  ACount := AColumns.Count;
  for I := 0 to ACount - 1 do
    CreateLineNumberBoxes(ASection, AColumns[I], APageArea.LineNumbers);
end;

procedure TdxRowsController.CreateLineNumberBoxes(ASection: TdxSection; AColumn: TdxColumn;
  ALineNumbers: TdxLineNumberBoxCollection);
var
  AMeasurer: TdxBoxMeasurer;
  I, ADistance, ACount: Integer;
  ABoxInfo: TdxBoxInfo;
  AFontInfo: TdxFontInfo;
  ARows: TdxRowCollection;
  ARow: TdxRow;
  ABox: TdxLineNumberBox;
  AText: string;
  ABounds: TRect;
  ASize: TSize;
begin
  AMeasurer := FColumnController.Measurer;
  if AMeasurer = nil then
    Exit;
  ADistance := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(ASection.LineNumbering.Distance);
  if ADistance <= 0 then
    ADistance := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(DocumentModel.UnitConverter.DocumentsToModelUnits(75));
  ABoxInfo := TdxBoxInfo.Create;
  ABoxInfo.StartPos := TdxFormatterPosition.Create(0, 0, 0); 
  ABoxInfo.EndPos := TdxFormatterPosition.Create(0, 0, 0); 
  AFontInfo := DocumentModel.FontCache[DocumentModel.LineNumberRun.FontCacheIndex];
  ARows := AColumn.Rows;
  ACount := ARows.Count;
  for I := 0 to ACount - 1 do
  begin
    ARow := ARows[I];
    if ARow.LineNumber > 0 then
    begin
      AText := IntToStr(ARow.LineNumber);
      ABox := TdxLineNumberBox.Create(DocumentModel.LineNumberRun, ARow, AText);
      ABoxInfo.Box := ABox;
      AMeasurer.MeasureText(ABoxInfo, AText, AFontInfo);
      ABounds := ARow.Bounds;
      ASize := ABoxInfo.Size;
      ABounds.Left := AColumn.Bounds.Left - ADistance - ASize.cx;
      ABounds.Width := ASize.cx;
      ABox.Bounds := ABounds;
      ALineNumbers.Add(ABox);
    end;
  end;
end;

function TdxRowsController.CreateLineSpacingCalculator: TdxLineSpacingCalculatorBase;
begin
  Result := TdxLineSpacingCalculatorBase.GetLineSpacingCalculator(FParagraph);
end;

procedure TdxRowsController.CreateNewCurrentRow(APrevCurrentRow: TdxRow; AAfterRestart: Boolean);
var
  ANewRow: TdxRow;
  APrevBottom: Integer;
begin
  ANewRow := ColumnController.CreateRow;
  APrevBottom := CalcPrevBottom(APrevCurrentRow, AAfterRestart, ANewRow);
  ANewRow.Bounds := CalcDefaultRowBounds(APrevBottom);
  if AAfterRestart then
    FCurrentRow.Free;
  CurrentRow := ANewRow;
  InitCurrentRow(False);
end;

procedure TdxRowsController.CreateNewCurrentRowAfterRestart(ASection: TdxSection; AColumn: TdxColumn);
var
  ALastRow: TdxRow;
  APrevColumn: TdxColumn;
  AParagraphIndex: TdxParagraphIndex;
begin
  ALastRow := AColumn.GetOwnRows.Last;
  InitialLineNumber := ASection.LineNumbering.StartingLineNumber;
  if ALastRow = nil then
  begin
    APrevColumn := ColumnController.GetPreviousColumn(AColumn);
    if (APrevColumn = nil) or APrevColumn.IsEmpty then
      LineNumber := InitialLineNumber
    else
      LineNumber := Abs(APrevColumn.Rows.Last.LineNumber);
  end
  else
    LineNumber := Abs(ALastRow.LineNumber);
  AParagraphIndex := FParagraph.Index - 1;
  if AParagraphIndex >= 0 then 
    if not PieceTable.Paragraphs[AParagraphIndex].SuppressLineNumbers and not TablesController.IsInsideTable then
      LineNumber := LineNumber + 1;
  LineNumberStep := ASection.LineNumbering.Step;
  CreateNewCurrentRow(ALastRow, True);
end;

function TdxRowsController.CreateTableGridCalculator(ADocumentModel: TdxDocumentModel;
  AWidthsCalculator: TdxTableWidthsCalculator; AMaxTableWidth: Integer): TdxTableGridCalculator;
begin
  Result := TdxTableGridCalculator.Create(ADocumentModel, AWidthsCalculator, AMaxTableWidth,
    False, False); 
  NotImplemented;
end;

function TdxRowsController.CreateTablesController: TdxTablesController;
begin
  Result := TdxTablesController.Create(Self);
end;

procedure TdxRowsController.EndParagraph;
var
  ASpacingAfter: Integer;
  ALastRow: TdxRow;
begin
  ASpacingAfter := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(FParagraph.ContextualSpacingAfter);
  if FInvisibleEmptyParagraphInCellAfterNestedTable then
    ASpacingAfter := 0;
  ALastRow := IncreaseLastRowHeight(ASpacingAfter);
  ALastRow.LastParagraphRowOriginalHeight := ALastRow.Height - ASpacingAfter;
  ALastRow.ProcessingFlags := ALastRow.ProcessingFlags + [TdxRowProcessingFlag.LastParagraphRow];
  TablesController.EndParagraph(ALastRow);
  OffsetCurrentRow(ASpacingAfter);
  FSpacingAfterPrevParagraph := ASpacingAfter;
  FIsSpacingAfterPrevParagraphValid := True;
end;

procedure TdxRowsController.EndRow(AFormatter: TdxParagraphBoxFormatter);
begin
  EndRowCore(FParagraph, AFormatter);
end;

procedure TdxRowsController.EndRowCore(AParagraph: TdxParagraph; AFormatter: TdxParagraphBoxFormatter);
var
  ADescent: Integer;
  ALineSpacing: Integer;
  R: TRect;
  ASpaceBefore: Integer;
begin
  HorizontalPositionController.IncrementCurrentHorizontalPosition(FTabsController.CalcLastTabWidth(CurrentRow, AFormatter));
  CurrentRow.Paragraph := AParagraph;
  if ShouldAddParagraphFrameBox(AParagraph) then
    CurrentColumn.AddParagraphFrame(AParagraph);
  CurrentColumn.Rows.Add(CurrentRow);
  if not AParagraph.SuppressLineNumbers and not TablesController.IsInsideTable then
    Inc(FLineNumber);
  if ShouldUseMaxDescent then
    ADescent := FMaxDescent
  else
    ADescent := 0;
  ALineSpacing := FLineSpacingCalculator.CalculateSpacing(CurrentRow.Bounds.Bottom - CurrentRow.Bounds.Top, FMaxAscent, ADescent, FMaxPictureHeight);
  R := CurrentRow.Bounds;
  ASpaceBefore := CurrentRow.SpacingBefore;

  Dec(R.Top, ASpaceBefore);
  R.Height := ALineSpacing + ASpaceBefore;
  CurrentRow.Bounds := R;
  FCurrentRowIndent := FRegularRowIndent;
  TablesController.OnCurrentRowFinished;
  HorizontalPositionController.OnCurrentRowFinished;
  CreateNewCurrentRow(CurrentRow, False);
end;

procedure TdxRowsController.EndSection;
begin
  EndParagraph;
  if Paragraph.Index + 1 < PieceTable.Paragraphs.Count then
    RaiseBeginNextSectionFormatting;
  ColumnController.PageAreaController.PageController.NextSection := True;
  MoveRowToNextColumn;
end;

procedure TdxRowsController.EnforceNextRowToStartFromNewPage;
begin
  if CurrentColumn <> nil then
  begin
    CurrentRow.Bounds := CalcDefaultRowBounds(CurrentColumn.Bounds.Bottom);
    HorizontalPositionController.OnCurrentRowHeightChanged(False);
  end;
end;

procedure TdxRowsController.EnsureSpacingAfterPrevParagraphValid(ACurrentParagraph: TdxParagraph);
var
  APrevParagraphIndex: TdxParagraphIndex;
begin
  if FIsSpacingAfterPrevParagraphValid then
    Exit;
  APrevParagraphIndex := ACurrentParagraph.Index - 1;
  FSpacingAfterPrevParagraph := GetActualSpacingAfter(APrevParagraphIndex);
  FIsSpacingAfterPrevParagraphValid := True;
end;

procedure TdxRowsController.ExpandLastTab(AFormatter: TdxParagraphBoxFormatter);
var
  AOffset: Integer;
begin
  AOffset := FTabsController.CalcLastTabWidth(CurrentRow, AFormatter);
  HorizontalPositionController.IncrementCurrentHorizontalPosition(AOffset);
end;

procedure TdxRowsController.FormatFloatingObjectTextBox(AFloatingObjectBox: TdxFloatingObjectBox;
  AContent: TdxTextBoxFloatingObjectContent; AAnchorBox: TdxFloatingObjectAnchorBox);
begin
  NotImplemented;
end;

function TdxRowsController.GetActualSpacingAfter(AParagraphIndex: TdxParagraphIndex): Integer;
var
  AParagraph: TdxParagraph;
begin
  Result := 0;
  if AParagraphIndex < 0 then
		Exit;
  AParagraph := PieceTable.Paragraphs[AParagraphIndex];
  if not ShouldIgnoreParagraphHeight(AParagraph) then
    Result := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(AParagraph.ContextualSpacingAfter);
end;

function TdxRowsController.GetActualTabInfo(ATabInfo: TdxTabInfo; AProperties: TdxListLevelProperties): TdxTabInfo;
var
  AIndentTabPosition, ATabPosition: Integer;
begin
  if Paragraph.FirstLineIndentType = TdxParagraphFirstLineIndent.Hanging then
  begin
    Result := TdxTabInfo.Create(Paragraph.LeftIndent);
    AIndentTabPosition := Result.GetLayoutPosition(DocumentModel.ToDocumentLayoutUnitConverter);
    ATabPosition := ATabInfo.GetLayoutPosition(DocumentModel.ToDocumentLayoutUnitConverter);
    if not ATabInfo.IsDefault and (ATabPosition <= AIndentTabPosition) then
    begin
      Result.Free;
      Result := ATabInfo.Clone;
    end;
  end
  else
    Result := ATabInfo.Clone;
end;

function TdxRowsController.GetBox(ABoxType: TdxBoxClass; ABoxInfo: TdxBoxInfo): TdxBox;
var
  ABox: TdxBox;
begin
  if (ABoxInfo.Box <> nil) and not (ABoxInfo.Box is TdxFloatingObjectAnchorBox) then
  begin
    ABox := ABoxInfo.Box;
    Assert(ABox.StartPos.AreEqual(ABoxInfo.StartPos));
    Assert(ABox.EndPos.AreEqual(ABoxInfo.EndPos));
  end
  else
  begin
    ABox := ABoxType.CreateBox;
    ABox.StartPos := ABoxInfo.StartPos;
    ABox.EndPos := ABoxInfo.EndPos;
    FBoxes.Add(ABox);
  end;
  Result := ABox;
end;

function TdxRowsController.GetCurrentHorizontalPosition: Integer;
begin
  Result := FHorizontalPositionController.CurrentHorizontalPosition;
end;

function TdxRowsController.GetDocumentModel: TdxDocumentModel;
begin
  Result := PieceTable.DocumentModel;
end;

function TdxRowsController.GetEffectiveLineNumberingRestartType(ASection: TdxSection): TdxLineNumberingRestart;
begin
  Result := ASection.LineNumbering.NumberingRestartType;
end;

function TdxRowsController.GetFontAscentAndFree(ANewBoxInfo: TdxBoxInfo): Integer;
begin
  Result := ANewBoxInfo.GetFontInfo(PieceTable).GetBaseAscentAndFree(DocumentModel);
end;

function TdxRowsController.GetFontAscentAndFree(ARun: TdxTextRunBase): Integer;
begin
  Result := DocumentModel.FontCache[ARun.FontCacheIndex].GetBaseAscentAndFree(DocumentModel);
end;

function TdxRowsController.GetFontDescent(ARun: TdxInlinePictureRun): Integer;
begin
  Result := DocumentModel.FontCache[ARun.FontCacheIndex].GetBaseDescent(DocumentModel);
end;

function TdxRowsController.GetFontDescent(ANewBoxInfo: TdxBoxInfo): Integer;
begin
  Result := ANewBoxInfo.GetFontInfo(PieceTable).GetBaseDescent(DocumentModel);
end;

function TdxRowsController.GetLegacyNumberingListBoxSeparatorWidth(ABox: TdxNumberingListBoxWithSeparator;
  ABounds: TRect; AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): Integer;
var
  AStartHorizontalPosition, ALegacySpace, ALegacyIndent, ATextRight: Integer;
begin
  AStartHorizontalPosition := CurrentHorizontalPosition;
  HorizontalPositionController.SetCurrentHorizontalPosition(ABounds.Right);
  ALegacySpace := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(AProperties.LegacySpace);
  ALegacyIndent := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(AProperties.LegacyIndent);
  ATextRight := Max(AStartHorizontalPosition + ALegacyIndent, ABounds.Right + ALegacySpace);
  if FParagraph.FirstLineIndentType = TdxParagraphFirstLineIndent.Hanging then
    ATextRight := Max(ATextRight, DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(FParagraph.FirstLineIndent) + AStartHorizontalPosition);
  Result := Math.Max(ATextRight - CurrentHorizontalPosition, 0);
end;

function TdxRowsController.GetNextTab(AFormatter: TdxParagraphBoxFormatter): TdxTabInfo;
var
  APosition, ARoundingFix, ALayoutTabPosition: Integer;
  ATabInfo: TdxTabInfo;
begin
  Result := nil;
  ExpandLastTab(AFormatter);
  APosition := DocumentModel.ToDocumentLayoutUnitConverter.ToModelUnits(CurrentHorizontalPosition - CurrentColumn.Bounds.Left);
  ARoundingFix := 0;
  repeat
    ATabInfo := FTabsController.GetNextTab(ARoundingFix + APosition);
    ALayoutTabPosition := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(ATabInfo.Position);
    if ALayoutTabPosition + CurrentColumn.Bounds.Left > CurrentHorizontalPosition then
    begin
      Result := ATabInfo;
      Break;
    end
    else
      Inc(ARoundingFix);
  until False;
end;

function TdxRowsController.GetNumberingListBoxSeparatorWidth(ABox: TdxNumberingListBoxWithSeparator;
  ABounds: TRect; AProperties: TdxListLevelProperties; AFormatter: TdxParagraphBoxFormatter): Integer;
var
  ATabInfo, AActualTabInfo: TdxTabInfo;
  ATabRight, ATabLeft: Integer;
begin
  HorizontalPositionController.SetCurrentHorizontalPosition(ABounds.Right);
  ATabInfo := GetNextTab(AFormatter);
  AActualTabInfo := GetActualTabInfo(ATabInfo, AProperties);
  try
    ATabRight := AActualTabInfo.GetLayoutPosition(DocumentModel.ToDocumentLayoutUnitConverter);
    ATabLeft := ABounds.Right - CurrentColumn.Bounds.Left;
    Result := ATabRight - ATabLeft;
    if Result < 0 then
    begin
      ATabRight := ATabInfo.GetLayoutPosition(DocumentModel.ToDocumentLayoutUnitConverter);
      Result := ATabRight - ATabLeft;
    end;
  finally
    AActualTabInfo.Free;
  end;
end;

function TdxRowsController.GetTextAreaForTable: TdxTextArea;
begin
  Result := HorizontalPositionController.GetTextAreaForTable;
end;

function TdxRowsController.GetTopLevelColumnController: IColumnController;
var
  ACurrentController: IColumnController;
begin
  ACurrentController := ColumnController;
  repeat
    if ACurrentController is TdxTableCellColumnController then
      ACurrentController := TdxTableCellColumnController(ACurrentController).Parent
    else
    begin
      Result := ACurrentController;
      Exit;
    end;
  until False;
  Result := nil;
end;

function TdxRowsController.IncreaseLastRowHeight(ADelta: Integer): TdxRow;
var
  R: TRect;
  ARow: TdxRow;
begin
  Assert(ADelta >= 0);
  ARow := CurrentColumn.Rows.Last;
  R := ARow.Bounds;
  R.Bottom := R.Bottom + ADelta; 
  ARow.Bounds := R;
  Result := ARow;
end;

procedure TdxRowsController.InitCurrentRow(AKeepTextAreas: Boolean);
begin
  SuppressHorizontalOverfull := False;
  HorizontalPositionController.OnCurrentRowHeightChanged(AKeepTextAreas);
  HorizontalPositionController.SetCurrentRowInitialHorizontalPosition;
  FMaxAscent := 0;
  FMaxPictureHeight := 0;
  FMaxDescent := 0;
  FCurrentRunIndex := -1;
  CurrentRow.ProcessingFlags := CurrentRow.ProcessingFlags + [TdxRowProcessingFlag.Clear];
  FTabsController.ClearLastTabPosition;
  if FTabsController.SingleDecimalTabInTable then
    AddSingleDecimalTabInTable;
  AssignCurrentRowLineNumber;
end;

function TdxRowsController.IsPositionOutsideRightParagraphBound(APos: Integer): Boolean;
begin
  Result := APos > FParagraphRight;
end;

function TdxRowsController.MoveCurrentRowDownToFitTable(ATableWidth, ATableTop: Integer): Integer;
begin
  Result := HorizontalPositionController.MoveCurrentRowDownToFitTable(ATableWidth, ATableTop);
end;

procedure TdxRowsController.MoveCurrentRowToNextColumnCore;
begin
  ClearRow(False);
  CurrentColumn := ColumnController.GetNextColumn(CurrentColumn, False);
  ApplyCurrentColumnBounds(CurrentColumn.Bounds);
  CurrentRow.Bounds := CalcDefaultRowBounds(CurrentColumn.Bounds.Top);
  if ColumnController.ShouldZeroSpacingBeforeWhenMoveRowToNextColumn then
    CurrentRow.SpacingBefore := 0
  else
    OffsetCurrentRow(CurrentRow.SpacingBefore);
  HorizontalPositionController.OnCurrentRowHeightChanged(false);
  if CurrentRow.NumberingListBox <> nil then
  begin
    CurrentRow.NumberingListBox.Bounds := FCurrentParagraphNumberingListBounds;
    CurrentRow.NumberingListBox := nil;
  end;
end;

procedure TdxRowsController.MoveRowToNextColumn;
begin
  TablesController.BeforeMoveRowToNextColumn;
  MoveCurrentRowToNextColumnCore;
  TablesController.AfterMoveRowToNextColumn;
end;

procedure TdxRowsController.MoveRowToNextPage;
begin
  TablesController.BeforeMoveRowToNextPage;
  ColumnController.ResetToFirstColumn;
  MoveCurrentRowToNextColumnCore;
  TablesController.AfterMoveRowToNextPage;
end;

procedure TdxRowsController.NewTableStarted;
begin
  RaiseTableStarted;
end;

procedure TdxRowsController.ObtainRowIndents;
begin
  case Paragraph.FirstLineIndentType of
    TdxParagraphFirstLineIndent.None:
      begin
        FCurrentRowIndent := 0;
        FRegularRowIndent := 0;
      end;
    TdxParagraphFirstLineIndent.Indented:
      begin
        FCurrentRowIndent := DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(Paragraph.FirstLineIndent);
        FRegularRowIndent := 0;
      end;
    TdxParagraphFirstLineIndent.Hanging:
      begin
        FCurrentRowIndent := -DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(Paragraph.FirstLineIndent);
        FRegularRowIndent := 0;
      end;
  end;
end;

procedure TdxRowsController.OffsetCurrentRow(AOffset: Integer);
var
  R: TRect;
begin
  Assert(AOffset >= 0);
  R := CurrentRow.Bounds;
  OffsetRect(R, 0, AOffset);
  CurrentRow.Bounds := R;
end;

procedure TdxRowsController.OnCellStart;
begin
  FSpacingAfterPrevParagraph := 0;
  FIsSpacingAfterPrevParagraphValid := True;
end;

procedure TdxRowsController.OnDeferredBeginParagraph;
begin
  ObtainRowIndents;
  RecalcRowBounds;
  HorizontalPositionController.OnCurrentRowHeightChanged(False);
  HorizontalPositionController.SetCurrentRowInitialHorizontalPosition;
end;

procedure TdxRowsController.OnPageBreakBeforeParagraph;
begin
  ColumnController.ResetToFirstColumn;
end;

procedure TdxRowsController.OnPageFormattingComplete(ASection: TdxSection; APage: TdxPage);
var
  ASectionIndex: TdxSectionIndex;
begin
  if FSuppressLineNumberingRecalculationForLastPage then
    Exit;
  if ASection.FirstParagraphIndex > FParagraph.Index then
  begin
    ASectionIndex := PieceTable.LookupSectionIndexByParagraphIndex(FParagraph.Index);
    if ASectionIndex >= 0 then
      ASection := DocumentModel.Sections[ASectionIndex];
  end;
  if GetEffectiveLineNumberingRestartType(ASection) = TdxLineNumberingRestart.NewPage then
  begin
    InitialLineNumber := ASection.LineNumbering.StartingLineNumber;
    LineNumberStep := ASection.LineNumbering.Step;
    AssignCurrentRowLineNumber;
  end;
  CreateLineNumberBoxes(ASection, APage);
end;

procedure TdxRowsController.PrepareCurrentRowBounds(AParagraph: TdxParagraph; ABeginFromParagraphStart: Boolean);
begin
  FreeAndNil(FLineSpacingCalculator);
  FLineSpacingCalculator := CreateLineSpacingCalculator;
  FDefaultRowHeight := FLineSpacingCalculator.DefaultRowHeight;
  ObtainRowIndents;
  if not ABeginFromParagraphStart then
    FCurrentRowIndent := FRegularRowIndent;
  FTabsController.BeginParagraph(AParagraph);
  ApplyCurrentColumnBounds(CurrentColumn.Bounds);
  CurrentRow.Bounds := CalcDefaultRowBounds(CurrentRow.Bounds.Top);
end;

procedure TdxRowsController.RaiseBeginNextSectionFormatting;
begin
  if not FOnBeginNextSectionFormatting.Empty then
    FOnBeginNextSectionFormatting.Invoke(Self, nil); 
end;

procedure TdxRowsController.RaiseTableStarted;
begin
  if not FOnTableStarted.Empty then
    FOnTableStarted.Invoke(Self, nil); 
end;

procedure TdxRowsController.RecalcRowBounds;
var
  ABounds: TRect;
begin
  ABounds := CurrentRow.Bounds;
  CurrentRow.Bounds := cxRectBounds(ParagraphLeft + FCurrentRowIndent, ABounds.Top,
    FParagraphRight - ParagraphLeft - FCurrentRowIndent, ABounds.Height);
end;

procedure TdxRowsController.RecreateHorizontalPositionController;
var
  APosition: Integer;
begin
  APosition := FHorizontalPositionController.CurrentHorizontalPosition;
  if FFloatingObjectsLayout.Items.Count > 0 then
  begin
    if FHorizontalPositionController.ClassType <> TdxFloatingObjectsCurrentHorizontalPositionController then
    begin
      FHorizontalPositionController.Free;
      FHorizontalPositionController := TdxFloatingObjectsCurrentHorizontalPositionController.Create(Self, APosition);
      FHorizontalPositionController.OnCurrentRowHeightChanged(False);
    end;
  end
  else
  begin
    if FHorizontalPositionController.ClassType <> TdxCurrentHorizontalPositionController then
    begin
      FHorizontalPositionController.Free;
      FHorizontalPositionController := CreateCurrentHorizontalPosition(APosition);
      FHorizontalPositionController.OnCurrentRowHeightChanged(False);
    end;
  end;
end;

procedure TdxRowsController.RemoveLastTextBoxes;
var
  ABoxes: TdxBoxCollection;
  ACount: Integer;
begin
  ABoxes := CurrentRow.Boxes;
  ABoxes.DeleteRange(FStartWordBoxIndex, ABoxes.Count - FStartWordBoxIndex);
  CurrentRow.Height := FHeightBeforeWord;
  FMaxAscent := FMaxAscentBeforeWord;
  FMaxDescent := FMaxDescentBeforeWord;
  FMaxPictureHeight := FMaxPictureHeightBeforeWord;
  FCurrentRunIndex := FRunIndexBeforeWord;
  CurrentRow.ProcessingFlags := FRowProcessingFlagsBeforeWord;
  ACount := ABoxes.Count;
  if ACount > 0 then
    HorizontalPositionController.RollbackCurrentHorizontalPositionTo(ABoxes[ACount - 1].Bounds.Right)
  else
    if CurrentRow.NumberingListBox = nil then
      HorizontalPositionController.SetCurrentRowInitialHorizontalPosition
    else
      HorizontalPositionController.RollbackCurrentHorizontalPositionTo(CurrentRow.NumberingListBox.Bounds.Right);
  FTabsController.UpdateLastTabPosition(ACount);
end;

procedure TdxRowsController.Reset(ASection: TdxSection; AKeepFloatingObjects: Boolean);
begin
  FCurrentColumn := nil; 
  FColumnController := GetTopLevelColumnController;
  InitialLineNumber := ASection.LineNumbering.StartingLineNumber;
  LineNumberStep := ASection.LineNumbering.Step;
  BeginSectionFormatting(ASection);
  CurrentColumn := ColumnController.GetNextColumn(nil, AKeepFloatingObjects);
  FreeAndNil(FCurrentRow);
  CurrentRow := ColumnController.CreateRow;
  CurrentRow.Bounds := Rect(0, CurrentColumn.Bounds.Top, 0, CurrentColumn.Bounds.Top);
  RecreateHorizontalPositionController;
  HorizontalPositionController.OnCurrentRowHeightChanged(False);
  FParagraph := PieceTable.Paragraphs.First;
  AssignCurrentRowLineNumber;
  TablesController.Reset;
end;

procedure TdxRowsController.ResizeFloatingObjectBoxToFitText(AFloatingObjectBox: TdxFloatingObjectBox;
  AContent: TdxTextBoxFloatingObjectContent; AAnchorBox: TdxFloatingObjectAnchorBox; ATextBoxContent: TRect;
  AActualSize: Integer);
var
  AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
  AFloatingObjectProperties: TdxFloatingObjectProperties;
  ADeltaHeight, ANewActualHeight: Integer;
  ARectangularObject: IdxRectangularScalableObject;
begin
  AUnitConverter := DocumentModel.ToDocumentLayoutUnitConverter;
  AFloatingObjectProperties := AContent.TextBox.AnchorRun.FloatingObjectProperties;
  ADeltaHeight := AActualSize - ATextBoxContent.Height;
  if ADeltaHeight = 0 then
    Exit;
  AFloatingObjectBox.IncreaseHeight(ADeltaHeight);
  AAnchorBox.IncreaseHeight(ADeltaHeight);
  ANewActualHeight := AFloatingObjectProperties.ActualHeight + AUnitConverter.ToModelUnits(ADeltaHeight);
  if ANewActualHeight <> AContent.OriginalSize.Height then
  begin
    ARectangularObject := AContent.TextBox.AnchorRun;
    AFloatingObjectProperties.DisableHistory := True;
    try
      ARectangularObject.ScaleY := 100 * ANewActualHeight / AContent.OriginalSize.Height;
    finally
      AFloatingObjectProperties.DisableHistory := False;
    end;
  end;
end;

procedure TdxRowsController.RestartFormattingFromTheMiddleOfSection(ASection: TdxSection; AColumn: TdxColumn);
begin
  FIsSpacingAfterPrevParagraphValid := False;
  CurrentColumn := AColumn;
  CreateNewCurrentRowAfterRestart(ASection, AColumn);
  RecreateHorizontalPositionController;
end;

procedure TdxRowsController.RestartFormattingFromTheStartOfRowAtCurrentPage(ASection: TdxSection; AColumn: TdxColumn);
begin
  FIsSpacingAfterPrevParagraphValid := False;
  if not (CurrentColumn is TdxTableCellColumn) then
  begin
    if AColumn = nil then
      AColumn := ColumnController.GetNextColumn(AColumn, True);
    CurrentColumn := AColumn;
  end;
  CreateNewCurrentRowAfterRestart(ASection, CurrentColumn);
end;

procedure TdxRowsController.RestartFormattingFromTheStartOfSection(ASection: TdxSection; AColumn: TdxColumn);
begin
  CurrentColumn := AColumn;
  CreateNewCurrentRowAfterRestart(ASection, AColumn);
  ApplySectionStart(ASection);
  FSuppressLineNumberingRecalculationForLastPage := True;
  try
    MoveRowToNextColumn;
  finally
    FSuppressLineNumberingRecalculationForLastPage := False;
  end;
end;

procedure TdxRowsController.SetColumnController(ANewColumnController: IColumnController);
var
  ANewRow: TdxRow;
begin
  if CurrentRow.Boxes.Count > 0 then
    raise Exception.Create('Error Message'); 
  FColumnController := ANewColumnController;
  ANewRow := ColumnController.CreateRow;
  ANewRow.Bounds := CurrentRow.Bounds;
  ANewRow.LineNumber := CurrentRow.LineNumber;
  CurrentRow := ANewRow;
end;

procedure TdxRowsController.SetCurrentRow(const Value: TdxRow);
begin
  FCurrentRow := Value;
end;

procedure TdxRowsController.SetInitialLineNumber(const Value: Integer);
begin
  FInitialLineNumber := Value;
  LineNumber := Value;
end;

procedure TdxRowsController.SetLineNumberStep(const Value: Integer);
begin
  FLineNumberStep := Max(1, Value);
end;

function TdxRowsController.ShouldAddParagraphFrameBox(AParagraph: TdxParagraph): Boolean;
begin
  Result := not TdxColor.IsTransparentOrEmpty(AParagraph.BackColor); 
end;

function TdxRowsController.ShouldChangeExistingFloatingObjectBounds(AFloatingObjectBox: TdxFloatingObjectBox;
  AAnchorBox: TdxFloatingObjectAnchorBox): Boolean;
var
  AExistingBounds, ANewBounds: TRect;
begin
  AExistingBounds := AFloatingObjectBox.ExtendedBounds;
  ANewBounds := AAnchorBox.ActualBounds;
  Result := not AFloatingObjectBox.LockPosition and (AExistingBounds.Top = ANewBounds.Top) and (ANewBounds.Left > AExistingBounds.Left);
end;

function TdxRowsController.ShouldIgnoreParagraphHeight(AParagraph: TdxParagraph): Boolean;
var
  ADocumentModel: TdxDocumentModel;
begin
  ADocumentModel := AParagraph.DocumentModel;
  Result := ADocumentModel.IsSpecialEmptyParagraphAfterInnerTable(AParagraph, TablesController.GetCurrentCell) and
    not ADocumentModel.IsCursorInParagraph(AParagraph);
end;

function TdxRowsController.ShouldUseMaxDescent: Boolean;
var
  ACount: Integer;
begin
  Result := True;
  ACount := CurrentRow.Boxes.Count;
  if ACount <= 0 then
    Exit;
  if not (CurrentRow.Boxes.First.GetRun(PieceTable) is TdxInlineObjectRun) then
    Exit;
  if ACount = 1 then
  begin
    Result := False;
    Exit;
  end;
  if (ACount = 2) and (CurrentRow.Boxes.Last is TdxParagraphMarkBox) then
  begin
    Result := False;
    Exit;
  end;
end;

procedure TdxRowsController.StartNewWord;
begin
  FStartWordBoxIndex := CurrentRow.Boxes.Count;
  FHeightBeforeWord := CurrentRow.Height;
  FMaxAscentBeforeWord := FMaxAscent;
  FMaxPictureHeightBeforeWord := FMaxPictureHeight;
  FMaxDescentBeforeWord := FMaxDescent;
  FRunIndexBeforeWord := FCurrentRunIndex;
  FRowProcessingFlagsBeforeWord := CurrentRow.ProcessingFlags;
end;

procedure TdxRowsController.TryToRemoveLastTabBox;
var
  ABoxes: TdxBoxCollection;
  AIndex: Integer;
  ATab: TdxTabSpaceBox;
begin
  ABoxes := CurrentRow.Boxes;
  AIndex := ABoxes.Count - 1;
  if ABoxes[AIndex] is TdxTabSpaceBox then
  begin
    ATab := TdxTabSpaceBox(ABoxes[AIndex]);
    HorizontalPositionController.RollbackCurrentHorizontalPositionTo(ATab.Bounds.Left);
    ABoxes.Delete(AIndex);
    FTabsController.UpdateLastTabPosition(AIndex);
  end;
end;

procedure TdxRowsController.UpdateCurrentRowHeight(ABoxInfo: TdxBoxInfo);
var
  ARunIndex: TdxRunIndex;
  APrevRowHeight: Integer;
begin
  ARunIndex := ABoxInfo.StartPos.RunIndex;
  if (ARunIndex = FCurrentRunIndex) and not ABoxInfo.ForceUpdateCurrentRowHeight then
    Exit;
  if not ABoxInfo.ForceUpdateCurrentRowHeight then
    FCurrentRunIndex := ARunIndex;
  UpdateMaxAscentAndDescent(ABoxInfo);
  APrevRowHeight := CurrentRow.Height;
  CurrentRow.Height := FLineSpacingCalculator.CalcRowHeight(CurrentRow.Height, FMaxAscent + FMaxDescent);
  if APrevRowHeight <> CurrentRow.Height then
    HorizontalPositionController.OnCurrentRowHeightChanged(False);
  TablesController.UpdateCurrentCellHeight;
end;

procedure TdxRowsController.UpdateCurrentRowHeightFromFloatingObject(ARun: TdxFloatingObjectAnchorRun;
  AMeasurer: IdxObjectMeasurer);
var
  APictureHeight: Integer;
begin
  FMaxAscentAndFree := Max(FMaxAscentAndFree, GetFontAscentAndFree(ARun));
  APictureHeight := Max(FMaxAscentAndFree, AMeasurer.MeasureFloatingObject(ARun).Height);
  CurrentRow.Height := FLineSpacingCalculator.CalcRowHeight(CurrentRow.Height, APictureHeight);
  TablesController.UpdateCurrentCellHeight;
end;

procedure TdxRowsController.UpdateCurrentRowHeightFromInlineObjectRun(ARun: TdxTextRunBase; ABox: TdxBox;
  AMeasurer: IdxObjectMeasurer);
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := TdxBoxInfo.Create;
  try
    ARun.Measure(ABoxInfo, AMeasurer);
    FMaxPictureHeight := Math.Max(FMaxPictureHeight, ABoxInfo.Size.cy);
    CurrentRow.Height := FLineSpacingCalculator.CalcRowHeightFromInlineObject(CurrentRow.Height, FMaxPictureHeight, FMaxDescent);
    TablesController.UpdateCurrentCellHeight;
  finally
    ABoxInfo.Free;
  end;
end;

procedure TdxRowsController.UpdateCurrentTableCellBottom(AFloatingObjectBox: TdxFloatingObjectBox);
var
  AFloatingObjectProperties: TdxFloatingObjectProperties;
begin
  AFloatingObjectProperties := AFloatingObjectBox.GetFloatingObjectRun.FloatingObjectProperties;
  if AFloatingObjectProperties.TextWrapType <> TdxFloatingObjectTextWrapType.None then
    TablesController.UpdateCurrentCellBottom(AFloatingObjectBox.ExtendedBounds.Bottom);
end;

procedure TdxRowsController.UpdateMaxAscentAndDescent(ABoxInfo: TdxBoxInfo);
begin
  FMaxAscent := Max(FMaxAscent, GetFontAscentAndFree(ABoxInfo));
  FMaxDescent := Max(FMaxDescent, GetFontDescent(ABoxInfo));
end;

{ TdxStateParagraphStart }

function TdxStateParagraphStart.CanFitCurrentRowToColumn(ABoxInfo: TdxBoxInfo): TdxCanFitCurrentRowToColumnResult;
begin
  if TdxRowProcessingFlag.LastInvisibleEmptyCellRowAfterNestedTable in RowsController.CurrentRow.ProcessingFlags then
    Result := TdxCanFitCurrentRowToColumnResult.RowFitted
  else
    Result := inherited CanFitCurrentRowToColumn(ABoxInfo);
end;

function TdxStateParagraphStart.ContinueFormat: TdxStateContinueFormatResult;
begin
  InsertNumberingListBox;
  Result := inherited ContinueFormat;
end;

procedure TdxStateParagraphStart.InsertNumberingListBox;
begin
  if Iterator.Paragraph.IsInList then
  begin
    if CanInsertNumberingListBox then
      AddNumberingListBox
    else
      Formatter.HasDeferredNumberingListBox := True;
  end;
end;

function TdxStateParagraphStart.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.ParagraphStart;
end;

function TdxStateParagraphStart.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.ParagraphStartAfterFloatingObject;
end;

{ TdxStateParagraphStartAfterFloatingObject }

function TdxStateParagraphStartAfterFloatingObject.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.ParagraphStartAfterFloatingObject;
end;

{ TdxStateParagraphStartFromTheMiddle }

function TdxStateParagraphStartFromTheMiddle.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.ParagraphStartFromTheMiddle;
end;

procedure TdxStateParagraphStartFromTheMiddle.InsertNumberingListBox;
begin
end;

function TdxStateParagraphStartFromTheMiddle.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := &Type;
end;

{ TdxStateParagraphStartFromTheMiddleAfterFloatingObject }

function TdxStateParagraphStartFromTheMiddleAfterFloatingObject.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.ParagraphStartFromTheMiddleAfterFloatingObject;
end;

function TdxStateParagraphStartFromTheMiddleAfterFloatingObject.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := &Type;
end;

{ TdxStateRowWithTextOnlyAfterFirstLeadingTab }

function TdxStateRowWithTextOnlyAfterFirstLeadingTab.GetHyphenationState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFirstLeadingTab;
end;

function TdxStateRowWithTextOnlyAfterFirstLeadingTab.GetNoHyphenationNextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplitAfterFirstLeadingTab;
end;

function TdxStateRowWithTextOnlyAfterFirstLeadingTab.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab;
end;

function TdxStateRowWithTextOnlyAfterFirstLeadingTab.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTabAfterFloatingObject;
end;

{ TdxStateRowTabBase }

procedure TdxStateRowTabBase.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Formatter.ApproveFloatingObjects;
  ABoxInfo.Size := cxSize(0, ABoxInfo.Size.cy);
  RowsController.AddTabBox(CurrentTab, ABoxInfo);
  TdxTabsController.CacheLeader(TdxTabSpaceBox(ABoxInfo.Box), Formatter);
  SwitchToNextState;
end;

function TdxStateRowTabBase.CanUseBox: Boolean;
begin
  Result := True;
end;

procedure TdxStateRowTabBase.ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult);
begin
  SwitchToNextState;
end;

function TdxStateRowTabBase.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowTabBase.CurrentTabPosition: Integer;
begin
  Result := CurrentTab.GetLayoutPosition(Formatter.DocumentModel.ToDocumentLayoutUnitConverter);
end;

function TdxStateRowTabBase.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

procedure TdxStateRowTabBase.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Formatter.Measurer.MeasureTab(ABoxInfo);
end;

{ TdxStateRowTab }

function TdxStateRowTab.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  CurrentTab := RowsController.GetNextTab(Formatter);
  if not CurrentTab.IsDefault then
    Result := TdxAddBoxResult.Success
  else
    if not RowsController.SuppressHorizontalOverfull and
      RowsController.IsPositionOutsideRightParagraphBound(CurrentTabPosition + RowsController.CurrentColumn.Bounds.Left) then
      Result := TdxAddBoxResult.HorizontalIntersect
    else
      Result := TdxAddBoxResult.Success;
end;

function TdxStateRowTab.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := FinalizeColumn(ABoxInfo);
end;

function TdxStateRowTab.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  FinalizeLine(ABoxInfo);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowTab.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := FinalizePage(ABoxInfo);
end;

function TdxStateRowTab.FinishParagraph: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateParagraphMarkBoxInfo;
  try
    ApplyParagraphMarkSize(ABoxInfo);
    FinalizeParagraph(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowTab.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    ApplyParagraphMarkSize(ABoxInfo);
    FinalizeSection(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowTab.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTab;
end;

function TdxStateRowTab.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTabAfterFloatingObject;
end;

function TdxStateRowTab.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Formatter.RollbackToStartOfWord;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  Result := TdxCompleteFormattingResult.Success;
end;

procedure TdxStateRowTab.SwitchToNextState;
var
  ANextState: TdxParagraphBoxFormatterState;
begin
  case Iterator.CurrentChar of
    TdxCharacters.Space,
    TdxCharacters.EmSpace,
    TdxCharacters.EnSpace:
      ANextState := TdxParagraphBoxFormatterState.RowSpaces;
    TdxCharacters.TabMark:
      begin
        Formatter.StartNewTab;
        Exit;
      end;
    TdxCharacters.LineBreak:
      ANextState := TdxParagraphBoxFormatterState.RowLineBreak;
    TdxCharacters.PageBreak:
      if RowsController.TablesController.IsInsideTable then
        ANextState := TdxParagraphBoxFormatterState.RowSpaces
      else
        if not RowsController.SupportsColumnAndPageBreaks then
          ANextState := TdxParagraphBoxFormatterState.RowLineBreak
        else
          ANextState := TdxParagraphBoxFormatterState.RowPageBreak;
    TdxCharacters.ColumnBreak:
      if RowsController.TablesController.IsInsideTable then
        ANextState := TdxParagraphBoxFormatterState.RowSpaces
      else
        if not RowsController.SupportsColumnAndPageBreaks then
          ANextState := TdxParagraphBoxFormatterState.RowLineBreak
        else
          ANextState := TdxParagraphBoxFormatterState.RowColumnBreak;
    TdxCharacters.ParagraphMark:
      if Iterator.IsParagraphMarkRun then
        Exit
      else
        ANextState := TdxParagraphBoxFormatterState.RowText;
    TdxCharacters.SectionMark:
      if Iterator.IsParagraphMarkRun then
        Exit
      else
        ANextState := TdxParagraphBoxFormatterState.RowText;
    TdxCharacters.FloatingObjectMark:
      if Iterator.IsFloatingObjectAnchorRun then
        ANextState := TdxParagraphBoxFormatterState.FloatingObject
      else
        ANextState := TdxParagraphBoxFormatterState.RowText;
  else
    ANextState := TdxParagraphBoxFormatterState.RowText;
  end;
  ChangeState(ANextState);
end;

{ TdxStateRowLeadingTab }

procedure TdxStateRowLeadingTab.ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert((CurrentRow.NumberingListBox <> nil) or (CurrentRow.NumberingListBox = nil) and (CurrentRow.Height = RowsController.DefaultRowHeight));
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowLeadingTab.ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert((CurrentRow.NumberingListBox <> nil) or (CurrentRow.NumberingListBox = nil) and (CurrentRow.Height = RowsController.DefaultRowHeight));
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowLeadingTab.ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert((CurrentRow.NumberingListBox <> nil) or (CurrentRow.NumberingListBox = nil) and (CurrentRow.Height = RowsController.DefaultRowHeight));
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowLeadingTab.ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert((CurrentRow.NumberingListBox <> nil) or (CurrentRow.NumberingListBox = nil) and (CurrentRow.Height = RowsController.DefaultRowHeight));
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

function TdxStateRowLeadingTab.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  CurrentTab := RowsController.GetNextTab(Formatter);
  if not RowsController.CanFitBoxToCurrentRow(cxNullSize) then
    Result := TdxAddBoxResult.HorizontalIntersect
  else
    if not CurrentTab.IsDefault then
      Result := TdxAddBoxResult.Success
    else
      if RowsController.IsPositionOutsideRightParagraphBound(CurrentTabPosition + RowsController.CurrentColumn.Bounds.Left) then
        Result := TdxAddBoxResult.HorizontalIntersect
      else
        Result := TdxAddBoxResult.Success;
end;

function TdxStateRowLeadingTab.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
    Result := FinalizeColumn(ABoxInfo)
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowLeadingTab.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    FinalizeLine(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowLeadingTab.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
    Result := FinalizePage(ABoxInfo)
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowLeadingTab.FinishParagraph: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateParagraphMarkBoxInfo;
  try
    Result := InternalFinishParagraphCore(ABoxInfo, FinalizeParagraph);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowLeadingTab.InternalFinishParagraphCore(ABoxInfo: TdxBoxInfo; AFinalizeHandler: TFinalizeHandler): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ApplyParagraphMarkSize(ABoxInfo);
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    AFinalizeHandler(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowLeadingTab.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    Result := InternalFinishParagraphCore(ABoxInfo, FinalizeSection);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowLeadingTab.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowLeadingTab;
end;

function TdxStateRowLeadingTab.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowLeadingTabAfterFloatingObject;
end;

function TdxStateRowLeadingTab.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  Formatter.RollbackToStartOfWord;
  SetCurrentRowHeightToLastBoxHeight;
  ACanFit := RowsController.CanFitCurrentRowToColumn;
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    EndRow;
    if RowsController.CanFitCurrentRowToColumn <> TdxCanFitCurrentRowToColumnResult.RowFitted then
    begin
      Result := RowsController.CompleteCurrentColumnFormatting;
      if Result <> TdxCompleteFormattingResult.Success then
        Exit;
      RowsController.MoveRowToNextColumn;
    end;
    ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

procedure TdxStateRowLeadingTab.SwitchToNextState;
var
  ANextState: TdxParagraphBoxFormatterState;
begin
  case Iterator.CurrentChar of
    TdxCharacters.Space,
    TdxCharacters.EmSpace,
    TdxCharacters.EnSpace:
      ANextState := TdxParagraphBoxFormatterState.RowWithSpacesOnly;
    TdxCharacters.TabMark:
      begin
        Formatter.StartNewTab;
        Exit;
      end;
    TdxCharacters.LineBreak:
      ANextState := TdxParagraphBoxFormatterState.RowLineBreak;
    TdxCharacters.PageBreak:
      if RowsController.TablesController.IsInsideTable then
        ANextState := TdxParagraphBoxFormatterState.RowWithSpacesOnly
      else
        if not RowsController.SupportsColumnAndPageBreaks then
          ANextState := TdxParagraphBoxFormatterState.RowLineBreak
        else
          ANextState := TdxParagraphBoxFormatterState.RowPageBreak;
    TdxCharacters.ColumnBreak:
      if RowsController.TablesController.IsInsideTable then
        ANextState := TdxParagraphBoxFormatterState.RowWithSpacesOnly
      else
        if not RowsController.SupportsColumnAndPageBreaks then
          ANextState := TdxParagraphBoxFormatterState.RowLineBreak
        else
          ANextState := TdxParagraphBoxFormatterState.RowColumnBreak;
    TdxCharacters.ParagraphMark:
      if Iterator.IsParagraphMarkRun then
        Exit
      else
        ANextState := TdxParagraphBoxFormatterState.RowText;
    TdxCharacters.SectionMark:
      if Iterator.IsParagraphMarkRun then
        Exit
      else
        ANextState := TdxParagraphBoxFormatterState.RowText;
    TdxCharacters.FloatingObjectMark:
      if Iterator.IsFloatingObjectAnchorRun then
        ANextState := TdxParagraphBoxFormatterState.FloatingObject
      else
        ANextState := TdxParagraphBoxFormatterState.RowText;
  else
    ANextState := TdxParagraphBoxFormatterState.RowText;
  end;
  ChangeState(ANextState);
end;

{ TdxStateRowFirstLeadingTab }

function TdxStateRowFirstLeadingTab.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  CurrentTab := RowsController.GetNextTab(Formatter);
  Result := TdxAddBoxResult.Success;
end;

function TdxStateRowFirstLeadingTab.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowFirstLeadingTab;
end;

function TdxStateRowFirstLeadingTab.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

procedure TdxStateRowFirstLeadingTab.SwitchToNextState;
var
  ANextState: TdxParagraphBoxFormatterState;
begin
  case Iterator.CurrentChar of
    TdxCharacters.Space,
    TdxCharacters.EmSpace,
    TdxCharacters.EnSpace:
      ANextState := TdxParagraphBoxFormatterState.RowWithSpacesOnly;
    TdxCharacters.TabMark:
      ANextState := TdxParagraphBoxFormatterState.RowLeadingTab;
    TdxCharacters.LineBreak:
      ANextState := TdxParagraphBoxFormatterState.RowLineBreak;
    TdxCharacters.Dash,
    TdxCharacters.EmDash,
    TdxCharacters.EnDash:
      ANextState := TdxParagraphBoxFormatterState.RowWithDashOnly;
    TdxCharacters.PageBreak:
      if RowsController.TablesController.IsInsideTable then
        ANextState := TdxParagraphBoxFormatterState.RowWithSpacesOnly
      else
        if not RowsController.SupportsColumnAndPageBreaks then
          ANextState := TdxParagraphBoxFormatterState.RowLineBreak
        else
          ANextState := TdxParagraphBoxFormatterState.RowPageBreak;
    TdxCharacters.ColumnBreak:
      if RowsController.TablesController.IsInsideTable then
        ANextState := TdxParagraphBoxFormatterState.RowWithSpacesOnly
      else
        if not RowsController.SupportsColumnAndPageBreaks then
          ANextState := TdxParagraphBoxFormatterState.RowLineBreak
        else
          ANextState := TdxParagraphBoxFormatterState.RowColumnBreak;
    TdxCharacters.ParagraphMark:
      if Iterator.IsParagraphMarkRun then
        Exit
      else
        ANextState := TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab;
    TdxCharacters.SectionMark:
      if Iterator.IsParagraphMarkRun then
        Exit
      else
        ANextState := TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab;
    TdxCharacters.FloatingObjectMark:
      if Iterator.IsFloatingObjectAnchorRun then
        ANextState := TdxParagraphBoxFormatterState.FloatingObject
      else
        ANextState := TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab;
  else
    ANextState := TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFirstLeadingTab;
  end;
  ChangeState(ANextState);
end;

{ TdxStateRowTextSplitAfterFirstLeadingTab }

function TdxStateRowTextSplitAfterFirstLeadingTab.CompleteRowProcessing: TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn;
  if ACanFit <> TdxCanFitCurrentRowToColumnResult.RowFitted then
    Result := Formatter.RollbackToStartOfRow(ACanFit)
  else
  begin
    EndRow;
    ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
    Result := TdxCompleteFormattingResult.Success;
  end;
end;

function TdxStateRowTextSplitAfterFirstLeadingTab.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplitAfterFirstLeadingTab;
end;

function TdxStateRowTextSplitAfterFirstLeadingTab.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplitAfterFirstLeadingTabAfterFloatingObject
end;

function TdxStateRowTextSplitAfterFirstLeadingTab.HandleUnsuccessfulSplitting: TdxCompleteFormattingResult;
begin
  Formatter.RollbackToStartOfWord;
  SetCurrentRowHeightToLastBoxHeight;
  Result := CompleteRowProcessing;
end;

{ TdxStateRowTextHyphenationFirstSyllable }

function TdxStateRowTextHyphenationFirstSyllable.GetNextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenation;
end;

function TdxStateRowTextHyphenationFirstSyllable.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllable;
end;

function TdxStateRowTextHyphenationFirstSyllable.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFloatingObject;
end;

function TdxStateRowTextHyphenationFirstSyllable.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  Formatter.RollbackToStartOfWord(Formatter.Iterator as TdxParagraphBoxIterator);
  Formatter.RollbackToStartOfWord;
  Formatter.RollbackToLastTab(Formatter.Iterator as TdxParagraphBoxIterator);
  Formatter.RollbackToLastTab;
  RowsController.TryToRemoveLastTabBox;
  if CurrentRow.Height <= 0 then
  begin
    SetCurrentRowHeightToLastBoxHeight;
    ACanFit := RowsController.CanFitCurrentRowToColumn;
    if ACanFit <> TdxCanFitCurrentRowToColumnResult.RowFitted then
    begin
      Formatter.ClearSyllableIterator;
      Exit(Formatter.RollbackToStartOfRow(ACanFit));
    end;
  end;
  EndRow;
  Formatter.ClearSyllableIterator;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  Result := TdxCompleteFormattingResult.Success;
end;

{ TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab }

function TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab.CompleteRowProcessing: TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn;
  if ACanFit <> TdxCanFitCurrentRowToColumnResult.RowFitted then
    Exit(Formatter.RollbackToStartOfRow(ACanFit))
  else
  begin
    EndRow;
    ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  end;
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFirstLeadingTab;
end;

function TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllableAfterFirstLeadingTabAfterFloatingObject;
end;

function TdxStateRowTextHyphenationFirstSyllableAfterFirstLeadingTab.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ASplitBoxResult: TdxSplitBoxResult;
begin
  ASplitBoxResult := SplitBox(ABoxInfo);
  if ASplitBoxResult <> TdxSplitBoxResult.FailedHorizontalOverfull then
  begin
    AddTextBox(ABoxInfo);
    EndRow;
    ChangeState(TdxParagraphBoxFormatterState.RowEmptyHyphenation);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
  begin
    Formatter.ClearSyllableIterator;
    Formatter.RollbackToStartOfWord;
    SetCurrentRowHeightToLastBoxHeight;
    Result := CompleteRowProcessing;
  end;
end;

{ TdxStateFloatingObject }

procedure TdxStateFloatingObject.AddSuccess(ABoxInfo: TdxBoxInfo);
var
  ARun: TdxTextRunBase;
  AFrameProperties: TdxFrameProperties;
  AObjectAnchorRun: TdxFloatingObjectAnchorRun;
begin
  ARun := Formatter.PieceTable.Runs[ABoxInfo.StartPos.RunIndex];
  if ARun is TdxFloatingObjectAnchorRun then
  begin
    AObjectAnchorRun := TdxFloatingObjectAnchorRun(ARun);
    if not AObjectAnchorRun.ExcludeFromLayout then
      if ABoxInfo.Box is TdxFloatingObjectAnchorBox then
        RowsController.AddFloatingObjectToLayout(TdxFloatingObjectAnchorBox(ABoxInfo.Box), ABoxInfo);
  end
  else
  begin
    AFrameProperties := ARun.Paragraph.FrameProperties;
    if AFrameProperties <> nil then
    begin
      NotImplemented();
    end;
  end;
end;

function TdxStateFloatingObject.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBoxCore(ABoxInfo);
end;

function TdxStateFloatingObject.CanUseBox: Boolean;
begin
  Result := True;
end;

function TdxStateFloatingObject.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateFloatingObject.ContinueFormat: TdxStateContinueFormatResult;
begin
  if Iterator.IsFloatingObjectAnchorRun or Iterator.IsParagraphFrame then
    Result := inherited ContinueFormat
  else
  begin
    SwitchToNextState;
    Result := TdxStateContinueFormatResult.Success;
  end;
end;

function TdxStateFloatingObject.FinishParagraphOrSecton;
begin
  Result := PreviousState.FinishParagraphOrSecton;
end;

function TdxStateFloatingObject.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.FloatingObject;
end;

function TdxStateFloatingObject.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateFloatingObject.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := ACh <> TdxCharacters.FloatingObjectMark;
end;

procedure TdxStateFloatingObject.SwitchToNextState;
var
  ANewState: ISupportsChangeStateManually;
begin
  ChangeState(PreviousState.StateAfterFloatingObject);
  if Supports(Formatter.State, ISupportsChangeStateManually, ANewState) then
    ANewState.ChangeStateManuallyIfNeeded(TdxParagraphIteratorResult.RunFinished);
end;

{ TdxStateRowEmpty }

procedure TdxStateRowEmpty.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Assert(False);
end;

function TdxStateRowEmpty.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxStateRowEmpty.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := TdxAddBoxResult.Success;
end;

function TdxStateRowEmpty.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowEmpty.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmpty;
end;

function TdxStateRowEmpty.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmptyAfterFloatingObject;
end;

function TdxStateRowEmpty.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowEmpty.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := False;
end;

{ TdxStateRowEmptyBase }

procedure TdxStateRowEmptyBase.ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert((CurrentRow.NumberingListBox <> nil) or (CurrentRow.Height = RowsController.DefaultRowHeight));
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowEmptyBase.ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert((CurrentRow.NumberingListBox <> nil) or (CurrentRow.Height = RowsController.DefaultRowHeight));
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowEmptyBase.ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert((CurrentRow.NumberingListBox <> nil) or (CurrentRow.Height = RowsController.DefaultRowHeight));
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowEmptyBase.ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo);
begin
  if not (TdxRowProcessingFlag.LastInvisibleEmptyCellRowAfterNestedTable in CurrentRow.ProcessingFlags) then 
    RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

function TdxStateRowEmptyBase.CanFitCurrentRowToColumn(ABoxInfo: TdxBoxInfo): TdxCanFitCurrentRowToColumnResult;
begin
  Result := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
end;

function TdxStateRowEmptyBase.ContinueFormat: TdxStateContinueFormatResult;
var
  ACh: Char;
begin
  ACh := Iterator.CurrentChar;
  if ACh = TdxCharacters.ParagraphMark then
  begin
    if Iterator.IsParagraphMarkRun then
      Exit(GetContinueFormatResult(FinishParagraph))
    else
      ChangeState(TdxParagraphBoxFormatterState.RowWithTextOnly);
  end
  else
    if ACh = TdxCharacters.SectionMark then
    begin
      if Iterator.IsParagraphMarkRun then
        Exit(GetContinueFormatResult(FinishSection))
      else
        ChangeState(TdxParagraphBoxFormatterState.RowWithTextOnly);
    end
    else
      if TdxCharacters.IsCharSpace(ACh) then
        ChangeState(TdxParagraphBoxFormatterState.RowWithSpacesOnly)
      else
        if TdxCharacters.IsCharDash(ACh) then
          ChangeState(TdxParagraphBoxFormatterState.RowWithDashOnly)
        else
          if ACh = TdxCharacters.TabMark then
            ChangeState(TdxParagraphBoxFormatterState.RowFirstLeadingTab)
          else
            if ACh = TdxCharacters.LineBreak then
              ChangeState(TdxParagraphBoxFormatterState.RowLineBreak)
            else
              if ACh = TdxCharacters.PageBreak then
              begin
                if RowsController.TablesController.IsInsideTable then
                  ChangeState(TdxParagraphBoxFormatterState.RowWithSpacesOnly)
                else
                  if not RowsController.SupportsColumnAndPageBreaks then
                    ChangeState(TdxParagraphBoxFormatterState.RowLineBreak)
                  else
                    ChangeState(TdxParagraphBoxFormatterState.RowPageBreak);
              end
              else
                if ACh = TdxCharacters.ColumnBreak then
                begin
                  if RowsController.TablesController.IsInsideTable then
                      ChangeState(TdxParagraphBoxFormatterState.RowWithSpacesOnly)
                  else
                    if not RowsController.SupportsColumnAndPageBreaks then
                      ChangeState(TdxParagraphBoxFormatterState.RowLineBreak)
                    else
                      ChangeState(TdxParagraphBoxFormatterState.RowColumnBreak);
                end
                else
                  if ACh = TdxCharacters.FloatingObjectMark then
                  begin
                    if Iterator.IsFloatingObjectAnchorRun then
                      ChangeState(TdxParagraphBoxFormatterState.FloatingObject)
                    else
                      ChangeState(TdxParagraphBoxFormatterState.RowWithTextOnly);
                  end
                  else
                    ChangeState(TdxParagraphBoxFormatterState.RowWithTextOnly);
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxStateRowEmptyBase.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
    Result := FinalizeColumn(ABoxInfo)
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowEmptyBase.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    FinalizeLine(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowEmptyBase.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
    Result := FinalizePage(ABoxInfo)
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowEmptyBase.FinishParagraph: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateParagraphMarkBoxInfo;
  try
    Result := InternalFinishParagraphCore(ABoxInfo, FinalizeParagraph);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowEmptyBase.InternalFinishParagraphCore(ABoxInfo: TdxBoxInfo; AFinalizeHandler: TFinalizeHandler): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ApplyParagraphMarkSize(ABoxInfo);
  ACanFit := CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    AFinalizeHandler(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowEmptyBase.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    Result := InternalFinishParagraphCore(ABoxInfo, FinalizeSection);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

{ TdxStateRowWithTextOnlyBase }

procedure TdxStateRowWithTextOnlyBase.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Formatter.ApproveFloatingObjects;
  AddTextBox(ABoxInfo);
  ChangeStateManuallyIfNeeded(ABoxInfo.IteratorResult);
end;

procedure TdxStateRowWithTextOnlyBase.ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  if CurrentRow.Boxes.Count <= 0 then
    RowsController.UpdateCurrentRowHeight(ABoxInfo)
  else
    inherited ApplyColumnBreakMarkSize(ABoxInfo);
end;

procedure TdxStateRowWithTextOnlyBase.ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  if CurrentRow.Boxes.Count <= 0 then
    RowsController.UpdateCurrentRowHeight(ABoxInfo)
  else
    inherited ApplyLineBreakMarkSize(ABoxInfo);
end;

procedure TdxStateRowWithTextOnlyBase.ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  if CurrentRow.Boxes.Count <= 0 then
    RowsController.UpdateCurrentRowHeight(ABoxInfo)
  else
    inherited ApplyPageBreakMarkSize(ABoxInfo);
end;

procedure TdxStateRowWithTextOnlyBase.ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo);
begin
  if CurrentRow.Boxes.Count <= 0 then
    RowsController.UpdateCurrentRowHeight(ABoxInfo)
  else
    inherited ApplyParagraphMarkSize(ABoxInfo);
end;

procedure TdxStateRowWithTextOnlyBase.ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult);
var
  ACurrentChar: Char;
  ANextState: TdxParagraphBoxFormatterState;
begin
  ACurrentChar := Iterator.CurrentChar;
  if ShouldChangeStateManually(ACurrentChar) then
  begin
    ANextState := CalcNextState(ACurrentChar);
    ChangeState(ANextState);
  end;
end;

function TdxStateRowWithTextOnlyBase.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert((CurrentRow.Boxes.Count <= 0) or (CurrentRow.Height > 0));
  Result := RowsController.CompleteCurrentColumnFormatting;
  if Result <> TdxCompleteFormattingResult.Success then
    Exit;
  if CurrentRow.Boxes.Count <= 0 then
    RowsController.UpdateCurrentRowHeight(ABoxInfo);
  Formatter.RowsController.AddBox(TdxColumnBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  RowsController.MoveRowToNextColumn;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
end;

function TdxStateRowWithTextOnlyBase.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert((CurrentRow.Boxes.Count <= 0) or (CurrentRow.Height > 0));

  if CurrentRow.Boxes.Count <= 0 then
    RowsController.UpdateCurrentRowHeight(ABoxInfo);
  Formatter.RowsController.AddBox(TdxLineBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowWithTextOnlyBase.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert((CurrentRow.Boxes.Count <= 0) or (CurrentRow.Height > 0));

  Result := RowsController.CompleteCurrentColumnFormatting;
  if Result <> TdxCompleteFormattingResult.Success then
    Exit;
  if CurrentRow.Boxes.Count <= 0 then
    RowsController.UpdateCurrentRowHeight(ABoxInfo);
  Formatter.RowsController.AddBox(TdxPageBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  RowsController.MoveRowToNextPage;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
end;

function TdxStateRowWithTextOnlyBase.FinishParagraph: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  Assert((CurrentRow.Boxes.Count <= 0) or (CurrentRow.Height > 0));
  ABoxInfo := CreateParagraphMarkBoxInfo;
  try
    ApplyParagraphMarkSize(ABoxInfo);
    FinalizeParagraph(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowWithTextOnlyBase.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  Assert((CurrentRow.Boxes.Count <= 0) or (CurrentRow.Height > 0));
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    ApplyParagraphMarkSize(ABoxInfo);
    FinalizeSection(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowWithTextOnlyBase.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if TdxCharacters.IsCharSpace(ACurrentCharacter) then
    Exit(TdxParagraphBoxFormatterState.RowSpaces);
  if TdxCharacters.IsCharDash(ACurrentCharacter) then
    Exit(DashAfterTextState);
  if ACurrentCharacter = TdxCharacters.TabMark then
    Exit(TdxParagraphBoxFormatterState.RowTab);
  if ACurrentCharacter = TdxCharacters.LineBreak then
    Exit(TdxParagraphBoxFormatterState.RowLineBreak);
  if ACurrentCharacter = TdxCharacters.PageBreak then
  begin
    if RowsController.TablesController.IsInsideTable then
      Exit(TdxParagraphBoxFormatterState.RowSpaces)
    else
      if not RowsController.SupportsColumnAndPageBreaks then
        Exit(TdxParagraphBoxFormatterState.RowLineBreak)
      else
        Exit(TdxParagraphBoxFormatterState.RowPageBreak);
  end;
  if ACurrentCharacter = TdxCharacters.ColumnBreak then
  begin
    if RowsController.TablesController.IsInsideTable then
      Exit(TdxParagraphBoxFormatterState.RowSpaces)
    else
      if not RowsController.SupportsColumnAndPageBreaks then
        Exit(TdxParagraphBoxFormatterState.RowLineBreak)
      else
        Exit(TdxParagraphBoxFormatterState.RowColumnBreak);
  end;
  if ACurrentCharacter = TdxCharacters.FloatingObjectMark then
  begin
    if Iterator.IsFloatingObjectAnchorRun() then
      Exit(TdxParagraphBoxFormatterState.FloatingObject)
    else
      Exit(TdxParagraphBoxFormatterState.RowSpaces);
  end;

  Assert(False); 
  Result := TdxParagraphBoxFormatterState.Final;
end;

function TdxStateRowWithTextOnlyBase.CanUseBox: Boolean;
begin
  Result := True;
end;

function TdxStateRowWithTextOnlyBase.DashAfterTextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDashAfterText;
end;

function TdxStateRowWithTextOnlyBase.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := TdxCharacters.IsCharSpace(ACh) or (ACh = TdxCharacters.TabMark) or (ACh = TdxCharacters.LineBreak) or
    (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak) or (ACh = TdxCharacters.FloatingObjectMark) or
    TdxCharacters.IsCharDash(ACh);
end;

function TdxStateRowWithTextOnlyBase.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Formatter.RollbackToStartOfWord;
  if Formatter.SuppressHyphenation then
    ChangeState(NoHyphenationNextState)
  else
    ChangeState(HyphenationState);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowWithTextOnlyBase.ShouldChangeStateManually(ACurrentCharacter: Char): Boolean;
begin
  Result := not Iterator.IsEnd and IsTerminatorChar(ACurrentCharacter);
end;

{ TdxStateRowWithTextOnly }

function TdxStateRowWithTextOnly.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBoxCore(ABoxInfo);
end;

function TdxStateRowWithTextOnly.DashAfterTextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithDashAfterTextOnly;
end;

function TdxStateRowWithTextOnly.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowWithTextOnly.GetHyphenationState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmptyHyphenation;
end;

function TdxStateRowWithTextOnly.GetNoHyphenationNextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplit;
end;

function TdxStateRowWithTextOnly.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithTextOnly;
end;

function TdxStateRowWithTextOnly.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithTextOnlyAfterFloatingObject;
end;

{ TdxStateRowEmptyHyphenation }

procedure TdxStateRowEmptyHyphenation.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Formatter.ApproveFloatingObjects;
  AddTextBox(ABoxInfo);
  if Iterator.CurrentChar = TdxCharacters.Hyphen then
  begin
    Iterator.Next;
    ChangeState(NextState);
  end;
end;

function TdxStateRowEmptyHyphenation.CalcFinalState(ACurrentChar: Char): TdxParagraphBoxFormatterState;
begin
  if FSwitchToRowEmpty then
    Exit(TdxParagraphBoxFormatterState.RowEmpty);
  case ACurrentChar of
    TdxCharacters.Space,
    TdxCharacters.EnSpace,
    TdxCharacters.EmSpace: 
      Exit(TdxParagraphBoxFormatterState.RowSpaces);
    TdxCharacters.TabMark:
      Exit(TdxParagraphBoxFormatterState.RowTab);
    TdxCharacters.LineBreak:
      Exit(TdxParagraphBoxFormatterState.RowLineBreak);
    TdxCharacters.PageBreak:
      begin
        if RowsController.TablesController.IsInsideTable then
          Exit(TdxParagraphBoxFormatterState.RowSpaces)
        else
          if not RowsController.SupportsColumnAndPageBreaks then
            Exit(TdxParagraphBoxFormatterState.RowLineBreak)
          else
            Exit(TdxParagraphBoxFormatterState.RowPageBreak);
      end;
    TdxCharacters.ColumnBreak:
      begin
        if RowsController.TablesController.IsInsideTable then
          Exit(TdxParagraphBoxFormatterState.RowSpaces)
        else
          if not RowsController.SupportsColumnAndPageBreaks then
            Exit(TdxParagraphBoxFormatterState.RowLineBreak)
          else
            Exit(TdxParagraphBoxFormatterState.RowColumnBreak);
      end;
  end;
  Assert(False); 
  Result := TdxParagraphBoxFormatterState.Final;
end;

function TdxStateRowEmptyHyphenation.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmptyHyphenationAfterFloatingObject;
end;

function TdxStateRowEmptyHyphenation.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBoxWithHyphenCore(ABoxInfo);
end;

function TdxStateRowEmptyHyphenation.CanAddBoxWithoutHyphen(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBoxCore(ABoxInfo);
end;

function TdxStateRowEmptyHyphenation.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := Formatter.RollbackToStartOfRowCore(ACanFit, TdxParagraphBoxFormatterState.RowEmptyHyphenation);
end;

function TdxStateRowEmptyHyphenation.FinishParagraph: TdxCompleteFormattingResult;
var
  ACurrentChar: Char;
begin
  if Formatter.Iterator.IsEnd then
  begin
    Assert(CurrentRow.Height > 0);
    if Formatter.Iterator.CurrentChar = TdxCharacters.ParagraphMark then
      Exit(inherited FinishParagraph)
    else
      Exit(inherited FinishSection);
  end
  else
  begin
    ACurrentChar := Formatter.Iterator.CurrentChar;
    ChangeState(CalcFinalState(ACurrentChar));
  end;
  Formatter.ClearSyllableIterator;
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowEmptyHyphenation.GetIterator: TdxParagraphIteratorBase;
begin
  Result := Formatter.SyllableIterator;
end;

function TdxStateRowEmptyHyphenation.GetNextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenation;
end;

function TdxStateRowEmptyHyphenation.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmptyHyphenation;
end;

function TdxStateRowEmptyHyphenation.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ASplitBoxResult: TdxSplitBoxResult;
begin
  Result := TdxCompleteFormattingResult.Success;
  ASplitBoxResult := SplitBox(ABoxInfo);
  if (ASplitBoxResult <> TdxSplitBoxResult.FailedHorizontalOverfull) then
  begin
    Formatter.ApproveFloatingObjects;
    AddTextBox(ABoxInfo);
  end;

  if not IsHyphenationOfWordComplete then
  begin
    EndRow;
    ChangeState(TdxParagraphBoxFormatterState.RowEmptyHyphenation);
    Exit;
  end;

  if Formatter.Iterator.IsEnd then 
    Exit;
  if Iterator.CurrentChar = TdxCharacters.PageBreak then
    Exit;
  if Iterator.CurrentChar = TdxCharacters.ColumnBreak then
    Exit;
  if (Iterator.CurrentChar <> TdxCharacters.LineBreak) or (ASplitBoxResult = TdxSplitBoxResult.SuccessSuppressedHorizontalOverfull) then
  begin
    EndRow;
    FSwitchToRowEmpty := True;
  end;
end;

function TdxStateRowEmptyHyphenation.IsHyphenationOfWordComplete: Boolean;
begin
  Result := Iterator.IsEnd;
end;

function TdxStateRowEmptyHyphenation.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := ACh = TdxCharacters.Hyphen;
end;

{ TdxStateRowEmptyHyphenationAfterFloatingObject }

function TdxStateRowEmptyHyphenationAfterFloatingObject.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmptyHyphenationAfterFloatingObject;
end;

{ TdxStateRowEmptyAfterFloatingObject }

function TdxStateRowEmptyAfterFloatingObject.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmptyAfterFloatingObject;
end;

{ TdxStateRowTextHyphenation }

function TdxStateRowTextHyphenation.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenation;
end;

function TdxStateRowTextHyphenation.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenationAfterFloatingObject;
end;

function TdxStateRowTextHyphenation.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Formatter.RollbackToStartOfWord;
  InsertHyphenBox;
  EndRow;
  ChangeState(NextState);
  Result := TdxCompleteFormattingResult.Success;
end;

procedure TdxStateRowTextHyphenation.InsertHyphenBox;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateHyphenBoxInfo;
  try
    RowsController.AddBox(TdxHyphenBox, ABoxInfo);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

procedure TdxStateRowTextHyphenation.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Formatter.ApproveFloatingObjects;
  AddTextBox(ABoxInfo);
  if Iterator.CurrentChar = TdxCharacters.Hyphen then
  begin
    Iterator.Next;
    Formatter.StartNewWord;
  end;
end;

function TdxStateRowTextHyphenation.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBoxWithHyphenCore(ABoxInfo);
end;

function TdxStateRowTextHyphenation.CanAddBoxWithoutHyphen(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBoxCore(ABoxInfo);
end;

function TdxStateRowTextHyphenation.CreateHyphenBoxInfo: TdxBoxInfo;
begin
  Result := TdxBoxInfo.Create;
  Result.StartPos := Iterator.GetPreviousPosition;
  Result.EndPos := Result.StartPos;
  Result.Size := Formatter.Measurer.MeasureHyphen(Result.StartPos, Result);
end;

function TdxStateRowTextHyphenation.GetNextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowEmptyHyphenation;
end;

{ TdxPageAreaController }

procedure TdxPageAreaController.BeginSectionFormatting(ASection: TdxSection);
begin
  State.BeginSectionFormatting(ASection);
end;

procedure TdxPageAreaController.ClearInvalidatedContent(APos: TdxFormatterPosition);
begin
  State.ClearInvalidatedContent(APos);
end;

function TdxPageAreaController.CompleteCurrentAreaFormatting: TdxCompleteFormattingResult;
begin
  Result := State.CompleteCurrentAreaFormatting;
end;

constructor TdxPageAreaController.Create(APageController: TdxPageController);
begin
  inherited Create;
  FPageController := APageController;
  SwitchToState(CreateDefaultState(0));
end;

destructor TdxPageAreaController.Destroy;
begin
  FreeAndNil(FState);
  inherited Destroy;
end;

function TdxPageAreaController.CreateDefaultState(ACurrentAreaIndex: Integer): TdxPageAreaControllerState;
begin
  Result := TdxDefaultPageAreaControllerState.Create(Self, ACurrentAreaIndex);
end;

function TdxPageAreaController.GetAreas: TdxPageAreaCollection;
begin
  Result := FPageController.Pages.Last.Areas;
end;

procedure TdxPageAreaController.RaiseResetFormattingFromCurrentArea;
var
  AArgs: TdxEventArgs;
begin
  if FOnResetFormattingFromCurrentArea.Empty then
    Exit;
  AArgs := TdxEventArgs.Create;
  try
    FOnResetFormattingFromCurrentArea.Invoke(Self, AArgs);
  finally
    AArgs.Free;
  end;
end;

function TdxPageAreaController.GetCurrentAreaBounds: TRect;
begin
  Result := State.CurrentAreaBounds;
end;

function TdxPageAreaController.GetNextPageArea(AKeepFloatingObjects: Boolean): TdxPageArea;
begin
  Result := State.GetNextPageArea(AKeepFloatingObjects);
end;

procedure TdxPageAreaController.Reset(ASection: TdxSection);
begin
  State.Reset(ASection);
end;

procedure TdxPageAreaController.RestartFormattingFromBeginOfCurrentArea;
begin
  RaiseResetFormattingFromCurrentArea;
end;

procedure TdxPageAreaController.RestartFormattingFromTheMiddleOfSection(ASection: TdxSection;
  ACurrentAreaIndex: Integer);
begin
  State.RestartFormattingFromTheMiddleOfSection(ASection, ACurrentAreaIndex);
end;

procedure TdxPageAreaController.RestartFormattingFromTheStartOfRowAtCurrentPage;
begin
  State.RestartFormattingFromTheStartOfRowAtCurrentPage;
end;

procedure TdxPageAreaController.RestartFormattingFromTheStartOfSection(ASection: TdxSection;
  ACurrentAreaIndex: Integer);
begin
  State.RestartFormattingFromTheStartOfSection(ASection, ACurrentAreaIndex);
end;

procedure TdxPageAreaController.SwitchToState(AState: TdxPageAreaControllerState);
begin
  FreeAndNil(FState);
  FState := AState;
end;

procedure TdxPageAreaController.RemoveLastPageArea;
begin
  Areas.Delete(Areas.Count - 1);
  if Areas.Count = 0 then
    PageController.RemoveLastPage;
end;

{ TdxPageAreaControllerState }

procedure TdxPageAreaControllerState.BeginSectionFormatting(ASection: TdxSection);
begin
  ApplySectionStart(ASection);
  CreateCurrentAreaBounds;
end;

procedure TdxPageAreaControllerState.ClearInvalidatedContent(APos: TdxFormatterPosition);
var
  AAreas: TdxPageAreaCollection;
  AAreaIndex: Integer;
begin
  AAreas := Areas;
  AAreaIndex := AAreas.BinarySearchBoxIndex(APos);
  if AAreaIndex < 0 then
  begin
    AAreaIndex := not AAreaIndex; 
  end;
  if AAreaIndex + 1 < AAreas.Count then
    AAreas.DeleteRange(AAreaIndex + 1, AAreas.Count - AAreaIndex - 1);
end;

constructor TdxPageAreaControllerState.Create(AOwner: TdxPageAreaController);
begin
  inherited Create;
  Assert(AOwner <> nil);
  FOwner := AOwner;
end;

procedure TdxPageAreaControllerState.CreateCurrentAreaBounds;
begin
  FCurrentAreaBounds := CreateCurrentAreaBoundsCore;
end;

function TdxPageAreaControllerState.GetAreas: TdxPageAreaCollection;
begin
  Result := Owner.Areas;
end;

procedure TdxPageAreaControllerState.RestoreCurrentAreaBounds(const AOldBounds: TRect);
begin
  FCurrentAreaBounds := AOldBounds;
end;

function TdxPageAreaControllerState.GetNextPageAreaCore: TdxPageArea;
begin
  Result := TdxPageArea.Create(PageController.DocumentLayout.DocumentModel.MainPieceTable.ContentType, PageController.CurrentSection);
  Result.Bounds := CurrentAreaBounds;
end;

function TdxPageAreaControllerState.GetPageController: TdxPageController;
begin
  Result := Owner.PageController;
end;

procedure TdxPageAreaControllerState.Reset(ASection: TdxSection);
begin
  BeginSectionFormatting(ASection);
end;

procedure TdxPageAreaControllerState.RestartFormattingFromTheMiddleOfSection(ASection: TdxSection;
  ACurrentAreaIndex: Integer);
begin
  Assert(ACurrentAreaIndex >= 0);
  CreateCurrentAreaBounds;
end;

procedure TdxPageAreaControllerState.RestartFormattingFromTheStartOfRowAtCurrentPage;
begin
end;

procedure TdxPageAreaControllerState.RestartFormattingFromTheStartOfSection(ASection: TdxSection;
  ACurrentAreaIndex: Integer);
begin
  Assert(ACurrentAreaIndex >= 0);
  CreateCurrentAreaBounds;
end;

{ TdxPageController }

constructor TdxPageController.Create(ADocumentLayout: TdxDocumentLayout;
  AFloatingObjectsLayout: TdxFloatingObjectsLayout);
begin
  inherited Create;
  Assert(ADocumentLayout <> nil);
  FDocumentLayout := ADocumentLayout;
  FNextPageOrdinalType := TdxSectionStartType.Continuous;
  FPageBoundsCalculator := CreatePageBoundsCalculator;
  if AFloatingObjectsLayout = nil then
    FFloatingObjectsLayout := TdxFloatingObjectsLayout.Create
  else
    FFloatingObjectsLayout := AFloatingObjectsLayout;
end;

destructor TdxPageController.Destroy;
begin
  FreeAndNil(FPageBoundsCalculator);
  FreeAndNil(FFloatingObjectsLayout);
  inherited Destroy;
end;

class constructor TdxPageController.Initialize;
begin
  FFloatingObjectBoxZOrderComparer := TdxFloatingObjectBoxZOrderComparer.Create;
end;

class destructor TdxPageController.Finalize;
begin
  FFloatingObjectBoxZOrderComparer.Free;
end;

procedure TdxPageController.EndTableFormatting;
var
  I, ANewPageCount: Integer;
  ANewPages: array of TdxPage;
begin
  FTableStarted := False;
  ANewPageCount := Pages.Count - FPagesBeforeTable;
  if ANewPageCount <= 0 then
    Exit;
  SetLength(ANewPages, ANewPageCount);                        
  for I := 0 to ANewPageCount - 1 do                          
  begin                                                       
    ANewPages[I] := Pages[FPagesBeforeTable];
    Pages.Extract(ANewPages[I]);
  end;

  for I := 0 to ANewPageCount - 1 do
  begin
    RaisePageFormattingComplete(Pages.Last, True);
    Pages.Add(ANewPages[I]);
  end;
end;

procedure TdxPageController.FinalizePagePrimaryFormatting(APage: TdxPage; ASkipAddingFloatingObjects: Boolean);
begin
  if not ASkipAddingFloatingObjects then
    AppendFloatingObjectsToPage(APage);
  FFirstPageOfSection := False;
end;

function TdxPageController.AppendFloatingObjectsToPage(APage: TdxPage): Boolean;
var
  AMaxRunIndex: TdxRunIndex;
  APageContainsFloatingObjects, APageContainsParagraphFrames: Boolean;
begin
  APageContainsFloatingObjects := (FloatingObjectsLayout.Items.Count > 0) or
    (FloatingObjectsLayout.ForegroundItems.Count > 0) or
    (FloatingObjectsLayout.BackgroundItems.Count > 0);
  APageContainsParagraphFrames := False;
  if not APageContainsFloatingObjects and not APageContainsParagraphFrames then
    Exit(True);


  AMaxRunIndex := IfThen(APage.IsEmpty, MaxInt, APage.GetLastPosition(PieceTable).RunIndex); 
  if ContainsOrphanedItems(FloatingObjectsLayout.Items, AMaxRunIndex) then
    Exit(False);
  if ContainsOrphanedItems(FloatingObjectsLayout.ForegroundItems, AMaxRunIndex) then
    Exit(False);
  if ContainsOrphanedItems(FloatingObjectsLayout.BackgroundItems, AMaxRunIndex) then
    Exit(False);
  AppendFloatingObjects(FloatingObjectsLayout.Items, APage.FloatingObjects, AMaxRunIndex);
  AppendFloatingObjects(FloatingObjectsLayout.ForegroundItems, APage.ForegroundFloatingObjects, AMaxRunIndex);
  AppendFloatingObjects(FloatingObjectsLayout.BackgroundItems, APage.BackgroundFloatingObjects, AMaxRunIndex);
  ClearFloatingObjectsLayout;
  Result := True;
end;

procedure TdxPageController.ApplyExistingFooterAreaBounds(APage: TdxPage);
begin
end;

procedure TdxPageController.ApplyExistingHeaderAreaBounds(APage: TdxPage);
begin
end;

procedure TdxPageController.ApplySectionStart(ASection: TdxSection);
begin
  FNextPageOrdinalType := CalculateNextPageOrdinalType(ASection);
end;

procedure TdxPageController.BeginNextSectionFormatting;
var
  ASection: TdxSection;
begin
  CurrentSectionIndex := CurrentSectionIndex + 1;
  ASection := DocumentLayout.DocumentModel.Sections[CurrentSectionIndex];
  ApplySectionStart(ASection);

  RestartFormattingFromTheMiddleOfSectionCore(ASection);
  FFirstPageOfSection := True;
end;

procedure TdxPageController.BeginTableFormatting;
begin
  if not FTableStarted then
    FPagesBeforeTable := Pages.Count;
  FTableStarted := True;
end;

function TdxPageController.CalculateNextPageOrdinalType(ASection: TdxSection): TdxSectionStartType;
begin
  Result := TdxSectionStartType.Continuous;
  case ASection.GeneralSettings.StartType of
    TdxSectionStartType.OddPage:
      Result := TdxSectionStartType.OddPage;
    TdxSectionStartType.EvenPage:
      Result := TdxSectionStartType.EvenPage;
  end;
end;

function TdxPageController.CalculatePageBounds(ASection: TdxSection): TRect;
begin
  Result := PageBoundsCalculator.CalculatePageBounds(ASection);
end;

function TdxPageController.CalculatePageOrdinal(AFirstPageOfSection: Boolean): TdxCalculatePageOrdinalResult;
var
  APageOrdinal, ANumSkippedPages: Integer;
begin
  if PageCount <= 0 then
  begin
    APageOrdinal := 0;
    ANumSkippedPages := 0;
  end
  else
  begin
    APageOrdinal := Pages.Last.PageOrdinal;
    ANumSkippedPages := Pages.Last.NumSkippedPages;
  end;

  Result := CalculatePageOrdinalCore(APageOrdinal, ANumSkippedPages, AFirstPageOfSection);
end;

function TdxPageController.CalculatePageOrdinalCore(APreviousPageOrdinal, ANumSkippedPages: Integer;
  AFirstPageOfSection: Boolean): TdxCalculatePageOrdinalResult;
var
  APageOrdinal: Integer;
  AIsPageOrdinalOdd: Boolean;
begin
  if AFirstPageOfSection and (CurrentSection.PageNumbering.StartingPageNumber > 0) then
    APageOrdinal := CurrentSection.PageNumbering.StartingPageNumber
  else
    APageOrdinal := APreviousPageOrdinal + 1;

  AIsPageOrdinalOdd := ((APageOrdinal) mod 2) <> 0; 
  case FNextPageOrdinalType of
    TdxSectionStartType.OddPage:
      begin
        if AIsPageOrdinalOdd then
          Result := TdxCalculatePageOrdinalResult.Create(APageOrdinal, ANumSkippedPages)
        else
          Result := TdxCalculatePageOrdinalResult.Create(APageOrdinal + 1, ANumSkippedPages + 1);
      end;
    TdxSectionStartType.EvenPage:
      begin
        if AIsPageOrdinalOdd then
          Result := TdxCalculatePageOrdinalResult.Create(APageOrdinal, ANumSkippedPages)
        else
          Result := TdxCalculatePageOrdinalResult.Create(APageOrdinal + 1, ANumSkippedPages + 1);
      end;
    else
      Result := TdxCalculatePageOrdinalResult.Create(APageOrdinal, ANumSkippedPages);
  end;
end;

procedure TdxPageController.ClearFloatingObjectsLayout;
begin
  FloatingObjectsLayout.Clear;
end;

function TdxPageController.ClearInvalidatedContent(APos: TdxFormatterPosition; AParagraphIndex: TdxParagraphIndex;
  AKeepFloatingObjects: Boolean): TdxClearInvalidatedContentResult;
var
  ALastColumn: TdxColumn;
  APieceTable: TdxPieceTable;
  APageIndex, APageCount: Integer;
  ANewSectionIndex: TdxSectionIndex;
  ASection: TdxSection;
begin
  if not AKeepFloatingObjects then
    FloatingObjectsLayout.ClearFloatingObjects(APos.RunIndex);
  FTableStarted := False;
  APageIndex := Pages.BinarySearchBoxIndex(APos);
  if APageIndex < 0 then
    APageIndex := not APageIndex; 
  if APageIndex > 0 then
  begin
    ALastColumn := Pages[APageIndex - 1].GetLastColumn;
    ALastColumn.RemoveEmptyTableViewInfos;
  end;
  if APageIndex >= Pages.Count then
  begin
    if not Pages.Last.IsEmpty then
      Exit(TdxClearInvalidatedContentResult.NoRestart);
    CorrectLastPageOrdinal(AParagraphIndex, APageIndex);
    Exit(TdxClearInvalidatedContentResult.NoRestart);
  end;
  Assert(APageIndex >= 0);
  if APageIndex + 1 < Pages.Count then
  begin
    APageCount := Pages.Count - APageIndex - 1;
    if APageCount > 0 then
    begin
      Pages.DeleteRange(APageIndex + 1, APageCount); 
      RaisePageCountChanged;
    end;
  end;
  APieceTable := PieceTable;
  Pages[APageIndex].ClearInvalidatedContent(APos.RunIndex, APieceTable);

  ANewSectionIndex := APieceTable.LookupSectionIndexByParagraphIndex(AParagraphIndex);
  if ANewSectionIndex > CurrentSectionIndex then
    Exit(TdxClearInvalidatedContentResult.NoRestart);
  CurrentSectionIndex := ANewSectionIndex;

  ASection := DocumentModel.Sections[CurrentSectionIndex];
  if (ASection.FirstParagraphIndex = AParagraphIndex) and (APos.Offset = 0) and (APos.RunIndex = APieceTable.Paragraphs[AParagraphIndex].FirstRunIndex) then
  begin
    CurrentSectionIndex := CurrentSectionIndex - 1;
    Pages[APageIndex].ClearInvalidatedContent(APos.RunIndex, nil); 
    Exit(TdxClearInvalidatedContentResult.RestartFromTheStartOfSection);
  end
  else
    Result := TdxClearInvalidatedContentResult.Restart;
end;

function TdxPageController.CompleteCurrentPageFormatting: TdxCompleteFormattingResult;
begin
  if (PageCount > 0) and not AppendFloatingObjectsToPage(Pages.Last) then
    Result := TdxCompleteFormattingResult.OrphanedFloatingObjects
  else
    Result := TdxCompleteFormattingResult.Success;
end;

function TdxPageController.ClearInvalidatedContentFromTheStartOfRowAtCurrentPage(APos: TdxFormatterPosition;
  AParagraphIndex: TdxParagraphIndex; AKeepFloatingObjects: Boolean): TdxClearInvalidatedContentResult;
begin
  if FTableStarted then
    Result := TdxClearInvalidatedContentResult.ClearOnlyTableCellRows
  else
    Result := ClearInvalidatedContent(APos, AParagraphIndex, AKeepFloatingObjects);
end;

procedure TdxPageController.CorrectLastPageOrdinal(AParagraphIndex: TdxParagraphIndex; APageIndex: Integer);
var
  APieceTable: TdxPieceTable;
  ALastNonEmptyPage: TdxPage;
  AEmptyPageStartLogPosition: TdxDocumentLogPosition;
  ASectionIndex: TdxSectionIndex;
  ASection: TdxSection;
  APageOrdinal: TdxCalculatePageOrdinalResult;
begin
  APageIndex := Pages.Count - 2; 
  if APageIndex < 0 then
    Exit;

  APieceTable := PieceTable;

  ALastNonEmptyPage := Pages[APageIndex];
  AEmptyPageStartLogPosition := ALastNonEmptyPage.GetLastPosition(APieceTable).LogPosition + 1;
  ASectionIndex := APieceTable.LookupSectionIndexByParagraphIndex(AParagraphIndex);
  ASection := DocumentModel.Sections[ASectionIndex];
  if (APieceTable.Paragraphs[ASection.FirstParagraphIndex].LogPosition = AEmptyPageStartLogPosition) then
  begin
    FNextPageOrdinalType := CalculateNextPageOrdinalType(ASection);
    APageOrdinal := CalculatePageOrdinal(FirstPageOfSection);
    Pages.Last.PageOrdinal := APageOrdinal.PageOrdinal;
    Pages.Last.NumSkippedPages := APageOrdinal.SkippedPageCount;
    FNextPageOrdinalType := TdxSectionStartType.Continuous;
  end;
end;

procedure TdxPageController.RemoveLastPage;
begin
  Pages.Delete(Pages.Count - 1);
end;

procedure TdxPageController.SetPageLastRunIndex(ARunIndex: TdxRunIndex);
begin
  FPageLastRunIndex := ARunIndex;
  FloatingObjectsLayout.ClearFloatingObjects(ARunIndex + 1);
end;

function TdxPageController.GetCurrentSection: TdxSection;
begin
  Result := FCurrentSection;
end;

function TdxPageController.GetNextPage(AKeepFloatingObjects: Boolean): TdxPage;
var
  ANewPage: TdxPage;
  AOriginalFirstPageOfSection: Boolean;
  APageOrdinal: TdxCalculatePageOrdinalResult;
begin
  AOriginalFirstPageOfSection := FFirstPageOfSection;
  if PageCount > 0 then
  begin
    if not FTableStarted then
      RaisePageFormattingComplete(Pages.Last, False)
    else
      AppendFloatingObjectsToPage(Pages.Last);
  end;

  if not AKeepFloatingObjects then
    ClearFloatingObjectsLayout;
  ANewPage := GetNextPageCore;
  ANewPage.PageIndex := PageCount;
  APageOrdinal := CalculatePageOrdinal(AOriginalFirstPageOfSection);
  ANewPage.PageOrdinal := APageOrdinal.PageOrdinal;
  ANewPage.NumSkippedPages := APageOrdinal.SkippedPageCount;
  ANewPage.NumPages := DocumentModel.ExtendedDocumentProperties.Pages;
  FNextPageOrdinalType := TdxSectionStartType.Continuous;
  Pages.Add(ANewPage);
  RaisePageFormattingStarted(ANewPage);
  FCurrentPageClientBounds := ANewPage.ClientBounds;
  FFirstPageOfSection := False;
  FNextSection := False;

  RaisePageCountChanged;
  Result := ANewPage;
end;

function TdxPageController.GetNextPageCore: TdxPage;
begin
  Result := TdxPage.Create;
  Result.Bounds := FPageBounds;
  Result.ClientBounds := FPageClientBounds;
end;

function TdxPageController.GetPages: TdxPageCollection;
begin
  Result := DocumentLayout.Pages;
end;

function TdxPageController.GetPieceTable: TdxPieceTable;
begin
  Result := DocumentModel.MainPieceTable;
end;

procedure TdxPageController.PopulateFloatingObjects(AFloatingObjects: TList<TdxFloatingObjectBox>);
var
  I: Integer;
  AFloatingObject: TdxFloatingObjectBox;
  ARun: TdxFloatingObjectAnchorRun;
begin
  if AFloatingObjects = nil then
    Exit;

  for I := 0 to AFloatingObjects.Count - 1 do
  begin
    AFloatingObject := AFloatingObjects[I];
    ARun := TdxFloatingObjectAnchorRun(AFloatingObject.PieceTable.Runs[AFloatingObject.StartPos.RunIndex]);
    FloatingObjectsLayout.Add(ARun, AFloatingObject);
  end;
end;

procedure TdxPageController.PopulateFloatingObjectsLayout(ALastPage: TdxPage);
begin
  PopulateFloatingObjects(ALastPage.InnerFloatingObjects);
  PopulateFloatingObjects(ALastPage.InnerForegroundFloatingObjects);
  PopulateFloatingObjects(ALastPage.InnerBackgroundFloatingObjects);
end;

function TdxPageController.GetDocumentModel: TdxDocumentModel;
begin
  Result := FDocumentLayout.DocumentModel;
end;

function TdxPageController.IsCurrentPageEven: Boolean;
begin
  if PageCount = 0 then
      Exit(False); 
  Result := Pages.Last.IsEven;
end;

procedure TdxPageController.AppendFloatingObjects(AFrom, ATo: TList<TdxFloatingObjectBox>; AMaxRunIndex: TdxRunIndex);
var
  I: Integer;
  AFloatingObject: TdxFloatingObjectBox;
begin
  ATo.Clear;
  for I := 0 to AFrom.Count - 1 do
  begin
    AFloatingObject := AFrom[I];
    if (AFloatingObject.PieceTable <> PieceTable) or (AFloatingObject.StartPos.RunIndex <= AMaxRunIndex) then
      ATo.Add(AFloatingObject);
  end;
  ATo.Sort(FFloatingObjectBoxZOrderComparer);
end;

procedure TdxPageController.RaisePageCountChanged;
begin
  FOnPageCountChanged.Invoke(Self, nil); 
end;

procedure TdxPageController.RaisePageFormattingComplete(APage: TdxPage; ASkipAddingFloatingObjects: Boolean);
var
  AArgs: TdxPageFormattingCompleteEventArgs;
begin
  if FOnPageFormattingComplete.Empty then
    Exit;
  AArgs := TdxPageFormattingCompleteEventArgs.Create(APage, False, ASkipAddingFloatingObjects);
  try
    FOnPageFormattingComplete.Invoke(Self, AArgs);
  finally
    AArgs.Free;
  end;
end;

procedure TdxPageController.RaisePageFormattingStarted(APage: TdxPage);
var
  AArgs: TdxPageFormattingCompleteEventArgs;
begin
  if FOnPageFormattingStarted.Empty then
    Exit;
  AArgs := TdxPageFormattingCompleteEventArgs.Create(APage, False, False);
  try
    FOnPageFormattingStarted.Invoke(Self, AArgs);
  finally
    AArgs.Free;
  end;
end;

procedure TdxPageController.Reset(AKeepFloatingObjects: Boolean);
begin
  Pages.Clear;             
  FTableStarted := False;
  CurrentSectionIndex := -1; 
  FreeAndNil(FPageBoundsCalculator);
  FPageBoundsCalculator := CreatePageBoundsCalculator;
  BeginNextSectionFormatting;
  if not AKeepFloatingObjects then
    ClearFloatingObjectsLayout;
  RaisePageCountChanged;
end;

procedure TdxPageController.RestartFormattingFromTheMiddleOfSection(ASection: TdxSection);
var
  ALastPage: TdxPage;
begin
  RestartFormattingFromTheMiddleOfSectionCore(ASection);
  ALastPage := Pages.Last;
  if ALastPage <> nil then
  begin
    ApplyExistingHeaderAreaBounds(ALastPage);
    ApplyExistingFooterAreaBounds(ALastPage);
    FCurrentPageClientBounds := ALastPage.ClientBounds;
  end;

  FNextPageOrdinalType := TdxSectionStartType.Continuous;
end;

procedure TdxPageController.RestartFormattingFromTheMiddleOfSectionCore(ASection: TdxSection);
var
  ALastPage: TdxPage;
begin
  FCurrentSection := ASection;
  FPageBounds := CalculatePageBounds(CurrentSection);
  FPageClientBounds := PageBoundsCalculator.CalculatePageClientBounds(CurrentSection);
  FCurrentPageClientBounds := FPageClientBounds;

  if Pages.Count > 0 then
  begin
    ALastPage := Pages.Last;
    if ALastPage.IsEmpty  then
    begin
      PopulateFloatingObjectsLayout(ALastPage);
      ALastPage.ClearFloatingObjects;
    end;
  end;
end;

procedure TdxPageController.RestartFormattingFromTheStartOfRowAtCurrentPage;
begin
end;

procedure TdxPageController.RestartFormattingFromTheStartOfSection;
var
  ASection: TdxSection;
begin
  CurrentSectionIndex := CurrentSectionIndex + 1;
  ASection := DocumentLayout.DocumentModel.Sections[CurrentSectionIndex];

  ApplySectionStart(ASection);
  RestartFormattingFromTheMiddleOfSectionCore(ASection);
  FFirstPageOfSection := True;
end;

function TdxPageController.ContainsOrphanedItems(AItems: TList<TdxFloatingObjectBox>; AMaxRunIndex: TdxRunIndex): Boolean;
var
  I: Integer;
  AFloatingObject: TdxFloatingObjectBox;
begin
  Result := False;
  if AItems.Count = 0 then
    Exit;
  for I := 0 to AItems.Count - 1 do
  begin
    AFloatingObject := AItems[i];
    if (AFloatingObject.PieceTable = PieceTable) and (AFloatingObject.StartPos.RunIndex > AMaxRunIndex) and
      AFloatingObject.WasRestart then
        Exit(True);
  end;
end;

function TdxPageController.GetPageCount: Integer;
begin
  Result := Pages.Count;
end;

{ TdxColumnController }

procedure TdxColumnController.AddColumn(AColumn: TdxColumn);
begin
  Columns.Add(AColumn);
end;

procedure TdxColumnController.ApplySectionStart(ASection: TdxSection);
begin
  case ASection.GeneralSettings.StartType of
    TdxSectionStartType.Continuous:
      FNextColumnIndex := 0;
    TdxSectionStartType.EvenPage, TdxSectionStartType.OddPage, TdxSectionStartType.NextPage:
      FNextColumnIndex := 0;
  end;
end;

procedure TdxColumnController.BeginSectionFormatting(ASection: TdxSection);
begin
  ApplySectionStart(ASection);
  CreateColumnBounds;
end;

function TdxColumnController.CalculateColumnBounds(AColumnIndex: Integer): TRect;
var
  AAreaBounds: TRect;
begin
  Result := CalculateColumnBoundsCore(AColumnIndex);
  AAreaBounds := FPageAreaController.CurrentAreaBounds;
  Result.Top := AAreaBounds.Top;
  Result.Bottom := AAreaBounds.Height;
end;

procedure TdxColumnController.CleanupEmptyBoxes(ALastColumn: TdxColumn);
begin
end;

procedure TdxColumnController.ClearInvalidatedContent(APos: TdxFormatterPosition);
begin
  ClearInvalidatedContentCore(APos, Columns);
end;

procedure TdxColumnController.ClearInvalidatedContentCore(APos: TdxFormatterPosition; AColumns: TdxColumnCollection);
var
  AColumn: TdxColumn;
  I, AColumnIndex, ACount, AParagraphIndex: Integer;
  ATables: TdxTableViewInfoCollection;
  ATableViewInfo: TdxTableViewInfo;
  ATable: TdxTable;
  AParagraph: TdxParagraph;
begin
  AColumnIndex := AColumns.BinarySearchBoxIndex(APos);
  if AColumnIndex < 0 then
  begin
    AColumnIndex := not AColumnIndex;
  end;
  if AColumnIndex < AColumns.Count then
  begin
    AColumn := AColumns[AColumnIndex];
    ATables := AColumn.InnerTables;
    if ATables <> nil then
    begin
      ACount := ATables.Count;
      for I := ACount - 1 downto 0 do
      begin
        ATableViewInfo := ATables[I];
        ATable := ATableViewInfo.Table;
        if ATable.Rows.Count = 0 then
          ATables.Delete(I)
        else
        begin
          AParagraphIndex := ATable.Rows.First.Cells.First.StartParagraphIndex;
          if AParagraphIndex >= ATable.PieceTable.Paragraphs.Count then
            ATables.Delete(I)
          else
          begin
            AParagraph := ATable.PieceTable.Paragraphs[AParagraphIndex];
            if AParagraph.FirstRunIndex >= APos.RunIndex then
              ATables.Delete(I);
          end;
        end;
      end;
    end;
  end;

  if AColumnIndex + 1 < AColumns.Count then
    AColumns.DeleteRange(AColumnIndex + 1, AColumns.Count - AColumnIndex - 1);
end;

constructor TdxColumnController.Create(APageAreaController: TdxPageAreaController);
begin
  inherited Create;
  Assert(APageAreaController <> nil, 'APageAreaController = nil');
  FPageAreaController := APageAreaController;
  FColumnsBounds := TList<TRect>.Create;
end;

destructor TdxColumnController.Destroy;
begin
  FColumnsBounds.Free;
  inherited Destroy;
end;

procedure TdxColumnController.CreateColumnBounds;
var
  ACalculator: TdxColumnsBoundsCalculator;
begin
  ACalculator := CreateColumnBoundsCalculator;
  try
    FreeAndNil(FColumnsBounds);
    FColumnsBounds := ACalculator.Calculate(FPageAreaController.PageController.CurrentSection, FPageAreaController.CurrentAreaBounds);
  finally
    ACalculator.Free;
  end;
end;

function TdxColumnController.CreateColumnBoundsCalculator: TdxColumnsBoundsCalculator;
begin
  Result := TdxColumnsBoundsCalculator.Create(FPageAreaController.PageController.DocumentLayout.DocumentModel.ToDocumentLayoutUnitConverter);
end;

function TdxColumnController.CreateRow: TdxRow;
begin
  Result := TdxRow.Create;
end;

procedure TdxColumnController.AddInnerTable(ATableViewInfo: TdxTableViewInfo);
begin
  Assert(ATableViewInfo.Table.ParentCell = nil);
end;

function TdxColumnController.CompleteCurrentColumnFormatting(AColumn: TdxColumn): TdxCompleteFormattingResult;
begin
  if (FNextColumnIndex > 0) or (AColumn = nil) then
    Result := TdxCompleteFormattingResult.Success
  else
    Result := PageAreaController.CompleteCurrentAreaFormatting;
end;

function TdxColumnController.GetCurrentPageBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
begin
  Result := ACurrentColumn.Bounds;
end;

function TdxColumnController.GetCurrentPageClientBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
begin
  Result := ACurrentColumn.Bounds;
end;

function TdxColumnController.GetColumns: TdxColumnCollection;
begin
  Result := FPageAreaController.Areas.Last.Columns;
end;

function TdxColumnController.GetPageAreaController: TdxPageAreaController;
begin
  Result := FPageAreaController;
end;

function TdxColumnController.GetMeasurer: TdxBoxMeasurer;
begin
  Result := PageAreaController.PageController.DocumentLayout.Measurer;
end;

function TdxColumnController.GetPageLastRunIndex: TdxRunIndex;
begin
  Result := PageAreaController.PageController.PageLastRunIndex;
end;

function TdxColumnController.GetNextColumn(AColumn: TdxColumn; AKeepFloatingObjects: Boolean): TdxColumn;
begin
  if FNextColumnIndex = 0 then
    PageAreaController.GetNextPageArea(AKeepFloatingObjects);

  if ColumnsBounds.Count = 0 then
    CreateColumnBounds;

  Result := GetNextColumnCore(AColumn);
  AddColumn(Result);
  RaiseGenerateNewColumn;
end;

procedure TdxColumnController.RemoveGeneratedColumn(AColumn: TdxColumn);
begin
  Assert(AColumn <> Columns.Last);
  Columns.Delete(Columns.Count - 1);
  if Columns.Count = 0 then
    PageAreaController.RemoveLastPageArea;
end;

function TdxColumnController.GetNextColumnCore(AColumn: TdxColumn): TdxColumn;
var
  ABounds: TRect;
begin
  Result := TdxColumn.Create;
  ABounds := CalculateColumnBounds(FNextColumnIndex);
  Result.Bounds := ABounds;
  FNextColumnIndex := (FNextColumnIndex + 1) mod ColumnsBounds.Count;
end;

function TdxColumnController.GetPreviousColumn(AColumn: TdxColumn): TdxColumn;
var
  AIndex: Integer;
begin
  AIndex := Columns.IndexOf(AColumn);
  if AIndex > 0 then
    Result := Columns[AIndex - 1]
  else
    Result := nil;
end;

function TdxColumnController.GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean;
begin
  Result := True;
end;

procedure TdxColumnController.RaiseGenerateNewColumn;
begin
  FOnGenerateNewColumn.Invoke(Self, nil);
end;

procedure TdxColumnController.Reset(ASection: TdxSection);
begin
  ResetToFirstColumn;
  ColumnsBounds.Clear;
  BeginSectionFormatting(ASection);
end;

procedure TdxColumnController.ResetToFirstColumn;
begin
  FNextColumnIndex := 0;
end;

procedure TdxColumnController.RestartFormattingFromTheMiddleOfSection(ASection: TdxSection;
  ACurrentColumnIndex: Integer);
begin
  CreateColumnBounds;
  FNextColumnIndex := (ACurrentColumnIndex + 1) mod ColumnsBounds.Count;
end;

procedure TdxColumnController.RestartFormattingFromTheStartOfRowAtCurrentPage;
begin
end;

procedure TdxColumnController.RestartFormattingFromTheStartOfSection(ASection: TdxSection;
  ACurrentColumnIndex: Integer);
begin
  CreateColumnBounds;
  FNextColumnIndex := (ACurrentColumnIndex + 1) mod ColumnsBounds.Count;
  ApplySectionStart(ASection);
end;

{ TdxDefaultPageAreaControllerState }

procedure TdxDefaultPageAreaControllerState.ApplyContinuousSectionStart(ASection: TdxSection);
begin
  FNextAreaIndex := 0;
end;

procedure TdxDefaultPageAreaControllerState.ApplySectionStart(ASection: TdxSection);
begin
  case ASection.GeneralSettings.StartType of
    TdxSectionStartType.Continuous:
      ApplyContinuousSectionStart(ASection);
    TdxSectionStartType.EvenPage, TdxSectionStartType.OddPage, TdxSectionStartType.NextPage:
      FNextAreaIndex := 0;
  end;
end;

procedure TdxDefaultPageAreaControllerState.BeginSectionFormatting(ASection: TdxSection);
begin
  FNextAreaIndex := 0;
  inherited BeginSectionFormatting(ASection);
end;

constructor TdxDefaultPageAreaControllerState.Create(AOwner: TdxPageAreaController; ANextAreaIndex: Integer);
begin
  inherited Create(AOwner);
  FNextAreaIndex := ANextAreaIndex;
end;

function TdxDefaultPageAreaControllerState.CompleteCurrentAreaFormatting: TdxCompleteFormattingResult;
var
  ABounds: TRect;
  ANewBoundsHeight: Integer;
begin
  if FNextAreaIndex = 0 then
    Exit(PageController.CompleteCurrentPageFormatting)
  else
  begin
    ABounds := CurrentAreaBounds;
    CreateCurrentAreaBounds;
    ANewBoundsHeight := CurrentAreaBounds.Height;
    RestoreCurrentAreaBounds(ABounds);
    if ANewBoundsHeight <= 0 then
    begin
      Result := PageController.CompleteCurrentPageFormatting;
      if Result <> TdxCompleteFormattingResult.Success then
        Exit(Result);
    end;
  end;
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxDefaultPageAreaControllerState.CreateCurrentAreaBoundsCore: TRect;
var
  ALastAreaBottom: Integer;
begin
  if FNextAreaIndex = 0 then
    Result := PageController.PageClientBounds
  else
  begin
    Assert(Areas.Count > 0);
    Result := PageController.PageClientBounds;
    ALastAreaBottom := Areas[FNextAreaIndex].Bounds.Bottom;
    Result.Height := Result.Bottom - ALastAreaBottom;
    Result := cxRectSetTop(Result, ALastAreaBottom);
  end;
end;

function TdxDefaultPageAreaControllerState.GetNextPageArea(AKeepFloatingObjects: Boolean): TdxPageArea;
begin
  if FNextAreaIndex = 0 then
  begin
    PageController.GetNextPage(AKeepFloatingObjects);
    CreateCurrentAreaBounds;
  end
  else
  begin
    CreateCurrentAreaBounds;
    if CurrentAreaBounds.Bottom - CurrentAreaBounds.Top  <= 0 then
    begin
      FNextAreaIndex := 0;
      PageController.GetNextPage(AKeepFloatingObjects);
      CreateCurrentAreaBounds;
    end;
  end;

  Result := GetNextPageAreaCore;
  Areas.Add(Result);
end;

procedure TdxDefaultPageAreaControllerState.RestartFormattingFromTheMiddleOfSection(ASection: TdxSection;
  ACurrentAreaIndex: Integer);
begin
  Assert(ACurrentAreaIndex >= 0);
  FNextAreaIndex := ACurrentAreaIndex;
  inherited RestartFormattingFromTheMiddleOfSection(ASection, ACurrentAreaIndex);
end;

procedure TdxDefaultPageAreaControllerState.RestartFormattingFromTheStartOfSection(ASection: TdxSection;
  ACurrentAreaIndex: Integer);
begin
  Assert(ACurrentAreaIndex >= 0);
  FNextAreaIndex := ACurrentAreaIndex;
  inherited RestartFormattingFromTheStartOfSection(ASection, ACurrentAreaIndex);
end;

{ TdxResetSecondaryFormattingForPageArgs }

constructor TdxResetSecondaryFormattingForPageArgs.Create(APage: TdxPage; APageIndex: Integer);
begin
  inherited Create;
  FPage := APage;
  FPageIndex := APageIndex;
end;

{ TdxPageFormattingCompleteEventArgs }

constructor TdxPageFormattingCompleteEventArgs.Create(APage: TdxPage; ADocumentFormattingComplete, ASkipAddingFloatingObjects: Boolean);
begin
  inherited Create;
  Assert(APage <> nil, 'page = nil');
  FPage := APage;
  FDocumentFormattingComplete := ADocumentFormattingComplete;
  FSkipAddingFloatingObjects := ASkipAddingFloatingObjects;
end;

{ TdxColumnsBoundsCalculator }

constructor TdxColumnsBoundsCalculator.Create(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter);
begin
  inherited Create;
  Assert(AUnitConverter <> nil);
  FUnitConverter := AUnitConverter;
end;

function TdxColumnsBoundsCalculator.Calculate(ASection: TdxSection; ABounds: TRect): TList<TRect>;
var
  AColumnsSettings: TdxSectionColumns;
  AColumnInfoCollection: TdxColumnInfoCollection;
begin
  Result := TList<TRect>.Create;
  AColumnsSettings := ASection.Columns;
  if AColumnsSettings.EqualWidthColumns then
    PopulateEqualWidthColumnsBounds(Result, ABounds, AColumnsSettings.ColumnCount, FUnitConverter.ToLayoutUnits(AColumnsSettings.Space))
  else
  begin
    AColumnInfoCollection := AColumnsSettings.GetColumns;
    try
      PopulateColumnsBounds(Result, ABounds, AColumnInfoCollection);
    finally
      AColumnInfoCollection.Free;
    end;
  end;
end;

procedure TdxColumnsBoundsCalculator.PopulateColumnsBounds(const ARects: TList<TRect>; const ABounds: TRect; const AColumnInfoCollection: TdxColumnInfoCollection);
var
  I, X, ACount: Integer;
  AColumnBounds: TRect;
begin
  X := ABounds.Left;
  ACount := AColumnInfoCollection.Count;
  for I := 0 to ACount - 1 do
  begin
    AColumnBounds := cxRectSetLeft(ABounds, X);
    AColumnBounds.Width := UnitConverter.ToLayoutUnits(AColumnInfoCollection[I].Width);
    Inc(X, AColumnBounds.Width + UnitConverter.ToLayoutUnits(AColumnInfoCollection[I].Space));
    ARects.Add(AColumnBounds);
  end;
end;

procedure TdxColumnsBoundsCalculator.PopulateEqualWidthColumnsBounds(const AColumnBounds: TList<TRect>; const ABounds: TRect; AColumnCount, ASpaceBetweenColumns: Integer);
var
  R: TRect;
  ARects: TRects;
begin
  Assert(AColumnCount > 0);
  R := ABounds;
  Dec(R.Right, ASpaceBetweenColumns * (AColumnCount - 1)); 
  ARects := TdxRectangleUtils.SplitHorizontally(R, AColumnCount);
  PopulateEqualWidthColumnsBoundsCore(AColumnBounds, ARects, ASpaceBetweenColumns);
end;

procedure TdxColumnsBoundsCalculator.PopulateEqualWidthColumnsBoundsCore(const ARects: TList<TRect>; const AColumnRects: TRects; ASpaceBetweenColumns: Integer);
var
  I: Integer;
  AColumnBounds: TRect;
begin
  Assert(Length(AColumnRects) > 0);
  ARects.Add(AColumnRects[0]);
  for I := Low(AColumnRects) to High(AColumnRects) do
  begin
    AColumnBounds := AColumnRects[I];
    AColumnBounds.Offset(I * ASpaceBetweenColumns, 0);
    ARects.Add(AColumnBounds);
  end;
end;

{ TdxPageBoundsCalculator }

function TdxPageBoundsCalculator.CalculatePageBounds(ASection: TdxSection): TRect;
var
  APageSettings: TdxSectionPage;
  AWidth, AHeight: Integer;
begin
  APageSettings := ASection.Page;
  AWidth := FUnitConverter.ToLayoutUnits(APageSettings.Width);
  AHeight := FUnitConverter.ToLayoutUnits(APageSettings.Height);
  Result := Rect(0, 0, AWidth, AHeight);
end;

function TdxPageBoundsCalculator.CalculatePageClientBounds(ASection: TdxSection): TRect;
var
  APage: TdxSectionPage;
  AMargins: TdxSectionMargins;
begin
  APage := ASection.Page;
  AMargins := ASection.Margins;
  Result := CalculatePageClientBoundsCore(APage.Width, APage.Height, AMargins.Left, Abs(AMargins.Top), AMargins.Right, Abs(AMargins.Bottom));
end;

function TdxPageBoundsCalculator.CalculatePageClientBoundsCore(APageWidth, APageHeight, AMarginLeft, AMarginTop,
  AMarginRight, AMarginBottom: Integer): TRect;
var
  AWidth, AHeight, ALeft, ATop, ARight, ABottom: Integer;
begin
  AWidth := UnitConverter.ToLayoutUnits(APageWidth);
  AHeight := UnitConverter.ToLayoutUnits(APageHeight);
  ALeft := UnitConverter.ToLayoutUnits(AMarginLeft);
  ATop := UnitConverter.ToLayoutUnits(AMarginTop);
  ARight := UnitConverter.ToLayoutUnits(AMarginRight);
  ABottom := UnitConverter.ToLayoutUnits(AMarginBottom);
  Result := Rect(ALeft, ATop, AWidth - ARight, AHeight - ABottom);
end;

constructor TdxPageBoundsCalculator.Create(AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter);
begin
  Assert(AUnitConverter <> nil);
  FUnitConverter := AUnitConverter;
end;

{ TdxCalculatePageOrdinalResult }

constructor TdxCalculatePageOrdinalResult.Create(APageOrdinal, ASkippedPageCount: Integer);
begin
  PageOrdinal := APageOrdinal;
  SkippedPageCount := ASkippedPageCount;
end;

{ TdxTablesController }

constructor TdxTablesController.Create(ARowsController: TdxRowsController);
begin
  inherited Create;
  FRowsController := ARowsController;
  FStates := TdxTablesControllerStateBaseStack.Create;
  FStates.Push(TdxTablesControllerNoTableState.Create(Self));
  FTableBreaks := TDictionary<TdxTable, TdxSortedList<TPair<Integer, Integer>>>.Create;
  FInfinityHeights := TDictionary<Integer, Integer>.Create;
end;

destructor TdxTablesController.Destroy;
var
  I: Integer;
begin
  for I := 0 to FStates.Count - 1 do
    FStates.Pop.Free;
  FreeAndNil(FStates);
  FreeAndNil(FInfinityHeights);
  FreeAndNil(FTableBreaks);
  inherited Destroy;
end;

type
  { TdxBreakComparer }

  TdxBreakComparer = class(TInterfacedObject, IdxComparable<TPair<Integer, Integer>>) 
  private
    FKey: Integer; 
  public
    constructor Create(AKey: Integer); 
    function CompareTo(const AOther: TPair<Integer, Integer>): Integer; 
  end;

  { TdxBreakComparer }

  constructor TdxBreakComparer.Create(AKey: Integer);
  begin
    inherited Create;
    FKey := AKey;
  end;

  function TdxBreakComparer.CompareTo(const AOther: TPair<Integer, Integer>): Integer;
  begin
    Result := AOther.Key - FKey;
  end;

function TdxTablesController.GetTableBreak(ATable: TdxTable; ATableViewInfoIndex: Integer;
  out ABottomBounds: Integer): TdxTableBreakType;
var
  ABreaks: TdxSortedList<TPair<Integer, Integer>>;
  AIndex: Integer;
begin
  ABottomBounds := 0;
  if not FTableBreaks.TryGetValue(ATable, ABreaks) then
    Exit(TdxTableBreakType.NoBreak);
  AIndex := ABreaks.BinarySearch(TdxBreakComparer.Create(ATableViewInfoIndex));
  if AIndex < 0 then
    Result := TdxTableBreakType.NoBreak
  else
  begin
    ABottomBounds := ABreaks[AIndex].Value;
    Result := TdxTableBreakType.NextPage;
  end;
end;

function TdxTablesController.IsInfinityHeight(ATableViewInfoIndex: Integer; out ABottomAnchorIndex: Integer): Boolean;
begin
  Result := FInfinityHeights.TryGetValue(ATableViewInfoIndex, ABottomAnchorIndex);
end;

type
  { TdxBreakPairComparer }

  TdxBreakPairComparer = class(TComparer<TPair<Integer, Integer>>) 
  public
    function Compare(const X, Y: TPair<Integer, Integer>): Integer; override; 
  end;

  function TdxBreakPairComparer.Compare(const X, Y: TPair<Integer, Integer>): Integer;
  begin
    Result := X.Key - Y.Key;
  end;

procedure TdxTablesController.AddTableBreak(ATable: TdxTable; ATableViewInfoIndex, ABottomBounds: Integer);
var
  ABreaks: TdxSortedList<TPair<Integer, Integer>>;
  AKeyIndex: Integer;
  ABreakPairComparer: IComparer<TPair<Integer, Integer>>;
begin
  if not FTableBreaks.TryGetValue(ATable, ABreaks) then
  begin
    ABreakPairComparer := TdxBreakPairComparer.Create;
    ABreaks := TdxSortedList<TPair<Integer, Integer>>.Create(ABreakPairComparer);
    ABreakPairComparer := nil;
    FTableBreaks.Add(ATable, ABreaks);
  end
  else
  begin
    AKeyIndex := ABreaks.Count - 1;
    while (AKeyIndex >= 0) and (ABreaks[AKeyIndex].Key > ATableViewInfoIndex) do
    begin
      ABreaks.Delete(AKeyIndex);
      Dec(AKeyIndex);
    end;
  end;
  ABreaks.Add(TPair<Integer, Integer>.Create(ATableViewInfoIndex, ABottomBounds));
end;

procedure TdxTablesController.AddInfinityTableBreak(ATableViewInfoIndex, ABottomAnchorIndex: Integer);
begin
  FInfinityHeights.Add(ATableViewInfoIndex, ABottomAnchorIndex);
end;

procedure TdxTablesController.RemoveAllTableBreaks;
begin
  FTableBreaks.Clear;
  FInfinityHeights.Clear;
end;

procedure TdxTablesController.BeginParagraph(AParagraph: TdxParagraph);
var
  AParagraphCell: TdxTableCell;
begin
  AParagraphCell := AParagraph.GetCell;
  State.EnsureCurrentCell(AParagraphCell);
end;

procedure TdxTablesController.StartNewTable(ANewCell: TdxTableCell);
var
  ACurrentNestedLevel: Integer;
begin
  ACurrentNestedLevel := States.Count - 1;
  if ANewCell.Table.NestedLevel > ACurrentNestedLevel then
    StartNewTable(ANewCell.Table.ParentCell);

  if ANewCell.Table.NestedLevel > 0 then
    StartInnerTable(ANewCell)
  else
    StartTopLevelTable(ANewCell);
end;

procedure TdxTablesController.StartTopLevelTable(ANewCell: TdxTableCell);
begin
  RowsController.NewTableStarted;
  States.Push(TdxTablesControllerTableState.Create(Self, ANewCell, RowsController.CurrentColumn.Rows.Count = 0));
end;

function TdxTablesController.CanFitRowToColumn(ALastTextRowBottom: TdxLayoutUnit; AColumn: TdxColumn): TdxCanFitCurrentRowToColumnResult;
begin
  Result := State.CanFitRowToColumn(ALastTextRowBottom, AColumn);
end;

procedure TdxTablesController.BeforeMoveRowToNextColumn;
var
  I, ACount: Integer;
begin
  ACount := States.Count;
  for I := ACount - 1 downto 0 do
    States.Items[I].BeforeMoveRowToNextColumn;
end;

procedure TdxTablesController.AfterMoveRowToNextColumn;
var
  I, ACount: Integer;
begin
  ACount := States.Count;
  for I := ACount - 1 downto 0 do
    States.Items[I].AfterMoveRowToNextColumn;
end;

procedure TdxTablesController.AfterMoveRowToNextPage;
begin
end;

procedure TdxTablesController.BeforeMoveRowToNextPage;
begin
end;

procedure TdxTablesController.StartInnerTable(ACell: TdxTableCell);
begin
  FRowsController.NewTableStarted();
  FStates.Push(TdxTablesControllerTableState.Create(Self, ACell, TdxTablesControllerTableState(State).CurrentCellViewInfoEmpty));
end;

procedure TdxTablesController.UpdateCurrentCellBottom(ABottom: TdxLayoutUnit);
var
  I, ACount: Integer;
begin
  ACount := States.Count;
  for I := ACount - 1 downto 0 do
    States.Items[I].UpdateCurrentCellBottom(ABottom);
end;

procedure TdxTablesController.UpdateCurrentCellHeight;
var
  I, ACount: Integer;
begin
  ACount := States.Count;
  for I := ACount - 1 downto 0 do
    States.Items[I].UpdateCurrentCellHeight(RowsController.CurrentRow);
end;

procedure TdxTablesController.ReturnToPrevState;
begin
  States.Pop;
end;

procedure TdxTablesController.LeaveCurrentTable(ANextCell: TdxTableCell);
var
  ATableState: TdxTablesControllerTableState;
begin
  ATableState := TdxTablesControllerTableState(State);
  ATableState.LeaveCurrentTable(False, False);
  ReturnToPrevState;
  if States.Count = 1 then
    RowsController.ColumnController.PageAreaController.PageController.EndTableFormatting;
  State.EnsureCurrentCell(ANextCell);
end;

procedure TdxTablesController.ClearInvalidatedContent;
var
  I: Integer;
begin
  for I := 0 to States.Count - 1 do 
    States.Pop.Free;
  States.Push(TdxTablesControllerNoTableState.Create(Self));
end;

procedure TdxTablesController.Reset;
begin
  ClearInvalidatedContent;
end;

procedure TdxTablesController.EndParagraph(ALastRow: TdxRow);
var
  I: Integer;
begin
  for I := 0 to States.Count - 1 do
    States[I].EndParagraph(ALastRow);
end;

function TdxTablesController.GetIsInsideTable: Boolean;
begin
  Result := States.Count > 1;
end;

function TdxTablesController.GetState: TdxTablesControllerStateBase;
begin
  Result := States.Peek;
end;

procedure TdxTablesController.OnCurrentRowFinished;
var
  I: Integer;
begin
  for I := States.Count - 1 downto 0 do
    States[I].OnCurrentRowFinished;
end;

function TdxTablesController.RollbackToStartOfRowTableOnFirstCellRowColumnOverfull: TdxParagraphIndex;
var
  I, ACount: Integer;
  AFirstTableRowViewInfoInColumn: Boolean;
  AParagraphIndex: TdxParagraphIndex;
begin
  ACount := FStates.Count;
  AFirstTableRowViewInfoInColumn := False;
  if ACount > 1 then
    AFirstTableRowViewInfoInColumn := TdxTablesControllerTableState(State).IsFirstTableRowViewInfoInColumn;
  for I := 0 to ACount - 1 do
  begin
    AParagraphIndex := States.Items[I].RollbackToStartOfRowTableOnFirstCellRowColumnOverfull(AFirstTableRowViewInfoInColumn, I = 0);
    if AParagraphIndex >= 0 then
      Exit(AParagraphIndex);
  end;
  Assert(False);
  Result := 0;
end;

function TdxTablesController.GetCurrentCell: TdxTableCell;
begin
  if FStates.Count <= 1 then
    Result := nil
  else
    Result := TdxTablesControllerTableState(FStates.Peek).GetCurrentCell;
end;

function TdxTablesController.CreateTableViewInfoManager(AParentTableViewInfoManager: TdxTableViewInfoManager;
  ARowsController: TdxRowsController): TdxTableViewInfoManager;
begin
  Result := TdxTableViewInfoManager.Create(AParentTableViewInfoManager, ARowsController);
end;

{ TdxTablesControllerStateBaseStack }

function TdxTablesControllerStateBaseStack.GetItem(Index: Integer): TdxTablesControllerStateBase;
begin
  Result := List[Index]
end;

function TdxTablesControllerStateBaseStack.Peek: TdxTablesControllerStateBase;
begin
  Result := inherited Peek;
end;

function TdxTablesControllerStateBaseStack.Pop: TdxTablesControllerStateBase;
begin
  Result := inherited Pop;
end;

function TdxTablesControllerStateBaseStack.Push(AItem: TdxTablesControllerStateBase): TdxTablesControllerStateBase;
begin
  Result := inherited Push(AItem);
end;

{ TdxTablesControllerStateBase }

constructor TdxTablesControllerStateBase.Create(ATablesController: TdxTablesController);
begin
  inherited Create;
  Assert(ATablesController <> nil);
  FTablesController := ATablesController;
end;

{ TdxTablesControllerNoTableState }

procedure TdxTablesControllerNoTableState.AfterMoveRowToNextColumn;
begin
end;

procedure TdxTablesControllerNoTableState.BeforeMoveRowToNextColumn;
begin
end;

function TdxTablesControllerNoTableState.CanFitRowToColumn(ALastTextRowBottom: Integer;
  AColumn: TdxColumn): TdxCanFitCurrentRowToColumnResult;
var
  AColumnBottom: Integer;
begin
  if TablesController.RowsController.CurrentColumn.Rows.Count <= 0 then
  begin
    Result := TdxCanFitCurrentRowToColumnResult.RowFitted;
    Exit;
  end;
  AColumnBottom := AColumn.Bounds.Bottom;
  if ALastTextRowBottom <= AColumnBottom then
    Result := TdxCanFitCurrentRowToColumnResult.RowFitted
  else
    Result := TdxCanFitCurrentRowToColumnResult.PlainRowNotFitted;
end;

procedure TdxTablesControllerNoTableState.EndParagraph(ALastRow: TdxRow);
begin
end;

procedure TdxTablesControllerNoTableState.EnsureCurrentCell(ACell: TdxTableCell);
begin
  if ACell <> nil then
  begin
    TablesController.RowsController.ColumnController.PageAreaController.PageController.BeginTableFormatting;
    TablesController.StartNewTable(ACell);
  end;
end;

procedure TdxTablesControllerNoTableState.OnCurrentRowFinished;
begin
end;

function TdxTablesControllerNoTableState.RollbackToStartOfRowTableOnFirstCellRowColumnOverfull(
  AFirstTableRowViewInfoInColumn, AInnerMostTable: Boolean): TdxParagraphIndex;
begin
  Result := 0; 
end;

procedure TdxTablesControllerNoTableState.UpdateCurrentCellBottom(ABottom: TdxLayoutUnit);
begin
end;

procedure TdxTablesControllerNoTableState.UpdateCurrentCellHeight(ARow: TdxRow);
begin
end;

{ TdxLayoutGridRectangle }

constructor TdxLayoutGridRectangle.Create(const ABounds: TRect; ARowIndex, AColumnIndex, AColumnSpan: Integer);
begin
  Bounds := ABounds;
  RowIndex := ARowIndex;
  ColumnIndex := AColumnIndex;
  ColumnSpan := AColumnSpan;
end;

{ TdxTableViewInfoManager }

constructor TdxTableViewInfoManager.TTableViewInfoAndStartRowSeparatorIndexComparer.Create(ARowSeparatorIndex: Integer);
begin
  inherited Create;
  FRowSeparatorIndex := ARowSeparatorIndex;
end;

function TdxTableViewInfoManager.TTableViewInfoAndStartRowSeparatorIndexComparer.CompareTo(const Value: TdxTableViewInfo): Integer;
var
  ADiff: Integer;
begin
  ADiff := Value.TopRowIndex - FRowSeparatorIndex;
  if ADiff <> 0 then
    Exit(ADiff);
  if Value.PrevTableViewInfo = nil then
    Result := 0
  else
    Result := Ord(Value.PrevTableViewInfo.BottomRowIndex = FRowSeparatorIndex);
end;

constructor TdxTableViewInfoManager.TTableViewInfoAndEndRowSeparatorIndexComparer.Create(ARowSeparatorIndex: Integer);
begin
  inherited Create;
  FRowSeparatorIndex := ARowSeparatorIndex;
end;

function TdxTableViewInfoManager.TTableViewInfoAndEndRowSeparatorIndexComparer.CompareTo(const Value: TdxTableViewInfo): Integer;
var
  ABottomSeparatorIndex, ATopSeparatorIndex: Integer;
begin
  ABottomSeparatorIndex := Value.BottomRowIndex + 1;
  if ABottomSeparatorIndex < FRowSeparatorIndex then
    Exit(-1);
  ATopSeparatorIndex := Value.TopRowIndex + 1;
  if ATopSeparatorIndex > FRowSeparatorIndex then
    Exit(1);
  if ABottomSeparatorIndex = FRowSeparatorIndex then
  begin
    if (Value.NextTableViewInfo <> nil) and (Value.NextTableViewInfo.TopRowIndex = Value.BottomRowIndex) then
      Result := -1
    else
      Result := 0;
  end
  else
    Result := 0;
end;

{ TdxTableCellLeftComparable }

constructor TdxTableCellLeftComparable.Create(APos: Integer);
begin
  inherited Create;
  FPos := APos;
end;

function TdxTableCellLeftComparable.CompareTo(const ACell: TdxTableCellViewInfo): Integer;
begin
  Result := ACell.Left - FPos;
end;

{ TdxTableViewInfoManager }

constructor TdxTableViewInfoManager.Create(AParentTableViewInfoManager: TdxTableViewInfoManager; ARowsController: TdxRowsController);
begin
  inherited Create;
  FTableViewInfos := TObjectList<TdxTableViewInfo>.Create;
  FCurrentCellBottoms := TList<TdxLayoutUnit>.Create;
  FRowsController := ARowsController;
  FParentTableViewInfoManager := AParentTableViewInfoManager;
end;

destructor TdxTableViewInfoManager.Destroy;
begin
  FreeAndNil(FTableViewInfos);
  FreeAndNil(FCurrentCellBottoms);
  inherited Destroy;
end;

function TdxTableViewInfoManager.StartNewTable(ATable: TdxTable; ACellBounds: TDictionary<TdxTableCell, TdxLayoutGridRectangle>;
  ALeftOffset, ARightCellMargin: TdxLayoutUnit; AFirstContentInParentCell: Boolean;
  ATableTextArea: TdxTextArea; out AMaxRight: TdxLayoutUnit): TdxTableViewInfo;
var
  AColumnWidth, AMaxTableWidth, APercentBaseWidth: Integer;
  ATableViewInfo: TdxTableViewInfo;
  AWidthsCalculator: TdxTableWidthsCalculator;
  ACalculator: TdxTableGridCalculator;
begin
  AColumnWidth := ATableTextArea.Width;
  AMaxTableWidth := AColumnWidth - ALeftOffset + ARightCellMargin;
  APercentBaseWidth := AColumnWidth;
  if (ATable.NestedLevel = 0) and not RowsController.MatchHorizontalTableIndentsToTextEdge then
    Inc(APercentBaseWidth, ARightCellMargin - ALeftOffset);
  AMaxRight := APercentBaseWidth;
  AWidthsCalculator := TdxTableWidthsCalculator.Create(ATable.PieceTable, RowsController.ColumnController.Measurer, AMaxTableWidth);
  try
    ACalculator := RowsController.CreateTableGridCalculator(ATable.DocumentModel, AWidthsCalculator, AMaxTableWidth);
    try
      FTableGrid := ACalculator.CalculateTableGrid(ATable, APercentBaseWidth);
    finally
      ACalculator.Free;
    end;
  finally
    AWidthsCalculator.Free;
  end;
  ATableViewInfo := CreateTableViewInfo(ATable, 0, AFirstContentInParentCell, nil);
  FCurrentTableViewInfoIndex := AddTableViewInfo(ATableViewInfo);
  FTable := ATable;
  Result := CurrentTableViewInfo;
end;

function TdxTableViewInfoManager.GetTableGrid: TdxTableGrid;
begin
  Result := FTableGrid;
end;

function TdxTableViewInfoManager.GetTableViewInfos: TList<TdxTableViewInfo>;
begin
  Result := FTableViewInfos;
end;

procedure TdxTableViewInfoManager.SetRowSeparator(ARowSeparatorIndex: Integer; AAnchor: TdxTableCellVerticalAnchor);
var
  ATableViewInfo: TdxTableViewInfo;
begin
  ATableViewInfo := GetStartTableViewInfoByRowSeparatorIndex(ARowSeparatorIndex);
  SetRowSeparatorCore(ATableViewInfo, ARowSeparatorIndex, AAnchor);
end;

procedure TdxTableViewInfoManager.SetRowSeparatorForCurrentTableViewInfo(ARowSeparatorIndex: Integer; AAnchor: TdxTableCellVerticalAnchor);
var
  AActualRowSeparatorIndex: Integer;
begin
  AActualRowSeparatorIndex := Min(CurrentTableViewInfo.TopRowIndex + CurrentTableViewInfo.Anchors.Count, ARowSeparatorIndex);
  if AActualRowSeparatorIndex - CurrentTableViewInfo.TopRowIndex > CurrentTableViewInfo.Rows.Count then
    Exit; 
  SetRowSeparatorCore(CurrentTableViewInfo, AActualRowSeparatorIndex, AAnchor);
end;

procedure TdxTableViewInfoManager.EnsureTableRowViewInfo(ATableViewInfo: TdxTableViewInfo; ARow: TdxTableRow; ARowIndex: Integer);
begin
  if ARowIndex < ATableViewInfo.TopRowIndex + ATableViewInfo.Rows.Count then
    Exit;
  Assert(ARowIndex = ATableViewInfo.TopRowIndex + ATableViewInfo.Rows.Count);
  ATableViewInfo.Rows.Add(CreateTableRowViewInfo(ATableViewInfo, ARow, ARowIndex));
end;

procedure TdxTableViewInfoManager.LeaveCurrentTable(ABeforeRestart: Boolean);
var
  ATableViewInfosCount: Integer;
  ALastTableViewInfo: TdxTableViewInfo;
begin
  if not ABeforeRestart then
  begin
    ATableViewInfosCount := FTableViewInfos.Count;
    Assert(ATableViewInfosCount > 0);
    ALastTableViewInfo := FTableViewInfos[ATableViewInfosCount - 1];
    if FParentTableViewInfoManager <> nil then
    begin
      FParentTableViewInfoManager.SetCurrentTableCellViewInfo(ALastTableViewInfo.ParentTableCellViewInfo);
      FParentTableViewInfoManager.SetCurrentParentColumn(ALastTableViewInfo.ParentTableCellViewInfo.TableViewInfo);
      FParentTableViewInfoManager.ValidateTopLevelColumn;
    end;
  end;
end;

procedure TdxTableViewInfoManager.RemoveAllInvalidRowsOnColumnOverfull(AFirstInvalidRowIndex: Integer);
var
  I, ALastValidTableViewInfoIndex, AFirstInvalidRowViewInfoIndex: Integer;
  ALastValidTableViewInfo, ATableViewInfo: TdxTableViewInfo;
  AShouldRemoveColumn: Boolean;
begin
  ALastValidTableViewInfoIndex := GetStartTableViewInfoIndexByRowSeparatorIndex(AFirstInvalidRowIndex);
  ALastValidTableViewInfo := FTableViewInfos[ALastValidTableViewInfoIndex];
  AFirstInvalidRowViewInfoIndex := AFirstInvalidRowIndex - ALastValidTableViewInfo.TopRowIndex;
  if AFirstInvalidRowViewInfoIndex = 0 then
    Dec(ALastValidTableViewInfoIndex);
  for I := FTableViewInfos.Count - 1 downto ALastValidTableViewInfoIndex + 1 do
  begin
    ATableViewInfo := FTableViewInfos[I];
    ATableViewInfo.Column.TopLevelColumn.RemoveTableViewInfoWithContent(ATableViewInfo);
    FTableViewInfos.Delete(I);
    FColumnController.RemoveTableViewInfo(ATableViewInfo);
    if ATableViewInfo.ParentTableCellViewInfo <> nil then
      ATableViewInfo.ParentTableCellViewInfo.InnerTables.Remove(ATableViewInfo);
    AShouldRemoveColumn := ShouldRemoveColumn(I, ATableViewInfo);
    if AShouldRemoveColumn then
        FColumnController.RemoveGeneratedColumn(ATableViewInfo.Column);
  end;
  if AFirstInvalidRowViewInfoIndex > 0 then
  begin
      ALastValidTableViewInfo.RemoveRowsFromIndex(AFirstInvalidRowViewInfoIndex);
      ALastValidTableViewInfo.NextTableViewInfo := nil;
  end;
end;

procedure TdxTableViewInfoManager.FixColumnOverflow;
var
  I, AAnchorCount: Integer;
  ALastAnchor: TdxTableCellVerticalAnchor;
  ATableViewInfo: TdxTableViewInfo;
  AVerticalAnchors: TdxTableCellVerticalAnchorCollection;
begin
  if FParentTableViewInfoManager <> nil then
    Exit;
  for I := 0 to FTableViewInfos.Count - 1 do
  begin
    ATableViewInfo := FTableViewInfos[I];
    AVerticalAnchors := ATableViewInfo.Anchors;
    AAnchorCount := ATableViewInfo.Anchors.Count;
    if AAnchorCount = 0 then
        Continue;
    ALastAnchor := AVerticalAnchors[AAnchorCount - 1];
    if ALastAnchor.VerticalPosition > ATableViewInfo.Column.Bounds.Bottom then
      ALastAnchor.VerticalPosition := ATableViewInfo.Column.Bounds.Bottom;
  end;
end;

function TdxTableViewInfoManager.IsCurrentCellViewInfoFirst: Boolean;
begin
  Result := not CurrentTableCellViewInfo.IsStartOnPreviousTableViewInfo;
end;

function TdxTableViewInfoManager.IsFirstTableRowViewInfoInColumn: Boolean;
begin
  if CurrentTableCellViewInfo.TopAnchorIndex > 0 then
    Exit(False);
  if not CurrentTableViewInfo.FirstContentInParentCell then
    Exit(False);
  if FParentTableViewInfoManager = nil then
    Result := True
  else
    Result := FParentTableViewInfoManager.IsFirstTableRowViewInfoInColumn;
end;

procedure TdxTableViewInfoManager.SetCurrentCellHasContent;
begin
  CurrentTableCellViewInfo.EmptyCell := True;
end;

function TdxTableViewInfoManager.GetCurrentTableViewInfoIndex: Integer;
begin
  Result := FCurrentTableViewInfoIndex; 
end;

function TdxTableViewInfoManager.GetCurrentTopLevelTableViewInfoIndex: Integer;
begin
  if FParentTableViewInfoManager <> nil then
    Result := FParentTableViewInfoManager.GetCurrentTopLevelTableViewInfoIndex
  else
    Result := GetCurrentTableViewInfoIndex;
end;

function TdxTableViewInfoManager.IsFirstContentInParentCell: Boolean;
begin
  Result := CurrentTableViewInfo.FirstContentInParentCell;
end;

function TdxTableViewInfoManager.GetParentTableViewInfoManager: TdxTableViewInfoManager;
begin
  Result := FParentTableViewInfoManager;
end;

function TdxTableViewInfoManager.GetRowStartAnchor(ARowIndex: Integer): IdxTableCellVerticalAnchor;
var
  ATableViewInfo: TdxTableViewInfo;
begin
  ATableViewInfo := GetStartTableViewInfoByRowSeparatorIndex(ARowIndex);
  Result := GetRowStartAnchor(ATableViewInfo, ARowIndex);
end;

function TdxTableViewInfoManager.GetRowStartColumn(ARowIndex: Integer): TdxColumn;
var
  ATableViewInfo: TdxTableViewInfo;
begin
  ATableViewInfo := GetStartTableViewInfoByRowSeparatorIndex(ARowIndex);
  Result := ATableViewInfo.Column;
end;

function TdxTableViewInfoManager.GetRowStartTableViewInfo(ATableRow: TdxTableRow): TdxTableViewInfo;
var
  ARowIndex: Integer;
begin
  ARowIndex := ATableRow.Table.Rows.IndexOf(ATableRow);
  Result := GetStartTableViewInfoByRowSeparatorIndex(ARowIndex);
end;

procedure TdxTableViewInfoManager.StartNextCell(ACell: TdxTableCell; const ABounds: TRect; const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect);
var
  AStartTableViewInfoIndex, ALastTableViewInfoIndex, ATableViewInfoIndex: Integer;
begin
  AStartTableViewInfoIndex := GetStartTableViewInfoIndexByRowSeparatorIndex(AGridBounds.RowIndex);
  ALastTableViewInfoIndex := GetEndTableViewInfoIndexByRowSeparatorIndex(AGridBounds.RowIndex + AGridBounds.RowSpan);
  CurrentTableCellViewInfo := AddFirstCellViewInfo(FTableViewInfos[AStartTableViewInfoIndex], ACell, ABounds, AGridBounds, ATextBounds);
  for ATableViewInfoIndex := AStartTableViewInfoIndex + 1 to ALastTableViewInfoIndex - 1 do
    AddMiddleCellViewInfo(FTableViewInfos[ATableViewInfoIndex], ACell, ABounds, AGridBounds, ATextBounds);
  if ALastTableViewInfoIndex > AStartTableViewInfoIndex then
    AddLastCellViewInfo(FTableViewInfos[ALastTableViewInfoIndex], ACell, ABounds, AGridBounds, ATextBounds);
  if AStartTableViewInfoIndex <> FCurrentTableViewInfoIndex then
  begin
    FCurrentTableViewInfoIndex := AStartTableViewInfoIndex;
    if FParentTableViewInfoManager <> nil then
    begin
      Assert(CurrentTableViewInfo.ParentTableCellViewInfo <> nil);
      FParentTableViewInfoManager.SetCurrentTableCellViewInfo(CurrentTableViewInfo.ParentTableCellViewInfo);
    end;
    SetCurrentParentColumn(CurrentTableViewInfo);
  end;
  ValidateTopLevelColumn;

  if (ACell.Row.CellSpacing.&Type = TdxWidthUnitType.ModelUnits) and (ACell.Row.CellSpacing.Value > 0) then
  begin
  end;
end;

function TdxTableViewInfoManager.GetLastTableBottom: Integer;
var
  ALastTableViewInfo: TdxTableViewInfo;
begin
  ALastTableViewInfo := GetLastTableViewInfo;
  Result := ALastTableViewInfo.GetTableBottom;
end;

function TdxTableViewInfoManager.GetBottomCellAnchor(ACurrentCell: TdxTableCell): TdxTableCellVerticalAnchor;
var
  ABottomAnchorIndex: Integer;
begin
  ABottomAnchorIndex := CurrentTableCellViewInfo.BottomAnchorIndex;
  Assert(ACurrentCell = CurrentTableCellViewInfo.Cell);
  Result := CurrentTableViewInfo.Anchors[ABottomAnchorIndex];
end;

function TdxTableViewInfoManager.GetBottomCellRowSeparatorIndex(ACurrentCell: TdxTableCell; ARowSpan: Integer): Integer;
begin
  Result := ACurrentCell.Table.Rows.IndexOf(ACurrentCell.Row) + ARowSpan;
end;

procedure TdxTableViewInfoManager.BeforeMoveRowToNextColumn(ACurrentCellBottom: TdxLayoutUnit);
begin
end;

procedure TdxTableViewInfoManager.AfterMoveRowToNextColumn;
var
  ACurrentCellBottom, ABottomTextIndent: TdxLayoutUnit;
  AColumnController: TdxTableCellColumnController;
  APrevTableViewInfo, ANewTableViewInfo: TdxTableViewInfo;
  ASuitableTableCellViewInfo: TdxTableCellViewInfo;
  ACount, ABottomAnchorIndex, ACurrentCellViewInfoIndex, ARowViewInfoIndex, ANewTableViewInfoIndex, ASplitAnchorIndex: Integer;
  ACells: TdxTableCellViewInfoCollection;
  ASplitAnchor: TdxTableCellVerticalAnchor;
  ABorders: TList<TdxHorizontalCellBordersInfo>;
  ACurrentRow: TdxRow;
  ARowBounds: TRect;
begin
  ACurrentCellBottom := CurrentCellBottom;
  AColumnController := ColumnController;
  if AColumnController.ViewInfo <> nil then
  begin
    APrevTableViewInfo := CurrentTableViewInfo;
    FCurrentTableViewInfoIndex := FTableViewInfos.IndexOf(AColumnController.ViewInfo);
    ASuitableTableCellViewInfo := FindSuitableTableCellViewInfo(CurrentTableViewInfo.Cells, CurrentTableCellViewInfo.Cell);

    if ASuitableTableCellViewInfo <> nil then
    begin
      CurrentTableCellViewInfo := ASuitableTableCellViewInfo;
      ACount := APrevTableViewInfo.Anchors.Count;
      APrevTableViewInfo.Anchors[ACount - 1].VerticalPosition := (Max(APrevTableViewInfo.BottomAnchor.VerticalPosition, ACurrentCellBottom));
    end
    else
    begin
      ABottomAnchorIndex := CurrentTableCellViewInfo.BottomAnchorIndex;

      MoveRowAndAnchors_(APrevTableViewInfo, CurrentTableViewInfo, ABottomAnchorIndex);
      ACells := APrevTableViewInfo.Rows.Last.Cells;
      ACurrentCellViewInfoIndex := TdxAlgorithms.BinarySearch<TdxTableCellViewInfo>(ACells, TdxTableCellLeftComparable.Create(CurrentTableCellViewInfo.Left));

      CurrentTableCellViewInfo := CurrentTableViewInfo.Rows.First.Cells[ACurrentCellViewInfoIndex];
    end
  end
  else
  begin
    ARowViewInfoIndex := CurrentTableCellViewInfo.BottomAnchorIndex - 1;
    ACells := CurrentTableViewInfo.Rows[CurrentTableCellViewInfo.BottomAnchorIndex - 1].Cells;
    ACurrentCellViewInfoIndex := TdxAlgorithms.BinarySearch<TdxTableCellViewInfo>(ACells, TdxTableCellLeftComparable.Create(CurrentTableCellViewInfo.Left));
    Assert(ACurrentCellViewInfoIndex >= 0);
    ANewTableViewInfo := TdxTableViewInfo.Create(FTable, AColumnController.CurrentTopLevelColumn, AColumnController.ParentColumn, CurrentTableViewInfo.VerticalBorderPositions, CurrentTableViewInfo.TopRowIndex + CurrentTableCellViewInfo.BottomAnchorIndex - 1, GetParentTableCellViewInfo, True);
    AColumnController.Parent.AddInnerTable(ANewTableViewInfo);
    ANewTableViewInfo.LeftOffset := CurrentTableViewInfo.LeftOffset;
    ANewTableViewInfo.TextAreaOffset := CurrentTableViewInfo.LeftOffset;
    ANewTableViewInfo.ModelRelativeIndent := CurrentTableViewInfo.ModelRelativeIndent;
    ANewTableViewInfoIndex := AddTableViewInfo(ANewTableViewInfo);
    AColumnController.CurrentTopLevelColumn.Tables.Add(ANewTableViewInfo);
    AColumnController.AddTableViewInfo(ANewTableViewInfo);
    MoveRowAndAnchor(CurrentTableViewInfo, ANewTableViewInfo, ARowViewInfoIndex + 1);
    ASplitAnchorIndex := CurrentTableCellViewInfo.BottomAnchorIndex;
    ASplitAnchor := CurrentTableViewInfo.Anchors[ASplitAnchorIndex - 1];
    ABorders := GetSplitAnchorHorizontalCellBorders(ASplitAnchor); 
    CurrentTableViewInfo.Anchors[ASplitAnchorIndex] := TdxTableCellVerticalAnchor.Create(ACurrentCellBottom, 0, ABorders);
    if ASplitAnchor <> nil then
      ABottomTextIndent := ASplitAnchor.BottomTextIndent
    else
      ABottomTextIndent := 0;
    ANewTableViewInfo.Anchors[0] := TdxTableCellVerticalAnchor.Create(ANewTableViewInfo.Column.Bounds.Top, ABottomTextIndent, ABorders);
    SplitCellsByAncor(CurrentTableViewInfo, ANewTableViewInfo, ASplitAnchorIndex);
    Assert(CurrentTableCellViewInfo.BottomAnchorIndex - ASplitAnchorIndex + 1 = 1);

    Assert(ANewTableViewInfo.Rows.First.Cells[ACurrentCellViewInfoIndex].Cell = CurrentTableCellViewInfo.Cell);
    CurrentTableCellViewInfo := ANewTableViewInfo.Rows.First.Cells[ACurrentCellViewInfoIndex];
    FCurrentTableViewInfoIndex := ANewTableViewInfoIndex;
  end;
  ACurrentRow := RowsController.CurrentRow;
  ARowBounds := ACurrentRow.Bounds;
  ARowBounds.MoveToTop(CurrentTableViewInfo.TopAnchor.VerticalPosition + CurrentTableViewInfo.TopAnchor.BottomTextIndent + ACurrentRow.SpacingBefore);
  RowsController.CurrentRow.Bounds := ARowBounds;
  RowsController.HorizontalPositionController.SetCurrentHorizontalPosition(ARowBounds.Left);

  ValidateTopLevelColumn;
end;

function TdxTableViewInfoManager.GetCurrentCellTop: Integer;
begin
  Result := CurrentTableViewInfo.Anchors[CurrentTableCellViewInfo.TopAnchorIndex].VerticalPosition;
end;

function TdxTableViewInfoManager.GetCurrentCellTopAnchorIndex: Integer;
begin
  Result := CurrentTableCellViewInfo.TopAnchorIndex;
end;

function TdxTableViewInfoManager.GetCurrentCellBottomAnchorIndex: Integer;
begin
  Result := CurrentTableCellViewInfo.BottomAnchorIndex;
end;

procedure TdxTableViewInfoManager.SetColumnController(Value: TdxTableCellColumnController);
begin
  if FColumnController <> nil then 
    raise Exception.Create('Can''t change ColumnController');
  FColumnController := Value;
end;

function TdxTableViewInfoManager.GetCurrentTableViewInfo: TdxTableViewInfo;
begin
  Result := FTableViewInfos[FCurrentTableViewInfoIndex];
end;

function TdxTableViewInfoManager.GetCurrentCellBottom: Integer;
begin
  Result := FCurrentCellBottoms[FCurrentTableViewInfoIndex];
end;

procedure TdxTableViewInfoManager.SetCurrentCellBottom(Value: Integer);
begin
  FCurrentCellBottoms[FCurrentTableViewInfoIndex] := Value;
end;


function TdxTableViewInfoManager.CreateTableViewInfo(ATable: TdxTable; ATopRowIndex: Integer; AFirstContentInParentCell: Boolean; AVerticalBorderPositions: TdxVerticalBorderPositions): TdxTableViewInfo;
var
  ACurrentController: TdxTableCellColumnController;
  ACurrentTopLevelColumn: TdxColumn;
begin
  if RowsController.ColumnController is TdxTableCellColumnController then
    ACurrentController := TdxTableCellColumnController(RowsController.ColumnController)
  else
    ACurrentController := nil;
  if ACurrentController <> nil then
    ACurrentTopLevelColumn := ACurrentController.CurrentTopLevelColumn
  else
    ACurrentTopLevelColumn := RowsController.CurrentColumn;
  Result := TdxTableViewInfo.Create(ATable, ACurrentTopLevelColumn, RowsController.CurrentColumn,
    AVerticalBorderPositions, ATopRowIndex, GetParentTableCellViewInfo, AFirstContentInParentCell);
  ACurrentTopLevelColumn.Tables.Add(Result);
  if ACurrentController <> nil then
    ACurrentController.AddInnerTable(Result);
end;

function TdxTableViewInfoManager.AddTableViewInfo(ATableViewInfo: TdxTableViewInfo): Integer;
var
  ALastTableViewInfo: TdxTableViewInfo;
begin
  Result := FTableViewInfos.Count;
  if Result > 0 then
  begin
    ALastTableViewInfo := FTableViewInfos[Result - 1];
    ALastTableViewInfo.NextTableViewInfo := ATableViewInfo;
    ATableViewInfo.PrevTableViewInfo := ALastTableViewInfo;
  end;
  FTableViewInfos.Add(ATableViewInfo);
  FCurrentCellBottoms.Add(0);
end;

function TdxTableViewInfoManager.GetStartTableViewInfoByRowSeparatorIndex(ARowSeparatorIndex: Integer): TdxTableViewInfo;
var
  ATableViewInfoIndex: Integer;
begin
  ATableViewInfoIndex := GetStartTableViewInfoIndexByRowSeparatorIndex(ARowSeparatorIndex);
  Result := FTableViewInfos[ATableViewInfoIndex];
end;

function TdxTableViewInfoManager.GetStartTableViewInfoIndexByRowSeparatorIndex(ARowSeparatorIndex: Integer): Integer;
var
  ATableViewInfoIndex: Integer;
begin
  ATableViewInfoIndex := TdxAlgorithms.BinarySearch<TdxTableViewInfo>(FTableViewInfos, TTableViewInfoAndStartRowSeparatorIndexComparer.Create(ARowSeparatorIndex));

  if ATableViewInfoIndex < 0 then
    ATableViewInfoIndex := (not ATableViewInfoIndex) - 1;

  Result := ATableViewInfoIndex;
end;

function TdxTableViewInfoManager.GetEndTableViewInfoIndexByRowSeparatorIndex(ARowSeparatorIndex: Integer): Integer;
var
  ATableViewInfoIndex: Integer;
begin
  Assert(ARowSeparatorIndex > 0);
  ATableViewInfoIndex := TdxAlgorithms.BinarySearch<TdxTableViewInfo>(FTableViewInfos, TTableViewInfoAndEndRowSeparatorIndexComparer.Create(ARowSeparatorIndex));

  if ATableViewInfoIndex < 0 then
  begin
    Assert(not ATableViewInfoIndex = FTableViewInfos.Count);
    ATableViewInfoIndex := FTableViewInfos.Count - 1;
  end;
  Result := ATableViewInfoIndex;
end;

procedure TdxTableViewInfoManager.SetRowSeparatorCore(ATableViewInfo: TdxTableViewInfo; ARowSeparatorIndex: Integer; AAnchor: TdxTableCellVerticalAnchor);
var
  AAnchorIndex: Integer;
begin
  AAnchorIndex := ARowSeparatorIndex - ATableViewInfo.TopRowIndex;
  Assert(AAnchorIndex <= ATableViewInfo.Rows.Count);
  ATableViewInfo.Anchors[AAnchorIndex] := AAnchor;
end;

function TdxTableViewInfoManager.CreateTableRowViewInfo(ATableViewInfo: TdxTableViewInfo; ARow: TdxTableRow; ARowIndex: Integer): TdxTableRowViewInfoBase;
var
  ACellSpacing: TdxModelUnit;
begin
  ACellSpacing := TdxTablesControllerTableState.GetActualCellSpacing(ARow);
  if ACellSpacing <> 0 then
    Result := TdxTableRowViewInfoWithCellSpacing.Create(ATableViewInfo, ARowIndex, ACellSpacing)
  else
    Result := TdxTableRowViewInfoNoCellSpacing.Create(ATableViewInfo, ARowIndex);
end;

function TdxTableViewInfoManager.GetRowStartAnchorIndex(ATableViewInfo: TdxTableViewInfo; ARowIndex: Integer): Integer;
begin
  Result := ARowIndex - ATableViewInfo.TopRowIndex;
end;

function TdxTableViewInfoManager.GetRowStartAnchor(ATableViewInfo: TdxTableViewInfo; ARowIndex: Integer): IdxTableCellVerticalAnchor;
var
  AAnchorIndex: Integer;
begin
  AAnchorIndex := GetRowStartAnchorIndex(ATableViewInfo, ARowIndex);
  Result := ATableViewInfo.Anchors[AAnchorIndex] as IdxTableCellVerticalAnchor; 
end;

function TdxTableViewInfoManager.AddFirstCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACell: TdxTableCell;
  const ABounds: TRect; const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect): TdxTableCellViewInfo;
var
  ATopAnchorIndex, ABottomAnchorIndex: Integer;
  ACellViewInfo: TdxTableCellViewInfo;
begin
  ATopAnchorIndex := GetRowStartAnchorIndex(ATableViewInfo, AGridBounds.RowIndex);
  ABottomAnchorIndex := ATopAnchorIndex + AGridBounds.RowSpan;
  if (ABottomAnchorIndex >= ATableViewInfo.Anchors.Count) and (ATableViewInfo.NextTableViewInfo <> nil) then
    ABottomAnchorIndex := ATableViewInfo.Anchors.Count - 1;
  ACellViewInfo := TdxTableCellViewInfo.Create(ATableViewInfo, ACell, ABounds.Left, ABounds.Width, ATextBounds.Left, ATextBounds.Width, ATopAnchorIndex, ABottomAnchorIndex, AGridBounds.RowIndex, AGridBounds.RowSpan);
  ATableViewInfo.Cells.Add(ACellViewInfo);
  AddTableCellViewInfo(ATableViewInfo, ACellViewInfo, ATopAnchorIndex, ABottomAnchorIndex - 1);
  Result := ACellViewInfo;
end;

procedure TdxTableViewInfoManager.AddMiddleCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACell: TdxTableCell;
  const ABounds: TRect; const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect);
var
  ATopAnchorIndex, ABottomAnchorIndex: Integer;
  ACellViewInfo: TdxTableCellViewInfo;
begin
  ATopAnchorIndex := 0;
  ABottomAnchorIndex := ATableViewInfo.Anchors.Count - 1;
  ACellViewInfo := TdxTableCellViewInfo.Create(ATableViewInfo, ACell, ABounds.Left, ABounds.Width, ATextBounds.Left, ATextBounds.Width, ATopAnchorIndex, ABottomAnchorIndex, AGridBounds.RowIndex, AGridBounds.RowSpan);
  ATableViewInfo.Cells.Add(ACellViewInfo);
  AddTableCellViewInfo(ATableViewInfo, ACellViewInfo, ATopAnchorIndex, ABottomAnchorIndex - 1);
end;

procedure TdxTableViewInfoManager.AddLastCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACell: TdxTableCell;
  const ABounds: TRect; const AGridBounds: TdxLayoutGridRectangle; const ATextBounds: TRect);
var
  ATopAnchorIndex, ABottomAnchorIndex: Integer;
  ACellViewInfo: TdxTableCellViewInfo;
begin
  ATopAnchorIndex := 0;
  ABottomAnchorIndex := GetRowStartAnchorIndex(ATableViewInfo, AGridBounds.RowIndex + AGridBounds.RowSpan);
  ACellViewInfo := TdxTableCellViewInfo.Create(ATableViewInfo, ACell, ABounds.Left, ABounds.Width, ATextBounds.Left, ATextBounds.Width, ATopAnchorIndex, ABottomAnchorIndex, AGridBounds.RowIndex, AGridBounds.RowSpan);
  ATableViewInfo.Cells.Add(ACellViewInfo);
  AddTableCellViewInfo(ATableViewInfo, ACellViewInfo, ATopAnchorIndex, ABottomAnchorIndex - 1);
end;

function TdxTableViewInfoManager.GetLastTableViewInfo: TdxTableViewInfo;
begin
  Result := FTableViewInfos[FTableViewInfos.Count - 1];
end;

procedure TdxTableViewInfoManager.AddTableCellViewInfo(ATableViewInfo: TdxTableViewInfo; ACellViewInfo: TdxTableCellViewInfo;
  AStartRowViewInfoIndex, AEndRowViewInfoIndex: Integer);
var
  ARowViewInfoIndex, ARowIndex, ACellIndex: Integer;
begin
  for ARowViewInfoIndex := AStartRowViewInfoIndex to AEndRowViewInfoIndex do
  begin
    ARowIndex := ATableViewInfo.TopRowIndex + ARowViewInfoIndex;
    EnsureTableRowViewInfo(ATableViewInfo, ATableViewInfo.Table.Rows[ARowIndex], ARowIndex);
    ACellIndex := TdxAlgorithms.BinarySearch<TdxTableCellViewInfo>(ATableViewInfo.Rows[ARowViewInfoIndex].Cells, TdxTableCellLeftComparable.Create(ACellViewInfo.Left));
    Assert(ACellIndex < 0);
    ACellIndex := not ACellIndex;
    ATableViewInfo.Rows[ARowViewInfoIndex].Cells.Insert(ACellIndex, ACellViewInfo);
  end;
end;

procedure TdxTableViewInfoManager.MoveRowAndAnchors_(ASource, ATarget: TdxTableViewInfo; AStartAnchorIndex: Integer);
var
  I, AInitialTargetTopRowIndex, AInitialSourceBottomRowIndex, ASourceRowCount, AMovedRowCount, ARowViewInfoIndex,
  ARowIndex, ASourceAnchorsCount, ALastMovedAnchorIndex, AMovedAnchorCount, AAnchorIndex, ASourceCellCount, ATargetCellCount: Integer;
  ARow: TdxTableRow;
  AKeepFirstAnchorInTarget: Boolean;
  ASourceAnchor: TdxTableCellVerticalAnchor;
  ATopTargetRowViewInfo, ASourceRowViewInfo: TdxTableRowViewInfoBase;
  ATargetCells, ASourceCells: TdxTableCellViewInfoCollection;
  ACellViewInfo: TdxTableCellViewInfo;
  AAction: TCellAction;
begin




  Assert(AStartAnchorIndex > 0);
  AInitialTargetTopRowIndex := ATarget.TopRowIndex;
  AInitialSourceBottomRowIndex := ASource.BottomRowIndex;
  ASourceRowCount := ASource.Rows.Count;
  AMovedRowCount := ASourceRowCount - AStartAnchorIndex;
  if ATarget.TopRowIndex = ASource.BottomRowIndex then
    Dec(AMovedRowCount);                          
  ATarget.Rows.ShiftForward(AMovedRowCount + 1);  
  for I := 0 to AMovedRowCount - 1 do
  begin
    ARowViewInfoIndex := AStartAnchorIndex + I;
    ARowIndex := ASource.TopRowIndex + ARowViewInfoIndex;
    ARow := ASource.Rows[ARowViewInfoIndex].Row;
    ATarget.Rows[I + 1] := CreateTableRowViewInfo(ATarget, ARow, ARowIndex);
  end;
  ATarget.Rows[0] := CreateTableRowViewInfo(ATarget, ASource.Rows[AStartAnchorIndex - 1].Row, ASource.TopRowIndex + AStartAnchorIndex - 1);
  ASource.Rows.RemoveRows(AStartAnchorIndex, ASourceRowCount - AStartAnchorIndex);

  ASourceAnchorsCount := ASource.Anchors.Count;
  if AInitialSourceBottomRowIndex = AInitialTargetTopRowIndex then
    ALastMovedAnchorIndex := ASourceAnchorsCount - 2
  else
    ALastMovedAnchorIndex := ASourceAnchorsCount - 1;

  AMovedAnchorCount := ALastMovedAnchorIndex - AStartAnchorIndex + 1;
  AKeepFirstAnchorInTarget := AInitialSourceBottomRowIndex = AInitialTargetTopRowIndex;
  if AKeepFirstAnchorInTarget then
    ATarget.Anchors.ShiftForward(1, AMovedAnchorCount)
  else
    ATarget.Anchors.ShiftForward(0, AMovedAnchorCount);

  if not AKeepFirstAnchorInTarget then
    raise Exception.Create(''); 
  for AAnchorIndex := AStartAnchorIndex to ALastMovedAnchorIndex do
  begin
    if ASource.Anchors[AAnchorIndex] <> nil then
    begin
      ASourceAnchor := ASource.Anchors[AAnchorIndex];
      ATarget.Anchors[AAnchorIndex - AStartAnchorIndex + 1] := ASourceAnchor.CloneWithNewVerticalPosition(0); ;
    end;
  end;
  ASource.Anchors.RemoveAnchors(AStartAnchorIndex, AMovedAnchorCount);

  ATarget.SetTopRowIndex(ASource.BottomRowIndex);

  ATopTargetRowViewInfo := ATarget.Rows[AMovedRowCount + 1];
  Assert(ATopTargetRowViewInfo <> nil);
  ATargetCells := ATopTargetRowViewInfo.Cells;
  ATargetCellCount := ATargetCells.Count;
  for I := 0 to ATargetCellCount - 1 do
  begin
    AddTableCellViewInfo(ATarget, ATargetCells[I], 0, AMovedRowCount);
    ATargetCells[I].ShiftBottom(AMovedAnchorCount);
    ATargetCells[I].SetTopAnchorIndexToLastAnchor();
  end;
  ASourceRowViewInfo := ASource.Rows[AStartAnchorIndex - 1];
  ASourceCells := ASourceRowViewInfo.Cells;
  ASourceCellCount := ASourceCells.Count;
  for I := 0 to ASourceCellCount - 1 do
  begin
    ACellViewInfo := ASourceCells[I];
    AAction := ShouldSplitCell(ASource, ATarget, ACellViewInfo, AStartAnchorIndex, ASourceAnchorsCount, AInitialSourceBottomRowIndex, AInitialTargetTopRowIndex); 
    case AAction of
      TCellAction.Split:
        SplitCellByAnchor(ASource, ATarget, AStartAnchorIndex, ACellViewInfo);
      TCellAction.SetBottomIndex:
        ACellViewInfo.SetBottomAnchorIndexToLastAnchor;
    end;
  end;
end;

function TdxTableViewInfoManager.ShouldSplitCell(ASource, ATarget: TdxTableViewInfo; ACellViewInfo: TdxTableCellViewInfo; ASplitAnchorIndex, ASourceAnchorsCount, AInitialSourceBottomRowIndex, AInitialTargetTopRowIndex: Integer): TCellAction;
var
  ABottomAnchorIndex: Integer;
begin
  ABottomAnchorIndex := ACellViewInfo.BottomAnchorIndex;
  if ABottomAnchorIndex < ASplitAnchorIndex then
    Exit(TCellAction.None);
  if ABottomAnchorIndex < ASourceAnchorsCount - 1 then
    Exit(TCellAction.Split);
  Assert(ABottomAnchorIndex = ASourceAnchorsCount - 1);
  if AInitialTargetTopRowIndex = AInitialSourceBottomRowIndex then
    Exit(TCellAction.SetBottomIndex);
  raise Exception.Create('ThrowInternalException'); 
end;

procedure TdxTableViewInfoManager.MoveRowAndAnchor(ASource, ATarget: TdxTableViewInfo; AStartAnchorIndex: Integer);
var
  ARowCount, ARowIndex, AAnchorCount, AAnchorIndex: Integer;
  ARow: TdxTableRow;
  ASourceAnchor: TdxTableCellVerticalAnchor;
begin
  ARowCount := ASource.Rows.Count;
  for ARowIndex := AStartAnchorIndex - 1 to ARowCount - 1 do
  begin
    ARow := ASource.Rows[ARowIndex].Row;
    EnsureTableRowViewInfo(ATarget, ARow, ASource.TopRowIndex + ARowIndex);
  end;

  AAnchorCount := ASource.Anchors.Count;
  for AAnchorIndex := AStartAnchorIndex to AAnchorCount - 1 do
  begin
    if ASource.Anchors[AAnchorIndex] <> nil then
    begin
      ASourceAnchor := ASource.Anchors[AAnchorIndex];
      ATarget.Anchors[AAnchorIndex - AStartAnchorIndex + 1] := ASourceAnchor.CloneWithNewVerticalPosition(0);
    end;
  end;
  ASource.Rows.RemoveRows(AStartAnchorIndex, ARowCount - AStartAnchorIndex);
  ASource.Anchors.RemoveAnchors(AStartAnchorIndex, AAnchorCount - AStartAnchorIndex);
end;

procedure TdxTableViewInfoManager.SplitCellsByAncor(ACurrentTableViewInfo, ANextTableViewInfo: TdxTableViewInfo; AAnchorIndex: Integer);
var
  I, ARowViewInfoIndex, ACellCount: Integer;
  ARowViewInfo: TdxTableRowViewInfoBase;
begin
  Assert(AAnchorIndex > 0);
  ARowViewInfoIndex := AAnchorIndex - 1;
  ARowViewInfo := ACurrentTableViewInfo.Rows[ARowViewInfoIndex];
  ACellCount := ARowViewInfo.Cells.Count;
  for I := 0 to ACellCount - 1 do
    SplitCellByAnchor(ACurrentTableViewInfo, ANextTableViewInfo, AAnchorIndex, ARowViewInfo.Cells[I]);
end;

procedure TdxTableViewInfoManager.SplitCellByAnchor(ACurrentTableViewInfo, ANextTableViewInfo: TdxTableViewInfo;
  AAnchorIndex: Integer; ATableCellViewInfo: TdxTableCellViewInfo);
var
  ANewBottomAnchorIndex, ARelativeLeft: Integer;
  ANextCellViewInfo: TdxTableCellViewInfo;
begin
  ANewBottomAnchorIndex := ATableCellViewInfo.BottomAnchorIndex - AAnchorIndex + 1;
  ARelativeLeft := ATableCellViewInfo.Left - ACurrentTableViewInfo.Column.Bounds.Left;
  ANextCellViewInfo := TdxTableCellViewInfo.Create(ANextTableViewInfo, ATableCellViewInfo.Cell, ARelativeLeft + ANextTableViewInfo.Column.Bounds.Left, ATableCellViewInfo.Width, ATableCellViewInfo.TextLeft, ATableCellViewInfo.TextWidth, 0, ANewBottomAnchorIndex, ATableCellViewInfo.StartRowIndex, ATableCellViewInfo.RowSpan);
  ANextTableViewInfo.Cells.Add(ANextCellViewInfo);
  ATableCellViewInfo.SetBottomAnchorIndexToLastAnchor;
  AddTableCellViewInfo(ANextTableViewInfo, ANextCellViewInfo, 0, ANewBottomAnchorIndex - 1);
end;

function TdxTableViewInfoManager.ShouldRemoveColumn(ATableViewInfoIndex: Integer; ATableViewInfo: TdxTableViewInfo): Boolean;
begin
  if ATableViewInfoIndex > 0 then
    Result := True
  else
    Result := ATableViewInfo.FirstContentInParentCell;
end;

function TdxTableViewInfoManager.GetTopLevelTableViewInfoManager: TdxTableViewInfoManager;
begin
  if FParentTableViewInfoManager <> nil then
    Result := FParentTableViewInfoManager.GetTopLevelTableViewInfoManager
  else
    Result := Self;
end;

function TdxTableViewInfoManager.GetParentTableCellViewInfo: TdxTableCellViewInfo;
begin
  if FParentTableViewInfoManager = nil then
    Result := nil
  else
    Result := FParentTableViewInfoManager.CurrentTableCellViewInfo;
end;

procedure TdxTableViewInfoManager.SetCurrentTableCellViewInfo(ATableCellViewInfo: TdxTableCellViewInfo);
begin
  Assert(ATableCellViewInfo <> nil);
  FCurrentTableCellViewInfo := ATableCellViewInfo; 
  FCurrentTableViewInfoIndex := FTableViewInfos.IndexOf(FCurrentTableCellViewInfo.TableViewInfo);
  if FParentTableViewInfoManager <> nil then
    FParentTableViewInfoManager.SetCurrentTableCellViewInfo(ATableCellViewInfo.TableViewInfo.ParentTableCellViewInfo);
  Assert(FCurrentTableViewInfoIndex >= 0);
end;

procedure TdxTableViewInfoManager.ValidateTopLevelColumn;
begin
end;

procedure TdxTableViewInfoManager.SetCurrentParentColumn(ATableViewInfo: TdxTableViewInfo);
begin
  FColumnController.SetCurrentParentColumn(ATableViewInfo.Column);
  if FParentTableViewInfoManager <> nil then
    FParentTableViewInfoManager.SetCurrentParentColumn(ATableViewInfo.ParentTableCellViewInfo.TableViewInfo);
end;

function TdxTableViewInfoManager.FindSuitableTableCellViewInfo(ACells: TdxTableCellViewInfoCollection; ACell: TdxTableCell): TdxTableCellViewInfo;
var
  I: Integer;
  ACellViewInfo: TdxTableCellViewInfo;
begin
  for I := ACells.Count - 1 downto 0 do
  begin
    ACellViewInfo := ACells[I];
    if ACellViewInfo.Cell = ACell then
      Exit(ACellViewInfo);
  end;
  Result := nil;
end;

function TdxTableViewInfoManager.GetSplitAnchorHorizontalCellBorders(ASplitAnchor: TdxTableCellVerticalAnchor): TList<TdxHorizontalCellBordersInfo>;
begin
  if ASplitAnchor <> nil then
    Result := ASplitAnchor.CellBorders
  else
    Result := nil;
end;


{ TdxTabsController }

constructor TdxTabsController.Create;
begin
  inherited Create;
  FTabStack := TdxIntegerStack.Create; 
  FTabs := TdxTabFormattingInfo.Create;
end;

destructor TdxTabsController.Destroy;
begin
  FreeAndNil(FTabs);
  FreeAndNil(FTabStack);
  inherited Destroy;
end;

function TdxTabsController.AdjustAlignedTabWidth(ATab: TdxTabInfo; ATabWidth, ALastNonSpaceBoxRight: Integer): Integer;
begin
  if IsTabPositionBehindParagraphRight(ATab) and (ALastNonSpaceBoxRight + ATabWidth > FParagraphRight) then
    Result := FParagraphRight - ALastNonSpaceBoxRight
  else
    Result := ATabWidth;
end;

procedure TdxTabsController.BeginParagraph(AParagraph: TdxParagraph);
begin
  FParagraph := AParagraph;
  FPieceTable := AParagraph.PieceTable;
  FDocumentModel := AParagraph.DocumentModel;
  ClearLastTabPosition;
  ObtainTabs;
end;

function TdxTabsController.CalcLastCenterTabWidth(ATab: TdxTabInfo; AStartIndex: Integer): Integer;
var
  ALastNonSpaceBoxIndex, ATabbedContentWidth, ALastNonSpaceBoxRight, ATabPosition, ATabWidth: Integer;
begin
  ALastNonSpaceBoxIndex := TdxRow.FindLastNonSpaceBoxIndex(FBoxes, AStartIndex);
  ATabbedContentWidth := FBoxes[ALastNonSpaceBoxIndex].Bounds.Right - FBoxes[AStartIndex].Bounds.Left;
  ALastNonSpaceBoxRight := FBoxes[ALastNonSpaceBoxIndex].Bounds.Right;
  ATabPosition := ATab.GetLayoutPosition(documentModel.ToDocumentLayoutUnitConverter);
  ATabWidth := Trunc(ATabbedContentWidth / 2 + (FColumnLeft + ATabPosition) - ALastNonSpaceBoxRight);
  ATabWidth := AdjustAlignedTabWidth(ATab, ATabWidth, ALastNonSpaceBoxRight);
  Result := Math.Max(0, ATabWidth);
end;

function TdxTabsController.CalcLastDecimalTabWidth(ATab: TdxTabInfo; AStartIndex: Integer;
  AFormatter: TdxParagraphBoxFormatter; ARowLeft: Integer): Integer;
var
  ALastNonSpaceBoxIndex, ADecimalPointPosition, ALastNonSpaceBoxRight, ATabPosition, ATabWidth: Integer;
begin
  ALastNonSpaceBoxIndex := TdxRow.FindLastNonSpaceBoxIndex(FBoxes, AStartIndex);
  ADecimalPointPosition := FindDecimalPointPosition(FBoxes, AStartIndex + 1, ALastNonSpaceBoxIndex, AFormatter, ARowLeft);
  if ALastNonSpaceBoxIndex >= 0 then
    ALastNonSpaceBoxRight := FBoxes[ALastNonSpaceBoxIndex].Bounds.Right
  else
    ALastNonSpaceBoxRight := ARowLeft;
  ATabPosition := ATab.GetLayoutPosition(FDocumentModel.ToDocumentLayoutUnitConverter);
  ATabWidth := (FColumnLeft + ATabPosition) - ADecimalPointPosition;
  ATabWidth := AdjustAlignedTabWidth(ATab, ATabWidth, ALastNonSpaceBoxRight);
  Result := Max(0, ATabWidth);
end;

function TdxTabsController.CalcLastRightTabWidth(ATab: TdxTabInfo; AStartIndex: Integer): Integer;
var
  ATabPosition, ALastNonSpaceBoxIndex: Integer;
begin
  ALastNonSpaceBoxIndex := TdxRow.FindLastNonSpaceBoxIndex(FBoxes, AStartIndex);
  ATabPosition := ATab.GetLayoutPosition(FDocumentModel.ToDocumentLayoutUnitConverter);
  Result := Max(0, (FColumnLeft + ATabPosition) - FBoxes[ALastNonSpaceBoxIndex].Bounds.Right);
end;

function TdxTabsController.CalcLastTabWidth(ARow: TdxRow; AFormatter: TdxParagraphBoxFormatter): Integer;
var
  AIndex, ATabWidth: Integer;
  ABox: TdxTabSpaceBox;
  ATab: TdxTabInfo;
  R: TRect;
begin
  if FTabStack.Count <= 0 then
    Exit(0);
  FBoxes := ARow.Boxes;

  AIndex := FTabStack.Pop;
  if AIndex >= 0 then
    ABox := TdxTabSpaceBox(FBoxes[AIndex])
  else
    ABox := nil;
  if ABox <> nil then
    ATab := ABox.TabInfo
  else
    ATab := FTabs[0];
  case ATab.Alignment of
    TdxTabAlignmentType.Right:
      ATabWidth := CalcLastRightTabWidth(ATab, AIndex);
    TdxTabAlignmentType.Center:
      ATabWidth := CalcLastCenterTabWidth(ATab, AIndex);
    TdxTabAlignmentType.Decimal:
      ATabWidth := CalcLastDecimalTabWidth(ATab, AIndex, AFormatter, ARow.Bounds.Left);
  else
    Exit(0);
  end;
  if ABox <> nil then
  begin
    R := ABox.Bounds;
    R.Width := ATabWidth;
    ABox.Bounds := R;
  end
  else
    ARow.TextOffset := ARow.TextOffset + ATabWidth;
  Result := ATabWidth;
end;

class function TdxTabsController.CalculateLeaderCount(ABox: TdxTabSpaceBox; APieceTable: TdxPieceTable): Integer;
begin
  if ABox.TabInfo.Leader = TdxTabLeaderType.None then
    Result := -1
  else
    Result := ABox.Bounds.Width div Max(1, GetTabLeaderCharacterWidth(ABox, APieceTable));
end;

class function TdxTabsController.GetTabLeaderCharacterWidth(ABox: TdxTabSpaceBox; APieceTable: TdxPieceTable): Integer;
var
  AFontInfo: TdxFontInfo;
begin
  AFontInfo := ABox.GetFontInfo(APieceTable);
  case ABox.TabInfo.Leader of
    TdxTabLeaderType.MiddleDots:
      Result := AFontInfo.MiddleDotWidth;
    TdxTabLeaderType.Hyphens:
      Result := AFontInfo.DashWidth;
    TdxTabLeaderType.EqualSign:
      Result := AFontInfo.EqualSignWidth;
    TdxTabLeaderType.ThickLine,
    TdxTabLeaderType.Underline:
      Result := AFontInfo.UnderscoreWidth;
    else
      Result := AFontInfo.DotWidth;
  end;
end;

class procedure TdxTabsController.CacheLeader(ABox: TdxTabSpaceBox; AFormatter: TdxParagraphBoxFormatter);
var
  AText: string;
  AFontInfo: TdxFontInfo;
  AMeasurer: TdxBoxMeasurer;
  ATextInfo: TdxTextViewInfo;
begin
  try
    if ABox.LeaderCount > 0 then
    begin
      AText := StringOfChar(GetTabLeaderCharacter(ABox.TabInfo.Leader), ABox.LeaderCount);
      AFontInfo := ABox.GetFontInfo(AFormatter.PieceTable);
      AMeasurer := AFormatter.Measurer;
      if AMeasurer.TextViewInfoCache.TryGetTextViewInfo(AText, AFontInfo) = nil then
      begin
        ATextInfo := AMeasurer.CreateTextViewInfo(nil, AText, AFontInfo);
        AMeasurer.TextViewInfoCache.AddTextViewInfo(AText, AFontInfo, ATextInfo);
      end;
    end;
  except
  end;
end;

class function TdxTabsController.GetTabLeaderCharacter(ALeaderType: TdxTabLeaderType): Char;
begin
  case ALeaderType of
    TdxTabLeaderType.MiddleDots:
      Result := TdxCharacters.MiddleDot;
    TdxTabLeaderType.Hyphens:
      Result := TdxCharacters.Dash;
    TdxTabLeaderType.EqualSign:
      Result := TdxCharacters.EqualSign;
    TdxTabLeaderType.ThickLine,
    TdxTabLeaderType.Underline:
      Result := TdxCharacters.Underscore;
    else
      Result := TdxCharacters.Dot;
  end;
end;

procedure TdxTabsController.ClearLastTabPosition;
begin
  FTabStack.Clear;
end;

procedure TdxTabsController.UpdateLastTabPosition(ABoxesCount: Integer);
begin
  if (FTabStack.Count > 0) and (FTabStack.Peek >= ABoxesCount) then
    FTabStack.Pop;
end;

function TdxTabsController.FindDecimalPointPosition(ABoxes: TdxBoxCollection; AFrom, ATo: Integer;
  AFormatter: TdxParagraphBoxFormatter; ARowLeft: Integer): Integer;

  function IndexOfAnyDefaultDecimalChars(const AText: string): Integer; 
  var
    I: Integer;
  begin
    for I := 0 to Length(DefaultDecimalChars) - 1 do
    begin
      Result := Pos(DefaultDecimalChars[I], AText);
      if Result > 0 then
        Break;
    end;
  end;

var
  ADecimalPointBoxIndex, ADecimalPointCharIndex: Integer;
  I: Integer;
  AText: string;
  ABox: TdxBox;
  ABoxInfo: TdxBoxInfo;
begin
  ADecimalPointBoxIndex := -1;
  ADecimalPointCharIndex := -1;
  for I := AFrom to ATo do
  begin
    AText := FBoxes[I].GetText(FPieceTable);
    ADecimalPointCharIndex := Pos(DocumentModel.NumberDecimalSeparator, AText);
    if(ADecimalPointCharIndex <= 0) then
      ADecimalPointCharIndex := IndexOfAnyDefaultDecimalChars(AText);
    if ADecimalPointCharIndex > 0 then
    begin
      ADecimalPointBoxIndex := I;
      Break;
    end;
  end;

  if (ADecimalPointBoxIndex < 0) then
  begin
    if ATo >= 0 then
      Result := FBoxes[ATo].Bounds.Right
    else
      Result := ARowLeft;
    Exit;
  end;

  ABox := FBoxes[ADecimalPointBoxIndex];

  if ADecimalPointCharIndex = 0 then
  begin
    Result := ABox.Bounds.Left;
    Exit;
  end;
  ABoxInfo := TdxBoxInfo.Create;
  try
    ABoxInfo.StartPos := ABox.StartPos;
    ABoxInfo.EndPos.CopyFrom(ABox.EndPos.RunIndex, ABox.StartPos.Offset + ADecimalPointCharIndex - 1, 0);
    AFormatter.Measurer.MeasureText(ABoxInfo);
    Result := ABox.Bounds.Left + ABoxInfo.Size.cx;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxTabsController.GetNextTab(APos: Integer): TdxTabInfo;
var
  ADefaultTabWidth, ATabNumber: Integer;
begin
  Result := FTabs.FindNextTab(APos);
  if Result = nil then
  begin
    ADefaultTabWidth := FDocumentModel.DocumentProperties.DefaultTabWidth;
    if ADefaultTabWidth <= 0 then
      Result := TdxDefaultTabInfo.Create(APos)
    else
    begin
      ATabNumber := (APos div ADefaultTabWidth) + 1;
      Result := TdxDefaultTabInfo.Create(ATabNumber * ADefaultTabWidth);
    end;
    FTabs.Add(Result); 
  end;
end;

function TdxTabsController.IsTabPositionBehindParagraphRight(ATab: TdxTabInfo): Boolean;
var
  ATabPosition: Integer;
begin
  ATabPosition := ATab.GetLayoutPosition(FDocumentModel.ToDocumentLayoutUnitConverter);
  Result := ATabPosition + FColumnLeft <= FParagraphRight;
end;

procedure TdxTabsController.ObtainTabs;
var
  ATempTabs, AAutomaticTabAtHangingIndent: TdxTabFormattingInfo;
begin
  FreeAndNil(FTabs);
  if (FParagraph.FirstLineIndentType = TdxParagraphFirstLineIndent.Hanging) and not FParagraph.IsInList then
  begin
    AAutomaticTabAtHangingIndent := TdxTabFormattingInfo.Create;
    try
      AAutomaticTabAtHangingIndent.Add(TdxDefaultTabInfo.Create(FParagraph.LeftIndent));
      ATempTabs := FParagraph.GetTabs;
      try
        FTabs := TdxTabFormattingInfo.Merge(ATempTabs, AAutomaticTabAtHangingIndent);
      finally
        FreeAndNil(ATempTabs);
      end;
    finally
      FreeAndNil(AAutomaticTabAtHangingIndent);
    end;
  end
  else
    FTabs := FParagraph.GetTabs;
  if (FTabs.Count = 1) and (FTabs[0].Alignment = TdxTabAlignmentType.Decimal) and FParagraph.IsInCell then
    SingleDecimalTabInTable := True
  else
    SingleDecimalTabInTable := False;
end;

procedure TdxTabsController.SaveLastTabPosition(APos: Integer);
begin
  FTabStack.Push(APos);
end;

{ TdxTableCellColumnController }

constructor TdxTableCellColumnController.Create(AParent: IColumnController; ACurrentParentColumn: TdxColumn; ALeft,
  ATop, AWidth: Integer; ATableViewInfo: TdxTableViewInfo; ACurrentCell: TdxTableCell);
begin
  inherited Create;
  Assert(AParent <> nil);
  Assert(ACurrentParentColumn <> nil);
  FParent := AParent;
  FCurrentParentColumn := ACurrentParentColumn;
  FCurrentCell := ACurrentCell;
  FGeneratedColumns := TdxColumnList.Create;
  FGeneratedColumns.Add(ACurrentParentColumn);
  StartNewCell(ACurrentParentColumn, ALeft, ATop, AWidth, ACurrentCell);
  FGeneratedTableViewInfo := TdxTableViewInfoList.Create;
  FGeneratedTableViewInfo.Add(ATableViewInfo);
end;

destructor TdxTableCellColumnController.Destroy;
begin
  FreeAndNil(FGeneratedTableViewInfo);
  FreeAndNil(FGeneratedColumns);
  inherited Destroy;
end;

function TdxTableCellColumnController.CreateRow: TdxRow;
begin
  FLastCreatedRow := TdxTableCellRow.Create(FCurrentCellViewInfo);
  Result := FLastCreatedRow;
end;

procedure TdxTableCellColumnController.AddInnerTable(ATableViewInfo: TdxTableViewInfo);
begin
  CurrentCell.AddInnerTable(ATableViewInfo);
end;

procedure TdxTableCellColumnController.RemoveGeneratedColumn(AColumn: TdxColumn);
var
  AInitialColumnCount: Integer;
begin
  AInitialColumnCount := FGeneratedColumns.Count;
  if AInitialColumnCount > 1 then
  begin
    FGeneratedColumns.Delete(AInitialColumnCount - 1);
    if FCurrentColumnIndex >= FGeneratedColumns.Count then
    begin
      FCurrentColumnIndex := FGeneratedColumns.Count - 1;
      FCurrentParentColumn := FGeneratedColumns[FCurrentColumnIndex];
    end;
  end;
  if (FParent is TdxColumnController) and (AInitialColumnCount > 1) then
    TdxColumnController(FParent).RemoveGeneratedColumn(AColumn);
end;

function TdxTableCellColumnController.GetPreviousColumn(AColumn: TdxColumn): TdxColumn;
begin
  Result := nil;
end;

function TdxTableCellColumnController.CompleteCurrentColumnFormatting(AColumn: TdxColumn): TdxCompleteFormattingResult;
begin
  if FCurrentColumnIndex + 1 >= FGeneratedColumns.Count then
    Result := FParent.CompleteCurrentColumnFormatting(AColumn)
  else
    Result := TdxCompleteFormattingResult.Success;
end;

procedure TdxTableCellColumnController.ResetToFirstColumn;
begin
  FParent.ResetToFirstColumn;
end;

function TdxTableCellColumnController.GetNextColumn(AColumn: TdxColumn; AKeepFloatingObjects: Boolean): TdxColumn;
var
  ANewColumn: TdxColumn;
  ABounds: TRect;
begin
  Inc(FCurrentColumnIndex);
  if (FCurrentColumnIndex >= FGeneratedColumns.Count) then
  begin
    ANewColumn := FParent.GetNextColumn(FCurrentParentColumn, AKeepFloatingObjects);
    FGeneratedColumns.Add(ANewColumn);
  end
  else
    if FParent is TdxTableCellColumnController then
      TdxTableCellColumnController(FParent).MoveToNextColumn;
  FCurrentParentColumn := FGeneratedColumns[FCurrentColumnIndex];
  ABounds := FCurrentParentColumn.Bounds;
  Result := TdxTableCellColumn.Create(FCurrentParentColumn, FCurrentCell);

  Result.Bounds := TRect.CreateSize(ABounds.Left + FLeft, ABounds.Top, FWidth, ABounds.Height);
end;

function TdxTableCellColumnController.GetPageAreaController: TdxPageAreaController;
begin
  Result := Parent.PageAreaController;
end;

procedure TdxTableCellColumnController.MoveToNextColumn;
begin
  Inc(FCurrentColumnIndex);
  if FParent is TdxTableCellColumnController then
    TdxTableCellColumnController(FParent).MoveToNextColumn();
  FCurrentParentColumn := FGeneratedColumns[FCurrentColumnIndex];
end;

function TdxTableCellColumnController.GetStartColumn: TdxColumn;
var
  ABounds: TRect;
begin
  ABounds := FGeneratedColumns[FCurrentColumnIndex].Bounds;
  Result := TdxTableCellColumn.Create(FGeneratedColumns[FCurrentColumnIndex], FCurrentCell);
  Result.Bounds := TRect.CreateSize(ABounds.Left + FLeft, FTop, FWidth, ABounds.Bottom - FTop);
end;

procedure TdxTableCellColumnController.SetCurrentParentColumn(AColumn: TdxColumn);
begin
  FCurrentColumnIndex := FGeneratedColumns.IndexOf(AColumn);
  FCurrentParentColumn := AColumn;
  if Parent is TdxTableCellColumnController then
    TdxTableCellColumnController(Parent).SetCurrentParentColumn(TdxTableCellColumn(AColumn).Parent);
end;

procedure TdxTableCellColumnController.StartNewCell(ACurrentParentColumn: TdxColumn; ALeft, ATop, AWidth: Integer;
  ACurrentCell: TdxTableCell);
begin
  FLeft := ALeft;
  FTop := ATop;
  FWidth := AWidth;
  FCurrentCell := ACurrentCell;
  SetCurrentParentColumn(ACurrentParentColumn);
end;

function TdxTableCellColumnController.GetMaxAnchor(AAnchor1, AAnchor2: TdxTableCellVerticalAnchor): TdxTableCellVerticalAnchor;
begin
  if AAnchor1.VerticalPosition > AAnchor2.VerticalPosition then
    Result := TdxTableCellVerticalAnchor.Create(AAnchor1.VerticalPosition, AAnchor1.BottomTextIndent, AAnchor1.CellBorders)
  else
    Result := TdxTableCellVerticalAnchor.Create(AAnchor2.VerticalPosition, AAnchor2.BottomTextIndent, AAnchor1.CellBorders);
end;

procedure TdxTableCellColumnController.AddTableViewInfo(ACurrentTableViewInfo: TdxTableViewInfo);
begin
  FGeneratedTableViewInfo.Add(ACurrentTableViewInfo);
end;

procedure TdxTableCellColumnController.RemoveTableViewInfo(ATableViewInfo: TdxTableViewInfo);
begin
  FGeneratedTableViewInfo.Remove(ATableViewInfo);
end;

function TdxTableCellColumnController.GetCurrentPageBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
begin
  Result := ACurrentColumn.Bounds;
end;

function TdxTableCellColumnController.GetCurrentPageClientBounds(ACurrentPage: TdxPage; ACurrentColumn: TdxColumn): TRect;
begin
  Result := ACurrentColumn.Bounds;
end;

function TdxTableCellColumnController.GetPageLastRunIndex: TdxRunIndex;
begin
  Result := PageAreaController.PageController.PageLastRunIndex;
end;

function TdxTableCellColumnController.GetShouldZeroSpacingBeforeWhenMoveRowToNextColumn: Boolean;
begin
  Result := False;
end;

procedure TdxTableCellColumnController.SetCurrentTableCellViewInfo(AValue: TdxTableCellViewInfo);
begin
  if FLastCreatedRow <> nil then
    FLastCreatedRow.CellViewInfo := AValue;
  FCurrentCellViewInfo := AValue;
end;

function TdxTableCellColumnController.GetMeasurer: TdxBoxMeasurer;
begin
  Result := FParent.Measurer;
end;

function TdxTableCellColumnController.GetParentColumn: TdxColumn;
begin
  Result := FGeneratedColumns[FCurrentColumnIndex];
end;

function TdxTableCellColumnController.GetLastParentColumn: TdxColumn;
begin
  Result := FGeneratedColumns[FGeneratedColumns.Count - 1];
end;

function TdxTableCellColumnController.GetFirstParentColumn: TdxColumn;
begin
  Result := FGeneratedColumns[0];
end;

function TdxTableCellColumnController.GetCurrentTopLevelColumn: TdxColumn;
begin
  if TObject(FParent) is TdxTableCellColumnController then
    Result := TdxTableCellColumnController(FParent).CurrentTopLevelColumn
  else
    Result := ParentColumn;
end;

function TdxTableCellColumnController.GetViewInfo: TdxTableViewInfo;
begin
  if FCurrentColumnIndex < FGeneratedTableViewInfo.Count then
    Result := FGeneratedTableViewInfo[FCurrentColumnIndex]
  else
    Result := nil;
end;

{ TdxColumnList }

function TdxColumnList.GetItems(Index: Integer): TdxColumn;
begin
  Result := TdxColumn(inherited Items[Index]);
end;

{ TdxTableViewInfoList }

function TdxTableViewInfoList.GetItems(Index: Integer): TdxTableViewInfo;
begin
  Result := TdxTableViewInfo(inherited Items[Index]);
end;

{ TdxTableCellColumn }

procedure TdxTableCellColumn.AddParagraphFrame(AParagraph: TdxParagraph);
begin
  TopLevelColumn.AddParagraphFrame(AParagraph);
end;

constructor TdxTableCellColumn.Create(AParent: TdxColumn; ACell: TdxTableCell);
begin
  inherited Create;
  Assert(AParent <> nil);
  FParent := AParent;
  FCell := ACell;
end;

function TdxTableCellColumn.GetOwnRows: TdxRowCollection;
begin
  Result := TdxTableCellViewInfo.GetRows(Rows, Cell);;
end;

function TdxTableCellColumn.GetRows: TdxRowCollection;
begin
  Result := FParent.Rows;
end;

function TdxTableCellColumn.GetTopLevelColumn: TdxColumn;
begin
  Result := Parent.TopLevelColumn;
end;

{ TdxSyllableBoxIterator }

constructor TdxSyllableBoxIterator.Create(AIterator: TdxParagraphBoxIterator; AHyphenationService: IHyphenationService);
begin
  inherited Create(AIterator.Paragraph, AIterator.PieceTable, AIterator.VisibleTextFilter);
  Assert(AHyphenationService <> nil);
  inherited SetPosition(AIterator.GetCurrentPosition);
  FHyphenationService := AHyphenationService;
  FIterator := AIterator;
  HyphenizeCurrentWord;
end;

destructor TdxSyllableBoxIterator.Destroy;
begin
  FreeAndNil(FHyphenPositions);
  inherited Destroy;
end;

function TdxSyllableBoxIterator.GetCurrentChar: Char;
begin
  if FHyphenAtCurrentPosition then
    Result := HyphenChar
  else
    Result := inherited CurrentChar;
end;

function TdxSyllableBoxIterator.GetCurrentWord: string;
var
  ABuilder: TdxChunkedStringBuilder;
  C: Char;
begin
  ABuilder := TdxChunkedStringBuilder.Create(32);
  try
    while not FIterator.IsEnd do
    begin
      C := FIterator.CurrentChar;
      if IsEndOfWord(C) then
        break;
      ABuilder.Append(C);
      FIterator.Next;
    end;
    FEndPos := FIterator.GetCurrentPosition;
    Result := ABuilder.ToString;
  finally
    ABuilder.Free;
  end;
end;

function TdxSyllableBoxIterator.GetHyphenChar: Char;
begin
  Result := TdxCharacters.Hyphen;
end;

function TdxSyllableBoxIterator.GetIsEnd: Boolean;
begin
  Result := ((RunIndex = FEndPos.RunIndex) and (Offset >= FEndPos.Offset)) or (RunIndex > FEndPos.RunIndex);
end;

procedure TdxSyllableBoxIterator.HyphenizeCurrentWord;
var
  AWord: string;
  AHyphenIndices: TList<Integer>;
  I, AHyphenIndex, AHyphenIndicesCount: Integer;
  AStartWordPosition: TdxFormatterPosition;
begin
  AWord := GetCurrentWord;
  AHyphenIndices := FHyphenationService.Hyphenize(AWord);
  try
    FreeAndNil(FHyphenPositions);
    FHyphenPositions := TdxFormatterPositionCollection.Create;

    AHyphenIndicesCount := AHyphenIndices.Count;
    if AHyphenIndicesCount <= 0 then
    begin
      SetInvalidHyphenPos;
      Exit;
    end;
    AStartWordPosition := GetCurrentPosition;
    I := 0;
    AHyphenIndex := 0;
    while not IsEnd and not IsWhiteSpace(inherited GetCurrentChar) do
    begin
      if I = AHyphenIndices[AHyphenIndex] then
      begin
        FHyphenPositions.Add(GetCurrentPosition);
        Inc(AHyphenIndex);
        if AHyphenIndex >= AHyphenIndicesCount then
          break;
      end;
      Inc(I);
      inherited Next;
    end;

    Assert(FHyphenPositions.Count = AHyphenIndicesCount);

    FHyphenPosIndex := 0;
    FHyphenPos := FHyphenPositions[FHyphenPosIndex];

    SetPosition(AStartWordPosition);
  finally
    AHyphenIndices.Free;
  end;
end;

function TdxSyllableBoxIterator.IsBreak(ACh: Char): Boolean;
begin
  Result := (ACh = TdxCharacters.LineBreak) or (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak);
end;

function TdxSyllableBoxIterator.IsEndOfWord(ACh: Char): Boolean;
begin
  Result := False;
  if IsWhiteSpace(ACh) then
    Result := True
  else
    if IsBreak(ACh) then
      Result := True;
end;

function TdxSyllableBoxIterator.IsWhiteSpace(ACh: Char): Boolean;
begin
  Result := (ACh = TdxCharacters.Space) or (ACh = TdxCharacters.TabMark);
end;

function TdxSyllableBoxIterator.Next: TdxParagraphIteratorResult;
begin
  if FHyphenAtCurrentPosition then
  begin
    FHyphenAtCurrentPosition := False;
    Inc(FHyphenPosIndex);
    if FHyphenPosIndex >= FHyphenPositions.Count then
      SetInvalidHyphenPos
    else
      FHyphenPos := FHyphenPositions[FHyphenPosIndex];

    Exit(TdxParagraphIteratorResult.Success);
  end;
  Result := inherited Next;
  if Result <> TdxParagraphIteratorResult.Finished then
  begin
    if (RunIndex = FHyphenPos.RunIndex) and (Offset = FHyphenPos.Offset) then
      FHyphenAtCurrentPosition := True;
    if IsEnd then
      Result := TdxParagraphIteratorResult.Finished;
  end;
end;

procedure TdxSyllableBoxIterator.SetInvalidHyphenPos;
begin
  FHyphenPos.CopyFrom(-1, -1, -1);
  FHyphenPosIndex := -1;
end;

procedure TdxSyllableBoxIterator.SetPosition(APos: TdxFormatterPosition);
begin
  SetPosition(APos.RunIndex, APos.Offset);
  BoxIndex := APos.BoxIndex;
end;

procedure TdxSyllableBoxIterator.SetPosition(ARunIndex: TdxRunIndex; AOffset: Integer);
var
  I, ACount: Integer;
  APos: TdxFormatterPosition;
begin
  inherited SetPosition(ARunIndex, AOffset);
  FHyphenAtCurrentPosition := False;
  ACount := FHyphenPositions.Count;
  for I := 0 to ACount - 1 do
  begin
    APos := FHyphenPositions[I];
    if (APos.RunIndex > ARunIndex) or ((APos.RunIndex = ARunIndex) and (APos.Offset > AOffset)) then
    begin
      FHyphenPosIndex := I;
      FHyphenPos := FHyphenPositions[FHyphenPosIndex];
      Break;
    end;
  end;
end;

{ TdxStateRowTextSplit }

procedure TdxStateRowTextSplit.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Formatter.ApproveFloatingObjects;
  AddTextBox(ABoxInfo);
  ChangeStateManuallyIfNeeded(ABoxInfo.IteratorResult);
end;

function TdxStateRowTextSplit.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  case ACurrentCharacter of
    TdxCharacters.Space,    
    TdxCharacters.EnSpace,  
    TdxCharacters.EmSpace:  
      Result := TdxParagraphBoxFormatterState.RowSpaces;
    TdxCharacters.TabMark:
      Result := TdxParagraphBoxFormatterState.RowTab;
    TdxCharacters.Dash,     
    TdxCharacters.EnDash,   
    TdxCharacters.EmDash:   
      Result := TdxParagraphBoxFormatterState.RowDash;
    TdxCharacters.LineBreak:
      Result := TdxParagraphBoxFormatterState.RowLineBreak;
    TdxCharacters.PageBreak,
    TdxCharacters.ColumnBreak:
      begin
        if RowsController.TablesController.IsInsideTable then
          Result := TdxParagraphBoxFormatterState.RowSpaces
        else
          if not RowsController.SupportsColumnAndPageBreaks then
            Result := TdxParagraphBoxFormatterState.RowLineBreak
          else
            if ACurrentCharacter = TdxCharacters.PageBreak then
              Result := TdxParagraphBoxFormatterState.RowPageBreak
            else
              Result := TdxParagraphBoxFormatterState.RowColumnBreak;
      end;
    TdxCharacters.FloatingObjectMark:
      begin
        if Iterator.IsFloatingObjectAnchorRun then
          Result := TdxParagraphBoxFormatterState.FloatingObject
        else
          Result := TdxParagraphBoxFormatterState.RowWithTextOnly
      end;
    else
      Assert(False); 
      Result := TdxParagraphBoxFormatterState.Final;
  end;
end;

function TdxStateRowTextSplit.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := CanAddBoxCore(ABoxInfo);
end;

procedure TdxStateRowTextSplit.ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult);
var
  ACurrentChar: Char;
  ANextState: TdxParagraphBoxFormatterState;
begin
  ACurrentChar := Iterator.CurrentChar;
  if ShouldChangeStateManually(AIteratorResult, ACurrentChar) then
  begin
    ANextState := CalcNextState(ACurrentChar);
    ChangeState(ANextState);
  end;
end;

function TdxStateRowTextSplit.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowTextSplit.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(CurrentRow.Height > 0);

  Result := RowsController.CompleteCurrentColumnFormatting;
  if Result <> TdxCompleteFormattingResult.Success then
    Exit;
  Formatter.RowsController.AddBox(TdxColumnBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  RowsController.MoveRowToNextColumn;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
end;

function TdxStateRowTextSplit.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(CurrentRow.Height > 0);
  Formatter.RowsController.AddBox(TdxLineBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowTextSplit.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(CurrentRow.Height > 0);

  Result := RowsController.CompleteCurrentColumnFormatting;
  if Result <> TdxCompleteFormattingResult.Success then
    Exit;
  Formatter.RowsController.AddBox(TdxPageBreakBox, ABoxInfo);
  Formatter.ApproveFloatingObjects;
  EndRow;
  RowsController.MoveRowToNextPage;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
end;

function TdxStateRowTextSplit.FinishParagraph: TdxCompleteFormattingResult;
begin
  Assert(CurrentRow.Height > 0);
  Result := inherited FinishParagraph;
end;

function TdxStateRowTextSplit.FinishSection: TdxCompleteFormattingResult;
begin
  Assert(CurrentRow.Height > 0);
  Result := inherited FinishSection;
end;

function TdxStateRowTextSplit.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplit;
end;

function TdxStateRowTextSplit.NextSplitState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplit;
end;

function TdxStateRowTextSplit.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplitAfterFloatingObject;
end;

function TdxStateRowTextSplit.HandleUnsuccessfulSplitting: TdxCompleteFormattingResult;
begin
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.RowTextSplit);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowTextSplit.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ASplitBoxResult: TdxSplitBoxResult;
begin
  Result := TdxCompleteFormattingResult.Success;
  ASplitBoxResult := SplitBox(ABoxInfo);
  if ASplitBoxResult <> TdxSplitBoxResult.FailedHorizontalOverfull then
  begin
    AddTextBox(ABoxInfo);
    Formatter.ApproveFloatingObjects;
    if not Iterator.IsEnd then
    begin
      if (Iterator.CurrentChar = TdxCharacters.PageBreak) and not RowsController.TablesController.IsInsideTable then
      begin
        if not RowsController.SupportsColumnAndPageBreaks then
          ChangeState(TdxParagraphBoxFormatterState.RowLineBreak)
        else
          ChangeState(TdxParagraphBoxFormatterState.RowPageBreak);
        Exit;
      end;
      if (Iterator.CurrentChar = TdxCharacters.ColumnBreak) and not RowsController.TablesController.IsInsideTable then
      begin
        if not RowsController.SupportsColumnAndPageBreaks then
          ChangeState(TdxParagraphBoxFormatterState.RowLineBreak)
        else
          ChangeState(TdxParagraphBoxFormatterState.RowColumnBreak);
        Exit;
      end;
      if (Iterator.CurrentChar = TdxCharacters.LineBreak) and (ASplitBoxResult <> TdxSplitBoxResult.SuccessSuppressedHorizontalOverfull) then
        ChangeState(TdxParagraphBoxFormatterState.RowLineBreak)
      else
      begin
        if not (Iterator.CurrentBox is TdxFloatingObjectAnchorBox) then
        begin
          EndRow;
          if IsTerminatorChar(Iterator.CurrentChar) then
            ChangeState(TdxParagraphBoxFormatterState.RowEmpty)
          else
            ChangeState(TdxParagraphBoxFormatterState.RowTextSplit);
        end;
      end;
    end;
  end
  else
    Result := HandleUnsuccessfulSplitting;
end;

function TdxStateRowTextSplit.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := TdxCharacters.IsCharSpace(ACh) or (ACh = TdxCharacters.TabMark) or (ACh = TdxCharacters.LineBreak) or
    (ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak) or (ACh = TdxCharacters.FloatingObjectMark) or
    TdxCharacters.IsCharDash(ACh);
end;

function TdxStateRowTextSplit.ShouldChangeStateManually(AIteratorResult: TdxParagraphIteratorResult;
  ACurrentCharacter: Char): Boolean;
begin
  Result := (AIteratorResult = TdxParagraphIteratorResult.Success) or ((not Iterator.IsEnd) and IsTerminatorChar(ACurrentCharacter));
end;

{ TdxStateRowTextSplitAfterFloatingObject }

function TdxStateRowTextSplitAfterFloatingObject.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextSplitAfterFloatingObject;
end;

{ TdxStateRowDashSplit }

function TdxStateRowDashSplit.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if not inherited IsTerminatorChar(ACurrentCharacter) then
    Result := TdxParagraphBoxFormatterState.RowText
  else
    Result := inherited CalcNextState(ACurrentCharacter);
end;

function TdxStateRowDashSplit.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDashSplit;
end;

function TdxStateRowDashSplit.HandleUnsuccessfulSplitting: TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  if CurrentRow.Height <= 0 then
  begin
    SetCurrentRowHeightToLastBoxHeight;
    ACanFit := RowsController.CanFitCurrentRowToColumn;
    if ACanFit <> TdxCanFitCurrentRowToColumnResult.RowFitted then
      Exit(Formatter.RollbackToStartOfRow(ACanFit));
  end;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowDashSplit.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := not TdxCharacters.IsCharDash(ACh);
end;

function TdxStateRowDashSplit.NextSplitState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDashSplit;
end;

function TdxStateRowDashSplit.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDashSplitAfterFloatingObject;
end;

{ TdxStateRowDashSplitAfterFloatingObject }

function TdxStateRowDashSplitAfterFloatingObject.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDashSplitAfterFloatingObject;
end;

{ TdxStateRowSpaces }

procedure TdxStateRowSpaces.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Formatter.ApproveFloatingObjects;
  RowsController.AddBox(TdxParagraphBoxFormatter.GetSpaceBoxTemplate(ABoxInfo), ABoxInfo);
  ChangeStateManuallyIfNeeded(ABoxInfo.IteratorResult);
end;

procedure TdxStateRowSpaces.ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

procedure TdxStateRowSpaces.ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

procedure TdxStateRowSpaces.ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

procedure TdxStateRowSpaces.ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo);
begin
  Assert(CurrentRow.Height <> 0);
end;

function TdxStateRowSpaces.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if ACurrentCharacter = TdxCharacters.TabMark then
    Exit( TdxParagraphBoxFormatterState.RowTab );
  if ACurrentCharacter = TdxCharacters.LineBreak then
    Exit( TdxParagraphBoxFormatterState.RowLineBreak );
  if ACurrentCharacter = TdxCharacters.PageBreak then
  begin
    Assert(not RowsController.TablesController.IsInsideTable);
    if not RowsController.SupportsColumnAndPageBreaks then
      Result := TdxParagraphBoxFormatterState.RowLineBreak
    else
      Result := TdxParagraphBoxFormatterState.RowPageBreak;
    Exit;
  end;
  if ACurrentCharacter = TdxCharacters.ColumnBreak then
  begin
    Assert(not RowsController.TablesController.IsInsideTable);
    if not RowsController.SupportsColumnAndPageBreaks then
      Result := TdxParagraphBoxFormatterState.RowLineBreak
    else
      Result := TdxParagraphBoxFormatterState.RowColumnBreak;
  end
  else
    if (ACurrentCharacter = TdxCharacters.FloatingObjectMark) and Iterator.IsFloatingObjectAnchorRun then
      Result := TdxParagraphBoxFormatterState.FloatingObject
    else
      if TdxCharacters.IsCharDash(ACurrentCharacter) then
        Result := TdxParagraphBoxFormatterState.RowDash
      else
        Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowSpaces.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := TdxAddBoxResult.Success;
end;

procedure TdxStateRowSpaces.ChangeStateManuallyIfNeeded(AIteratorResult: TdxParagraphIteratorResult);
var
  ACurrentChar: Char;
  ANextState: TdxParagraphBoxFormatterState;
begin
  ACurrentChar := Iterator.CurrentChar;
  if ShouldChangeStateManually(ACurrentChar) then
  begin
    ANextState := CalcNextState(ACurrentChar);
    ChangeState(ANextState);
  end;
end;

function TdxStateRowSpaces.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowSpaces.CanUseBox: Boolean;
begin
  Result := True;
end;

function TdxStateRowSpaces.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowSpaces;
end;

function TdxStateRowSpaces.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowSpaces.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := not TdxCharacters.IsCharSpace(ACh) or
    (((ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak)) and RowsController.TablesController.IsInsideTable);
end;

procedure TdxStateRowSpaces.MeasureBoxContent(ABoxInfo: TdxBoxInfo);
begin
  Formatter.Measurer.MeasureSpaces(ABoxInfo);
end;

function TdxStateRowSpaces.ShouldChangeStateManually(ACurrentCharacter: Char): Boolean;
begin
  Result := (not Iterator.IsEnd) and IsTerminatorChar(ACurrentCharacter);
end;

{ TdxStateRowWithSpacesOnly }

procedure TdxStateRowWithSpacesOnly.ApplyColumnBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowWithSpacesOnly.ApplyLineBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowWithSpacesOnly.ApplyPageBreakMarkSize(ABoxInfo: TdxBoxInfo);
begin
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

procedure TdxStateRowWithSpacesOnly.ApplyParagraphMarkSize(ABoxInfo: TdxBoxInfo);
begin
  RowsController.UpdateCurrentRowHeight(ABoxInfo);
end;

function TdxStateRowWithSpacesOnly.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if ACurrentCharacter = TdxCharacters.TabMark then
    Result := TdxParagraphBoxFormatterState.RowLeadingTab
  else
    if ACurrentCharacter = TdxCharacters.LineBreak then
      Result := TdxParagraphBoxFormatterState.RowLineBreak
    else
      if ACurrentCharacter = TdxCharacters.PageBreak then
      begin
        if not RowsController.SupportsColumnAndPageBreaks then
          Result := TdxParagraphBoxFormatterState.RowLineBreak
        else
          Result := TdxParagraphBoxFormatterState.RowPageBreak;
      end
      else
        if ACurrentCharacter = TdxCharacters.ColumnBreak then
        begin
          if not RowsController.SupportsColumnAndPageBreaks then
            Result := TdxParagraphBoxFormatterState.RowLineBreak
          else
            Result := TdxParagraphBoxFormatterState.RowColumnBreak;
        end
        else
          if TdxCharacters.IsCharDash(ACurrentCharacter) then
            Result := TdxParagraphBoxFormatterState.RowDash
          else
            if (ACurrentCharacter = TdxCharacters.FloatingObjectMark) and Iterator.IsFloatingObjectAnchorRun then
              Result := TdxParagraphBoxFormatterState.FloatingObject
            else
              Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowWithSpacesOnly.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
    Result := FinalizeColumn(ABoxInfo)
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowWithSpacesOnly.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    FinalizeLine(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowWithSpacesOnly.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
    Result := FinalizePage(ABoxInfo)
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowWithSpacesOnly.FinishParagraph: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateParagraphMarkBoxInfo;
  try
    Result := InternalFinishParagraphCore(ABoxInfo, FinalizeParagraph);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowWithSpacesOnly.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    Result := InternalFinishParagraphCore(ABoxInfo, FinalizeSection);
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateRowWithSpacesOnly.InternalFinishParagraphCore(ABoxInfo: TdxBoxInfo;
  AFinalizeHandler: TFinalizeHandler): TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  ApplyParagraphMarkSize(ABoxInfo);
  ACanFit := RowsController.CanFitCurrentRowToColumn(ABoxInfo);
  if ACanFit = TdxCanFitCurrentRowToColumnResult.RowFitted then
  begin
    AFinalizeHandler(ABoxInfo);
    Result := TdxCompleteFormattingResult.Success;
  end
  else
    Result := Formatter.RollbackToStartOfRow(ACanFit);
end;

function TdxStateRowWithSpacesOnly.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithSpacesOnly;
end;

{ TdxStateRowText }

function TdxStateRowText.DashAfterTextState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDashAfterText;
end;

function TdxStateRowText.GetHyphenationState: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextHyphenationFirstSyllable;
end;

function TdxStateRowText.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowText.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  if Formatter.SuppressHyphenation then
    Result := WrapLine
  else
    Result := inherited HorizontalOverfull(ABoxInfo);
end;

function TdxStateRowText.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowTextAfterFloatingObject;
end;

function TdxStateRowText.WrapLine: TdxCompleteFormattingResult;
var
  ACanFit: TdxCanFitCurrentRowToColumnResult;
begin
  Formatter.RollbackToStartOfWord;
  Formatter.RollbackToLastTab;
  RowsController.TryToRemoveLastTabBox;
  if CurrentRow.Height <= 0 then
  begin
    SetCurrentRowHeightToLastBoxHeight;
    ACanFit := RowsController.CanFitCurrentRowToColumn;
    if ACanFit <> TdxCanFitCurrentRowToColumnResult.RowFitted then
    begin
      Formatter.ClearSyllableIterator;
      Exit(Formatter.RollbackToStartOfRow(ACanFit));
    end;
  end;
  EndRow;
  ChangeState(TdxParagraphBoxFormatterState.RowEmpty);
  Result := TdxCompleteFormattingResult.Success;
end;

{ TdxStateRowDashAfterText }

function TdxStateRowDashAfterText.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if TdxCharacters.IsCharDash(ACurrentCharacter) then
    Exit(TdxParagraphBoxFormatterState.RowDash);
  if inherited IsTerminatorChar(ACurrentCharacter) then
    Result := inherited CalcNextState(ACurrentCharacter)
  else
    Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowDashAfterText.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowDashAfterText.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

{ TdxStateRowWithDashAfterTextOnly }

function TdxStateRowWithDashAfterTextOnly.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if TdxCharacters.IsCharDash(ACurrentCharacter) then 
    Exit(TdxParagraphBoxFormatterState.RowDash);
  if inherited IsTerminatorChar(ACurrentCharacter) then
    Result := inherited CalcNextState(ACurrentCharacter)
  else
    Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowWithDashAfterTextOnly.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithDashAfterTextOnly;
end;

function TdxStateRowWithDashAfterTextOnly.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Formatter.RollbackToStartOfWord;
  ChangeState(TdxParagraphBoxFormatterState.RowDashSplit);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowWithDashAfterTextOnly.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

{ TdxStateRowDash }

function TdxStateRowDash.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if inherited IsTerminatorChar(ACurrentCharacter) then
    Result := inherited CalcNextState(ACurrentCharacter)
  else
    Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowDash.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDash;
end;

function TdxStateRowDash.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Formatter.RollbackToStartOfWord;
  ChangeState(TdxParagraphBoxFormatterState.RowDashSplit);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowDash.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := not TdxCharacters.IsCharDash(ACh) or
    (((ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak)) and RowsController.TablesController.IsInsideTable);
end;

function TdxStateRowDash.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowDashAfterFloatingObject;
end;

{ TdxStateRowWithDashOnly }

function TdxStateRowWithDashOnly.CalcNextState(ACurrentCharacter: Char): TdxParagraphBoxFormatterState;
begin
  if inherited IsTerminatorChar(ACurrentCharacter) then
    Result := inherited CalcNextState(ACurrentCharacter)
  else
    Result := TdxParagraphBoxFormatterState.RowText;
end;

function TdxStateRowWithDashOnly.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithDashOnly;
end;

function TdxStateRowWithDashOnly.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Formatter.RollbackToStartOfWord;
  ChangeState(TdxParagraphBoxFormatterState.RowDashSplit);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowWithDashOnly.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := not TdxCharacters.IsCharDash(ACh) or
   (((ACh = TdxCharacters.PageBreak) or (ACh = TdxCharacters.ColumnBreak)) and RowsController.TablesController.IsInsideTable);
end;

function TdxStateRowWithDashOnly.GetStateAfterFloatingObject: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowWithDashOnlyAfterFloatingObject;
end;

{ TdxFormatterStartEndState }

procedure TdxFormatterStartEndState.AddSuccess(ABoxInfo: TdxBoxInfo);
begin
  Assert(False);
end;

function TdxFormatterStartEndState.AppendBox(var ABoxInfo: TdxBoxInfo): TdxStateContinueFormatResult;
begin
  Result := TdxStateContinueFormatResult.Success;
end;

function TdxFormatterStartEndState.CanAddBox(ABoxInfo: TdxBoxInfo): TdxAddBoxResult;
begin
  Result := TdxAddBoxResult.Success;
end;

function TdxFormatterStartEndState.ColumnOverfull(ACanFit: TdxCanFitCurrentRowToColumnResult; ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxFormatterStartEndState.HorizontalOverfull(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxFormatterStartEndState.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := False;
end;

{ TdxStateContinueFormattingFromParagraph }

constructor TdxStateContinueFormattingFromParagraph.Create(AFormatter: TdxParagraphBoxFormatter; AParagraphIndex: TdxParagraphIndex); 
begin
  inherited Create(AFormatter);
  FParagraphIndex := AParagraphIndex;
end;

function TdxStateContinueFormattingFromParagraph.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.ContinueFromParagraph;
end;

{ TdxStateSectionBreakAfterParagraphMark }

function TdxStateSectionBreakAfterParagraphMark.ContinueFormat: TdxStateContinueFormatResult;
begin
  Assert(Iterator.CurrentChar = TdxCharacters.SectionMark);
  FinishSection;
  Result := GetContinueFormatResult(FinishSection);
end;

function TdxStateSectionBreakAfterParagraphMark.CreateSectionMarkBoxInfo: TdxBoxInfo;
begin
  Result := TdxBoxInfo.Create;
  Result.Box := Iterator.CurrentBox;
  Result.StartPos := Iterator.GetCurrentPosition;
  Result.EndPos := Result.StartPos;
  Formatter.Measurer.MeasureSectionMark(Result);
end;

function TdxStateSectionBreakAfterParagraphMark.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateSectionBreakAfterParagraphMark.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateSectionBreakAfterParagraphMark.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateSectionBreakAfterParagraphMark.FinishParagraph: TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateSectionBreakAfterParagraphMark.FinishSection: TdxCompleteFormattingResult;
var
  ABoxInfo: TdxBoxInfo;
begin
  ABoxInfo := CreateSectionMarkBoxInfo;
  try
    if RowsController.CanAddSectionBreakToPrevRow then
    begin
      RowsController.AddSectionBreakBoxToPrevRow(TdxSectionMarkBox, ABoxInfo);
      ChangeState(TdxParagraphBoxFormatterState.Final);
    end
    else
    begin
      if CurrentRow.Height = 0 then
        RowsController.UpdateCurrentRowHeight(ABoxInfo);
      ApplyParagraphMarkSize(ABoxInfo);
      FinalizeSection(ABoxInfo);
    end;
    Result := TdxCompleteFormattingResult.Success;
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

function TdxStateSectionBreakAfterParagraphMark.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.SectionBreakAfterParagraphMark
end;

{ TdxStateRowBreakBase }

function TdxStateRowBreakBase.FinishParagraph: TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowBreakBase.FinishSection: TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowBreakBase.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowBreakBase.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowBreakBase.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateRowBreakBase.IsTerminatorChar(ACh: Char): Boolean;
begin
  Result := True;
end;

{ TdxStateRowPageBreak }

function TdxStateRowPageBreak.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowPageBreak;
end;

function TdxStateRowPageBreak.ContinueFormat: TdxStateContinueFormatResult;
var
  ABoxInfo: TdxBoxInfo;
  ACurrentBox: TdxBox;
begin
  if not RowsController.TablesController.IsInsideTable then
  begin
    ABoxInfo := TdxBoxInfo.Create;
    try
      ACurrentBox := Iterator.CurrentBox;
      ABoxInfo.Box := ACurrentBox;
      ABoxInfo.StartPos := ACurrentBox.StartPos;
      ABoxInfo.EndPos := ACurrentBox.EndPos;
      ABoxInfo.Size := ABoxInfo.Box.Bounds.Size;
      PreviousState.ApplyPageBreakMarkSize(ABoxInfo);
      Iterator.Next;
      Result := GetContinueFormatResult(PreviousState.FinishPage(ABoxInfo));
    finally
      FreeAndNil(ABoxInfo);
    end;
  end
  else
  begin
    Iterator.Next;
    ChangeState(PreviousState.&Type);
    Result := TdxStateContinueFormatResult.Success;
  end;
end;

{ TdxStateRowColumnBreak }

function TdxStateRowColumnBreak.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowColumnBreak;
end;

function TdxStateRowColumnBreak.ContinueFormat: TdxStateContinueFormatResult;
var
  ABoxInfo: TdxBoxInfo;
  ACurrentBox: TdxBox;
begin
  ABoxInfo := TdxBoxInfo.Create;
  try
    ACurrentBox := Iterator.CurrentBox;
    ABoxInfo.Box := ACurrentBox;
    ABoxInfo.StartPos := ACurrentBox.StartPos;
    ABoxInfo.EndPos := ACurrentBox.EndPos;
    ABoxInfo.Size := ABoxInfo.Box.Bounds.Size;
    PreviousState.ApplyColumnBreakMarkSize(ABoxInfo);
    Iterator.Next;
    Result := GetContinueFormatResult(PreviousState.FinishColumn(ABoxInfo));
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

{ TdxStateRowLineBreak }

function TdxStateRowLineBreak.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.RowLineBreak;
end;

function TdxStateRowLineBreak.ContinueFormat: TdxStateContinueFormatResult;
var
  ABoxInfo: TdxBoxInfo;
  ACurrentBox: TdxBox;
begin
  ABoxInfo := TdxBoxInfo.Create;
  try
    ACurrentBox := Iterator.CurrentBox;
    ABoxInfo.Box := ACurrentBox;
    ABoxInfo.StartPos := ACurrentBox.StartPos;
    ABoxInfo.EndPos := ACurrentBox.EndPos;
    ABoxInfo.Size := ABoxInfo.Box.Bounds.Size;
    PreviousState.ApplyLineBreakMarkSize(ABoxInfo);
    Iterator.Next;
    Result := GetContinueFormatResult(PreviousState.FinishLine(ABoxInfo));
  finally
    FreeAndNil(ABoxInfo);
  end;
end;

{ TdxStateFinal }

function TdxStateFinal.FinishColumn(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateFinal.FinishLine(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateFinal.FinishPage(ABoxInfo: TdxBoxInfo): TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateFinal.FinishParagraph: TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateFinal.FinishSection: TdxCompleteFormattingResult;
begin
  Assert(False);
  Result := TdxCompleteFormattingResult.Success;
end;

function TdxStateFinal.GetFormattingComplete: Boolean;
begin
  Result := True;
end;

function TdxStateFinal.GetType: TdxParagraphBoxFormatterState;
begin
  Result := TdxParagraphBoxFormatterState.Final;
end;

{ TdxBoxInfoList }

function TdxBoxInfoList.GetItem(Index: Integer): TdxBoxInfo;
begin
  Result := TdxBoxInfo(inherited Items[Index]);
end;

{ TdxIntegerStack }

procedure TdxIntegerStack.Clear;
begin
  List.Clear;
end;

function TdxIntegerStack.Pop: TdxNativeInt;
begin
  Result := TdxNativeInt(inherited Pop);
end;

function TdxIntegerStack.Push(AItem: TdxNativeInt): Pointer;
begin
  Result := inherited Push(Pointer(AItem));
end;

function TdxIntegerStack.Peek: TdxNativeInt;
begin
  Result := TdxNativeInt(inherited Peek);
end;

{ TdxFloatingObjectSizeController }

constructor TdxFloatingObjectSizeController.Create(APieceTable: TdxPieceTable);
begin
  inherited Create;
  FPieceTable := APieceTable;
end;

procedure TdxFloatingObjectSizeController.UpdateFloatingObjectBox(AFloatingObjectAnchorBox: TdxFloatingObjectAnchorBox);
const
  MSWordBorderOffset = 30; 
var
  AIsTextBox: Boolean;
  AOutlineWidth, ALeftOffset, ARightOffset, ATopOffset, ABottomOffset, ALeftPosition, ARightPosition: Integer;
  ACurrentRun: TdxTextRunBase;
  ARotatedLocation: IdxFloatingObjectLocation;
  AObjectAnchorRun: TdxFloatingObjectAnchorRun;
  AFloatingObjectProperties: TdxFloatingObjectProperties;
  AConverter: TdxDocumentModelUnitToLayoutUnitConverter;
  ARotatedShapeBounds, AShapeBounds, ARect, AActualSizeBounds, ANewShapeBounds, ANewContentBounds: TRect;
begin
  ACurrentRun := PieceTable.Runs[AFloatingObjectAnchorBox.StartPos.RunIndex];
  AObjectAnchorRun := TdxFloatingObjectAnchorRun(ACurrentRun); 
  AIsTextBox := AObjectAnchorRun.Content is TdxTextBoxFloatingObjectContent;
  AFloatingObjectProperties := AObjectAnchorRun.FloatingObjectProperties;
  ARotatedLocation := CreateRotatedFloatingObjectLocation(AFloatingObjectProperties, AObjectAnchorRun.Shape.Rotation);
  ARotatedShapeBounds := CalculateAbsoluteFloatingObjectShapeBounds(ARotatedLocation, AObjectAnchorRun.Shape, AIsTextBox);
  AShapeBounds := CalculateAbsoluteFloatingObjectShapeBounds(AFloatingObjectProperties, AObjectAnchorRun.Shape, AIsTextBox);

  if AFloatingObjectProperties.HorizontalPositionAlignment <> TdxFloatingObjectHorizontalPositionAlignment.None then
    AShapeBounds := CenterRectangleHorizontally(AShapeBounds, ARotatedShapeBounds)
  else
  begin
    ARect := CenterRectangleHorizontally(ARotatedShapeBounds, AShapeBounds);
    ARotatedShapeBounds := ValidateRotatedShapeHorizontalPosition(ARect, AFloatingObjectProperties);
    AShapeBounds := CenterRectangleHorizontally(AShapeBounds, ARotatedShapeBounds);
  end;

  if AFloatingObjectProperties.VerticalPositionAlignment <> TdxFloatingObjectVerticalPositionAlignment.None then
    AShapeBounds := CenterRectangleVertically(AShapeBounds, ARotatedShapeBounds)
  else
  begin
    ARect := CenterRectangleVertically(ARotatedShapeBounds, AShapeBounds);
    ARotatedShapeBounds := ValidateRotatedShapeVerticalPosition(ARect, AFloatingObjectProperties);
    AShapeBounds := CenterRectangleVertically(AShapeBounds, ARotatedShapeBounds);
  end;

  AFloatingObjectAnchorBox.ShapeBounds := AShapeBounds;
  AFloatingObjectAnchorBox.ContentBounds := CalculateAbsoluteFloatingObjectContentBounds(AFloatingObjectProperties, AObjectAnchorRun.Shape, AFloatingObjectAnchorBox.ShapeBounds);
  AFloatingObjectAnchorBox.ActualBounds := CalculateActualAbsoluteFloatingObjectBounds(AFloatingObjectProperties, ARotatedShapeBounds);
  AConverter := DocumentModel.ToDocumentLayoutUnitConverter;
  if ACurrentRun.Paragraph.FrameProperties <> nil then
  begin
      ALeftOffset := 0;
      ARightOffset := 0;
      ATopOffset := 0;
      ABottomOffset := 0;
    NotImplemented;

    if ALeftOffset <> 0 then
      ALeftPosition := AConverter.ToLayoutUnits(MSWordBorderOffset)
    else
      ALeftPosition := 0;
    if ARightOffset <> 0 then
      ARightPosition := AConverter.ToLayoutUnits(MSWordBorderOffset)
    else
      ARightPosition := 0;

    ANewShapeBounds := AFloatingObjectAnchorBox.ShapeBounds;
    AFloatingObjectAnchorBox.ShapeBounds := TRect.CreateSize(ANewShapeBounds.Left - ALeftPosition, ANewShapeBounds.Top,
      ANewShapeBounds.Width + ALeftOffset + ARightOffset + ALeftPosition + ARightPosition, ANewShapeBounds.Height + ATopOffset + ABottomOffset);

    ANewContentBounds := AFloatingObjectAnchorBox.ContentBounds;
    AFloatingObjectAnchorBox.ContentBounds := TRect.CreateSize(ANewContentBounds.Left + ALeftOffset,
      ANewContentBounds.Top + ATopOffset, ANewContentBounds.Width, ANewContentBounds.Height - ATopOffset - ABottomOffset);
  end;
  if AIsTextBox then
  begin
    AActualSizeBounds := AFloatingObjectAnchorBox.ShapeBounds;
    AOutlineWidth := AConverter.ToLayoutUnits(AObjectAnchorRun.Shape.OutlineWidth);
    AActualSizeBounds.SetLocation(AActualSizeBounds.Left, Trunc(AActualSizeBounds.Top + AOutlineWidth / 2));
    AActualSizeBounds.Height := AActualSizeBounds.Height - AOutlineWidth;
    AFloatingObjectAnchorBox.SetActualSizeBounds(AActualSizeBounds);
  end
  else
    AFloatingObjectAnchorBox.SetActualSizeBounds(AFloatingObjectAnchorBox.ContentBounds);
end;

function TdxFloatingObjectSizeController.CreateRotatedFloatingObjectLocation(AProperties: IdxFloatingObjectLocation;
  AAngle: Integer): IdxFloatingObjectLocation;
var
  AActualSize: TSize;
  AMatrix: TdxTransformMatrix;
  AResult: TdxExplicitFloatingObjectLocation;
  ABoundingRectangle, ARelativeBoundingRectangle: TRect;
  AFloatingObjectProperties: TdxFloatingObjectProperties;
begin
  AMatrix := TdxTransformMatrix.Create;
  try
    AMatrix.Rotate(DocumentModel.UnitConverter.ModelUnitsToDegreeF(-AAngle));
    if AProperties is TdxFloatingObjectProperties then
    begin
      AFloatingObjectProperties := TdxFloatingObjectProperties(AProperties);
      AActualSize := AFloatingObjectProperties.ActualSize;
    end
    else
      AFloatingObjectProperties := nil;
    ABoundingRectangle := TdxRectangleUtils.BoundingRectangle(TRect.CreateSize(AActualSize), AMatrix);
    ARelativeBoundingRectangle := TdxRectangleUtils.BoundingRectangle(TRect.CreateSize(
      cxSize(AProperties.RelativeWidth.Width, AProperties.RelativeHeight.Height)), AMatrix);
  finally
    AMatrix.Free;
  end;
  AResult := TdxExplicitFloatingObjectLocation.Create;
  AResult.ActualWidth := ABoundingRectangle.Width;
  AResult.ActualHeight := ABoundingRectangle.Height;
  AResult.OffsetX := AProperties.OffsetX;
  AResult.OffsetY := AProperties.OffsetY;
  AResult.HorizontalPositionAlignment := AProperties.HorizontalPositionAlignment;
  AResult.VerticalPositionAlignment := AProperties.VerticalPositionAlignment;
  AResult.HorizontalPositionType := AProperties.HorizontalPositionType;
  AResult.VerticalPositionType := AProperties.VerticalPositionType;
  if AProperties.UseRelativeWidth then
  begin
    AResult.UseRelativeWidth := True;
    AResult.RelativeWidth := TdxFloatingObjectRelativeWidth.Create(AProperties.RelativeWidth.From,
      ARelativeBoundingRectangle.Width);
  end;
  if AProperties.UseRelativeHeight then
  begin
    AResult.UseRelativeHeight := True;
    AResult.RelativeHeight := TdxFloatingObjectRelativeHeight.Create(AProperties.RelativeHeight.From,
      ARelativeBoundingRectangle.Height);
  end;
  if (AFloatingObjectProperties <> nil) and AFloatingObjectProperties.UsePercentOffset then
  begin
    AResult.PercentOffsetX := AProperties.PercentOffsetX;
    AResult.PercentOffsetY := AProperties.PercentOffsetY;
  end;
  Result := AResult;
end;

function TdxFloatingObjectSizeController.CalculateAbsoluteFloatingObjectShapeBounds(ALocation: IdxFloatingObjectLocation; AShape: TdxShape;
  AIsTextBox: Boolean): TRect;
var
  ASize: TSize;
  ABounds: TRect;
  APosition: TPoint;
  AOutlineWidth, AWidth, AHeight: Integer;
  AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
begin
  AUnitConverter := DocumentModel.ToDocumentLayoutUnitConverter;
  ABounds := CalculateAbsoluteFloatingObjectBounds(ALocation);
  APosition := ABounds.Location;
  ASize := ABounds.Size;
  if not TdxColor.IsTransparentOrEmpty(AShape.OutlineColor) then 
    if AIsTextBox then
      ASize.Height := ASize.Height + AShape.OutlineWidth
    else
    begin
      AOutlineWidth := AUnitConverter.ToLayoutUnits(AShape.OutlineWidth);
      APosition.X := APosition.X - AOutlineWidth;
      APosition.Y := APosition.Y - AOutlineWidth;
      ASize.Width := ASize.Width + 2 * AShape.OutlineWidth;
      ASize.Height := ASize.Height + 2 * AShape.OutlineWidth;
    end;
  AWidth := AUnitConverter.ToLayoutUnits(ASize.Width);
  AHeight := AUnitConverter.ToLayoutUnits(ASize.Height);
  Result := TRect.CreateSize(APosition, AWidth, AHeight);
end;

function TdxFloatingObjectSizeController.CenterRectangleHorizontally(const ARectangle, AWhere: TRect): TRect;
begin
  Result := ARectangle;
  Result.SetLocation(Trunc(AWhere.Left + (AWhere.Width - ARectangle.Width) / 2), ARectangle.Top);
end;

function TdxFloatingObjectSizeController.CalculateAbsoluteFloatingObjectContentBounds(AFloatingObjectProperties: IdxFloatingObjectLocation;
  AShape: TdxShape; const AShapeBounds: TRect): TRect;
var
  AWidth: Integer;
  AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
begin
  if TdxColor.IsTransparentOrEmpty(AShape.OutlineColor) then 
    Exit(AShapeBounds);

  AUnitConverter := DocumentModel.ToDocumentLayoutUnitConverter;
  AWidth := AUnitConverter.ToLayoutUnits(AShape.OutlineWidth);
  Result := AShapeBounds;
  Result.SetLocation(Result.Left + AWidth, Result.Top + AWidth);
  Result.Width := Result.Width - 2 * AWidth;
  Result.Height := Result.Height - 2 * AWidth;
end;

function TdxFloatingObjectSizeController.CalculateActualAbsoluteFloatingObjectBounds(
  AFloatingObjectProperties: TdxFloatingObjectProperties; const ABounds: TRect): TRect;
var
  AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter;
begin
  AUnitConverter := DocumentModel.ToDocumentLayoutUnitConverter;

  Result := ABounds;
  Result.SetLocation(
    Result.Left - AUnitConverter.ToLayoutUnits(AFloatingObjectProperties.LeftDistance),
    Result.Top - AUnitConverter.ToLayoutUnits(AFloatingObjectProperties.TopDistance));
  Result.Width := Result.Width + AUnitConverter.ToLayoutUnits(AFloatingObjectProperties.LeftDistance + AFloatingObjectProperties.RightDistance);
  Result.Height := Result.Height + AUnitConverter.ToLayoutUnits(AFloatingObjectProperties.TopDistance + AFloatingObjectProperties.BottomDistance);
end;

function TdxFloatingObjectSizeController.CenterRectangleVertically(const ARectangle, AWhere: TRect): TRect;
begin
  Result := ARectangle;
  Result.SetLocation(AWhere.Left ,Trunc(AWhere.Top + (AWhere.Height - ARectangle.Height) / 2));
end;

function TdxFloatingObjectSizeController.CalculateAbsoluteFloatingObjectBounds(
  ALocation: IdxFloatingObjectLocation): TRect;
begin
  Result := TRect.CreateSize(cxSize(ALocation.ActualWidth, ALocation.ActualHeight));
end;

function TdxFloatingObjectSizeController.ValidateRotatedShapeHorizontalPosition(const ARect: TRect;
  AProperties: TdxFloatingObjectProperties): TRect;
begin
  Result := ARect;
end;

function TdxFloatingObjectSizeController.ValidateRotatedShapeVerticalPosition(const ARect: TRect;
  AProperties: TdxFloatingObjectProperties): TRect;
begin
  Result := ARect;
end;

function TdxFloatingObjectSizeController.GetDocumentModel: TdxDocumentModel;
begin
  Result := PieceTable.DocumentModel;
end;

{ TdxFloatingObjectSizeAndPositionController }

constructor TdxFloatingObjectSizeAndPositionController.Create(ARowsController: TdxRowsController); 
begin
  inherited Create(ARowsController.PieceTable);
  FRowsController := ARowsController;
end;

function TdxFloatingObjectSizeAndPositionController.GetColumnController: IColumnController;
begin
  Result := RowsController.ColumnController;
end;

function TdxFloatingObjectSizeAndPositionController.GetCurrentColumn: TdxColumn;
begin
  Result := RowsController.CurrentColumn;
end;

function TdxFloatingObjectSizeAndPositionController.GetCurrentHorizontalPosition: Integer;
begin
  Result := RowsController.CurrentHorizontalPosition;
end;

function TdxFloatingObjectSizeAndPositionController.GetCurrentRow: TdxRow;
begin
  Result := RowsController.CurrentRow;
end;

function TdxFloatingObjectSizeAndPositionController.GetHorizontalPositionController: TdxCurrentHorizontalPositionController;
begin
  Result := RowsController.HorizontalPositionController;
end;

function TdxFloatingObjectSizeAndPositionController.CalculateAbsoluteFloatingObjectBounds(ALocation: IdxFloatingObjectLocation): TRect;
var
  ASz: TSize;
  ALeft, ATop: Integer;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo;
begin
  APlacementInfo := CalculatePlacementInfo;
  try
    ASz := cxSize(CalculateAbsoluteFloatingObjectWidth(ALocation, APlacementInfo),
      CalculateAbsoluteFloatingObjectHeight(ALocation, APlacementInfo));
    ALeft := CalculateAbsoluteFloatingObjectX(ALocation, APlacementInfo, ASz.cx);
    ATop := CalculateAbsoluteFloatingObjectY(ALocation, APlacementInfo, ASz.cy);
    Result := TRect.CreateSize(ALeft, ATop, ASz);
  finally
    APlacementInfo.Free;
  end;
end;

function TdxFloatingObjectSizeAndPositionController.CalculateAbsoluteFloatingObjectWidth(
  ALocation: IdxFloatingObjectLocation; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
var
  ACalculator: TdxFloatingObjectHorizontalPositionCalculator;
begin
  ACalculator := TdxFloatingObjectHorizontalPositionCalculator.Create(DocumentModel.ToDocumentLayoutUnitConverter);
  try
    Result := ACalculator.CalculateAbsoluteFloatingObjectWidth(ALocation, APlacementInfo);
  finally
    ACalculator.Free;
  end;
end;

function TdxFloatingObjectSizeAndPositionController.CalculateAbsoluteFloatingObjectHeight(ALocation: IdxFloatingObjectLocation;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
var
  ACalculator: TdxFloatingObjectVerticalPositionCalculator;
begin
  ACalculator := TdxFloatingObjectVerticalPositionCalculator.Create(DocumentModel.ToDocumentLayoutUnitConverter);
  try
    Result := ACalculator.CalculateAbsoluteFloatingObjectHeight(ALocation, APlacementInfo);
  finally
    ACalculator.Free;
  end;
end;

function TdxFloatingObjectSizeAndPositionController.ValidateRotatedShapeHorizontalPosition(const AShapeBounds: TRect;
  AProperties: TdxFloatingObjectProperties): TRect;
var
  AOverflow: Integer;
  APageBounds: TRect;
begin
  if (AProperties.TextWrapType = TdxFloatingObjectTextWrapType.None) and AProperties.UseTextWrapType then
    Exit(AShapeBounds);

  APageBounds := CalculatePlacementInfo.PageBounds;
  AOverflow := AShapeBounds.Left + AShapeBounds.Width - APageBounds.Right;
  if AOverflow > 0 then
    AShapeBounds.SetLocation(AShapeBounds.Left - AOverflow, AShapeBounds.Top);

  AShapeBounds.SetLocation(Math.Max(APageBounds.Left, AShapeBounds.Left), AShapeBounds.Top);
  Result := AShapeBounds;
end;

function TdxFloatingObjectSizeAndPositionController.ValidateRotatedShapeVerticalPosition(const AShapeBounds: TRect;
  AProperties: TdxFloatingObjectProperties): TRect;
var
  APageBounds: TRect;
  AHeight, ABottom: Integer;
begin
  if (AProperties.TextWrapType = TdxFloatingObjectTextWrapType.None) and AProperties.UseTextWrapType then
    Exit(AShapeBounds);

  AHeight := AShapeBounds.Height;
  ABottom := AShapeBounds.Top + AHeight;
  APageBounds := CalculatePlacementInfo.PageBounds;
  if ABottom >= APageBounds.Bottom then
    AShapeBounds.SetLocation(AShapeBounds.Left, AShapeBounds.Top - ABottom - APageBounds.Bottom);
  if AShapeBounds.Top < APageBounds.Top then
    AShapeBounds.SetLocation(AShapeBounds.Left, APageBounds.Top);
  Result := AShapeBounds;
end;

function TdxFloatingObjectSizeAndPositionController.CalculatePlacementInfo: TdxFloatingObjectTargetPlacementInfo;
var
 APage: TdxPage;
begin
  Result := TdxFloatingObjectTargetPlacementInfo.Create;
  APage := ColumnController.PageAreaController.PageController.Pages.Last;
  Result.PageBounds := ColumnController.GetCurrentPageBounds(APage, CurrentColumn);
  Result.PageClientBounds := ColumnController.GetCurrentPageClientBounds(APage, CurrentColumn);
  Result.ColumnBounds := HorizontalPositionController.CalculateFloatingObjectColumnBounds(CurrentColumn);
  Result.OriginalColumnBounds := CurrentColumn.Bounds;
  Result.OriginX := CurrentHorizontalPosition;
  Result.OriginY := CurrentRow.Bounds.Top;
end;

function TdxFloatingObjectSizeAndPositionController.CalculateAbsoluteFloatingObjectX(ALocation: IdxFloatingObjectLocation;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo; AActualWidth: Integer): Integer;
var
  ACalculator: TdxFloatingObjectHorizontalPositionCalculator;
begin
  ACalculator := TdxFloatingObjectHorizontalPositionCalculator.Create(DocumentModel.ToDocumentLayoutUnitConverter);
  try
    Result := ACalculator.CalculateAbsoluteFloatingObjectX(ALocation, APlacementInfo, AActualWidth);
  finally
    ACalculator.Free;
  end;
end;

function TdxFloatingObjectSizeAndPositionController.CalculateAbsoluteFloatingObjectY(ALocation: IdxFloatingObjectLocation;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo; AActualHeight: Integer): Integer;
var
  ACalculator: TdxFloatingObjectVerticalPositionCalculator;
begin
  ACalculator := TdxFloatingObjectVerticalPositionCalculator.Create(DocumentModel.ToDocumentLayoutUnitConverter);
  try
    Result := ACalculator.CalculateAbsoluteFloatingObjectY(ALocation, APlacementInfo, AActualHeight);
  finally
    ACalculator.Free;
  end;
end;

{ TdxFloatingObjectTargetPlacementInfo }

procedure TdxFloatingObjectTargetPlacementInfo.SetColumnBounds(const Value: TRect);
begin
  FColumnBounds := Value;
  FOriginalColumnBounds := Value;
end;

{ TdxFloatingObjectHorizontalPositionCalculator }

function TdxFloatingObjectHorizontalPositionCalculator.CalculateAbsoluteFloatingObjectHorizontalAlignmentPosition(
  AAlignment: TdxFloatingObjectHorizontalPositionAlignment; AAlignBounds: TRect; AActualWidth: Integer): Integer;
begin
  case AAlignment of
    TdxFloatingObjectHorizontalPositionAlignment.Inside,
    TdxFloatingObjectHorizontalPositionAlignment.Left:
      Result := AAlignBounds.Left;
    TdxFloatingObjectHorizontalPositionAlignment.Outside,
    TdxFloatingObjectHorizontalPositionAlignment.Right:
      Result := AAlignBounds.Right - FUnitConverter.ToLayoutUnits(AActualWidth);
    TdxFloatingObjectHorizontalPositionAlignment.Center:
      Result := Trunc((AAlignBounds.Right + AAlignBounds.Left) / 2 - FUnitConverter.ToLayoutUnits(AActualWidth) / 2);
    else
      raise Exception.Create('');
  end;
end;

function TdxFloatingObjectHorizontalPositionCalculator.CalculateAbsoluteFloatingObjectWidth(
  ALocation: IdxFloatingObjectLocation; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  if not ALocation.UseRelativeWidth or (ALocation.RelativeWidth.Width = 0) then
    Result := ALocation.ActualWidth
  else
    Result := CalculateAbsoluteFloatingObjectWidthCore(ALocation, APlacementInfo);
end;

function TdxFloatingObjectHorizontalPositionCalculator.CalculateAbsoluteFloatingObjectWidthCore(
  ALocation: IdxFloatingObjectLocation; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
var
  ARelativeWidth: TdxFloatingObjectRelativeWidth;
begin
  ARelativeWidth := ALocation.RelativeWidth;
  Result := Trunc(ARelativeWidth.Width / 100000 * GetPercentBaseWidth(ARelativeWidth.From, APlacementInfo));
end;

function TdxFloatingObjectHorizontalPositionCalculator.CalculateAbsoluteFloatingObjectX(
  ALocation: IdxFloatingObjectLocation; APlacementInfo: TdxFloatingObjectTargetPlacementInfo;
  AActualWidth: Integer): Integer;
var
  AAlignBounds: TRect;
  AOffsetX, APercentBaseWidth: Integer;
begin
  if ALocation.HorizontalPositionAlignment = TdxFloatingObjectHorizontalPositionAlignment.None then
  begin
    AOffsetX := ALocation.OffsetX;
    if ALocation.PercentOffsetX <> 0 then
    begin
      APercentBaseWidth := CalculateFloatingObjectOffsetPercentBase(ALocation.HorizontalPositionType, APlacementInfo);
      AOffsetX := Trunc(ALocation.PercentOffsetX / 100000 * APercentBaseWidth);
    end;
    Result := CalculateAbsoluteFloatingObjectXCore(ALocation.HorizontalPositionType, AOffsetX, APlacementInfo);
  end
  else
  begin
    AAlignBounds := CalculateAlignBounds(ALocation, APlacementInfo);
    Result := CalculateAbsoluteFloatingObjectHorizontalAlignmentPosition(ALocation.HorizontalPositionAlignment,
      AAlignBounds, AActualWidth);
  end;
end;

function TdxFloatingObjectHorizontalPositionCalculator.CalculateAbsoluteFloatingObjectXCore(
  AHorizontalPositionType: TdxFloatingObjectHorizontalPositionType; AOffsetX: Integer;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AHorizontalPositionType of
    TdxFloatingObjectHorizontalPositionType.LeftMargin,
    TdxFloatingObjectHorizontalPositionType.InsideMargin,
    TdxFloatingObjectHorizontalPositionType.Page:
      Result := FUnitConverter.ToLayoutUnits(AOffsetX) + APlacementInfo.PageBounds.Left;
    TdxFloatingObjectHorizontalPositionType.Column:
      Result := FUnitConverter.ToLayoutUnits(AOffsetX) + APlacementInfo.ColumnBounds.Left;
    TdxFloatingObjectHorizontalPositionType.Margin:
      Result := FUnitConverter.ToLayoutUnits(AOffsetX) + APlacementInfo.PageClientBounds.Left;
    TdxFloatingObjectHorizontalPositionType.OutsideMargin,
    TdxFloatingObjectHorizontalPositionType.RightMargin:
      Result := FUnitConverter.ToLayoutUnits(AOffsetX) + APlacementInfo.PageClientBounds.Right;
    TdxFloatingObjectHorizontalPositionType.Character:
      Result := FUnitConverter.ToLayoutUnits(AOffsetX) + APlacementInfo.OriginX;
    else
      raise Exception.Create('');
  end;
end;

function TdxFloatingObjectHorizontalPositionCalculator.CalculateAlignBounds(ALocation: IdxFloatingObjectLocation;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): TRect;
var
  APageBounds, APageClientBounds: TRect;
begin
  case ALocation.HorizontalPositionType of
    TdxFloatingObjectHorizontalPositionType.LeftMargin,
    TdxFloatingObjectHorizontalPositionType.InsideMargin:
      begin
        APageBounds := APlacementInfo.PageBounds;
        Result := TRect.CreateSize(APageBounds.Left, APageBounds.Top, APlacementInfo.PageClientBounds.Left - APageBounds.Left, APageBounds.Height);
      end;
    TdxFloatingObjectHorizontalPositionType.Page:
        Result := APlacementInfo.PageBounds;
    TdxFloatingObjectHorizontalPositionType.Column:
        Result := APlacementInfo.OriginalColumnBounds;
    TdxFloatingObjectHorizontalPositionType.OutsideMargin,
    TdxFloatingObjectHorizontalPositionType.RightMargin:
      begin
        APageBounds := APlacementInfo.PageBounds;
        APageClientBounds := APlacementInfo.PageClientBounds;
        Result := TRect.CreateSize(APageClientBounds.Right, APageBounds.Top, APageBounds.Right - APageClientBounds.Right, APageBounds.Height);
      end;
    TdxFloatingObjectHorizontalPositionType.Margin:
        Result := APlacementInfo.PageClientBounds;
    TdxFloatingObjectHorizontalPositionType.Character:
        Result := Rect(APlacementInfo.OriginX, 0, APlacementInfo.OriginX, 0);
    else
      raise Exception.Create('');
  end;
end;

function TdxFloatingObjectHorizontalPositionCalculator.CalculateFloatingObjectOffsetPercentBase(
  AType: TdxFloatingObjectHorizontalPositionType; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AType of
    TdxFloatingObjectHorizontalPositionType.Page:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Width);
    TdxFloatingObjectHorizontalPositionType.Margin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Width);
    TdxFloatingObjectHorizontalPositionType.OutsideMargin, TdxFloatingObjectHorizontalPositionType.LeftMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Left - APlacementInfo.PageBounds.Left);
    TdxFloatingObjectHorizontalPositionType.InsideMargin, TdxFloatingObjectHorizontalPositionType.RightMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Right - APlacementInfo.PageBounds.Right);
    else
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Width);
  end;
end;

function TdxFloatingObjectHorizontalPositionCalculator.CalculateFloatingObjectOffsetX(
  AHorizontalPositionType: TdxFloatingObjectHorizontalPositionType; X: Integer;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AHorizontalPositionType of
    TdxFloatingObjectHorizontalPositionType.LeftMargin,
    TdxFloatingObjectHorizontalPositionType.InsideMargin,
    TdxFloatingObjectHorizontalPositionType.Page:
      Result := FUnitConverter.ToModelUnits(X - APlacementInfo.PageBounds.Left);
    TdxFloatingObjectHorizontalPositionType.Column:
      Result := FUnitConverter.ToModelUnits(X - APlacementInfo.ColumnBounds.Left);
    TdxFloatingObjectHorizontalPositionType.Margin:
      Result := FUnitConverter.ToModelUnits(X - APlacementInfo.PageClientBounds.Left);
    TdxFloatingObjectHorizontalPositionType.OutsideMargin,
    TdxFloatingObjectHorizontalPositionType.RightMargin:
      Result := FUnitConverter.ToModelUnits(X - APlacementInfo.PageClientBounds.Right);
    TdxFloatingObjectHorizontalPositionType.Character:
      Result := FUnitConverter.ToModelUnits(X - APlacementInfo.OriginX);
    else
      raise Exception.Create('');
  end;
end;

constructor TdxFloatingObjectHorizontalPositionCalculator.Create(
  AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter);
begin
  inherited Create;
  FUnitConverter := AUnitConverter;
end;

function TdxFloatingObjectHorizontalPositionCalculator.GetPercentBaseWidth(
  AFrom: TdxFloatingObjectRelativeFromHorizontal; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AFrom of
    TdxFloatingObjectRelativeFromHorizontal.Page:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Width);
    TdxFloatingObjectRelativeFromHorizontal.Margin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Width);
    TdxFloatingObjectRelativeFromHorizontal.OutsideMargin, TdxFloatingObjectRelativeFromHorizontal.LeftMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Left - APlacementInfo.PageBounds.Left);
    TdxFloatingObjectRelativeFromHorizontal.InsideMargin, TdxFloatingObjectRelativeFromHorizontal.RightMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Right - APlacementInfo.PageBounds.Right);
    else
      raise Exception.Create('');
  end;
end;


{ TdxFloatingObjectVerticalPositionCalculator }

function TdxFloatingObjectVerticalPositionCalculator.CalculateAbsoluteFloatingObjectHeight(
  ALocation: IdxFloatingObjectLocation; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  if not ALocation.UseRelativeHeight or (ALocation.RelativeHeight.Height = 0) then
    Result := ALocation.ActualHeight
  else
    Result := CalculateAbsoluteFloatingObjectHeightCore(ALocation, APlacementInfo);
end;

function TdxFloatingObjectVerticalPositionCalculator.CalculateAbsoluteFloatingObjectHeightCore(
  ALocation: IdxFloatingObjectLocation; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
var
  ARelativeHeight: TdxFloatingObjectRelativeHeight;
begin
  ARelativeHeight := ALocation.RelativeHeight;
  Result :=  Trunc(ARelativeHeight.Height / 100000.0 * GetPercentBaseHeight(ARelativeHeight.From, APlacementInfo));
end;

function TdxFloatingObjectVerticalPositionCalculator.CalculateAbsoluteFloatingObjectVerticalAlignmentPosition(
  AAlignment: TdxFloatingObjectVerticalPositionAlignment; const AAlignBounds: TRect; AActualHeight: Integer): Integer;
begin
  case AAlignment of
    TdxFloatingObjectVerticalPositionAlignment.Inside,
    TdxFloatingObjectVerticalPositionAlignment.Top:
      Result := AAlignBounds.Top;
    TdxFloatingObjectVerticalPositionAlignment.Outside,
    TdxFloatingObjectVerticalPositionAlignment.Bottom:
      Result := AAlignBounds.Bottom - FUnitConverter.ToLayoutUnits(AActualHeight);
    TdxFloatingObjectVerticalPositionAlignment.Center:
      Result := Trunc((AAlignBounds.Bottom + AAlignBounds.Top) / 2 - FUnitConverter.ToLayoutUnits(AActualHeight) / 2);
    else
      raise Exception.Create('');
  end;
end;

function TdxFloatingObjectVerticalPositionCalculator.CalculateAbsoluteFloatingObjectY(
  ALocation: IdxFloatingObjectLocation; APlacementInfo: TdxFloatingObjectTargetPlacementInfo;
  AActualHeight: Integer): Integer;
var
  AAlignBounds: TRect;
  AOffsetY, APercentBaseWidth: Integer;
begin
  if ALocation.VerticalPositionAlignment = TdxFloatingObjectVerticalPositionAlignment.None then
  begin
    AOffsetY := ALocation.OffsetY;
    if ALocation.PercentOffsetY <> 0 then
    begin
      APercentBaseWidth := CalculateFloatingObjectOffsetPercentBase(ALocation.VerticalPositionType, APlacementInfo);
      AOffsetY := Trunc(ALocation.PercentOffsetY / 100000 * APercentBaseWidth);
    end;
    Result := CalculateAbsoluteFloatingObjectYCore(ALocation.VerticalPositionType, AOffsetY, APlacementInfo);
  end
  else
  begin
    AAlignBounds := CalculateAlignBounds(ALocation, APlacementInfo);
    Result := CalculateAbsoluteFloatingObjectVerticalAlignmentPosition(ALocation.VerticalPositionAlignment,
      AAlignBounds, AActualHeight);
  end;
end;

function TdxFloatingObjectVerticalPositionCalculator.CalculateAbsoluteFloatingObjectYCore(
  AType: TdxFloatingObjectVerticalPositionType; AOffsetY: Integer;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AType of
    TdxFloatingObjectVerticalPositionType.Paragraph,
    TdxFloatingObjectVerticalPositionType.Line:
      Result := APlacementInfo.OriginY + FUnitConverter.ToLayoutUnits(AOffsetY);
    TdxFloatingObjectVerticalPositionType.Page,
    TdxFloatingObjectVerticalPositionType.OutsideMargin,
    TdxFloatingObjectVerticalPositionType.InsideMargin,
    TdxFloatingObjectVerticalPositionType.TopMargin:
      Result := APlacementInfo.PageBounds.Top + FUnitConverter.ToLayoutUnits(AOffsetY);
    TdxFloatingObjectVerticalPositionType.BottomMargin:
      Result := APlacementInfo.PageBounds.Bottom + FUnitConverter.ToLayoutUnits(AOffsetY);
    TdxFloatingObjectVerticalPositionType.Margin:
      Result := APlacementInfo.ColumnBounds.Top + FUnitConverter.ToLayoutUnits(AOffsetY);
    else
      raise Exception.Create('');
  end;
end;

function TdxFloatingObjectVerticalPositionCalculator.CalculateAlignBounds(ALocation: IdxFloatingObjectLocation;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): TRect;
var
  APageBounds, APageClientBounds: TRect;
begin
  case ALocation.VerticalPositionType of
    TdxFloatingObjectVerticalPositionType.TopMargin,
    TdxFloatingObjectVerticalPositionType.OutsideMargin,
    TdxFloatingObjectVerticalPositionType.InsideMargin:
      begin
        APageBounds := APlacementInfo.PageBounds;
        APageClientBounds := APlacementInfo.PageClientBounds;
        Result := TRect.CreateSize(APageBounds.Left, APageBounds.Top, APageBounds.Width,
          APageClientBounds.Top - APageBounds.Top);
      end;
    TdxFloatingObjectVerticalPositionType.Page:
      Result := APlacementInfo.PageBounds;
    TdxFloatingObjectVerticalPositionType.BottomMargin:
      begin
        APageBounds := APlacementInfo.PageBounds;
        APageClientBounds := APlacementInfo.PageClientBounds;
        Result := TRect.CreateSize(APageBounds.Left, APageClientBounds.Bottom, APageBounds.Width,
          APageBounds.Bottom - APageClientBounds.Bottom);
      end;
    TdxFloatingObjectVerticalPositionType.Margin:
      Result := APlacementInfo.PageClientBounds;
    TdxFloatingObjectVerticalPositionType.Line,
    TdxFloatingObjectVerticalPositionType.Paragraph:
      Result := TRect.CreateSize(0, APlacementInfo.OriginY, 0, 0);
    else
      raise Exception.Create('');
  end;
end;

function TdxFloatingObjectVerticalPositionCalculator.CalculateFloatingObjectOffsetPercentBase(
  AType: TdxFloatingObjectVerticalPositionType; APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AType of
    TdxFloatingObjectVerticalPositionType.Page:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Height);
    TdxFloatingObjectVerticalPositionType.Margin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Height);
    TdxFloatingObjectVerticalPositionType.OutsideMargin,
      TdxFloatingObjectVerticalPositionType.InsideMargin,
      TdxFloatingObjectVerticalPositionType.TopMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Top - APlacementInfo.PageBounds.Top);
    TdxFloatingObjectVerticalPositionType.BottomMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Bottom - APlacementInfo.PageBounds.Bottom);
    else
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Height);
  end;
end;

function TdxFloatingObjectVerticalPositionCalculator.CalculateFloatingObjectOffsetY(
  AVerticalPositionType: TdxFloatingObjectVerticalPositionType; Y: Integer;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AVerticalPositionType of
    TdxFloatingObjectVerticalPositionType.Paragraph,
    TdxFloatingObjectVerticalPositionType.Line:
      Result := FUnitConverter.ToModelUnits(Y - APlacementInfo.OriginY);
    TdxFloatingObjectVerticalPositionType.Page,
    TdxFloatingObjectVerticalPositionType.OutsideMargin,
    TdxFloatingObjectVerticalPositionType.InsideMargin,
    TdxFloatingObjectVerticalPositionType.TopMargin:
      Result := FUnitConverter.ToModelUnits(Y - APlacementInfo.PageBounds.Top);
    TdxFloatingObjectVerticalPositionType.BottomMargin:
      Result := FUnitConverter.ToModelUnits(Y - APlacementInfo.PageBounds.Bottom);
    TdxFloatingObjectVerticalPositionType.Margin:
      Result := FUnitConverter.ToModelUnits(Y - APlacementInfo.ColumnBounds.Top);
    else
      raise Exception.Create('');
  end;
end;

constructor TdxFloatingObjectVerticalPositionCalculator.Create(
  AUnitConverter: TdxDocumentModelUnitToLayoutUnitConverter);
begin
  inherited Create;
  FUnitConverter := AUnitConverter;
end;

function TdxFloatingObjectVerticalPositionCalculator.GetPercentBaseHeight(AFrom: TdxFloatingObjectRelativeFromVertical;
  APlacementInfo: TdxFloatingObjectTargetPlacementInfo): Integer;
begin
  case AFrom of
    TdxFloatingObjectRelativeFromVertical.Page:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Height);
    TdxFloatingObjectRelativeFromVertical.Margin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Height);
    TdxFloatingObjectRelativeFromVertical.OutsideMargin,
      TdxFloatingObjectRelativeFromVertical.InsideMargin,
      TdxFloatingObjectRelativeFromVertical.TopMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageClientBounds.Top - APlacementInfo.PageBounds.Top);
    TdxFloatingObjectRelativeFromVertical.BottomMargin:
      Result := FUnitConverter.ToModelUnits(APlacementInfo.PageBounds.Bottom - APlacementInfo.PageBounds.Bottom);
    else
      raise Exception.Create('');
  end;
end;

function TdxFloatingObjectVerticalPositionCalculator.ValidateY(Y, AActualHeight: Integer;
  const ATargetBounds: TRect): Integer;
var
  AHeight, ABottom: Integer;
begin
  AHeight := FUnitConverter.ToLayoutUnits(AActualHeight);
  ABottom := Y + AHeight;
  if ABottom >= ATargetBounds.Bottom then
    Y := Y - ABottom - ATargetBounds.Bottom;
  if Y < ATargetBounds.Top then
    Y := ATargetBounds.Top;
  Result := Y;
end;

{ TdxFloatingObjectsCurrentHorizontalPositionController }

function TdxFloatingObjectsCurrentHorizontalPositionController.AdvanceHorizontalPositionToNextTextArea: Boolean;
begin
  if FCurrentTextAreaIndex + 1 >= FTextAreas.Count then
    Exit(False);
  Inc(FCurrentTextAreaIndex);
  InnerCurrentHorizontalPosition := FTextAreas[FCurrentTextAreaIndex].Start;
  Result := True;
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.AppendRowBoxRange(ABoxRanges: TdxRowBoxRangeCollection;
  AFirstBoxIndex, ALastBoxIndex, ATextAreaIndex: Integer);
var
  ACurrentRow: TdxRow;
  ALeft, ARight: Integer;
  ACurrentRowBounds, ABounds : TRect;
begin
  ACurrentRow := RowsController.CurrentRow;

  if ATextAreaIndex < 0 then
    ATextAreaIndex := Min(FTextAreas.Count - 1, not ATextAreaIndex);

  ALeft := FTextAreas[ATextAreaIndex].Start;
  ARight := FTextAreas[ATextAreaIndex].&End;
  ACurrentRowBounds := ACurrentRow.Bounds;
  ABounds := TRect.CreateSize(ALeft, ACurrentRowBounds.Top, ARight - ALeft, ACurrentRowBounds.Height);
  ABoxRanges.Add(TdxRowBoxRange.Create(AFirstBoxIndex, ALastBoxIndex, ABounds));
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.AreTextAreasChanged(APrevTextAreas,
  ATextAreas: TList<TdxTextArea>): Boolean;
var
  I, ACount: Integer;
begin
  if APrevTextAreas = nil then
    Exit(True);

  ACount := ATextAreas.Count;
  if ACount <> APrevTextAreas.Count then
    Exit(True);
  for I := 0 to ACount - 1 do
    if (APrevTextAreas[I].Start <> ATextAreas[I].Start) or (APrevTextAreas[I].&End <> ATextAreas[I].&End) then
      Exit(True);

  Result := False;
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.CalculateBoxBounds(ABoxInfo: TdxBoxInfo): TRect;
var
  I: Integer;
begin
  if FTextAreas.Count <= 0 then
  begin
    CanFitCurrentRowToColumn(ABoxInfo.Size.cy);
    RowsController.UpdateCurrentRowHeight(ABoxInfo);
    if FTextAreas.Count <= 0 then
      Exit(inherited CalculateBoxBounds(ABoxInfo));
    Assert(FTextAreas.Count > 0);
  end;
  if (CurrentHorizontalPosition + ABoxInfo.Size.Width > FTextAreas[FCurrentTextAreaIndex].&End) and
    ShouldBeMovedToNextTextArea(ABoxInfo.Box) then
    for I := FCurrentTextAreaIndex + 1 to FTextAreas.Count - 1 do
      if FTextAreas[I].Width > ABoxInfo.Size.Width then
      begin
        InnerCurrentHorizontalPosition := FTextAreas[I].Start;

        FCurrentTextAreaIndex := I;
        Break;
      end;
  Result := inherited CalculateBoxBounds(ABoxInfo);
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.CalculateTextAreas(
  AParagraphFrameItems: TList<TdxParagraphFrameBox>; AFloatingObjectItems: TList<TdxFloatingObjectBox>;
  const ABounds: TRect): TList<TdxTextArea>;
var
  I, ALeft, AFloatingObjectsCount: Integer;
  AProcessedFloatingObjects: TList<TdxFloatingObjectBox>;
  AResult: TdxTextAreaCollectionEx;
begin
  AProcessedFloatingObjects := TList<TdxFloatingObjectBox>.Create;
  AResult := TdxTextAreaCollectionEx.Create;
  ALeft := Min(ABounds.Left, ABounds.Right);
  AResult.Add(TdxTextArea.Create(ALeft, Max(ALeft + 1, ABounds.Right)));
  if AFloatingObjectItems = nil then 
    AFloatingObjectsCount := 0
  else
    AFloatingObjectsCount := AFloatingObjectItems.Count;

  for I := 0 to AFloatingObjectsCount - 1 do
    RowsController.FloatingObjectsLayout.ProcessFloatingObject(AFloatingObjectItems[I], AProcessedFloatingObjects, AResult, ABounds);
  AResult.Sort;
  Result := AResult.InnerList;
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.CanFitBoxToCurrentRow(const ABoxSize: TSize): Boolean;
var
  I: Integer;
begin
  if RowsController.SuppressHorizontalOverfull then
    Exit(True);

  if FTextAreas.Count <= 0 then
    Exit(False);

  if CurrentHorizontalPosition + ABoxSize.Width <= FTextAreas[FCurrentTextAreaIndex].&End then
    Exit(True);

  for I := FCurrentTextAreaIndex to FTextAreas.Count - 1 do
    if FTextAreas[I].Width > ABoxSize.Width then
      Exit(True);

  Result := False;
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.CanFitCurrentRowToColumn(
  ANewRowHeight: Integer): TdxCanFitCurrentRowToColumnResult;
var
  ACurrentRowBounds: TRect;
  ATextAreasRecreated, ARecreated: Boolean;
begin
  if (ANewRowHeight <= FLastRowHeight) and (FTextAreas.Count > 0) then
    Exit(inherited CanFitCurrentRowToColumn(ANewRowHeight));

  ACurrentRowBounds := RowsController.CurrentRow.Bounds;
  ATextAreasRecreated := DoCurrentRowHeightChanged(ANewRowHeight, ACurrentRowBounds, False, False);

  while True do
  begin
    if FTextAreas.Count > 0 then
    begin
      RowsController.CurrentRow.Bounds := ACurrentRowBounds;
      if ATextAreasRecreated then
        Exit(TdxCanFitCurrentRowToColumnResult.TextAreasRecreated);
      Exit(inherited CanFitCurrentRowToColumn(ANewRowHeight));
    end;

    ACurrentRowBounds.MoveToTop(ACurrentRowBounds.Top + 1); 
    if ACurrentRowBounds.Bottom > RowsController.CurrentColumn.Bounds.Bottom then
    begin
      if RowsController.CurrentColumn.Rows.Count = 0 then
        ATextAreasRecreated := DoCurrentRowHeightChanged(ANewRowHeight, ACurrentRowBounds, False, True);
      RowsController.CurrentRow.Bounds := ACurrentRowBounds;
      if ATextAreasRecreated then
        Exit(TdxCanFitCurrentRowToColumnResult.TextAreasRecreated)
      else
        Exit(inherited CanFitCurrentRowToColumn(ANewRowHeight));
    end;

    ARecreated := DoCurrentRowHeightChanged(ANewRowHeight, ACurrentRowBounds, False, False);
    ATextAreasRecreated := ATextAreasRecreated or ARecreated;
  end;

end;

function TdxFloatingObjectsCurrentHorizontalPositionController.CanFitNumberingListBoxToCurrentRow(
  const ABoxSize: TSize): Boolean;
begin
  if RowsController.SuppressHorizontalOverfull then
    Exit(True);

  if FTextAreas.Count <= 0 then
    Exit(False);

  Result := CurrentHorizontalPosition + ABoxSize.Width <= FTextAreas[FCurrentTextAreaIndex].&End;
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.GetTextAreaForTable: TdxTextArea;
begin
  if (FTextAreas = nil) or (FTextAreas.Count < 1) then
  begin
    Assert(False);
    Result := inherited GetTextAreaForTable;
  end
  else
    Result := FTextAreas[0];
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.IncrementCurrentHorizontalPosition(ADelta: Integer);
begin
  inherited IncrementCurrentHorizontalPosition(ADelta);
  InnerCurrentHorizontalPosition := UpdateCurrentTextAreaIndex;
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.MoveCurrentRowDownToFitTable(ATableWidth,
  ATableTop: Integer): Integer;
var
  Y, AMaxY: Integer;
  ATopTableRowBounds: TRect;
  ATextAreasRecreated, ATableFit: Boolean;
begin
  AMaxY := RowsController.CurrentColumn.Bounds.Bottom;
  ATopTableRowBounds := RowsController.CurrentColumn.Bounds;
  ATopTableRowBounds.Height := 0;
  ATopTableRowBounds.MoveToTop(ATableTop);
  for Y := ATableTop to AMaxY do
  begin
    ATextAreasRecreated := DoCurrentRowHeightChanged(0, ATopTableRowBounds, False, False);
    if ATextAreasRecreated or (Y = ATableTop) then
    begin
      ATableFit := (FTextAreas.Count = 1) and (FTextAreas[0].Width >= ATableWidth);
      if ATableFit then
        Exit(ATopTableRowBounds.Top);
    end;
    ATopTableRowBounds.MoveToTop(ATopTableRowBounds.Top + 1);
  end;
  ATopTableRowBounds.MoveToTop(ATableTop);
  DoCurrentRowHeightChanged(0, ATopTableRowBounds, False, True);
  Result := ATableTop;
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.OnCurrentRowFinished;
var
  ACurrentRow: TdxRow;
  ABoxes: TdxBoxCollection;
  I, ACount, AFirstBoxIndex, ATextAreaIndex: Integer;
begin
  if FTextAreas.Count <= 0 then
    Exit;

  ACurrentRow := RowsController.CurrentRow;
  if FTextAreas.Count = 1 then
  begin
    TryCreateDefaultBoxRange(ACurrentRow);
    Exit;
  end;

  ABoxes := ACurrentRow.Boxes;
  ACount := ABoxes.Count;
  if ACount <= 0 then
    Exit;

  AFirstBoxIndex := 0;
  ATextAreaIndex := TdxAlgorithms.BinarySearch<TdxTextArea>(FTextAreas, TdxTextAreaAndXComparable.Create(ABoxes[AFirstBoxIndex].Bounds.Left));
  for I := 1 to ACount - 1 do
    if ABoxes[I - 1].Bounds.Right <> ABoxes[I].Bounds.Left then
    begin
      AppendRowBoxRange(ACurrentRow.BoxRanges, AFirstBoxIndex, I - 1, ATextAreaIndex);
      AFirstBoxIndex := I;
      ATextAreaIndex := TdxAlgorithms.BinarySearch<TdxTextArea>(FTextAreas, TdxTextAreaAndXComparable.Create(ABoxes[i].Bounds.Left));
    end;
  AppendRowBoxRange(ACurrentRow.BoxRanges, AFirstBoxIndex, ACount - 1, ATextAreaIndex);
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.OnCurrentRowHeightChanged(AKeepTextAreas: Boolean);
var
  ACurrentRowBounds: TRect;
begin
  ACurrentRowBounds := RowsController.CurrentRow.Bounds;
  DoCurrentRowHeightChanged(ACurrentRowBounds.Height, ACurrentRowBounds, AKeepTextAreas, False);
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.RollbackCurrentHorizontalPositionTo(AValue: Integer);
begin
  inherited RollbackCurrentHorizontalPositionTo(AValue);
  UpdateCurrentTextAreaIndex;
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.SetCurrentHorizontalPosition(AValue: Integer);
begin
  inherited SetCurrentHorizontalPosition(AValue);
  UpdateCurrentTextAreaIndex;
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.SetCurrentRowInitialHorizontalPosition;
begin
  if FTextAreas.Count > 0 then
  begin
    InnerCurrentHorizontalPosition := FTextAreas[0].Start;
    FCurrentTextAreaIndex := 0;
  end
  else
  begin
    inherited SetCurrentRowInitialHorizontalPosition;
    UpdateCurrentTextAreaIndex;
  end;
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.ShouldBeMovedToNextTextArea(ABox: TdxBox): Boolean;
begin
  Result := not (
    Supports(ABox, ISpaceBox) or 
    (ABox is TdxParagraphMarkBox) or
    (ABox is TdxLineBreakBox) or
    (ABox is TdxSectionMarkBox) or
    (ABox is TdxPageBreakBox) or
    (ABox is TdxColumnBreakBox));
end;

procedure TdxFloatingObjectsCurrentHorizontalPositionController.TryCreateDefaultBoxRange(ACurrentRow: TdxRow);
begin
  if (FTextAreas.Count = 1) and (FTextAreas[0].Start = ACurrentRow.Bounds.Left) and (FTextAreas[0].&End = ACurrentRow.Bounds.Right) then
    Exit;

  AppendRowBoxRange(ACurrentRow.BoxRanges, 0, ACurrentRow.Boxes.Count - 1, 0);
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.UpdateCurrentTextAreaIndex: Integer;
var
  ANewTextAreaIndex: Integer;
begin
  ANewTextAreaIndex := TdxAlgorithms.BinarySearch<TdxTextArea>(FTextAreas, TdxTextAreaAndXComparable.Create(InnerCurrentHorizontalPosition));
  if ANewTextAreaIndex < 0 then
  begin
    ANewTextAreaIndex := not ANewTextAreaIndex;
    if ANewTextAreaIndex >= FTextAreas.Count then
      Exit(InnerCurrentHorizontalPosition);
  end;
  if FCurrentTextAreaIndex <> ANewTextAreaIndex then
  begin
    FCurrentTextAreaIndex := ANewTextAreaIndex;
    Result := FTextAreas[FCurrentTextAreaIndex].Start;
  end
  else
    Result := InnerCurrentHorizontalPosition;
end;

function TdxFloatingObjectsCurrentHorizontalPositionController.DoCurrentRowHeightChanged(AHeight: Integer;
  const ACurrentRowBounds: TRect; AKeepTextAreas, AIgnoreFloatingObjects: Boolean): Boolean;
var
  ALayout: TdxFloatingObjectsLayout;
  AFloatingObjects: TList<TdxFloatingObjectBox>;
  AParagraphFrames: TList<TdxParagraphFrameBox>;
  APrevTextAreas: TList<TdxTextArea>;
begin
  ALayout := RowsController.FloatingObjectsLayout;
  ACurrentRowBounds.Height := AHeight;
  if AIgnoreFloatingObjects then
    AFloatingObjects := nil 
  else
    AFloatingObjects :=  ALayout.GetObjectsInRectangle(ACurrentRowBounds);

  AParagraphFrames := nil;

  APrevTextAreas := FTextAreas;
  if (APrevTextAreas = nil) or not AKeepTextAreas then
    FTextAreas := CalculateTextAreas(AParagraphFrames, AFloatingObjects, ACurrentRowBounds);
  FLastRowHeight := ACurrentRowBounds.Height;

  if FTextAreas.Count > 0 then
    InnerCurrentHorizontalPosition := Max(InnerCurrentHorizontalPosition, FTextAreas[0].Start);

  UpdateCurrentTextAreaIndex;
  Result := AreTextAreasChanged(APrevTextAreas, FTextAreas);
end;

{ TdxTextAreaCollectionEx }

procedure TdxTextAreaCollectionEx.Add(const AInterval: TdxTextArea);
begin
  AddCore(AInterval);
end;

function TdxTextAreaCollectionEx.AddCore(const AInterval: TdxTextArea): Integer;
var
  I: Integer;
  AToRemove: TList<TdxTextArea>;
  ATextArea: TdxTextArea;
begin
  if Contains(AInterval) then
    Exit(0);

  AToRemove := TList<TdxTextArea>.Create;
  ATextArea := AInterval;
  for I := 0 to Count - 1 do
    if ATextArea.IntersectsWith(Self[I]) then
    begin
      ATextArea := TdxTextArea.Union(Self[I], AInterval);
      AToRemove.Add(InnerList[I]);
    end;

  RemoveCore(AToRemove);

  if ATextArea.Width > 0 then
    InnerList.Add(ATextArea);
  Result := Count - 1;
end;

procedure TdxTextAreaCollectionEx.AddCore(AToAdd: TList<TdxTextArea>);
var
  I: Integer;
begin
  for I := 0 to AToAdd.Count - 1 do
    InnerList.Add(AToAdd[I]);
end;

function TdxTextAreaCollectionEx.Contains(const AInterval: TdxTextArea): Boolean;
var
  I: Integer;
begin
  if InnerList.Contains(AInterval) then
    Exit(True);

  for I := 0 to Count - 1 do
    if Self[I].Contains(AInterval) then
      Exit(True);

  Result := False;
end;

constructor TdxTextAreaCollectionEx.Create;
begin
  inherited Create;
  FInnerList := TList<TdxTextArea>.Create;
end;

destructor TdxTextAreaCollectionEx.Destroy;
begin
  FreeAndNil(FInnerList);
  inherited Destroy;
end;

function TdxTextAreaCollectionEx.GetCount: Integer;
begin
  Result := InnerList.Count;
end;

function TdxTextAreaCollectionEx.GetItem(Index: Integer): TdxTextArea;
begin
  Result := InnerList[Index];
end;

function TdxTextAreaCollectionEx.Remove(const AInterval: TdxTextArea): Boolean;
var
  I, AIndex, ASubtractResultCount, ASubtractResultIndex: Integer;
  AToRemove, AToAdd, ASubtractResult: TList<TdxTextArea>;
begin
  AIndex := InnerList.IndexOf(AInterval);
  if AIndex >= 0 then
  begin
    InnerList.Delete(AIndex);
    Exit(True);
  end;

  AToRemove := TList<TdxTextArea>.Create;
  AToAdd := TList<TdxTextArea>.Create;
  try
    for I := 0 to Count - 1 do
      if AInterval.IntersectsWithExcludingBounds(Self[I]) then
      begin
        AToRemove.Add(Self[I]);
        ASubtractResult := Self[I].Subtract(AInterval);
        ASubtractResultCount := ASubtractResult.Count;
        for ASubtractResultIndex := 0 to ASubtractResultCount - 1 do
          AToAdd.Add(ASubtractResult[ASubtractResultIndex]);
      end;
    RemoveCore(AToRemove);
    AddCore(AToAdd);
  finally
    AToAdd.Free;
    AToRemove.Free;
  end;
  Result := True;
end;

procedure TdxTextAreaCollectionEx.RemoveCore(AToRemove: TList<TdxTextArea>);
var
  I: Integer;
begin
  for I := 0 to AToRemove.Count - 1 do
    InnerList.Remove(AToRemove[I]);
end;

type
  { TdxTextAreaStartComparer }

  TdxTextAreaStartComparer = class(TInterfacedObject, IComparer<TdxTextArea>) 
  public
    function Compare(const X, Y: TdxTextArea): Integer; 
  end;

  function TdxTextAreaStartComparer.Compare(const X, Y: TdxTextArea): Integer;
  begin
    Result := TComparer<Integer>.Default.Compare(X.Start, Y.Start);
  end;

procedure TdxTextAreaCollectionEx.Sort;
var
  AComparer: IComparer<TdxTextArea>;
begin
  AComparer := TdxTextAreaStartComparer.Create;
  InnerList.Sort(AComparer); 
  AComparer := nil;
end;

{ TdxTextAreaAndXComparable }

function TdxTextAreaAndXComparable.CompareTo(const AOther: TdxTextArea): Integer;
begin
  if FX < AOther.Start then
    Result := 1
  else
    if FX > AOther.Start then
    begin
      if FX <= AOther.&End then
        Result := 0
      else
        Result := -1;
    end
    else
      Result := 0;
end;

constructor TdxTextAreaAndXComparable.Create(X: Integer);
begin
  inherited Create;
  FX := X;
end;

{ TdxFloatingObjectsLayout }

constructor TdxFloatingObjectsLayout.Create;
begin
  inherited Create;
  FItems := TList<TdxFloatingObjectBox>.Create;
  FForegroundItems := TList<TdxFloatingObjectBox>.Create;
  FBackgroundItems := TList<TdxFloatingObjectBox>.Create;
  FRuns := TList<TdxFloatingObjectAnchorRun>.Create;
  FObjectsTable := TDictionary<TdxFloatingObjectAnchorRun, TdxFloatingObjectBox>.Create;
  FObjectToRunMapTable := TDictionary<TdxFloatingObjectBox, TdxFloatingObjectAnchorRun>.Create;
end;

destructor TdxFloatingObjectsLayout.Destroy;
begin
  FreeAndNil(FObjectToRunMapTable);
  FreeAndNil(FObjectsTable);
  FreeAndNil(FRuns);
  FreeAndNil(FBackgroundItems);
  FreeAndNil(FForegroundItems);
  FreeAndNil(FItems);
  inherited Destroy;
end;

function TdxFloatingObjectsLayout.FindLeftMostX(AProcessedObjects: TList<TdxFloatingObjectBox>; AInitialX: Integer;
  const ABounds: TRect): Integer;
var
  I, AObjectBoundsRight: Integer;
begin
  Result := ABounds.Left;
  for I := 0 to AProcessedObjects.Count - 1 do
  begin
    AObjectBoundsRight := AProcessedObjects[I].ExtendedBounds.Right;
    if (AObjectBoundsRight < AInitialX) and (AObjectBoundsRight > Result) then
      Result := AObjectBoundsRight;
  end;
end;

function TdxFloatingObjectsLayout.FindRightMostX(AProcessedObjects: TList<TdxFloatingObjectBox>; AInitialX: Integer;
  const ABounds: TRect): Integer;
var
  I, AObjectBoundsLeft: Integer;
begin
  Result := ABounds.Right;
  for I := 0 to AProcessedObjects.Count - 1 do
  begin
    AObjectBoundsLeft := AProcessedObjects[i].ExtendedBounds.Left;
    if (AObjectBoundsLeft > AInitialX) and (AObjectBoundsLeft < Result) then
      Result := AObjectBoundsLeft;
  end;
end;

function TdxFloatingObjectsLayout.GetAllObjectsInRectangle(const ABounds: TRect): TList<TdxFloatingObjectBox>;
begin
  ABounds.Height := Max(1, ABounds.Height);
  Result := TList<TdxFloatingObjectBox>.Create;
  GetObjectsInRectangle(Items, Result, ABounds);
  GetObjectsInRectangle(ForegroundItems, Result, ABounds);
  GetObjectsInRectangle(BackgroundItems, Result, ABounds);
end;

function TdxFloatingObjectsLayout.GetFloatingObject(AObjectAnchorRun: TdxFloatingObjectAnchorRun): TdxFloatingObjectBox;
begin
  if not FObjectsTable.TryGetValue(AObjectAnchorRun, Result) then
    Result := nil;
end;

procedure TdxFloatingObjectsLayout.GetFloatingObjects(AWhere: TList<TdxFloatingObjectBox>; APieceTable: TdxPieceTable;
  AObjects: TList<TdxFloatingObjectBox>);
var
  I: Integer;
begin
  if AObjects = nil then
    Exit;

  for I := 0 to AObjects.Count - 1 do
    if AObjects[I].PieceTable = APieceTable then
        AWhere.Add(AObjects[I]);
end;

function TdxFloatingObjectsLayout.GetFloatingObjects(APieceTable: TdxPieceTable): TList<TdxFloatingObjectBox>;
begin
  Result := TList<TdxFloatingObjectBox>.Create;
  GetFloatingObjects(Result, APieceTable, Items);
  GetFloatingObjects(Result, APieceTable, ForegroundItems);
  GetFloatingObjects(Result, APieceTable, BackgroundItems);
end;

procedure TdxFloatingObjectsLayout.GetObjectsInRectangle(AWhere, ATo: TList<TdxFloatingObjectBox>;
  const ABounds: TRect);
var
  I: Integer;
begin
  for I := 0 to AWhere.Count - 1 do
    if AWhere[I].ExtendedBounds.IntersectsWith(ABounds) then
      ATo.Add(AWhere[I]);
end;

type
  { TdxLayoutRectangularFloatingObjectXComparer }

  TdxLayoutRectangularFloatingObjectXComparer = class(TInterfacedObject, IComparer<TdxFloatingObjectBox>) 
  public
    function Compare(const X, Y: TdxFloatingObjectBox): Integer; 
  end;

  function TdxLayoutRectangularFloatingObjectXComparer.Compare(const X, Y: TdxFloatingObjectBox): Integer;
  begin
    Result := TComparer<Integer>.Default.Compare(X.X, Y.X);
  end;

type
  { TdxLayoutRectangularFloatingObjectYComparer }

  TdxLayoutRectangularFloatingObjectYComparer = class(TInterfacedObject, IComparer<TdxFloatingObjectBox>) 
  public
    function Compare(const X, Y: TdxFloatingObjectBox): Integer; 
  end;

  function TdxLayoutRectangularFloatingObjectYComparer.Compare(const X, Y: TdxFloatingObjectBox): Integer;
  begin
    Result := TComparer<Integer>.Default.Compare(X.Y, Y.Y);
  end;

class constructor TdxFloatingObjectsLayout.Initialize;
begin
  FHorizontalObjectComparer := TdxLayoutRectangularFloatingObjectXComparer.Create;
  FVerticalObjectComparer := TdxLayoutRectangularFloatingObjectYComparer.Create;
end;

procedure TdxFloatingObjectsLayout.MoveFloatingObjectsVertically(ADeltaY: Integer; APieceTable: TdxPieceTable);
var
  I: Integer;
  AObjects: TList<TdxFloatingObjectBox>;
begin
  AObjects := GetFloatingObjects(APieceTable);
  for I := 0 to AObjects.Count - 1 do
    AObjects[I].MoveVertically(ADeltaY);
end;

procedure TdxFloatingObjectsLayout.ProcessFloatingObject(AFloatingObject: TdxFloatingObjectBox;
  AProcessedObjects: TList<TdxFloatingObjectBox>; AResult: TdxTextAreaCollectionEx; const AInitialBounds: TRect);
var
  ABounds: TRect;
  ALeftMostX, ARightMostX, ALeftSideWidth, ARightSideWidth: Integer;
begin
  ABounds := AFloatingObject.ExtendedBounds;
  ALeftMostX := FindLeftMostX(AProcessedObjects, ABounds.Left, AInitialBounds);
  ARightMostX := FindRightMostX(AProcessedObjects, ABounds.Right, AInitialBounds);

  AProcessedObjects.Add(AFloatingObject);

  AResult.Remove(TdxTextArea.Create(ABounds.Left, ABounds.Right));
  ALeftSideWidth := ABounds.Left - ALeftMostX;
  ARightSideWidth := ARightMostX - ABounds.Right;
  if not AFloatingObject.CanPutTextAtLeft or (AFloatingObject.PutTextAtLargestSide and (ALeftSideWidth <= ARightSideWidth)) then
    AResult.Remove(TdxTextArea.Create(ALeftMostX, ABounds.Left));

  if not AFloatingObject.CanPutTextAtRight or (AFloatingObject.PutTextAtLargestSide and (ARightSideWidth < ALeftSideWidth)) then
    AResult.Remove(TdxTextArea.Create(ABounds.Right, ARightMostX));
end;

function TdxFloatingObjectsLayout.GetObjectsInRectangle(const ABounds: TRect): TList<TdxFloatingObjectBox>;
begin
  ABounds.Height := Math.Max(1, ABounds.Height);
  Result := TList<TdxFloatingObjectBox>.Create;
  GetObjectsInRectangle(Items, Result, ABounds);
  Result.Sort(FHorizontalObjectComparer);
end;

procedure TdxFloatingObjectsLayout.Add(AObjectAnchorRun: TdxFloatingObjectAnchorRun;
  AFloatingObject: TdxFloatingObjectBox);
begin
  if Runs.Contains(AObjectAnchorRun) then
    Exit;

  Runs.Add(AObjectAnchorRun);
  FObjectsTable.Add(AObjectAnchorRun, AFloatingObject);
  FObjectToRunMapTable.Add(AFloatingObject, AObjectAnchorRun);

  if AObjectAnchorRun.FloatingObjectProperties.TextWrapType = TdxFloatingObjectTextWrapType.None then
  begin
    if AObjectAnchorRun.FloatingObjectProperties.IsBehindDoc then
      FBackgroundItems.Add(AFloatingObject)
    else
      FForegroundItems.Add(AFloatingObject);
  end
  else
    Add(AFloatingObject);
end;

procedure TdxFloatingObjectsLayout.Add(AFloatingObject: TdxFloatingObjectBox);
begin
  NotImplemented()
end;

procedure TdxFloatingObjectsLayout.ClearFloatingObjects(ARunIndex: TdxRunIndex; AObjects: TList<TdxFloatingObjectBox>);
var
  I, ACount: Integer;
  AFloatingObject: TdxFloatingObjectBox;
  ARun: TdxFloatingObjectAnchorRun;
begin
  ACount := AObjects.Count;
  for I := ACount - 1 downto 0 do
  begin
    AFloatingObject := AObjects[I];
    if ARunIndex <= AFloatingObject.StartPos.RunIndex then
    begin
      AObjects.Delete(I);
      AFloatingObject.LockPosition := False;
      ARun := FObjectToRunMapTable[AFloatingObject];
      FObjectToRunMapTable.Remove(AFloatingObject);
      Runs.Remove(ARun);
      FObjectsTable.Remove(ARun);
    end;
  end;
end;

function TdxFloatingObjectsLayout.CalculateTextAreas(AItems: TList<TdxFloatingObjectBox>;
  const ABounds: TRect): TList<TdxTextArea>;
var
  I, ALeft: Integer;
  AResult: TdxTextAreaCollectionEx;
  AProcessedObjects: TList<TdxFloatingObjectBox>;
begin
  AProcessedObjects := TList<TdxFloatingObjectBox>.Create;
  try
    AResult := TdxTextAreaCollectionEx.Create;
    ALeft := Min(ABounds.Left, ABounds.Right);
    AResult.Add(TdxTextArea.Create(ALeft, Max(ALeft + 1, ABounds.Right)));
    for I := 0 to AItems.Count - 1 do
      ProcessFloatingObject(AItems[i], AProcessedObjects, AResult, ABounds);
  finally
    AProcessedObjects.Free;
  end;
  AResult.Sort;
  Result := AResult.InnerList;
end;

procedure TdxFloatingObjectsLayout.Clear;
begin
  Items.Clear;
  BackgroundItems.Clear;
  ForegroundItems.Clear;
  Runs.Clear;
  FObjectsTable.Clear;
  FObjectToRunMapTable.Clear;
end;

procedure TdxFloatingObjectsLayout.ClearFloatingObjects(ARunIndex: TdxRunIndex);
begin
  ClearFloatingObjects(ARunIndex, Items);
  ClearFloatingObjects(ARunIndex, BackgroundItems);
  ClearFloatingObjects(ARunIndex, ForegroundItems);
end;

function TdxFloatingObjectsLayout.ContainsRun(AObjectAnchorRun: TdxFloatingObjectAnchorRun): Boolean;
begin
  Result := FRuns.Contains(AObjectAnchorRun);
end;

{ TdxTableCellVerticalBorderCalculator }

constructor TdxTableCellVerticalBorderCalculator.Create(ATable: TdxTable);
begin
  inherited Create;
  FTable := ATable;
end;

class function TdxTableCellVerticalBorderCalculator.GetCellByColumnIndex(ARow: TdxTableRow;
  AStartColumnIndex: Integer): TdxTableCell;
var
  I, AColumnIndex: Integer;
  ACurrentCell: TdxTableCell;
  ACells: TdxTableCellCollection;
begin
  AColumnIndex := ARow.GridBefore;
  ACells := ARow.Cells;
  for I := 0 to ACells.Count - 1 do
  begin
    ACurrentCell := ACells[I];
    if (AStartColumnIndex >= AColumnIndex) and (AStartColumnIndex < AColumnIndex + ACurrentCell.ColumnSpan) then
      Exit(ACurrentCell);
    Inc(AColumnIndex, ACurrentCell.ColumnSpan);
  end;
  Result := nil;
end;

class function TdxTableCellVerticalBorderCalculator.GetCellByEndColumnIndex(ARow: TdxTableRow;
  AEndColumnIndex: Integer): TdxTableCell;
var
  ACellByColumnIndex: TdxTableCell;
  ACellIndex: Integer;
begin
  ACellByColumnIndex := GetCellByColumnIndex(ARow, AEndColumnIndex);
  if ACellByColumnIndex = nil then
    Exit(nil);

  if GetStartColumnIndex(ACellByColumnIndex, False) + ACellByColumnIndex.ColumnSpan - 1 <= AEndColumnIndex then
    Exit(ACellByColumnIndex);
  ACellIndex := ARow.Cells.IndexOf(ACellByColumnIndex);
  if ACellIndex <> 0 then
    Result := ARow.Cells[ACellIndex - 1]
  else
    Result := nil;
end;

class function TdxTableCellVerticalBorderCalculator.GetCellByStartColumnIndex(ARow: TdxTableRow;
  AStartColumnIndex: Integer; ALayoutIndex: Boolean): TdxTableCell;
var
  ACell: TdxTableCell;
  AColumnIndex, ACellIndex: Integer;
begin
  if ALayoutIndex then
    AColumnIndex := ARow.LayoutProperties.GridBefore
  else
    AColumnIndex := ARow.GridBefore;
  ACellIndex := 0;
  while (AColumnIndex < AStartColumnIndex) and (ACellIndex < ARow.Cells.Count) do
  begin
    ACell := ARow.Cells[ACellIndex];
    if ALayoutIndex then
      Inc(AColumnIndex, ACell.LayoutProperties.ColumnSpan)
    else
      Inc(AColumnIndex, ACell.ColumnSpan);
    Inc(ACellIndex);
  end;
  if ACellIndex < ARow.Cells.Count then
    Result := ARow.Cells[ACellIndex]
  else
    Result := nil;
end;

class function TdxTableCellVerticalBorderCalculator.GetCellsByIntervalColumnIndex(ARow: TdxTableRow; AStartColumnIndex,
  AEndColumnIndex: Integer): TList<TdxTableCell>;
var
  ACell: TdxTableCell;
  ACellStartColumnIndex: Integer;
begin
  Result := TList<TdxTableCell>.Create;
  while AStartColumnIndex <= AEndColumnIndex do
  begin
    ACell := GetCellByColumnIndex(ARow, AStartColumnIndex);
    if ACell = nil then
      Exit;
    Result.Add(ACell);
    ACellStartColumnIndex := GetStartColumnIndex(ACell, False);
    Inc(AStartColumnIndex, AStartColumnIndex - ACellStartColumnIndex + ACell.ColumnSpan);
  end;
end;

function TdxTableCellVerticalBorderCalculator.GetLeftBorder(ABorderCalculator: TdxTableBorderCalculator;
  ACell: TdxTableCell): TdxBorderInfo;
var
  ACellIndex: Integer;
  APrevCellBorder: TdxBorderBase;
begin
  if (ACell.Row.CellSpacing.&Type = TdxWidthUnitType.ModelUnits) and (ACell.Row.CellSpacing.Value > 0) then
    Exit(ACell.GetActualLeftCellBorder.Info);

  ACellIndex := ACell.Row.Cells.IndexOf(ACell);
  if ACellIndex > 0 then
  begin
    APrevCellBorder := ACell.Row.Cells[ACellIndex - 1].GetActualRightCellBorder;
    Result := ABorderCalculator.GetVerticalBorderSource(FTable, APrevCellBorder.Info, ACell.GetActualLeftCellBorder.Info);
  end
  else
    Result := ACell.GetActualLeftCellBorder.Info;
end;

function TdxTableCellVerticalBorderCalculator.GetLeftBorderWidth(ABorderCalculator: TdxTableBorderCalculator;
  ACell: TdxTableCell; ALayoutIndex: Boolean): TdxModelUnit;
var
  I, AStartColumnIndex: Integer;
  AVerticalSpanCells: TList<TdxTableCell>;
begin
  AStartColumnIndex := GetStartColumnIndex(ACell, ALayoutIndex);
  AVerticalSpanCells := GetVerticalSpanCells(ACell, AStartColumnIndex, ALayoutIndex);
  try
    Result := 0;
    for I := 0 to AVerticalSpanCells.Count - 1 do
      Result := Math.Max(ABorderCalculator.GetActualWidth(GetLeftBorder(ABorderCalculator, AVerticalSpanCells[I])), Result);
  finally
    AVerticalSpanCells.Free;
  end;
end;

function TdxTableCellVerticalBorderCalculator.GetRightBorder(ABorderCalculator: TdxTableBorderCalculator;
  ACell: TdxTableCell): TdxBorderInfo;
var
  ACellIndex: Integer;
  ANextCellBorder: TdxBorderInfo;
begin
  if (ACell.Row.CellSpacing.&Type = TdxWidthUnitType.ModelUnits) and (ACell.Row.CellSpacing.Value > 0) then
    Exit(ACell.GetActualRightCellBorder.Info);

  ACellIndex := ACell.Row.Cells.IndexOf(ACell);
  if ACellIndex + 1 < ACell.Row.Cells.Count then
  begin
    ANextCellBorder := ACell.Row.Cells[ACellIndex + 1].GetActualLeftCellBorder.Info;
    Result := ABorderCalculator.GetVerticalBorderSource(FTable, ANextCellBorder, ACell.GetActualRightCellBorder.Info);
  end
  else
    Result := ACell.GetActualRightCellBorder.Info;
end;

function TdxTableCellVerticalBorderCalculator.GetRightBorderWidth(ABorderCalculator: TdxTableBorderCalculator;
  ACell: TdxTableCell): TdxModelUnit;
var
  I, AStartColumnIndex: Integer;
  AVerticalSpanCells: TList<TdxTableCell>;
begin
  AStartColumnIndex := GetStartColumnIndex(ACell, True);
  AVerticalSpanCells := GetVerticalSpanCells(ACell, AStartColumnIndex, True);
  try
    Result := 0;
    for I := 0 to AVerticalSpanCells.Count - 1 do
      Result := Math.Max(ABorderCalculator.GetActualWidth(GetRightBorder(ABorderCalculator, AVerticalSpanCells[I])), Result);
  finally
    AVerticalSpanCells.Free;
  end;
end;

class function TdxTableCellVerticalBorderCalculator.GetStartColumnIndex(ACell: TdxTableCell;
  ALayoutIndex: Boolean): Integer;
var
  ARow: TdxTableRow;
  ACurrentCell: TdxTableCell;
  ACells: TdxTableCellCollection;
  AColumnIndex, ACellIndex, ACount: Integer;
begin
  ARow := ACell.Row;
  if ALayoutIndex then
    AColumnIndex := ARow.LayoutProperties.GridBefore
  else
    AColumnIndex := ARow.GridBefore;
  ACellIndex := 0;
  ACells := ARow.Cells;
  ACount := ACells.Count;
  while (ACellIndex < ACount) and (ACells[ACellIndex] <> ACell) do
  begin
    ACurrentCell := ACells[ACellIndex];
    if ALayoutIndex then
      Inc(AColumnIndex, ACurrentCell.LayoutProperties.ColumnSpan)
    else
      Inc(AColumnIndex, ACurrentCell.ColumnSpan);
    Inc(ACellIndex);
  end;
  Result := AColumnIndex;
end;

class function TdxTableCellVerticalBorderCalculator.GetVerticalSpanCells(ACell: TdxTableCell;
  ALayoutIndex: Boolean): TList<TdxTableCell>;
var
  AStartColumnIndex: Integer;
begin
  AStartColumnIndex := GetStartColumnIndex(ACell, ALayoutIndex);
  Result := GetVerticalSpanCells(ACell, AStartColumnIndex, ALayoutIndex);
end;

class function TdxTableCellVerticalBorderCalculator.GetVerticalSpanCells(ACell: TdxTableCell;
  AStartColumnIndex: Integer; ALayoutIndex: Boolean): TList<TdxTableCell>;
var
  ATable: TdxTable;
  ARowIndex: Integer;
  ARow: TdxTableRow;
  ARowCell: TdxTableCell;
begin
  Result := TList<TdxTableCell>.Create;
  Result.Add(ACell);
  if ACell.VerticalMerging <> TdxMergingState.Restart then
    Exit;
  ATable := ACell.Table;
  for ARowIndex := ATable.Rows.IndexOf(ACell.Row) + 1 to ATable.Rows.Count do
  begin
    ARow := ATable.Rows[ARowIndex];
    ARowCell := GetCellByStartColumnIndex(ARow, AStartColumnIndex, ALayoutIndex);
    if (ARowCell <> nil) and (ARowCell.VerticalMerging = TdxMergingState.Continue) then
      Result.Add(ARowCell)
    else
      Break;
  end;
end;

{ TdxTableCellIterator }

constructor TdxTableCellIterator.Create(ARow: TdxTableRow);
begin
  inherited Create;
  FRow := ARow;
  FCurrentStartColumnIndex := -1;
  FCurrentEndColumnIndex := -1;
  FCurrentCellIndex := -1;
end;

function TdxTableCellIterator.GetCellCount: Integer;
begin
  Result := Row.Cells.Count;
end;

function TdxTableCellIterator.GetCurrentCell: TdxTableCell;
begin
  if (FCurrentCellIndex >= 0) and (FCurrentCellIndex < CellCount) then
    Result := FRow.Cells[FCurrentCellIndex]
  else
    Result := nil;
end;

function TdxTableCellIterator.GetCurrentEndColumnIndex: Integer;
begin
  Result := FCurrentEndColumnIndex;
end;

function TdxTableCellIterator.GetCurrentStartColumnIndex: Integer;
begin
  Result := FCurrentStartColumnIndex;
end;

function TdxTableCellIterator.GetEndOfRow: Boolean;
begin
  Result := FCurrentCellIndex >= CellCount;
end;

function TdxTableCellIterator.MoveNextCell: Boolean;
begin
  if EndOfRow then
    Exit(False);
  Inc(FCurrentCellIndex);
  if FCurrentCellIndex = 0 then
    FCurrentStartColumnIndex := Row.LayoutProperties.GridBefore
  else
    FCurrentStartColumnIndex := CurrentEndColumnIndex + 1;
  if FCurrentCellIndex < Row.Cells.Count then
    FCurrentEndColumnIndex := CurrentStartColumnIndex + CurrentCell.LayoutProperties.ColumnSpan - 1
  else
    FCurrentEndColumnIndex := FCurrentStartColumnIndex;
  Result := True;
end;

procedure TdxTableCellIterator.SetStartColumnIndex(ANewStartColumnIndex: Integer);
begin
  if ANewStartColumnIndex < CurrentStartColumnIndex then
    raise Exception.Create(''); 
  while ANewStartColumnIndex > CurrentEndColumnIndex do
    if not MoveNextCell then
      Exit;
  FCurrentStartColumnIndex := ANewStartColumnIndex;
end;

{ TdxTableCellEmptyIterator }

function TdxTableCellEmptyIterator.GetCurrentCell: TdxTableCell;
begin
  Result := nil;
end;

function TdxTableCellEmptyIterator.GetCurrentEndColumnIndex: Integer;
begin
  raise Exception.Create(''); 
end;

function TdxTableCellEmptyIterator.GetCurrentStartColumnIndex: Integer;
begin
  raise Exception.Create(''); 
end;

function TdxTableCellEmptyIterator.GetEndOfRow: Boolean;
begin
  Result := True;
end;

function TdxTableCellEmptyIterator.MoveNextCell: Boolean;
begin
  Result := False;
end;

procedure TdxTableCellEmptyIterator.SetStartColumnIndex(ANewStartColumnIndex: Integer);
begin
  raise Exception.Create(''); 
end;

{ TdxTablesControllerTableState }

constructor TdxTablesControllerTableState.Create(ATablesController: TdxTablesController; AStartCell: TdxTableCell;
  AFirstContentInParentCell: Boolean);
var
  ATable: TdxTable;
  ATablesControllerState: TdxTablesControllerStateBase;
begin
  inherited Create(ATablesController);
  Assert(AStartCell <> nil);
  FCurrentCell := AStartCell;
  FBorderCalculator := TdxTableBorderCalculator.Create;
  FUnitConverter := AStartCell.DocumentModel.ToDocumentLayoutUnitConverter;
  ATablesControllerState := ATablesController.State;
  if ATablesControllerState is TdxTablesControllerTableState then
    FTableViewInfoManager := ATablesController.CreateTableViewInfoManager(TdxTablesControllerTableState(ATablesControllerState).TableViewInfoManager, TablesController.RowsController)
  else
    FTableViewInfoManager := ATablesController.CreateTableViewInfoManager(nil, TablesController.RowsController);
  FCellsBounds := TDictionary<TdxTableCell, TdxLayoutGridRectangle>.Create;
  ATable := CurrentCell.Table;
  FVerticalBordersCalculator := TdxTableCellVerticalBorderCalculator.Create(ATable);
  FHorizontalBordersCalculator := TdxTableCellHorizontalBorderCalculator.Create(ATable);
  PrepareGridCellBounds;
  StartNewTable(ATable, AFirstContentInParentCell);
end;

procedure TdxTablesControllerTableState.AddTopAnchor;
var
  ARowSeparatorIndex: Integer;
  ABottomTextIndent: TdxLayoutUnit;
  ATopAnchor: TdxTableCellVerticalAnchor;
  ACellBordersInfo: TList<TdxHorizontalCellBordersInfo>;
begin
  ARowSeparatorIndex := CurrentCell.RowIndex;
  ABottomTextIndent := CalculateBottomTextIndent(ARowSeparatorIndex, ACellBordersInfo);
  ATopAnchor := TdxTableCellVerticalAnchor.Create(TableViewInfoManager.CurrentCellBottom, ABottomTextIndent, ACellBordersInfo);
  ATopAnchor.TopTextIndent := UnitConverter.ToLayoutUnits(GetActualCellTopMargin(CurrentCell));

  TableViewInfoManager.SetRowSeparator(ARowSeparatorIndex, ATopAnchor);
  TableViewInfoManager.SetRowSeparatorForCurrentTableViewInfo(ARowSeparatorIndex, ATopAnchor);
end;

procedure TdxTablesControllerTableState.AfterMoveRowToNextColumn;
begin
  TableViewInfoManager.AfterMoveRowToNextColumn;
  TableViewInfoManager.CurrentCellBottom := 0;
  FCurrentCellRowCount := 0;
  FTableViewInfoManager.ValidateTopLevelColumn;
end;

procedure TdxTablesControllerTableState.BeforeMoveRowToNextColumn;
begin
end;

function TdxTablesControllerTableState.CalcFirstInvalidRowIndex: Integer;
var
  ANewResult: Integer;
begin
  Result := CurrentTable.Rows.IndexOf(CurrentCell.Row);
  while Result > 0 do
  begin
    ANewResult := CalcFirstInvalidRowIndexCore(Result);
    if ANewResult = Result then
      Break
    else
      Result := ANewResult;
  end;
end;

function TdxTablesControllerTableState.CalcFirstInvalidRowIndexCore(AStartRowIndex: Integer): Integer;
var
  I: Integer;
  ACell, ATopCell: TdxTableCell;
  ACells: TdxTableCellCollection;
begin
  Result := AStartRowIndex;
  ACells := CurrentTable.Rows[AStartRowIndex].Cells;
  for I := 0 to ACells.Count - 1 do
  begin
    ACell := ACells[I];
    if ACell = CurrentCell then
      Continue;
    if ACell.VerticalMerging <> TdxMergingState.Continue then
      Continue;
    ATopCell := CurrentTable.GetFirstCellInVerticalMergingGroup(ACell);
    Result := Math.Min(Result, CurrentTable.Rows.IndexOf(ATopCell.Row));
  end;
end;

function TdxTablesControllerTableState.CalculateBottomTextIndent(ARowSeparatorIndex: Integer;
  out ACellBordersInfo: TList<TdxHorizontalCellBordersInfo>): TdxLayoutUnit;
var
  AIndentCalculator: TdxBottomTextIndentCalculatorBase;
begin
  AIndentCalculator := TdxBottomTextIndentCalculatorBase.CreateCalculator(Self, CurrentTable, ARowSeparatorIndex);
  try
    Result := AIndentCalculator.CalculateBottomTextIndent(ACellBordersInfo);
  finally
    AIndentCalculator.Free;
  end;
end;

function TdxTablesControllerTableState.CalculateTableActualLeftRelativeOffset(ATable: TdxTable): TdxModelUnit;
var
  AFirstCell: TdxTableCell;
  ALeftMargin: TdxMarginUnitBase;
  ALeftBorderWidth, ALeftMarginValue: TdxModelUnit;
begin
  AFirstCell := ATable.Rows.First.Cells.First;
  ALeftBorderWidth := VerticalBordersCalculator.GetLeftBorderWidth(FBorderCalculator, AFirstCell, False);
  if (ATable.NestedLevel > 0) or  RowsController.MatchHorizontalTableIndentsToTextEdge then
    Result := Trunc(ALeftBorderWidth / 2)
  else
  begin
    ALeftMargin := AFirstCell.GetActualLeftMargin();
    if ALeftMargin.&Type = TdxWidthUnitType.ModelUnits then
      ALeftMarginValue := ALeftMargin.Value
    else
      ALeftMarginValue := 0;
    Result := -Math.Max(Trunc(ALeftMarginValue), Trunc(ALeftBorderWidth / 2));
  end;
end;

function TdxTablesControllerTableState.CanFitRowToColumn(ALastTextRowBottom: Integer;
  AColumn: TdxColumn): TdxCanFitCurrentRowToColumnResult;
var
  AInfinityHeight: Boolean;
  ASpecialBottomAnchorIndex, ABottomBounds: Integer;
  ABreakType: TdxTableBreakType;
  ACells: TList<TdxTableCell>;
  ACell: TdxTableCell;
  ACurrentTableViewInfoManager: TdxTableViewInfoManager;
  ATable: TdxTable;
begin
  AInfinityHeight := TablesController.IsInfinityHeight(TableViewInfoManager.GetCurrentTopLevelTableViewInfoIndex, ASpecialBottomAnchorIndex);
  if AInfinityHeight then
  begin
    if CurrentTable.NestedLevel > 0 then
      Exit(TdxCanFitCurrentRowToColumnResult.RowFitted);
    if TableViewInfoManager.GetCurrentCellTopAnchorIndex >= ASpecialBottomAnchorIndex then
    begin
      TableViewInfoManager.SetCurrentCellHasContent;
      Exit(TdxCanFitCurrentRowToColumnResult.PlainRowNotFitted);
    end
    else
      Exit(TdxCanFitCurrentRowToColumnResult.RowFitted);
  end;
  if FCurrentCellRowCount = 0 then
  begin
    ABreakType := TdxTableBreakType.NoBreak;
    ABottomBounds := AColumn.Bounds.Bottom;
    if not IsFirstTableRowViewInfoInColumn then
    begin
      ACell := CurrentCell;
      ACurrentTableViewInfoManager := FTableViewInfoManager;
      while ACell <> nil do
      begin
        ATable := ACell.Table;
        ABreakType := TablesController.GetTableBreak(ATable, FTableViewInfoManager.GetCurrentTableViewInfoIndex, ABottomBounds);

        if ACell.VerticalMerging = TdxMergingState.Restart then
        begin
          ACells := ACell.GetVerticalSpanCells;
          if ACells.Count > 1 then 
            if (ABreakType = TdxTableBreakType.NoBreak) or (FTableViewInfoManager.GetCurrentCellTop <> ABottomBounds) then
              Exit(TdxCanFitCurrentRowToColumnResult.RowFitted);
        end;
        if ABreakType <> TdxTableBreakType.NoBreak then
          Break;
        if ACurrentTableViewInfoManager.GetCurrentCellTopAnchorIndex > 0 then
          Break;
        if not ACurrentTableViewInfoManager.IsCurrentCellViewInfoFirst then
          Break;
        if not ACurrentTableViewInfoManager.IsFirstContentInParentCell then
          Break;
        ACurrentTableViewInfoManager := FTableViewInfoManager.GetParentTableViewInfoManager;
        ACell := ACell.Table.ParentCell;
      end;
    end
    else
      ABreakType := TdxTableBreakType.NoBreak;
    if ABreakType = TdxTableBreakType.NextPage then
    begin
      if ALastTextRowBottom > ABottomBounds then
      begin
        FTableViewInfoManager.SetCurrentCellHasContent;
        Exit(TdxCanFitCurrentRowToColumnResult.PlainRowNotFitted);
      end
      else
        Exit(TdxCanFitCurrentRowToColumnResult.RowFitted);
    end;
    if ALastTextRowBottom > AColumn.Bounds.Bottom then
      Result := TdxCanFitCurrentRowToColumnResult.FirstCellRowNotFitted
    else
      Result := TdxCanFitCurrentRowToColumnResult.RowFitted;
  end
  else
  begin
    ABottomBounds := AColumn.Bounds.Bottom;
    ABreakType := TablesController.GetTableBreak(CurrentTable, FTableViewInfoManager.GetCurrentTableViewInfoIndex, ABottomBounds);
    if ABreakType = TdxTableBreakType.NoBreak then
      ABottomBounds := AColumn.Bounds.Bottom;
    if ALastTextRowBottom > ABottomBounds then
      Result := TdxCanFitCurrentRowToColumnResult.PlainRowNotFitted
    else
      Result := TdxCanFitCurrentRowToColumnResult.RowFitted;
  end;
end;

procedure TdxTablesControllerTableState.ChangeCurrentCellInTheSameTable(ANewCell: TdxTableCell);
var
  ARowIndex: Integer;
  ACellBounds: TRect;
  ATopAnchor: IdxTableCellVerticalAnchor;
  AColumnController: TdxTableCellColumnController;
  ABounds, ATextBounds, ANewRowBounds: TdxLayoutRectangle;
begin
  if FRestartFrom = TdxRestartFrom.NoRestart then
  begin
    tableViewInfoManager.ValidateTopLevelColumn;
    FinishCurrentCell;
  end;
  FCurrentCell := ANewCell;
  EnsureCurrentTableRow(CurrentCell.Row);
  ARowIndex := ANewCell.Table.Rows.IndexOf(ANewCell.Row);
  ATopAnchor := TableViewInfoManager.GetRowStartAnchor(ARowIndex);
  if ATopAnchor = nil then  
  begin
    AddTopAnchor;
    ATopAnchor := TableViewInfoManager.GetRowStartAnchor(ARowIndex);
  end;
  ABounds := GetCellBounds(CurrentCell);

  AColumnController := TdxTableCellColumnController(RowsController.ColumnController);

  ACellBounds := ABounds;
  ACellBounds.Offset(AColumnController.ParentColumn.Bounds.Left, 0);
  ATextBounds := GetTextBounds(CurrentCell, ABounds);

  TableViewInfoManager.StartNextCell(CurrentCell, ACellBounds, FCellsBounds[CurrentCell], ATextBounds);

  AColumnController.StartNewCell(TableViewInfoManager.GetRowStartColumn(ARowIndex), ATextBounds.Left, ATextBounds.Top, ATextBounds.Width, CurrentCell);
  RowsController.CurrentColumn := AColumnController.GetStartColumn;
  RowsController.OnCellStart;
  ANewRowBounds := RowsController.CurrentRow.Bounds;
  ANewRowBounds.MoveToTop(ATextBounds.Top);
  RowsController.CurrentRow.Bounds := ANewRowBounds;
  TableViewInfoManager.CurrentCellBottom := ATopAnchor.VerticalPosition + ATopAnchor.BottomTextIndent;
  FTableViewInfoManager.ValidateTopLevelColumn;
end;

procedure TdxTablesControllerTableState.EndParagraph(ALastRow: TdxRow);
begin
  UpdateCurrentCellHeight(ALastRow);
end;

procedure TdxTablesControllerTableState.EnsureCurrentCell(ACell: TdxTableCell);
var
  ASameLevelCell: TdxTableCell;
begin
  if FRestartFrom = TdxRestartFrom.CellStart then
  begin
    ASameLevelCell := ACell;
    while ASameLevelCell.Table.NestedLevel > CurrentTable.NestedLevel do
      ASameLevelCell := ASameLevelCell.Table.ParentCell;
    ChangeCurrentCellInTheSameTable(ASameLevelCell);
    FRestartFrom := TdxRestartFrom.NoRestart;
    if ACell.Table.NestedLevel > CurrentTable.NestedLevel then
      TablesController.StartNewTable(ACell);
    Exit;
  end;
  if ACell = CurrentCell then
    Exit;
  if (ACell = nil) or (CurrentCell.Table.ParentCell = ACell) then
  begin
    TablesController.LeaveCurrentTable(ACell);
    Exit;
  end;
  if ACell.Table = CurrentTable then
  begin
    ChangeCurrentCellInTheSameTable(ACell);
    Exit;
  end;
  if ACell.Table.NestedLevel = CurrentTable.NestedLevel then
  begin
    TablesController.LeaveCurrentTable(ACell);
    Exit;
  end;
  if ACell.Table.NestedLevel > CurrentTable.NestedLevel then
  begin
    ASameLevelCell := ACell;
    while ASameLevelCell.Table.NestedLevel > CurrentTable.NestedLevel do
      ASameLevelCell := ASameLevelCell.Table.ParentCell;
    if ASameLevelCell <> CurrentCell then
    begin
      if ASameLevelCell.Table = CurrentCell.Table then
        ChangeCurrentCellInTheSameTable(ASameLevelCell)
      else
      begin
        TablesController.LeaveCurrentTable(ACell);
        Exit;
      end;
    end;
  end;
  if ACell.Table.NestedLevel < CurrentTable.NestedLevel then
  begin
    TablesController.LeaveCurrentTable(ACell);
    Exit;
  end;
  TablesController.StartNewTable(ACell);
end;

procedure TdxTablesControllerTableState.EnsureCurrentTableRow(ARow: TdxTableRow);
begin
  if CurrentRow <> ARow then
    FCurrentRow := ARow;
end;

procedure TdxTablesControllerTableState.FinishCurrentCell;
var
  ABottomMargin, ARowTop, ACurrentRowHeight, ARowHeight, ABottomTextIndent: TdxLayoutUnit;
  ACurrentCellBottom, ANewBottom: Integer;
  AFixedRowHeight: Boolean;
  ABottomAnchor, AAnchor, AMaxAnchor: TdxTableCellVerticalAnchor;
  ARowSeparatorIndex: Integer;
  AColumnController: TdxTableCellColumnController;
  ACellBordersInfo: TList<TdxHorizontalCellBordersInfo>;
begin
  ABottomMargin := UnitConverter.ToLayoutUnits(GetActualCellBottomMargin(CurrentCell));
  ACurrentCellBottom := TableViewInfoManager.CurrentCellBottom;
  Inc(ACurrentCellBottom, ABottomMargin);
  AFixedRowHeight := False;
  if (FCellsBounds[CurrentCell].RowSpan = 1) and (CurrentCell.Row.Height.&Type <> TdxHeightUnitType.Auto) then
  begin
    ARowTop := TableViewInfoManager.GetCurrentCellTop;
    ACurrentRowHeight := ACurrentCellBottom - TableViewInfoManager.GetCurrentCellTop;
    ARowHeight := CurrentCell.DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(FCurrentCell.Row.Height.Value);
    if (FCurrentCell.Row.Height.&Type = TdxHeightUnitType.Minimum) and (ACurrentRowHeight < ARowHeight) then
      ACurrentCellBottom := ACurrentCellBottom + (ARowHeight - ACurrentRowHeight)
    else
      if FCurrentCell.Row.Height.&Type = TdxHeightUnitType.Exact then
      begin
        ACurrentCellBottom := ARowTop + ARowHeight;
        AFixedRowHeight := True;
      end;
  end;

  ABottomAnchor := TableViewInfoManager.GetBottomCellAnchor(CurrentCell);
  ARowSeparatorIndex := FTableViewInfoManager.GetBottomCellRowSeparatorIndex(CurrentCell, FCellsBounds[CurrentCell].RowSpan);
  AColumnController := TdxTableCellColumnController(RowsController.ColumnController);
  if ABottomAnchor = nil then
  begin
    ABottomTextIndent := CalculateBottomTextIndent(ARowSeparatorIndex, ACellBordersInfo);
    ABottomAnchor := TdxTableCellVerticalAnchor.Create(ACurrentCellBottom, ABottomTextIndent, ACellBordersInfo);
    ABottomAnchor.TopTextIndent := ABottomMargin;
    TableViewInfoManager.SetRowSeparator(ARowSeparatorIndex, ABottomAnchor);
  end
  else
  begin
    if ABottomMargin > ABottomAnchor.TopTextIndent then
    begin
      ANewBottom := ABottomAnchor.VerticalPosition - ABottomAnchor.TopTextIndent + ABottomMargin;
      if not AFixedRowHeight then
        ACurrentCellBottom := Math.Max(ACurrentCellBottom, ANewBottom);
    end;

    AAnchor := TdxTableCellVerticalAnchor.Create(ACurrentCellBottom, ABottomAnchor.BottomTextIndent, ABottomAnchor.CellBorders);
    AMaxAnchor := AColumnController.GetMaxAnchor(ABottomAnchor, AAnchor);
    ABottomAnchor := AMaxAnchor;
    ABottomAnchor.TopTextIndent := Math.Max(ABottomMargin, ABottomAnchor.TopTextIndent);
  end;
  TableViewInfoManager.SetRowSeparatorForCurrentTableViewInfo(ARowSeparatorIndex, ABottomAnchor);
  FCurrentCellRowCount := 0;
  TableViewInfoManager.CurrentCellBottom := ACurrentCellBottom;
end;

procedure TdxTablesControllerTableState.FinishCurrentTable;
var
  I: Integer;
  ATableViewInfo: TdxTableViewInfo;
  ATableViewInfos: TList<TdxTableViewInfo>;
  APendingCells: TList<TdxTableCellViewInfo>;
begin
  ATableViewInfos := TableViewInfoManager.GetTableViewInfos;
  APendingCells := TList<TdxTableCellViewInfo>.Create;
  for I := 0 to ATableViewInfos.Count - 1 do
  begin
    ATableViewInfo := ATableViewInfos[I];
    FTableViewInfoManager.FixColumnOverflow;
    ATableViewInfo.Complete(0, 0);
    ProcessPendingCells(ATableViewInfo, APendingCells);
  end;
  Assert(ATableViewInfos.Count > 0);
  if ATableViewInfos[0].Cells.Count = 0 then
  begin
    Assert(ATableViewInfos.Count > 1);
    ATableViewInfos[1].PrevTableViewInfo := nil;
    ATableViewInfos[0].TopLevelColumn.RemoveTableViewInfoWithContent(ATableViewInfos[0]);
  end;
  if CurrentTable.NestedLevel = 0 then
    TablesController.RemoveAllTableBreaks;
end;

function TdxTablesControllerTableState.GetActualCellBottomMargin(ACell: TdxTableCell): TdxModelUnit;
var
  AMargin: TdxWidthUnit;
begin
  AMargin := ACell.GetActualBottomMargin;
  if AMargin.&Type = TdxWidthUnitType.ModelUnits then
    Result := AMargin.Value
  else
    Result := 0;
end;

function TdxTablesControllerTableState.GetActualCellLeftMargin(ACell: TdxTableCell): TdxModelUnit;
var
  AMargin: TdxWidthUnit;
begin
  AMargin := ACell.GetActualLeftMargin();
  if AMargin.&Type = TdxWidthUnitType.ModelUnits then
    Result := AMargin.Value
  else
    Result := 0;
end;

function TdxTablesControllerTableState.GetActualCellRightMargin(ACell: TdxTableCell): TdxModelUnit;
var
  AMargin: TdxWidthUnit;
begin
  AMargin := ACell.GetActualRightMargin;
  if AMargin.&Type = TdxWidthUnitType.ModelUnits then
    Result := AMargin.Value
  else
    Result := 0;
end;

class function TdxTablesControllerTableState.GetActualCellSpacing(ARow: TdxTableRow): TdxModelUnit;
var
  ASpacing: TdxWidthUnit;
begin
  ASpacing := ARow.CellSpacing;
  if ASpacing.&Type = TdxWidthUnitType.ModelUnits then
    Result := ASpacing.Value * 2
  else
    Result := 0;
end;

function TdxTablesControllerTableState.GetActualCellTopMargin(ACell: TdxTableCell): TdxModelUnit;
var
  AMargin: TdxWidthUnit;
begin
  if ACell = nil then
    Exit(0);
  AMargin := ACell.GetActualTopMargin;
  if AMargin.&Type = TdxWidthUnitType.ModelUnits then
    Result := AMargin.Value
  else
    Result := 0;
end;

function TdxTablesControllerTableState.GetCellBounds(ANewCell: TdxTableCell): TdxLayoutRectangle;
var
  ACellSpacing: TdxWidthUnit;
  ATopMargin: TdxMarginUnitBase;
  AGridPosition: TdxLayoutGridRectangle;
  ATopAnchor: IdxTableCellVerticalAnchor;
  AIsFirstCellInRow, AIsLastCellInRow: Boolean;
  ALeftBorderWidth, ARightBorderWidth: TdxModelUnit;
  AConverter: TdxDocumentModelUnitToLayoutUnitConverter;
  ACellTopMargin, ATopBorderHeight, ACellSpacingValue, AGridColumnCount, ALeftShift, ARightShift: Integer;
begin
  AGridPosition := FCellsBounds[ANewCell];
  Result := AGridPosition.Bounds;
  ATopMargin := ANewCell.GetActualTopMargin;
  if ATopMargin.&Type = TdxWidthUnitType.ModelUnits then
    ACellTopMargin := ATopMargin.Value
  else
    ACellTopMargin := 0;
  ACellTopMargin := ANewCell.DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(ACellTopMargin);
  ATopBorderHeight := FBorderCalculator.GetActualWidth(ANewCell.GetActualTopCellBorder);
  ATopBorderHeight := ANewCell.DocumentModel.ToDocumentLayoutUnitConverter.ToLayoutUnits(ATopBorderHeight);
  ATopAnchor := TableViewInfoManager.GetRowStartAnchor(AGridPosition.RowIndex);
  Result.MoveToTop(ATopAnchor.VerticalPosition + ATopAnchor.BottomTextIndent - ACellTopMargin - ATopBorderHeight);

  ACellSpacing := ANewCell.Row.CellSpacing;
  ACellSpacingValue := ACellSpacing.Value * 2;
  if (ACellSpacing.&Type = TdxWidthUnitType.ModelUnits) and (ACellSpacingValue > 0) then
  begin
    AConverter := ANewCell.DocumentModel.ToDocumentLayoutUnitConverter;
    AIsFirstCellInRow := AGridPosition.ColumnIndex = 0;
    AGridColumnCount := ANewCell.Row.Cells.Last.GetEndColumnIndexConsiderRowGrid + 1;
    AIsLastCellInRow := (AGridPosition.ColumnIndex + AGridPosition.ColumnSpan) = AGridColumnCount;
    if AIsFirstCellInRow then
      ALeftShift :=  ACellSpacingValue
    else
      ALeftShift := ACellSpacingValue div 2;
    if AIsLastCellInRow then
      ARightShift := ACellSpacingValue
    else
      ARightShift := Trunc(ACellSpacingValue - ACellSpacingValue / 2);

    ALeftBorderWidth := VerticalBordersCalculator.GetLeftBorderWidth(FBorderCalculator, ANewCell, True);
    ARightBorderWidth := VerticalBordersCalculator.GetRightBorderWidth(FBorderCalculator, ANewCell);
    Inc(ALeftShift, ALeftBorderWidth div 2);
    Inc(ARightShift, ARightBorderWidth div 2);
    ALeftShift := AConverter.ToLayoutUnits(ALeftShift);
    ARightShift := AConverter.ToLayoutUnits(ARightShift);
    if not AIsFirstCellInRow then
    begin
      Result.MoveToLeft(Result.Left + ALeftShift); 
      Result.Width := Result.Width - ALeftShift + ARightShift; 
    end
    else
      Result.Width := Result.Width - ARightShift;
    if Result.Width < 0 then
      Result.Width := 1;
  end;
end;

function TdxTablesControllerTableState.GetCurrentCell: TdxTableCell;
begin
  Result := CurrentCell;
end;

function TdxTablesControllerTableState.GetCurrentCellViewInfoEmpty: Boolean;
begin
  Result := FCurrentCellRowCount = 0;
end;

function TdxTablesControllerTableState.GetCurrentTable: TdxTable;
begin
  if CurrentCell <> nil then
    Result := CurrentCell.Table
  else
    Result := nil;
end;

function TdxTablesControllerTableState.GetRowOffset(ARow: TdxTableRow): Integer;
var
  AAlignment: TdxTableRowAlignment;
begin
  AAlignment := ARow.TableRowAlignment;
  if (AAlignment <> TdxTableRowAlignment.Center) and (AAlignment <> TdxTableRowAlignment.Right) then
    Exit(0);
  case AAlignment of
    TdxTableRowAlignment.Center:
      Result := (FMaxTableWidth - FTableRight) div 2; 
    TdxTableRowAlignment.Right:
      Result := FMaxTableWidth - FTableRight; 
  else
    Result := 0;
  end;
end;

function TdxTablesControllerTableState.GetRowsController: TdxRowsController;
begin
  Result := TablesController.RowsController;
end;

function TdxTablesControllerTableState.GetRowSpan(AGridPosition: TdxLayoutGridRectangle): Integer;
var
  I, J: Integer;
  ARow: TdxTableRow;
  ACellFound: Boolean;
  ACell: TdxTableCell;
  ARows: TdxTableRowCollection;
  ACells: TdxTableCellCollection;
  ANextGridPosition: TdxLayoutGridRectangle;
begin
  ARows := CurrentTable.Rows;
  Result := 1;
  for I := AGridPosition.RowIndex + 1 to ARows.Count - 1 do
  begin
    ARow := ARows[I];
    ACells := ARow.Cells;
    ACellFound := False;
    for J := 0 to ACells.Count - 1 do
    begin
      ACell := ACells[J];
      if ACell.VerticalMerging = TdxMergingState.Continue then
      begin
        ANextGridPosition := FCellsBounds[ACell];
        if (ANextGridPosition.ColumnIndex = AGridPosition.ColumnIndex) and (ANextGridPosition.ColumnSpan = AGridPosition.ColumnSpan) then
        begin
          ACellFound := True;
          Break;
        end;
      end;
    end;
    if not ACellFound then
      Break;
    Inc(Result);
  end;
end;

function TdxTablesControllerTableState.GetTextBounds(ACell: TdxTableCell;
  ACellBounds: TdxLayoutRectangle): TdxLayoutRectangle;
var
  ATopBorderHeight, ATopMargin, ALeftBorderWidth, ARightBorderWidth, ALeftMargin, ARightMargin,
    ALeftOffsetInModelUnit, ARightOffsetInModelUnit: TdxModelUnit;
  ALeftOffset, AMarginsWidth, ATopOffset: TdxLayoutUnit;
begin
  ATopBorderHeight := FBorderCalculator.GetActualWidth(ACell.GetActualTopCellBorder);
  ATopMargin := GetActualCellTopMargin(ACell);

  ALeftBorderWidth := VerticalBordersCalculator.GetLeftBorderWidth(FBorderCalculator, ACell, True);
  ARightBorderWidth := VerticalBordersCalculator.GetRightBorderWidth(FBorderCalculator, ACell);
  ALeftMargin := GetActualCellLeftMargin(ACell);
  ARightMargin := GetActualCellRightMargin(ACell);

  ALeftOffsetInModelUnit := Math.Max(ALeftMargin, ALeftBorderWidth div 2);
  ARightOffsetInModelUnit := Math.Max(ARightMargin, ARightBorderWidth div 2);
  ALeftOffset := UnitConverter.ToLayoutUnits(ALeftOffsetInModelUnit);
  AMarginsWidth := UnitConverter.ToLayoutUnits(ALeftOffsetInModelUnit + ARightOffsetInModelUnit);
  ATopOffset := UnitConverter.ToLayoutUnits(ATopBorderHeight + ATopMargin);
  Result := TdxLayoutRectangle.Create(ACellBounds.Left + ALeftOffset, ACellBounds.Top + ATopOffset,
    Math.Max(0, ACellBounds.Width - AMarginsWidth), ACellBounds.Height);
end;

function TdxTablesControllerTableState.GetWidth(AGrid: TdxTableGrid; AFirstColumn, ACount: Integer): TdxLayoutUnit;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to ACount - 1 do
    Inc(Result, Math.Max(AGrid.Columns[AFirstColumn + I].Width, 1));
end;

function TdxTablesControllerTableState.IsFirstTableRowViewInfoInColumn: Boolean;
begin
  Result := FTableViewInfoManager.IsFirstTableRowViewInfoInColumn;
end;

procedure TdxTablesControllerTableState.LeaveCurrentTable(ABeforeRestart, ARoolbackParent: Boolean);
var
  AColumnController: TdxTableCellColumnController;
  ANewRowBounds: TdxLayoutRectangle;
begin
  if not ABeforeRestart then
  begin
    FinishCurrentCell;
    FinishCurrentTable;
  end;
  TableViewInfoManager.LeaveCurrentTable(ABeforeRestart);
  AColumnController := TdxTableCellColumnController(RowsController.ColumnController);
  RowsController.SetColumnController(AColumnController.Parent);
  FCurrentCell := nil;
  if ABeforeRestart then
  begin
    if not ARoolbackParent then
      RowsController.CurrentColumn := AColumnController.FirstParentColumn
    else
      RowsController.CurrentColumn := AColumnController.ParentColumn;
  end
  else
    RowsController.CurrentColumn := AColumnController.LastParentColumn;

  if not ABeforeRestart then
  begin
    ANewRowBounds := RowsController.CurrentRow.Bounds;
    ANewRowBounds.MoveToTop(TableViewInfoManager.GetLastTableBottom);
    ANewRowBounds.Width := RowsController.CurrentColumn.Bounds.Width;
    RowsController.CurrentRow.Bounds := ANewRowBounds;
  end
  else
    RowsController.CurrentRow.Bounds := FRowBoundsBeforeTableStart;
end;

procedure TdxTablesControllerTableState.OnCurrentRowFinished;
begin
  Inc(FCurrentCellRowCount);
end;

function TdxTablesControllerTableState.PrepareCellsBounds(ALeftOffset: TdxLayoutUnit): TdxVerticalBorderPositions;
var
  ABounds: TRect;
  ARow: TdxTableRow;
  ACell: TdxTableCell;
  AGrid: TdxTableGrid;
  ALeft, AWidth: TdxLayoutUnit;
  ARows: TdxTableRowCollection;
  ACells: TdxTableCellCollection;
  AGridBounds, ACellBounds: TdxLayoutGridRectangle;
  I, J, ARowCount, AColumnIndex, AOffset, AColumnSpan: Integer;
  AInitialPositions, AShiftedPositions: TdxSortedList<TdxLayoutUnit>;
begin
  AGrid := TableViewInfoManager.GetTableGrid;
  ARows := CurrentTable.Rows;
  AInitialPositions := TdxSortedList<Integer>.Create;
  ARowCount := ARows.Count;
  for I := 0 to ARowCount - 1 do
  begin
    ARow := ARows[I];
    ACells := ARow.Cells;
    ALeft := GetWidth(AGrid, 0, ARow.GridBefore) + ALeftOffset;
    AColumnIndex := ARow.GridBefore;
    AInitialPositions.Add(ALeft);
    for J := 0 to ACells.Count - 1 do
    begin
      ACell := ACells[J];
      AWidth := GetWidth(AGrid, AColumnIndex, ACell.ColumnSpan);
      ACellBounds := FCellsBounds[ACell];
      ACellBounds.Bounds := TdxLayoutRectangle.CreateSize(ALeft, 0, AWidth, 0);
      FCellsBounds[ACell] := ACellBounds;
      Inc(ALeft, AWidth);
      Inc(AColumnIndex, ACell.ColumnSpan);
      AInitialPositions.Add(ALeft);
    end;
    if (I = 0) or (ALeft > FTableRight) then
      FTableRight := ALeft;
  end;
  AShiftedPositions := TdxSortedList<Integer>.Create;
  for I := 0 to ARowCount - 1 do
  begin
    ARow := ARows[I];
    AOffset := GetRowOffset(ARow);
    ShiftRow(AShiftedPositions, ARow, AOffset);
  end;
  for I := 0 to ARowCount - 1 do
  begin
    ARow := ARows[I];
    ARow.LayoutProperties.GridBefore := AShiftedPositions.BinarySearch(FCellsBounds[ARow.Cells.First].Bounds.Left);
    ARow.LayoutProperties.GridAfter := AShiftedPositions.Count - AShiftedPositions.BinarySearch(FCellsBounds[ARow.Cells.Last].Bounds.Right) - 1;
    ACells := ARow.Cells;
    AColumnIndex := ARow.LayoutProperties.GridBefore;
    for J := 0 to ACells.Count - 1 do
    begin
      ACell := ACells[J];
      AGridBounds := FCellsBounds[ACell];
      ABounds := AGridBounds.Bounds;
      AColumnSpan := AShiftedPositions.BinarySearch(ABounds.Right) - AShiftedPositions.BinarySearch(ABounds.Left);
      ACell.LayoutProperties.ColumnSpan := AColumnSpan;
      AGridBounds.ColumnIndex := AColumnIndex;
      AGridBounds.ColumnSpan := AColumnSpan;
      Inc(AColumnIndex, AColumnSpan);
    end;
  end;
  Result := TdxVerticalBorderPositions.Create(AInitialPositions, AShiftedPositions);
end;

procedure TdxTablesControllerTableState.PrepareGridCellBounds;
var
  ARow: TdxTableRow;
  ACell: TdxTableCell;
  ACellBounds: TdxLayoutGridRectangle;
  ARows: TdxTableRowCollection;
  ACells: TdxTableCellCollection;
  I, J, ARowCount, AColumnIndex: Integer;
begin
  ARows := CurrentTable.Rows;
  ARowCount := ARows.Count;
  for I := 0 to ARowCount - 1 do
  begin
    ARow := ARows[I];
    ACells := ARow.Cells;
    AColumnIndex := ARow.GridBefore;
    for J := 0 to ACells.Count - 1 do
    begin
      ACell := ACells[J];
      FCellsBounds.Add(ACell, TdxLayoutGridRectangle.Create(cxNullRect, I, AColumnIndex, ACell.ColumnSpan)); 
      Inc(AColumnIndex, ACell.ColumnSpan);
    end;
  end;
  for I := 0 to ARowCount - 1 do
  begin
    ARow := ARows[I];
    ACells := ARow.Cells;
    for J := 0 to ACells.Count - 1 do
    begin
      ACell := ACells[J];
      ACellBounds := FCellsBounds[ACell];
      case ACell.VerticalMerging of
        TdxMergingState.None:
          ACellBounds.RowSpan := 1;
        TdxMergingState.Continue:
          ACellBounds.RowSpan := 0;
        TdxMergingState.Restart:
          ACellBounds.RowSpan := GetRowSpan(FCellsBounds[ACell]);
      end;
      FCellsBounds[ACell] := ACellBounds;
    end;
  end;
end;

procedure TdxTablesControllerTableState.ProcessPendingCells(ATableViewInfo: TdxTableViewInfo;
  APendingCells: TList<TdxTableCellViewInfo>);
var
  I: Integer;
begin
  for I := APendingCells.Count - 1 downto 0 do
    Assert(False);
end;

function TdxTablesControllerTableState.RollbackToStartOfRowTableOnFirstCellRowColumnOverfull(
  AFirstTableRowViewInfoInColumn, AInnerMostTable: Boolean): TdxParagraphIndex;
var
  ACells: TdxTableCellCollection;
  AParagraphIndex: TdxParagraphIndex;
  I, ANewColumnBottom, ABottomAnchorIndex, AFirstInvalidRowIndex, ANestedLevel: Integer;
begin
  if AFirstTableRowViewInfoInColumn then
  begin
    ABottomAnchorIndex := FTableViewInfoManager.GetCurrentCellBottomAnchorIndex;
    if CurrentCell.Table.NestedLevel = 0 then
    begin
      TablesController.AddInfinityTableBreak(FTableViewInfoManager.GetCurrentTableViewInfoIndex, ABottomAnchorIndex);
      AFirstInvalidRowIndex := CalcFirstInvalidRowIndex;
    end
    else
      AFirstInvalidRowIndex := 0;
  end
  else
  begin
    ANewColumnBottom := FTableViewInfoManager.GetCurrentCellTop;
    if AInnerMostTable then
      TablesController.AddTableBreak(CurrentTable, FTableViewInfoManager.GetCurrentTableViewInfoIndex, ANewColumnBottom);
    AFirstInvalidRowIndex := CalcFirstInvalidRowIndex;
  end;

  TableViewInfoManager.RemoveAllInvalidRowsOnColumnOverfull(AFirstInvalidRowIndex);
  if AFirstInvalidRowIndex = 0 then
  begin
    FRestartFrom := TdxRestartFrom.TableStart;
    AParagraphIndex := CurrentTable.Rows.First.Cells.First.StartParagraphIndex;
    ANestedLevel := CurrentCell.Table.NestedLevel;
    LeaveCurrentTable(True, CurrentTable.NestedLevel > 0);
    TablesController.ReturnToPrevState;
    if ANestedLevel > 0 then
      Exit(-1)
    else
      Exit(AParagraphIndex);
  end
  else
  begin
    ACells := CurrentTable.Rows[AFirstInvalidRowIndex].Cells;
    for I := 0 to ACells.Count - 1 do
      if ACells[I].VerticalMerging <> TdxMergingState.Continue then
      begin
        FRestartFrom := TdxRestartFrom.CellStart;
        Exit(ACells[I].StartParagraphIndex);
      end;
    raise Exception.Create(''); 
    Exit(0); 
  end;
end;

procedure TdxTablesControllerTableState.ShiftRow(APositions: TdxSortedList<TdxLayoutUnit>; ARow: TdxTableRow;
  AOffset: Integer);
var
  I: Integer;
  ABounds: TRect;
  ACell: TdxTableCell;
  ACells: TdxTableCellCollection;
  ACellBounds: TdxLayoutGridRectangle;
begin
  ACells := ARow.Cells;
  for I := 0 to ACells.Count - 1 do
  begin
    ACell := ACells[I];
    ABounds := FCellsBounds[ACell].Bounds;
    ABounds.Offset(AOffset, 0);
    APositions.Add(ABounds.Left);
    ACellBounds := FCellsBounds[ACell];
    ACellBounds.Bounds := ABounds;
    FCellsBounds[ACell] := ACellBounds;
  end;
  APositions.Add(FCellsBounds[ACells.Last].Bounds.Right);
end;

procedure TdxTablesControllerTableState.StartNewTable(ATable: TdxTable; AFirstContentInParentCell: Boolean);
var
  ATableTop: Integer;
  AColumnBounds: TRect;
  ATableTextArea: TdxTextArea;
  ATableViewInfo: TdxTableViewInfo;
  ATableCellController: TdxTableCellColumnController;
  ACellBordersInfo: TList<TdxHorizontalCellBordersInfo>;
  ABounds, ACellBounds, ATextBounds, ANewRowBounds: TdxLayoutRectangle;
  AModelRelativeIndent, AActualTableIndent, AModelLeftOffset, ARightCellMargin: TdxModelUnit;
  ALayoutLeftOffset, ALayoutRightTableBorder, ARightTableBorder, ALayoutRightCellMaring, AMinTableWidth,
    ABottomTextIndent: TdxLayoutUnit;
begin
  AModelRelativeIndent := CalculateTableActualLeftRelativeOffset(ATable);
  if ATable.TableIndent.&Type = TdxWidthUnitType.ModelUnits then
    AActualTableIndent := ATable.TableIndent.Value
  else
    AActualTableIndent := 0;
  AModelLeftOffset := AActualTableIndent + AModelRelativeIndent;
  ALayoutLeftOffset := UnitConverter.ToLayoutUnits(AModelLeftOffset);
  ARightCellMargin := GetActualCellRightMargin(ATable.Rows.First.Cells.First);
  ALayoutRightTableBorder := 0;
  if RowsController.MatchHorizontalTableIndentsToTextEdge then
  begin
    ARightTableBorder := VerticalBordersCalculator.GetRightBorderWidth(FBorderCalculator, ATable.FirstRow.LastCell);
    ALayoutRightTableBorder := UnitConverter.ToLayoutUnits(ARightTableBorder);
  end;
  ALayoutRightCellMaring := UnitConverter.ToLayoutUnits(ARightCellMargin);
  ATableTop := RowsController.CurrentRow.Bounds.Top;
  if (ATable.TableLayout = TdxTableLayoutType.Autofit) and ((ATable.PreferredWidth.&Type = TdxWidthUnitType.Auto) or (ATable.PreferredWidth.&Type = TdxWidthUnitType.&Nil)) then
  begin
    AMinTableWidth := 1;
    ATableTop := RowsController.MoveCurrentRowDownToFitTable(AMinTableWidth, ATableTop);
    ATableTextArea := RowsController.GetTextAreaForTable;
    ATableViewInfo := TableViewInfoManager.StartNewTable(ATable, FCellsBounds, ALayoutLeftOffset, ALayoutRightCellMaring, AFirstContentInParentCell, ATableTextArea, FMaxTableWidth);
    ATableViewInfo.TextAreaOffset := ATableTextArea.Start - RowsController.CurrentColumn.Bounds.Left;
    Inc(ALayoutLeftOffset, ATableViewInfo.TextAreaOffset);
  end
  else
  begin
    AColumnBounds := RowsController.CurrentColumn.Bounds;
    ATableTextArea := TdxTextArea.Create(AColumnBounds.Left, AColumnBounds.Right - ALayoutRightTableBorder);
    ATableViewInfo := TableViewInfoManager.StartNewTable(ATable, FCellsBounds, ALayoutLeftOffset, ALayoutRightCellMaring, AFirstContentInParentCell, ATableTextArea, FMaxTableWidth);
    ATableTop := RowsController.MoveCurrentRowDownToFitTable(AColumnBounds.Width, ATableTop);
    ATableTextArea := RowsController.GetTextAreaForTable;
    ATableViewInfo.TextAreaOffset := ATableTextArea.Start - RowsController.CurrentColumn.Bounds.Left;
    Inc(ALayoutLeftOffset, ATableViewInfo.TextAreaOffset);
  end;

  ATableViewInfo.VerticalBorderPositions := PrepareCellsBounds(ALayoutLeftOffset);
  ATableViewInfo.LeftOffset := ALayoutLeftOffset;
  ATableViewInfo.ModelRelativeIndent := AModelRelativeIndent;
  EnsureCurrentTableRow(CurrentCell.Row);

  ABottomTextIndent := CalculateBottomTextIndent(0, ACellBordersInfo);
  FRowBoundsBeforeTableStart := RowsController.CurrentRow.Bounds;
  TableViewInfoManager.SetRowSeparator(0, TdxTableCellVerticalAnchor.Create(ATableTop, ABottomTextIndent, ACellBordersInfo));

  ABounds := GetCellBounds(CurrentCell);

  ACellBounds := ABounds;

  ACellBounds.Offset(RowsController.CurrentColumn.Bounds.Left, 0);
  ATextBounds := GetTextBounds(CurrentCell, ABounds);

  ATableCellController := TdxTableCellColumnController.Create(RowsController.ColumnController,
    RowsController.CurrentColumn, ATextBounds.Left, ATextBounds.Top, ATextBounds.Width, ATableViewInfo, CurrentCell);
  RowsController.SetColumnController(ATableCellController);
  TableViewInfoManager.ColumnController := ATableCellController;

  TableViewInfoManager.StartNextCell(CurrentCell, ACellBounds, FCellsBounds[CurrentCell], ATextBounds);

  ANewRowBounds := RowsController.CurrentRow.Bounds;
  ANewRowBounds.MoveToTop(ATextBounds.Top);
  RowsController.CurrentRow.Bounds := ANewRowBounds;

  TableViewInfoManager.CurrentCellBottom := ATableViewInfo.TopAnchor.VerticalPosition + ATableViewInfo.TopAnchor.BottomTextIndent;

  RowsController.CurrentColumn := ATableCellController.GetStartColumn;
  RowsController.OnCellStart;
  FCurrentCellRowCount := 0;
end;

procedure TdxTablesControllerTableState.UpdateCurrentCellBottom(ABottom: TdxLayoutUnit);
begin
  if CurrentCell = nil then
    Exit;
  TableViewInfoManager.CurrentCellBottom := Math.Max(ABottom, TableViewInfoManager.CurrentCellBottom);
end;

procedure TdxTablesControllerTableState.UpdateCurrentCellHeight(ARow: TdxRow);
begin
  if CurrentCell = nil then
    Exit;
  TableViewInfoManager.CurrentCellBottom := Math.Max(ARow.Bounds.Bottom, TableViewInfoManager.CurrentCellBottom);
end;

{ TdxTableCellBorderIterator }

constructor TdxTableCellBorderIterator.Create(AAboveRow, ABelowRow: TdxTableRow);
begin
  if (AAboveRow = nil) and (ABelowRow = nil) then
    raise Exception.Create(''); 
  if AAboveRow <> nil then
    FAboveRowIterator := TdxTableCellIterator.Create(AAboveRow)
  else
    FAboveRowIterator := TdxTableCellEmptyIterator.Create;
  if ABelowRow <> nil then
    FBelowRowIterator := TdxTableCellIterator.Create(ABelowRow)
  else
    FBelowRowIterator := TdxTableCellEmptyIterator.Create;
  FCurrentCellAbove := nil;
  FCurrentCellBelow := nil;
  FAboveRowIterator.MoveNextCell;
  FBelowRowIterator.MoveNextCell;
end;

function TdxTableCellBorderIterator.GetCurrentAboveInfo: TdxHorizontalCellBordersInfo;
var
  ABorder: TdxBorderBase;
begin
  if FCurrentCellAbove = nil then
    Exit(nil);
  ABorder := FCurrentCellAbove.GetActualBottomCellBorder;
  Result := TdxHorizontalCellBordersInfo.Create(FCurrentCellAbove, FCurrentCellBelow, ABorder, FCurrentStartColumnIndex, FCurrentEndColumnIndex);
end;

function TdxTableCellBorderIterator.GetCurrentBelowInfo: TdxHorizontalCellBordersInfo;
var
  ABorder: TdxBorderBase;
begin
  if FCurrentCellBelow = nil then
    Exit(nil);
  ABorder := FCurrentCellBelow.GetActualTopCellBorder;
  Result := TdxHorizontalCellBordersInfo.Create(FCurrentCellAbove, FCurrentCellBelow, ABorder, FCurrentStartColumnIndex, FCurrentEndColumnIndex);
end;

function TdxTableCellBorderIterator.GetCurrentMergedInfo: TdxHorizontalCellBordersInfo;
begin
  Assert((FCurrentCellAbove <> nil) and (FCurrentCellBelow <> nil));
  Result := TdxHorizontalCellBordersInfo.Create(FCurrentCellAbove, FCurrentCellBelow, nil, FCurrentStartColumnIndex, FCurrentEndColumnIndex);
end;

function TdxTableCellBorderIterator.IsVerticallyMerged: Boolean;
begin
  if (FCurrentCellAbove = nil) or (FCurrentCellBelow = nil) then
    Exit(False);
  Result := ((FCurrentCellAbove.VerticalMerging = TdxMergingState.Restart) or (FCurrentCellAbove.VerticalMerging = TdxMergingState.Continue)) and
    (FCurrentCellBelow.VerticalMerging = TdxMergingState.Continue);
end;

function TdxTableCellBorderIterator.MoveNext: Boolean;
var
  AAboveStartColumnIndex, ABelowStartColumnIndex: Integer;
begin
  if FAboveRowIterator.EndOfRow and FBelowRowIterator.EndOfRow then
    Exit(False);
  if FAboveRowIterator.EndOfRow then
  begin
    FCurrentStartColumnIndex := FBelowRowIterator.CurrentStartColumnIndex;
    FCurrentEndColumnIndex := FBelowRowIterator.CurrentEndColumnIndex;
    FCurrentCellAbove := nil;
    FCurrentCellBelow := FBelowRowIterator.CurrentCell;
    FBelowRowIterator.SetStartColumnIndex(FCurrentEndColumnIndex + 1);
    Exit(True);
  end;
  if FBelowRowIterator.EndOfRow then
  begin
    FCurrentStartColumnIndex := FAboveRowIterator.CurrentStartColumnIndex;
    FCurrentEndColumnIndex := FAboveRowIterator.CurrentEndColumnIndex;
    FCurrentCellAbove := FAboveRowIterator.CurrentCell;
    FCurrentCellBelow := nil;
    FAboveRowIterator.SetStartColumnIndex(FCurrentEndColumnIndex + 1);
    Exit(True);
  end;
  AAboveStartColumnIndex := FAboveRowIterator.CurrentStartColumnIndex;
  ABelowStartColumnIndex := FBelowRowIterator.CurrentStartColumnIndex;
  if AAboveStartColumnIndex < ABelowStartColumnIndex then
  begin
    FCurrentStartColumnIndex := AAboveStartColumnIndex;
    FCurrentEndColumnIndex := Math.Min(ABelowStartColumnIndex - 1, FAboveRowIterator.CurrentEndColumnIndex);
    FCurrentCellAbove := FAboveRowIterator.CurrentCell;
    FAboveRowIterator.SetStartColumnIndex(FCurrentEndColumnIndex + 1);
    Result := True;
  end
  else
    if ABelowStartColumnIndex < AAboveStartColumnIndex then
    begin
      FCurrentStartColumnIndex := ABelowStartColumnIndex;
      FCurrentEndColumnIndex := Math.Min(AAboveStartColumnIndex - 1, FBelowRowIterator.CurrentEndColumnIndex);
      FCurrentCellBelow := FBelowRowIterator.CurrentCell;
      FBelowRowIterator.SetStartColumnIndex(FCurrentEndColumnIndex + 1);
      Result := True;
    end
    else
    begin
      FCurrentStartColumnIndex := AAboveStartColumnIndex;
      FCurrentEndColumnIndex := Math.Min(FAboveRowIterator.CurrentEndColumnIndex, FBelowRowIterator.CurrentEndColumnIndex);
      FCurrentCellAbove := FAboveRowIterator.CurrentCell;
      FCurrentCellBelow := FBelowRowIterator.CurrentCell;
      FAboveRowIterator.SetStartColumnIndex(FCurrentEndColumnIndex + 1);
      FBelowRowIterator.SetStartColumnIndex(FCurrentEndColumnIndex + 1);
      Result := True;
    end;
end;

{ TdxTableCellHorizontalBorderCalculator }

constructor TdxTableCellHorizontalBorderCalculator.Create(ATable: TdxTable);
begin
  inherited Create;
  FTable := ATable;
end;

function TdxTableCellHorizontalBorderCalculator.GetAnchorBorders(
  AAnchorIndex: Integer): TList<TdxHorizontalCellBordersInfo>;
var
  ARowAboveIndex, ARowBelowIndex: Integer;
  ARowAbove, ARowBelow: TdxTableRow;
  AIterator: TdxTableCellBorderIterator;
  ABorder: TdxHorizontalCellBordersInfo;
  AAboveBorder, ABelowBorder: TdxHorizontalCellBordersInfo;
begin
  ARowAboveIndex := AAnchorIndex - 1;
  ARowBelowIndex := AAnchorIndex;
  if ARowAboveIndex >= 0 then
    ARowAbove := FTable.Rows[ARowAboveIndex]
  else
    ARowAbove := nil;
  if ARowBelowIndex < FTable.Rows.Count then
    ARowBelow := FTable.Rows[ARowBelowIndex]
  else
    ARowBelow := nil;
  AIterator := TdxTableCellBorderIterator.Create(ARowAbove, ARowBelow);
  try
    Result := TList<TdxHorizontalCellBordersInfo>.Create;
    while AIterator.MoveNext do
    begin
      if not AIterator.IsVerticallyMerged then
      begin
        AAboveBorder := AIterator.CurrentAboveInfo;
        ABelowBorder := AIterator.CurrentBelowInfo;
        ABorder := ResolveBorder(FTable, AAboveBorder, ABelowBorder);
      end
      else
        ABorder := AIterator.CurrentMergedInfo;
      Result.Add(ABorder);
    end;
  finally
    AIterator.Free;
  end;
end;

function TdxTableCellHorizontalBorderCalculator.GetBottomBorders(
  ARow: TdxTableRow): TList<TdxHorizontalCellBordersInfo>;
var
  ARowIndex, AAnchorIndex: Integer;
begin
  ARowIndex := FTable.Rows.IndexOf(ARow);
  AAnchorIndex := ARowIndex + 1;
  Result := GetAnchorBorders(AAnchorIndex);
end;

function TdxTableCellHorizontalBorderCalculator.GetTopBorders(ARow: TdxTableRow): TList<TdxHorizontalCellBordersInfo>;
var
  ARowIndex, AAnchorIndex: Integer;
begin
  ARowIndex := FTable.Rows.IndexOf(ARow);
  AAnchorIndex := ARowIndex;
  Result := GetAnchorBorders(AAnchorIndex);
end;

function TdxTableCellHorizontalBorderCalculator.ResolveBorder(ATable: TdxTable; ABorder1,
  ABorder2: TdxHorizontalCellBordersInfo): TdxHorizontalCellBordersInfo;
var
  ABorderCalculator: TdxTableBorderCalculator;
  ABorderInfo: TdxBorderInfo;
begin
  Assert((ABorder1 <> nil) or (ABorder2 <> nil));
  if ABorder1 = nil then
    Exit(ABorder2);
  if ABorder2 = nil then
    Exit(ABorder1);
  ABorderCalculator := TdxTableBorderCalculator.Create;
  try
    ABorderInfo := ABorderCalculator.GetVerticalBorderSource(ATable, ABorder1.Border.Info, ABorder2.Border.Info);
  finally
    ABorderCalculator.Free;
  end;
  if ABorderInfo = ABorder1.Border.Info then
    Result := ABorder1
  else
    Result := ABorder2;
end;

end.
