{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control.Mouse.DragAndDrop;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, Controls, SysUtils, ActiveX,
  dxRichEdit.Utils.DataObject, dxRichEdit.Control.HitTest, dxRichEdit.Commands, dxRichEdit.Utils.OfficeImage,
  dxRichEdit.View.Core, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.View.ViewInfo,
  dxRichEdit.Control.Mouse, dxRichEdit.Control.DragAndDrop.Types,
  dxRichEdit.Control.Mouse.Types;

type
  { TdxRichEditDragCaretCustomVisualizer }

  TdxRichEditDragCaretCustomVisualizer = class abstract 
  public
    procedure Finish; virtual;
    procedure Start; virtual;
    procedure ShowCaret(ACaretLogPosition: TdxDocumentLogPosition); virtual; abstract;
    procedure HideCaret(ACaretLogPosition: TdxDocumentLogPosition); virtual; abstract;
  end;

  { TdxRichEditDragCaretVisualizer }

  TdxRichEditDragCaretVisualizer = class(TdxRichEditDragCaretCustomVisualizer)  
  strict private
    FControl: IdxRichEditControl;
  private
    procedure DrawCaret(ACaretLogPosition: TdxDocumentLogPosition);
    function GetCaret: TdxDragCaret;
  protected
    property Control: IdxRichEditControl read FControl;
    property Caret: TdxDragCaret read GetCaret;
  public
    constructor Create(AControl: IdxRichEditControl);
    procedure Finish; override;
    procedure Start; override;
    procedure ShowCaret(ACaretLogPosition: Integer); override;
    procedure HideCaret(ACaretLogPosition: Integer); override;
  end;

  { TdxBeginMouseDragHelperState }

  TdxBeginMouseDragHelperState = class(TdxRichEditMouseCustomState)
  private
    FInitialPoint: TPoint;
    FDragState: IdxRichEditMouseState;
    function IsDragStarted(const Args: TdxMouseEventArgs): Boolean;
  protected
    function StopClickTimerOnStart: Boolean; override;
    property InitialPoint: TPoint read FInitialPoint;
  public
    constructor Create(AController: TdxRichEditMouseController; ADragState: TdxRichEditMouseCustomState;
      const P: TPoint); reintroduce; virtual;
    destructor Destroy; override;

    procedure HandleMouseMove(const Args: TdxMouseEventArgs); override;
    procedure HandleMouseUp(const Args: TdxMouseEventArgs); override;
    procedure HandleMouseWheel(const Args: TdxMouseEventArgs); override;

    property DragState: IdxRichEditMouseState read FDragState;
  end;

  { TdxRichEditBeginMouseDragHelperState }

  TdxRichEditBeginMouseDragHelperState = class(TdxBeginMouseDragHelperState)
  private
    procedure HandleEndDocumentUpdate(ASender: TObject; E: TdxDocumentUpdateCompleteEventArgs);
  public
    procedure Start; override;
    procedure Finish; override;
    procedure HandleMouseWheel(const Args: TdxMouseEventArgs); override;
  end;

  { TdxBeginContentDragHelperState }

  TdxBeginContentDragHelperState = class(TdxRichEditBeginMouseDragHelperState)
  private
    FResetSelectionOnMouseUp: Boolean;
  public
    constructor Create(AController: TdxRichEditMouseController;
      ADragState: TdxRichEditMouseCustomState; const P: TPoint); override;
    procedure HandleMouseUp(const Args: TdxMouseEventArgs); override;

    property ResetSelectionOnMouseUp: Boolean read FResetSelectionOnMouseUp write FResetSelectionOnMouseUp;
  end;

  { TdxBeginMouseDragHyperlinkClickHandleHelperState }

  TdxBeginMouseDragHyperlinkClickHandleHelperState = class(TdxRichEditBeginMouseDragHelperState)
  private
    procedure InternalHandleMouseUp(const Args: TdxMouseEventArgs);
    procedure ValidateCursorPosition(AControl: IdxRichEditControl);
  public
    procedure HandleMouseUp(const Args: TdxMouseEventArgs); override;
  end;

  { TdxCancellableDragMouseHandlerStateBase }

  TdxCancellableDragMouseHandlerStateBase = class abstract(TdxRichEditMouseCustomState, IdxKeyboardHandlerService)
  strict private
    FDataObject: IdxDataObject;
  protected
    function AutoScrollEnabled: Boolean; override;
    function CanShowToolTip: Boolean; override;
    function StopClickTimerOnStart: Boolean; override;
    function CalculateHitTest(const P: TPoint): TdxRichEditHitTestResult; override;

    function GetDataObject(const AData: IDataObject): IdxDataObject; virtual;

    function HitTestDetailsLevel: TdxDocumentLayoutDetailsLevel; virtual; abstract;
    function HandleDragDropManually: Boolean; virtual;

    function HandleKeyDown(const Args: TdxKeyEventArgs): Boolean;
    function HandleKeyUp(const Args: TdxKeyEventArgs): Boolean;
    function HandleKeyPress(const Args: TdxKeyPressEventArgs): Boolean;

    procedure BeginVisualFeedback; virtual;
    procedure EndVisualFeedback; virtual;
    procedure ShowVisualFeedback; virtual; abstract;
    procedure HideVisualFeedback; virtual; abstract;

    function CalculateMouseCursor(AShiftState: TShiftState): TCursor; virtual; abstract;
    function CreateDataObject: IdxDataObject; virtual; abstract;
    function CommitDrag(const P: TPoint; AShiftState: TShiftState; const ADataObject: IdxDataObject): Boolean; virtual; abstract;
    function ContinueDrag(const Args: TdxDragEventArgs): TdxDragDropEffects; overload;
    function ContinueDrag(const P: TPoint; const AllowedEffects: TdxDragDropEffects; AShiftState: TShiftState; const ADataObject: IdxDataObject): TdxDragDropEffects; overload; virtual; abstract;

    property DataObject: IdxDataObject read FDataObject;
  public
    procedure Start; override;
    procedure Finish; override;
    procedure HandleMouseMove(const Args: TdxMouseEventArgs); override;
    procedure HandleMouseUp(const Args: TdxMouseEventArgs); override;
    procedure HandleMouseWheel(const Args: TdxMouseEventArgs); override;
  end;

  { TdxDragContentMouseHandlerStateCalculator }

  TdxDragContentMouseHandlerStateCalculator = class
  strict private
    FControl: IdxRichEditControl;
  public
    constructor Create(const AControl: IdxRichEditControl);
  public
    function CanDropContentTo(AHitTestResult: TdxRichEditHitTestResult; APieceTable: TdxPieceTable): Boolean;
    function UpdateDocumentModelPosition(const APos: TdxDocumentModelPosition): TdxDocumentModelPosition;
    procedure UpdateVisualState; virtual;
    procedure OnInternalDragStart; virtual;
  end;

  { TdxDragContentMouseHandlerStateBase }

  TdxDragContentMouseHandlerStateBase = class(TdxCancellableDragMouseHandlerStateBase)
  const
    RichEditDataFormatSelection: string = 'dxRichEdit.DocumentModel.Selection';
  private
    FCalculator: TdxDragContentMouseHandlerStateCalculator;
    FCaretLogPosition: TdxDocumentLogPosition;
    FCaretVisualizer: TdxRichEditDragCaretVisualizer;
  protected
    function HitTestDetailsLevel: TdxDocumentLayoutDetailsLevel; override;
    procedure BeginVisualFeedback; override;
    procedure EndVisualFeedback; override;
    procedure ShowVisualFeedback; override;
    procedure HideVisualFeedback; override;

    function CalculateDragDropEffects(ASiftState: TShiftState): TdxDragDropEffects; virtual;
    function CanDropData(const ADataObject: IdxDataObject): Boolean; overload; virtual;
    function CanDropContentTo(AHitTestResult: TdxRichEditHitTestResult): Boolean; virtual;
    function ContinueDrag(const P: TPoint;
      const AllowedEffects: TdxDragDropEffects; AShiftState: TShiftState; const ADataObject: IdxDataObject): TdxDragDropEffects; override;
    function CreateCaretVisualizer: TdxRichEditDragCaretVisualizer; virtual;
    function CreateDropCommand(APos: PdxDocumentModelPosition;
      const ADataObject: IdxDataObject; AIsControlPressed: Boolean): TdxCommand; virtual;
    function GetHitTestDocumentModelPosition(AHitTestResult: TdxRichEditHitTestResult): TdxDocumentModelPosition;
    function ShouldShowVisualFeedback(const P: TPoint): Boolean; virtual;
    function UpdateModelPosition(const APos: TdxDocumentModelPosition): TdxDocumentModelPosition;
    procedure UpdateVisualState; virtual;
    function CalculateMouseCursor(AShiftState: TShiftState): TCursor; override;
    function CommitDrag(const APoint: TPoint; AShiftState: TShiftState; const ADataObject: IdxDataObject): Boolean; override;
    function CreateDataObject: IdxDataObject; override;

    property Calculator: TdxDragContentMouseHandlerStateCalculator read FCalculator;
    property CaretVisualizer: TdxRichEditDragCaretVisualizer read FCaretVisualizer;
  public
    constructor Create(AController: TdxRichEditMouseController); override;
    destructor Destroy; override;

    function CanDropData(const Args: PdxDragEventArgs): Boolean; overload;

    procedure Start; override;
    procedure Finish; override;

    property CaretLogPosition: TdxDocumentLogPosition read FCaretLogPosition write FCaretLogPosition;
  end;

  { TdxDragContentStandardMouseHandlerStateBase }

  TdxDragContentStandardMouseHandlerStateBase = class abstract(TdxDragContentMouseHandlerStateBase)
  protected
    function HandleDragDropManually: Boolean; override;
    procedure SetMouseCursor(ACursor: TCursor); override;

    procedure DoDragOver(Args: PdxDragEventArgs); override;
    procedure DoDragDrop(Args: PdxDragEventArgs); override;
    procedure QueryContinueDrag(Args: PdxQueryContinueDragEventArgs); override;

    procedure DoDragDropCore(Args: PdxDragEventArgs); virtual;
  end;

  { TdxDragContentStandardMouseHandlerState }

  TdxDragContentStandardMouseHandlerState = class(TdxDragContentStandardMouseHandlerStateBase)
  private
    FDragToExternalTarget: Boolean;
  public
    constructor Create(AController: TdxRichEditMouseController); override;
    procedure Start; override;

    procedure DoDragEnter(Args: PdxDragEventArgs); override;
    procedure DoDragLeave; override;
    procedure QueryContinueDrag(Args: PdxQueryContinueDragEventArgs); override;
  end;

  { TdxDragExternalContentMouseHandlerState }

  TdxDragExternalContentMouseHandlerState = class(TdxDragContentStandardMouseHandlerStateBase)
  protected
    function CalculateDragDropEffects(ASiftState: TShiftState): TdxDragDropEffects; override;
    function CreateDataObject: IdxDataObject; override;
    function ContinueDrag(const P: TPoint;
      const AllowedEffects: TdxDragDropEffects; AShiftState: TShiftState; const ADataObject: IdxDataObject): TdxDragDropEffects; override;
    function CommitDrag(const P: TPoint; AShiftState: TShiftState; const ADataObject: IdxDataObject): Boolean; override;
    function CreateDropCommand(APos: PdxDocumentModelPosition;
      const ADataObject: IdxDataObject; AIsControlPressed: Boolean): TdxCommand; override;
  public
    procedure DoDragOver(Args: PdxDragEventArgs); override;
    procedure DoDragLeave; override;
  end;

  { TdxDragContentManuallyMouseHandlerState }

  TdxDragContentManuallyMouseHandlerState = class(TdxDragContentMouseHandlerStateBase)
  public
    procedure Start; override;
  end;

  { TdxDragFloatingObjectMouseHandlerStateCalculator }

  TdxDragFloatingObjectMouseHandlerStateCalculator = class
  private
    FAnchor: TdxFloatingObjectAnchorRun;
    FLogicalPointOffset: TPoint;
  protected
    procedure Init(AAnchor: TdxFloatingObjectAnchorRun; const AOffset: TPoint); virtual;

    property Anchor: TdxFloatingObjectAnchorRun read FAnchor;
    property LogicalPointOffset: TPoint read FLogicalPointOffset;
  public
    function CanDropTo(APoint: TdxRichEditHitTestResult): Boolean; virtual;
  end;

  { TdxDragFloatingObjectManuallyMouseHandlerStateStrategy }

  TdxDragFloatingObjectManuallyMouseHandlerStateStrategy = class
  public
  end;

  { TdxDragFloatingObjectManuallyMouseHandlerState }

  TdxDragFloatingObjectManuallyMouseHandlerState = class(TdxCancellableDragMouseHandlerStateBase) 
  private
    FInitialContentBounds: TRect; 
    FInitialShapeBounds: TRect; 
    FRotationAngle: Double; 
    FRun: TdxFloatingObjectAnchorRun; 
    FImage: TdxOfficeImage; 
    FInitialLogicalClickPoint: TPoint; 
    FClickPointLogicalOffset: TPoint; 
    FOldTopLeftCorner: TPoint; 
    FMinAffectedRunIndex: TdxRunIndex; 
    FPlatformStrategy: TdxDragFloatingObjectManuallyMouseHandlerStateStrategy; 
    FCalculator: TdxDragFloatingObjectMouseHandlerStateCalculator; 
  protected
    function CalculateMouseCursor(AShiftState: TShiftState): TCursor; override;
    function CreateDataObject: IdxDataObject; override;
    function CommitDrag(const P: TPoint; AShiftState: TShiftState; const ADataObject: IdxDataObject): Boolean; override;
    procedure ShowVisualFeedback; override;
    procedure HideVisualFeedback; override;

    function CreatePlatformStrategy: TdxDragFloatingObjectManuallyMouseHandlerStateStrategy; virtual; 
    function CalculateMinAffectedRunIndex(APage: TdxPage): TdxRunIndex; 
    function HitTestDetailsLevel: TdxDocumentLayoutDetailsLevel; override; 
  public
    constructor Create(AMouseHandler: TdxRichEditMouseController; AHitTestResult: TdxRichEditHitTestResult); reintroduce;  

    property FeedbackImage: TdxOfficeImage read FImage; 
    property InitialShapeBounds: TRect read FInitialShapeBounds; 
    property InitialContentBounds: TRect read FInitialContentBounds; 
    property Run: TdxFloatingObjectAnchorRun read FRun; 
    property RotationAngle: Double read FRotationAngle; 
  end;

implementation

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  cxGeometry, cxControls, Windows, dxTypeHelpers,
  dxRichEdit.DocumentModel.FieldRange, dxRichEdit.Commands.DragAndDrop,
  dxRichEdit.Commands.CopyAndPaste, dxRichEdit.Control.Cursors,
  dxRichEdit.Options, dxRichEdit.LayoutEngine.Formatter, dxRichEdit.DocumentModel.Core;

type
  TdxRichEditMouseControllerHelper = class helper for TdxRichEditMouseController
  private
    function GetActiveView: TdxRichEditView;
    function GetControl: IdxRichEditControl;
    function GetState: TdxRichEditMouseCustomState;
  public
    procedure SwitchToDefaultState;
    procedure SwitchStateCore(ANewState: TdxRichEditMouseCustomState; const AMousePosition: TPoint);

    function CreateDragContentMouseHandlerStateCalculator: TdxDragContentMouseHandlerStateCalculator;
    function CreateDragFloatingObjectMouseHandlerStateCalculator: TdxDragFloatingObjectMouseHandlerStateCalculator;
    function IsControlPressed: Boolean;

    property ActiveView: TdxRichEditView read GetActiveView;
    property Control: IdxRichEditControl read GetControl;
    property State: TdxRichEditMouseCustomState read GetState;
  end;

  { TdxRichEditCursorsHelper }

  TdxRichEditCursorsHelper = class helper for TdxRichEditCursors
  public
    class function GetCursor(AEffect: TdxDragDropEffects): TCursor; static;
  end;

{ TdxRichEditMouseControllerHelper }

procedure TdxRichEditMouseControllerHelper.SwitchToDefaultState;
begin
  inherited SwitchToDefaultState;
end;

procedure TdxRichEditMouseControllerHelper.SwitchStateCore(ANewState: TdxRichEditMouseCustomState; const AMousePosition: TPoint);
begin
  inherited SwitchStateCore(ANewState, AMousePosition);
end;

function TdxRichEditMouseControllerHelper.CreateDragContentMouseHandlerStateCalculator: TdxDragContentMouseHandlerStateCalculator;
begin
  Result := TdxDragContentMouseHandlerStateCalculator(inherited CreateDragContentMouseHandlerStateCalculator);
end;

function TdxRichEditMouseControllerHelper.CreateDragFloatingObjectMouseHandlerStateCalculator: TdxDragFloatingObjectMouseHandlerStateCalculator;
begin
  Result := TdxDragFloatingObjectMouseHandlerStateCalculator(inherited CreateDragFloatingObjectMouseHandlerStateCalculator);
end;

function TdxRichEditMouseControllerHelper.IsControlPressed: Boolean;
begin
  Result := inherited IsControlPressed;
end;

function TdxRichEditMouseControllerHelper.GetActiveView: TdxRichEditView;
begin
  Result := inherited ActiveView;
end;

function TdxRichEditMouseControllerHelper.GetControl: IdxRichEditControl;
begin
  Result := inherited Control;
end;

function TdxRichEditMouseControllerHelper.GetState: TdxRichEditMouseCustomState;
begin
  Result := inherited State;
end;

{ TdxRichEditCursorsHelper }

class function TdxRichEditCursorsHelper.GetCursor(AEffect: TdxDragDropEffects): TCursor;
begin
  if TdxDragDropEffect.Scroll in AEffect then
    Result := TdxRichEditCursors.Hand
  else
  if TdxDragDropEffect.Link in AEffect then
    Result := TdxRichEditCursors.Hand
  else
  if TdxDragDropEffect.Copy in AEffect then
    Result := TdxRichEditCursors.Hand
  else
  if TdxDragDropEffect.Move in AEffect then
    Result := TdxRichEditCursors.Hand
  else
    Result := TdxRichEditCursors.Default;
end;

{ TdxRichEditDragCaretCustomVisualizer }

procedure TdxRichEditDragCaretCustomVisualizer.Finish;
begin
end;

procedure TdxRichEditDragCaretCustomVisualizer.Start;
begin
end;

{ TdxRichEditDragCaretVisualizer }

constructor TdxRichEditDragCaretVisualizer.Create(AControl: IdxRichEditControl);
begin
  inherited Create;
  FControl := AControl;
end;

procedure TdxRichEditDragCaretVisualizer.DrawCaret(
  ACaretLogPosition: TdxDocumentLogPosition);
begin
  Caret.SetLogPosition(ACaretLogPosition);
  Control.DrawDragCaret;
end;

procedure TdxRichEditDragCaretVisualizer.Finish;
begin
  Control.DestroyDragCaret;
  inherited Finish;
end;

function TdxRichEditDragCaretVisualizer.GetCaret: TdxDragCaret;
begin
  Result := Control.DragCaret;
end;

procedure TdxRichEditDragCaretVisualizer.HideCaret(ACaretLogPosition: Integer);
begin
  if not Caret.IsHidden then
  begin
    DrawCaret(ACaretLogPosition);
    Caret.IsHidden := True;
  end;
end;

procedure TdxRichEditDragCaretVisualizer.ShowCaret(ACaretLogPosition: Integer);
begin
  if Caret.IsHidden then
  begin
    DrawCaret(ACaretLogPosition);
    Caret.IsHidden := False;
  end;
end;

procedure TdxRichEditDragCaretVisualizer.Start;
begin
  Control.CreateDragCaret;
  inherited Start;
end;

{ TdxBeginMouseDragHelperState }

constructor TdxBeginMouseDragHelperState.Create(
  AController: TdxRichEditMouseController;
  ADragState: TdxRichEditMouseCustomState; const P: TPoint);
begin
  inherited Create(AController);
  FDragState := ADragState;
  FInitialPoint := P;
end;

destructor TdxBeginMouseDragHelperState.Destroy;
begin
  FDragState := nil;
  inherited Destroy;
end;

procedure TdxBeginMouseDragHelperState.HandleMouseMove(
  const Args: TdxMouseEventArgs);
begin
  if IsDragStarted(Args) then
  begin
    LockRelease;
    try
      Controller.SwitchStateCore(DragState.State, Args.MousePos);
      DragState.HandleMouseMove(Args);
    finally
      UnlockRelease;
    end;
  end;
end;

procedure TdxBeginMouseDragHelperState.HandleMouseUp(
  const Args: TdxMouseEventArgs);
begin
  if mbLeft in Args.Buttons then
    Controller.SwitchToDefaultState;
end;

procedure TdxBeginMouseDragHelperState.HandleMouseWheel(const Args: TdxMouseEventArgs);
begin
  Controller.SwitchStateCore(DragState.State, cxNullPoint);
  DragState.HandleMouseWheel(Args);
end;

function TdxBeginMouseDragHelperState.IsDragStarted(
  const Args: TdxMouseEventArgs): Boolean;
begin
  Result := IsPointInDragDetectArea(Args.MousePos, InitialPoint.X, InitialPoint.Y);
end;

function TdxBeginMouseDragHelperState.StopClickTimerOnStart: Boolean;
begin
  Result := False;
end;

{ TdxRichEditBeginMouseDragHelperState }

procedure TdxRichEditBeginMouseDragHelperState.Start;
begin
  inherited Start;
  DocumentModel.EndDocumentUpdate.Add(HandleEndDocumentUpdate);
end;

procedure TdxRichEditBeginMouseDragHelperState.Finish;
begin
  inherited Finish;
  DocumentModel.EndDocumentUpdate.Remove(HandleEndDocumentUpdate);
end;

procedure TdxRichEditBeginMouseDragHelperState.HandleMouseWheel(const Args: TdxMouseEventArgs);
begin
  Controller.SwitchStateCore(DragState.State, InitialPoint);
  DragState.HandleMouseMove(Args);
end;

procedure TdxRichEditBeginMouseDragHelperState.HandleEndDocumentUpdate(ASender: TObject;
  E: TdxDocumentUpdateCompleteEventArgs);
var
  AHandler: IdxEndDocumentUpdateHandler;
begin
  if Supports(DragState, IdxEndDocumentUpdateHandler, AHandler) then
    AHandler.HandleEndDocumentUpdate(E);
end;

{ TdxBeginContentDragHelperState }

constructor TdxBeginContentDragHelperState.Create(AController: TdxRichEditMouseController;
  ADragState: TdxRichEditMouseCustomState; const P: TPoint);
begin
  inherited Create(AController, ADragState, P);
  FResetSelectionOnMouseUp := True;
end;

procedure TdxBeginContentDragHelperState.HandleMouseUp(
  const Args: TdxMouseEventArgs);
var
  ACommand: TdxPlaceCaretToPhysicalPointCommand;
begin
  inherited HandleMouseUp(Args);
  if ResetSelectionOnMouseUp then
  begin
    ACommand := TdxPlaceCaretToPhysicalPointCommand2.Create(Control);
    try
      ACommand.PhysicalPoint := Args.MousePos;
      Control.InnerControl.DocumentModel.Selection.ClearMultiSelection;
      ACommand.ExecuteCore;
    finally
      ACommand.Free;
    end;
  end;
end;

{ TdxBeginMouseDragHyperlinkClickHandleHelperState }

procedure TdxBeginMouseDragHyperlinkClickHandleHelperState.HandleMouseUp(
  const Args: TdxMouseEventArgs);
begin
  LockRelease;
  try
    inherited HandleMouseUp(Args);
    InternalHandleMouseUp(Args);
  finally
    UnlockRelease;
  end;
end;

procedure TdxBeginMouseDragHyperlinkClickHandleHelperState.InternalHandleMouseUp(
  const Args: TdxMouseEventArgs);
var
  AHandler: TdxHyperlinkMouseClickHandler;
begin
  ValidateCursorPosition(Control);
  AHandler := TdxHyperlinkMouseClickHandler.Create(Control);
  try
    AHandler.HandleMouseUp(Args);
  finally
    AHandler.Free;
  end;
end;

procedure TdxBeginMouseDragHyperlinkClickHandleHelperState.ValidateCursorPosition(
  AControl: IdxRichEditControl);
var
  ASelection: TdxSelection;
  ACursorPos, ARunStartPosition: TdxDocumentLogPosition;
  ARunIndex: TdxRunIndex;
  AParagraphIndex: TdxParagraphIndex;
  ARun: TdxTextRunBase;
  ATextFilter: IVisibleTextFilter;
begin
  ASelection := DocumentModel.Selection;
  if ASelection.Length <> 0 then
    Exit;
  ACursorPos := ASelection.Start;
  if ACursorPos <= ActivePieceTable.DocumentStartLogPosition then
    Exit;
  AParagraphIndex := ActivePieceTable.FindParagraphIndex(ACursorPos);
  ARunStartPosition := ActivePieceTable.FindRunStartLogPosition(
    ActivePieceTable.Paragraphs[AParagraphIndex], ACursorPos, ARunIndex);
  if (ARunStartPosition <> ACursorPos) or (ARunIndex = 0) then
    Exit;
  ARun := ActivePieceTable.Runs[ARunIndex - 1];
  if not (ARun is TdxFieldCodeRunBase) then
    Exit;
  ATextFilter := ActivePieceTable.VisibleTextFilter;
  ACursorPos := ATextFilter.GetPrevVisibleLogPosition(ACursorPos + 1, True);
  if (ACursorPos < ActivePieceTable.DocumentStartLogPosition) or (ACursorPos = ASelection.Start) then
    Exit;
  AControl.BeginUpdate;
  try
    ASelection.Start := ACursorPos;
    ASelection.&End := ACursorPos;
  finally
    AControl.EndUpdate;
  end;
end;

{ TdxCancellableDragMouseHandlerStateBase }

function TdxCancellableDragMouseHandlerStateBase.AutoScrollEnabled: Boolean;
begin
  Result := True;
end;

procedure TdxCancellableDragMouseHandlerStateBase.BeginVisualFeedback;
begin
end;

function TdxCancellableDragMouseHandlerStateBase.CalculateHitTest(
  const P: TPoint): TdxRichEditHitTestResult;
var
  ARequest: TdxRichEditHitTestRequest;
begin
  ARequest := TdxRichEditHitTestRequest.Create(DocumentModel.ActivePieceTable);
  try
    ARequest.PhysicalPoint := P;
    ARequest.DetailsLevel := HitTestDetailsLevel;
    ARequest.Accuracy := Controller.ActiveView.DefaultHitTestPageAccuracy or NearestPageArea or
      NearestColumn or NearestTableRow or NearestTableCell or NearestRow or NearestBox or
      NearestCharacter;
    Result := Control.InnerControl.ActiveView.HitTestCore(ARequest, True);
    if not Result.IsValid(HitTestDetailsLevel) then
      FreeAndNil(Result);
  finally
    ARequest.Free;
  end;
end;

function TdxCancellableDragMouseHandlerStateBase.GetDataObject(const AData: IDataObject): IdxDataObject;
begin
  if AData = nil then
    Result := nil
  else
    if not Supports(AData, IdxDataObject, Result) then
      Result := TdxDragAndDropExternalDataObject.Create(AData);
end;

function TdxCancellableDragMouseHandlerStateBase.CanShowToolTip: Boolean;
begin
  Result := True;
end;

procedure TdxCancellableDragMouseHandlerStateBase.EndVisualFeedback;
begin
end;

function TdxCancellableDragMouseHandlerStateBase.ContinueDrag(const Args: TdxDragEventArgs): TdxDragDropEffects;
var
  ADataObject: IdxDataObject;
begin
  ADataObject := GetDataObject(Args.Data);
  try
    Result := ContinueDrag(Args.P, Args.AllowedEffect, Args.KeyState, ADataObject);
  finally
    ADataObject := nil;
  end;
end;

procedure TdxCancellableDragMouseHandlerStateBase.Finish;
begin
  EndVisualFeedback;
  Controller.Control.RemoveKeyboardService(Self);
  inherited Finish;
end;

function TdxCancellableDragMouseHandlerStateBase.HandleDragDropManually: Boolean;
begin
  Result := True;
end;

function TdxCancellableDragMouseHandlerStateBase.HandleKeyDown(const Args: TdxKeyEventArgs): Boolean;
begin
  Result := False;
  if Args.KeyData = VK_ESCAPE then
  begin
    Result := True;
    HideVisualFeedback;
    Controller.SwitchToDefaultState;
    Controller.State.HandleMouseMove(Controller.CreateFakeMouseMoveEventArgs);
  end;
  if Args.Control then
    SetMouseCursor(CalculateMouseCursor(Args.ShiftState));
end;

function TdxCancellableDragMouseHandlerStateBase.HandleKeyUp(const Args: TdxKeyEventArgs): Boolean;
begin
  SetMouseCursor(CalculateMouseCursor(Args.ShiftState));
  Result := False;
end;

function TdxCancellableDragMouseHandlerStateBase.HandleKeyPress(const Args: TdxKeyPressEventArgs): Boolean;
begin
  Result := False;
end;

procedure TdxCancellableDragMouseHandlerStateBase.HandleMouseMove(
  const Args: TdxMouseEventArgs);
begin
  if HandleDragDropManually then
    ContinueDrag(Args.MousePos, [TdxDragDropEffect.Move, TdxDragDropEffect.Copy], Args.Shift, DataObject);
end;

procedure TdxCancellableDragMouseHandlerStateBase.HandleMouseUp(
  const Args: TdxMouseEventArgs);
begin
  if not HandleDragDropManually then
    Exit;
  HideVisualFeedback;
  CommitDrag(Args.MousePos, Args.Shift, nil);
  Controller.SwitchToDefaultState;
end;

procedure TdxCancellableDragMouseHandlerStateBase.HandleMouseWheel(
  const Args: TdxMouseEventArgs);
begin
  inherited HandleMouseWheel(Args);
  ContinueDrag(Args.MousePos, [TdxDragDropEffect.Move, TdxDragDropEffect.Copy], Args.Shift, nil);
end;

procedure TdxCancellableDragMouseHandlerStateBase.Start;
begin
  inherited Start;
  Controller.Control.AddKeyboardService(Self);
  FDataObject := CreateDataObject;
  BeginVisualFeedback;
end;

function TdxCancellableDragMouseHandlerStateBase.StopClickTimerOnStart: Boolean;
begin
  Result := False;
end;

{ TdxDragContentMouseHandlerStateCalculator }

constructor TdxDragContentMouseHandlerStateCalculator.Create(const AControl: IdxRichEditControl);
begin
  inherited Create;
  FControl := AControl;
end;

{ TdxDragContentMouseHandlerStateCalculator }

function TdxDragContentMouseHandlerStateCalculator.CanDropContentTo(AHitTestResult: TdxRichEditHitTestResult; APieceTable: TdxPieceTable): Boolean;
begin
  Result := False;
  if AHitTestResult.DetailsLevel < TdxDocumentLayoutDetailsLevel.Box then
    Exit;
  if AHitTestResult.PageArea.PieceTable = APieceTable then
    Result := True
  else
  begin
    if not APieceTable.ContentType.IsTextBox or (AHitTestResult.FloatingObjectBox = nil) then
      Exit;
    assert(False);
  end;
end;

function TdxDragContentMouseHandlerStateCalculator.UpdateDocumentModelPosition(const APos: TdxDocumentModelPosition): TdxDocumentModelPosition;
begin
  Result := APos;
end;

procedure TdxDragContentMouseHandlerStateCalculator.UpdateVisualState;
begin
  TWinControl(FControl.GetRichEditControl).Invalidate;
end;

procedure TdxDragContentMouseHandlerStateCalculator.OnInternalDragStart;
begin
end;

{ TdxDragContentMouseHandlerStateBase }

constructor TdxDragContentMouseHandlerStateBase.Create(
  AController: TdxRichEditMouseController);
begin
  inherited Create(AController);
  FCaretVisualizer := CreateCaretVisualizer;
  FCalculator := Controller.CreateDragContentMouseHandlerStateCalculator;
end;

destructor TdxDragContentMouseHandlerStateBase.Destroy;
begin
  FreeAndNil(FCalculator);
  FreeAndNil(FCaretVisualizer);
  inherited Destroy;
end;

function TdxDragContentMouseHandlerStateBase.CanDropData(const Args: PdxDragEventArgs): Boolean;
var
  AData: IdxDataObject;
begin
  AData := GetDataObject(Args.Data);
  try
    Result := CanDropData(AData);
  finally
    AData := nil;
  end;
end;

function TdxDragContentMouseHandlerStateBase.CreateCaretVisualizer: TdxRichEditDragCaretVisualizer;
begin
  Result := TdxRichEditDragCaretVisualizer.Create(Control);
end;

function TdxDragContentMouseHandlerStateBase.CreateDropCommand(APos: PdxDocumentModelPosition;
  const ADataObject: IdxDataObject; AIsControlPressed: Boolean): TdxCommand;
begin
  if AIsControlPressed then
    Result := TdxDragCopyContentCommand.Create(Control, APos)
  else
    Result := TdxDragMoveContentCommand.Create(Control, APos);
end;

function TdxDragContentMouseHandlerStateBase.CalculateDragDropEffects(ASiftState: TShiftState): TdxDragDropEffects;
begin
  if ssCtrl in ASiftState then
    Result := [TdxDragDropEffect.Copy]
  else
    Result := [TdxDragDropEffect.Move];
end;

function TdxDragContentMouseHandlerStateBase.CanDropContentTo(
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
begin
  Result := FCalculator.CanDropContentTo(AHitTestResult, DocumentModel.ActivePieceTable);
end;

function TdxDragContentMouseHandlerStateBase.CanDropData(
  const ADataObject: IdxDataObject): Boolean;
var
  ACommand: TdxRichEditCommand;
begin
  if ADataObject = nil then
    Result := False
  else
  begin
    ACommand := TdxPasteDataObjectCoreCommand.Create(Control, ADataObject);
    try
      Result := ACommand.CanExecute;
    finally
      ACommand.Free;
    end;
  end;
end;

function TdxDragContentMouseHandlerStateBase.ContinueDrag(const P: TPoint;
  const AllowedEffects: TdxDragDropEffects; AShiftState: TShiftState;
  const ADataObject: IdxDataObject): TdxDragDropEffects;
var
  AHitTestResult: TdxRichEditHitTestResult;
  APos: TdxDocumentModelPosition;
  AShouldShowVisualFeedback: Boolean;
begin
  AHitTestResult := CalculateHitTest(P);
  try
    if not CanDropData(ADataObject) or (AHitTestResult = nil) or not CanDropContentTo(AHitTestResult) then
    begin
      HideVisualFeedback;
      SetMouseCursor(TdxRichEditCursors.GetCursor([]));
      Exit([TdxDragDropEffect.None]);
    end;
    Result := CalculateDragDropEffects(AShiftState) * AllowedEffects;
    APos := GetHitTestDocumentModelPosition(AHitTestResult);
    AShouldShowVisualFeedback := ShouldShowVisualFeedback(AHitTestResult.LogicalPoint);
    if APos.LogPosition = CaretLogPosition then
    begin
      if AShouldShowVisualFeedback then
        ShowVisualFeedback
      else
        HideVisualFeedback;
      Exit;
    end;
    HideVisualFeedback;
    FCaretLogPosition := APos.LogPosition;
    UpdateVisualState;
    if AShouldShowVisualFeedback then
      ShowVisualFeedback;
    SetMouseCursor(TdxRichEditCursors.GetCursor(Result));
  finally
    AHitTestResult.Free;
  end;
end;

procedure TdxDragContentMouseHandlerStateBase.BeginVisualFeedback;
begin
  CaretVisualizer.Start;
end;

procedure TdxDragContentMouseHandlerStateBase.EndVisualFeedback;
begin
  CaretVisualizer.Finish;
end;

procedure TdxDragContentMouseHandlerStateBase.Finish;
begin
  inherited Finish;
  TWinControl(Control.GetRichEditControl).Invalidate;
end;

function TdxDragContentMouseHandlerStateBase.CalculateMouseCursor(AShiftState: TShiftState): TCursor;
begin
  Result := TdxRichEditCursors.GetCursor(CalculateDragDropEffects(AShiftState));
end;

function TdxDragContentMouseHandlerStateBase.CommitDrag(const APoint: TPoint;
  AShiftState: TShiftState; const ADataObject: IdxDataObject): Boolean;
var
  AHitTestResult: TdxRichEditHitTestResult;
  APos: TdxDocumentModelPosition;
  ACommand: TdxCommand;
begin
  AHitTestResult := CalculateHitTest(APoint);
  try
    Result := (AHitTestResult <> nil) and CanDropContentTo(AHitTestResult);
    if Result then
    begin
      HideVisualFeedback;
      APos := GetHitTestDocumentModelPosition(AHitTestResult);
      ACommand := CreateDropCommand(@APos, ADataObject, ssCtrl in AShiftState);
      try
        ACommand.Execute;
      finally
        ACommand.Free;
      end;
    end;
  finally
    AHitTestResult.Free;
  end;
end;

function TdxDragContentMouseHandlerStateBase.CreateDataObject: IdxDataObject; 
var
  AManager: TdxCopySelectionManager;
  ASelection: TdxSelection;
  AOptions: TdxRtfDocumentExporterOptions;
  ARtf: AnsiString;
  AData: string;
  ASelections: TdxSelectionRangeCollection;
begin
  AManager := TdxCopySelectionManager.Create(Control.InnerControl);
  try
    ASelection := DocumentModel.Selection;
    Result := TdxDragAndDropDataObject.Create;
    Result.Open;
    try
      ASelections := ASelection.GetSortedSelectionCollection;
      try
        AOptions := TdxRtfDocumentExporterOptions.Create;
        try
          AOptions.ExportFinalParagraphMark := TdxExportFinalParagraphMark.Never;
          ARtf := AManager.GetRtfText(ASelection.PieceTable, ASelections, AOptions, True);
          Result.SetData(TdxOfficeDataFormats.Rtf, ARtf);
          AData := AManager.GetPlainText(ASelection.PieceTable, ASelections);
          Result.SetData(TdxOfficeDataFormats.UnicodeText, AData);
          AData := IntToStr(Control.GetRichEditControl.GetHashCode);
          Result.SetData(RichEditDataFormatSelection, AData);
          AData := AManager.GetSuppressStoreImageSizeCollection(ASelection.PieceTable, ASelections);
          Result.SetData(TdxOfficeDataFormats.SuppressStoreImageSize, AData);
        finally
          AOptions.Free;
        end;
      finally
        ASelections.Free;
      end;
    finally
      Result.Close;
    end;
  finally
    AManager.Free;
  end;
end;

function TdxDragContentMouseHandlerStateBase.GetHitTestDocumentModelPosition(
  AHitTestResult: TdxRichEditHitTestResult): TdxDocumentModelPosition;
begin
  if AHitTestResult.Character <> nil then
    Result := AHitTestResult.Character.GetFirstPosition(AHitTestResult.PieceTable)
  else
    Result := AHitTestResult.Box.GetFirstPosition(AHitTestResult.PieceTable);
  Result := UpdateModelPosition(Result);
end;

procedure TdxDragContentMouseHandlerStateBase.HideVisualFeedback;
begin
  CaretVisualizer.HideCaret(CaretLogPosition);
end;

function TdxDragContentMouseHandlerStateBase.HitTestDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.Character;
end;

function TdxDragContentMouseHandlerStateBase.ShouldShowVisualFeedback(
  const P: TPoint): Boolean;
begin
  Result := True;
end;

procedure TdxDragContentMouseHandlerStateBase.ShowVisualFeedback;
begin
  CaretVisualizer.ShowCaret(CaretLogPosition);
end;

procedure TdxDragContentMouseHandlerStateBase.Start;
begin
  inherited Start;
  FCaretLogPosition := -1;
end;

function TdxDragContentMouseHandlerStateBase.UpdateModelPosition(
  const APos: TdxDocumentModelPosition): TdxDocumentModelPosition;
begin
  Result := FCalculator.UpdateDocumentModelPosition(APos);
end;

procedure TdxDragContentMouseHandlerStateBase.UpdateVisualState;
begin
  FCalculator.UpdateVisualState;
end;

{ TdxDragContentStandardMouseHandlerStateBase }

function TdxDragContentStandardMouseHandlerStateBase.HandleDragDropManually: Boolean;
begin
  Result := False;
end;

procedure TdxDragContentStandardMouseHandlerStateBase.SetMouseCursor(
  ACursor: TCursor);
begin
end;

procedure TdxDragContentStandardMouseHandlerStateBase.DoDragOver(Args: PdxDragEventArgs);
begin
  Args.Effect := ContinueDrag(Args^);
end;

procedure TdxDragContentStandardMouseHandlerStateBase.DoDragDrop(Args: PdxDragEventArgs);
begin
  inherited DoDragDrop(Args);
  DoDragDropCore(Args);
  Controller.SwitchToDefaultState;
end;

procedure TdxDragContentStandardMouseHandlerStateBase.QueryContinueDrag(Args: PdxQueryContinueDragEventArgs);
var
  AIsCommandDisabled: Boolean;
begin
  AIsCommandDisabled := not InnerControl.Options.Behavior.DragAllowed;
  if Args.EscapePressed or AIsCommandDisabled then
  begin
    Args.Action := TdxDragAction.Cancel;
    Controller.SwitchToDefaultState;
  end;
end;

procedure TdxDragContentStandardMouseHandlerStateBase.DoDragDropCore(Args: PdxDragEventArgs);
var
  ADataObject: IdxDataObject;
begin
  if Control.InnerControl.Options.Behavior.DropAllowed then
  begin
    ADataObject := GetDataObject(Args.Data);
    try
      CommitDrag(Args.P, Args.KeyState, ADataObject);
    finally
      ADataObject := nil;
    end;
  end;
end;

{ TdxDragContentStandardMouseHandlerState }

constructor TdxDragContentStandardMouseHandlerState.Create(
  AController: TdxRichEditMouseController);
begin
  inherited Create(AController);
  FDragToExternalTarget := True;
end;

procedure TdxDragContentStandardMouseHandlerState.Start;
begin
  inherited Start;
  Calculator.OnInternalDragStart;
  Control.DoDragDrop(DataObject, [TdxDragDropEffect.Move, TdxDragDropEffect.Copy]);
end;

procedure TdxDragContentStandardMouseHandlerState.DoDragEnter(Args: PdxDragEventArgs);
begin
  FDragToExternalTarget := False;
  Args.Effect := ContinueDrag(Args^);
end;

procedure TdxDragContentStandardMouseHandlerState.DoDragLeave;
begin
  FDragToExternalTarget := True;
  inherited DoDragLeave;
  Controller.SwitchToDefaultState;
end;

procedure TdxDragContentStandardMouseHandlerState.QueryContinueDrag(Args: PdxQueryContinueDragEventArgs);
begin
  inherited QueryContinueDrag(Args);
  if (Args.Action = TdxDragAction.Drop) and FDragToExternalTarget then
    Controller.SwitchToDefaultState;
end;

{ TdxDragExternalContentMouseHandlerState }

procedure TdxDragExternalContentMouseHandlerState.DoDragOver(Args: PdxDragEventArgs);
var
  P: TPoint;
  AHitTestResult: TdxRichEditHitTestResult;
begin
  inherited DoDragOver(Args);
  if UseHover then
  begin
    P := Args.P;
    AHitTestResult := CalculateHitTest(P);
    try
      UpdateHover(AHitTestResult);
    finally
      AHitTestResult.Free;
    end;
  end;
end;

procedure TdxDragExternalContentMouseHandlerState.DoDragLeave;
begin
  Controller.SwitchToDefaultState;
end;

function TdxDragExternalContentMouseHandlerState.CalculateDragDropEffects(ASiftState: TShiftState): TdxDragDropEffects;
begin
  Result := [TdxDragDropEffect.Copy];
end;

function TdxDragExternalContentMouseHandlerState.CreateDataObject: IdxDataObject;
begin
  Result := nil;
end;

function TdxDragExternalContentMouseHandlerState.ContinueDrag(const P: TPoint;
  const AllowedEffects: TdxDragDropEffects; AShiftState: TShiftState;
  const ADataObject: IdxDataObject): TdxDragDropEffects;
var
  ACommand: TdxPasteLoadDocumentFromFileCommand;
begin
  Result := inherited ContinueDrag(P, AllowedEffects, AShiftState, ADataObject);
  if Result = [] then
  begin
    ACommand := TdxPasteLoadDocumentFromFileCommand.Create(Control);
    try
      ACommand.PasteSource := TdxDataObjectPasteSource.Create(ADataObject);
      if ACommand.CanExecute then
        Result := CalculateDragDropEffects(AShiftState);
    finally
      ACommand.Free;
    end;
  end;
end;

function TdxDragExternalContentMouseHandlerState.CommitDrag(const P: TPoint; AShiftState: TShiftState;
  const ADataObject: IdxDataObject): Boolean;
var
  ACommand: TdxPasteLoadDocumentFromFileCommand;
begin
  Result := inherited CommitDrag(P, AShiftState, ADataObject);
  if not Result then
  begin
    ACommand := TdxPasteLoadDocumentFromFileCommand.Create(Control);
    try
      ACommand.PasteSource := TdxDataObjectPasteSource.Create(ADataObject);
      ACommand.Execute;
    finally
      ACommand.Free;
    end;
  end;
end;

function TdxDragExternalContentMouseHandlerState.CreateDropCommand(APos: PdxDocumentModelPosition;
  const ADataObject: IdxDataObject; AIsControlPressed: Boolean): TdxCommand;
begin
  Result := TdxDragCopyExternalContentCommand.Create(Control, APos, ADataObject);
end;

{ TdxDragContentManuallyMouseHandlerState }

procedure TdxDragContentManuallyMouseHandlerState.Start;
begin
  inherited Start;
  Calculator.OnInternalDragStart;
end;

{ TdxDragFloatingObjectManuallyMouseHandlerState }

function TdxDragFloatingObjectManuallyMouseHandlerState.CalculateMinAffectedRunIndex(APage: TdxPage): TdxRunIndex;
var
  ARunInfo: TdxRunInfo;
  ASelection: TdxSelection;
  APieceTable: TdxPieceTable;
  AResult: TdxRichEditHitTestResult;
  APageController: TdxPageController;
  ARequest: TdxRichEditHitTestRequest;
  ACalculator: TdxBoxHitTestCalculator;
  AFrameProperties: TdxFrameProperties;
begin
  ASelection := DocumentModel.Selection;
  Assert(ASelection.Length = 1);
  ARunInfo := ASelection.PieceTable.FindRunInfo(ASelection.NormalizedStart, ASelection.Length);
  try
    FRun := ASelection.PieceTable.Runs[ARunInfo.Start.RunIndex] as TdxFloatingObjectAnchorRun; 
    AFrameProperties := ASelection.PieceTable.Runs[ARunInfo.Start.RunIndex].Paragraph.FrameProperties;
  finally
    ARunInfo.Free;
  end;
  APieceTable := FRun.PieceTable;

  if (FRun = nil) and (AFrameProperties <> nil) then
    APieceTable := ASelection.PieceTable;

  ARequest := TdxRichEditHitTestRequest.Create(APieceTable);
  ARequest.LogicalPoint := FOldTopLeftCorner;
  ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Row;
  ARequest.Accuracy := NearestPageArea or NearestColumn or NearestRow; 

  APageController := Control.InnerControl.Formatter.DocumentFormatter.Controller.PageController;
  AResult := TdxRichEditHitTestResult.Create(APageController.DocumentLayout, APieceTable);
  AResult.Page := APage;
  AResult.IncreaseDetailsLevel(TdxDocumentLayoutDetailsLevel.Page);


  ACalculator := APageController.CreateHitTestCalculator(ARequest, AResult);
  ACalculator.CalcHitTest(APage);
  if AResult.Row <> nil then
    Result := AResult.Row.GetFirstPosition(APieceTable).RunIndex
  else
    Result := 0; 
end;

constructor TdxDragFloatingObjectManuallyMouseHandlerState.Create(AMouseHandler: TdxRichEditMouseController;
  AHitTestResult: TdxRichEditHitTestResult);
var
  ABox: TdxFloatingObjectBox;
begin
  inherited Create(AMouseHandler);
  ABox := AHitTestResult.FloatingObjectBox;
  Assert(ABox <> nil);
  FPlatformStrategy := CreatePlatformStrategy;
  FInitialLogicalClickPoint := AHitTestResult.LogicalPoint;
  FOldTopLeftCorner := ABox.Bounds.Location;
  FClickPointLogicalOffset := Point(FOldTopLeftCorner.X - FInitialLogicalClickPoint.X, FOldTopLeftCorner.Y - FInitialLogicalClickPoint.Y);
  FInitialShapeBounds := ABox.Bounds;
  FInitialContentBounds := ABox.ContentBounds;
  FMinAffectedRunIndex := CalculateMinAffectedRunIndex(AHitTestResult.Page);
  FCalculator := AMouseHandler.CreateDragFloatingObjectMouseHandlerStateCalculator;
  FRotationAngle := DocumentModel.GetBoxEffectiveRotationAngleInDegrees(ABox);
end;

function TdxDragFloatingObjectManuallyMouseHandlerState.CreatePlatformStrategy: TdxDragFloatingObjectManuallyMouseHandlerStateStrategy;
begin
  Assert(False);
  Result := nil;
end;

function TdxDragFloatingObjectManuallyMouseHandlerState.CalculateMouseCursor(AShiftState: TShiftState): TCursor;
begin
  Result := TdxRichEditCursors.Default;
end;

function TdxDragFloatingObjectManuallyMouseHandlerState.CreateDataObject: IdxDataObject;
begin
  Result := nil;
end;

function TdxDragFloatingObjectManuallyMouseHandlerState.CommitDrag(const P: TPoint; AShiftState: TShiftState; const ADataObject: IdxDataObject): Boolean;
begin
  Result := Boolean(NotImplemented);
end;

procedure TdxDragFloatingObjectManuallyMouseHandlerState.ShowVisualFeedback;
begin
  NotImplemented;
end;

procedure TdxDragFloatingObjectManuallyMouseHandlerState.HideVisualFeedback;
begin
  NotImplemented;
end;

function TdxDragFloatingObjectManuallyMouseHandlerState.HitTestDetailsLevel: TdxDocumentLayoutDetailsLevel;
begin
  Result := TdxDocumentLayoutDetailsLevel.None;
end;

{ TdxDragFloatingObjectMouseHandlerStateCalculator }

function TdxDragFloatingObjectMouseHandlerStateCalculator.CanDropTo(APoint: TdxRichEditHitTestResult): Boolean;
begin
  Result := True;
end;

procedure TdxDragFloatingObjectMouseHandlerStateCalculator.Init(AAnchor: TdxFloatingObjectAnchorRun;
  const AOffset: TPoint);
begin
  FAnchor := AAnchor;
  FLogicalPointOffset := AOffset;
end;

end.
