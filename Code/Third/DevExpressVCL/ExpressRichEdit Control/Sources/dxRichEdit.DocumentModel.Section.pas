{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.Section;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, Generics.Collections, dxRichEdit.Platform.Font,
  dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.Utils.Types;

type

  TdxPaperKind = (
    A2 = $42,
    A3 = 8,
    A3Extra = $3f,
    A3ExtraTransverse = $44,
    A3Rotated = $4c,
    A3Transverse = $43,
    A4 = 9,
    A4Extra = $35,
    A4Plus = 60,
    A4Rotated = $4d,
    A4Small = 10,
    A4Transverse = $37,
    A5 = 11,
    A5Extra = $40,
    A5Rotated = $4e,
    A5Transverse = $3d,
    A6 = 70,
    A6Rotated = $53,
    APlus = $39,
    B4 = 12,
    B4Envelope = $21,
    B4JisRotated = $4f,
    B5 = 13,
    B5Envelope = $22,
    B5Extra = $41,
    B5JisRotated = 80,
    B5Transverse = $3e,
    B6Envelope = $23,
    B6Jis = $58,
    B6JisRotated = $59,
    BPlus = $3a,
    C3Envelope = $1d,
    C4Envelope = 30,
    C5Envelope = $1c,
    C65Envelope = $20,
    C6Envelope = $1f,
    CSheet = $18,
    Custom = 0,
    DLEnvelope = $1b,
    DSheet = $19,
    ESheet = $1a,
    Executive = 7,
    Folio = 14,
    GermanLegalFanfold = $29,
    GermanStandardFanfold = 40,
    InviteEnvelope = $2f,
    IsoB4 = $2a,
    ItalyEnvelope = $24,
    JapaneseDoublePostcard = $45,
    JapaneseDoublePostcardRotated = $52,
    JapaneseEnvelopeChouNumber3 = $49,
    JapaneseEnvelopeChouNumber3Rotated = $56,
    JapaneseEnvelopeChouNumber4 = $4a,
    JapaneseEnvelopeChouNumber4Rotated = $57,
    JapaneseEnvelopeKakuNumber2 = $47,
    JapaneseEnvelopeKakuNumber2Rotated = $54,
    JapaneseEnvelopeKakuNumber3 = $48,
    JapaneseEnvelopeKakuNumber3Rotated = $55,
    JapaneseEnvelopeYouNumber4 = $5b,
    JapaneseEnvelopeYouNumber4Rotated = $5c,
    JapanesePostcard = $2b,
    JapanesePostcardRotated = $51,
    Ledger = 4,
    Legal = 5,
    LegalExtra = $33,
    Letter = 1,
    LetterExtra = 50,
    LetterExtraTransverse = $38,
    LetterPlus = $3b,
    LetterRotated = $4b,
    LetterSmall = 2,
    LetterTransverse = $36,
    MonarchEnvelope = $25,
    Note = $12,
    Number10Envelope = 20,
    Number11Envelope = $15,
    Number12Envelope = $16,
    Number14Envelope = $17,
    Number9Envelope = $13,
    PersonalEnvelope = $26,
    Prc16K = $5d,
    Prc16KRotated = $6a,
    Prc32K = $5e,
    Prc32KBig = $5f,
    Prc32KBigRotated = $6c,
    Prc32KRotated = $6b,
    PrcEnvelopeNumber1 = $60,
    PrcEnvelopeNumber10 = $69,
    PrcEnvelopeNumber10Rotated = $76,
    PrcEnvelopeNumber1Rotated = $6d,
    PrcEnvelopeNumber2 = $61,
    PrcEnvelopeNumber2Rotated = 110,
    PrcEnvelopeNumber3 = $62,
    PrcEnvelopeNumber3Rotated = $6f,
    PrcEnvelopeNumber4 = $63,
    PrcEnvelopeNumber4Rotated = $70,
    PrcEnvelopeNumber5 = 100,
    PrcEnvelopeNumber5Rotated = $71,
    PrcEnvelopeNumber6 = $65,
    PrcEnvelopeNumber6Rotated = $72,
    PrcEnvelopeNumber7 = $66,
    PrcEnvelopeNumber7Rotated = $73,
    PrcEnvelopeNumber8 = $67,
    PrcEnvelopeNumber8Rotated = $74,
    PrcEnvelopeNumber9 = $68,
    PrcEnvelopeNumber9Rotated = $75,
    Quarto = 15,
    Standard10x11 = $2d,
    Standard10x14 = $10,
    Standard11x17 = $11,
    Standard12x11 = 90,
    Standard15x11 = $2e,
    Standard9x11 = $2c,
    Statement = 6,
    Tabloid = 3,
    TabloidExtra = $34,
    USStandardFanfold = $27);

  TdxNumberingFormat = (
    Decimal = 0,
    AIUEOHiragana,
    AIUEOFullWidthHiragana,
    ArabicAbjad,
    ArabicAlpha,
    Bullet,
    CardinalText,
    Chicago,
    ChineseCounting,
    ChineseCountingThousand,
    ChineseLegalSimplified,
    Chosung,
    DecimalEnclosedCircle,
    DecimalEnclosedCircleChinese,
    DecimalEnclosedFullstop,
    DecimalEnclosedParenthses,
    DecimalFullWidth,
    DecimalFullWidth2,
    DecimalHalfWidth,
    DecimalZero,
    Ganada,
    Hebrew1,
    Hebrew2,
    Hex,
    HindiConsonants,
    HindiDescriptive,
    HindiNumbers,
    HindiVowels,
    IdeographDigital,
    IdeographEnclosedCircle,
    IdeographLegalTraditional,
    IdeographTraditional,
    IdeographZodiac,
    IdeographZodiacTraditional,
    Iroha,
    IrohaFullWidth,
    JapaneseCounting,
    JapaneseDigitalTenThousand,
    JapaneseLegal,
    KoreanCounting,
    KoreanDigital,
    KoreanDigital2,
    KoreanLegal,
    LowerLetter,
    LowerRoman,
    None,
    NumberInDash,
    Ordinal,
    OrdinalText,
    RussianLower,
    RussianUpper,
    TaiwaneseCounting,
    TaiwaneseCountingThousand,
    TaiwaneseDigital,
    ThaiDescriptive,
    ThaiLetters,
    ThaiNumbers,
    UpperLetter,
    UpperRoman,
    VietnameseDescriptive
  );

  TdxSectionGutterAlignment = (Left = 0, Right = 1, Top = 2, Bottom = 3);

  TdxSectionStartType = (NextPage, OddPage, EvenPage, Continuous, Column);

  TdxLineNumberingRestart = (NewPage, NewSection, Continuous);

  TdxMarginsInfo = class(TcxIUnknownObject, IdxCloneable<TdxMarginsInfo>, IdxSupportsCopyFrom<TdxMarginsInfo>, IdxSupportsSizeOf)
  private
    FBottom: Integer;
    FFooterOffset: Integer;
    FGutter: Integer;
    FGutterAlignment: TdxSectionGutterAlignment;
    FHeaderOffset: Integer;
    FLeft: Integer;
    FRight: Integer;
    FTop: Integer;
  public
    constructor Create;
    // IdxCloneable
    function Clone: TdxMarginsInfo;
    // IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxMarginsInfo);

    property Bottom: Integer read FBottom write FBottom;
    property FooterOffset: Integer read FFooterOffset write FFooterOffset;
    property Gutter: Integer read FGutter write FGutter;
    property GutterAlignment: TdxSectionGutterAlignment read FGutterAlignment write FGutterAlignment;
    property HeaderOffset: Integer read FHeaderOffset write FHeaderOffset;
    property Left: Integer read FLeft write FLeft;
    property Right: Integer read FRight write FRight;
    property Top: Integer read FTop write FTop;
  end;

  TdxPageInfo = class(TcxIUnknownObject, IdxCloneable<TdxPageInfo>, IdxSupportsCopyFrom<TdxPageInfo>, IdxSupportsSizeOf)
  private
    FHeight: Integer;
    FLandscape: Boolean;
    FPaperKind: TdxPaperKind;
    FWidth: Integer;
  public
    constructor Create;
    // IdxCloneable
    function Clone: TdxPageInfo;
    // IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxPageInfo);

    procedure ValidatePaperKind(AUnitConverter: TdxDocumentModelUnitConverter);

    property Height: Integer read FHeight write FHeight;
    property Landscape: Boolean read FLandscape write FLandscape;
    property PaperKind: TdxPaperKind read FPaperKind write FPaperKind;
    property Width: Integer read FWidth write FWidth;
  end;
  TdxPageInfoClass = class of TdxPageInfo;

  TdxGeneralSectionInfo = class(TcxIUnknownObject, IdxCloneable<TdxGeneralSectionInfo>, IdxSupportsCopyFrom<TdxGeneralSectionInfo>, IdxSupportsSizeOf)
  private
    FDifferentFirstPage: Boolean;
    FFirstPagePaperSource: Integer;
    FOnlyAllowEditingOfFormFields: Boolean;
    FOtherPagePaperSource: Integer;
    FStartType: TdxSectionStartType;
    FTextDirection: TdxTextDirection;
    FVerticalTextAlignment: TdxVerticalAlignment;
  public
    constructor Create;
    // IdxCloneable
    function Clone: TdxGeneralSectionInfo;
    // IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxGeneralSectionInfo);

    property DifferentFirstPage: Boolean read FDifferentFirstPage write FDifferentFirstPage;
    property FirstPagePaperSource: Integer read FFirstPagePaperSource write FFirstPagePaperSource;
    property OnlyAllowEditingOfFormFields: Boolean read FOnlyAllowEditingOfFormFields write FOnlyAllowEditingOfFormFields;
    property OtherPagePaperSource: Integer read FOtherPagePaperSource write FOtherPagePaperSource;
    property StartType: TdxSectionStartType read FStartType write FStartType;
    property TextDirection: TdxTextDirection read FTextDirection write FTextDirection;
    property VerticalTextAlignment: TdxVerticalAlignment read FVerticalTextAlignment write FVerticalTextAlignment;
  end;
  TdxGeneralSectionInfoClass = class of TdxGeneralSectionInfo;

  TdxPageNumberingInfo = class(TcxIUnknownObject, IdxCloneable<TdxPageNumberingInfo>, IdxSupportsCopyFrom<TdxPageNumberingInfo>, IdxSupportsSizeOf)
  private
    FChapterSeparator: Char;
    FChapterHeaderStyle: Integer;
    FNumberingFormat: TdxNumberingFormat;
    FStartingPageNumber: Integer;
  public
    // IdxCloneable
    function Clone: TdxPageNumberingInfo;
    // IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxPageNumberingInfo);

    property ChapterSeparator: Char read FChapterSeparator write FChapterSeparator;
    property ChapterHeaderStyle: Integer read FChapterHeaderStyle write FChapterHeaderStyle;
    property NumberingFormat: TdxNumberingFormat read FNumberingFormat write FNumberingFormat;
    property StartingPageNumber: Integer read FStartingPageNumber write FStartingPageNumber;
  end;

  TdxLineNumberingInfo = class(TInterfacedPersistent, IdxCloneable<TdxLineNumberingInfo>,
    IdxSupportsCopyFrom<TdxLineNumberingInfo>, IdxSupportsSizeOf)
  private
    FDistance: Integer;
    FNumberingRestartType: TdxLineNumberingRestart;
    FStartingLineNumber: Integer;
    FStep: Integer;
  public
    // IdxCloneable
    function Clone: TdxLineNumberingInfo;
    // IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxLineNumberingInfo);

    property Distance: Integer read FDistance write FDistance;
    property NumberingRestartType: TdxLineNumberingRestart read FNumberingRestartType write FNumberingRestartType;
    property StartingLineNumber: Integer read FStartingLineNumber write FStartingLineNumber;
    property Step: Integer read FStep write FStep;
  end;

  TdxPaperSizeCalculator = class
  private
    FPaperSizeTable: TDictionary<TdxPaperKind, TSize>;
    procedure PopulateSizeTable;
  public
    constructor Create;
    destructor Destroy; override;

    class function CalculatePaperKind(const ASize: TSize; const ADefaultValue: TdxPaperKind): TdxPaperKind; overload;
    class function CalculatePaperKind(const ASize: TSize; const ADefaultValue: TdxPaperKind;
      ATolerance: Integer; const ABadSizeDefaultValue: TdxPaperKind): TdxPaperKind; overload;
    class function CalculatePaperSize(const APaperKind: TdxPaperKind): TSize;

    property PaperSizeTable: TDictionary<TdxPaperKind, TSize> read FPaperSizeTable;
  end;

implementation

uses
  cxGeometry;

{ TdxMarginsInfo }

function TdxMarginsInfo.Clone: TdxMarginsInfo;
begin
  Result := TdxMarginsInfo.Create;
  Result.CopyFrom(Self);
end;

constructor TdxMarginsInfo.Create;
begin
  inherited Create;
end;

procedure TdxMarginsInfo.CopyFrom(const Source: TdxMarginsInfo);
begin
  FBottom := Source.Bottom;
  FFooterOffset := Source.FooterOffset;
  FGutter := Source.Gutter;
  FGutterAlignment := Source.GutterAlignment;
  FHeaderOffset := Source.HeaderOffset;
  FLeft := Source.Left;
  FRight := Source.Right;
  FTop := Source.Top;
end;


{ TdxPageInfo }

constructor TdxPageInfo.Create;
begin
  inherited Create;
end;

procedure TdxPageInfo.CopyFrom(const Source: TdxPageInfo);
begin
  Height := Source.Height;
  Landscape := Source.Landscape;
  PaperKind := Source.PaperKind;
  Width := Source.Width;
end;

function TdxPageInfo.Clone: TdxPageInfo;
begin
  Result := TdxPageInfoClass(ClassType).Create;
  Result.CopyFrom(Self);
end;

procedure TdxPageInfo.ValidatePaperKind(AUnitConverter: TdxDocumentModelUnitConverter);
var
  APaperKindSize, ASize: TSize;
  APaperKind: TdxPaperKind;
begin
  APaperKindSize := TdxPaperSizeCalculator.CalculatePaperSize(PaperKind);
  if Width <= 0 then
  begin
    if Landscape then
      Width := AUnitConverter.TwipsToModelUnits(APaperKindSize.cy)
    else
      Width := AUnitConverter.TwipsToModelUnits(APaperKindSize.cx);
  end;

  if Height <= 0 then
  begin
    if Landscape then
      Height := AUnitConverter.TwipsToModelUnits(APaperKindSize.cx)
    else
      Height := AUnitConverter.TwipsToModelUnits(APaperKindSize.cy);
  end;
  if Landscape then
    ASize := cxSize(Height, Width)
  else
    ASize := cxSize(Width, Height);
  ASize := AUnitConverter.ModelUnitsToTwips(ASize);

  APaperKind := TdxPaperSizeCalculator.CalculatePaperKind(ASize, TdxPaperKind.Custom, 0, TdxPaperKind.Letter);
  PaperKind := APaperKind;
end;

{ TdxGeneralSectionInfo }

constructor TdxGeneralSectionInfo.Create;
begin
  inherited Create;
  FStartType := TdxSectionStartType.NextPage;
end;

procedure TdxGeneralSectionInfo.CopyFrom(const Source: TdxGeneralSectionInfo);
begin
  DifferentFirstPage := Source.DifferentFirstPage;
  FirstPagePaperSource := Source.FirstPagePaperSource;
  OnlyAllowEditingOfFormFields := Source.OnlyAllowEditingOfFormFields;
  OtherPagePaperSource := Source.OtherPagePaperSource;
  StartType := Source.StartType;
  TextDirection := Source.TextDirection;
  VerticalTextAlignment := Source.VerticalTextAlignment;
end;

function TdxGeneralSectionInfo.Clone: TdxGeneralSectionInfo;
begin
  Result := TdxGeneralSectionInfoClass(ClassType).Create;
  Result.CopyFrom(Self);
end;

{ TdxPageNumberingInfo }

procedure TdxPageNumberingInfo.CopyFrom(const Source: TdxPageNumberingInfo);
begin
  FChapterSeparator := Source.ChapterSeparator;
  FChapterHeaderStyle := Source.ChapterHeaderStyle;
  FNumberingFormat :=  Source.NumberingFormat;
  FStartingPageNumber := Source.StartingPageNumber;
end;

function TdxPageNumberingInfo.Clone: TdxPageNumberingInfo;
begin
  Result := TdxPageNumberingInfo.Create;
  Result.CopyFrom(Self);
end;

{ TdxLineNumberingInfo }

procedure TdxLineNumberingInfo.CopyFrom(const Source: TdxLineNumberingInfo);
var
  ASource: TdxLineNumberingInfo;
begin
  ASource := TdxLineNumberingInfo(Source);
  FDistance := ASource.Distance;
  FNumberingRestartType := ASource.NumberingRestartType;
  FStartingLineNumber := ASource.StartingLineNumber;
  FStep := ASource.Step;
end;

function TdxLineNumberingInfo.Clone: TdxLineNumberingInfo;
begin
  Result := TdxLineNumberingInfo.Create;
  Result.CopyFrom(Self);
end;

{ TdxPaperSizeCalculator }

class function TdxPaperSizeCalculator.CalculatePaperKind(
  const ASize: TSize; const ADefaultValue: TdxPaperKind): TdxPaperKind;
begin
  Result := CalculatePaperKind(ASize, ADefaultValue, 0, ADefaultValue);
end;

class function TdxPaperSizeCalculator.CalculatePaperKind(const ASize: TSize;
  const ADefaultValue: TdxPaperKind; ATolerance: Integer; const ABadSizeDefaultValue: TdxPaperKind): TdxPaperKind;
var
  ACalculator: TdxPaperSizeCalculator;
  AItem: TPair<TdxPaperKind, TSize>;
begin
  ACalculator := TdxPaperSizeCalculator.Create;
  try
    if (ASize.cx = 0) or (ASize.cy = 0) then
      Result := ABadSizeDefaultValue
    else
    begin
      Result := ADefaultValue;
      for AItem in ACalculator.PaperSizeTable do
      begin
        if (Abs(ASize.cx - AItem.Value.cx) <= ATolerance) and
            (Abs(ASize.cy - AItem.Value.cy) <= ATolerance) then
          Exit(AItem.Key)
      end;
    end;
  finally
    ACalculator.Free;
  end;
end;

class function TdxPaperSizeCalculator.CalculatePaperSize(
  const APaperKind: TdxPaperKind): TSize;
var
  ACalculator: TdxPaperSizeCalculator;
begin
  ACalculator := TdxPaperSizeCalculator.Create;
  try
    if not ACalculator.PaperSizeTable.TryGetValue(APaperKind, Result) then
      Result := cxSize(12240, 15840);
  finally
    ACalculator.Free;
  end;
end;

constructor TdxPaperSizeCalculator.Create;
begin
  inherited Create;
  FPaperSizeTable := TDictionary<TdxPaperKind, TSize>.Create;
  PopulateSizeTable;
end;

destructor TdxPaperSizeCalculator.Destroy;
begin
  FreeAndNil(FPaperSizeTable);
  inherited;
end;

procedure TdxPaperSizeCalculator.PopulateSizeTable;
begin
  FPaperSizeTable.Add(TdxPaperKind.Letter, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.LetterSmall, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.Tabloid, cxSize(15840, 24480));
  FPaperSizeTable.Add(TdxPaperKind.Ledger, cxSize(24480, 15840));
  FPaperSizeTable.Add(TdxPaperKind.Legal, cxSize(12240, 20160));
  FPaperSizeTable.Add(TdxPaperKind.Statement, cxSize(7920, 12240));
  FPaperSizeTable.Add(TdxPaperKind.Executive, cxSize(10440, 15120));
  FPaperSizeTable.Add(TdxPaperKind.A3, cxSize(16839, 23814));
  FPaperSizeTable.Add(TdxPaperKind.A4, cxSize(11907, 16839));
  FPaperSizeTable.Add(TdxPaperKind.A4Small, cxSize(11907, 16839));
  FPaperSizeTable.Add(TdxPaperKind.A5, cxSize(8391, 11907));
  FPaperSizeTable.Add(TdxPaperKind.B4, cxSize(14572, 20639));
  FPaperSizeTable.Add(TdxPaperKind.B5, cxSize(10319, 14571));
  FPaperSizeTable.Add(TdxPaperKind.Folio, cxSize(12240, 18720));
  FPaperSizeTable.Add(TdxPaperKind.Quarto, cxSize(12189, 15591));
  FPaperSizeTable.Add(TdxPaperKind.Standard10x14, cxSize(14400, 20160));
  FPaperSizeTable.Add(TdxPaperKind.Standard11x17, cxSize(15840, 24480));
  FPaperSizeTable.Add(TdxPaperKind.Note, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.Number9Envelope, cxSize(5580, 12780));
  FPaperSizeTable.Add(TdxPaperKind.Number10Envelope, cxSize(5940, 13680));
  FPaperSizeTable.Add(TdxPaperKind.Number11Envelope, cxSize(6480, 14940));
  FPaperSizeTable.Add(TdxPaperKind.Number12Envelope, cxSize(6840, 15840));
  FPaperSizeTable.Add(TdxPaperKind.Number14Envelope, cxSize(7200, 16560));
  FPaperSizeTable.Add(TdxPaperKind.CSheet, cxSize(24480, 31680));
  FPaperSizeTable.Add(TdxPaperKind.DSheet, cxSize(31680, 48960));
  FPaperSizeTable.Add(TdxPaperKind.ESheet, cxSize(48960, 63360));
  FPaperSizeTable.Add(TdxPaperKind.DLEnvelope, cxSize(6237, 12474));
  FPaperSizeTable.Add(TdxPaperKind.C5Envelope, cxSize(9185, 12984));
  FPaperSizeTable.Add(TdxPaperKind.C3Envelope, cxSize(18369, 25965));
  FPaperSizeTable.Add(TdxPaperKind.C4Envelope, cxSize(12983, 18369));
  FPaperSizeTable.Add(TdxPaperKind.C6Envelope, cxSize(6463, 9184));
  FPaperSizeTable.Add(TdxPaperKind.C65Envelope, cxSize(6463, 12983));
  FPaperSizeTable.Add(TdxPaperKind.B4Envelope, cxSize(14173, 20013));
  FPaperSizeTable.Add(TdxPaperKind.B5Envelope, cxSize(9978, 14173));
  FPaperSizeTable.Add(TdxPaperKind.B6Envelope, cxSize(9978, 7087));
  FPaperSizeTable.Add(TdxPaperKind.ItalyEnvelope, cxSize(6236, 13039));
  FPaperSizeTable.Add(TdxPaperKind.MonarchEnvelope, cxSize(5580, 10800));
  FPaperSizeTable.Add(TdxPaperKind.PersonalEnvelope, cxSize(5220, 9360));
  FPaperSizeTable.Add(TdxPaperKind.USStandardFanfold, cxSize(21420, 15840));
  FPaperSizeTable.Add(TdxPaperKind.GermanStandardFanfold, cxSize(12240, 17280));
  FPaperSizeTable.Add(TdxPaperKind.GermanLegalFanfold, cxSize(12240, 18720));
  FPaperSizeTable.Add(TdxPaperKind.IsoB4, cxSize(14173, 20013));
  FPaperSizeTable.Add(TdxPaperKind.JapanesePostcard, cxSize(5669, 8391));
  FPaperSizeTable.Add(TdxPaperKind.Standard9x11, cxSize(12960, 15840));
  FPaperSizeTable.Add(TdxPaperKind.Standard10x11, cxSize(14400, 15840));
  FPaperSizeTable.Add(TdxPaperKind.Standard15x11, cxSize(21600, 15840));
  FPaperSizeTable.Add(TdxPaperKind.InviteEnvelope, cxSize(12472, 12472));
  FPaperSizeTable.Add(TdxPaperKind.LetterExtra, cxSize(13680, 17280));
  FPaperSizeTable.Add(TdxPaperKind.LegalExtra, cxSize(13680, 21600));
  FPaperSizeTable.Add(TdxPaperKind.TabloidExtra, cxSize(16834, 25920));
  FPaperSizeTable.Add(TdxPaperKind.A4Extra, cxSize(13349, 18274));
  FPaperSizeTable.Add(TdxPaperKind.LetterTransverse, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.A4Transverse, cxSize(11907, 16839));
  FPaperSizeTable.Add(TdxPaperKind.LetterExtraTransverse, cxSize(13680, 17280));
  FPaperSizeTable.Add(TdxPaperKind.APlus, cxSize(12869, 20183));
  FPaperSizeTable.Add(TdxPaperKind.BPlus, cxSize(17291, 27609));
  FPaperSizeTable.Add(TdxPaperKind.LetterPlus, cxSize(12240, 18274));
  FPaperSizeTable.Add(TdxPaperKind.A4Plus, cxSize(11907, 18709));
  FPaperSizeTable.Add(TdxPaperKind.A5Transverse, cxSize(8391, 11907));
  FPaperSizeTable.Add(TdxPaperKind.B5Transverse, cxSize(10319, 14571));
  FPaperSizeTable.Add(TdxPaperKind.A3Extra, cxSize(18255, 25228));
  FPaperSizeTable.Add(TdxPaperKind.A5Extra, cxSize(9865, 13323));
  FPaperSizeTable.Add(TdxPaperKind.B5Extra, cxSize(11395, 15647));
  FPaperSizeTable.Add(TdxPaperKind.A2, cxSize(23811, 33676));
  FPaperSizeTable.Add(TdxPaperKind.A3Transverse, cxSize(16839, 23814));
  FPaperSizeTable.Add(TdxPaperKind.A3ExtraTransverse, cxSize(18255, 25228));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseDoublePostcard, cxSize(11339, 8391));
  FPaperSizeTable.Add(TdxPaperKind.A6, cxSize(5953, 8391));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeKakuNumber2, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeKakuNumber3, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeChouNumber3, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeChouNumber4, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.LetterRotated, cxSize(15840, 12240));
  FPaperSizeTable.Add(TdxPaperKind.A3Rotated, cxSize(23814, 16839));
  FPaperSizeTable.Add(TdxPaperKind.A4Rotated, cxSize(16839, 11907));
  FPaperSizeTable.Add(TdxPaperKind.A5Rotated, cxSize(11907, 8391));
  FPaperSizeTable.Add(TdxPaperKind.B4JisRotated, cxSize(20636, 14570));
  FPaperSizeTable.Add(TdxPaperKind.B5JisRotated, cxSize(14570, 10318));
  FPaperSizeTable.Add(TdxPaperKind.JapanesePostcardRotated, cxSize(8391, 5669));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseDoublePostcardRotated, cxSize(8391, 11339));
  FPaperSizeTable.Add(TdxPaperKind.A6Rotated, cxSize(8391, 5953));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeKakuNumber2Rotated, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeKakuNumber3Rotated, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeChouNumber3Rotated, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeChouNumber4Rotated, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.B6Jis, cxSize(7257, 10318));
  FPaperSizeTable.Add(TdxPaperKind.B6JisRotated, cxSize(10318, 7257));
  FPaperSizeTable.Add(TdxPaperKind.Standard12x11, cxSize(17280, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeYouNumber4, cxSize(12240, 15840));
  FPaperSizeTable.Add(TdxPaperKind.JapaneseEnvelopeYouNumber4Rotated, cxSize(15840, 12240));
  FPaperSizeTable.Add(TdxPaperKind.Prc16K, cxSize(8277, 12189));
  FPaperSizeTable.Add(TdxPaperKind.Prc32K, cxSize(5499, 8561));
  FPaperSizeTable.Add(TdxPaperKind.Prc32KBig, cxSize(5499, 8561));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber1, cxSize(5783, 9354));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber2, cxSize(5783, 9978));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber3, cxSize(7087, 9978));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber4, cxSize(6236, 11792));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber5, cxSize(6236, 12472));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber6, cxSize(6803, 13039));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber7, cxSize(9071, 13039));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber8, cxSize(6803, 17518));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber9, cxSize(12983, 18369));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber10, cxSize(18369, 25965));
  FPaperSizeTable.Add(TdxPaperKind.Prc16KRotated, cxSize(12189, 8277));
  FPaperSizeTable.Add(TdxPaperKind.Prc32KRotated, cxSize(8561, 5499));
  FPaperSizeTable.Add(TdxPaperKind.Prc32KBigRotated, cxSize(8561, 5499));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber1Rotated, cxSize(9354, 5783));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber2Rotated, cxSize(9978, 5783));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber3Rotated, cxSize(9978, 7087));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber4Rotated, cxSize(11792, 6236));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber5Rotated, cxSize(12472, 6236));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber6Rotated, cxSize(13039, 6803));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber7Rotated, cxSize(13039, 9071));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber8Rotated, cxSize(17518, 6803));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber9Rotated, cxSize(18369, 12983));
  FPaperSizeTable.Add(TdxPaperKind.PrcEnvelopeNumber10Rotated, cxSize(25965, 18369));
end;

end.
