{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Export.Html;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Classes, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.Core,
  dxRichEdit.Options;

type
  { TdxHtmlExporterBase }

  TdxHtmlExporterBase = class abstract
  private
    FDocumentModel: TdxCustomDocumentModel;
  protected
    function EmbedImages: Boolean; virtual; abstract;
    function ExportToBodyTag: Boolean; virtual; abstract;
    function ExportStylesAsStyleTag: Boolean; virtual; abstract;
    function ExportStylesAsLink: Boolean; virtual; abstract;
    function Encoding: TEncoding; virtual; abstract;
    function UseHtml5: Boolean; virtual;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel);
  end;

  { TdxHtmlExporter }

  TdxHtmlExporter = class(TdxHtmlExporterBase)
  strict private
    FDocumentModel: TdxDocumentModel;
    FOptions: TdxHtmlDocumentExporterOptions;
  private
  protected
    function EmbedImages: Boolean; override;
    function ExportToBodyTag: Boolean; override;
    function ExportStylesAsStyleTag: Boolean; override;
    function ExportStylesAsLink: Boolean; override;
    function Encoding: TEncoding; override;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AOptions: TdxHtmlDocumentExporterOptions);

    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property Options: TdxHtmlDocumentExporterOptions read FOptions;
  end;

implementation

{ TdxHtmlExporterBase }

constructor TdxHtmlExporterBase.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
end;

function TdxHtmlExporterBase.UseHtml5: Boolean;
begin
  Result := False;
end;

{ TdxHtmlExporter }

constructor TdxHtmlExporter.Create(ADocumentModel: TdxDocumentModel;
  AOptions: TdxHtmlDocumentExporterOptions);
begin
  inherited Create(ADocumentModel);
  FDocumentModel := ADocumentModel;
  FOptions := AOptions;
  NotImplemented;
end;

function TdxHtmlExporter.EmbedImages: Boolean;
begin
  Result := Boolean(NotImplemented);
end;

function TdxHtmlExporter.ExportToBodyTag: Boolean;
begin
  Result := Boolean(NotImplemented);
end;

function TdxHtmlExporter.ExportStylesAsStyleTag: Boolean;
begin
  Result := Boolean(NotImplemented);
end;

function TdxHtmlExporter.ExportStylesAsLink: Boolean;
begin
  Result := Boolean(NotImplemented);
end;

function TdxHtmlExporter.Encoding: TEncoding;
begin
  Result := Options.Encoding;
end;

end.
