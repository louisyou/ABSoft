{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Platform.Win.Painter;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Classes, Types, SysUtils, Graphics,
  dxCore, cxGeometry, dxCoreClasses, cxGraphics, dxGDIPlusAPI, dxGDIPlusClasses,
  dxRichEdit.Utils.OfficeImage,
  dxRichEdit.DocumentModel.PieceTable,
  dxRichEdit.LayoutEngine.BoxMeasurer,
  dxRichEdit.Platform.Font,
  dxRichEdit.Platform.Win.Font,
  dxRichEdit.Platform.PatternLinePainter;

type
  TdxRectangleF = TdxRectF; 

  TdxMatrixStack = class(TList); 
  TdxSmoothingModeStack = class(TList); 
  TdxPixelOffsetModeStack = class(TList); 
  TdxRegionStack = class(TList); 
  TdxRectangleFStack = class(TList); 

  { TdxGdiStringViewInfo }

  TdxGdiStringViewInfo = record
    GlyphCount: Integer;
    Glyphs: TSmallIntDynArray;
    CharacterWidths: TIntegerDynArray;
    procedure Release;
  end;

  IGraphicsCache = interface
  ['{DB80F601-3309-4E78-BC94-DA40B8DAFA24}']
  end;

  TdxGraphicsCache = class(TcxIUnknownObject, IGraphicsCache)

  end;


  TdxDrawStringActualImplementationDelegate = 
    procedure (AHdc: THandle; const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect; AStringFormat: Integer) of object;

  TdxPainter = class abstract (TcxIUnknownObject, IdxPatternLinePaintingSupport)
  private
    FAllowChangeTextForeColor: Boolean;
    FCanvas: TcxCanvas;
    FTextForeColor: TdxColor;
    procedure SetTextForeColor(const Value: TdxColor);
  protected
    function GetRectangularClipBounds: TdxRectangleF; virtual; abstract;
    procedure SetClipBounds(const Value: TdxRectangleF); virtual; abstract;
    function GetDpiY: Integer; virtual; abstract;

    property Canvas: TcxCanvas read FCanvas;
  public
    constructor Create(ACanvas: TcxCanvas); virtual;

    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TdxColor; ARectangle: TRect); overload;
    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TdxColor; ARectangle: TRect; AStringFormat: Integer); overload;
    procedure DrawString(const AText: string; ABrush: TBrush; AFont: TdxFont; X, Y: Double); overload; virtual; abstract;
    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; ARectangle: TRect); overload; virtual; abstract;
    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; ARectangle: TRect; AStringFormat: Integer); overload; virtual; abstract;

    property RectangularClipBounds: TdxRectangleF read GetRectangularClipBounds;
    property ClipBounds: TdxRectangleF read GetRectangularClipBounds write SetClipBounds;
    property TextForeColor: TdxColor read FTextForeColor write SetTextForeColor;
    property DpiY: Integer read GetDpiY;
    property AllowChangeTextForeColor: Boolean read FAllowChangeTextForeColor write FAllowChangeTextForeColor;

    procedure FillRectangle(ABrush: TBrush; const ABounds: TRect); overload; virtual; abstract;
    procedure FillRectangle(AColor: TdxColor; const ABounds: TRect); overload; virtual; abstract;

    procedure DrawRectangle(APen: TPen; const ABounds: TRect); virtual; abstract;
    procedure FillEllipse(ABrush: TBrush; ABounds: TRect); overload; virtual; abstract;
    procedure FillEllipse(ABrush: TBrush; X, Y, AWidth, AHeight: Double); overload; virtual;
    procedure DrawSpacesString(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TColor; const ARectangle: TRect); overload; virtual;
    procedure DrawSpacesString(const AText: string; AFontInfo: TdxFontInfo; const ARectangle: TRect); overload; virtual; abstract;

    procedure DrawImage(AImg: TdxOfficeImage; ABounds: TRect); overload; virtual; abstract;
    procedure DrawImage(AImg: TdxOfficeImage; ABounds: TRect; AImgActualSizeInLayoutUnits: TSize; ASizing: TdxImageSizeMode); overload; virtual;

    procedure DrawLine(APen: TPen; X1, Y1, X2, Y2: Double); virtual; abstract;
    procedure DrawLines(APen: TPen; const APoints: array of TPointFloat); virtual; abstract;
    procedure FillPolygon(ABrush: TBrush; const APoints: TArray<TdxPointF>); virtual; abstract;
    procedure ResetCellBoundsClip; virtual; abstract; 
    procedure FinishPaint; virtual; 
    function GetPen(AColor: TColor; AThickness: Double): TPen; virtual; abstract;
    procedure ReleasePen(APen: TPen); virtual; abstract;
    function TryPushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double): Boolean;
    procedure PushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double); virtual; abstract;
    procedure PopTransform; virtual; abstract;
    procedure PushSmoothingMode(AHighQuality: Boolean); virtual; abstract;
    procedure PopSmoothingMode; virtual; abstract;
  end;

  TdxGdiPlusPainterBase = class abstract (TdxPainter)
  private
    FSf: Integer;
  protected
    class function CreateStringFormat: Integer;
  public
    constructor Create(ACanvas: TcxCanvas); override;
    procedure DrawSpacesString(const AText: string; AFontInfo: TdxFontInfo; const ABounds: TRect); override;

    property StringFormat: Integer read FSf;
  end;

  TdxGdiPlusPainter = class(TdxGdiPlusPainterBase)
  private
    FCache: IGraphicsCache;
    FTransformsStack: TdxMatrixStack;
    FSmoothingmodeStack: TdxSmoothingModeStack;
    FPixelmodeStack: TdxPixelOffsetModeStack;
    FClipRegions: TdxRegionStack;
    FRectangularBounds: TdxRectangleFStack;
    FRectangularClipBounds: TdxRectangleF;
    FDpiY: Integer;

    function GetHasTransform: Boolean;
    function GetGraphics: TcxCanvas;
  protected
    function GetDpiY: Integer; override;
    function GetRectangularClipBounds: TdxRectangleF; override;
  public
    constructor Create(ACanvas: TcxCanvas); override; 
    destructor Destroy; override;

    procedure SetClipBounds(const ABounds: TdxRectangleF); override;
    procedure FillRectangle(ABrush: TBrush; const AActualBounds: TRect); overload; override;
    procedure DrawRectangle(APen: TPen; const ABounds: TRect); override;
    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect); overload; override;
    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect; AStringFormat: Integer); overload; override;
    procedure DrawImage(AImg: TdxOfficeImage; ABounds: TRect); override;
    procedure DrawImage(AImg: TdxOfficeImage; ABounds: TRect; AImgActualSizeInLayoutUnits: TSize; ASizing: TdxImageSizeMode); override;

    procedure DrawLine(APen: TPen; X1, Y1, X2, Y2: Double); override;
    procedure DrawLines(APen: TPen; const APoints: array of TPointFloat); override;
    procedure FillPolygon(ABrush: TBrush; const APoints: TArray<TdxPointF>); override;
    procedure ResetCellBoundsClip; override; 

    procedure FillRectangle(AColor: TdxColor; const ABounds: TRect); override;
    function GetPen(AColor: TColor; AThickness: Double): TPen; override;
    procedure ReleasePen(APen: TPen); override;
    procedure PushSmoothingMode(AHighQuality: Boolean); override;
    procedure PopSmoothingMode; override;
    property Graphics: TcxCanvas read GetGraphics;
    property Cache: IGraphicsCache read FCache;
    property HasTransform: Boolean read GetHasTransform;
  end;

  TdxGdiPainter = class(TdxGdiPlusPainter) 
    procedure FillEllipse(ABrush: TBrush; ABounds: TRect); override;
    procedure PushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double); override;
    procedure PopTransform; override;

    procedure DrawSpacesString(const AText: string; AFontInfo: TdxFontInfo; const ABounds: TRect); override;
    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect); overload; override;
    procedure DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect; AStringFormat: Integer); overload; override;
    procedure DefaultDrawStringImplementation(AHdc: THandle; const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect; AStringFormat: Integer); virtual;
    procedure DrawStringImpl(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TdxColor; ABounds: TRect; AStringFormat: Integer; AImpl: TdxDrawStringActualImplementationDelegate);
    function GetMeasureHdc(AHdc: THandle): THandle; virtual; 
    procedure ReleaseMeasureHdc(AMeasureHdc: THandle); virtual; 
    procedure DrawNonCachedString(AHdc: THandle; AFontInfo: TdxFontInfo; const AText: string; ABounds: TRect; AStringFormat: Integer); virtual;
    procedure DrawNonCachedStringCore(AHdc, AMeasureHdc: THandle; const AText: string; ABounds: TRect; AStringFormat: Integer); virtual;
    procedure DrawNonCachedStringCoreAlignTopLeft(AHdc, AMeasureHdc: THandle; const AText: string; const ABounds: TRect); virtual;
    function GenerateStringViewInfo(AHdc: THandle; const AText: string): TdxGdiStringViewInfo;
  end;

  { TdxRichEditGdiPainter }

  TdxRichEditGdiPainter = class(TdxGdiPainter)
  private
    FMeasurer: TdxGdiBoxMeasurer;
  public
    constructor Create(ACanvas: TcxCanvas; AMeasurer: TdxGdiBoxMeasurer); reintroduce; 

    function GetMeasureHdc(AHdc: THandle): THandle; override;
    procedure DefaultDrawStringImplementation(AHdc: THandle; const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect; AStringFormat: Integer); override;
    procedure DrawStringCore(AHdc: THandle; const AText: string; const ABounds: TRect; ATextInfo: TdxGdiTextViewInfo); virtual;

    property Measurer: TdxGdiBoxMeasurer read FMeasurer;
  end;

implementation

uses
  Math, dxRichEdit.DocumentModel.Core, dxTypeHelpers;

{ TdxGdiStringViewInfo }

procedure TdxGdiStringViewInfo.Release;
begin
  CharacterWidths := nil;
  Glyphs := nil;
end;

{ TdxPainter }

procedure TdxPainter.DrawString(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TdxColor;
  ARectangle: TRect);
begin
  TextForeColor := AForeColor;
  DrawString(AText, AFontInfo, ARectangle);
end;

procedure TdxPainter.DrawString(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TdxColor;
  ARectangle: TRect; AStringFormat: Integer);
begin
  TextForeColor := AForeColor;
  DrawString(AText, AFontInfo, ARectangle, AStringFormat);
end;

procedure TdxPainter.SetTextForeColor(const Value: TdxColor);
begin
  if FAllowChangeTextForeColor then
    FTextForeColor := Value;
end;

function TdxPainter.TryPushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double): Boolean;
var
  ANeedApplyTransform: Boolean;
begin
  ANeedApplyTransform := (Frac(AAngleInDegrees) <> 0) and ((Trunc(AAngleInDegrees) mod 360) <> 0); 
  if not ANeedApplyTransform then
    Exit(False);
  PushRotationTransform(ACenter, AAngleInDegrees);
  Result := True;
end;

constructor TdxPainter.Create(ACanvas: TcxCanvas);
begin
  inherited Create;
  FAllowChangeTextForeColor := True;
  FCanvas := ACanvas;
end;


procedure TdxPainter.FillEllipse(ABrush: TBrush; X, Y, AWidth, AHeight: Double);
begin
  NotImplemented;
end;

procedure TdxPainter.FinishPaint;
begin
end;

procedure TdxPainter.DrawSpacesString(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TColor;
  const ARectangle: TRect);
begin
  TextForeColor := AForeColor;
  DrawSpacesString(AText, AFontInfo, ARectangle);
end;

procedure TdxPainter.DrawImage(AImg: TdxOfficeImage; ABounds: TRect; AImgActualSizeInLayoutUnits: TSize;
  ASizing: TdxImageSizeMode);
begin
end;

{ TdxRichEditGdiPainter }

constructor TdxRichEditGdiPainter.Create(ACanvas: TcxCanvas; AMeasurer: TdxGdiBoxMeasurer); 
begin
  inherited Create(ACanvas); 
  Assert(AMeasurer <> nil);
  FMeasurer := AMeasurer;
end;

procedure TdxRichEditGdiPainter.DefaultDrawStringImplementation(AHdc: THandle; const AText: string; AFontInfo: TdxFontInfo;
  ABounds: TRect; AStringFormat: Integer);
var
  ATextInfo: TdxGdiTextViewInfo;
begin
  ATextInfo := TdxGdiTextViewInfo(Measurer.TextViewInfoCache.TryGetTextViewInfo(AText, AFontInfo));
  if ATextInfo <> nil then
    DrawStringCore(AHdc, AText, ABounds, ATextInfo)
  else
    DrawNonCachedString(AHdc, AFontInfo, AText, ABounds, AStringFormat);
end;

procedure TdxRichEditGdiPainter.DrawStringCore(AHdc: THandle; const AText: string; const ABounds: TRect; ATextInfo: TdxGdiTextViewInfo);
begin
  ExtTextOut(AHdc, ABounds.Left, ABounds.Top, ETO_GLYPH_INDEX, @ABounds, @ATextInfo.Glyphs[0],
    ATextInfo.GlyphCount, @ATextInfo.CharacterWidths[0]);
end;

function TdxRichEditGdiPainter.GetMeasureHdc(AHdc: THandle): THandle;
begin
  Result := Measurer.Graphics.Handle;
end;

{ TdxGdiPainter }

procedure TdxGdiPainter.FillEllipse(ABrush: TBrush; ABounds: TRect);
begin
end;

procedure TdxGdiPainter.PushRotationTransform(ACenter: TPoint; const AAngleInDegrees: Double);
begin
end;

procedure TdxGdiPainter.PopTransform;
begin
end;

procedure TdxGdiPainter.DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect);
begin
  DrawString(AText, AFontInfo, ABounds, StringFormat);
end;

procedure TdxGdiPainter.DefaultDrawStringImplementation(AHdc: THandle; const AText: string; AFontInfo: TdxFontInfo;
  ABounds: TRect; AStringFormat: Integer);
begin
  DrawNonCachedString(AHdc, AFontInfo, AText, ABounds, AStringFormat);
end;

procedure TdxGdiPainter.DrawNonCachedString(AHdc: THandle; AFontInfo: TdxFontInfo; const AText: string;
  ABounds: TRect; AStringFormat: Integer);
var
  AMeasureHdc: THandle;
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  AMeasureHdc := GetMeasureHdc(AHdc);
  SelectObject(AMeasureHdc, AGdiFontInfo.GdiFontHandle);
  DrawNonCachedStringCore(AHdc, AMeasureHdc, AText, ABounds, AStringFormat);
end;

procedure TdxGdiPainter.DrawNonCachedStringCore(AHdc, AMeasureHdc: THandle; const AText: string; ABounds: TRect;
  AStringFormat: Integer);
begin
                DrawNonCachedStringCoreAlignTopLeft(AHdc, AMeasureHdc, AText, ABounds);
end;

procedure TdxGdiPainter.DrawNonCachedStringCoreAlignTopLeft(AHdc, AMeasureHdc: THandle; const AText: string;
  const ABounds: TRect);
var
  AClipRect: TRect;
  AStringViewInfo: TdxGdiStringViewInfo;
begin
  AStringViewInfo := GenerateStringViewInfo(AMeasureHdc, AText);
  AClipRect := ABounds;
  ExtTextOut(
    AHdc,
    ABounds.Left,
    ABounds.Top,
    ETO_GLYPH_INDEX,
    @AClipRect,
    @AStringViewInfo.Glyphs[0],
    AStringViewInfo.GlyphCount,
    @AStringViewInfo.CharacterWidths[0]);
  AStringViewInfo.Release;
end;

procedure TdxGdiPainter.DrawSpacesString(const AText: string;
  AFontInfo: TdxFontInfo; const ABounds: TRect);
var
  AStringViewInfo: TdxGdiStringViewInfo;
  I, ACount, ASpaceWidth, ARemainder, AWidth: Integer;
  AClipRect: TRect;
begin
  ACount := Length(AText);
  if ACount = 0 then
    Exit;

  AStringViewInfo := GenerateStringViewInfo(Canvas.Handle, AText);
  AClipRect := ABounds;

  ASpaceWidth := ABounds.Width div ACount;
  ARemainder := ABounds.Width mod ACount;
  for I := 0 to ACount - 1 do
  begin
    AWidth := ASpaceWidth;
    if ARemainder > 0 then
    begin
      Inc(AWidth);
      Dec(ARemainder);
    end;
    AStringViewInfo.CharacterWidths[I] := AWidth;
  end;
  ExtTextOut(
    Canvas.Handle,
    ABounds.Left,
    ABounds.Top,
    ETO_GLYPH_INDEX,
    @AClipRect,
    @AStringViewInfo.Glyphs[0],
    AStringViewInfo.GlyphCount,
    @AStringViewInfo.CharacterWidths[0]);
  AStringViewInfo.Release;
end;

procedure TdxGdiPainter.DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect;
  AStringFormat: Integer);
begin
  if HasTransform then
    inherited DrawString(AText, AFontInfo, ABounds, AStringFormat)
  else
    DrawStringImpl(AText, AFontInfo, TextForeColor, ABounds, AStringFormat, DefaultDrawStringImplementation);
end;

procedure TdxGdiPainter.DrawStringImpl(const AText: string; AFontInfo: TdxFontInfo; AForeColor: TdxColor;
  ABounds: TRect; AStringFormat: Integer; AImpl: TdxDrawStringActualImplementationDelegate);
var
  ADC: THandle;
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin

  ADC := Graphics.Handle;
  if not RectVisible(ADC, ABounds) then
    Exit;

  SelectObject(ADC, AGdiFontInfo.GdiFontHandle);
  SetTextColor(ADC, ColorToRGB(AForeColor)); 
  SetBkMode(ADC, TRANSPARENT);
  AImpl(ADC, AText, AFontInfo, ABounds, stringFormat); 
end;

function TdxGdiPainter.GenerateStringViewInfo(AHdc: THandle; const AText: string): TdxGdiStringViewInfo;
var
  AGcpResults: TGCPResults;
  ASz: Cardinal;
  ALength: Integer;
  ALpDx: TIntegerDynArray;
  ALpGlyphs: TSmallIntDynArray;
begin
  ALength := Length(AText);
  cxZeroMemory(@AGcpResults, SizeOf(TGCPResults)); //Win32.GCP_RESULTS gcpResults = new Win32.GCP_RESULTS();
  AGcpResults.lStructSize := SizeOf(GCP_RESULTS);
  AGcpResults.lpOutString := nil;
  AGcpResults.lpOrder := nil;

  SetLength(ALpDx, ALength);
  AGcpResults.lpDx := @ALpDx[0]; //gcpResults.lpDx = Marshal.AllocCoTaskMem(sizeof(int) * text.Length);
  AGcpResults.lpCaretPos := nil;
  AGcpResults.lpClass := nil;

  SetLength(ALpGlyphs, ALength);
  AGcpResults.lpGlyphs := @ALpGlyphs[0]; //gcpResults.lpGlyphs = Marshal.AllocCoTaskMem(sizeof(short) * text.Length);
  AGcpResults.nGlyphs := ALength;

  ASz := GetCharacterPlacement(AHdc, PChar(AText), ALength, 0, AGcpResults, GCP_USEKERNING or GCP_LIGATE);
  if (ASz = 0) and (ALength > 0) then
  begin
      AGcpResults.lpDx := nil;
      AGcpResults.lpGlyphs := nil;
  end;

  Result.Glyphs := ALpGlyphs;
  Result.GlyphCount := AGcpResults.nGlyphs;
  Result.CharacterWidths := ALpDx;
end;

function TdxGdiPainter.GetMeasureHdc(AHdc: THandle): THandle;
begin
  Result := AHdc;
end;

procedure TdxGdiPainter.ReleaseMeasureHdc(AMeasureHdc: THandle);
begin
//do nothing
end;

{ TdxGdiPlusPainter }

constructor TdxGdiPlusPainter.Create(ACanvas: TcxCanvas); 
begin
  inherited Create(ACanvas);
  FCache := nil; 
  FTransformsStack := TdxMatrixStack.Create;
  FSmoothingmodeStack := TdxSmoothingModeStack.Create;
  FPixelmodeStack := TdxPixelOffsetModeStack.Create;
  FClipRegions := TdxRegionStack.Create;
  FRectangularBounds := TdxRectangleFStack.Create;
end;

destructor TdxGdiPlusPainter.Destroy;
begin
  FreeAndNil(FTransformsStack);
  FreeAndNil(FSmoothingmodeStack);
  FreeAndNil(FPixelmodeStack);
  FreeAndNil(FClipRegions);
  FreeAndNil(FRectangularBounds);
  inherited Destroy;
end;

procedure TdxGdiPlusPainter.DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect);
begin
  DrawString(AText, AFontInfo, ABounds, StringFormat);
end;

type

  { TdxSnapToDevicePixelsHelper }

  TdxSnapToDevicePixelsHelper = class
  public
    class function GetCorrectedBounds(AGraphics: TdxGraphics; ASizeInPixel: TSize; ABounds: TdxRectangleF): TdxRectangleF;
  end;

  class function TdxSnapToDevicePixelsHelper.GetCorrectedBounds(AGraphics: TdxGraphics; ASizeInPixel: TSize; ABounds: TdxRectangleF): TdxRectangleF;
  var
    APoints: array[0..1] of TdxPointF;
    AScreenWidth, AScreenHeight: Integer;
  begin
    APoints[0] := dxPointF(ABounds.Left, ABounds.Top);
    APoints[1] := dxPointF(ABounds.Right, ABounds.Bottom);
    AScreenWidth := Trunc(APoints[1].X - APoints[0].X);
    AScreenHeight := Trunc(APoints[1].Y - APoints[0].Y);
    if (Abs(ASizeInPixel.cx - AScreenWidth) <= 5) and (Abs(ASizeInPixel.cy - AScreenHeight) <= 5) then
    begin
      APoints[0].X := Trunc(APoints[0].X + 0.5);
      APoints[0].Y := Trunc(APoints[0].Y + 0.5);
      APoints[1].X := APoints[0].X + ASizeInPixel.cx;
      APoints[1].Y := APoints[0].Y + ASizeInPixel.cy;
      Result := dxRectF(APoints[0].X, APoints[0].Y, APoints[1].X, APoints[1].Y);

    end
    else
      Result := ABounds;
  end;

procedure TdxGdiPlusPainter.DrawImage(AImg: TdxOfficeImage; ABounds: TRect);
var
  ACorrectedBounds: TdxRectangleF;
begin
  ACorrectedBounds := TdxSnapToDevicePixelsHelper.GetCorrectedBounds(Graphics, AImg.Size, cxRectF(ABounds));
  AImg.StretchDraw(Graphics.Handle, ACorrectedBounds, AImg.ClientRectInImageUnits);
end;

procedure TdxGdiPlusPainter.DrawImage(AImg: TdxOfficeImage; ABounds: TRect; AImgActualSizeInLayoutUnits: TSize;
  ASizing: TdxImageSizeMode);
var
  AImgRect: TRect;
begin
  Graphics.SaveClipRegion;
    AImgRect := ABounds; 
    Graphics.IntersectClipRect(ABounds);
    DrawImage(AImg, AImgRect);
  Graphics.RestoreClipRegion;
end;

procedure TdxGdiPlusPainter.DrawLine(APen: TPen; X1, Y1, X2, Y2: Double);
begin
  Canvas.Pen := APen;
  Canvas.MoveTo(Trunc(X1), Trunc(Y1));
  Canvas.LineTo(Trunc(X2), Trunc(Y2));
end;

procedure TdxGdiPlusPainter.DrawLines(APen: TPen; const APoints: array of TPointFloat);
var
  I: Integer;
begin
  for I := 1 to High(APoints) do
    DrawLine(APen, APoints[I - 1].x, APoints[I - 1].y, APoints[I].x, APoints[I].y);
end;

procedure TdxGdiPlusPainter.DrawRectangle(APen: TPen; const ABounds: TRect);
var
  AOldPen: TPen;
begin
  AOldPen := TPen.Create;
  try
    AOldPen.Assign(Canvas.Pen);
    Canvas.Pen := APen;
    Canvas.Rectangle(ABounds);
    Canvas.Pen := AOldPen;
  finally
    AOldPen.Free;
  end;
end;

procedure TdxGdiPlusPainter.DrawString(const AText: string; AFontInfo: TdxFontInfo; ABounds: TRect;
  AStringFormat: Integer);
var
  AGdiFontInfo: TdxGdiFontInfo absolute AFontInfo;
begin
  NotImplemented;
end;

procedure TdxGdiPlusPainter.FillRectangle(ABrush: TBrush; const AActualBounds: TRect);
var
  AOldBrush: TBrush;
begin
  AOldBrush := Graphics.Brush;
  try
    Graphics.Brush := ABrush;
    Graphics.Brush.Style := bsDiagCross;
    Graphics.FillRect(AActualBounds);
  finally
    Graphics.Brush := AOldBrush;
  end;
end;

procedure TdxGdiPlusPainter.FillPolygon(ABrush: TBrush; const APoints: TArray<TdxPointF>);
var
  I: Integer;
  P: TArray<TPoint>;
  AOldBrush: TBrush;
begin
  SetLength(P, Length(APoints));
  for I := 0 to Length(APoints) - 1 do
    P[I] := Point(Trunc(APoints[I].X), Trunc(APoints[I].Y));

  AOldBrush := Graphics.Brush;
  try
    Graphics.Brush := ABrush;
    Graphics.Polygon(P);
  finally
    Graphics.Brush := AOldBrush;
  end;
end;

procedure TdxGdiPlusPainter.FillRectangle(AColor: TdxColor; const ABounds: TRect);
begin
  Graphics.Brush.Color := AColor;
  Graphics.Brush.Style := bsSolid;
  Graphics.FillRect(ABounds);
end;

function TdxGdiPlusPainter.GetDpiY: Integer;
begin
  Result := FDpiY;
end;

function TdxGdiPlusPainter.GetGraphics: TcxCanvas;
begin
  Result := Canvas; 
end;

function TdxGdiPlusPainter.GetHasTransform: Boolean;
begin
  Result := FTransformsStack.Count > 0;
end;

function TdxGdiPlusPainter.GetPen(AColor: TColor; AThickness: Double): TPen;
var
  ALogBrush: TLOGBRUSH;
  AWidth: Integer;
begin
  Result := Canvas.Pen;
  AWidth := Trunc(AThickness);
  if AWidth > 1 then
  begin
    ALogBrush.lbStyle := BS_SOLID;
    ALogBrush.lbColor := AColor;
    ALogBrush.lbHatch := 0;
    Result.Handle := ExtCreatePen(PS_GEOMETRIC or PS_SOLID or PS_ENDCAP_FLAT, AWidth, ALogBrush, 0, nil);
  end
  else
    Result.Color := AColor;
end;

function TdxGdiPlusPainter.GetRectangularClipBounds: TdxRectangleF;
begin
  Result := FRectangularClipBounds;
end;

procedure TdxGdiPlusPainter.PopSmoothingMode;
begin
end;

procedure TdxGdiPlusPainter.PushSmoothingMode(AHighQuality: Boolean);
begin
end;

procedure TdxGdiPlusPainter.ReleasePen(APen: TPen);
begin
//do nothing
end;

procedure TdxGdiPlusPainter.ResetCellBoundsClip;
begin
end;

procedure TdxGdiPlusPainter.SetClipBounds(const ABounds: TdxRectangleF);
begin
  if FClipRegions.Count > 0 then
  begin
  end
  else
  begin
  end;
  FRectangularClipBounds := ABounds; 
end;

{ TdxGdiPlusPainterBase }

constructor TdxGdiPlusPainterBase.Create(ACanvas: TcxCanvas);
begin
  inherited Create(ACanvas);
  FSf := CreateStringFormat;
end;

class function TdxGdiPlusPainterBase.CreateStringFormat: Integer;
begin
  Result := 0;
end;

procedure TdxGdiPlusPainterBase.DrawSpacesString(const AText: string; AFontInfo: TdxFontInfo; const ABounds: TRect);
begin
  DrawString(AText, AFontInfo, ABounds);
end;

end.
