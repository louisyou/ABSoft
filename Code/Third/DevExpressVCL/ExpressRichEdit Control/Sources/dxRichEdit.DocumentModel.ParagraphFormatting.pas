{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.ParagraphFormatting;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Classes, SysUtils, Graphics, dxRichEdit.DocumentModel.CharacterFormatting, dxCoreClasses,
  dxRichEdit.Utils.Types, dxRichEdit.DocumentModel.IndexBasedObject, dxRichEdit.DocumentModel.UnitConverter,
  dxRichEdit.Platform.Font, dxRichEdit.DocumentModel.MergedProperties, dxRichEdit.DocumentModel.History.IndexChangedHistoryItem, dxRichEdit.DocumentModel.Core;

const
  NumberingListIndexNoNumberingList = -2;
  NumberingListIndexListIndexNotSetted = -1;
  NumberingListIndexMinValue = 0;
  NumberingListIndexMaxValue = MaxInt;

type
  TdxNumberingListIndex = Integer;

  TdxParagraphAlignment = (Left, Right, Center, Justify);

  TdxParagraphLineSpacing = (Single, Sesquialteral, Double, Multiple, Exactly, AtLeast);

  TdxParagraphFirstLineIndent = (None = 0, Indented, Hanging);

  IdxParagraphProperties = interface
  ['{AF526453-1103-4CAD-8569-255F8342D704}']
    function GetAfterAutoSpacing: Boolean;
    function GetAlignment: TdxParagraphAlignment;
    function GetBackColor: TColor;
    function GetBeforeAutoSpacing: Boolean;
    function GetContextualSpacing: Boolean;
    function GetFirstLineIndent: Integer;
    function GetFirstLineIndentType: TdxParagraphFirstLineIndent;
    function GetKeepLinesTogether: Boolean;
    function GetKeepWithNext: Boolean;
    function GetLeftIndent: Integer;
    function GetLineSpacing: Single;
    function GetLineSpacingType: TdxParagraphLineSpacing;
    function GetOutlineLevel: Integer;
    function GetPageBreakBefore: Boolean;
    function GetRightIndent: Integer;
    function GetSpacingAfter: Integer;
    function GetSpacingBefore: Integer;
    function GetSuppressHyphenation: Boolean;
    function GetSuppressLineNumbers: Boolean;
    function GetWidowOrphanControl: Boolean;
    procedure SetAfterAutoSpacing(const Value: Boolean);
    procedure SetAlignment(const Value: TdxParagraphAlignment);
    procedure SetBackColor(const Value: TColor);
    procedure SetBeforeAutoSpacing(const Value: Boolean);
    procedure SetContextualSpacing(const Value: Boolean);
    procedure SetFirstLineIndent(const Value: Integer);
    procedure SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
    procedure SetKeepLinesTogether(const Value: Boolean);
    procedure SetKeepWithNext(const Value: Boolean);
    procedure SetLeftIndent(const Value: Integer);
    procedure SetLineSpacing(const Value: Single);
    procedure SetLineSpacingType(const Value: TdxParagraphLineSpacing);
    procedure SetOutlineLevel(const Value: Integer);
    procedure SetPageBreakBefore(const Value: Boolean);
    procedure SetRightIndent(const Value: Integer);
    procedure SetSpacingAfter(const Value: Integer);
    procedure SetSpacingBefore(const Value: Integer);
    procedure SetSuppressHyphenation(const Value: Boolean);
    procedure SetSuppressLineNumbers(const Value: Boolean);
    procedure SetWidowOrphanControl(const Value: Boolean);

    property AfterAutoSpacing: Boolean read GetAfterAutoSpacing write SetAfterAutoSpacing;
    property Alignment: TdxParagraphAlignment read GetAlignment write SetAlignment;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property BeforeAutoSpacing: Boolean read GetBeforeAutoSpacing write SetBeforeAutoSpacing;
    property ContextualSpacing: Boolean read GetContextualSpacing write SetContextualSpacing;
    property FirstLineIndent: Integer read GetFirstLineIndent write SetFirstLineIndent;
    property FirstLineIndentType: TdxParagraphFirstLineIndent read GetFirstLineIndentType write SetFirstLineIndentType;
    property KeepLinesTogether: Boolean read GetKeepLinesTogether write SetKeepLinesTogether;
    property KeepWithNext: Boolean read GetKeepWithNext write SetKeepWithNext;
    property LeftIndent: Integer read GetLeftIndent write SetLeftIndent;
    property LineSpacing: Single read GetLineSpacing write SetLineSpacing;
    property LineSpacingType: TdxParagraphLineSpacing read GetLineSpacingType write SetLineSpacingType;
    property OutlineLevel: Integer read GetOutlineLevel write SetOutlineLevel;
    property PageBreakBefore: Boolean read GetPageBreakBefore write SetPageBreakBefore;
    property RightIndent: Integer read GetRightIndent write SetRightIndent;
    property SpacingAfter: Integer read GetSpacingAfter write SetSpacingAfter;
    property SpacingBefore: Integer read GetSpacingBefore write SetSpacingBefore;
    property SuppressHyphenation: Boolean read GetSuppressHyphenation write SetSuppressHyphenation;
    property SuppressLineNumbers: Boolean read GetSuppressLineNumbers write SetSuppressLineNumbers;
    property WidowOrphanControl: Boolean read GetWidowOrphanControl write SetWidowOrphanControl;
  end;

  IdxParagraphPropertiesContainer = interface 
  ['{B65775E3-D102-4DD4-968C-33EBC09EE673}']
    function GetPieceTable: TObject;
    function CreateParagraphPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnParagraphPropertiesChanged;

    property PieceTable: TObject read GetPieceTable;
  end;

  TdxParagraphFormattingInfo = class(TcxIUnknownObject,
    IdxCloneable<TdxParagraphFormattingInfo>,
    IdxSupportsCopyFrom<TdxParagraphFormattingInfo>,
    IdxSupportsSizeOf, IdxParagraphProperties)
  private const
    MaskLineSpacingType     = $00000007; 
    MaskFirstLineIndentType = $00000018; 
    MaskAlignment           = $00000060; 
    MaskOutlineLevel        = $00000780; 
    MaskSuppressHyphenation = $00000800;
    MaskSuppressLineNumbers = $00001000;
    MaskContextualSpacing   = $00002000;
    MaskPageBreakBefore     = $00004000;
    MaskBeforeAutoSpacing   = $00008000;
    MaskAfterAutoSpacing    = $00010000;
    MaskKeepWithNext        = $00020000;
    MaskKeepLinesTogether   = $00040000;
    MaskWidowOrphanControl  = $00080000;
  private
    FPackedValues: Integer;
    FLeftIndent: Integer;
    FRightIndent: Integer;
    FSpacingBefore: Integer;
    FSpacingAfter: Integer;
    FFirstLineIndent: Integer;
    FLineSpacing: Single;
    FBackColor: TColor;
    function GetBooleanValue(AMask: Integer): Boolean; inline;
    procedure SetBooleanValue(AMask: Integer; AValue: Boolean); inline;

    //IdxParagraphProperties
    function GetAfterAutoSpacing: Boolean; inline;
    function GetAlignment: TdxParagraphAlignment; inline;
    function GetBackColor: TColor; inline;
    function GetBeforeAutoSpacing: Boolean; inline;
    function GetContextualSpacing: Boolean; inline;
    function GetFirstLineIndent: Integer; inline;
    function GetFirstLineIndentType: TdxParagraphFirstLineIndent; inline;
    function GetKeepLinesTogether: Boolean; inline;
    function GetKeepWithNext: Boolean; inline;
    function GetLeftIndent: Integer; inline;
    function GetLineSpacing: Single; inline;
    function GetLineSpacingType: TdxParagraphLineSpacing; inline;
    function GetOutlineLevel: Integer; inline;
    function GetPageBreakBefore: Boolean; inline;
    function GetRightIndent: Integer; inline;
    function GetSpacingAfter: Integer; inline;
    function GetSpacingBefore: Integer; inline;
    function GetSuppressHyphenation: Boolean; inline;
    function GetSuppressLineNumbers: Boolean; inline;
    function GetWidowOrphanControl: Boolean; inline;
    procedure SetAfterAutoSpacing(const Value: Boolean); inline;
    procedure SetAlignment(const Value: TdxParagraphAlignment); inline;
    procedure SetBackColor(const Value: TColor); inline;
    procedure SetBeforeAutoSpacing(const Value: Boolean); inline;
    procedure SetContextualSpacing(const Value: Boolean); inline;
    procedure SetFirstLineIndent(const Value: Integer);
    procedure SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent); inline;
    procedure SetKeepLinesTogether(const Value: Boolean); inline;
    procedure SetKeepWithNext(const Value: Boolean); inline;
    procedure SetLeftIndent(const Value: Integer); inline;
    procedure SetLineSpacing(const Value: Single);
    procedure SetLineSpacingType(const Value: TdxParagraphLineSpacing); inline;
    procedure SetOutlineLevel(const Value: Integer); inline;
    procedure SetPageBreakBefore(const Value: Boolean); inline;
    procedure SetRightIndent(const Value: Integer); inline;
    procedure SetSpacingAfter(const Value: Integer);
    procedure SetSpacingBefore(const Value: Integer);
    procedure SetSuppressHyphenation(const Value: Boolean); inline;
    procedure SetSuppressLineNumbers(const Value: Boolean); inline;
    procedure SetWidowOrphanControl(const Value: Boolean); inline;
  protected
    property PackedValues: Integer read FPackedValues;
  public
    constructor Create; virtual; 
    //IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxParagraphFormattingInfo); virtual;
    //IdxCloneable
    function Clone: TdxParagraphFormattingInfo;

    function Equals(Obj: TObject): Boolean; override;
    function GetHashCode: Integer; override;
    function SkipCompareFirstLineIndent: Boolean; inline;
    function SkipCompareLineSpacing: Boolean; inline;

    property AfterAutoSpacing: Boolean read GetAfterAutoSpacing write SetAfterAutoSpacing;
    property Alignment: TdxParagraphAlignment read GetAlignment write SetAlignment;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property BeforeAutoSpacing: Boolean read GetBeforeAutoSpacing write SetBeforeAutoSpacing;
    property ContextualSpacing: Boolean read GetContextualSpacing write SetContextualSpacing;
    property FirstLineIndent: Integer read GetFirstLineIndent write SetFirstLineIndent;
    property FirstLineIndentType: TdxParagraphFirstLineIndent read GetFirstLineIndentType write SetFirstLineIndentType;
    property KeepLinesTogether: Boolean read GetKeepLinesTogether write SetKeepLinesTogether;
    property KeepWithNext: Boolean read GetKeepWithNext write SetKeepWithNext;
    property LeftIndent: Integer read GetLeftIndent write SetLeftIndent;
    property LineSpacing: Single read GetLineSpacing write SetLineSpacing;
    property LineSpacingType: TdxParagraphLineSpacing read GetLineSpacingType write SetLineSpacingType;
    property OutlineLevel: Integer read GetOutlineLevel write SetOutlineLevel;
    property PageBreakBefore: Boolean read GetPageBreakBefore write SetPageBreakBefore;
    property RightIndent: Integer read GetRightIndent write SetRightIndent;
    property SpacingAfter: Integer read GetSpacingAfter write SetSpacingAfter;
    property SpacingBefore: Integer read GetSpacingBefore write SetSpacingBefore;
    property SuppressHyphenation: Boolean read GetSuppressHyphenation write SetSuppressHyphenation;
    property SuppressLineNumbers: Boolean read GetSuppressLineNumbers write SetSuppressLineNumbers;
    property WidowOrphanControl: Boolean read GetWidowOrphanControl write SetWidowOrphanControl;
  end;
  TdxParagraphFormattingInfoClass = class of TdxParagraphFormattingInfo;

  TdxUsedParagraphFormattingOption = (UseAlignment, UseLeftIndent, UseRightIndent,
			UseSpacingBefore, UseSpacingAfter, UseLineSpacing, UseFirstLineIndent,
			UseSuppressHyphenation, UseSuppressLineNumbers, UseContextualSpacing, UsePageBreakBefore,
			UseBeforeAutoSpacing, UseAfterAutoSpacing, UseKeepWithNext, UseKeepLinesTogether,
      UseWidowOrphanControl, UseOutlineLevel, UseBackColor);

  TdxUsedParagraphFormattingOptions = set of TdxUsedParagraphFormattingOption;

  TdxParagraphFormattingOptions = class(TcxIUnknownObject,
    IdxCloneable<TdxParagraphFormattingOptions>,
    IdxSupportsCopyFrom<TdxParagraphFormattingOptions>,
    IdxSupportsSizeOf)
  private
    FValue: TdxUsedParagraphFormattingOptions;
    function GetValue(const Index: TdxUsedParagraphFormattingOption): Boolean; inline;
    procedure SetValue(const Index: TdxUsedParagraphFormattingOption; const Value: Boolean); inline;
  protected
    property Value: TdxUsedParagraphFormattingOptions read FValue;
  public
    constructor Create(AUsedValues: TdxUsedParagraphFormattingOptions = []);

    procedure CopyFrom(const Source: TdxParagraphFormattingOptions);
    function Clone: TdxParagraphFormattingOptions;
    function Equals(Obj: TObject): Boolean; override;
    function GetHashCode: Integer; override;

    property UseAlignment: Boolean index TdxUsedParagraphFormattingOption.UseAlignment read GetValue write SetValue;
    property UseLeftIndent: Boolean index TdxUsedParagraphFormattingOption.UseLeftIndent read GetValue write SetValue;
    property UseRightIndent: Boolean index TdxUsedParagraphFormattingOption.UseRightIndent read GetValue write SetValue;
    property UseSpacingBefore: Boolean index TdxUsedParagraphFormattingOption.UseSpacingBefore read GetValue write SetValue;
    property UseSpacingAfter: Boolean index TdxUsedParagraphFormattingOption.UseSpacingAfter read GetValue write SetValue;
    property UseLineSpacing: Boolean index TdxUsedParagraphFormattingOption.UseLineSpacing read GetValue write SetValue;
    property UseFirstLineIndent: Boolean index TdxUsedParagraphFormattingOption.UseFirstLineIndent read GetValue write SetValue;
    property UseSuppressHyphenation: Boolean index TdxUsedParagraphFormattingOption.UseSuppressHyphenation read GetValue write SetValue;
    property UseSuppressLineNumbers: Boolean index TdxUsedParagraphFormattingOption.UseSuppressLineNumbers read GetValue write SetValue;
    property UseContextualSpacing: Boolean index TdxUsedParagraphFormattingOption.UseContextualSpacing read GetValue write SetValue;
    property UsePageBreakBefore: Boolean index TdxUsedParagraphFormattingOption.UsePageBreakBefore read GetValue write SetValue;
    property UseBeforeAutoSpacing: Boolean index TdxUsedParagraphFormattingOption.UseBeforeAutoSpacing read GetValue write SetValue;
    property UseAfterAutoSpacing: Boolean index TdxUsedParagraphFormattingOption.UseAfterAutoSpacing read GetValue write SetValue;
    property UseKeepWithNext: Boolean index TdxUsedParagraphFormattingOption.UseKeepWithNext read GetValue write SetValue;
    property UseKeepLinesTogether: Boolean index TdxUsedParagraphFormattingOption.UseKeepLinesTogether read GetValue write SetValue;
    property UseWidowOrphanControl: Boolean index TdxUsedParagraphFormattingOption.UseWidowOrphanControl read GetValue write SetValue;
    property UseOutlineLevel: Boolean index TdxUsedParagraphFormattingOption.UseOutlineLevel read GetValue write SetValue;
    property UseBackColor: Boolean index TdxUsedParagraphFormattingOption.UseBackColor read GetValue write SetValue;
  end;

  { TdxParagraphFormattingBase }

  TdxParagraphFormattingBase = class(TdxIndexBasedObjectB<TdxParagraphFormattingInfo, TdxParagraphFormattingOptions>,
    IdxCloneable<TdxParagraphFormattingBase>,
    IdxSupportsCopyFrom<TdxParagraphFormattingBase>, IdxParagraphProperties)
  private
    //IdxParagraphProperties
    function GetAfterAutoSpacing: Boolean; inline;
    function GetAlignment: TdxParagraphAlignment; inline;
    function GetBackColor: TColor; inline;
    function GetBeforeAutoSpacing: Boolean; inline;
    function GetContextualSpacing: Boolean; inline;
    function GetFirstLineIndent: Integer; inline;
    function GetFirstLineIndentType: TdxParagraphFirstLineIndent; inline;
    function GetKeepLinesTogether: Boolean; inline;
    function GetKeepWithNext: Boolean; inline;
    function GetLeftIndent: Integer; inline;
    function GetLineSpacing: Single; inline;
    function GetLineSpacingType: TdxParagraphLineSpacing; inline;
    function GetOutlineLevel: Integer; inline;
    function GetPageBreakBefore: Boolean; inline;
    function GetRightIndent: Integer; inline;
    function GetSpacingAfter: Integer; inline;
    function GetSpacingBefore: Integer; inline;
    function GetSuppressHyphenation: Boolean; inline;
    function GetSuppressLineNumbers: Boolean; inline;
    function GetWidowOrphanControl: Boolean; inline;

    procedure SetAfterAutoSpacing(const Value: Boolean);
    procedure SetAfterAutoSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseAfterAutoSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetAlignment(const Value: TdxParagraphAlignment);
    procedure SetAlignmentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseAlignmentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetBackColor(const Value: TColor);
    procedure SetBackColorCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseBackColorCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetBeforeAutoSpacing(const Value: Boolean);
    procedure SetBeforeAutoSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseBeforeAutoSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetContextualSpacing(const Value: Boolean);
    procedure SetContextualSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseContextualSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetFirstLineIndent(const Value: Integer);
    procedure SetFirstLineIndentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseFirstLineIndentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
    procedure SetFirstLineIndentTypeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseFirstLineIndentTypeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetKeepLinesTogether(const Value: Boolean);
    procedure SetKeepLinesTogetherCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseKeepLinesTogetherCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetKeepWithNext(const Value: Boolean);
    procedure SetKeepWithNextCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseKeepWithNextCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetLeftIndent(const Value: Integer);
    procedure SetLeftIndentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseLeftIndentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetLineSpacing(const Value: Single);
    procedure SetLineSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Single); inline;
    procedure SetUseLineSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetLineSpacingType(const Value: TdxParagraphLineSpacing);
    procedure SetLineSpacingTypeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseLineSpacingTypeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetOutlineLevel(const Value: Integer);
    procedure SetOutlineLevelCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseOutlineLevelCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetPageBreakBefore(const Value: Boolean);
    procedure SetPageBreakBeforeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUsePageBreakBeforeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetRightIndent(const Value: Integer);
    procedure SetRightIndentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseRightIndentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetSpacingAfter(const Value: Integer);
    procedure SetSpacingAfterCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseSpacingAfterCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetSpacingBefore(const Value: Integer);
    procedure SetSpacingBeforeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer); inline;
    procedure SetUseSpacingBeforeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetSuppressHyphenation(const Value: Boolean);
    procedure SetSuppressHyphenationCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseSuppressHyphenationCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetSuppressLineNumbers(const Value: Boolean);
    procedure SetSuppressLineNumbersCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseSuppressLineNumbersCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
    procedure SetWidowOrphanControl(const Value: Boolean);
    procedure SetWidowOrphanControlCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean); inline;
    procedure SetUseWidowOrphanControlCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean); inline;
  protected
    function GetInfoCache: TdxUniqueItemsCache<TdxParagraphFormattingInfo>; override;
    function GetOptionsCache: TdxUniqueItemsCache<TdxParagraphFormattingOptions>; override;
    function PropertyEquals(const AOther: TdxIndexBasedObject<TdxParagraphFormattingInfo, TdxParagraphFormattingOptions>): Boolean; override;
  public
    //IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxParagraphFormattingBase); overload;
    procedure CopyFrom(const AInfo: TdxParagraphFormattingInfo; const AOptions: TdxParagraphFormattingOptions); overload;
    //IdxCloneable
    function Clone: TdxParagraphFormattingBase;

    property AfterAutoSpacing: Boolean read GetAfterAutoSpacing write SetAfterAutoSpacing;
    property Alignment: TdxParagraphAlignment read GetAlignment write SetAlignment;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property BeforeAutoSpacing: Boolean read GetBeforeAutoSpacing write SetBeforeAutoSpacing;
    property ContextualSpacing: Boolean read GetContextualSpacing write SetContextualSpacing;
    property FirstLineIndent: Integer read GetFirstLineIndent write SetFirstLineIndent;
    property FirstLineIndentType: TdxParagraphFirstLineIndent read GetFirstLineIndentType write SetFirstLineIndentType;
    property KeepLinesTogether: Boolean read GetKeepLinesTogether write SetKeepLinesTogether;
    property KeepWithNext: Boolean read GetKeepWithNext write SetKeepWithNext;
    property LeftIndent: Integer read GetLeftIndent write SetLeftIndent;
    property LineSpacing: Single read GetLineSpacing write SetLineSpacing;
    property LineSpacingType: TdxParagraphLineSpacing read GetLineSpacingType write SetLineSpacingType;
    property OutlineLevel: Integer read GetOutlineLevel write SetOutlineLevel;
    property PageBreakBefore: Boolean read GetPageBreakBefore write SetPageBreakBefore;
    property RightIndent: Integer read GetRightIndent write SetRightIndent;
    property SpacingAfter: Integer read GetSpacingAfter write SetSpacingAfter;
    property SpacingBefore: Integer read GetSpacingBefore write SetSpacingBefore;
    property SuppressHyphenation: Boolean read GetSuppressHyphenation write SetSuppressHyphenation;
    property SuppressLineNumbers: Boolean read GetSuppressLineNumbers write SetSuppressLineNumbers;
    property WidowOrphanControl: Boolean read GetWidowOrphanControl write SetWidowOrphanControl;
end;

  { TdxParagraphProperties }

  TdxParagraphProperties = class(TdxRichEditIndexBasedObject<TdxParagraphFormattingBase>, IdxParagraphProperties)
  private
    FOwner: IdxParagraphPropertiesContainer;
    function GetPieceTable(AOwner: IdxParagraphPropertiesContainer): TObject;

    function GetAfterAutoSpacing: Boolean;
    function GetAlignment: TdxParagraphAlignment;
    function GetBackColor: TColor;
    function GetBeforeAutoSpacing: Boolean;
    function GetContextualSpacing: Boolean;
    function GetFirstLineIndent: Integer;
    function GetFirstLineIndentType: TdxParagraphFirstLineIndent;
    function GetKeepLinesTogether: Boolean;
    function GetKeepWithNext: Boolean;
    function GetLeftIndent: Integer;
    function GetLineSpacing: Single;
    function GetLineSpacingType: TdxParagraphLineSpacing;
    function GetOutlineLevel: Integer;
    function GetPageBreakBefore: Boolean;
    function GetRightIndent: Integer;
    function GetSpacingAfter: Integer;
    function GetSpacingBefore: Integer;
    function GetSuppressHyphenation: Boolean;
    function GetSuppressLineNumbers: Boolean;
    function GetWidowOrphanControl: Boolean;

    function GetUseAlignment: Boolean;
    function GetUseLeftIndent: Boolean;
    function GetUseRightIndent: Boolean;
    function GetUseSpacingBefore: Boolean;
    function GetUseSpacingAfter: Boolean;
    function GetUseLineSpacing: Boolean;
    function GetUseFirstLineIndent: Boolean;
    function GetUseSuppressHyphenation: Boolean;
    function GetUseSuppressLineNumbers: Boolean;
    function GetUseContextualSpacing: Boolean;
    function GetUsePageBreakBefore: Boolean;
    function GetUseBeforeAutoSpacing: Boolean;
    function GetUseAfterAutoSpacing: Boolean;
    function GetUseKeepWithNext: Boolean;
    function GetUseKeepLinesTogether: Boolean;
    function GetUseWidowOrphanControl: Boolean;
    function GetUseOutlineLevel: Boolean;
    function GetUseBackColor: Boolean;

    function SetAfterAutoSpacingCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetAfterAutoSpacing(const Value: Boolean);
    procedure SetAlignment(const Value: TdxParagraphAlignment);
    function SetAlignmentCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetBackColor(const Value: TColor);
    function SetBackColorCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetBeforeAutoSpacing(const Value: Boolean);
    function SetBeforeAutoSpacingCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetContextualSpacing(const Value: Boolean);
    function SetContextualSpacingCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetFirstLineIndent(const Value: Integer);
    function SetFirstLineIndentCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
    function SetFirstLineIndentTypeCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetKeepLinesTogether(const Value: Boolean);
    function SetKeepLinesTogetherCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetKeepWithNext(const Value: Boolean);
    function SetKeepWithNextCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetLeftIndent(const Value: Integer);
    function SetLeftIndentCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetLineSpacing(const Value: Single);
    function SetLineSpacingCore(const AInfo: TdxParagraphFormattingBase; const Value: Single): TdxDocumentModelChangeActions;
    procedure SetLineSpacingType(const Value: TdxParagraphLineSpacing);
    function SetLineSpacingTypeCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetOutlineLevel(const Value: Integer);
    function SetOutlineLevelCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetPageBreakBefore(const Value: Boolean);
    function SetPageBreakBeforeCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetRightIndent(const Value: Integer);
    function SetRightIndentCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetSpacingAfter(const Value: Integer);
    function SetSpacingAfterCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetSpacingBefore(const Value: Integer);
    function SetSpacingBeforeCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
    procedure SetSuppressHyphenation(const Value: Boolean);
    function SetSuppressHyphenationCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetSuppressLineNumbers(const Value: Boolean);
    function SetSuppressLineNumbersCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
    procedure SetWidowOrphanControl(const Value: Boolean);
    function SetWidowOrphanControlCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
  protected
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxParagraphFormattingBase>; override;
    procedure OnIndexChanged; override;
    function CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>; override;
    function GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener; override; 
  public
    constructor Create(const AOwner: IdxParagraphPropertiesContainer); reintroduce;
    destructor Destroy; override;

    class procedure ApplyPropertiesDiff(ATarget: TdxParagraphProperties; ATargetMergedInfo: TdxParagraphFormattingInfo; ASourceMergedInfo: TdxParagraphFormattingInfo);
    procedure CopyFrom(const AParagraphProperties: TdxMergedProperties<TdxParagraphFormattingInfo, TdxParagraphFormattingOptions>); overload;
    function Equals(Obj: TObject): Boolean; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function GetHashCode: Integer; override;
    procedure Merge(AProperties: TdxParagraphProperties); virtual;
    procedure Reset;
    procedure ResetUse(AMask: TdxUsedParagraphFormattingOptions);
    procedure ResetAllUse;
    function UseVal(AMask: TdxUsedParagraphFormattingOption): Boolean; 

    property AfterAutoSpacing: Boolean read GetAfterAutoSpacing write SetAfterAutoSpacing;
    property Alignment: TdxParagraphAlignment read GetAlignment write SetAlignment;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property BeforeAutoSpacing: Boolean read GetBeforeAutoSpacing write SetBeforeAutoSpacing;
    property ContextualSpacing: Boolean read GetContextualSpacing write SetContextualSpacing;
    property FirstLineIndent: Integer read GetFirstLineIndent write SetFirstLineIndent;
    property FirstLineIndentType: TdxParagraphFirstLineIndent read GetFirstLineIndentType write SetFirstLineIndentType;
    property KeepLinesTogether: Boolean read GetKeepLinesTogether write SetKeepLinesTogether;
    property KeepWithNext: Boolean read GetKeepWithNext write SetKeepWithNext;
    property LeftIndent: Integer read GetLeftIndent write SetLeftIndent;
    property LineSpacing: Single read GetLineSpacing write SetLineSpacing;
    property LineSpacingType: TdxParagraphLineSpacing read GetLineSpacingType write SetLineSpacingType;
    property OutlineLevel: Integer read GetOutlineLevel write SetOutlineLevel;
    property PageBreakBefore: Boolean read GetPageBreakBefore write SetPageBreakBefore;
    property RightIndent: Integer read GetRightIndent write SetRightIndent;
    property SpacingAfter: Integer read GetSpacingAfter write SetSpacingAfter;
    property SpacingBefore: Integer read GetSpacingBefore write SetSpacingBefore;
    property SuppressHyphenation: Boolean read GetSuppressHyphenation write SetSuppressHyphenation;
    property SuppressLineNumbers: Boolean read GetSuppressLineNumbers write SetSuppressLineNumbers;
    property WidowOrphanControl: Boolean read GetWidowOrphanControl write SetWidowOrphanControl;

    property UseAlignment: Boolean read GetUseAlignment;
    property UseLeftIndent: Boolean read GetUseLeftIndent;
    property UseRightIndent: Boolean read GetUseRightIndent;
    property UseSpacingBefore: Boolean read GetUseSpacingBefore;
    property UseSpacingAfter: Boolean read GetUseSpacingAfter;
    property UseLineSpacing: Boolean read GetUseLineSpacing;
    property UseFirstLineIndent: Boolean read GetUseFirstLineIndent;
    property UseSuppressHyphenation: Boolean read GetUseSuppressHyphenation;
    property UseSuppressLineNumbers: Boolean read GetUseSuppressLineNumbers;
    property UseContextualSpacing: Boolean read GetUseContextualSpacing;
    property UsePageBreakBefore: Boolean read GetUsePageBreakBefore;
    property UseBeforeAutoSpacing: Boolean read GetUseBeforeAutoSpacing;
    property UseAfterAutoSpacing: Boolean read GetUseAfterAutoSpacing;
    property UseKeepWithNext: Boolean read GetUseKeepWithNext;
    property UseKeepLinesTogether: Boolean read GetUseKeepLinesTogether;
    property UseWidowOrphanControl: Boolean read GetUseWidowOrphanControl;
    property UseOutlineLevel: Boolean read GetUseOutlineLevel;
    property UseBackColor: Boolean read GetUseBackColor;
  end;

  { TdxParagraphFormattingInfoCache }

  TdxParagraphFormattingInfoCache = class(TdxUniqueItemsCache<TdxParagraphFormattingInfo>)
  const
    DefaultItemIndex = 0;
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxParagraphFormattingInfo; override;
  end;

  { TdxParagraphFormattingOptionsCache }

  TdxParagraphFormattingOptionsCache = class(TdxUniqueItemsCache<TdxParagraphFormattingOptions>)
  const
    EmptyParagraphFormattingOptionIndex = 0;
    RootParagraphFormattingOptionIndex = 1;
  private
    procedure AddRootStyleOptions;
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxParagraphFormattingOptions; override;
    procedure InitItems(const AUnitConverter: IdxDocumentModelUnitConverter); override;
  end;

  { TdxParagraphFormattingCache }

  TdxParagraphFormattingCache = class(TdxUniqueItemsCache<TdxParagraphFormattingBase>)
  const
    EmptyParagraphFormattingOptionIndex = 0;
    RootParagraphFormattingOptionIndex = 1;
  private
    FDocumentModel: TObject;
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxParagraphFormattingBase; override;
  public
    constructor Create(ADocumentModel: TObject); overload;
    property DocumentModel: TObject read FDocumentModel;
  end;

  { TdxMergedParagraphProperties }

  TdxMergedParagraphProperties = class(TdxMergedProperties<TdxParagraphFormattingInfo, TdxParagraphFormattingOptions>)
  public 
    constructor Create(AInfo: TdxParagraphFormattingInfo; AOptions: TdxParagraphFormattingOptions); override;
    destructor Destroy; override;
  end;

  { TdxParagraphPropertiesMerger }

  TdxParagraphPropertiesMerger = class(TdxPropertiesMergerBase<TdxParagraphFormattingInfo, TdxParagraphFormattingOptions, TdxMergedParagraphProperties>)
  protected
    procedure MergeCore(const AInfo: TdxParagraphFormattingInfo; const AOptions: TdxParagraphFormattingOptions); override;
  public
    constructor Create(const AProperties: TdxParagraphProperties); overload;

    procedure Merge(const AProperties: TdxParagraphProperties); overload;
  end;

  { TdxParagraphFormattingChangeActionsCalculator }

  TdxParagraphFormattingChangeType = (None, Alignment, LeftIndent, RightIndent, SpacingBefore,
    SpacingAfter, LineSpacingType, LineSpacing, FirstLineIndentType, FirstLineIndent,
    SuppressHyphenation, SuppressLineNumbers, ContextualSpacing, PageBreakBefore,
    BeforeAutoSpacing, AfterAutoSpacing, KeepWithNext, KeepLinesTogether, WidowOrphanControl,
    OutlineLevel, BackColor, ParagraphStyle, BatchUpdate, NumberingListIndex);

  TdxParagraphFormattingChangeActionsCalculator = class
  public
    class function CalculateChangeActions(const AChange: TdxParagraphFormattingChangeType): TdxDocumentModelChangeActions;
  end;

  { TdxParagraphParagraphPropertiesChangedHistoryItem }

  TdxParagraphParagraphPropertiesChangedHistoryItem = class(TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>)
  private
    FParagraphIndex: Integer;
  public
    constructor Create(const APieceTable: TdxDocumentModelPart; AParagraphIndex: Integer); reintroduce;

    function GetObject: IdxIndexBasedObject<TdxDocumentModelChangeActions>; override;

    property ParagraphIndex: Integer read FParagraphIndex;
  end;

  { TdxParagraphMergedParagraphPropertiesCachedResult }

  TdxParagraphMergedParagraphPropertiesCachedResult = class 
  private
    FTableStyleParagraphPropertiesIndex: Integer; 
    FOwnListLevelParagraphPropertiesIndex: Integer; 
    FParagraphPropertiesIndex: Integer; 
    FMergedParagraphProperties: TdxMergedParagraphProperties; 
    FParagraphStyleIndex: Integer; 
    procedure SetMergedParagraphProperties(Value: TdxMergedParagraphProperties);
  public
    constructor Create;
    destructor Destroy; override;

    property ParagraphPropertiesIndex: Integer read FParagraphPropertiesIndex write FParagraphPropertiesIndex; 
    property ParagraphStyleIndex: Integer read FParagraphStyleIndex write FParagraphStyleIndex; 
    property TableStyleParagraphPropertiesIndex: Integer read FTableStyleParagraphPropertiesIndex write FTableStyleParagraphPropertiesIndex; 
    property OwnListLevelParagraphPropertiesIndex: Integer read FOwnListLevelParagraphPropertiesIndex write FOwnListLevelParagraphPropertiesIndex; 
    property MergedParagraphProperties: TdxMergedParagraphProperties read FMergedParagraphProperties write SetMergedParagraphProperties; 
  end;

implementation

uses
  dxRichEdit.Utils.BatchUpdateHelper, RTLConsts, 
  Math, dxRichEdit.DocumentModel.PieceTable;

const
  dxParagraphFormattingOptionsUseAll = [TdxUsedParagraphFormattingOption.UseAlignment,
    TdxUsedParagraphFormattingOption.UseLeftIndent, TdxUsedParagraphFormattingOption.UseRightIndent,
    TdxUsedParagraphFormattingOption.UseSpacingBefore, TdxUsedParagraphFormattingOption.UseSpacingAfter,
    TdxUsedParagraphFormattingOption.UseLineSpacing, TdxUsedParagraphFormattingOption.UseFirstLineIndent,
		TdxUsedParagraphFormattingOption.UseSuppressHyphenation, TdxUsedParagraphFormattingOption.UseSuppressLineNumbers,
    TdxUsedParagraphFormattingOption.UseContextualSpacing, TdxUsedParagraphFormattingOption.UsePageBreakBefore,
		TdxUsedParagraphFormattingOption.UseBeforeAutoSpacing, TdxUsedParagraphFormattingOption.UseAfterAutoSpacing,
    TdxUsedParagraphFormattingOption.UseKeepWithNext, TdxUsedParagraphFormattingOption.UseKeepLinesTogether,
    TdxUsedParagraphFormattingOption.UseWidowOrphanControl, TdxUsedParagraphFormattingOption.UseOutlineLevel,
    TdxUsedParagraphFormattingOption.UseBackColor];

{ TdxParagraphFormattingInfo }

constructor TdxParagraphFormattingInfo.Create;
begin
  inherited Create; 
end;

procedure TdxParagraphFormattingInfo.CopyFrom(const Source: TdxParagraphFormattingInfo);
begin
  FPackedValues := Source.PackedValues;

  LeftIndent := Source.LeftIndent;
  RightIndent := Source.RightIndent;
  SpacingBefore := Source.SpacingBefore;
  SpacingAfter := Source.SpacingAfter;
  LineSpacing := Source.LineSpacing;
  FirstLineIndent := Source.FirstLineIndent;
  BackColor := Source.BackColor;
end;

function TdxParagraphFormattingInfo.Clone: TdxParagraphFormattingInfo;
begin
  Result := TdxParagraphFormattingInfoClass(ClassType).Create;
  Result.CopyFrom(Self);
end;

function TdxParagraphFormattingInfo.Equals(Obj: TObject): Boolean;
var
  AInfo: TdxParagraphFormattingInfo absolute Obj;
begin
  Assert(Obj is TdxParagraphFormattingInfo);
  Result :=
  //Alignment
  //FirstLineIndentType
  //LineSpacingType
  //SuppressHyphenation
  //SuppressLineNumbers
  //ContextualSpacing
  //PageBreakBefore
  //BeforeAutoSpacing
  //AfterAutoSpacing
  //KeepWithNext
  //KeepLinesTogether
  //WidowOrphanControl
  //OutlineLevel
    (FPackedValues = AInfo.PackedValues) and
    (LeftIndent = AInfo.LeftIndent) and
    (RightIndent = AInfo.RightIndent) and
    (SpacingAfter = AInfo.SpacingAfter) and
    (SpacingBefore = AInfo.SpacingBefore) and
    (SkipCompareLineSpacing or (LineSpacing = AInfo.LineSpacing)) and
    (SkipCompareFirstLineIndent or (FirstLineIndent = AInfo.FirstLineIndent)) and
    (BackColor = AInfo.BackColor);
end;

function TdxParagraphFormattingInfo.GetHashCode: Integer;
begin

  Result :=
    FPackedValues xor
    FLeftIndent xor
    FRightIndent xor
    FSpacingAfter xor
    FSpacingBefore;

  if not SkipCompareLineSpacing then
    Result := Result xor PInteger(@FLineSpacing)^;

  if not SkipCompareFirstLineIndent then
    Result := Result xor Integer(FFirstLineIndent);

  Result := Result xor Integer(FBackColor);
end;


function TdxParagraphFormattingInfo.SkipCompareLineSpacing: Boolean;
begin
  Result := False;
end;

function TdxParagraphFormattingInfo.SkipCompareFirstLineIndent: Boolean;
begin
  Result := False;
end;

function TdxParagraphFormattingInfo.GetBooleanValue(AMask: Integer): Boolean;
begin
  Result := FPackedValues and AMask <> 0;
end;

procedure TdxParagraphFormattingInfo.SetBooleanValue(AMask: Integer; AValue: Boolean);
begin
  if AValue then
    FPackedValues := FPackedValues or AMask
  else
    FPackedValues := FPackedValues and not AMask;
end;

function TdxParagraphFormattingInfo.GetAfterAutoSpacing: Boolean;
begin
  Result := GetBooleanValue(MaskAfterAutoSpacing);
end;

function TdxParagraphFormattingInfo.GetAlignment: TdxParagraphAlignment;
begin
  Result := TdxParagraphAlignment((FPackedValues and MaskAlignment) shr 5);
end;

function TdxParagraphFormattingInfo.GetBackColor: TColor;
begin
  Result := FBackColor;
end;

function TdxParagraphFormattingInfo.GetBeforeAutoSpacing: Boolean;
begin
  Result := GetBooleanValue(MaskBeforeAutoSpacing);
end;

function TdxParagraphFormattingInfo.GetContextualSpacing: Boolean;
begin
  Result := GetBooleanValue(MaskContextualSpacing);
end;

function TdxParagraphFormattingInfo.GetFirstLineIndent: Integer;
begin
  Result := FFirstLineIndent;
end;

function TdxParagraphFormattingInfo.GetFirstLineIndentType: TdxParagraphFirstLineIndent;
begin
  Result := TdxParagraphFirstLineIndent((FPackedValues and MaskFirstLineIndentType) shr 3);
end;

function TdxParagraphFormattingInfo.GetKeepLinesTogether: Boolean;
begin
  Result := GetBooleanValue(MaskKeepLinesTogether);
end;

function TdxParagraphFormattingInfo.GetKeepWithNext: Boolean;
begin
  Result := GetBooleanValue(MaskKeepWithNext);
end;

function TdxParagraphFormattingInfo.GetLeftIndent: Integer;
begin
  Result := FLeftIndent;
end;

function TdxParagraphFormattingInfo.GetLineSpacing: Single;
begin
  Result := FLineSpacing;
end;

function TdxParagraphFormattingInfo.GetLineSpacingType: TdxParagraphLineSpacing;
begin
  Result := TdxParagraphLineSpacing(FPackedValues and MaskLineSpacingType);
end;

function TdxParagraphFormattingInfo.GetOutlineLevel: Integer;
begin
  Result := (FPackedValues and MaskOutlineLevel) shr 7;
end;

function TdxParagraphFormattingInfo.GetPageBreakBefore: Boolean;
begin
  Result := GetBooleanValue(MaskPageBreakBefore);
end;

function TdxParagraphFormattingInfo.GetRightIndent: Integer;
begin
  Result := FRightIndent;
end;

function TdxParagraphFormattingInfo.GetSpacingAfter: Integer;
begin
  Result := FSpacingAfter;
end;

function TdxParagraphFormattingInfo.GetSpacingBefore: Integer;
begin
  Result := FSpacingBefore;
end;

function TdxParagraphFormattingInfo.GetSuppressHyphenation: Boolean;
begin
  Result := GetBooleanValue(MaskSuppressHyphenation);
end;

function TdxParagraphFormattingInfo.GetSuppressLineNumbers: Boolean;
begin
  Result := GetBooleanValue(MaskSuppressLineNumbers);
end;

function TdxParagraphFormattingInfo.GetWidowOrphanControl: Boolean;
begin
  Result := GetBooleanValue(MaskWidowOrphanControl);
end;

procedure TdxParagraphFormattingInfo.SetAfterAutoSpacing(const Value: Boolean);
begin
  SetBooleanValue(MaskAfterAutoSpacing, Value);
end;

procedure TdxParagraphFormattingInfo.SetAlignment(const Value: TdxParagraphAlignment);
begin
  FPackedValues := FPackedValues and not MaskAlignment;
  FPackedValues := FPackedValues or (Integer(Ord(Value)) shl 5);
end;

procedure TdxParagraphFormattingInfo.SetBackColor(const Value: TColor);
begin
  FBackColor := Value;
end;

procedure TdxParagraphFormattingInfo.SetBeforeAutoSpacing(const Value: Boolean);
begin
  SetBooleanValue(MaskBeforeAutoSpacing, Value);
end;

procedure TdxParagraphFormattingInfo.SetContextualSpacing(const Value: Boolean);
begin
  SetBooleanValue(MaskContextualSpacing, Value);
end;

procedure TdxParagraphFormattingInfo.SetFirstLineIndent(const Value: Integer);

  procedure Error;
  begin
    raise Exception.CreateFmt('Bad parameter: FirstLineIndent = %d', [Value]);
  end;

begin
  if Value < 0 then
    Error;
  FFirstLineIndent := Value;
end;

procedure TdxParagraphFormattingInfo.SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
begin
  FPackedValues := FPackedValues and not MaskFirstLineIndentType;
  FPackedValues := FPackedValues or (Integer(Ord(Value)) shl 3);
end;

procedure TdxParagraphFormattingInfo.SetKeepLinesTogether(const Value: Boolean);
begin
  SetBooleanValue(MaskKeepLinesTogether, Value);
end;

procedure TdxParagraphFormattingInfo.SetKeepWithNext(const Value: Boolean);
begin
  SetBooleanValue(MaskKeepWithNext, Value);
end;

procedure TdxParagraphFormattingInfo.SetLeftIndent(const Value: Integer);
begin
  FLeftIndent := Value;
end;

procedure TdxParagraphFormattingInfo.SetLineSpacing(const Value: Single);

  procedure Error;
  begin
    raise Exception.CreateFmt('Bad parameter: LineSpacing = %g', [Value]);
  end;

begin
  if Value < 0 then
    Error;
  FLineSpacing := Value;
end;

procedure TdxParagraphFormattingInfo.SetLineSpacingType(const Value: TdxParagraphLineSpacing);
begin
  FPackedValues := FPackedValues and not MaskLineSpacingType;
  FPackedValues := FPackedValues or Integer(Ord(Value));
end;

procedure TdxParagraphFormattingInfo.SetOutlineLevel(const Value: Integer);
begin
  FPackedValues := FPackedValues and not MaskOutlineLevel;
  FPackedValues := FPackedValues or ((Integer(Value) shl 7) and MaskOutlineLevel);
end;

procedure TdxParagraphFormattingInfo.SetPageBreakBefore(const Value: Boolean);
begin
  SetBooleanValue(MaskPageBreakBefore, Value);
end;

procedure TdxParagraphFormattingInfo.SetRightIndent(const Value: Integer);
begin
  FRightIndent := Value;
end;

procedure TdxParagraphFormattingInfo.SetSpacingAfter(const Value: Integer);

  procedure Error;
  begin
    raise Exception.CreateFmt('Bad parameter: SpacingAfter = %d', [Value]);
  end;

begin
  if Value < 0 then
    Error;
  FSpacingAfter := Value;
end;

procedure TdxParagraphFormattingInfo.SetSpacingBefore(const Value: Integer);

  procedure Error;
  begin
    raise Exception.CreateFmt('Bad parameter: SpacingBefore = %d', [Value]);
  end;

begin
  if Value < 0 then
    Error;
  FSpacingBefore := Value;
end;

procedure TdxParagraphFormattingInfo.SetSuppressHyphenation(const Value: Boolean);
begin
  SetBooleanValue(MaskSuppressHyphenation, Value);
end;

procedure TdxParagraphFormattingInfo.SetSuppressLineNumbers(const Value: Boolean);
begin
  SetBooleanValue(MaskSuppressLineNumbers, Value);
end;

procedure TdxParagraphFormattingInfo.SetWidowOrphanControl(const Value: Boolean);
begin
  SetBooleanValue(MaskWidowOrphanControl, Value);
end;

{ TdxParagraphFormattingOptions }

constructor TdxParagraphFormattingOptions.Create(AUsedValues: TdxUsedParagraphFormattingOptions = []);
begin
  inherited Create;
  FValue := AUsedValues;
end;

procedure TdxParagraphFormattingOptions.CopyFrom(const Source: TdxParagraphFormattingOptions);
begin
  FValue := Source.FValue;
end;

function TdxParagraphFormattingOptions.Clone: TdxParagraphFormattingOptions;
begin
  Result := TdxParagraphFormattingOptions.Create;
  Result.CopyFrom(Self);
end;

function TdxParagraphFormattingOptions.Equals(Obj: TObject): Boolean;
begin
  Result := Value = TdxParagraphFormattingOptions(Obj).Value;
end;

function TdxParagraphFormattingOptions.GetHashCode: Integer;
begin
  Result := Integer(Value);
end;

function TdxParagraphFormattingOptions.GetValue(const Index: TdxUsedParagraphFormattingOption): Boolean;
begin
  Result := Index in FValue;
end;

procedure TdxParagraphFormattingOptions.SetValue(const Index: TdxUsedParagraphFormattingOption; const Value: Boolean);
begin
  if Value then
    Include(FValue, Index)
  else
    Exclude(FValue, Index);
end;

{ TdxParagraphFormattingBase }

procedure TdxParagraphFormattingBase.CopyFrom(const Source: TdxParagraphFormattingBase);
begin
  CopyFrom(Source.Info, Source.Options);
end;

procedure TdxParagraphFormattingBase.CopyFrom(const AInfo: TdxParagraphFormattingInfo; const AOptions: TdxParagraphFormattingOptions);
begin
  CopyFromCore(AInfo, AOptions);
end;

function TdxParagraphFormattingBase.Clone: TdxParagraphFormattingBase;
begin
  Result := TdxParagraphFormattingBase.Create(PieceTable, DocumentModel, InfoIndex, OptionsIndex);
end;

function TdxParagraphFormattingBase.GetInfoCache: TdxUniqueItemsCache<TdxParagraphFormattingInfo>;
begin
  Result := TdxDocumentModel(DocumentModel).Cache.ParagraphFormattingInfoCache;
end;

function TdxParagraphFormattingBase.GetOptionsCache: TdxUniqueItemsCache<TdxParagraphFormattingOptions>;
begin
  Result := TdxDocumentModel(DocumentModel).Cache.ParagraphFormattingOptionsCache;
end;

function TdxParagraphFormattingBase.PropertyEquals(const AOther: TdxIndexBasedObject<TdxParagraphFormattingInfo, TdxParagraphFormattingOptions>): Boolean;
begin
  Result := (Options.Value = AOther.Options.Value) and Info.Equals(AOther.Info);
end;

function TdxParagraphFormattingBase.GetAfterAutoSpacing: Boolean;
begin
  Result := Info.AfterAutoSpacing;
end;

function TdxParagraphFormattingBase.GetAlignment: TdxParagraphAlignment;
begin
  Result := Info.Alignment;
end;

function TdxParagraphFormattingBase.GetBackColor: TColor;
begin
  Result := Info.BackColor;
end;

function TdxParagraphFormattingBase.GetBeforeAutoSpacing: Boolean;
begin
  Result := Info.BeforeAutoSpacing;
end;

function TdxParagraphFormattingBase.GetContextualSpacing: Boolean;
begin
  Result := Info.ContextualSpacing;
end;

function TdxParagraphFormattingBase.GetFirstLineIndent: Integer;
begin
  Result := Info.FirstLineIndent;
end;

function TdxParagraphFormattingBase.GetFirstLineIndentType: TdxParagraphFirstLineIndent;
begin
  Result := Info.FirstLineIndentType;
end;

function TdxParagraphFormattingBase.GetKeepLinesTogether: Boolean;
begin
  Result := Info.KeepLinesTogether;
end;

function TdxParagraphFormattingBase.GetKeepWithNext: Boolean;
begin
  Result := Info.KeepWithNext;
end;

function TdxParagraphFormattingBase.GetLeftIndent: Integer;
begin
  Result := Info.LeftIndent;
end;

function TdxParagraphFormattingBase.GetLineSpacing: Single;
begin
  Result := Info.LineSpacing;
end;

function TdxParagraphFormattingBase.GetLineSpacingType: TdxParagraphLineSpacing;
begin
  Result := Info.LineSpacingType;
end;

function TdxParagraphFormattingBase.GetOutlineLevel: Integer;
begin
  Result := Info.OutlineLevel;
end;

function TdxParagraphFormattingBase.GetPageBreakBefore: Boolean;
begin
  Result := Info.PageBreakBefore;
end;

function TdxParagraphFormattingBase.GetRightIndent: Integer;
begin
  Result := Info.RightIndent;
end;

function TdxParagraphFormattingBase.GetSpacingAfter: Integer;
begin
  Result := Info.SpacingAfter;
end;

function TdxParagraphFormattingBase.GetSpacingBefore: Integer;
begin
  Result := Info.SpacingBefore;
end;

function TdxParagraphFormattingBase.GetSuppressHyphenation: Boolean;
begin
  Result := Info.SuppressHyphenation;
end;

function TdxParagraphFormattingBase.GetSuppressLineNumbers: Boolean;
begin
  Result := Info.SuppressLineNumbers;
end;

function TdxParagraphFormattingBase.GetWidowOrphanControl: Boolean;
begin
  Result := Info.WidowOrphanControl;
end;

procedure TdxParagraphFormattingBase.SetAfterAutoSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.AfterAutoSpacing := Value;
end;

procedure TdxParagraphFormattingBase.SetUseAfterAutoSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseAfterAutoSpacing := AValue;
end;

procedure TdxParagraphFormattingBase.SetAfterAutoSpacing(const Value: Boolean);
begin
  if (Info.AfterAutoSpacing = Value) and Options.UseAfterAutoSpacing then
    Exit;
  SetPropertyValue<Boolean>(SetAfterAutoSpacingCore, Value, SetUseAfterAutoSpacingCore);
end;

procedure TdxParagraphFormattingBase.SetAlignmentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.Alignment := TdxParagraphAlignment(Value);
end;

procedure TdxParagraphFormattingBase.SetUseAlignmentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseAlignment := AValue;
end;

procedure TdxParagraphFormattingBase.SetAlignment(const Value: TdxParagraphAlignment);
begin
  if (Info.Alignment = Value) and Options.UseAlignment then
    Exit;
  SetPropertyValue<Integer>(SetAlignmentCore, Ord(Value), SetUseAlignmentCore);
end;

procedure TdxParagraphFormattingBase.SetBackColorCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.BackColor := Value;
end;

procedure TdxParagraphFormattingBase.SetUseBackColorCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseBackColor := AValue;
end;

procedure TdxParagraphFormattingBase.SetBackColor(const Value: TColor);
begin
  if (Info.BackColor = Value) and Options.UseBackColor then
    Exit;
  SetPropertyValue<Integer>(SetBackColorCore, Value, SetUseBackColorCore);
end;

procedure TdxParagraphFormattingBase.SetBeforeAutoSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.BeforeAutoSpacing := Value;
end;

procedure TdxParagraphFormattingBase.SetUseBeforeAutoSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseBeforeAutoSpacing := AValue;
end;

procedure TdxParagraphFormattingBase.SetBeforeAutoSpacing(const Value: Boolean);
begin
  if (Info.BeforeAutoSpacing = Value) and Options.UseBeforeAutoSpacing then
    Exit;
  SetPropertyValue<Boolean>(SetBeforeAutoSpacingCore, Value, SetUseBeforeAutoSpacingCore);
end;

procedure TdxParagraphFormattingBase.SetContextualSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.ContextualSpacing := Value;
end;

procedure TdxParagraphFormattingBase.SetUseContextualSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseContextualSpacing := AValue;
end;

procedure TdxParagraphFormattingBase.SetContextualSpacing(const Value: Boolean);
begin
  if (Info.ContextualSpacing = Value) and Options.UseContextualSpacing then
    Exit;
  SetPropertyValue<Boolean>(SetContextualSpacingCore, Value, SetUseContextualSpacingCore);
end;

procedure TdxParagraphFormattingBase.SetFirstLineIndentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.FirstLineIndent := Value;
end;

procedure TdxParagraphFormattingBase.SetUseFirstLineIndentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseFirstLineIndent := AValue;
end;

procedure TdxParagraphFormattingBase.SetFirstLineIndent(const Value: Integer);
begin
  if (Info.FirstLineIndent = Value) and Options.UseFirstLineIndent then
    Exit;
  SetPropertyValue<Integer>(SetFirstLineIndentCore, Value, SetUseFirstLineIndentCore);
end;

procedure TdxParagraphFormattingBase.SetFirstLineIndentTypeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.FirstLineIndentType := TdxParagraphFirstLineIndent(Value);
  AInfo.FirstLineIndent := 0;
end;

procedure TdxParagraphFormattingBase.SetUseFirstLineIndentTypeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseFirstLineIndent := AValue;
end;

procedure TdxParagraphFormattingBase.SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
begin
  if (Info.FirstLineIndentType = Value) and Options.UseFirstLineIndent then
    Exit;
  SetPropertyValue<Integer>(SetFirstLineIndentTypeCore, Ord(Value), SetUseFirstLineIndentTypeCore);
end;

procedure TdxParagraphFormattingBase.SetKeepLinesTogetherCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.KeepLinesTogether := Value;
end;

procedure TdxParagraphFormattingBase.SetUseKeepLinesTogetherCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseKeepLinesTogether := AValue;
end;

procedure TdxParagraphFormattingBase.SetKeepLinesTogether(const Value: Boolean);
begin
  if (Info.KeepLinesTogether = Value) and Options.UseKeepLinesTogether then
    Exit;
  SetPropertyValue<Boolean>(SetKeepLinesTogetherCore, Value, SetUseKeepLinesTogetherCore);
end;

procedure TdxParagraphFormattingBase.SetKeepWithNextCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.KeepWithNext := Value;
end;

procedure TdxParagraphFormattingBase.SetUseKeepWithNextCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseKeepWithNext := AValue;
end;

procedure TdxParagraphFormattingBase.SetKeepWithNext(const Value: Boolean);
begin
  if (Info.KeepWithNext = Value) and Options.UseKeepWithNext then
    Exit;
  SetPropertyValue<Boolean>(SetKeepWithNextCore, Value, SetUseKeepWithNextCore);
end;

procedure TdxParagraphFormattingBase.SetLeftIndentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.LeftIndent := Value;
end;

procedure TdxParagraphFormattingBase.SetUseLeftIndentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseLeftIndent := AValue;
end;

procedure TdxParagraphFormattingBase.SetLeftIndent(const Value: Integer);
begin
  if (Info.LeftIndent = Value) and Options.UseLeftIndent then
    Exit;
  SetPropertyValue<Integer>(SetLeftIndentCore, Value, SetUseLeftIndentCore);
end;

procedure TdxParagraphFormattingBase.SetLineSpacingCore(const AInfo: TdxParagraphFormattingInfo; const Value: Single);
begin
  AInfo.LineSpacing := Value;
end;

procedure TdxParagraphFormattingBase.SetUseLineSpacingCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseLineSpacing := AValue;
end;

procedure TdxParagraphFormattingBase.SetLineSpacing(const Value: Single);
begin
  if (Info.LineSpacing = Value) and Options.UseLineSpacing then
    Exit;
  SetPropertyValue<Single>(SetLineSpacingCore, Value, SetUseLineSpacingCore);
end;

procedure TdxParagraphFormattingBase.SetLineSpacingTypeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.LineSpacingType := TdxParagraphLineSpacing(Value);
  AInfo.LineSpacing := 0;
end;

procedure TdxParagraphFormattingBase.SetUseLineSpacingTypeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseLineSpacing := AValue;
end;

procedure TdxParagraphFormattingBase.SetLineSpacingType(const Value: TdxParagraphLineSpacing);
begin
  if (Info.LineSpacingType = Value) and Options.UseLineSpacing then
    Exit;
  SetPropertyValue<Integer>(SetLineSpacingTypeCore, Ord(Value), SetUseLineSpacingTypeCore);
end;

procedure TdxParagraphFormattingBase.SetOutlineLevelCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.OutlineLevel := Value;
end;

procedure TdxParagraphFormattingBase.SetUseOutlineLevelCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseOutlineLevel := AValue;
end;

procedure TdxParagraphFormattingBase.SetOutlineLevel(const Value: Integer);
begin
  if (Info.OutlineLevel = Value) and Options.UseOutlineLevel then
    Exit;
  SetPropertyValue<Integer>(SetOutlineLevelCore, Value, SetUseOutlineLevelCore);
end;

procedure TdxParagraphFormattingBase.SetPageBreakBeforeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.PageBreakBefore := Value;
end;

procedure TdxParagraphFormattingBase.SetUsePageBreakBeforeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UsePageBreakBefore := AValue;
end;

procedure TdxParagraphFormattingBase.SetPageBreakBefore(const Value: Boolean);
begin
  if (Info.PageBreakBefore = Value) and Options.UsePageBreakBefore then
    Exit;
  SetPropertyValue<Boolean>(SetPageBreakBeforeCore, Value, SetUsePageBreakBeforeCore);
end;

procedure TdxParagraphFormattingBase.SetRightIndentCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.RightIndent := Value;
end;

procedure TdxParagraphFormattingBase.SetUseRightIndentCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseRightIndent := AValue;
end;

procedure TdxParagraphFormattingBase.SetRightIndent(const Value: Integer);
begin
  if (Info.RightIndent = Value) and Options.UseRightIndent then
    Exit;
  SetPropertyValue<Integer>(SetRightIndentCore, Value, SetUseRightIndentCore);
end;

procedure TdxParagraphFormattingBase.SetSpacingAfterCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.SpacingAfter := Value;
end;

procedure TdxParagraphFormattingBase.SetUseSpacingAfterCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseSpacingAfter := AValue;
end;

procedure TdxParagraphFormattingBase.SetSpacingAfter(const Value: Integer);
begin
  if (Info.SpacingAfter = Value) and Options.UseSpacingAfter then
    Exit;
  SetPropertyValue<Integer>(SetSpacingAfterCore, Value, SetUseSpacingAfterCore);
end;

procedure TdxParagraphFormattingBase.SetSpacingBeforeCore(const AInfo: TdxParagraphFormattingInfo; const Value: Integer);
begin
  AInfo.SpacingBefore := Value;
end;

procedure TdxParagraphFormattingBase.SetUseSpacingBeforeCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseSpacingBefore := AValue;
end;

procedure TdxParagraphFormattingBase.SetSpacingBefore(const Value: Integer);
begin
  if (Info.SpacingBefore = Value) and Options.UseSpacingBefore then
    Exit;
  SetPropertyValue<Integer>(SetSpacingBeforeCore, Value, SetUseSpacingBeforeCore);
end;

procedure TdxParagraphFormattingBase.SetSuppressHyphenationCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.SuppressHyphenation := Value;
end;

procedure TdxParagraphFormattingBase.SetUseSuppressHyphenationCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseSuppressHyphenation := AValue;
end;

procedure TdxParagraphFormattingBase.SetSuppressHyphenation(const Value: Boolean);
begin
  if (Info.SuppressHyphenation = Value) and Options.UseSuppressHyphenation then
    Exit;
  SetPropertyValue<Boolean>(SetSuppressHyphenationCore, Value, SetUseSuppressHyphenationCore);
end;

procedure TdxParagraphFormattingBase.SetSuppressLineNumbersCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.SuppressLineNumbers := True;
end;

procedure TdxParagraphFormattingBase.SetUseSuppressLineNumbersCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseSuppressLineNumbers := AValue;
end;

procedure TdxParagraphFormattingBase.SetSuppressLineNumbers(const Value: Boolean);
begin
  if (Info.SuppressLineNumbers = Value) and Options.UseSuppressLineNumbers then
    Exit;
  SetPropertyValue<Boolean>(SetSuppressLineNumbersCore, Value, SetUseSuppressLineNumbersCore);
end;

procedure TdxParagraphFormattingBase.SetWidowOrphanControlCore(const AInfo: TdxParagraphFormattingInfo; const Value: Boolean);
begin
  AInfo.WidowOrphanControl := Value;
end;

procedure TdxParagraphFormattingBase.SetUseWidowOrphanControlCore(const AOptions: TdxParagraphFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseWidowOrphanControl := AValue;
end;

procedure TdxParagraphFormattingBase.SetWidowOrphanControl(const Value: Boolean);
begin
  if (Info.WidowOrphanControl = Value) and Options.UseWidowOrphanControl then
    Exit;
  SetPropertyValue<Boolean>(SetWidowOrphanControlCore, Value, SetUseWidowOrphanControlCore);
end;

{ TdxParagraphProperties }

constructor TdxParagraphProperties.Create(const AOwner: IdxParagraphPropertiesContainer);
begin
  inherited Create(GetPieceTable(AOwner) as TdxPieceTable);
  FOwner := AOwner;
end;

destructor TdxParagraphProperties.Destroy;
begin
  inherited Destroy;
end;

function TdxParagraphProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.BatchUpdate);
end;

class procedure TdxParagraphProperties.ApplyPropertiesDiff(ATarget: TdxParagraphProperties; ATargetMergedInfo,
  ASourceMergedInfo: TdxParagraphFormattingInfo);
begin
  if ATargetMergedInfo.Alignment <> ASourceMergedInfo.Alignment then
    ATarget.Alignment := ASourceMergedInfo.Alignment;

  if ATargetMergedInfo.FirstLineIndent <> ASourceMergedInfo.FirstLineIndent then
    ATarget.FirstLineIndent := ASourceMergedInfo.FirstLineIndent;

  if ATargetMergedInfo.FirstLineIndentType <> ASourceMergedInfo.FirstLineIndentType then
    ATarget.FirstLineIndentType := ASourceMergedInfo.FirstLineIndentType;

  if ATargetMergedInfo.LeftIndent <> ASourceMergedInfo.LeftIndent then
    ATarget.LeftIndent := ASourceMergedInfo.LeftIndent;

  if ATargetMergedInfo.LineSpacingType <> ASourceMergedInfo.LineSpacingType then 
    ATarget.LineSpacingType := ASourceMergedInfo.LineSpacingType;

  if ATargetMergedInfo.LineSpacing <> ASourceMergedInfo.LineSpacing then 
    ATarget.LineSpacing := ASourceMergedInfo.LineSpacing;

  if ATargetMergedInfo.RightIndent <> ASourceMergedInfo.RightIndent then
    ATarget.RightIndent := ASourceMergedInfo.RightIndent;

  if ATargetMergedInfo.SpacingAfter <> ASourceMergedInfo.SpacingAfter then
    ATarget.SpacingAfter := ASourceMergedInfo.SpacingAfter;

  if ATargetMergedInfo.SpacingBefore <> ASourceMergedInfo.SpacingBefore then
    ATarget.SpacingBefore := ASourceMergedInfo.SpacingBefore;

  if ATargetMergedInfo.SuppressHyphenation <> ASourceMergedInfo.SuppressHyphenation then
    ATarget.SuppressHyphenation := ASourceMergedInfo.SuppressHyphenation;

  if ATargetMergedInfo.SuppressLineNumbers <> ASourceMergedInfo.SuppressLineNumbers then
    ATarget.SuppressLineNumbers := ASourceMergedInfo.SuppressLineNumbers;

  if ATargetMergedInfo.ContextualSpacing <> ASourceMergedInfo.ContextualSpacing then
    ATarget.ContextualSpacing := ASourceMergedInfo.ContextualSpacing;

  if ATargetMergedInfo.PageBreakBefore <> ASourceMergedInfo.PageBreakBefore then
    ATarget.PageBreakBefore := ASourceMergedInfo.PageBreakBefore;

  if ATargetMergedInfo.OutlineLevel <> ASourceMergedInfo.OutlineLevel then
    ATarget.OutlineLevel := ASourceMergedInfo.OutlineLevel;

  if ATargetMergedInfo.BackColor <> ASourceMergedInfo.BackColor then
    ATarget.BackColor := ASourceMergedInfo.BackColor;
end;

procedure TdxParagraphProperties.CopyFrom(const AParagraphProperties: TdxMergedProperties<TdxParagraphFormattingInfo, TdxParagraphFormattingOptions>);
var
  AInfo: TdxParagraphFormattingBase;
  AIsDeferredInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AInfo.CopyFromCore(AParagraphProperties.Info, AParagraphProperties.Options);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
end;

function TdxParagraphProperties.Equals(Obj: TObject): Boolean;
var
  AOther: TdxParagraphProperties absolute Obj;
begin
  Result := Obj is TdxParagraphProperties;
  if Result then
  begin
    if DocumentModel = AOther.DocumentModel then
      Result := Index = AOther.Index
    else
      Result := Info.Equals(AOther.Info);
  end;
end;

procedure TdxParagraphProperties.Reset;
var
  AInfo: TdxParagraphFormattingBase;
  AEmptyInfo: TdxParagraphFormattingBase;
  AIsDeferredInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AEmptyInfo := GetCache(DocumentModel)[TdxParagraphFormattingCache.EmptyParagraphFormattingOptionIndex];
  AInfo.ReplaceInfo(AEmptyInfo.Info, AEmptyInfo.Options);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
end;

procedure TdxParagraphProperties.ResetUse(AMask: TdxUsedParagraphFormattingOptions);
var
  AInfo: TdxParagraphFormattingBase;
  AOptions: TdxParagraphFormattingOptions;
  AFormattingInfo: TdxParagraphFormattingInfo;
  AIsDeferredInfo, AIsDeferredOptions, AIsDefferedFormattingInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AOptions := AInfo.GetOptionsForModification(AIsDeferredOptions);
  AOptions.FValue := AOptions.Value - AMask; 
  AFormattingInfo := AInfo.GetInfoForModification(AIsDefferedFormattingInfo);
  AInfo.ReplaceInfo(AFormattingInfo, AOptions);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
  if not AIsDeferredOptions then AOptions.Free;
  if not AIsDefferedFormattingInfo then AFormattingInfo.Free;
end;

procedure TdxParagraphProperties.ResetAllUse;
var
  AInfo: TdxParagraphFormattingBase;
  AOptions: TdxParagraphFormattingOptions;
  AFormattingInfo: TdxParagraphFormattingInfo;
  AIsDeferredInfo, AIsDeferredOptions, AIsDefferedFormattingInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AOptions := AInfo.GetOptionsForModification(AIsDeferredOptions);
  AOptions.FValue := [];
  AFormattingInfo := AInfo.GetInfoForModification(AIsDefferedFormattingInfo);
  AInfo.ReplaceInfo(AFormattingInfo, AOptions);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
  if not AIsDeferredOptions then AOptions.Free;
  if not AIsDefferedFormattingInfo then AFormattingInfo.Free;
end;

procedure TdxParagraphProperties.Merge(AProperties: TdxParagraphProperties);
var
  AMerger: TdxParagraphPropertiesMerger;
begin
  AMerger := TdxParagraphPropertiesMerger.Create(Self);
  try
    AMerger.Merge(AProperties);
    CopyFrom(AMerger.MergedProperties);
  finally
    AMerger.Free;
  end;
end;

function TdxParagraphProperties.GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxParagraphFormattingBase>;
begin
  Result := TdxDocumentModel(DocumentModel).Cache.ParagraphFormattingCache;
end;

procedure TdxParagraphProperties.OnIndexChanged;
begin
  inherited OnIndexChanged;
  FOwner.OnParagraphPropertiesChanged
end;

function TdxParagraphProperties.CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := FOwner.CreateParagraphPropertiesChangedHistoryItem;
end;

function TdxParagraphProperties.GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener;
begin
  if not Supports(FOwner, IdxObtainAffectedRangeListener, Result) then
    Result := nil;
end;

function TdxParagraphProperties.GetPieceTable(AOwner: IdxParagraphPropertiesContainer): TObject;
begin
  Result := AOwner.PieceTable;
end;

function TdxParagraphProperties.GetAfterAutoSpacing: Boolean;
begin
  Result := Info.AfterAutoSpacing;
end;

function TdxParagraphProperties.GetAlignment: TdxParagraphAlignment;
begin
  Result := Info.Alignment;
end;

function TdxParagraphProperties.GetBackColor: TColor;
begin
  Result := Info.BackColor;
end;

function TdxParagraphProperties.GetBeforeAutoSpacing: Boolean;
begin
  Result := Info.BeforeAutoSpacing;
end;

function TdxParagraphProperties.GetContextualSpacing: Boolean;
begin
  Result := Info.ContextualSpacing;
end;

function TdxParagraphProperties.GetFirstLineIndent: Integer;
begin
  Result := Info.FirstLineIndent;
end;

function TdxParagraphProperties.GetFirstLineIndentType: TdxParagraphFirstLineIndent;
begin
  Result := Info.FirstLineIndentType;
end;

function TdxParagraphProperties.GetHashCode: Integer;
begin
  Result := Index;
end;

function TdxParagraphProperties.GetKeepLinesTogether: Boolean;
begin
  Result := Info.KeepLinesTogether;
end;

function TdxParagraphProperties.GetKeepWithNext: Boolean;
begin
  Result := Info.KeepWithNext;
end;

function TdxParagraphProperties.GetLeftIndent: Integer;
begin
  Result := Info.LeftIndent;
end;

function TdxParagraphProperties.GetLineSpacing: Single;
begin
  Result := Info.LineSpacing;
end;

function TdxParagraphProperties.GetLineSpacingType: TdxParagraphLineSpacing;
begin
  Result := Info.LineSpacingType;
end;

function TdxParagraphProperties.GetOutlineLevel: Integer;
begin
  Result := Info.OutlineLevel;
end;

function TdxParagraphProperties.GetPageBreakBefore: Boolean;
begin
  Result := Info.PageBreakBefore;
end;

function TdxParagraphProperties.GetRightIndent: Integer;
begin
  Result := Info.RightIndent;
end;

function TdxParagraphProperties.GetSpacingAfter: Integer;
begin
  Result := Info.SpacingAfter;
end;

function TdxParagraphProperties.GetSpacingBefore: Integer;
begin
  Result := Info.SpacingBefore;
end;

function TdxParagraphProperties.GetSuppressHyphenation: Boolean;
begin
  Result := Info.SuppressHyphenation;
end;

function TdxParagraphProperties.GetSuppressLineNumbers: Boolean;
begin
  Result := Info.SuppressLineNumbers;
end;

function TdxParagraphProperties.GetWidowOrphanControl: Boolean;
begin
  Result := Info.WidowOrphanControl;
end;

function TdxParagraphProperties.GetUseAlignment: Boolean;
begin
  Result := Info.Options.UseAlignment;
end;

function TdxParagraphProperties.GetUseLeftIndent: Boolean;
begin
  Result := Info.Options.UseLeftIndent;
end;

function TdxParagraphProperties.GetUseRightIndent: Boolean;
begin
  Result := Info.Options.UseRightIndent;
end;

function TdxParagraphProperties.GetUseSpacingBefore: Boolean;
begin
  Result := Info.Options.UseSpacingBefore;
end;

function TdxParagraphProperties.GetUseSpacingAfter: Boolean;
begin
  Result := Info.Options.UseSpacingAfter;
end;

function TdxParagraphProperties.GetUseLineSpacing: Boolean;
begin
  Result := Info.Options.UseLineSpacing;
end;

function TdxParagraphProperties.GetUseFirstLineIndent: Boolean;
begin
  Result := Info.Options.UseFirstLineIndent;
end;

function TdxParagraphProperties.GetUseSuppressHyphenation: Boolean;
begin
  Result := Info.Options.UseSuppressHyphenation;
end;

function TdxParagraphProperties.GetUseSuppressLineNumbers: Boolean;
begin
  Result := Info.Options.UseSuppressLineNumbers;
end;

function TdxParagraphProperties.GetUseContextualSpacing: Boolean;
begin
  Result := Info.Options.UseContextualSpacing;
end;

function TdxParagraphProperties.GetUsePageBreakBefore: Boolean;
begin
  Result := Info.Options.UsePageBreakBefore;
end;

function TdxParagraphProperties.GetUseBeforeAutoSpacing: Boolean;
begin
  Result := Info.Options.UseBeforeAutoSpacing;
end;

function TdxParagraphProperties.GetUseAfterAutoSpacing: Boolean;
begin
  Result := Info.Options.UseAfterAutoSpacing;
end;

function TdxParagraphProperties.GetUseKeepWithNext: Boolean;
begin
  Result := Info.Options.UseKeepWithNext;
end;

function TdxParagraphProperties.GetUseKeepLinesTogether: Boolean;
begin
  Result := Info.Options.UseKeepLinesTogether;
end;

function TdxParagraphProperties.GetUseWidowOrphanControl: Boolean;
begin
  Result := Info.Options.UseWidowOrphanControl;
end;

function TdxParagraphProperties.GetUseOutlineLevel: Boolean;
begin
  Result := Info.Options.UseOutlineLevel;
end;

function TdxParagraphProperties.GetUseBackColor: Boolean;
begin
  Result := Info.Options.UseBackColor;
end;

function TdxParagraphProperties.SetAfterAutoSpacingCore(const AInfo: TdxParagraphFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.AfterAutoSpacing := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.AfterAutoSpacing);
end;

procedure TdxParagraphProperties.SetAfterAutoSpacing(const Value: Boolean);
begin
  if (Info.AfterAutoSpacing = Value) and UseAfterAutoSpacing then
    Exit;
  SetPropertyValue<Boolean>(SetAfterAutoSpacingCore, Value);
end;

procedure TdxParagraphProperties.SetAlignment(const Value: TdxParagraphAlignment);
begin
  if (Alignment = Value) and UseAlignment then
    Exit;
  SetPropertyValue<Integer>(SetAlignmentCore, Ord(Value));
end;

function TdxParagraphProperties.SetAlignmentCore(const AInfo: TdxParagraphFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.Alignment := TdxParagraphAlignment(Value);
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.Alignment);
end;

procedure TdxParagraphProperties.SetBackColor(const Value: TColor);
begin
  if (BackColor = Value) and UseBackColor then
    Exit;
  SetPropertyValue<Integer>(SetBackColorCore, Value);
end;

function TdxParagraphProperties.SetBackColorCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.BackColor := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.BackColor);
end;

procedure TdxParagraphProperties.SetBeforeAutoSpacing(const Value: Boolean);
begin
  if (BeforeAutoSpacing = Value) and UseBeforeAutoSpacing then
    Exit;
  SetPropertyValue<Boolean>(SetBeforeAutoSpacingCore, Value);
end;

function TdxParagraphProperties.SetBeforeAutoSpacingCore(
  const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.BeforeAutoSpacing := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.BeforeAutoSpacing);
end;

procedure TdxParagraphProperties.SetContextualSpacing(const Value: Boolean);
begin
  if (ContextualSpacing = Value) and UseContextualSpacing then
    Exit;
  SetPropertyValue<Boolean>(SetContextualSpacingCore, Value);
end;

function TdxParagraphProperties.SetContextualSpacingCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.ContextualSpacing := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.ContextualSpacing);
end;

procedure TdxParagraphProperties.SetFirstLineIndent(const Value: Integer);
begin
  if (FirstLineIndent = Value) and UseFirstLineIndent then
    Exit;
  SetPropertyValue<Integer>(SetFirstLineIndentCore, Value);
end;

function TdxParagraphProperties.SetFirstLineIndentCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.FirstLineIndent := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.FirstLineIndent);
end;

procedure TdxParagraphProperties.SetFirstLineIndentType(const Value: TdxParagraphFirstLineIndent);
begin
  if (FirstLineIndentType = Value) and UseFirstLineIndent then
    Exit;
  SetPropertyValue<Integer>(SetFirstLineIndentTypeCore, Ord(Value));
end;

function TdxParagraphProperties.SetFirstLineIndentTypeCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.FirstLineIndentType := TdxParagraphFirstLineIndent(Value);
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.FirstLineIndentType);
end;

procedure TdxParagraphProperties.SetKeepLinesTogether(const Value: Boolean);
begin
  if (KeepLinesTogether = Value) and UseKeepLinesTogether then
    Exit;
  SetPropertyValue<Boolean>(SetKeepLinesTogetherCore, Value);
end;

function TdxParagraphProperties.SetKeepLinesTogetherCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.KeepLinesTogether := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.Alignment);
end;

procedure TdxParagraphProperties.SetKeepWithNext(const Value: Boolean);
begin
  if (KeepWithNext = Value) and UseKeepWithNext then
    Exit;
  SetPropertyValue<Boolean>(SetKeepWithNextCore, Value);
end;

function TdxParagraphProperties.SetKeepWithNextCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.KeepWithNext := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.KeepWithNext);
end;

procedure TdxParagraphProperties.SetLeftIndent(const Value: Integer);
begin
  if (LeftIndent = Value) and UseLeftIndent then
    Exit;
  SetPropertyValue<Integer>(SetLeftIndentCore, Value);
end;

function TdxParagraphProperties.SetLeftIndentCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.LeftIndent := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.LeftIndent);
end;

procedure TdxParagraphProperties.SetLineSpacing(const Value: Single);
begin
  if (LineSpacing = Value) and UseLineSpacing then
    Exit;
  SetPropertyValue<Single>(SetLineSpacingCore, Value);
end;

function TdxParagraphProperties.SetLineSpacingCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Single): TdxDocumentModelChangeActions;
begin
  AInfo.LineSpacing := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.LineSpacing);
end;

procedure TdxParagraphProperties.SetLineSpacingType(const Value: TdxParagraphLineSpacing);
begin
  if (LineSpacingType = Value) and UseLineSpacing then
    Exit;
  SetPropertyValue<Integer>(SetLineSpacingTypeCore, Ord(Value));
end;

function TdxParagraphProperties.SetLineSpacingTypeCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.LineSpacingType := TdxParagraphLineSpacing(Value);
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.LineSpacingType);
end;

procedure TdxParagraphProperties.SetOutlineLevel(const Value: Integer);
begin
  if (OutlineLevel = Value) and UseOutlineLevel then
    Exit;
  SetPropertyValue<Integer>(SetOutlineLevelCore, Value);
end;

function TdxParagraphProperties.SetOutlineLevelCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.OutlineLevel := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.OutlineLevel);
end;

procedure TdxParagraphProperties.SetPageBreakBefore(const Value: Boolean);
begin
  if (PageBreakBefore = Value) and UsePageBreakBefore then
    Exit;
  SetPropertyValue<Boolean>(SetPageBreakBeforeCore, Value);
end;

function TdxParagraphProperties.SetPageBreakBeforeCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.PageBreakBefore := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.PageBreakBefore);
end;

procedure TdxParagraphProperties.SetRightIndent(const Value: Integer);
begin
  if (RightIndent = Value) and UseRightIndent then
    Exit;
  SetPropertyValue<Integer>(SetRightIndentCore, Value);
end;

function TdxParagraphProperties.SetRightIndentCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.RightIndent := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.RightIndent);
end;

procedure TdxParagraphProperties.SetSpacingAfter(const Value: Integer);
begin
  if (SpacingAfter = Value) and UseSpacingAfter then
    Exit;
  SetPropertyValue<Integer>(SetSpacingAfterCore, Value);
end;

function TdxParagraphProperties.SetSpacingAfterCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.SpacingAfter := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.SpacingAfter);
end;

procedure TdxParagraphProperties.SetSpacingBefore(const Value: Integer);
begin
  if (SpacingBefore = Value) and UseSpacingBefore then
    Exit;
  SetPropertyValue<Integer>(SetSpacingBeforeCore, Value);
end;

function TdxParagraphProperties.SetSpacingBeforeCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.SpacingBefore := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.SpacingBefore);
end;

procedure TdxParagraphProperties.SetSuppressHyphenation(const Value: Boolean);
begin
  if (SuppressHyphenation = Value) and UseSuppressHyphenation then
    Exit;
  SetPropertyValue<Boolean>(SetSuppressHyphenationCore, Value);
end;

function TdxParagraphProperties.SetSuppressHyphenationCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.SuppressHyphenation := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.SuppressHyphenation);
end;

procedure TdxParagraphProperties.SetSuppressLineNumbers(const Value: Boolean);
begin
  if (SuppressLineNumbers = Value) and UseSuppressLineNumbers then
    Exit;
  SetPropertyValue<Boolean>(SetSuppressLineNumbersCore, Value);
end;

function TdxParagraphProperties.SetSuppressLineNumbersCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.SuppressLineNumbers := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.SuppressLineNumbers);
end;

procedure TdxParagraphProperties.SetWidowOrphanControl(const Value: Boolean);
begin
  if (WidowOrphanControl = Value) and UseWidowOrphanControl then
    Exit;
  SetPropertyValue<Boolean>(SetWidowOrphanControlCore, Value);
end;

function TdxParagraphProperties.SetWidowOrphanControlCore(const AInfo: TdxParagraphFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.WidowOrphanControl := Value;
  Result := TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(TdxParagraphFormattingChangeType.WidowOrphanControl);
end;

function TdxParagraphProperties.UseVal(AMask: TdxUsedParagraphFormattingOption): Boolean;
begin
  Result := AMask in Info.Options.Value;
end;

{ TdxParagraphFormattingInfoCache }

function TdxParagraphFormattingInfoCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxParagraphFormattingInfo;
begin
  Result := TdxParagraphFormattingInfo.Create;
  Result.WidowOrphanControl := True;
end;

{ TdxParagraphFormattingOptionsCache }

function TdxParagraphFormattingOptionsCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxParagraphFormattingOptions;
begin
  Result := TdxParagraphFormattingOptions.Create;
end;

procedure TdxParagraphFormattingOptionsCache.InitItems(
  const AUnitConverter: IdxDocumentModelUnitConverter);
begin
  inherited InitItems(AUnitConverter);
  AddRootStyleOptions;
end;

procedure TdxParagraphFormattingOptionsCache.AddRootStyleOptions;
begin
  AppendItem(TdxParagraphFormattingOptions.Create(dxParagraphFormattingOptionsUseAll));
end;

{ TdxParagraphFormattingCache }

constructor TdxParagraphFormattingCache.Create(ADocumentModel: TObject);
begin
  inherited Create((ADocumentModel as TdxDocumentModel).UnitConverter);
  FDocumentModel := ADocumentModel;
  AppendItem(TdxParagraphFormattingBase.Create(TdxDocumentModel(DocumentModel).MainPieceTable,
    TdxDocumentModel(DocumentModel), 0,
    TdxParagraphFormattingOptionsCache.EmptyParagraphFormattingOptionIndex));
  AppendItem(TdxParagraphFormattingBase.Create(TdxDocumentModel(DocumentModel).MainPieceTable,
    TdxDocumentModel(DocumentModel), 0,
    TdxParagraphFormattingOptionsCache.RootParagraphFormattingOptionIndex));
end;

function TdxParagraphFormattingCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxParagraphFormattingBase;
begin
  Result := nil;
end;

{ TdxParagraphPropertiesMerger }

constructor TdxParagraphPropertiesMerger.Create(const AProperties: TdxParagraphProperties);
var
  AInfo: TdxParagraphFormattingBase;
begin
  AInfo := AProperties.Info;
  inherited Create(TdxMergedParagraphProperties.Create(AInfo.Info, AInfo.Options));
end;

procedure TdxParagraphPropertiesMerger.Merge(const AProperties: TdxParagraphProperties);
var
  AInfo: TdxParagraphFormattingBase;
begin
  AInfo := AProperties.Info;
  MergeCore(AInfo.Info, AInfo.Options);
end;

procedure TdxParagraphPropertiesMerger.MergeCore(const AInfo: TdxParagraphFormattingInfo;
  const AOptions: TdxParagraphFormattingOptions);
var
  AOwnInfo: TdxParagraphFormattingInfo;
  AOwnOptions: TdxParagraphFormattingOptions;
begin
  AOwnOptions := OwnOptions;
  AOwnInfo := OwnInfo;
  if not AOwnOptions.UseAlignment and AOptions.UseAlignment then
  begin
    AOwnInfo.Alignment := AInfo.Alignment;
    AOwnOptions.UseAlignment := True;
  end;
  if not AOwnOptions.UseFirstLineIndent and AOptions.UseFirstLineIndent then
  begin
    AOwnInfo.FirstLineIndentType := AInfo.FirstLineIndentType;
    AOwnInfo.FirstLineIndent := AInfo.FirstLineIndent;
    AOwnOptions.UseFirstLineIndent := True;
  end;
  if not AOwnOptions.UseLeftIndent and AOptions.UseLeftIndent then
  begin
    AOwnInfo.LeftIndent := AInfo.LeftIndent;
    AOwnOptions.UseLeftIndent := True;
  end;
  if not AOwnOptions.UseLineSpacing and AOptions.UseLineSpacing then
  begin
    AOwnInfo.LineSpacing := AInfo.LineSpacing;
    AOwnInfo.LineSpacingType := AInfo.LineSpacingType;
    AOwnOptions.UseLineSpacing := True;
  end;
  if not AOwnOptions.UseRightIndent and AOptions.UseRightIndent then
  begin
    AOwnInfo.RightIndent := AInfo.RightIndent;
    AOwnOptions.UseRightIndent := True;
  end;
  if not AOwnOptions.UseSpacingAfter and AOptions.UseSpacingAfter then
  begin
    AOwnInfo.SpacingAfter := AInfo.SpacingAfter;
    AOwnOptions.UseSpacingAfter := True;
  end;
  if not AOwnOptions.UseSpacingBefore and AOptions.UseSpacingBefore then
  begin
    AOwnInfo.SpacingBefore := AInfo.SpacingBefore;
    AOwnOptions.UseSpacingBefore := True;
  end;
  if not AOwnOptions.UseSuppressHyphenation and AOptions.UseSuppressHyphenation then
  begin
    AOwnInfo.SuppressHyphenation := AInfo.SuppressHyphenation;
    AOwnOptions.UseSuppressHyphenation := True;
  end;
  if not AOwnOptions.UseSuppressLineNumbers and AOptions.UseSuppressLineNumbers then
  begin
    AOwnInfo.SuppressLineNumbers := AInfo.SuppressLineNumbers;
    AOwnOptions.UseSuppressLineNumbers := True;
  end;
  if not AOwnOptions.UseContextualSpacing and AOptions.UseContextualSpacing then
  begin
    AOwnInfo.ContextualSpacing := AInfo.ContextualSpacing;
    AOwnOptions.UseContextualSpacing := True;
  end;
  if not AOwnOptions.UsePageBreakBefore and AOptions.UsePageBreakBefore then
  begin
    AOwnInfo.PageBreakBefore := AInfo.PageBreakBefore;
    AOwnOptions.UsePageBreakBefore := True;
  end;
  if not AOwnOptions.UseBeforeAutoSpacing and AOptions.UseBeforeAutoSpacing then
  begin
    AOwnInfo.BeforeAutoSpacing := AInfo.BeforeAutoSpacing;
    AOwnOptions.UseBeforeAutoSpacing := True;
  end;
  if not AOwnOptions.UseAfterAutoSpacing and AOptions.UseAfterAutoSpacing then
  begin
    AOwnInfo.AfterAutoSpacing := AInfo.AfterAutoSpacing;
    AOwnOptions.UseAfterAutoSpacing := True;
  end;
  if not AOwnOptions.UseKeepWithNext and AOptions.UseKeepWithNext then
  begin
    AOwnInfo.KeepWithNext := AInfo.KeepWithNext;
    AOwnOptions.UseKeepWithNext := True;
  end;
  if not AOwnOptions.UseKeepLinesTogether and AOptions.UseKeepLinesTogether then
  begin
    AOwnInfo.KeepLinesTogether := AInfo.KeepLinesTogether;
    AOwnOptions.UseKeepLinesTogether := True;
  end;
  if not AOwnOptions.UseWidowOrphanControl and AOptions.UseWidowOrphanControl then
  begin
    AOwnInfo.WidowOrphanControl := AInfo.WidowOrphanControl;
    AOwnOptions.UseWidowOrphanControl := True;
  end;
  if not AOwnOptions.UseOutlineLevel and AOptions.UseOutlineLevel then
  begin
    AOwnInfo.OutlineLevel := AInfo.OutlineLevel;
    AOwnOptions.UseOutlineLevel := True;
  end;
  if not AOwnOptions.UseBackColor and AOptions.UseBackColor then
  begin
    AOwnInfo.BackColor := AInfo.BackColor;
    AOwnOptions.UseBackColor := True;
  end;
end;

{ TdxCharacterFormattingChangeActionsCalculator }

class function TdxParagraphFormattingChangeActionsCalculator.CalculateChangeActions(
  const AChange: TdxParagraphFormattingChangeType): TdxDocumentModelChangeActions;
const
  AParagraphFormattingChangeActionsTable: array[TdxParagraphFormattingChangeType] of TdxDocumentModelChangeActions =
    (
    [],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ForceResetHorizontalRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ForceResetHorizontalRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ForceResetHorizontalRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ForceResetHorizontalRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetAllPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ResetRuler],
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
     TdxDocumentModelChangeAction.ResetSpellingCheck, TdxDocumentModelChangeAction.ForceResetHorizontalRuler]
    );
begin
  Result := AParagraphFormattingChangeActionsTable[AChange];
end;


{ TdxParagraphParagraphPropertiesChangedHistoryItem }

constructor TdxParagraphParagraphPropertiesChangedHistoryItem.Create(
  const APieceTable: TdxDocumentModelPart; AParagraphIndex: Integer);
begin
  inherited Create(APieceTable);
  Assert(AParagraphIndex >= 0);
  FParagraphIndex := AParagraphIndex;
end;

function TdxParagraphParagraphPropertiesChangedHistoryItem.GetObject: IdxIndexBasedObject<TdxDocumentModelChangeActions>;
begin
  Result := TdxPieceTable(DocumentModelPart).Paragraphs[ParagraphIndex].ParagraphProperties;
end;

{ TdxMergedParagraphProperties }

constructor TdxMergedParagraphProperties.Create(
  AInfo: TdxParagraphFormattingInfo; AOptions: TdxParagraphFormattingOptions);
begin
  inherited;
end;

destructor TdxMergedParagraphProperties.Destroy;
begin
  inherited;
end;

{ TdxParagraphMergedParagraphPropertiesCachedResult }

constructor TdxParagraphMergedParagraphPropertiesCachedResult.Create;
begin
  inherited Create;
  FTableStyleParagraphPropertiesIndex := -1;
  FOwnListLevelParagraphPropertiesIndex := -1;
  FParagraphPropertiesIndex := -1;
  FParagraphStyleIndex := -1;
end;

destructor TdxParagraphMergedParagraphPropertiesCachedResult.Destroy;
begin
  FreeAndNil(FMergedParagraphProperties);
  inherited Destroy;
end;

procedure TdxParagraphMergedParagraphPropertiesCachedResult.SetMergedParagraphProperties(Value: TdxMergedParagraphProperties);
begin
  if MergedParagraphProperties <> Value then
  begin
    FMergedParagraphProperties.Free;
    FMergedParagraphProperties := Value;
  end;
end;

end.
