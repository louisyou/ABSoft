{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Actions.Res;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Classes, {$IFDEF DELPHI17} Actions, {$ENDIF}ActnList, ImgList, Controls, cxGraphics, dxRichEdit.Actions;

type
  TdxRichEditControlActions = class(TDataModule)
    cxImageList1: TcxImageList;
    ActionList1: TActionList;
  {$IFDEF MSWINDOWS}
    acAlignmentCenter: TdxRichEditControlToggleParagraphAlignmentCenter;
    acAlignmentJustify: TdxRichEditControlToggleParagraphAlignmentJustify;
    acAlignmentLeft: TdxRichEditControlToggleParagraphAlignmentLeft;
    acAlignmentRight: TdxRichEditControlToggleParagraphAlignmentRight;
    acBold: TdxRichEditControlToggleFontBold;
    acCopySelection: TdxRichEditControlCopySelection;
    acCutSelection: TdxRichEditControlCutSelection;
    acDecreaseFontSize: TdxRichEditControlDecreaseFontSize;
    acDoubleParagraphSpacing: TdxRichEditControlSetDoubleParagraphSpacing;
    acIncreaseFontSize: TdxRichEditControlIncreaseFontSize;
    acItalic: TdxRichEditControlToggleFontItalic;
    acPasteSelection: TdxRichEditControlPasteSelection;
    acRedo: TdxRichEditControlRedo;
    acSesquialteralParagraphSpacing: TdxRichEditControlSetSesquialteralParagraphSpacing;
    acSingleParagraphSpacing: TdxRichEditControlSetSingleParagraphSpacing;
    acToggleFontSubscript: TdxRichEditControlToggleFontSubscript;
    acToggleFontSuperscript: TdxRichEditControlToggleFontSuperscript;
    acUnderline: TdxRichEditControlToggleFontUnderline;
    acUndo: TdxRichEditControlUndo;
    acToggleSimpleNumberingList: TdxRichEditControlToggleSimpleNumberingList;
    acToggleBulletedList: TdxRichEditControlToggleBulletedList;
    acToggleMultiLevelList: TdxRichEditControlToggleMultiLevelList;
    acSelectAll: TdxRichEditControlSelectAll;
    acToggleFontDoubleUnderline: TdxRichEditControlToggleFontDoubleUnderline;
    acToggleFontStrikeout: TdxRichEditControlToggleFontStrikeout;
    acToggleFontDoubleStrikeout: TdxRichEditControlToggleFontDoubleStrikeout;
    acToggleShowWhitespace: TdxRichEditControlToggleShowWhitespace;
    acIncrementIndent: TdxRichEditControlIncrementIndent;
    acDecrementIndent: TdxRichEditControlDecrementIndent;
    acShowParagraphForm: TdxRichEditControlShowParagraphForm;
    acChangeFontName: TdxRichEditControlChangeFontName;
    acChangeFontSize: TdxRichEditControlChangeFontSize;
  {$ENDIF}
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dxRichEditControlActions: TdxRichEditControlActions;

implementation

{$R *.dfm}

end.
