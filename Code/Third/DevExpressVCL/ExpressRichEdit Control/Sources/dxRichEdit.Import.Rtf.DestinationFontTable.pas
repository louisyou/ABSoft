{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationFontTable;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.Import.Rtf;

type
  TdxFontTableDestination = class(TdxRichEditRtfDestinationBase)
  private
    FFontInfo: TdxRtfFontInfo;
    procedure AddFontInfo;
  protected
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    function GetKeywordHT: TdxKeywordTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
    function ProcessKeywordCore(const AKeyword: string; AParameterValue: Integer; AHasParameter: Boolean): Boolean; override;

    procedure ProcessCharCore(AChar: Char); override;
    procedure FontCharsetHandler(AParameterValue: Integer);
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
    destructor Destroy; override;

    procedure AfterPopRtfState; override;
  end;

implementation

uses
  dxRichEdit.Platform.Font;

constructor TdxFontTableDestination.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FFontInfo := TdxRtfFontInfo.Create;
end;

destructor TdxFontTableDestination.Destroy;
begin
  FreeAndNil(FFontInfo);
  inherited;
end;

function TdxFontTableDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

function TdxFontTableDestination.GetKeywordHT: TdxKeywordTranslatorTable;
begin
  Result := nil;
end;

procedure TdxFontTableDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
//do nothing
end;

function TdxFontTableDestination.ProcessKeywordCore(const AKeyword: string;
  AParameterValue: Integer; AHasParameter: Boolean): Boolean;
begin
  if not AHasParameter then
    AParameterValue := 0;
  Result := True;
  if AKeyword = 'f' then
    FFontInfo.ID := AParameterValue
  else
    if AKeyword = 'fcharset' then
      FontCharsetHandler(AParameterValue)
    else
      if AKeyword = 'bin' then
        Result := inherited ProcessKeywordCore(AKeyword, AParameterValue, AHasParameter)
      else
        Result := False;
end;

procedure TdxFontTableDestination.AddFontInfo;
begin
  if FFontInfo.Name = '' then
    FFontInfo.Name := Importer.DocumentModel.Cache.CharacterFormattingInfoCache.DefaultItem.FontName;
  Importer.DocumentProperties.Fonts.Add(FFontInfo);
  FFontInfo := TdxRtfFontInfo.Create;
end;

procedure TdxFontTableDestination.ProcessCharCore(AChar: Char);
begin
  if AChar = ';' then
  begin
    AddFontInfo;
    Importer.SetCodePage(Importer.DocumentProperties.DefaultCodePage);
  end
  else
    FFontInfo.Name := FFontInfo.Name + AChar;
end;

procedure TdxFontTableDestination.FontCharsetHandler(AParameterValue: Integer);
begin
  FFontInfo.Charset := AParameterValue;
  if FFontInfo.Charset >= 0 then
    Importer.SetCodePage(CodePageFromCharset(FFontInfo.Charset));
end;

procedure TdxFontTableDestination.AfterPopRtfState;
var
  AFontInfo: TdxRtfFontInfo;
  AProperties: TdxRtfDocumentProperties;
begin
  AProperties := Importer.DocumentProperties;
  AFontInfo := AProperties.Fonts.GetRtfFontInfoById(AProperties.DefaultFontNumber);
  Importer.Position.CharacterFormatting.FontName := AFontInfo.Name;
  if (FFontInfo <> AProperties.Fonts.DefaultRtfFontInfo) and (FFontInfo.Charset >= 0) then
    Importer.DocumentProperties.DefaultCodePage := CodePageFromCharset(AFontInfo.Charset);
  Importer.SetCodePage(Importer.DocumentProperties.DefaultCodePage);
end;

end.
