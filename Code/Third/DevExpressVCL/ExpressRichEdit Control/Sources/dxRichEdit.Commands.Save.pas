{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Commands.Save;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Graphics, Windows, Controls, Generics.Collections, dxRichEdit.Commands.Insert,
  dxCoreClasses, dxRichEdit.DocumentModel.Core, dxRichEdit.Utils.DataObject, dxRichEdit.DocumentModel.Commands,
  dxRichEdit.View.ViewInfo, dxRichEdit.Control, dxRichEdit.View.Core, dxRichEdit.DocumentModel.PieceTableIterators,
  dxRichEdit.Control.HitTest, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Commands, dxRichEdit.Commands.IDs,
  dxRichEdit.Utils.OfficeImage, dxRichEdit.Options, dxRichEdit.Commands.MultiCommand;

type
  { TdxSaveDocumentCommand }

  TdxSaveDocumentCommand = class(TdxRichEditMenuItemSimpleCommand)
  protected
    procedure ExecuteCore; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxSaveDocumentAsCommand }

  TdxSaveDocumentAsCommand = class(TdxRichEditMenuItemSimpleCommand)
  protected
    procedure ExecuteCore; override;
    function ShouldBeExecutedOnKeyUpInSilverlightEnvironment: Boolean; override;
    function ShowsModalDialog: Boolean; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

implementation

{ TdxSaveDocumentCommand }

procedure TdxSaveDocumentCommand.ExecuteCore;
begin
  CheckExecutedAtUIThread;
  if InnerControl.SaveDocument then
    InnerControl.Modified := False;
end;

class function TdxSaveDocumentCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.FileSave;
end;

procedure TdxSaveDocumentCommand.UpdateUIStateCore(
  const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  ApplyCommandsRestriction(AState, Options.Behavior.Save, InnerControl.Modified);
end;

{ TdxSaveDocumentAsCommand }

procedure TdxSaveDocumentAsCommand.ExecuteCore;
begin
  CheckExecutedAtUIThread;
  if InnerControl.SaveDocumentAs then
    InnerControl.Modified := False;
end;

class function TdxSaveDocumentAsCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.FileSaveAs;
end;

function TdxSaveDocumentAsCommand.ShouldBeExecutedOnKeyUpInSilverlightEnvironment: Boolean;
begin
  Result := True;
end;

function TdxSaveDocumentAsCommand.ShowsModalDialog: Boolean;
begin
  Result := True;
end;

procedure TdxSaveDocumentAsCommand.UpdateUIStateCore(
  const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  ApplyCommandsRestriction(AState, Options.Behavior.SaveAs);
end;

end.
