{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Actions.Reg;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Classes, ActnList,dxRichEdit.Actions, dxRichEdit.Actions.Res,
  {$IFDEF DELPHI17}Actions, ActionEditors, {$ENDIF} ImgList;

{$IFDEF DELPHI17}
type

  { TdxRichEditControlIDEActions }

  TdxRichEditControlIDEActions = class(TIDEActions)
  public
    class var OldIDEActionsClass: TIDEActionsClass;
    class procedure AssignAction(Source, Destination: TBasicAction); override;
    class function CreateImageList(ActionList: TContainedActionList): TCustomImageList; override;
    class function DefaultActionClass: TContainedActionClass; override;
    class function BaseActionClass: TContainedActionClass; override;
    class function BaseActionListClass: TContainedActionListClass; override;
    class procedure CopyImageIfAvailable(const NewAction: TContainedAction; const ActionList: TContainedActionList); override;
    class procedure RegisterImageLink(const ActionList: TContainedActionList; const ImageLink: TObject); override;
    class procedure UnregisterImageLink(const ActionList: TContainedActionList; const ImageLink: TObject); override;
  end;
{$ENDIF}

procedure Register;

const
  sdxRichEditControlCategory = 'DevExpress RichEdit Control';

implementation

uses
  cxGraphics, Graphics;

procedure Register;
begin
  RegisterActions(sdxRichEditControlCategory,
    [
      TdxRichEditControlToggleFontBold,
      TdxRichEditControlToggleFontItalic,
      TdxRichEditControlToggleFontUnderline,
      TdxRichEditControlToggleFontDoubleUnderline,
      TdxRichEditControlToggleFontStrikeout,
      TdxRichEditControlToggleFontDoubleStrikeout,
      TdxRichEditControlToggleParagraphAlignmentLeft,
      TdxRichEditControlToggleParagraphAlignmentCenter,
      TdxRichEditControlToggleParagraphAlignmentRight,
      TdxRichEditControlToggleParagraphAlignmentJustify,
      TdxRichEditControlToggleFontSuperscript,
      TdxRichEditControlToggleFontSubscript,
      TdxRichEditControlToggleBulletedList,
      TdxRichEditControlToggleSimpleNumberingList,
      TdxRichEditControlToggleMultiLevelList,
      TdxRichEditControlToggleShowWhitespace,
      TdxRichEditControlCopySelection,
      TdxRichEditControlPasteSelection,
      TdxRichEditControlCutSelection,
      TdxRichEditControlSetSingleParagraphSpacing,
      TdxRichEditControlSetDoubleParagraphSpacing,
      TdxRichEditControlSetSesquialteralParagraphSpacing,
      TdxRichEditControlUndo,
      TdxRichEditControlRedo,
      TdxRichEditControlIncreaseFontSize,
      TdxRichEditControlIncrementIndent,
      TdxRichEditControlDecreaseFontSize,
      TdxRichEditControlDecrementIndent,
      TdxRichEditControlSelectAll,
      TdxRichEditControlShowParagraphForm,
      TdxRichEditControlChangeFontName,
      TdxRichEditControlChangeFontSize
    ],
    TdxRichEditControlActions);
end;

{$IFDEF DELPHI17}
{ TdxRichEditControlIDEActions }

type
  TdxRichEditControlActionAccess = class(TdxRichEditControlAction);

class procedure TdxRichEditControlIDEActions.AssignAction(Source, Destination: TBasicAction);
var
  ABitmap: TcxBitmap;
  AImages: TcxImageList;
  ASource: TdxRichEditControlAction;
  ADestination: TdxRichEditControlActionAccess;
begin
  OldIDEActionsClass.AssignAction(Source, Destination); 
  if (Source is TdxRichEditControlAction) and (Destination is TdxRichEditControlAction) then
  begin
    ASource := TdxRichEditControlAction(Source);
    ADestination := TdxRichEditControlActionAccess(Destination);
  end
  else
    Exit;
  if not (ADestination.FImage is TBitmap) then
    Exit;
  if (ASource.ActionList <> nil) and (ASource.ActionList.Images is TcxImageList) then
    AImages := TcxImageList(ASource.ActionList.Images)
  else
    Exit;
  FreeAndNil(ADestination.FImage);
  ABitmap := TcxBitmap.Create;
  ABitmap.PixelFormat := pf32bit;
  ADestination.FImage := ABitmap;
  AImages.GetImage(ASource.ImageIndex, ABitmap);
end;

class function TdxRichEditControlIDEActions.BaseActionClass: TContainedActionClass;
begin
  Result := OldIDEActionsClass.BaseActionClass; 
end;

class function TdxRichEditControlIDEActions.BaseActionListClass: TContainedActionListClass;
begin
  Result := OldIDEActionsClass.BaseActionListClass; 
end;

class procedure TdxRichEditControlIDEActions.CopyImageIfAvailable(const NewAction: TContainedAction;
  const ActionList: TContainedActionList);
begin
  OldIDEActionsClass.CopyImageIfAvailable(NewAction, ActionList); 
end;

class function TdxRichEditControlIDEActions.CreateImageList(ActionList: TContainedActionList): TCustomImageList;
begin
  Result := OldIDEActionsClass.CreateImageList(ActionList); 
end;

class function TdxRichEditControlIDEActions.DefaultActionClass: TContainedActionClass;
begin
  Result := OldIDEActionsClass.DefaultActionClass; 
end;

class procedure TdxRichEditControlIDEActions.RegisterImageLink(const ActionList: TContainedActionList;
  const ImageLink: TObject);
begin
  OldIDEActionsClass.RegisterImageLink(ActionList, ImageLink);
end;

class procedure TdxRichEditControlIDEActions.UnregisterImageLink(const ActionList: TContainedActionList;
  const ImageLink: TObject);
begin
  OldIDEActionsClass.UnregisterImageLink(ActionList, ImageLink);
end;

initialization
  if TdxRichEditControlIDEActions.OldIDEActionsClass = nil then
  begin
    TdxRichEditControlIDEActions.OldIDEActionsClass := GetIDEActions('VCL');
    if (TdxRichEditControlIDEActions.OldIDEActionsClass <> nil) and
      (TdxRichEditControlIDEActions.OldIDEActionsClass <> TdxRichEditControlIDEActions) then
    begin
      UnregisterActionsInFramework('VCL');
      RegisterActionsInFramework('VCL', TdxRichEditControlIDEActions);
    end
    else
      TdxRichEditControlIDEActions.OldIDEActionsClass := nil;
  end;

finalization
  if TdxRichEditControlIDEActions.OldIDEActionsClass <> nil then
  begin
    UnregisterActionsInFramework('VCL');
    RegisterActionsInFramework('VCL', TdxRichEditControlIDEActions.OldIDEActionsClass);
    TdxRichEditControlIDEActions.OldIDEActionsClass := nil;
  end;
{$ENDIF}

end.
