{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Dialogs.Numbering;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, dxRichEdit.Dialog.CustomDialog, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxLayoutcxEditAdapters, dxLayoutControlAdapters, Menus, StdCtrls,
  cxButtons, dxLayoutContainer, cxGroupBox, cxRadioGroup, dxLayoutControl, cxListBox, cxClasses;

type
  TNumberingListDialogForm = class(TdxRichEditCustomDialogForm)
    lcMainGroup_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutControl1Group1: TdxLayoutGroup;
    dxLayoutControl1Group2: TdxLayoutGroup;
    btnCustomize: TcxButton;
    dxLayoutControl1Item2: TdxLayoutItem;
    btnOk: TcxButton;
    dxLayoutControl1Item3: TdxLayoutItem;
    btnCancel: TcxButton;
    dxLayoutControl1Item4: TdxLayoutItem;
    lcgOutlineNumbered: TdxLayoutGroup;
    lcgBulleted: TdxLayoutGroup;
    lcgNumbered: TdxLayoutGroup;
    lbBulleted: TcxListBox;
    dxLayoutControl1Item5: TdxLayoutItem;
    lbNumbered: TcxListBox;
    dxLayoutControl1Item6: TdxLayoutItem;
    rbRestartNumbering: TcxRadioButton;
    rbContinuePreviousList: TcxRadioButton;
    dxLayoutControl1Item7: TdxLayoutItem;
    dxLayoutControl1Group4: TdxLayoutGroup;
    dxLayoutControl1Item8: TdxLayoutItem;
    dxLayoutControl1Group3: TdxLayoutGroup;
  protected
    procedure ApplyLocalization; override;
  public
  end;

var
  NumberingListDialogForm: TNumberingListDialogForm;

implementation

uses
  dxCore, dxRichEdit.DialogStrs;

{$R *.dfm}

{ TNumberingListDialogForm }

procedure TNumberingListDialogForm.ApplyLocalization;
begin
  Caption := cxGetResourceString(@sdxNumberingListDialogForm);
  btnCustomize.Caption := cxGetResourceString(@sdxNumberingListDialogButtonCustomize);
  btnOk.Caption := cxGetResourceString(@sdxNumberingListDialogButtonOk);
  btnCancel.Caption := cxGetResourceString(@sdxNumberingListDialogButtonCancel);
  lcgBulleted.CaptionOptions.Text := cxGetResourceString(@sdxNumberingListDialogBulleted);
  lcgNumbered.CaptionOptions.Text := cxGetResourceString(@sdxNumberingListDialogNumbered);
  lcgOutlineNumbered.CaptionOptions.Text := cxGetResourceString(@sdxNumberingListDialogOutlineNumbered);
end;

end.
