{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.UnicodeRangeInfo;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Classes;

type
  TdxUnicodeSubrange = record
    LowValue: WideChar;
    HiValue: WideChar;
    Bit: Integer;
    function ContainsChar(ACharacter: WideChar): Boolean;
  end;
  PdxUnicodeSubrange = ^TdxUnicodeSubrange;

  TdxUnicodeSubrangeArray = array of TdxUnicodeSubrange;

  TdxUnicodeRangeInfo = class
  private
    FCapacity: Integer;
    FCount: Integer;
    FRanges: TdxUnicodeSubrangeArray;
    procedure SetCapacity(AValue: Integer);
  protected
    procedure AddSubrange(AStartCharacter, AEndCharacter: WideChar; ABit: Integer);
    procedure Grow;
    procedure PopulateSubranges;
  public
    constructor Create;
    destructor Destroy; override;
    function LookupSubrange(ACharacter: WideChar): PdxUnicodeSubrange;

    property Capacity: Integer read FCapacity write SetCapacity;
    property Count: Integer read FCount;
  end;

function UnicodeRangeInfo: TdxUnicodeRangeInfo;

implementation

uses
  Math, SysUtils;

var
  FUnicodeRangeInfo: TdxUnicodeRangeInfo;

function UnicodeRangeInfo: TdxUnicodeRangeInfo;
begin
  if FUnicodeRangeInfo = nil then
    FUnicodeRangeInfo := TdxUnicodeRangeInfo.Create;
  Result := FUnicodeRangeInfo;
end;

function TdxUnicodeSubrange.ContainsChar(ACharacter: WideChar): Boolean;
begin
  Result := ((ACharacter >= LowValue) and (ACharacter <= HiValue));
end;

{ UnicodeRangeInfo }

constructor TdxUnicodeRangeInfo.Create;
begin
  inherited Create;
  PopulateSubranges;
end;

destructor TdxUnicodeRangeInfo.Destroy;
begin
  FRanges := nil;
  inherited Destroy;
end;

procedure TdxUnicodeRangeInfo.PopulateSubranges;
begin
  Capacity := 135;
  AddSubrange(#$0000, #$007F, 0); // Basic Latin
  AddSubrange(#$0080, #$00FF, 1); // Latin-1 Supplement
  AddSubrange(#$0100, #$017F, 2); // Latin Extended-A
  AddSubrange(#$0180, #$024F, 3); // Latin Extended-B
  AddSubrange(#$0250, #$02AF, 4); // IPA Extensions
  AddSubrange(#$02B0, #$02FF, 5); // Spacing Modifier Letters
  AddSubrange(#$0300, #$036F, 6); // Combining Diacritical Marks
  AddSubrange(#$0370, #$03FF, 7); // Greek and Coptic
  AddSubrange(#$0400, #$04FF, 9); // Cyrillic
  AddSubrange(#$0500, #$052F, 9); // Cyrillic Supplement
  AddSubrange(#$0530, #$058F, 10); // Armenian
  AddSubrange(#$0590, #$05FF, 11); // Hebrew
  AddSubrange(#$0600, #$06FF, 13); // Arabic
  AddSubrange(#$0700, #$074F, 71); // Syriac
  AddSubrange(#$0750, #$077F, 13); // Arabic Supplement
  AddSubrange(#$0780, #$07BF, 72); // Thaana
  AddSubrange(#$07C0, #$07FF, 14); // NKo
  AddSubrange(#$0900, #$097F, 15); // Devanagari
  AddSubrange(#$0980, #$09FF, 16); // Bengali
  AddSubrange(#$0A00, #$0A7F, 17); // Gurmukhi
  AddSubrange(#$0A80, #$0AFF, 18); // Gujarati
  AddSubrange(#$0B00, #$0B7F, 19); // Oriya
  AddSubrange(#$0B80, #$0BFF, 20); // Tamil
  AddSubrange(#$0C00, #$0C7F, 21); // Telugu
  AddSubrange(#$0C80, #$0CFF, 22); // Kannada
  AddSubrange(#$0D00, #$0D7F, 23); // Malayalam
  AddSubrange(#$0D80, #$0DFF, 73); // Sinhala
  AddSubrange(#$0E00, #$0E7F, 24); // Thai
  AddSubrange(#$0E80, #$0EFF, 25); // Lao
  AddSubrange(#$0F00, #$0FFF, 70); // Tibetan
  AddSubrange(#$1000, #$109F, 74); // Myanmar
  AddSubrange(#$10A0, #$10FF, 26); // Georgian
  AddSubrange(#$1100, #$11FF, 28); // Hangul Jamo
  AddSubrange(#$1200, #$137F, 75); // Ethiopic
  AddSubrange(#$1380, #$139F, 75); // Ethiopic Supplement
  AddSubrange(#$13A0, #$13FF, 76); // Cherokee
  AddSubrange(#$1400, #$167F, 77); // Unified Canadian Aboriginal Syllabics
  AddSubrange(#$1680, #$169F, 78); // Ogham
  AddSubrange(#$16A0, #$16FF, 79); // Runic
  AddSubrange(#$1700, #$171F, 84); // Tagalog
  AddSubrange(#$1720, #$173F, 84); // Hanunoo
  AddSubrange(#$1740, #$175F, 84); // Buhid
  AddSubrange(#$1760, #$177F, 84); // Tagbanwa
  AddSubrange(#$1780, #$17FF, 80); // Khmer
  AddSubrange(#$1800, #$18AF, 81); // Mongolian
  AddSubrange(#$1900, #$194F, 93); // Limbu
  AddSubrange(#$1950, #$197F, 94); // Tai Le
  AddSubrange(#$1980, #$19DF, 95); // New Tai Lue
  AddSubrange(#$19E0, #$19FF, 80); // Khmer Symbols
  AddSubrange(#$1A00, #$1A1F, 96); // Buginese
  AddSubrange(#$1B00, #$1B7F, 27); // Balinese
  AddSubrange(#$1B80, #$1BBF, 112); // Sundanese
  AddSubrange(#$1C00, #$1C4F, 113); // Lepcha
  AddSubrange(#$1C50, #$1C7F, 114); // Ol Chiki
  AddSubrange(#$1D00, #$1D7F, 4); // Phonetic Extensions
  AddSubrange(#$1D80, #$1DBF, 4); // Phonetic Extensions Supplement
  AddSubrange(#$1DC0, #$1DFF, 6); // Combining Diacritical Marks Supplement
  AddSubrange(#$1E00, #$1EFF, 29); // Latin Extended Additional
  AddSubrange(#$1F00, #$1FFF, 30); // Greek Extended
  AddSubrange(#$2000, #$206F, 31); // General Punctuation
  AddSubrange(#$2070, #$209F, 32); // Superscripts And Subscripts
  AddSubrange(#$20A0, #$20CF, 33); // Currency Symbols
  AddSubrange(#$20D0, #$20FF, 34); // Combining Diacritical Marks For Symbols
  AddSubrange(#$2100, #$214F, 35); // Letterlike Symbols
  AddSubrange(#$2150, #$218F, 36); // Number Forms
  AddSubrange(#$2190, #$21FF, 37); // Arrows
  AddSubrange(#$2200, #$22FF, 38); // Mathematical Operators
  AddSubrange(#$2300, #$23FF, 39); // Miscellaneous Technical
  AddSubrange(#$2400, #$243F, 40); // Control Pictures
  AddSubrange(#$2440, #$245F, 41); // Optical Character Recognition
  AddSubrange(#$2460, #$24FF, 42); // Enclosed Alphanumerics
  AddSubrange(#$2500, #$257F, 43); // Box Drawing
  AddSubrange(#$2580, #$259F, 44); // Block Elements
  AddSubrange(#$25A0, #$25FF, 45); // Geometric Shapes
  AddSubrange(#$2600, #$26FF, 46); // Miscellaneous Symbols
  AddSubrange(#$2700, #$27BF, 47); // Dingbats
  AddSubrange(#$27C0, #$27EF, 38); // Miscellaneous Mathematical Symbols-A
  AddSubrange(#$27F0, #$27FF, 37); // Supplemental Arrows-A
  AddSubrange(#$2800, #$28FF, 82); // Braille Patterns
  AddSubrange(#$2900, #$297F, 37); // Supplemental Arrows-B
  AddSubrange(#$2980, #$29FF, 38); // Miscellaneous Mathematical Symbols-B
  AddSubrange(#$2A00, #$2AFF, 38); // Supplemental Mathematical Operators
  AddSubrange(#$2B00, #$2BFF, 37); // Miscellaneous Symbols and Arrows
  AddSubrange(#$2C00, #$2C5F, 97); // Glagolitic
  AddSubrange(#$2C60, #$2C7F, 29); // Latin Extended-C
  AddSubrange(#$2C80, #$2CFF, 8); // Coptic
  AddSubrange(#$2D00, #$2D2F, 26); // Georgian Supplement
  AddSubrange(#$2D30, #$2D7F, 98); // Tifinagh
  AddSubrange(#$2D80, #$2DDF, 75); // Ethiopic Extended
  AddSubrange(#$2DE0, #$2DFF, 9); // Cyrillic Extended-A
  AddSubrange(#$2E00, #$2E7F, 31); // Supplemental Punctuation
  AddSubrange(#$2E80, #$2EFF, 59); // CJK Radicals Supplement
  AddSubrange(#$2F00, #$2FDF, 59); // Kangxi Radicals
  AddSubrange(#$2FF0, #$2FFF, 59); // Ideographic Description Characters
  AddSubrange(#$3000, #$303F, 48); // CJK Symbols And Punctuation
  AddSubrange(#$3040, #$309F, 49); // Hiragana
  AddSubrange(#$30A0, #$30FF, 50); // Katakana
  AddSubrange(#$3100, #$312F, 51); // Bopomofo
  AddSubrange(#$3130, #$318F, 52); // Hangul Compatibility Jamo
  AddSubrange(#$3190, #$319F, 59); // Kanbun
  AddSubrange(#$31A0, #$31BF, 51); // Bopomofo Extended
  AddSubrange(#$31C0, #$31EF, 61); // CJK Strokes
  AddSubrange(#$31F0, #$31FF, 50); // Katakana Phonetic Extensions
  AddSubrange(#$3200, #$32FF, 54); // Enclosed CJK Letters And Months
  AddSubrange(#$3300, #$33FF, 55); // CJK Compatibility
  AddSubrange(#$3400, #$4DBF, 59); // CJK Unified Ideographs Extension A
  AddSubrange(#$4DC0, #$4DFF, 99); // Yijing Hexagram Symbols
  AddSubrange(#$4E00, #$9FFF, 59); // CJK Unified Ideographs
  AddSubrange(#$A000, #$A48F, 83); // Yi Syllables
  AddSubrange(#$A490, #$A4CF, 83); // Yi Radicals
  AddSubrange(#$A500, #$A63F, 12); // Vai
  AddSubrange(#$A640, #$A69F, 9); // Cyrillic Extended-B
  AddSubrange(#$A700, #$A71F, 5); // Modifier Tone Letters
  AddSubrange(#$A720, #$A7FF, 29); // Latin Extended-D
  AddSubrange(#$A800, #$A82F, 100); // Syloti Nagri
  AddSubrange(#$A840, #$A87F, 53); // Phags-pa
  AddSubrange(#$A880, #$A8DF, 115); // Saurashtra
  AddSubrange(#$A900, #$A92F, 116); // Kayah Li
  AddSubrange(#$A930, #$A95F, 117); // Rejang
  AddSubrange(#$AA00, #$AA5F, 118); // Cham
  AddSubrange(#$AC00, #$D7AF, 56); // Hangul Syllables
  AddSubrange(#$D800, #$DFFF, 57); // Non-Plane 0. Note that setting this bit implies that there is at least one supplementary code point beyond the Basic Multilingual Plane (BMP) that is supported by this font. See Surrogates and Supplementary Characters.
  AddSubrange(#$E000, #$F8FF, 60); // Private Use Area
  AddSubrange(#$F900, #$FAFF, 61); // CJK Compatibility Ideographs
  AddSubrange(#$FB00, #$FB4F, 62); // Alphabetic Presentation Forms
  AddSubrange(#$FB50, #$FDFF, 63); // Arabic Presentation Forms-A
  AddSubrange(#$FE00, #$FE0F, 91); // Variation Selectors
  AddSubrange(#$FE10, #$FE1F, 65); // Vertical Forms
  AddSubrange(#$FE20, #$FE2F, 64); // Combining Half Marks
  AddSubrange(#$FE30, #$FE4F, 65); // CJK Compatibility Forms
  AddSubrange(#$FE50, #$FE6F, 66); // Small Form Variants
  AddSubrange(#$FE70, #$FEFF, 67); // Arabic Presentation Forms-B
  AddSubrange(#$FF00, #$FFEF, 68); // Halfwidth And Fullwidth Forms
  AddSubrange(#$FFF0, #$FFFF, 69); // Specials

  //// Unsorted yet!
  //AddSubrange(#$10900, #$1091F', 58); // Phoenician
  //AddSubrange(#$20000, #$2A6DF', 59); // CJK Unified Ideographs Extension B
  //AddSubrange(#$2F800, #$2FA1F', 61); // CJK Compatibility Ideographs Supplement
  //AddSubrange(#$10300, #$1032F', 85); // Old Italic
  //AddSubrange(#$10330, #$1034F', 86); // Gothic
  //AddSubrange(#$10400, #$1044F', 87); // Deseret
  //AddSubrange(#$1D000, #$1D0FF', 88); // Byzantine Musical Symbols
  //AddSubrange(#$1D100, #$1D1FF', 88); // Musical Symbols
  //AddSubrange(#$1D200, #$1D24F', 88); // Ancient Greek Musical Notation
  //AddSubrange(#$1D400, #$1D7FF', 89); // Mathematical Alphanumeric Symbols
  //AddSubrange(#$FF000, #$FFFFD', 90); // Private Use (plane 15)
  //AddSubrange(#$10000, #$10FFFD, 90); // Private Use (plane 16)
  //AddSubrange(#$E0100, #$E01EF, 91); // Variation Selectors Supplement
  //AddSubrange(#$E0000, #$E007F, 92); // Tags
  //AddSubrange(#$10000, #$1007F, 101); // Linear B Syllabary
  //AddSubrange(#$10080, #$100FF, 101); // Linear B Ideograms
  //AddSubrange(#$10100, #$1013F, 101); // Aegean Numbers
  //AddSubrange(#$10140, #$1018F, 102); // Ancient Greek Numbers
  //AddSubrange(#$10380, #$1039F, 103); // Ugaritic
  //AddSubrange(#$103A0, #$103DF, 104); // Old Persian
  //AddSubrange(#$10450, #$1047F, 105); // Shavian
  //AddSubrange(#$10480, #$104AF, 106); // Osmanya
  //AddSubrange(#$10800, #$1083F, 107); // Cypriot Syllabary
  //AddSubrange(#$10A00, #$10A5F, 108); // Kharoshthi
  //AddSubrange(#$1D300, #$1D35F, 109); // Tai Xuan Jing Symbols
  //AddSubrange(#$12000, #$123FF, 110); // Cuneiform
  //AddSubrange(#$12400, #$1247F, 110); // Cuneiform Numbers and Punctuation
  //AddSubrange(#$1D360, #$1D37F, 111); // Counting Rod Numerals
  //AddSubrange(#$10190, #$101CF, 119); // Ancient Symbols
  //AddSubrange(#$101D0, #$101FF, 120); // Phaistos Disc
  //AddSubrange(#$10280, #$1029F, 121); // Lycian
  //AddSubrange(#$102A0, #$102DF, 121); // Carian
  //AddSubrange(#$10920, #$1093F, 121); // Lydian
  //AddSubrange(#$1F000, #$1F02F, 122); // Mahjong Tiles
  //AddSubrange(#$1F030, #$1F09F, 122); // Domino Tilesend;
end;

procedure TdxUnicodeRangeInfo.AddSubrange(AStartCharacter, AEndCharacter: WideChar; ABit: Integer);
begin
  if FCount = FCapacity then
    Grow;
  with FRanges[FCount] do
  begin
    LowValue := AStartCharacter;
    HiValue  := AEndCharacter;
    Bit      := ABit;
  end;
  Inc(FCount);
end;

procedure TdxUnicodeRangeInfo.Grow;
begin
  Capacity := Capacity + Max(Capacity div 2, 64);
end;

function TdxUnicodeRangeInfo.LookupSubrange(ACharacter: WideChar): PdxUnicodeSubrange;
var
  I, H, L: Integer;
begin
  Result := nil;
  L := 0;
  H := FCount - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    if ACharacter < FRanges[I].LowValue then
      H := I - 1
    else
      if ACharacter > FRanges[I].HiValue then
        L := I + 1
      else
      begin
        Result := @FRanges[I];
        Break;
      end;
  end;
end;

procedure TdxUnicodeRangeInfo.SetCapacity(AValue: Integer);
begin
  if FCapacity <> AValue then
  begin
    FCapacity := AValue;
    SetLength(FRanges, Capacity);
  end;
end;

initialization

finalization
  FreeAndNil(FUnicodeRangeInfo);

end.
