{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.TableFormatting;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Windows, SysUtils, Graphics, Generics.Collections,
  dxRichEdit.DocumentModel.IndexBasedObject, dxRichEdit.DocumentModel.Core,
  dxCoreClasses, dxRichEdit.Utils.Types, dxRichEdit.DocumentModel.ParagraphFormatting,
  dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.Borders,
  dxRichEdit.DocumentModel.MergedProperties, dxRichEdit.DocumentModel.History.IndexChangedHistoryItem,
  dxRichEdit.Platform.Font;

type
  TdxTableBorders = class;
  TdxCellMargins = class;
  TdxTopMarginUnit = class;
  TdxBottomMarginUnit = class;
  TdxLeftMarginUnit = class;
  TdxRightMarginUnit = class;
  TdxTableIndent = class;
  TdxCellSpacing =  class;
  TdxTableRowGeneralSettings = class;
  TdxTableCellProperties = class;
  TdxTableCellBorders = class;
  TdxTableCellGeneralSettings = class;
  TdxMergedTableCellProperties = class;
  TdxTableCellGeneralSettingsInfo = class;

  TdxConditionalRowType = (
    Unknown = 0,
    FirstRow,
    LastRow,
    EvenRowBand,
    OddRowBand,
    Normal
  );

  TdxConditionalColumnType = (
    Unknown = 0,
    FirstColumn,
    LastColumn,
    EvenColumnBand,
    OddColumnBand,
    Normal
  );

  { IdxPropertiesContainerWithMask<T> }

  IdxPropertiesContainerWithMask<T> = interface(IdxPropertiesContainer)
  ['{958DB55C-A2BD-4BEB-AD5A-8DFDEAA82EBA}']
    function GetUse(AMask: T): Boolean;
  end;

  TdxConditionalTableStyleFormattingTypes = (
    WholeTable        = 4096,
    FirstRow          = 2048, 
    LastRow           = 1024, 
    FirstColumn       = 512,  
    LastColumn        = 256,  
    OddColumnBanding  = 128,  
    EvenColumnBanding = 64,   
    OddRowBanding     = 32,   
    EvenRowBanding    = 16,   
    TopRightCell      = 8,    
    TopLeftCell       = 4,    
    BottomRightCell   = 2,    
    BottomLeftCell    = 1     
  );

  { IdxTableCellBorders }

  IdxTableCellBorders = interface
  ['{F9F4AE61-BE76-4915-906B-62BEFE060AFD}']
    function GetTopBorder: TdxBorderBase;
    function GetBottomBorder: TdxBorderBase;
    function GetLeftBorder: TdxBorderBase;
    function GetRightBorder: TdxBorderBase;

    property TopBorder: TdxBorderBase read GetTopBorder;
    property BottomBorder: TdxBorderBase read GetBottomBorder;
    property LeftBorder: TdxBorderBase read GetLeftBorder;
    property RightBorder: TdxBorderBase read GetRightBorder;
  end;

  { IdxCellPropertiesContainer }

  IdxCellPropertiesContainer = interface(IdxPropertiesContainerWithMask<Integer>)
  ['{080423F7-0E05-456D-9288-86D4CA892F65}']
  end;

  { IdxCellPropertiesOwner }

  IdxCellPropertiesOwner = interface
  ['{465BC58F-F854-408C-A2EE-9445B83C073E}']
    function CreateCellPropertiesChangedHistoryItem(AProperties: TdxTableCellProperties): TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
  end;

  { TdxBoolPropertyAccessor<T> }

  TdxBoolPropertyAccessor<T> = class
  type
    TdxGetOptionValueDelegate = reference to function(AOptions: T): Boolean;
    TdxSetOptionValueDelegate = reference to procedure(AOptions: T; const Value: Boolean);
  strict private
    FGet: TdxGetOptionValueDelegate;
    FSet: TdxSetOptionValueDelegate;
  public
    constructor Create(AGet: TdxGetOptionValueDelegate; ASet: TdxSetOptionValueDelegate);

    property Get: TdxGetOptionValueDelegate read FGet;
    property &Set: TdxSetOptionValueDelegate read FSet;
  end;

  { TdxPropertiesDictionary<T> }

  TdxPropertiesDictionary<T> = class(TObjectDictionary<TdxProperties, T>)
  public
    constructor Create;
  end;

  { TdxPropertiesBase<T> }

  TdxPropertiesBase<T: class,
    IdxCloneable<T>,
    IdxSupportsCopyFrom<T>,
    IdxSupportsSizeOf> = class abstract(TdxRichEditIndexBasedObject<T>, IdxPropertiesContainer)
  private
    FSuspendCount: Integer;
    FDeferredChanges: TdxDocumentModelChangeActions;
    function GetIsSuspendUpdateOptions: Boolean;
    function GetPieceTable: TObject;
  protected
    function GetAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<T>>; virtual; abstract;
    //IPropertiesContainer
    procedure BeginPropertiesUpdate;
    procedure EndPropertiesUpdate;
    procedure BeginChanging(AChangedProperty: TdxProperties);
    procedure ResetPropertyUse(AChangedProperty: TdxProperties);
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;
    procedure EndChanging;
    function ResetOptionsCore(AChangedProperty: TdxProperties; out AIsDeferred: Boolean): T; virtual;
    function ChangeOptionsCore(AChangedProperty: TdxProperties; out AIsDeferred: Boolean): T; virtual;

    function ChangePropertiesOptions(AAccessor: TdxBoolPropertyAccessor<T>; out AIsDeferred: Boolean): T;
    function ResetPropertiesOptions(AAccessor: TdxBoolPropertyAccessor<T>; out AIsDeferred: Boolean): T; virtual;

    property AccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<T>> read GetAccessorTable;
    property IsSuspendUpdateOptions: Boolean read GetIsSuspendUpdateOptions;
  public
    property PieceTable: TObject read GetPieceTable;
  end;

  { TdxTablePropertiesOptions }

  TdxTablePropertiesOptions = class(TcxIUnknownObject, IdxCloneable<TdxTablePropertiesOptions>,
    IdxSupportsCopyFrom<TdxTablePropertiesOptions>, IdxSupportsSizeOf)
  public const
    MaskUseNone                   = $00000000; 
    MaskUseLeftMargin             = $00000001; 
    MaskUseRightMargin            = $00000002; 
    MaskUseTopMargin              = $00000004; 
    MaskUseBottomMargin           = $00000008; 
    MaskUseCellSpacing            = $00000010; 
    MaskUseTableIndent            = $00000020; 
    MaskUseTableLayout            = $00000040; 
    MaskUseTableLook              = $00000080; 
    MaskUsePreferredWidth         = $00000100; 
    MaskUseTableStyleColBandSize  = $00000200; 
    MaskUseTableStyleRowBandSize  = $00000400; 
    MaskUseIsTableOverlap         = $00000800; 
    MaskUseFloatingPosition       = $00001000; 
    MaskUseLeftBorder             = $00002000; 
    MaskUseRightBorder            = $00004000; 
    MaskUseTopBorder              = $00008000; 
    MaskUseBottomBorder           = $00010000; 
    MaskUseInsideHorizontalBorder = $00020000; 
    MaskUseInsideVerticalBorder   = $00040000; 
    MaskUseBackgroundColor        = $00080000; 
    MaskUseTableAlignment         = $00100000; 
    MaskUseBorders                = $0007E000; 
    MaskUseAvoidDoubleBorders     = $00200000; 
    MaskUseAll                    = $7FFFFFFF; 
  private
    FValue: Integer;
    function GetUseAvoidDoubleBorders: Boolean; inline;
    function GetUseBackgroundColor: Boolean; inline;
    function GetUseBorders: Boolean; inline;
    function GetUseBottomBorder: Boolean; inline;
    function GetUseBottomMargin: Boolean; inline;
    function GetUseCellSpacing: Boolean; inline;
    function GetUseFloatingPosition: Boolean; inline;
    function GetUseInsideHorizontalBorder: Boolean; inline;
    function GetUseInsideVerticalBorder: Boolean; inline;
    function GetUseIsTableOverlap: Boolean; inline;
    function GetUseLeftBorder: Boolean; inline;
    function GetUseLeftMargin: Boolean; inline;
    function GetUsePreferredWidth: Boolean; inline;
    function GetUseRightBorder: Boolean; inline;
    function GetUseRightMargin: Boolean; inline;
    function GetUseTableAlignment: Boolean; inline;
    function GetUseTableIndent: Boolean; inline;
    function GetUseTableLayout: Boolean; inline;
    function GetUseTableLook: Boolean; inline;
    function GetUseTableStyleColBandSize: Boolean; inline;
    function GetUseTableStyleRowBandSize: Boolean; inline;
    function GetUseTopBorder: Boolean; inline;
    function GetUseTopMargin: Boolean; inline;
    procedure SetUseAvoidDoubleBorders(const Value: Boolean); inline;
    procedure SetUseBackgroundColor(const Value: Boolean); inline;
    procedure SetUseBorders(const Value: Boolean); inline;
    procedure SetUseBottomBorder(const Value: Boolean); inline;
    procedure SetUseBottomMargin(const Value: Boolean); inline;
    procedure SetUseCellSpacing(const Value: Boolean); inline;
    procedure SetUseFloatingPosition(const Value: Boolean); inline;
    procedure SetUseInsideHorizontalBorder(const Value: Boolean); inline;
    procedure SetUseInsideVerticalBorder(const Value: Boolean); inline;
    procedure SetUseIsTableOverlap(const Value: Boolean); inline;
    procedure SetUseLeftBorder(const Value: Boolean); inline;
    procedure SetUseLeftMargin(const Value: Boolean); inline;
    procedure SetUsePreferredWidth(const Value: Boolean); inline;
    procedure SetUseRightBorder(const Value: Boolean); inline;
    procedure SetUseRightMargin(const Value: Boolean); inline;
    procedure SetUseTableAlignment(const Value: Boolean); inline;
    procedure SetUseTableIndent(const Value: Boolean); inline;
    procedure SetUseTableLayout(const Value: Boolean); inline;
    procedure SetUseTableLook(const Value: Boolean); inline;
    procedure SetUseTableStyleColBandSize(const Value: Boolean); inline;
    procedure SetUseTableStyleRowBandSize(const Value: Boolean); inline;
    procedure SetUseTopBorder(const Value: Boolean); inline;
    procedure SetUseTopMargin(const Value: Boolean); inline;
  protected
    procedure SetValue(AMask: Integer; ABitVal: Boolean); inline;

    class function GetOptionsUseLeftBorder(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseLeftBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseRightBorder(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseRightBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTopBorder(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTopBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseBottomBorder(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseBottomBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseInsideVerticalBorder(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseInsideVerticalBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseInsideHorizontalBorder(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseInsideHorizontalBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseLeftMargin(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseLeftMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseRightMargin(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseRightMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTopMargin(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTopMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseBottomMargin(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseBottomMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseCellSpacing(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseCellSpacing(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUsePreferredWidth(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUsePreferredWidth(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTableIndent(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableIndent(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTableLayout(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableLayout(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTableAlignment(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableAlignment(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTableLook(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableLook(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTableOverlap(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableOverlap(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTableStyleColBandSize(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableStyleColBandSize(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseTableStyleRowBandSize(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableStyleRowBandSize(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseFloatingPosition(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseFloatingPosition(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseBackgroundColor(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseBackgroundColor(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;
    class function GetOptionsUseAvoidDoubleBorders(AOptions: TdxTablePropertiesOptions): Boolean; static;
    class procedure SetOptionsUseAvoidDoubleBorders(AOptions: TdxTablePropertiesOptions; const Value: Boolean); static;

    property Value: Integer read FValue;
  public
    constructor Create(AValue: Integer = 0);
    procedure CopyFrom(const AOptions: TdxTablePropertiesOptions);
    function Clone: TdxTablePropertiesOptions;
    function Equals(AObject: TObject): Boolean; override;
    function GetHashCode: Integer; override;
    function GetValue(AMask: Integer): Boolean; inline;
    property UseLeftMargin: Boolean read GetUseLeftMargin write SetUseLeftMargin;
    property UseRightMargin: Boolean read GetUseRightMargin write SetUseRightMargin;
    property UseTopMargin: Boolean read GetUseTopMargin write SetUseTopMargin;
    property UseBottomMargin: Boolean read GetUseBottomMargin write SetUseBottomMargin;
    property UseCellSpacing: Boolean read GetUseCellSpacing write SetUseCellSpacing;
    property UseTableIndent: Boolean read GetUseTableIndent write SetUseTableIndent;
    property UseTableLayout: Boolean read GetUseTableLayout write SetUseTableLayout;
    property UseTableAlignment: Boolean read GetUseTableAlignment write SetUseTableAlignment;
    property UseTableLook: Boolean read GetUseTableLook write SetUseTableLook;
    property UsePreferredWidth: Boolean read GetUsePreferredWidth write SetUsePreferredWidth;
    property UseTableStyleColBandSize: Boolean read GetUseTableStyleColBandSize write SetUseTableStyleColBandSize;
    property UseTableStyleRowBandSize: Boolean read GetUseTableStyleRowBandSize write SetUseTableStyleRowBandSize;
    property UseIsTableOverlap: Boolean read GetUseIsTableOverlap write SetUseIsTableOverlap;
    property UseFloatingPosition: Boolean read GetUseFloatingPosition write SetUseFloatingPosition;
    property UseBackgroundColor: Boolean read GetUseBackgroundColor write SetUseBackgroundColor;
    property UseLeftBorder: Boolean read GetUseLeftBorder write SetUseLeftBorder;
    property UseRightBorder: Boolean read GetUseRightBorder write SetUseRightBorder;
    property UseTopBorder: Boolean read GetUseTopBorder write SetUseTopBorder;
    property UseBottomBorder: Boolean read GetUseBottomBorder write SetUseBottomBorder;
    property UseInsideHorizontalBorder: Boolean read GetUseInsideHorizontalBorder write SetUseInsideHorizontalBorder;
    property UseInsideVerticalBorder: Boolean read GetUseInsideVerticalBorder write SetUseInsideVerticalBorder;
    property UseBorders: Boolean read GetUseBorders write SetUseBorders;
    property UseAvoidDoubleBorders: Boolean read GetUseAvoidDoubleBorders write SetUseAvoidDoubleBorders;
  end;

  { IdxTablePropertiesContainer }

  IdxTablePropertiesContainer = interface(IdxPropertiesContainerWithMask<Integer>)
  ['{0FEE551B-B04A-412F-B4FA-C47DCBCEBFB1}']
  end;

  { IdxCellMarginsContainer }

  IdxCellMarginsContainer = interface(IdxPropertiesContainer)
  ['{F842C839-4EE1-4ABD-99FA-EB1ED07D8F8A}']
    function GetUseLeftMargin: Boolean;
    function GetUseRightMargin: Boolean;
    function GetUseTopMargin: Boolean;
    function GetUseBottomMargin: Boolean;

    property UseLeftMargin: Boolean read GetUseLeftMargin;
    property UseRightMargin: Boolean read GetUseRightMargin;
    property UseTopMargin: Boolean read GetUseTopMargin;
    property UseBottomMargin: Boolean read GetUseBottomMargin;
  end;

  TdxHorizontalAlignMode = (
    None,
    Center,  
    Inside,  
    Left,    
    Outside, 
    Right
  );

  TdxVerticalAlignMode = (
    None,
    Bottom,  
    Center,
    &Inline, 
    Inside,  
    Outside,
    Top
  );

  TdxVerticalAnchorTypes = (
    Margin,  
    Page,    
    Paragraph
  );

  TdxHorizontalAnchorTypes = (
    Margin,  
    Page,    
    Column
  );

  TdxTextWrapping = (
    Never,
    Around
  );

  { TdxTableFloatingPositionInfo }

  TdxTableFloatingPositionInfo = class(TcxIUnknownObject, IdxCloneable<TdxTableFloatingPositionInfo>,
    IdxSupportsCopyFrom<TdxTableFloatingPositionInfo>, IdxSupportsSizeOf)
  private
    FBottomFromText: Integer;
    FLeftFromText: Integer;
    FTopFromText: Integer;
    FRightFromText: Integer;
    FTableHorizontalPosition: Integer;
    FTableVerticalPosition: Integer;
    FHorizAlign: TdxHorizontalAlignMode;
    FVertAlign: TdxVerticalAlignMode;
    FHorizAnchor: TdxHorizontalAnchorTypes;
    FVertAnchor: TdxVerticalAnchorTypes;
    FTextWrapping: TdxTextWrapping;
  protected
    function IsHorizontalAbsolutePositionUse: Boolean;
    function IsVerticalAbsolutePositionUse: Boolean;
  public
    procedure CopyFrom(const AInfo: TdxTableFloatingPositionInfo);
    function Clone: TdxTableFloatingPositionInfo;
    function Equals(AObject: TObject): Boolean; override;
    function GetHashCode: Integer; override;
    property BottomFromText: Integer read FBottomFromText write FBottomFromText;
    property LeftFromText: Integer read FLeftFromText write FLeftFromText;
    property TopFromText: Integer read FTopFromText write FTopFromText;
    property RightFromText: Integer read FRightFromText write FRightFromText;
    property TableHorizontalPosition: Integer read FTableHorizontalPosition write FTableHorizontalPosition;
    property TableVerticalPosition: Integer read FTableVerticalPosition write FTableVerticalPosition;
    property HorizontalAlign: TdxHorizontalAlignMode read FHorizAlign write FHorizAlign;
    property VerticalAlign: TdxVerticalAlignMode read FVertAlign write FVertAlign;
    property HorizontalAnchor: TdxHorizontalAnchorTypes read FHorizAnchor write FHorizAnchor;
    property VerticalAnchor: TdxVerticalAnchorTypes read FVertAnchor write FVertAnchor;
    property TextWrapping: TdxTextWrapping read FTextWrapping write FTextWrapping;
  end;

  TdxTableFloatingPositionChangeType = (
    None = 0,
    LeftFromText,
    RightFromText,
    TopFromText,
    BottomFromText,
    TableHorizontalPosition,
    TableVerticalPosition,
    HorizontalAlign,
    VerticalAlign,
    HorizontalAnchor,
    VerticalAnchor,
    TextWrapping,
    BatchUpdate
  );

  { TdxTableFloatingPositionChangeActionsCalculator }

  TdxTableFloatingPositionChangeActionsCalculator = class
  public
    class function CalculateChangeActions(AChange: TdxTableFloatingPositionChangeType): TdxDocumentModelChangeActions;
  end;

  { TdxTableFloatingPosition }

  TdxTableFloatingPosition = class(TdxRichEditIndexBasedObject<TdxTableFloatingPositionInfo>)
  private
    FOwner: IdxPropertiesContainer;
    function GetBottomFromText: Integer;
    function GetHorizontalAlign: TdxHorizontalAlignMode;
    function GetHorizontalAnchor: TdxHorizontalAnchorTypes;
    function GetLeftFromText: Integer;
    function GetRightFromText: Integer;
    function GetTableHorizontalPosition: Integer;
    function GetTableVerticalPosition: Integer;
    function GetTextWrapping: TdxTextWrapping;
    function GetTopFromText: Integer;
    function GetVerticalAlign: TdxVerticalAlignMode;
    function GetVerticalAnchor: TdxVerticalAnchorTypes;
    procedure InnerSetBottomFromText(const Value: Integer);
    procedure InnerSetHorizontalAlign(const Value: TdxHorizontalAlignMode);
    procedure InnerSetHorizontalAnchor(const Value: TdxHorizontalAnchorTypes);
    procedure InnerSetLeftFromText(const Value: Integer);
    procedure InnerSetRightFromText(const Value: Integer);
    procedure InnerSetTableHorizontalPosition(const Value: Integer);
    procedure InnerSetTableVerticalPosition(const Value: Integer);
    procedure InnerSetTextWrapping(const Value: TdxTextWrapping);
    procedure InnerSetTopFromText(const Value: Integer);
    procedure InnerSetVerticalAlign(const Value: TdxVerticalAlignMode);
    procedure InnerSetVerticalAnchor(const Value: TdxVerticalAnchorTypes);
  protected
    function SetBottomFromText(const AInfo: TdxTableFloatingPositionInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetLeftFromText(const AInfo: TdxTableFloatingPositionInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetTopFromText(const AInfo: TdxTableFloatingPositionInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetRightFromText(const AInfo: TdxTableFloatingPositionInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetTableHorizontalPosition(const AInfo: TdxTableFloatingPositionInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetTableVerticalPosition(const AInfo: TdxTableFloatingPositionInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetHorizontalAlign(const AInfo: TdxTableFloatingPositionInfo; const AValue: TdxHorizontalAlignMode): TdxDocumentModelChangeActions;
    function SetVerticalAlign(const AInfo: TdxTableFloatingPositionInfo; const AValue: TdxVerticalAlignMode): TdxDocumentModelChangeActions;
    function SetHorizontalAnchor(const AInfo: TdxTableFloatingPositionInfo; const AValue: TdxHorizontalAnchorTypes): TdxDocumentModelChangeActions;
    function SetVerticalAnchor(const AInfo: TdxTableFloatingPositionInfo; const AValue: TdxVerticalAnchorTypes): TdxDocumentModelChangeActions;
    function SetTextWrapping(const AInfo: TdxTableFloatingPositionInfo; const AValue: TdxTextWrapping): TdxDocumentModelChangeActions;

    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableFloatingPositionInfo>; override;
    procedure OnBeginAssign; override;
    procedure OnEndAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function IsHorizontalRelativePositionUse: Boolean;
    function IsVerticalRelativePositionUse: Boolean;

    property Owner: IdxPropertiesContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; const AOwner: IdxPropertiesContainer); reintroduce;

    property BottomFromText: Integer read GetBottomFromText write InnerSetBottomFromText;
    property LeftFromText: Integer read GetLeftFromText write InnerSetLeftFromText;
    property TopFromText: Integer read GetTopFromText write InnerSetTopFromText;
    property RightFromText: Integer read GetRightFromText write InnerSetRightFromText;
    property TableHorizontalPosition: Integer read GetTableHorizontalPosition write InnerSetTableHorizontalPosition;
    property TableVerticalPosition: Integer read GetTableVerticalPosition write InnerSetTableVerticalPosition;
    property HorizontalAlign: TdxHorizontalAlignMode read GetHorizontalAlign write InnerSetHorizontalAlign;
    property VerticalAlign: TdxVerticalAlignMode read GetVerticalAlign write InnerSetVerticalAlign;
    property HorizontalAnchor: TdxHorizontalAnchorTypes read GetHorizontalAnchor write InnerSetHorizontalAnchor;
    property VerticalAnchor: TdxVerticalAnchorTypes read GetVerticalAnchor write InnerSetVerticalAnchor;
    property TextWrapping: TdxTextWrapping read GetTextWrapping write InnerSetTextWrapping;
  end;

  { TdxTableFloatingPositionInfoCache }

  TdxTableFloatingPositionInfoCache = class(TdxUniqueItemsCache<TdxTableFloatingPositionInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableFloatingPositionInfo; override;
  end;

  TdxTableLayoutType = (
    Fixed,
    Autofit
  );

  TdxTableRowAlignment = (
    Both,
    Center,
    Distribute,
    Left,
    NumTab,   
    Right
  );

  TdxTableLookTypes = (
    None = $0000,
    ApplyFirstRow = $0020,
    ApplyLastRow = $0040,
    ApplyFirstColumn = $0080,
    ApplyLastColumn = $0100,
    DoNotApplyRowBanding = $0200,
    DoNotApplyColumnBanding = $0400
  );

  { TdxTableGeneralSettingsInfo }

  TdxTableGeneralSettingsInfo =
    class(TcxIUnknownObject, IdxCloneable<TdxTableGeneralSettingsInfo>,
      IdxSupportsCopyFrom<TdxTableGeneralSettingsInfo>, IdxSupportsSizeOf)
  private
    FTableStyleColumnBandSize: Integer;
    FTableStyleRowBandSize: Integer;
    FIsTableOverlap: Boolean;
    FAvoidDoubleBorders: Boolean;
    FTableLayout: TdxTableLayoutType;
    FTableLook: TdxTableLookTypes;
    FBackgroundColor: TColor;
    FTableAlignment: TdxTableRowAlignment;
  public
    procedure CopyFrom(const AInfo: TdxTableGeneralSettingsInfo);
    function Clone: TdxTableGeneralSettingsInfo;
    function Equals(AObject: TObject): Boolean; override;
    property TableStyleColBandSize: Integer read FTableStyleColumnBandSize write FTableStyleColumnBandSize; 
    property TableStyleRowBandSize: Integer read FTableStyleRowBandSize write FTableStyleRowBandSize; 
    property IsTableOverlap: Boolean read FIsTableOverlap write FIsTableOverlap; 
    property AvoidDoubleBorders: Boolean read FAvoidDoubleBorders write FAvoidDoubleBorders; 
    property TableLayout: TdxTableLayoutType read FTableLayout write FTableLayout; 
    property TableAlignment: TdxTableRowAlignment read FTableAlignment write FTableAlignment; 
    property TableLook: TdxTableLookTypes read FTableLook write FTableLook; 
    property BackgroundColor: TColor read FBackgroundColor write FBackgroundColor; 
  end;

  TdxTableGeneralSettingsChangeType = (
    None = 0,
    TableLayout,
    TableLook,
    TableStyleColumnBandSize,
    TableStyleRowBandSize,
    IsTableOverlap,
    BackgroundColor,
    BatchUpdate,
    TableAlignment,
    AvoidDoubleBorders
  );

  { TdxTableGeneralSettingsChangeActionsCalculator }

  TdxTableGeneralSettingsChangeActionsCalculator = class
  public
    class function CalculateChangeActions(AChange: TdxTableGeneralSettingsChangeType): TdxDocumentModelChangeActions; static;
  end;

  { TdxTableGeneralSettingsInfoCache }

  TdxTableGeneralSettingsInfoCache = class(TdxUniqueItemsCache<TdxTableGeneralSettingsInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableGeneralSettingsInfo; override;
  end;

  { TdxTableGeneralSettings }

  TdxTableGeneralSettings = class(TdxRichEditIndexBasedObject<TdxTableGeneralSettingsInfo>)
  private
    FOwner: IdxPropertiesContainer;
    function GetAvoidDoubleBorders: Boolean;
    function GetBackgroundColor: TColor;
    function GetIsTableOverlap: Boolean;
    function GetTableAlignment: TdxTableRowAlignment;
    function GetTableLayout: TdxTableLayoutType;
    function GetTableLook: TdxTableLookTypes;
    function GetTableStyleColumnBandSize: Integer;
    function GetTableStyleRowBandSize: Integer;
    procedure InnerSetAvoidDoubleBorders(const Value: Boolean);
    procedure InnerSetBackgroundColor(const Value: TColor);
    procedure InnerSetTableAlignment(const Value: TdxTableRowAlignment);
    procedure InnerSetTableLayout(const Value: TdxTableLayoutType);
    procedure InnerSetTableLook(const Value: TdxTableLookTypes);
    procedure InnerSetTableStyleColumnBandSize(const Value: Integer);
    procedure InnerSetTableStyleRowBandSize(const Value: Integer);
    procedure InnerSetIsTableOverlap(const Value: Boolean);
  protected
    function SetTableLayout(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions; overload;
    function SetTableAlignment(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetTableLook(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetBackgroundColor(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetTableStyleColumnBandSize(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetTableStyleRowBandSize(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetTableOverlap(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetAvoidDoubleBorders(const ASettings: TdxTableGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableGeneralSettingsInfo>; override;
    procedure BeginChanging(AChangedProperty: TdxProperties); virtual;
    procedure EndChanging; virtual;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs); override;

    property Owner: IdxPropertiesContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; const AOwner: IdxPropertiesContainer); reintroduce;
    procedure CopyFrom(const ANewSettings: TdxTableGeneralSettings); overload;

    property TableLayout: TdxTableLayoutType read GetTableLayout write InnerSetTableLayout;
    property TableAlignment: TdxTableRowAlignment read GetTableAlignment write InnerSetTableAlignment;
    property TableLook: TdxTableLookTypes read GetTableLook write InnerSetTableLook;
    property BackgroundColor: TColor read GetBackgroundColor write InnerSetBackgroundColor;
    property TableStyleColumnBandSize: Integer read GetTableStyleColumnBandSize write InnerSetTableStyleColumnBandSize;
    property TableStyleRowBandSize: Integer read GetTableStyleRowBandSize write InnerSetTableStyleRowBandSize;
    property IsTableOverlap: Boolean read GetIsTableOverlap write InnerSetIsTableOverlap;
    property AvoidDoubleBorders: Boolean read GetAvoidDoubleBorders write InnerSetAvoidDoubleBorders;
  end;

  { TdxTableProperties }

  TdxTableProperties = class(TdxPropertiesBase<TdxTablePropertiesOptions>,
    IdxTablePropertiesContainer,
    IdxPropertiesContainerWithMask<Integer>,
    IdxCellMarginsContainer)
  strict private class var
    FAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTablePropertiesOptions>>;
    class constructor Initialize;
    class destructor Finalize;
    class function CreateAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTablePropertiesOptions>>;
  private
    FCellMargins: TdxCellMargins;
    FCellSpacing: TdxCellSpacing;
    FIndent: TdxTableIndent;
    FPreferredWidth: TdxPreferredWidth;
    FGeneralSettings: TdxTableGeneralSettings;
    FBorders: TdxTableBorders;
    FFloatingPosition: TdxTableFloatingPosition;
    function GetAvoidDoubleBorders: Boolean;
    function GetBackgroundColor: TColor;
    function GetIsTableOverlap: Boolean;
    function GetTableAlignment: TdxTableRowAlignment;
    function GetTableLayout: TdxTableLayoutType;
    function GetTableLook: TdxTableLookTypes;
    function GetTableStyleColBandSize: Integer;
    function GetTableStyleRowBandSize: Integer;
    function GetUseAvoidDoubleBorders: Boolean;
    function GetUseBackgroundColor: Boolean;
    function GetUseBottomMargin: Boolean;
    function GetUseCellSpacing: Boolean;
    function GetUseFloatingPosition: Boolean;
    function GetUseIsTableOverlap: Boolean;
    function GetUseLeftMargin: Boolean;
    function GetUsePreferredWidth: Boolean;
    function GetUseRightMargin: Boolean;
    function GetUseTableAlignment: Boolean;
    function GetUseTableIndent: Boolean;
    function GetUseTableLayout: Boolean;
    function GetUseTableLook: Boolean;
    function GetUseTableStyleColBandSize: Boolean;
    function GetUseTableStyleRowBandSize: Boolean;
    function GetUseTopMargin: Boolean;
    procedure SetAvoidDoubleBorders(const Value: Boolean);
    procedure SetBackgroundColor(const Value: TColor);
    procedure SetIsTableOverlap(const Value: Boolean);
    procedure SetTableAlignment(const Value: TdxTableRowAlignment);
    procedure InnerSetTableLayout(const Value: TdxTableLayoutType);
    procedure SetTableLook(const Value: TdxTableLookTypes);
    procedure SetTableStyleColBandSize(const Value: Integer);
    procedure SetTableStyleRowBandSize(const Value: Integer);
  protected
    function GetAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTablePropertiesOptions>>; override;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTablePropertiesOptions>; override;
    procedure ResetUse(AMask: Integer);
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
  public
    constructor Create(const ADocumentModelPart: TdxDocumentModelPart); override;
    destructor Destroy; override;
    procedure CopyFrom(ANewProperties: TdxTableProperties);
    function GetUse(AMask: Integer): Boolean;

    procedure Reset;
    procedure ResetAllUse;
    procedure Merge(AProperties: TdxTableProperties); virtual;

    property CellMargins: TdxCellMargins read FCellMargins;
    property UseLeftMargin: Boolean read GetUseLeftMargin;
    property UseRightMargin: Boolean read GetUseRightMargin;
    property UseTopMargin: Boolean read GetUseTopMargin;
    property UseBottomMargin: Boolean read GetUseBottomMargin;
    property CellSpacing: TdxCellSpacing read FCellSpacing;
    property UseCellSpacing: Boolean read GetUseCellSpacing;
    property TableIndent: TdxTableIndent read FIndent;
    property UseTableIndent: Boolean read GetUseTableIndent;
    property PreferredWidth: TdxPreferredWidth read FPreferredWidth;
    property UsePreferredWidth: Boolean read GetUsePreferredWidth;
    property Borders: TdxTableBorders read FBorders;
    property TableLayout: TdxTableLayoutType read GetTableLayout write InnerSetTableLayout;
    property UseTableLayout: Boolean read GetUseTableLayout;
    property TableAlignment: TdxTableRowAlignment read GetTableAlignment write SetTableAlignment;
    property UseTableAlignment: Boolean read GetUseTableAlignment;
    property TableLook: TdxTableLookTypes read GetTableLook write SetTableLook;
    property UseTableLook: Boolean read GetUseTableLook;
    property TableStyleColBandSize: Integer read GetTableStyleColBandSize write SetTableStyleColBandSize;
    property UseTableStyleColBandSize: Boolean read GetUseTableStyleColBandSize;
    property TableStyleRowBandSize: Integer read GetTableStyleRowBandSize write SetTableStyleRowBandSize;
    property UseTableStyleRowBandSize: Boolean read GetUseTableStyleRowBandSize;
    property IsTableOverlap: Boolean read GetIsTableOverlap write SetIsTableOverlap;
    property UseIsTableOverlap: Boolean read GetUseIsTableOverlap;
    property FloatingPosition: TdxTableFloatingPosition read FFloatingPosition;
    property UseFloatingPosition: Boolean read GetUseFloatingPosition;
    property BackgroundColor: TColor read GetBackgroundColor write SetBackgroundColor;
    property UseBackgroundColor: Boolean read GetUseBackgroundColor;
    property AvoidDoubleBorders: Boolean read GetAvoidDoubleBorders write SetAvoidDoubleBorders;
    property UseAvoidDoubleBorders: Boolean read GetUseAvoidDoubleBorders;

    property GeneralSettings: TdxTableGeneralSettings read FGeneralSettings;
  end;

  { TdxTablePropertiesOptionsCache }

  TdxTablePropertiesOptionsCache = class(TdxUniqueItemsCache<TdxTablePropertiesOptions>)
  public const
    EmptyTableFormattingOptionsItem = 0;
    RootTableFormattingOptionsItem  = 1;
  protected
    procedure InitItems(const AUnitConverter: IdxDocumentModelUnitConverter); override;
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTablePropertiesOptions; override;
    procedure AddRootStyleOptions;
  end;

  { TdxBordersBase<T> }

  TdxBordersBase<T> = class abstract(TcxIUnknownObject)
  private
    FOwner: IdxPropertiesContainerWithMask<T>;
    FBottomBorder: TdxBottomBorder;
    FLeftBorder: TdxLeftBorder;
    FRightBorder: TdxRightBorder;
    FTopBorder: TdxTopBorder;
    FInsideHorizontalBorder: TdxInsideHorizontalBorder;
    FInsideVerticalBorder: TdxInsideVerticalBorder;
    function GetUseLeftBorder: Boolean;
    function GetUseRightBorder: Boolean;
    function GetUseTopBorder: Boolean;
    function GetUseBottomBorder: Boolean;
    function GetUseInsideHorizontalBorder: Boolean;
    function GetUseInsideVerticalBorder: Boolean;
  protected
    function GetBottomBorder: TdxBorderBase;
    function GetLeftBorder: TdxBorderBase;
    function GetRightBorder: TdxBorderBase;
    function GetTopBorder: TdxBorderBase;

    procedure CopyFromCore(const ABorders: TdxBordersBase<T>); virtual;

    function UseLeftBorderMask: T; virtual; abstract;
    function UseRightBorderMask: T; virtual; abstract;
    function UseTopBorderMask: T; virtual; abstract;
    function UseBottomBorderMask: T; virtual; abstract;
    function UseInsideHorizontalBorderMask: T; virtual; abstract;
    function UseInsideVerticalBorderMask: T; virtual; abstract;

    property Owner: IdxPropertiesContainerWithMask<T> read FOwner;
  public
    constructor Create(APieceTable: TObject; const AOwner: IdxPropertiesContainerWithMask<T>);
    destructor Destroy; override;
    procedure CopyFrom(ABorders: TdxBordersBase<T>);
    procedure Merge(ABorders: TdxBordersBase<T>); virtual;

    property TopBorder: TdxTopBorder read FTopBorder;
    property LeftBorder: TdxLeftBorder read FLeftBorder;
    property RightBorder: TdxRightBorder read FRightBorder;
    property BottomBorder: TdxBottomBorder read FBottomBorder;
    property InsideHorizontalBorder: TdxInsideHorizontalBorder read FInsideHorizontalBorder;
    property InsideVerticalBorder: TdxInsideVerticalBorder read FInsideVerticalBorder;

    property UseLeftBorder: Boolean read GetUseLeftBorder;
    property UseRightBorder: Boolean read GetUseRightBorder;
    property UseTopBorder: Boolean read GetUseTopBorder;
    property UseBottomBorder: Boolean read GetUseBottomBorder;
    property UseInsideHorizontalBorder: Boolean read GetUseInsideHorizontalBorder;
    property UseInsideVerticalBorder: Boolean read GetUseInsideVerticalBorder;
  end;

  { TdxTableBorders }

  TdxTableBorders = class(TdxBordersBase<Integer>)
  protected
    function UseLeftBorderMask: Integer; override;
    function UseRightBorderMask: Integer; override;
    function UseTopBorderMask: Integer; override;
    function UseBottomBorderMask: Integer; override;
    function UseInsideHorizontalBorderMask: Integer; override;
    function UseInsideVerticalBorderMask: Integer; override;
  end;

  { TdxCombinedCellMarginsInfo }

  TdxCombinedCellMarginsInfo = class(TcxIUnknownObject, IdxCloneable<TdxCombinedCellMarginsInfo>)
  private
    FTop: TdxWidthUnitInfo;
    FLeft: TdxWidthUnitInfo;
    FRight: TdxWidthUnitInfo;
    FBottom: TdxWidthUnitInfo;
  public
    constructor Create; overload;
    constructor Create(ACellMargins: TdxCellMargins); overload;
    procedure CopyFrom(AInfo: TdxCombinedCellMarginsInfo);
    function Clone: TdxCombinedCellMarginsInfo;

    property Top: TdxWidthUnitInfo read FTop;
    property Left: TdxWidthUnitInfo read FLeft;
    property Right: TdxWidthUnitInfo read FRight;
    property Bottom: TdxWidthUnitInfo read FBottom;
  end;

  { TdxCellMargins }

  TdxCellMargins = class
  private
    FOwner: IdxCellMarginsContainer;
    FTop: TdxMarginUnitBase;
    FLeft: TdxMarginUnitBase;
    FRight: TdxMarginUnitBase;
    FBottom: TdxMarginUnitBase;
    function GetUseLeftMargin: Boolean;
    function GetUseRightMargin: Boolean;
    function GetUseTopMargin: Boolean;
    function GetUseBottomMargin: Boolean;
  protected
    function CreateTopMargin(APieceTable: TObject): TdxTopMarginUnit; virtual;
    function CreateBottomMargin(APieceTable: TObject): TdxBottomMarginUnit; virtual;
    function CreateLeftMargin(APieceTable: TObject): TdxLeftMarginUnit; virtual;
    function CreateRightMargin(APieceTable: TObject): TdxRightMarginUnit;

    property Owner: IdxCellMarginsContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; AOwner: IdxCellMarginsContainer);
    destructor Destroy; override;
    procedure CopyFrom(ANewMargins: TdxCellMargins); overload;
    procedure CopyFrom(ANewMargins: TdxCombinedCellMarginsInfo); overload;
    procedure Merge(AMargins: TdxCellMargins);

    property Top: TdxMarginUnitBase read FTop;
    property Left: TdxMarginUnitBase read FLeft;
    property Right: TdxMarginUnitBase read FRight;
    property Bottom: TdxMarginUnitBase read FBottom;

    property UseLeftMargin: Boolean read GetUseLeftMargin;
    property UseRightMargin: Boolean read GetUseRightMargin;
    property UseTopMargin: Boolean read GetUseTopMargin;
    property UseBottomMargin: Boolean read GetUseBottomMargin;
  end;

  { TdxLeftMarginUnit }

  TdxLeftMarginUnit = class(TdxMarginUnitBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxRightMarginUnit }

  TdxRightMarginUnit = class(TdxMarginUnitBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxTopMarginUnit }

  TdxTopMarginUnit = class(TdxMarginUnitBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxBottomMarginUnit }

  TdxBottomMarginUnit = class(TdxMarginUnitBase)
  protected
    function GetProperty: TdxProperties; override;
  end;

  { TdxTableIndent }

  TdxTableIndent = class(TdxWidthUnit)
  protected
    procedure OnBeginAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function SetTypeCore(const AUnit: TdxWidthUnitInfo; const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions; override;
    function SetValueCore(const AUnit: TdxWidthUnitInfo; const AValue: Integer): TdxDocumentModelChangeActions; override;
  end;

  { TdxCellSpacing }

  TdxCellSpacing = class(TdxWidthUnit)
  protected
    procedure OnBeginAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function SetTypeCore(const AUnit: TdxWidthUnitInfo;
      const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions; override;
    function SetValueCore(const AUnit: TdxWidthUnitInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; override;
  end;

  { TdxTableRowPropertiesOptions }

  TdxTableRowPropertiesOptions = class(TcxIUnknownObject,
    IdxCloneable<TdxTableRowPropertiesOptions>,
    IdxSupportsCopyFrom<TdxTableRowPropertiesOptions>,
    IdxSupportsSizeOf)
  public const
    MaskUseNone              = $00000000;
    MaskUseHeight            = $00000001;
    MaskUseCantSplit         = $00000002;
    MaskUseHideCellMark      = $00000004;
    MaskUseGridBefore        = $00000008;
    MaskUseGridAfter         = $00000010;
    MaskUseWidthBefore       = $00000020;
    MaskUseWidthAfter        = $00000040;
    MaskUseCellSpacing       = $00000080;
    MaskUseTableRowAlignment = $00000100;
    MaskUseHeader            = $00000400;
    MaskUseAll               = $7FFFFFF;
  strict private
    FValue: Integer;
    function GetUseHeight: Boolean; inline;
    procedure SetUseHeight(const AValue: Boolean); inline;
    function GetUseCantSplit: Boolean; inline;
    procedure SetUseCantSplit(const AValue: Boolean); inline;
    function GetUseHideCellMark: Boolean; inline;
    procedure SetUseHideCellMark(const AValue: Boolean); inline;
    function GetUseHeader: Boolean; inline;
    procedure SetUseHeader(const AValue: Boolean); inline;
    function GetUseGridBefore: Boolean; inline;
    procedure SetUseGridBefore(const AValue: Boolean); inline;
    function GetUseGridAfter: Boolean; inline;
    procedure SetUseGridAfter(const AValue: Boolean); inline;
    function GetUseWidthBefore: Boolean; inline;
    procedure SetUseWidthBefore(const AValue: Boolean); inline;
    function GetUseWidthAfter: Boolean; inline;
    procedure SetUseWidthAfter(const AValue: Boolean); inline;
    function GetUseCellSpacing: Boolean; inline;
    procedure SetUseCellSpacing(const AValue: Boolean); inline;
    function GetUseTableRowAlignment: Boolean; inline;
    procedure SetUseTableRowAlignment(const AValue: Boolean); inline;
  protected
    class function GetOptionsUseHeight(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseHeight(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseCantSplit(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseCantSplit(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseHideCellMark(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseHideCellMark(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseHeader(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseHeader(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseGridBefore(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseGridBefore(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseGridAfter(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseGridAfter(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseWidthBefore(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseWidthBefore(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseWidthAfter(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseWidthAfter(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseCellSpacing(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseCellSpacing(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseTableRowAlignment(AOptions: TdxTableRowPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTableRowAlignment(AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean); static;

    procedure SetVal(AMask: Integer; ABitValue: Boolean); inline;

    property Value: Integer read FValue;
  public
    constructor Create(AValue: Integer = 0);
    function Clone: TdxTableRowPropertiesOptions;
    procedure CopyFrom(const AOptions: TdxTableRowPropertiesOptions);
    function Equals(AObject: TObject): Boolean; override;
    function GetHashCode: Integer; override;
    function GetVal(AMask: Integer): Boolean; inline;

    property UseHeight: Boolean read GetUseHeight write SetUseHeight;
    property UseCantSplit: Boolean read GetUseCantSplit write SetUseCantSplit;
    property UseHideCellMark: Boolean read GetUseHideCellMark write SetUseHideCellMark;
    property UseHeader: Boolean read GetUseHeader write SetUseHeader;
    property UseGridBefore: Boolean read GetUseGridBefore write SetUseGridBefore;
    property UseGridAfter: Boolean read GetUseGridAfter write SetUseGridAfter;
    property UseWidthBefore: Boolean read GetUseWidthBefore write SetUseWidthBefore;
    property UseWidthAfter: Boolean read GetUseWidthAfter write SetUseWidthAfter;
    property UseCellSpacing: Boolean read GetUseCellSpacing write SetUseCellSpacing;
    property UseTableRowAlignment: Boolean read GetUseTableRowAlignment write SetUseTableRowAlignment;
  end;

  { TdxTableRowProperties }

  TdxTableRowProperties = class(TdxPropertiesBase<TdxTableRowPropertiesOptions>)
  strict private class var
    FAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>>;
    class constructor Initialize;
    class destructor Finalize;
    class function CreateAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>>;
  private
    FHeight: TdxHeightUnit;
    FWidthBefore: TdxWidthUnit;
    FWidthAfter: TdxWidthUnit;
    FCellSpacing: TdxWidthUnit;
    FGeneralSettings: TdxTableRowGeneralSettings;
    function GetUseHeight: Boolean;
    function GetUseWidthBefore: Boolean;
    function GetUseWidthAfter: Boolean;
    function GetUseCellSpacing: Boolean;
    function GetHeader: Boolean;
    procedure SetHeader(const Value: Boolean);
    function GetUseHeader: Boolean;
    function GetHideCellMark: Boolean;
    procedure SetHideCellMark(const Value: Boolean);
    function GetUseHideCellMark: Boolean;
    function GetCantSplit: Boolean;
    procedure SetCantSplit(const Value: Boolean);
    function GetUseCantSplit: Boolean;
    function GetTableRowAlignment: TdxTableRowAlignment;
    procedure SetTableRowAlignment(const Value: TdxTableRowAlignment);
    function GetUseTableRowAlignment: Boolean;
    function GetGridAfter: Integer;
    procedure SetGridAfter(const Value: Integer);
    function GetUseGridAfter: Boolean;
    function GetGridBefore: Integer;
    procedure SetGridBefore(const Value: Integer);
    function GetUseGridBefore: Boolean;
  protected
    function GetAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>>; override;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableRowPropertiesOptions>; override;
    procedure ResetUse(AMask: Integer);
    procedure ResetAllUse;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;

    property GeneralSettings: TdxTableRowGeneralSettings read FGeneralSettings;
  public
    constructor Create(const ADocumentModelPart: TdxDocumentModelPart); override;
    destructor Destroy; override;
    procedure CopyFrom(AProperties: TdxTableRowProperties);
    function GetUse(AMask: Integer): Boolean;
    procedure Reset;

    procedure Merge(AProperties: TdxTableRowProperties);

    property Height: TdxHeightUnit read FHeight;
    property UseHeight: Boolean read GetUseHeight;
    property WidthBefore: TdxWidthUnit read FWidthBefore;
    property UseWidthBefore: Boolean read GetUseWidthBefore;
    property WidthAfter: TdxWidthUnit read FWidthAfter;
    property UseWidthAfter: Boolean read GetUseWidthAfter;
    property CellSpacing: TdxWidthUnit read FCellSpacing;
    property UseCellSpacing: Boolean read GetUseCellSpacing;
    property Header: Boolean read GetHeader write SetHeader;
    property UseHeader: Boolean read GetUseHeader;
    property HideCellMark: Boolean read GetHideCellMark write SetHideCellMark;
    property UseHideCellMark: Boolean read GetUseHideCellMark;
    property CantSplit: Boolean read GetCantSplit write SetCantSplit;
    property UseCantSplit: Boolean read GetUseCantSplit;
    property TableRowAlignment: TdxTableRowAlignment read GetTableRowAlignment write SetTableRowAlignment;
    property UseTableRowAlignment: Boolean read GetUseTableRowAlignment;
    property GridAfter: Integer read GetGridAfter write SetGridAfter;
    property UseGridAfter: Boolean read GetUseGridAfter;
    property GridBefore: Integer read GetGridBefore write SetGridBefore;
    property UseGridBefore: Boolean read GetUseGridBefore;
  end;

  { TdxTableRowGeneralSettingsInfo }

  TdxTableRowGeneralSettingsInfo = class(TcxIUnknownObject, IdxCloneable<TdxTableRowGeneralSettingsInfo>,
    IdxSupportsCopyFrom<TdxTableRowGeneralSettingsInfo>, IdxSupportsSizeOf) 
  private
    FCantSplit: Boolean;
    FHideCellMark: Boolean;
    FHeader: Boolean;
    FGridBefore: Integer;
    FGridAfter: Integer;
    FTableRowAlignment: TdxTableRowAlignment;
  public
    procedure CopyFrom(const AInfo: TdxTableRowGeneralSettingsInfo);
    function Clone: TdxTableRowGeneralSettingsInfo;
    function Equals(AObject: TObject): Boolean; override;
    property CantSplit: Boolean read FCantSplit write FCantSplit;
    property HideCellMark: Boolean read FHideCellMark write FHideCellMark;
    property Header: Boolean read FHeader write FHeader;
    property GridBefore: Integer read FGridBefore write FGridBefore;
    property GridAfter: Integer read FGridAfter write FGridAfter;
    property TableRowAlignment: TdxTableRowAlignment read FTableRowAlignment write FTableRowAlignment;
  end;

  { TdxTableRowGeneralSettingsInfoCache }

  TdxTableRowGeneralSettingsInfoCache = class(TdxUniqueItemsCache<TdxTableRowGeneralSettingsInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableRowGeneralSettingsInfo; override;
  end;

  TdxTableRowChangeType = (
    None = 0,
    Header,
    HideCellMark,
    CantSplit,
    TableRowAlignment,
    TableRowConditionalFormatting,
    GridAfter,
    GridBefore,
    BatchUpdate
  );

  { TdxTableRowChangeActionsCalculator }

  TdxTableRowChangeActionsCalculator = class
  public
    class function CalculateChangeActions(AChange: TdxTableRowChangeType): TdxDocumentModelChangeActions;
  end;

  { TdxTableRowGeneralSettings }

  TdxTableRowGeneralSettings = class(TdxRichEditIndexBasedObject<TdxTableRowGeneralSettingsInfo>)
  private
    FOwner: IdxPropertiesContainer;
    function GetHeader: Boolean;
    procedure InnerSetHeader(const Value: Boolean);
    function GetHideCellMark: Boolean;
    procedure InnerSetHideCellMark(const Value: Boolean);
    function GetCantSplit: Boolean;
    procedure InnerSetCantSplit(const Value: Boolean);
    function GetTableRowAlignment: TdxTableRowAlignment;
    procedure InnerSetTableRowAlignment(const Value: TdxTableRowAlignment);
    function GetGridAfter: Integer;
    procedure InnerSetGridAfter(const Value: Integer);
    function GetGridBefore: Integer;
    procedure InnerSetGridBefore(const Value: Integer);
  protected
    procedure BeginChanging(AChangedProperty: TdxProperties); virtual;
    procedure EndChanging; virtual;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs); override;
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;
    function SetHeader(const AInfo: TdxTableRowGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetHideCellMark(const AInfo: TdxTableRowGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetCantSplit(const AInfo: TdxTableRowGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetTableRowAlignment(const AInfo: TdxTableRowGeneralSettingsInfo; const AValue: TdxTableRowAlignment): TdxDocumentModelChangeActions;
    function SetGridAfter(const AInfo: TdxTableRowGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetGridBefore(const AInfo: TdxTableRowGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableRowGeneralSettingsInfo>; override;

    property Owner: IdxPropertiesContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; const AOwner: IdxPropertiesContainer); reintroduce;
    procedure CopyFrom(const ASettings: TdxTableRowGeneralSettings); overload;

    property Header: Boolean read GetHeader write InnerSetHeader;
    property HideCellMark: Boolean read GetHideCellMark write InnerSetHideCellMark;
    property CantSplit: Boolean read GetCantSplit write InnerSetCantSplit;
    property TableRowAlignment: TdxTableRowAlignment read GetTableRowAlignment write InnerSetTableRowAlignment;
    property GridAfter: Integer read GetGridAfter write InnerSetGridAfter;
    property GridBefore: Integer read GetGridBefore write InnerSetGridBefore;
  end;

  { TdxCombinedTableRowPropertiesInfo }

  TdxCombinedTableRowPropertiesInfo =
    class(TcxIUnknownObject,
      IdxCloneable<TdxCombinedTableRowPropertiesInfo>,
      IdxSupportsCopyFrom<TdxCombinedTableRowPropertiesInfo>)
  private
    FWidthAfter: TdxWidthUnitInfo;
    FWidthBefore: TdxWidthUnitInfo;
    FCellSpacing: TdxWidthUnitInfo;
    FGeneralSettings: TdxTableRowGeneralSettingsInfo;
    FHeight: TdxHeightUnitInfo;
  protected
    constructor Create; overload;
  public
    constructor Create(ARowProperties: TdxTableRowProperties); overload;
    function Clone: TdxCombinedTableRowPropertiesInfo;
    procedure CopyFrom(const AValue: TdxCombinedTableRowPropertiesInfo);

    property Height: TdxHeightUnitInfo read FHeight;
    property WidthBefore: TdxWidthUnitInfo read FWidthBefore;
    property WidthAfter: TdxWidthUnitInfo read FWidthAfter;
    property CellSpacing: TdxWidthUnitInfo read FCellSpacing;
    property GeneralSettings: TdxTableRowGeneralSettingsInfo read FGeneralSettings;
  end;

  { TdxMergedTableRowProperties }

  TdxMergedTableRowProperties = class(TdxMergedProperties<TdxCombinedTableRowPropertiesInfo, TdxTableRowPropertiesOptions>);

  { TdxTableRowPropertiesMerger }

  TdxTableRowPropertiesMerger =
    class(TdxPropertiesMergerBase<TdxCombinedTableRowPropertiesInfo,
      TdxTableRowPropertiesOptions, TdxMergedTableRowProperties>)
  protected
    procedure MergeCore(const AInfo: TdxCombinedTableRowPropertiesInfo; const AOptions: TdxTableRowPropertiesOptions); override;
  public
    constructor Create(AInitialProperties: TdxTableRowProperties); overload;
    constructor Create(AInitialProperties: TdxMergedTableRowProperties); overload;
    procedure Merge(AProperties: TdxTableRowProperties); overload;
  end;

  { TdxRowHeight }

  TdxRowHeight = class(TdxHeightUnit)
  protected
    procedure OnBeginAssign; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
  end;

  { TdxWidthBefore }

  TdxWidthBefore = class(TdxWidthUnit)
  protected
    procedure OnBeginAssign; override;
    function SetTypeCore(const AUnit: TdxWidthUnitInfo;
      const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions; override;
    function SetValueCore(const AUnit: TdxWidthUnitInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; override;
  end;

  { TdxWidthAfter }

  TdxWidthAfter = class(TdxWidthUnit)
  protected
    procedure OnBeginAssign; override;
    function SetTypeCore(const AUnit: TdxWidthUnitInfo;
      const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions; override;
    function SetValueCore(const AUnit: TdxWidthUnitInfo;
      const AValue: Integer): TdxDocumentModelChangeActions; override;
  end;

  { TdxTableRowPropertiesOptionsCache }

  TdxTableRowPropertiesOptionsCache = class(TdxUniqueItemsCache<TdxTableRowPropertiesOptions>)
  public const
    EmptyRowPropertiesOptionsItem = 0;
    RootRowPropertiesOptionsItem = 1;
  protected
    procedure InitItems(const AUnitConverter: IdxDocumentModelUnitConverter); override;
    function CreateDefaultItem(
      const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableRowPropertiesOptions; override;
    procedure AddRootStyleOptions;
  end;

  { TdxTableCellPropertiesOptions }

  TdxTableCellPropertiesOptions = class(TcxIUnknownObject,
    IdxCloneable<TdxTableCellPropertiesOptions>,
    IdxSupportsCopyFrom<TdxTableCellPropertiesOptions>,
    IdxSupportsSizeOf)
  const
    MaskUseNone = $00000000;
    MaskUsePreferredWidth = $00000001;
    MaskUseHideCellMark = $00000002;
    MaskUseNoWrap = $00000004;
    MaskUseFitText = $00000008;
    MaskUseLeftMargin = $00000010;
    MaskUseRightMargin = $00000020;
    MaskUseTopMargin = $00000040;
    MaskUseBottomMargin = $00000080;
    MaskUseTextDirection = $00000100;
    MaskUseVerticalAlignment = $00000200;
    MaskUseCellConditionalFormatting = $00000800;
    MaskUseLeftBorder = $00001000;
    MaskUseRightBorder = $00002000;
    MaskUseTopBorder = $00004000;
    MaskUseBottomBorder = $00008000;
    MaskUseInsideHorizontalBorder = $00010000;
    MaskUseInsideVerticalBorder = $000020000;
    MaskUseTopLeftDiagonalBorder = $00040000;
    MaskUseTopRightDiagonalBorder = $00080000;
    MaskUseBackgroundColor = $00100000;
    MaskUseAll = $7FFFFFF;
  private
    FValue: Integer;
    function GetUseBackgroundColor: Boolean; inline;
    function GetUseBottomBorder: Boolean; inline;
    function GetUseBottomMargin: Boolean; inline;
    function GetUseCellConditionalFormatting: Boolean; inline;
    function GetUseFitText: Boolean; inline;
    function GetUseHideCellMark: Boolean; inline;
    function GetUseInsideHorizontalBorder: Boolean; inline;
    function GetUseInsideVerticalBorder: Boolean; inline;
    function GetUseLeftBorder: Boolean; inline;
    function GetUseLeftMargin: Boolean; inline;
    function GetUseNoWrap: Boolean; inline;
    function GetUsePreferredWidth: Boolean; inline;
    function GetUseRightBorder: Boolean; inline;
    function GetUseRightMargin: Boolean; inline;
    function GetUseTextDirection: Boolean; inline;
    function GetUseTopBorder: Boolean; inline;
    function GetUseTopLeftDiagonalBorder: Boolean; inline;
    function GetUseTopMargin: Boolean; inline;
    function GetUseTopRightDiagonalBorder: Boolean; inline;
    function GetUseVerticalAlignment: Boolean; inline;
    procedure SetUseBackgroundColor(const Value: Boolean); inline;
    procedure SetUseBottomBorder(const Value: Boolean); inline;
    procedure SetUseBottomMargin(const Value: Boolean); inline;
    procedure SetUseCellConditionalFormatting(const Value: Boolean); inline;
    procedure SetUseFitText(const Value: Boolean); inline;
    procedure SetUseHideCellMark(const Value: Boolean); inline;
    procedure SetUseInsideHorizontalBorder(const Value: Boolean); inline;
    procedure SetUseInsideVerticalBorder(const Value: Boolean); inline;
    procedure SetUseLeftBorder(const Value: Boolean); inline;
    procedure SetUseLeftMargin(const Value: Boolean); inline;
    procedure SetUseNoWrap(const Value: Boolean); inline;
    procedure SetUsePreferredWidth(const Value: Boolean); inline;
    procedure SetUseRightBorder(const Value: Boolean); inline;
    procedure SetUseRightMargin(const Value: Boolean); inline;
    procedure SetUseTextDirection(const Value: Boolean); inline;
    procedure SetUseTopBorder(const Value: Boolean); inline;
    procedure SetUseTopLeftDiagonalBorder(const Value: Boolean); inline;
    procedure SetUseTopMargin(const Value: Boolean); inline;
    procedure SetUseTopRightDiagonalBorder(const Value: Boolean); inline;
    procedure SetUseVerticalAlignment(const Value: Boolean); inline;
  protected
    class function GetOptionsUseCellConditionalFormatting(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseCellConditionalFormatting(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;

    class function GetOptionsUseTopMargin(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTopMargin(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseBottomMargin(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseBottomMargin(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseLeftMargin(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseLeftMargin(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseRightMargin(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseRightMargin(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;

    class function GetOptionsUseFitText(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseFitText(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseHideCellMark(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseHideCellMark(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseNoWrap(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseNoWrap(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUsePreferredWidth(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUsePreferredWidth(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseTextDirection(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTextDirection(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseVerticalAlignment(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseVerticalAlignment(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseVerticalMerging(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseVerticalMerging(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseColumnSpan(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseColumnSpan(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class procedure SetOptionsUseBackgroundColor(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseBackgroundColor(AOptions: TdxTableCellPropertiesOptions): Boolean; static;

    class function GetOptionsUseLeftBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseLeftBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseRightBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseRightBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseTopBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTopBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseBottomBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseBottomBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseInsideVerticalBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseInsideVerticalBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseInsideHorizontalBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseInsideHorizontalBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseTopLeftDiagonalBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTopLeftDiagonalBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;
    class function GetOptionsUseTopRightDiagonalBorder(AOptions: TdxTableCellPropertiesOptions): Boolean; static;
    class procedure SetOptionsUseTopRightDiagonalBorder(AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean); static;

    procedure SetVal(AMask: Integer; ABitVal: Boolean); inline;
  public
    constructor Create(AValue: Integer = 0);
    function Clone: TdxTableCellPropertiesOptions;
    procedure CopyFrom(const AOptions: TdxTableCellPropertiesOptions);
    function Equals(AObject: TObject): Boolean; override;
    function GetHashCode: Integer; override;
    function GetVal(AMask: Integer): Boolean; inline;

    property UseLeftBorder: Boolean read GetUseLeftBorder write SetUseLeftBorder;
    property UseRightBorder: Boolean read GetUseRightBorder write SetUseRightBorder;
    property UseTopBorder: Boolean read GetUseTopBorder write SetUseTopBorder;
    property UseBottomBorder: Boolean read GetUseBottomBorder write SetUseBottomBorder;
    property UseInsideHorizontalBorder: Boolean read GetUseInsideHorizontalBorder write SetUseInsideHorizontalBorder;
    property UseInsideVerticalBorder: Boolean read GetUseInsideVerticalBorder write SetUseInsideVerticalBorder;
    property UseTopLeftDiagonalBorder: Boolean read GetUseTopLeftDiagonalBorder write SetUseTopLeftDiagonalBorder;
    property UseTopRightDiagonalBorder: Boolean read GetUseTopRightDiagonalBorder write SetUseTopRightDiagonalBorder;

    property UsePreferredWidth: Boolean read GetUsePreferredWidth write SetUsePreferredWidth;
    property UseHideCellMark: Boolean read GetUseHideCellMark write SetUseHideCellMark;
    property UseNoWrap: Boolean read GetUseNoWrap write SetUseNoWrap;
    property UseFitText: Boolean read GetUseFitText write SetUseFitText;
    property UseLeftMargin: Boolean read GetUseLeftMargin write SetUseLeftMargin;
    property UseRightMargin: Boolean read GetUseRightMargin write SetUseRightMargin;
    property UseTopMargin: Boolean read GetUseTopMargin write SetUseTopMargin;
    property UseBottomMargin: Boolean read GetUseBottomMargin write SetUseBottomMargin;
    property UseTextDirection: Boolean read GetUseTextDirection write SetUseTextDirection;
    property UseVerticalAlignment: Boolean read GetUseVerticalAlignment write SetUseVerticalAlignment;
    property UseCellConditionalFormatting: Boolean read GetUseCellConditionalFormatting write SetUseCellConditionalFormatting;
    property UseBackgroundColor: Boolean read GetUseBackgroundColor write SetUseBackgroundColor;

    property Value: Integer read FValue;
  end;

  { TdxTableCellChangeActionsCalculator }

  TdxTableCellChangeType =(
    None = 0,
    HideCellMark,
    NoWrap,
    FitText,
    TextDirection,
    VerticalAlignment,
    ColumnSpan,
    HorizontalMerging,
    VerticalMerging,
    ConditionalFormatting,
    BackgroundColor,
    BatchUpdate
  );

  TdxTableCellChangeActionsCalculator = class
  public
    class function CalculateChangeActions(AChange: TdxTableCellChangeType): TdxDocumentModelChangeActions; static;
  end;

  { TdxTableCellPropertiesOptionsCache }

  TdxTableCellPropertiesOptionsCache = class(TdxUniqueItemsCache<TdxTableCellPropertiesOptions>)
  public const
    EmptyCellPropertiesOptionsItem = 0;
    RootCellPropertiesOptionsItem = 1;
  protected
    procedure InitItems(const AUnitConverter: IdxDocumentModelUnitConverter); override;
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableCellPropertiesOptions; override;
    procedure AddRootStyleOptions;
  end;

  { TdxTableCellProperties }

  TdxMergingState = (
    None,
    Continue,
    Restart
  );

  TdxTableCellProperties = class(TdxPropertiesBase<TdxTableCellPropertiesOptions>,
    IdxCellPropertiesContainer,
    IdxCellMarginsContainer)
  strict private class var
    FAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>>;
    class constructor Initialize;
    class destructor Finalize;
    class function CreateAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>>;
  private
    FPreferredWidth: TdxPreferredWidth;
    FOwner: IdxCellPropertiesOwner;
    FCellMargins: TdxCellMargins;
    FBorders: TdxTableCellBorders;
    FGeneralSettings: TdxTableCellGeneralSettings;
    FPropertiesChanged: TdxMulticastMethod<TdxEventHandler>;
    function GetBackgroundColor: TColor;
    function GetCellConditionalFormatting: TdxConditionalTableStyleFormattingTypes;
    function GetColumnSpan: Integer;
    function GetFitText: Boolean;
    function GetHideCellMark: Boolean;
    function GetNoWrap: Boolean;
    function GetTextDirection: TdxTextDirection;
    function GetUseBackgroundColor: Boolean;
    function GetUseBottomMargin: Boolean;
    function GetUseCellConditionalFormatting: Boolean;
    function GetUseFitText: Boolean;
    function GetUseHideCellMark: Boolean;
    function GetUseLeftMargin: Boolean;
    function GetUseNoWrap: Boolean;
    function GetUsePreferredWidth: Boolean;
    function GetUseRightMargin: Boolean;
    function GetUseTextDirection: Boolean;
    function GetUseTopMargin: Boolean;
    function GetUseVerticalAlignment: Boolean;
    function GetVerticalAlignment: TdxVerticalAlignment;
    function GetVerticalMerging: TdxMergingState;
    procedure SetBackgroundColor(const Value: TColor);
    procedure SetCellConditionalFormatting(const Value: TdxConditionalTableStyleFormattingTypes);
    procedure SetColumnSpan(const Value: Integer);
    procedure SetFitText(const Value: Boolean);
    procedure SetHideCellMark(const Value: Boolean);
    procedure SetNoWrap(const Value: Boolean);
    procedure SetTextDirection(const Value: TdxTextDirection);
    procedure SetVerticalAlignment(const Value: TdxVerticalAlignment);
    procedure SetVerticalMerging(const Value: TdxMergingState);
  protected
    function GetAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>>; override;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableCellPropertiesOptions>; override;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>; override;
    procedure ResetUse(AMask: Integer);
    procedure ResetAllUse;
    procedure RaisePropertiesChanged;

    property GeneralSettings: TdxTableCellGeneralSettings read FGeneralSettings;
  public
    constructor Create(APieceTable: TObject; AOwner: IdxCellPropertiesOwner); reintroduce;
    destructor Destroy; override;

    function GetUse(AMask: Integer): Boolean;
    procedure CopyFrom(AProperties: TdxTableCellProperties); overload;
    procedure CopyFrom(AProperties: TdxMergedTableCellProperties); overload;
    procedure Reset;
    procedure Merge(AProperties: TdxTableCellProperties);

    property PreferredWidth: TdxPreferredWidth read FPreferredWidth;
    property UsePreferredWidth: Boolean read GetUsePreferredWidth;
    property CellMargins: TdxCellMargins read FCellMargins;
    property UseLeftMargin: Boolean read GetUseLeftMargin;
    property UseRightMargin: Boolean read GetUseRightMargin;
    property UseTopMargin: Boolean read GetUseTopMargin;
    property UseBottomMargin: Boolean read GetUseBottomMargin;
    property Borders: TdxTableCellBorders read FBorders;
    property HideCellMark: Boolean read GetHideCellMark write SetHideCellMark;
    property UseHideCellMark: Boolean read GetUseHideCellMark;
    property NoWrap: Boolean read GetNoWrap write SetNoWrap;
    property UseNoWrap: Boolean read GetUseNoWrap;
    property FitText: Boolean read GetFitText write SetFitText;
    property UseFitText: Boolean read GetUseFitText;
    property BackgroundColor: TColor read GetBackgroundColor write SetBackgroundColor;
    property UseBackgroundColor: Boolean read GetUseBackgroundColor;
    property TextDirection: TdxTextDirection read GetTextDirection write SetTextDirection;
    property UseTextDirection: Boolean read GetUseTextDirection;
    property VerticalAlignment: TdxVerticalAlignment read GetVerticalAlignment write SetVerticalAlignment;
    property UseVerticalAlignment: Boolean read GetUseVerticalAlignment;
    property ColumnSpan: Integer read GetColumnSpan write SetColumnSpan;
    property VerticalMerging: TdxMergingState read GetVerticalMerging write SetVerticalMerging;
    property CellConditionalFormatting: TdxConditionalTableStyleFormattingTypes read GetCellConditionalFormatting write SetCellConditionalFormatting;
    property UseCellConditionalFormatting: Boolean read GetUseCellConditionalFormatting;
    property PropertiesChanged: TdxMulticastMethod<TdxEventHandler> read FPropertiesChanged;
  end;

  { TdxCombinedCellBordersInfo }

  TdxCombinedCellBordersInfo = class(TcxIUnknownObject,
    IdxCloneable<TdxCombinedCellBordersInfo>,
    IdxSupportsCopyFrom<TdxCombinedCellBordersInfo>)
  private
    FBottomBorder: TdxBorderInfo;
    FLeftBorder: TdxBorderInfo;
    FRightBorder: TdxBorderInfo;
    FTopBorder: TdxBorderInfo;
    FInsideHorizontalBorder: TdxBorderInfo;
    FInsideVerticalBorder: TdxBorderInfo;
    FTopLeftDiagonalBorder: TdxBorderInfo;
    FTopRightDiagonalBorder: TdxBorderInfo;
  protected
  public
    constructor Create; overload;
    constructor Create(ATableBorders: TdxTableCellBorders); overload;
    procedure CopyFrom(const AInfo: TdxCombinedCellBordersInfo);
    function Clone: TdxCombinedCellBordersInfo;

    property BottomBorder: TdxBorderInfo read FBottomBorder;
    property LeftBorder: TdxBorderInfo read FLeftBorder;
    property RightBorder: TdxBorderInfo read FRightBorder;
    property TopBorder: TdxBorderInfo read FTopBorder;
    property InsideHorizontalBorder: TdxBorderInfo read FInsideHorizontalBorder;
    property InsideVerticalBorder: TdxBorderInfo read FInsideVerticalBorder;
    property TopLeftDiagonalBorder: TdxBorderInfo read FTopLeftDiagonalBorder;
    property TopRightDiagonalBorder: TdxBorderInfo read FTopRightDiagonalBorder;
  end;

  { TdxCombinedTableBordersInfo }

  TdxCombinedTableBordersInfo = class(TcxIUnknownObject,
    IdxCloneable<TdxCombinedTableBordersInfo>,
    IdxSupportsCopyFrom<TdxCombinedTableBordersInfo>)
  private
    FBottomBorder: TdxBorderInfo;
    FInsideVerticalBorder: TdxBorderInfo;
    FInsideHorizontalBorder: TdxBorderInfo;
    FTopBorder: TdxBorderInfo;
    FLeftBorder: TdxBorderInfo;
    FRightBorder: TdxBorderInfo;
  protected
    constructor Create; overload;
  public
    constructor Create(ATableBorders: TdxTableBorders); overload;
    procedure CopyFrom(const AInfo: TdxCombinedTableBordersInfo);
    function Clone: TdxCombinedTableBordersInfo;

    property BottomBorder: TdxBorderInfo read FBottomBorder;
    property LeftBorder: TdxBorderInfo read FLeftBorder;
    property RightBorder: TdxBorderInfo read FRightBorder;
    property TopBorder: TdxBorderInfo read FTopBorder;
    property InsideHorizontalBorder: TdxBorderInfo read FInsideHorizontalBorder;
    property InsideVerticalBorder: TdxBorderInfo read FInsideVerticalBorder;
  end;

  { TdxCombinedTablePropertiesInfo }

  TdxCombinedTablePropertiesInfo = class(TcxIUnknownObject,
    IdxCloneable<TdxCombinedTablePropertiesInfo>)
  private
    FIndent: TdxWidthUnitInfo;
    FFloatingPosition: TdxTableFloatingPositionInfo;
    FPreferredWidth: TdxWidthUnitInfo;
    FBorders: TdxCombinedTableBordersInfo;
    FCellMargins: TdxCombinedCellMarginsInfo;
    FCellSpacing: TdxWidthUnitInfo;
    FGeneralSettings: TdxTableGeneralSettingsInfo;
  protected
    constructor Create; overload;
  public
    constructor Create(ATableProperties: TdxTableProperties); overload;
    destructor Destroy; override;
    procedure CopyFrom(AInfo: TdxCombinedTablePropertiesInfo);
    function Clone: TdxCombinedTablePropertiesInfo;

    property CellMargins: TdxCombinedCellMarginsInfo read FCellMargins;
    property Borders: TdxCombinedTableBordersInfo read FBorders;
    property CellSpacing: TdxWidthUnitInfo read FCellSpacing;
    property TableIndent: TdxWidthUnitInfo read FIndent;
    property PreferredWidth: TdxWidthUnitInfo read FPreferredWidth;
    property GeneralSettings: TdxTableGeneralSettingsInfo read FGeneralSettings;
    property FloatingPosition: TdxTableFloatingPositionInfo read FFloatingPosition;
  end;

  { TdxMergedTableProperties }

  TdxMergedTableProperties = class(TdxMergedProperties<TdxCombinedTablePropertiesInfo, TdxTablePropertiesOptions>);

  { TdxTablePropertiesMerger }

  TdxTablePropertiesMerger = class(TdxPropertiesMergerBase<TdxCombinedTablePropertiesInfo,
    TdxTablePropertiesOptions,
    TdxMergedTableProperties>)
  protected
    procedure MergeCore(const AInfo: TdxCombinedTablePropertiesInfo; const AOptions: TdxTablePropertiesOptions); override;
  public
    constructor Create(AInitialProperties: TdxTableProperties); overload;
    constructor Create(AInitialProperties: TdxMergedTableProperties); overload;
    procedure Merge(AProperties: TdxTableProperties); overload;
  end;

  { TdxCombinedCellPropertiesInfo }

  TdxCombinedCellPropertiesInfo = class(TcxIUnknownObject,
    IdxCloneable<TdxCombinedCellPropertiesInfo>,
    IdxSupportsCopyFrom<TdxCombinedCellPropertiesInfo>)
  private
    FPreferredWidth: TdxWidthUnitInfo;
    FCellMargins: TdxCombinedCellMarginsInfo;
    FBorders: TdxCombinedCellBordersInfo;
    FGeneralSettings: TdxTableCellGeneralSettingsInfo;
  public
    constructor Create(ACellProperties: TdxTableCellProperties); overload;
    constructor Create; overload;
    function Clone: TdxCombinedCellPropertiesInfo;
    procedure CopyFrom(const AValue: TdxCombinedCellPropertiesInfo);

    property PreferredWidth: TdxWidthUnitInfo read FPreferredWidth;
    property CellMargins: TdxCombinedCellMarginsInfo read FCellMargins;
    property Borders: TdxCombinedCellBordersInfo read FBorders;
    property GeneralSettings: TdxTableCellGeneralSettingsInfo read FGeneralSettings;
  end;

  { TdxMergedTableCellProperties }

  TdxMergedTableCellProperties = class(TdxMergedProperties<TdxCombinedCellPropertiesInfo, TdxTableCellPropertiesOptions>);

  { TdxTableCellPropertiesMerger }

  TdxTableCellPropertiesMerger = class(TdxPropertiesMergerBase<TdxCombinedCellPropertiesInfo,
    TdxTableCellPropertiesOptions,
    TdxMergedTableCellProperties>)
  protected
    procedure MergeCore(const AInfo: TdxCombinedCellPropertiesInfo; const AOptions: TdxTableCellPropertiesOptions); override;
  public
    constructor Create(AInitialProperties: TdxTableCellProperties); overload;
    constructor Create(AInitialProperties: TdxMergedTableCellProperties); overload;
    procedure Merge(AProperties: TdxTableCellProperties); overload;
  end;

  { TdxTableCellBorders }

  TdxTableCellBorders = class(TdxBordersBase<Integer>, IdxTableCellBorders)
  private
    FTopLeftDiagonalBorder: TdxTopLeftDiagonalBorder;
    FTopRightDiagonalBorder: TdxTopRightDiagonalBorder;
    function GetUseTopLeftDiagonalBorder: Boolean;
    function GetUseTopRightDiagonalBorder: Boolean;
  protected
    procedure CopyFromCore(const ABorders: TdxBordersBase<Integer>); override;
    function UseLeftBorderMask: Integer; override;
    function UseRightBorderMask: Integer; override;
    function UseTopBorderMask: Integer; override;
    function UseBottomBorderMask: Integer; override;
    function UseInsideHorizontalBorderMask: Integer; override;
    function UseInsideVerticalBorderMask: Integer; override;
  public
    constructor Create(APieceTable: TObject; AOwner: IdxCellPropertiesContainer);
    destructor Destroy; override;
    procedure Merge(ABorders: TdxBordersBase<Integer>); override;
    procedure CopyFrom(const ABorders: TdxCombinedCellBordersInfo); overload;

    property TopLeftDiagonalBorder: TdxTopLeftDiagonalBorder read FTopLeftDiagonalBorder;
    property TopRightDiagonalBorder: TdxTopRightDiagonalBorder read FTopRightDiagonalBorder;
    property UseTopLeftDiagonalBorder: Boolean read GetUseTopLeftDiagonalBorder;
    property UseTopRightDiagonalBorder: Boolean read GetUseTopRightDiagonalBorder;
  end;

  { TdxTableCellGeneralSettingsInfo }

  TdxTableCellGeneralSettingsInfo = class(TcxIUnknownObject,
    IdxCloneable<TdxTableCellGeneralSettingsInfo>, IdxSupportsCopyFrom<TdxTableCellGeneralSettingsInfo>,
    IdxSupportsSizeOf)
  private
    FHideCellMark: Boolean; 
    FNoWrap: Boolean;
    FFitText: Boolean;
    FTextDirection: TdxTextDirection;
    FVerticalAlignment: TdxVerticalAlignment;
    FColumnSpan: Integer;
    FHorizontalMerging: TdxMergingState;
    FVerticalMerging: TdxMergingState;
    FCellConditionalFormatting: TdxConditionalTableStyleFormattingTypes;
    FBackgroundColor: TColor;
  public
    constructor Create;
    function Clone: TdxTableCellGeneralSettingsInfo;
    procedure CopyFrom(const AInfo: TdxTableCellGeneralSettingsInfo);
    function Equals(AObject: TObject): Boolean; override;

    property HideCellMark: Boolean read FHideCellMark write FHideCellMark;
    property NoWrap: Boolean read FNoWrap write FNoWrap;
    property FitText: Boolean read FFitText write FFitText;
    property TextDirection: TdxTextDirection read FTextDirection write FTextDirection;
    property VerticalAlignment: TdxVerticalAlignment read FVerticalAlignment write FVerticalAlignment;
    property ColumnSpan: Integer read FColumnSpan write FColumnSpan;
    property HorizontalMerging: TdxMergingState read FHorizontalMerging write FHorizontalMerging;
    property VerticalMerging: TdxMergingState read FVerticalMerging write FVerticalMerging;
    property CellConditionalFormatting: TdxConditionalTableStyleFormattingTypes read FCellConditionalFormatting write FCellConditionalFormatting;
    property BackgroundColor: TColor read FBackgroundColor write FBackgroundColor;
  end;

  { TdxTableCellGeneralSettings }

  TdxTableCellGeneralSettings = class(TdxRichEditIndexBasedObject<TdxTableCellGeneralSettingsInfo>)
  private
    FOwner: IdxPropertiesContainer;
    function GetCellConditionalFormatting: TdxConditionalTableStyleFormattingTypes;
    function GetColumnSpan: Integer;
    function GetFitText: Boolean;
    function GetHideCellMark: Boolean;
    function GetNoWrap: Boolean;
    function GetTextDirection: TdxTextDirection;
    function GetVerticalAlignment: TdxVerticalAlignment;
    function GetVerticalMerging: TdxMergingState;
    procedure InnerSetCellConditionalFormatting(const Value: TdxConditionalTableStyleFormattingTypes);
    procedure InnerSetColumnSpan(const Value: Integer);
    procedure InnerSetFitText(const Value: Boolean);
    procedure InnerSetHideCellMark(const Value: Boolean);
    procedure InnerSetNoWrap(const Value: Boolean);
    procedure InnerSetTextDirection(const Value: TdxTextDirection);
    procedure InnerSetVerticalAlignment(const Value: TdxVerticalAlignment);
    procedure InnerSetVerticalMerging(const Value: TdxMergingState);
    function GetBackgroundColor: TColor;
    procedure InnerSetBackgroundColor(const Value: TColor);
  protected
    function SetHideCellMark(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetNoWrap(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetFitText(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: Boolean): TdxDocumentModelChangeActions;
    function SetTextDirection(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: TdxTextDirection): TdxDocumentModelChangeActions;
    function SetVerticalAlignment(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: TdxVerticalAlignment): TdxDocumentModelChangeActions;
    function SetColumnSpan(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: Integer): TdxDocumentModelChangeActions;
    function SetVerticalMerging(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: TdxMergingState): TdxDocumentModelChangeActions;
    function SetCellConditionalFormatting(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: TdxConditionalTableStyleFormattingTypes): TdxDocumentModelChangeActions;
    function SetBackgroundColor(const AInfo: TdxTableCellGeneralSettingsInfo; const AValue: TColor): TdxDocumentModelChangeActions;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableCellGeneralSettingsInfo>; override;
    procedure BeginChanging(AChangedProperty: TdxProperties); virtual;
    procedure EndChanging; virtual;
    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs); override;

    property Owner: IdxPropertiesContainer read FOwner;
  public
    constructor Create(APieceTable: TObject; AOwner: IdxPropertiesContainer); reintroduce;
    procedure CopyFrom(ANewSettings: TdxTableCellGeneralSettings); overload;

    property HideCellMark: Boolean read GetHideCellMark write InnerSetHideCellMark;
    property NoWrap: Boolean read GetNoWrap write InnerSetNoWrap;
    property FitText: Boolean read GetFitText write InnerSetFitText;
    property TextDirection: TdxTextDirection read GetTextDirection write InnerSetTextDirection;
    property VerticalAlignment: TdxVerticalAlignment read GetVerticalAlignment write InnerSetVerticalAlignment;
    property ColumnSpan: Integer read GetColumnSpan write InnerSetColumnSpan;
    property VerticalMerging: TdxMergingState read GetVerticalMerging write InnerSetVerticalMerging;
    property CellConditionalFormatting: TdxConditionalTableStyleFormattingTypes read GetCellConditionalFormatting write InnerSetCellConditionalFormatting;
    property BackgroundColor: TColor read GetBackgroundColor write InnerSetBackgroundColor;
  end;

  { TdxTableCellGeneralSettingsInfoCache }

  TdxTableCellGeneralSettingsInfoCache = class(TdxUniqueItemsCache<TdxTableCellGeneralSettingsInfo>)
  protected
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableCellGeneralSettingsInfo; override;
  end;

implementation

uses
  Classes, RTLConsts, dxRichEdit.Utils.BatchUpdateHelper, 
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Utils.Colors;

{ TdxPropertiesDictionary<T> }

constructor TdxPropertiesDictionary<T>.Create;
begin
  inherited Create([doOwnsValues]);
end;

{ TdxPropertiesBase<T> }

procedure TdxPropertiesBase<T>.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
begin
  if FSuspendCount > 0 then
    FDeferredChanges := FDeferredChanges + AChangeActions 
  else
    inherited ApplyChanges(AChangeActions);
end;

procedure TdxPropertiesBase<T>.BeginChanging(AChangedProperty: TdxProperties);
var
  AOptions: T;
  AHistory: TdxDocumentHistory;
  AIsDeferred: Boolean;
begin
  AOptions := ChangeOptionsCore(AChangedProperty, AIsDeferred);
  if not IsSuspendUpdateOptions then
  begin
    AHistory := DocumentModel.History;
    if AHistory <> nil then
      AHistory.BeginTransaction;
  end;
  if AOptions <> nil then
  begin
    ReplaceInfo(AOptions, GetBatchUpdateChangeActions);
    if not AIsDeferred then
      AOptions.Free;
  end;
end;

procedure TdxPropertiesBase<T>.BeginPropertiesUpdate;
begin
  Inc(FSuspendCount);
  if FSuspendCount > 1 then
    Exit;
  DocumentModel.History.BeginTransaction;
  BeginUpdate;
  FDeferredChanges := [];
end;

function TdxPropertiesBase<T>.ChangeOptionsCore(AChangedProperty: TdxProperties; out AIsDeferred: Boolean): T;
var
  AAccessor: TdxBoolPropertyAccessor<T>;
begin
  if AccessorTable.TryGetValue(AChangedProperty, AAccessor) then
    Result := ChangePropertiesOptions(AAccessor, AIsDeferred)
  else
  begin
    raise Exception.Create('ThrowInternalException');
    Result := Default(T);
  end;
end;

function TdxPropertiesBase<T>.ChangePropertiesOptions(AAccessor: TdxBoolPropertyAccessor<T>; out AIsDeferred: Boolean): T;
begin
  Result := GetInfoForModification(AIsDeferred);
  if not AAccessor.Get(Result) then
    AAccessor.&Set(Result, True)
  else
  begin
    if not AIsDeferred then
    begin
      Result.Free;
      AIsDeferred := True; 
    end;

    Result := Default(T);
  end;
end;

function TdxPropertiesBase<T>.ResetPropertiesOptions(AAccessor: TdxBoolPropertyAccessor<T>; out AIsDeferred: Boolean): T;
begin
  Result := GetInfoForModification(AIsDeferred);
  if AAccessor.Get(Result) then
    AAccessor.&Set(Result, False)
  else
  begin
    if not AIsDeferred then
    begin
      Result.Free;
      AIsDeferred := True; 
    end;

    Result := Default(T);
  end;
end;

procedure TdxPropertiesBase<T>.EndChanging;
var
  AHistory: TdxDocumentHistory;
begin
  if not IsSuspendUpdateOptions then
  begin
    AHistory := DocumentModel.History;
    if (AHistory <> nil) and (AHistory.Transaction <> nil) then
      AHistory.EndTransaction;
  end;
end;

procedure TdxPropertiesBase<T>.EndPropertiesUpdate;
var
  AHistory: TdxDocumentHistory;
begin
  if IsSuspendUpdateOptions then
  begin
    Dec(FSuspendCount);
    if FSuspendCount > 0 then
      Exit;
  end;
  if FDeferredChanges <> [] then
      inherited ApplyChanges(FDeferredChanges);
  FDeferredChanges := [];
  EndUpdate;
  AHistory := DocumentModel.History;
  Assert(AHistory.Transaction <> nil);
  AHistory.EndTransaction;
end;

function TdxPropertiesBase<T>.GetIsSuspendUpdateOptions: Boolean;
begin
  Result := FSuspendCount > 0;
end;

function TdxPropertiesBase<T>.GetPieceTable: TObject;
begin
  Result := TdxPieceTable(DocumentModelPart);
end;

function TdxPropertiesBase<T>.ResetOptionsCore(AChangedProperty: TdxProperties; out AIsDeferred: Boolean): T;
var
  AAccessor: TdxBoolPropertyAccessor<T>;
begin
  if AccessorTable.TryGetValue(AChangedProperty, AAccessor) then
    Result := ResetPropertiesOptions(AAccessor, AIsDeferred)
  else
    raise Exception.Create('ThrowInternalException');
end;

procedure TdxPropertiesBase<T>.ResetPropertyUse(AChangedProperty: TdxProperties);
var
  AOptions: T;
  AIsDeferred: Boolean;
begin
  AOptions := ResetOptionsCore(AChangedProperty, AIsDeferred);
  ReplaceInfo(AOptions, GetBatchUpdateChangeActions);
  if not AIsDeferred then
    AOptions.Free;
end;

{ TdxTablePropertiesOptions }


constructor TdxTablePropertiesOptions.Create(AValue: Integer);
begin
  inherited Create;
  FValue := AValue;
end;


function TdxTablePropertiesOptions.GetValue(AMask: Integer): Boolean;
begin
  Result := (FValue and AMask) <> 0;
end;

procedure TdxTablePropertiesOptions.SetValue(AMask: Integer; ABitVal: Boolean);
begin
  if ABitVal then
    FValue := FValue or AMask
  else
    FValue := FValue and not AMask;
end;

procedure TdxTablePropertiesOptions.CopyFrom(const AOptions: TdxTablePropertiesOptions);
begin
  FValue := AOptions.FValue;
end;

function TdxTablePropertiesOptions.Clone: TdxTablePropertiesOptions;
begin
  Result := TdxTablePropertiesOptions.Create;
  Result.CopyFrom(Self);
end;

function TdxTablePropertiesOptions.Equals(AObject: TObject): Boolean;
begin
  if AObject is TdxTablePropertiesOptions then
    Result := Value = TdxTablePropertiesOptions(AObject).Value
  else
    Result :=  False;
end;

function TdxTablePropertiesOptions.GetHashCode: Integer;
begin
  Result := Value;
end;

function TdxTablePropertiesOptions.GetUseAvoidDoubleBorders: Boolean;
begin
  Result := GetValue(MaskUseAvoidDoubleBorders);
end;

function TdxTablePropertiesOptions.GetUseBackgroundColor: Boolean;
begin
  Result := GetValue(MaskUseBackgroundColor);
end;

function TdxTablePropertiesOptions.GetUseBorders: Boolean;
begin
  Result := GetValue(MaskUseBorders);
end;

function TdxTablePropertiesOptions.GetUseBottomBorder: Boolean;
begin
  Result := GetValue(MaskUseBottomBorder);
end;

function TdxTablePropertiesOptions.GetUseBottomMargin: Boolean;
begin
  Result := GetValue(MaskUseBottomMargin);
end;

function TdxTablePropertiesOptions.GetUseCellSpacing: Boolean;
begin
  Result := GetValue(MaskUseCellSpacing);
end;

function TdxTablePropertiesOptions.GetUseFloatingPosition: Boolean;
begin
  Result := GetValue(MaskUseFloatingPosition);
end;

function TdxTablePropertiesOptions.GetUseInsideHorizontalBorder: Boolean;
begin
  Result := GetValue(MaskUseInsideHorizontalBorder);
end;

function TdxTablePropertiesOptions.GetUseInsideVerticalBorder: Boolean;
begin
  Result := GetValue(MaskUseInsideVerticalBorder);
end;

function TdxTablePropertiesOptions.GetUseIsTableOverlap: Boolean;
begin
  Result := GetValue(MaskUseIsTableOverlap);
end;

function TdxTablePropertiesOptions.GetUseLeftBorder: Boolean;
begin
  Result := GetValue(MaskUseLeftBorder);
end;

function TdxTablePropertiesOptions.GetUseLeftMargin: Boolean;
begin
  Result := GetValue(MaskUseLeftMargin);
end;

function TdxTablePropertiesOptions.GetUsePreferredWidth: Boolean;
begin
  Result := GetValue(MaskUsePreferredWidth);
end;

function TdxTablePropertiesOptions.GetUseRightBorder: Boolean;
begin
  Result := GetValue(MaskUseRightBorder);
end;

function TdxTablePropertiesOptions.GetUseRightMargin: Boolean;
begin
  Result := GetValue(MaskUseRightMargin);
end;

function TdxTablePropertiesOptions.GetUseTableAlignment: Boolean;
begin
  Result := GetValue(MaskUseTableAlignment);
end;

function TdxTablePropertiesOptions.GetUseTableIndent: Boolean;
begin
  Result := GetValue(MaskUseTableIndent);
end;

function TdxTablePropertiesOptions.GetUseTableLayout: Boolean;
begin
  Result := GetValue(MaskUseTableLayout);
end;

function TdxTablePropertiesOptions.GetUseTableLook: Boolean;
begin
  Result := GetValue(MaskUseTableLook);
end;

function TdxTablePropertiesOptions.GetUseTableStyleColBandSize: Boolean;
begin
  Result := GetValue(MaskUseTableStyleColBandSize);
end;

function TdxTablePropertiesOptions.GetUseTableStyleRowBandSize: Boolean;
begin
  Result := GetValue(MaskUseTableStyleRowBandSize);
end;

function TdxTablePropertiesOptions.GetUseTopBorder: Boolean;
begin
  Result := GetValue(MaskUseTopBorder);
end;

function TdxTablePropertiesOptions.GetUseTopMargin: Boolean;
begin
  Result := GetValue(MaskUseTopMargin);
end;

procedure TdxTablePropertiesOptions.SetUseAvoidDoubleBorders(const Value: Boolean);
begin
  SetValue(MaskUseAvoidDoubleBorders, Value);
end;

procedure TdxTablePropertiesOptions.SetUseBackgroundColor(const Value: Boolean);
begin
  SetValue(MaskUseBackgroundColor, Value);
end;

procedure TdxTablePropertiesOptions.SetUseBorders(const Value: Boolean);
begin
  SetValue(MaskUseBorders, Value);
end;

procedure TdxTablePropertiesOptions.SetUseBottomBorder(const Value: Boolean);
begin
  SetValue(MaskUseBottomBorder, Value);
end;

procedure TdxTablePropertiesOptions.SetUseBottomMargin(const Value: Boolean);
begin
  SetValue(MaskUseBottomMargin, Value);
end;

procedure TdxTablePropertiesOptions.SetUseCellSpacing(const Value: Boolean);
begin
  SetValue(MaskUseCellSpacing, Value);
end;

procedure TdxTablePropertiesOptions.SetUseFloatingPosition(const Value: Boolean);
begin
  SetValue(MaskUseFloatingPosition, Value);
end;

procedure TdxTablePropertiesOptions.SetUseInsideHorizontalBorder(const Value: Boolean);
begin
  SetValue(MaskUseInsideHorizontalBorder, Value);
end;

procedure TdxTablePropertiesOptions.SetUseInsideVerticalBorder(const Value: Boolean);
begin
  SetValue(MaskUseInsideVerticalBorder, Value);
end;

procedure TdxTablePropertiesOptions.SetUseIsTableOverlap(const Value: Boolean);
begin
  SetValue(MaskUseIsTableOverlap, Value);
end;

procedure TdxTablePropertiesOptions.SetUseLeftBorder(const Value: Boolean);
begin
  SetValue(MaskUseLeftBorder, Value);
end;

procedure TdxTablePropertiesOptions.SetUseLeftMargin(const Value: Boolean);
begin
  SetValue(MaskUseLeftMargin, Value);
end;

procedure TdxTablePropertiesOptions.SetUsePreferredWidth(const Value: Boolean);
begin
  SetValue(MaskUsePreferredWidth, Value);
end;

procedure TdxTablePropertiesOptions.SetUseRightBorder(const Value: Boolean);
begin
  SetValue(MaskUseRightBorder, Value);
end;

procedure TdxTablePropertiesOptions.SetUseRightMargin(const Value: Boolean);
begin
  SetValue(MaskUseRightMargin, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTableAlignment(const Value: Boolean);
begin
  SetValue(MaskUseTableAlignment, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTableIndent(const Value: Boolean);
begin
  SetValue(MaskUseTableIndent, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTableLayout(const Value: Boolean);
begin
  SetValue(MaskUseTableLayout, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTableLook(const Value: Boolean);
begin
  SetValue(MaskUseTableLook, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTableStyleColBandSize(const Value: Boolean);
begin
  SetValue(MaskUseTableStyleColBandSize, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTableStyleRowBandSize(const Value: Boolean);
begin
  SetValue(MaskUseTableStyleRowBandSize, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTopBorder(const Value: Boolean);
begin
  SetValue(MaskUseTopBorder, Value);
end;

procedure TdxTablePropertiesOptions.SetUseTopMargin(const Value: Boolean);
begin
  SetValue(MaskUseTopMargin, Value);
end;

class function TdxTablePropertiesOptions.GetOptionsUseLeftBorder(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseLeftBorder;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseLeftBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseLeftBorder := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseRightBorder(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseRightBorder;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseRightBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseRightBorder := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTopBorder(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTopBorder;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTopBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTopBorder := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseBottomBorder(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseBottomBorder;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseBottomBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseBottomBorder := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseInsideVerticalBorder(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseInsideVerticalBorder;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseInsideVerticalBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseInsideVerticalBorder := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseInsideHorizontalBorder(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseInsideHorizontalBorder;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseInsideHorizontalBorder(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseInsideHorizontalBorder := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseLeftMargin(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseLeftMargin;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseLeftMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseLeftMargin := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseRightMargin(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseRightMargin;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseRightMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseRightMargin := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTopMargin(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTopMargin;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTopMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTopMargin := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseBottomMargin(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseBottomMargin;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseBottomMargin(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseBottomMargin := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseCellSpacing(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseCellSpacing;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseCellSpacing(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseCellSpacing := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUsePreferredWidth(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UsePreferredWidth;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUsePreferredWidth(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UsePreferredWidth := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTableIndent(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTableIndent;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTableIndent(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTableIndent := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTableLayout(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTableLayout;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTableLayout(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTableLayout := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTableAlignment(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTableAlignment;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTableAlignment(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTableAlignment := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTableLook(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTableLook;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTableLook(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTableLook := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTableOverlap(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseIsTableOverlap;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTableOverlap(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseIsTableOverlap := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTableStyleColBandSize(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTableStyleColBandSize;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTableStyleColBandSize(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTableStyleColBandSize := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseTableStyleRowBandSize(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseTableStyleRowBandSize;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseTableStyleRowBandSize(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseTableStyleRowBandSize := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseFloatingPosition(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseFloatingPosition;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseFloatingPosition(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseFloatingPosition := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseBackgroundColor(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseBackgroundColor;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseBackgroundColor(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseBackgroundColor := Value;
end;

class function TdxTablePropertiesOptions.GetOptionsUseAvoidDoubleBorders(AOptions: TdxTablePropertiesOptions): Boolean;
begin
  Result := AOptions.UseAvoidDoubleBorders;
end;

class procedure TdxTablePropertiesOptions.SetOptionsUseAvoidDoubleBorders(AOptions: TdxTablePropertiesOptions; const Value: Boolean);
begin
  AOptions.UseAvoidDoubleBorders := Value;
end;

{ TdxBoolPropertyAccessor<T> }

constructor TdxBoolPropertyAccessor<T>.Create(AGet: TdxGetOptionValueDelegate; ASet: TdxSetOptionValueDelegate);
begin
  inherited Create;
  FGet := AGet;
  FSet := ASet;
end;

{ TdxTableFloatingPositionInfo }

function TdxTableFloatingPositionInfo.Clone: TdxTableFloatingPositionInfo;
begin
  Result := TdxTableFloatingPositionInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxTableFloatingPositionInfo.CopyFrom(const AInfo: TdxTableFloatingPositionInfo);
begin
  BottomFromText := AInfo.BottomFromText;
  LeftFromText := AInfo.LeftFromText;
  TopFromText := AInfo.TopFromText;
  RightFromText := AInfo.RightFromText;
  TableHorizontalPosition := AInfo.TableHorizontalPosition;
  TableVerticalPosition := AInfo.TableVerticalPosition;
  HorizontalAlign := AInfo.HorizontalAlign;
  VerticalAlign := AInfo.VerticalAlign;
  HorizontalAnchor := AInfo.HorizontalAnchor;
  VerticalAnchor := AInfo.VerticalAnchor;
  TextWrapping := AInfo.TextWrapping;
end;

function TdxTableFloatingPositionInfo.Equals(AObject: TObject): Boolean;
var
  APos: TdxTableFloatingPositionInfo;
begin
  if AObject is TdxTableFloatingPositionInfo then
  begin
    APos := TdxTableFloatingPositionInfo(AObject);
    Result := (BottomFromText = APos.BottomFromText) and
      (LeftFromText = APos.LeftFromText) and
      (TopFromText = APos.TopFromText) and
      (RightFromText = APos.RightFromText) and
      (TableHorizontalPosition = APos.TableHorizontalPosition) and
      (TableVerticalPosition = APos.TableVerticalPosition) and
      (HorizontalAlign = APos.HorizontalAlign) and
      (VerticalAlign = APos.VerticalAlign) and
      (HorizontalAnchor = APos.HorizontalAnchor) and
      (VerticalAnchor = APos.VerticalAnchor) and
      (TextWrapping = APos.TextWrapping);
  end
  else
    Result := False;
end;

function TdxTableFloatingPositionInfo.GetHashCode: Integer;
begin
  Result := inherited GetHashCode;
end;

function TdxTableFloatingPositionInfo.IsHorizontalAbsolutePositionUse: Boolean;
begin
  Result := HorizontalAlign = TdxHorizontalAlignMode.None;
end;

function TdxTableFloatingPositionInfo.IsVerticalAbsolutePositionUse: Boolean;
begin
  Result := VerticalAlign = TdxVerticalAlignMode.None;
end;

{ TdxTableFloatingPosition }

constructor TdxTableFloatingPosition.Create(APieceTable: TObject; const AOwner: IdxPropertiesContainer);
begin
  inherited Create(APieceTable as TdxPieceTable);
  FOwner := AOwner;
end;

function TdxTableFloatingPosition.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.BatchUpdate);
end;

function TdxTableFloatingPosition.GetBottomFromText: Integer;
begin
  Result := Info.BottomFromText;
end;

function TdxTableFloatingPosition.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableFloatingPositionInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TableFloatingPositionInfoCache;
end;

function TdxTableFloatingPosition.GetHorizontalAlign: TdxHorizontalAlignMode;
begin
  Result := Info.HorizontalAlign;
end;

function TdxTableFloatingPosition.GetHorizontalAnchor: TdxHorizontalAnchorTypes;
begin
  Result := Info.HorizontalAnchor;
end;

function TdxTableFloatingPosition.GetLeftFromText: Integer;
begin
  Result := Info.LeftFromText;
end;

function TdxTableFloatingPosition.GetRightFromText: Integer;
begin
  Result := Info.RightFromText;
end;

function TdxTableFloatingPosition.GetTableHorizontalPosition: Integer;
begin
  Result := Info.TableHorizontalPosition;
end;

function TdxTableFloatingPosition.GetTableVerticalPosition: Integer;
begin
  Result := Info.TableVerticalPosition;
end;

function TdxTableFloatingPosition.GetTextWrapping: TdxTextWrapping;
begin
  Result := Info.TextWrapping;
end;

function TdxTableFloatingPosition.GetTopFromText: Integer;
begin
  Result := Info.TopFromText;
end;

function TdxTableFloatingPosition.GetVerticalAlign: TdxVerticalAlignMode;
begin
  Result := Info.VerticalAlign;
end;

function TdxTableFloatingPosition.GetVerticalAnchor: TdxVerticalAnchorTypes;
begin
  Result := Info.VerticalAnchor;
end;

procedure TdxTableFloatingPosition.InnerSetBottomFromText(const Value: Integer);
begin
  if Value = Info.BottomFromText then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetBottomFromText, Value);
end;

procedure TdxTableFloatingPosition.InnerSetHorizontalAlign(const Value: TdxHorizontalAlignMode);
begin
  if Value = Info.HorizontalAlign then
    NotifyFakeAssign
  else
    SetPropertyValue<TdxHorizontalAlignMode>(SetHorizontalAlign, Value);
end;

procedure TdxTableFloatingPosition.InnerSetHorizontalAnchor(const Value: TdxHorizontalAnchorTypes);
begin
  if Value = Info.HorizontalAnchor then
    NotifyFakeAssign
  else
    SetPropertyValue<TdxHorizontalAnchorTypes>(SetHorizontalAnchor, Value);
end;

procedure TdxTableFloatingPosition.InnerSetLeftFromText(const Value: Integer);
begin
  if Value = Info.LeftFromText then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetLeftFromText, Value);
end;

procedure TdxTableFloatingPosition.InnerSetRightFromText(const Value: Integer);
begin
  if Value = Info.RightFromText then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetRightFromText, Value);
end;

procedure TdxTableFloatingPosition.InnerSetTableHorizontalPosition(const Value: Integer);
begin
  if Value = Info.TableHorizontalPosition then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetTableHorizontalPosition, Value);
end;

procedure TdxTableFloatingPosition.InnerSetTableVerticalPosition(const Value: Integer);
begin
  if Value = Info.TableVerticalPosition then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetTableVerticalPosition, Value);
end;

procedure TdxTableFloatingPosition.InnerSetTextWrapping(const Value: TdxTextWrapping);
begin
  if Value = Info.TextWrapping then
    NotifyFakeAssign
  else
    SetPropertyValue<TdxTextWrapping>(SetTextWrapping, Value);
end;

procedure TdxTableFloatingPosition.InnerSetTopFromText(const Value: Integer);
begin
  if Value = Info.TopFromText then
    NotifyFakeAssign
  else
    SetPropertyValue<Integer>(SetTopFromText, Value);
end;

procedure TdxTableFloatingPosition.InnerSetVerticalAlign(const Value: TdxVerticalAlignMode);
begin
  if Value = Info.VerticalAlign then
      NotifyFakeAssign
  else
    SetPropertyValue<TdxVerticalAlignMode>(SetVerticalAlign, Value);
end;

procedure TdxTableFloatingPosition.InnerSetVerticalAnchor(const Value: TdxVerticalAnchorTypes);
begin
  if Value = Info.VerticalAnchor then
    NotifyFakeAssign
  else
    SetPropertyValue<TdxVerticalAnchorTypes>(SetVerticalAnchor, Value);
end;

function TdxTableFloatingPosition.IsHorizontalRelativePositionUse: Boolean;
begin
  Result := HorizontalAlign <> TdxHorizontalAlignMode.None;
end;

function TdxTableFloatingPosition.IsVerticalRelativePositionUse: Boolean;
begin
  Result := VerticalAlign <> TdxVerticalAlignMode.None;
end;

procedure TdxTableFloatingPosition.OnBeginAssign;
begin
  inherited OnBeginAssign;
  if Owner <> nil then
    Owner.BeginChanging(TdxProperties.TableFloatingPosition);
end;

procedure TdxTableFloatingPosition.OnEndAssign;
begin
  if Owner <> nil then
    Owner.EndChanging;
  inherited OnEndAssign;
end;

function TdxTableFloatingPosition.SetBottomFromText(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.BottomFromText := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.BottomFromText);
end;

function TdxTableFloatingPosition.SetHorizontalAlign(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: TdxHorizontalAlignMode): TdxDocumentModelChangeActions;
begin
  AInfo.HorizontalAlign := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.HorizontalAlign);
end;

function TdxTableFloatingPosition.SetHorizontalAnchor(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: TdxHorizontalAnchorTypes): TdxDocumentModelChangeActions;
begin
  AInfo.HorizontalAnchor := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.HorizontalAnchor);
end;

function TdxTableFloatingPosition.SetLeftFromText(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.LeftFromText := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.LeftFromText);
end;

function TdxTableFloatingPosition.SetRightFromText(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.RightFromText := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.RightFromText);
end;

function TdxTableFloatingPosition.SetTableHorizontalPosition(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.TableHorizontalPosition := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.TableHorizontalPosition);
end;

function TdxTableFloatingPosition.SetTableVerticalPosition(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.TableVerticalPosition := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.TableVerticalPosition);
end;

function TdxTableFloatingPosition.SetTextWrapping(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: TdxTextWrapping): TdxDocumentModelChangeActions;
begin
  AInfo.TextWrapping := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.TextWrapping);
end;

function TdxTableFloatingPosition.SetTopFromText(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.TopFromText := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.TopFromText);
end;

function TdxTableFloatingPosition.SetVerticalAlign(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: TdxVerticalAlignMode): TdxDocumentModelChangeActions;
begin
  AInfo.VerticalAlign := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.VerticalAlign);
end;

function TdxTableFloatingPosition.SetVerticalAnchor(const AInfo: TdxTableFloatingPositionInfo;
  const AValue: TdxVerticalAnchorTypes): TdxDocumentModelChangeActions;
begin
  AInfo.VerticalAnchor := AValue;
  Result := TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(TdxTableFloatingPositionChangeType.VerticalAnchor);
end;

{ TdxTableFloatingPositionInfoCache }

function TdxTableFloatingPositionInfoCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableFloatingPositionInfo;
begin
  Result := TdxTableFloatingPositionInfo.Create;
  Result.HorizontalAlign := TdxHorizontalAlignMode.None;
  Result.VerticalAlign := TdxVerticalAlignMode.None;
  Result.HorizontalAnchor := TdxHorizontalAnchorTypes.Page;
  Result.VerticalAnchor := TdxVerticalAnchorTypes.Page;
  Result.TableHorizontalPosition := 0;
  Result.TableVerticalPosition := 0;
  Result.TextWrapping := TdxTextWrapping.Never;
end;

{ TdxTableGeneralSettingsInfo }

procedure TdxTableGeneralSettingsInfo.CopyFrom(const AInfo: TdxTableGeneralSettingsInfo);
begin
  TableStyleColBandSize := AInfo.TableStyleColBandSize;
  TableStyleRowBandSize := AInfo.TableStyleRowBandSize;
  IsTableOverlap := AInfo.IsTableOverlap;
  TableLayout := AInfo.TableLayout;
  TableLook := AInfo.TableLook;
  BackgroundColor := AInfo.BackgroundColor;
  TableAlignment := AInfo.TableAlignment;
  AvoidDoubleBorders := AInfo.AvoidDoubleBorders;
end;

function TdxTableGeneralSettingsInfo.Clone: TdxTableGeneralSettingsInfo;
begin
  Result := TdxTableGeneralSettingsInfo.Create;
  Result.CopyFrom(Self);
end;

function TdxTableGeneralSettingsInfo.Equals(AObject: TObject): Boolean;
var
  AInfo: TdxTableGeneralSettingsInfo;
begin
  if AObject is TdxTableGeneralSettingsInfo then
  begin
    AInfo := TdxTableGeneralSettingsInfo(AObject);
    Result := (TableLayout = AInfo.TableLayout) and
      (TableLook = AInfo.TableLook) and
      (IsTableOverlap = AInfo.IsTableOverlap) and
      (TableStyleColBandSize = AInfo.TableStyleColBandSize) and
      (TableStyleRowBandSize = AInfo.TableStyleRowBandSize) and
      (BackgroundColor = AInfo.BackgroundColor) and
      (TableAlignment = AInfo.TableAlignment) and
      (AvoidDoubleBorders = AInfo.AvoidDoubleBorders);
  end
  else
    Result := False;
end;

{ TdxTableGeneralSettingsChangeActionsCalculator }

class function TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(
  AChange: TdxTableGeneralSettingsChangeType): TdxDocumentModelChangeActions;
const
  ATableGeneralSettingsChangeActionsTable: array[TdxTableGeneralSettingsChangeType] of TdxDocumentModelChangeActions = (
    [], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout] 
  );
begin
  Result := ATableGeneralSettingsChangeActionsTable[AChange];
end;

{ TdxTableGeneralSettingsInfoCache }

function TdxTableGeneralSettingsInfoCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableGeneralSettingsInfo;
begin
  Result := TdxTableGeneralSettingsInfo.Create;
  Result.TableStyleColBandSize := 1;
  Result.TableStyleRowBandSize := 1;
  Result.IsTableOverlap := True;
  Result.TableLayout := TdxTableLayoutType.Autofit;
  Result.TableLook := TdxTableLookTypes.None;
  Result.BackgroundColor := TdxColor.Empty; //DXColor.Empty;
  Result.TableAlignment := TdxTableRowAlignment.Left;
end;

{ TdxTableGeneralSettings }

procedure TdxTableGeneralSettings.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
begin
  Owner.ApplyChanges(AChangeActions);
end;

procedure TdxTableGeneralSettings.CopyFrom(const ANewSettings: TdxTableGeneralSettings);
begin
  Owner.BeginPropertiesUpdate;
  try
    BeginUpdate;
    try
      TableLayout := ANewSettings.TableLayout;
      TableLook := ANewSettings.TableLook;
      IsTableOverlap := ANewSettings.IsTableOverlap;
      TableStyleColumnBandSize := ANewSettings.TableStyleColumnBandSize;
      TableStyleRowBandSize := ANewSettings.TableStyleRowBandSize;
      BackgroundColor := ANewSettings.BackgroundColor;
      TableAlignment := ANewSettings.TableAlignment;
      AvoidDoubleBorders := ANewSettings.AvoidDoubleBorders;
    finally
      EndUpdate;
    end;
  finally
    Owner.EndPropertiesUpdate;
  end;
end;

procedure TdxTableGeneralSettings.BeginChanging(AChangedProperty: TdxProperties);
begin
  DocumentModel.BeginUpdate;
  Owner.BeginChanging(AChangedProperty);
end;

constructor TdxTableGeneralSettings.Create(APieceTable: TObject; const AOwner: IdxPropertiesContainer);
begin
  inherited Create(APieceTable as TdxPieceTable);
  Assert(AOwner <> nil);
  FOwner := AOwner;
end;

procedure TdxTableGeneralSettings.EndChanging;
begin
  Owner.EndChanging;
  DocumentModel.EndUpdate;
end;

function TdxTableGeneralSettings.GetAvoidDoubleBorders: Boolean;
begin
  Result := Info.AvoidDoubleBorders;
end;

function TdxTableGeneralSettings.GetBackgroundColor: TColor;
begin
  Result := Info.BackgroundColor;
end;

function TdxTableGeneralSettings.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.BatchUpdate);
end;

function TdxTableGeneralSettings.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableGeneralSettingsInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TableGeneralSettingsInfoCache;
end;

function TdxTableGeneralSettings.GetIsTableOverlap: Boolean;
begin
  Result := Info.IsTableOverlap;
end;

function TdxTableGeneralSettings.GetTableAlignment: TdxTableRowAlignment;
begin
  Result := Info.TableAlignment;
end;

function TdxTableGeneralSettings.GetTableLayout: TdxTableLayoutType;
begin
  Result := Info.TableLayout;
end;

function TdxTableGeneralSettings.GetTableLook: TdxTableLookTypes;
begin
  Result := Info.TableLook;
end;

function TdxTableGeneralSettings.GetTableStyleColumnBandSize: Integer;
begin
  Result := Info.TableStyleColBandSize;
end;

function TdxTableGeneralSettings.GetTableStyleRowBandSize: Integer;
begin
  Result := Info.TableStyleRowBandSize;
end;

procedure TdxTableGeneralSettings.InnerSetAvoidDoubleBorders(const Value: Boolean);
begin
  BeginChanging(TdxProperties.AvoidDoubleBorders);
  try
    if Info.AvoidDoubleBorders = Value then
      Exit;
    SetPropertyValue<Boolean>(SetAvoidDoubleBorders, Value);
  finally
    EndChanging;
  end;
end;

procedure TdxTableGeneralSettings.InnerSetBackgroundColor(const Value: TColor);
begin
  BeginChanging(TdxProperties.BackgroundColor);
  try
    if Info.BackgroundColor = Value then
      Exit;
    SetPropertyValue<Integer>(SetBackgroundColor, Ord(Value));
  finally
    EndChanging;
  end;
end;

procedure TdxTableGeneralSettings.InnerSetTableAlignment(const Value: TdxTableRowAlignment);
begin
  BeginChanging(TdxProperties.TableAlignment);
  try
    if Info.TableAlignment = Value then
      Exit;
    SetPropertyValue<Integer>(SetTableAlignment, Ord(Value));
  finally
    EndChanging;
  end;
end;

procedure TdxTableGeneralSettings.InnerSetTableLayout(const Value: TdxTableLayoutType);
begin
  BeginChanging(TdxProperties.TableLayout);
  try
    if Info.TableLayout = Value then
      Exit;
    SetPropertyValue<Integer>(SetTableLayout, Ord(Value));
  finally
    EndChanging;
  end;
end;

procedure TdxTableGeneralSettings.InnerSetTableLook(const Value: TdxTableLookTypes);
begin
  BeginChanging(TdxProperties.TableLook);
  try
    if Info.TableLook = Value then
      Exit;
    SetPropertyValue<Integer>(SetTableLook, Ord(Value));
  finally
    EndChanging;
  end;
end;

procedure TdxTableGeneralSettings.InnerSetTableStyleColumnBandSize(const Value: Integer);
begin
  if Value < 0 then
    Assert(False);
  BeginChanging(TdxProperties.TableStyleColumnBandSize);
  try
    if Info.TableStyleColBandSize = Value then
      Exit;
    SetPropertyValue<Integer>(SetTableStyleColumnBandSize, Value);
  finally
    EndChanging;
  end;
end;

procedure TdxTableGeneralSettings.InnerSetTableStyleRowBandSize(const Value: Integer);
begin
  if Value < 0 then
      Assert(False);
  BeginChanging(TdxProperties.TableStyleRowBandSize);
  try
    if Info.TableStyleRowBandSize = Value then
      Exit;
    SetPropertyValue<Integer>(SetTableStyleRowBandSize, Value);
  finally
    EndChanging;
  end;
end;

procedure TdxTableGeneralSettings.RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
begin
  Owner.RaiseObtainAffectedRange(AArgs);
end;

function TdxTableGeneralSettings.SetAvoidDoubleBorders(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  ASettings.AvoidDoubleBorders := AValue;
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.AvoidDoubleBorders);
end;

function TdxTableGeneralSettings.SetBackgroundColor(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  ASettings.BackgroundColor := AValue;
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.BackgroundColor);
end;

procedure TdxTableGeneralSettings.InnerSetIsTableOverlap(const Value: Boolean);
begin
  BeginChanging(TdxProperties.IsTableOverlap);
  try
    if Info.IsTableOverlap = Value then
      Exit;
    SetPropertyValue<Boolean>(SetTableOverlap, Value);
  finally
    EndChanging;
  end;
end;

function TdxTableGeneralSettings.SetTableAlignment(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  ASettings.TableAlignment := TdxTableRowAlignment(AValue);
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.TableAlignment);
end;

function TdxTableGeneralSettings.SetTableLayout(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  ASettings.TableLayout := TdxTableLayoutType(AValue);
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.TableLayout);
end;

function TdxTableGeneralSettings.SetTableLook(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  ASettings.TableLook := TdxTableLookTypes(AValue);
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.TableLook);
end;

function TdxTableGeneralSettings.SetTableOverlap(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  ASettings.IsTableOverlap := AValue;
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.IsTableOverlap);
end;

function TdxTableGeneralSettings.SetTableStyleColumnBandSize(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  ASettings.TableStyleColBandSize := AValue;
  Result := TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.TableStyleColumnBandSize);
end;

function TdxTableGeneralSettings.SetTableStyleRowBandSize(const ASettings: TdxTableGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  ASettings.TableStyleRowBandSize := AValue;
  TdxTableGeneralSettingsChangeActionsCalculator.CalculateChangeActions(TdxTableGeneralSettingsChangeType.TableStyleRowBandSize);
end;

{ TdxTableProperties }

constructor TdxTableProperties.Create(const ADocumentModelPart: TdxDocumentModelPart);
begin
  inherited Create(ADocumentModelPart as TdxPieceTable);
  FCellMargins := TdxCellMargins.Create(PieceTable, Self);
  FCellSpacing := TdxCellSpacing.Create(PieceTable, Self);
  FIndent := TdxTableIndent.Create(PieceTable, Self);
  FPreferredWidth := TdxPreferredWidth.Create(PieceTable, Self);
  FGeneralSettings := TdxTableGeneralSettings.Create(PieceTable, Self);
  FBorders := TdxTableBorders.Create(PieceTable, Self);
  FFloatingPosition := TdxTableFloatingPosition.Create(PieceTable, Self);
end;

destructor TdxTableProperties.Destroy;
begin
  FFloatingPosition.Free;
  FBorders.Free;
  FGeneralSettings.Free;
  FPreferredWidth.Free;
  FIndent.Free;
  FCellSpacing.Free;
  FCellMargins.Free;
  inherited Destroy;
end;

class constructor TdxTableProperties.Initialize;
begin
  FAccessorTable := CreateAccessorTable;
end;

class destructor TdxTableProperties.Finalize;
begin
  FreeAndNil(FAccessorTable);
end;

class function TdxTableProperties.CreateAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTablePropertiesOptions>>;
begin
  Result := TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTablePropertiesOptions>>.Create;
  Result.Add(TdxProperties.LeftBorder, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseLeftBorder, TdxTablePropertiesOptions.SetOptionsUseLeftBorder));
  Result.Add(TdxProperties.RightBorder, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseRightBorder, TdxTablePropertiesOptions.SetOptionsUseRightBorder));
  Result.Add(TdxProperties.TopBorder, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTopBorder, TdxTablePropertiesOptions.SetOptionsUseTopBorder));
  Result.Add(TdxProperties.BottomBorder, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseBottomBorder, TdxTablePropertiesOptions.SetOptionsUseBottomBorder));
  Result.Add(TdxProperties.InsideHorizontalBorder, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseInsideHorizontalBorder, TdxTablePropertiesOptions.SetOptionsUseInsideHorizontalBorder));
  Result.Add(TdxProperties.InsideVerticalBorder, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseInsideVerticalBorder, TdxTablePropertiesOptions.SetOptionsUseInsideVerticalBorder));

  Result.Add(TdxProperties.LeftMargin, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseLeftMargin, TdxTablePropertiesOptions.SetOptionsUseLeftMargin));
  Result.Add(TdxProperties.RightMargin, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseRightMargin, TdxTablePropertiesOptions.SetOptionsUseRightMargin));
  Result.Add(TdxProperties.TopMargin, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTopMargin, TdxTablePropertiesOptions.SetOptionsUseTopMargin));
  Result.Add(TdxProperties.BottomMargin, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseBottomMargin, TdxTablePropertiesOptions.SetOptionsUseBottomMargin));

  Result.Add(TdxProperties.CellSpacing, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseCellSpacing, TdxTablePropertiesOptions.SetOptionsUseCellSpacing));
  Result.Add(TdxProperties.PreferredWidth, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUsePreferredWidth, TdxTablePropertiesOptions.SetOptionsUsePreferredWidth));
  Result.Add(TdxProperties.TableIndent, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTableIndent, TdxTablePropertiesOptions.SetOptionsUseTableIndent));
  Result.Add(TdxProperties.TableLayout, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTableLayout, TdxTablePropertiesOptions.SetOptionsUseTableLayout));
  Result.Add(TdxProperties.TableAlignment, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTableAlignment, TdxTablePropertiesOptions.SetOptionsUseTableAlignment));
  Result.Add(TdxProperties.TableLook, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTableLook, TdxTablePropertiesOptions.SetOptionsUseTableLook));
  Result.Add(TdxProperties.IsTableOverlap, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTableOverlap, TdxTablePropertiesOptions.SetOptionsUseTableOverlap));
  Result.Add(TdxProperties.TableStyleColumnBandSize, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTableStyleColBandSize, TdxTablePropertiesOptions.SetOptionsUseTableStyleColBandSize));
  Result.Add(TdxProperties.TableStyleRowBandSize, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseTableStyleRowBandSize, TdxTablePropertiesOptions.SetOptionsUseTableStyleRowBandSize));
  Result.Add(TdxProperties.TableFloatingPosition, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseFloatingPosition, TdxTablePropertiesOptions.SetOptionsUseFloatingPosition));
  Result.Add(TdxProperties.BackgroundColor, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseBackgroundColor, TdxTablePropertiesOptions.SetOptionsUseBackgroundColor));
  Result.Add(TdxProperties.AvoidDoubleBorders, TdxBoolPropertyAccessor<TdxTablePropertiesOptions>.Create(TdxTablePropertiesOptions.GetOptionsUseAvoidDoubleBorders, TdxTablePropertiesOptions.SetOptionsUseAvoidDoubleBorders));
end;

procedure TdxTableProperties.CopyFrom(ANewProperties: TdxTableProperties);
var
  AOptions: TdxTablePropertiesOptions;
  AContainer: IdxPropertiesContainer;
  AIsDeferredInfo: Boolean;
begin
  AContainer := Self; 
  DocumentModel.BeginUpdate;
  AContainer.BeginPropertiesUpdate;
  try
    if DocumentModel = ANewProperties.DocumentModel then
    begin
      Borders.CopyFrom(ANewProperties.Borders);
      GeneralSettings.CopyFrom(ANewProperties.GeneralSettings);
      CellMargins.CopyFrom(ANewProperties.CellMargins);
      CellSpacing.CopyFrom(ANewProperties.CellSpacing);
      TableIndent.CopyFrom(ANewProperties.TableIndent);
      PreferredWidth.CopyFrom(ANewProperties.PreferredWidth);
      FloatingPosition.CopyFrom(ANewProperties.FloatingPosition);
      AOptions := GetInfoForModification(AIsDeferredInfo);
      AOptions.CopyFrom(ANewProperties.Info);
      ReplaceInfo(AOptions, []);
      if AIsDeferredInfo then
        AOptions.Free;
    end
    else
    begin
      Borders.CopyFrom(ANewProperties.Borders);
      GeneralSettings.CopyFrom(ANewProperties.GeneralSettings.Info);
      CellMargins.CopyFrom(ANewProperties.CellMargins);
      CellSpacing.CopyFrom(ANewProperties.CellSpacing.Info);
      TableIndent.CopyFrom(ANewProperties.TableIndent.Info);
      PreferredWidth.CopyFrom(ANewProperties.PreferredWidth.Info);
      FloatingPosition.CopyFrom(ANewProperties.FloatingPosition.Info);
      Info.CopyFrom(ANewProperties.Info);
    end;
  finally
    AContainer.EndPropertiesUpdate;
    DocumentModel.EndUpdate;
  end;
end;

function TdxTableProperties.GetAvoidDoubleBorders: Boolean;
begin
  Result := GeneralSettings.AvoidDoubleBorders;
end;

function TdxTableProperties.GetBackgroundColor: TColor;
begin
  Result := GeneralSettings.BackgroundColor;
end;

function TdxTableProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [];
end;

procedure TdxTableProperties.Reset;
begin
  NotImplemented;
end;

procedure TdxTableProperties.ResetAllUse;
begin
  ReplaceInfo(GetCache(DocumentModel)[TdxTablePropertiesOptionsCache.EmptyTableFormattingOptionsItem], GetBatchUpdateChangeActions);
end;

function TdxTableProperties.GetAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTablePropertiesOptions>>;
begin
  Result := FAccessorTable;
end;

function TdxTableProperties.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTablePropertiesOptions>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TablePropertiesOptionsCache;
end;

procedure TdxTableProperties.ResetUse(AMask: Integer);
var
  ANewOptions: TdxTablePropertiesOptions;
begin
  ANewOptions := TdxTablePropertiesOptions.Create(Info.Value and not AMask);
  try
    ReplaceInfo(ANewOptions, GetBatchUpdateChangeActions);
  finally
    ANewOptions.Free;
  end;
end;

function TdxTableProperties.GetIsTableOverlap: Boolean;
begin
  Result := GeneralSettings.IsTableOverlap;
end;

function TdxTableProperties.GetTableAlignment: TdxTableRowAlignment;
begin
  Result := GeneralSettings.TableAlignment;
end;

function TdxTableProperties.GetTableLayout: TdxTableLayoutType;
begin
  Result := GeneralSettings.TableLayout;
end;

function TdxTableProperties.GetTableLook: TdxTableLookTypes;
begin
  Result := GeneralSettings.TableLook;
end;

function TdxTableProperties.GetTableStyleColBandSize: Integer;
begin
  Result := GeneralSettings.TableStyleColumnBandSize;
end;

function TdxTableProperties.GetTableStyleRowBandSize: Integer;
begin
  Result := GeneralSettings.TableStyleRowBandSize;
end;

function TdxTableProperties.GetUse(AMask: Integer): Boolean;
begin
  Result := Info.GetValue(AMask);
end;

function TdxTableProperties.GetUseAvoidDoubleBorders: Boolean;
begin
  Result := Info.UseAvoidDoubleBorders;
end;

function TdxTableProperties.GetUseBackgroundColor: Boolean;
begin
  Result := Info.UseBackgroundColor;
end;

function TdxTableProperties.GetUseBottomMargin: Boolean;
begin
  Result := Info.UseBottomMargin;
end;

function TdxTableProperties.GetUseCellSpacing: Boolean;
begin
  Result := Info.UseCellSpacing;
end;

function TdxTableProperties.GetUseFloatingPosition: Boolean;
begin
  Result := Info.UseFloatingPosition;
end;

function TdxTableProperties.GetUseIsTableOverlap: Boolean;
begin
  Result := Info.UseIsTableOverlap;
end;

function TdxTableProperties.GetUseLeftMargin: Boolean;
begin
  Result := Info.UseLeftMargin;
end;

function TdxTableProperties.GetUsePreferredWidth: Boolean;
begin
  Result := Info.UsePreferredWidth;
end;

function TdxTableProperties.GetUseRightMargin: Boolean;
begin
  Result := Info.UseRightMargin;
end;

function TdxTableProperties.GetUseTableAlignment: Boolean;
begin
  Result := Info.UseTableAlignment;
end;

function TdxTableProperties.GetUseTableIndent: Boolean;
begin
  Result := Info.UseTableIndent;
end;

function TdxTableProperties.GetUseTableLayout: Boolean;
begin
  Result := Info.UseTableLayout;
end;

function TdxTableProperties.GetUseTableLook: Boolean;
begin
  Result := Info.UseTableLook;
end;

function TdxTableProperties.GetUseTableStyleColBandSize: Boolean;
begin
  Result := Info.UseTableStyleColBandSize;
end;

function TdxTableProperties.GetUseTableStyleRowBandSize: Boolean;
begin
  Result := Info.UseTableStyleRowBandSize;
end;

function TdxTableProperties.GetUseTopMargin: Boolean;
begin
  Result := Info.UseTopMargin;
end;

procedure TdxTableProperties.SetAvoidDoubleBorders(const Value: Boolean);
begin
  GeneralSettings.AvoidDoubleBorders := Value;
end;

procedure TdxTableProperties.SetBackgroundColor(const Value: TColor);
begin
  GeneralSettings.BackgroundColor := Value;
end;

procedure TdxTableProperties.SetIsTableOverlap(const Value: Boolean);
begin
  GeneralSettings.IsTableOverlap := Value;
end;

procedure TdxTableProperties.SetTableAlignment(const Value: TdxTableRowAlignment);
begin
  GeneralSettings.TableAlignment := Value;
end;

procedure TdxTableProperties.InnerSetTableLayout(const Value: TdxTableLayoutType);
begin
  GeneralSettings.TableLayout := Value;
end;

procedure TdxTableProperties.Merge(AProperties: TdxTableProperties);
var
  AContainer: IdxPropertiesContainer;
begin
  AContainer := Self as IdxPropertiesContainer;
  AContainer.BeginPropertiesUpdate;
  try
    Borders.Merge(AProperties.Borders);
    CellMargins.Merge(AProperties.CellMargins);
    if not UseCellSpacing and AProperties.UseCellSpacing then
      CellSpacing.CopyFrom(AProperties.CellSpacing);
    if not UseIsTableOverlap and AProperties.UseIsTableOverlap then
      IsTableOverlap := AProperties.IsTableOverlap;
    if not UsePreferredWidth and AProperties.UsePreferredWidth then
      PreferredWidth.CopyFrom(AProperties.PreferredWidth);
    if not UseTableIndent and AProperties.UseTableIndent then
      TableIndent.CopyFrom(AProperties.TableIndent);
    if not UseTableLayout and AProperties.UseTableLayout then
      TableLayout := AProperties.TableLayout;
    if not UseTableLook and AProperties.UseTableLook then
      TableLook := AProperties.TableLook;
    if not UseTableStyleColBandSize and AProperties.UseTableStyleColBandSize then
      TableStyleColBandSize := AProperties.TableStyleColBandSize;
    if not UseTableStyleRowBandSize and AProperties.UseTableStyleRowBandSize then
      TableStyleRowBandSize := AProperties.TableStyleRowBandSize;
    if not UseFloatingPosition and AProperties.UseFloatingPosition then
      FloatingPosition.CopyFrom(AProperties.FloatingPosition);
    if not UseBackgroundColor and AProperties.UseBackgroundColor then
      BackgroundColor := AProperties.BackgroundColor;
    if not UseTableAlignment and AProperties.UseTableAlignment then
      TableAlignment := AProperties.TableAlignment;
  finally
    AContainer.EndPropertiesUpdate;
  end;
end;

procedure TdxTableProperties.SetTableLook(const Value: TdxTableLookTypes);
begin
  GeneralSettings.TableLook := Value;
end;

procedure TdxTableProperties.SetTableStyleColBandSize(const Value: Integer);
begin
  GeneralSettings.TableStyleColumnBandSize := Value;
end;

procedure TdxTableProperties.SetTableStyleRowBandSize(const Value: Integer);
begin
  GeneralSettings.TableStyleRowBandSize := Value;
end;

{ TdxTablePropertiesOptionsCache }

procedure TdxTablePropertiesOptionsCache.AddRootStyleOptions;
begin
  AppendItem(TdxTablePropertiesOptions.Create(TdxTablePropertiesOptions.MaskUseAll));
end;

function TdxTablePropertiesOptionsCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxTablePropertiesOptions;
begin
  Result := TdxTablePropertiesOptions.Create;
end;

procedure TdxTablePropertiesOptionsCache.InitItems(const AUnitConverter: IdxDocumentModelUnitConverter);
begin
  inherited InitItems(AUnitConverter);
  AddRootStyleOptions;
end;

{ TdxBordersBase<T> }

procedure TdxBordersBase<T>.CopyFrom(ABorders: TdxBordersBase<T>);
begin
  Owner.BeginPropertiesUpdate;
  try
    CopyFromCore(ABorders);
  finally
    Owner.EndPropertiesUpdate;
  end;
end;

procedure TdxBordersBase<T>.CopyFromCore(const ABorders: TdxBordersBase<T>);
begin
  if TopBorder.DocumentModel = ABorders.TopBorder.DocumentModel then
  begin
    if ABorders.UseTopBorder then
      TopBorder.CopyFrom(ABorders.TopBorder);

    if ABorders.UseLeftBorder then
      LeftBorder.CopyFrom(ABorders.LeftBorder);
    if ABorders.UseRightBorder then
      RightBorder.CopyFrom(ABorders.RightBorder);
    if ABorders.UseBottomBorder then
      BottomBorder.CopyFrom(ABorders.BottomBorder);
    if ABorders.UseInsideHorizontalBorder then
      InsideHorizontalBorder.CopyFrom(ABorders.InsideHorizontalBorder);
    if ABorders.UseInsideVerticalBorder then
      InsideVerticalBorder.CopyFrom(ABorders.InsideVerticalBorder);
  end
  else
  begin
    if ABorders.UseTopBorder then
      TopBorder.CopyFrom(ABorders.TopBorder.Info);
    if ABorders.UseLeftBorder then
      LeftBorder.CopyFrom(ABorders.LeftBorder.Info);
    if ABorders.UseRightBorder then
      RightBorder.CopyFrom(ABorders.RightBorder.Info);
    if ABorders.UseBottomBorder then
      BottomBorder.CopyFrom(ABorders.BottomBorder.Info);
    if ABorders.UseInsideHorizontalBorder then
      InsideHorizontalBorder.CopyFrom(ABorders.InsideHorizontalBorder.Info);
    if ABorders.UseInsideVerticalBorder then
      InsideVerticalBorder.CopyFrom(ABorders.InsideVerticalBorder.Info);
  end;
end;

constructor TdxBordersBase<T>.Create(APieceTable: TObject; const AOwner: IdxPropertiesContainerWithMask<T>);
begin
  inherited Create;
  Assert(AOwner <> nil); 
  FOwner := AOwner;
  FTopBorder := TdxTopBorder.Create(APieceTable, AOwner);
  FLeftBorder := TdxLeftBorder.Create(APieceTable, AOwner);
  FRightBorder := TdxRightBorder.Create(APieceTable, AOwner);
  FBottomBorder := TdxBottomBorder.Create(APieceTable, AOwner);
  FInsideHorizontalBorder := TdxInsideHorizontalBorder.Create(APieceTable, AOwner);
  FInsideVerticalBorder := TdxInsideVerticalBorder.Create(APieceTable, AOwner);
end;

destructor TdxBordersBase<T>.Destroy;
begin
  FInsideVerticalBorder.Free;
  FInsideHorizontalBorder.Free;
  FBottomBorder.Free;
  FRightBorder.Free;
  FLeftBorder.Free;
  FTopBorder.Free;
  inherited Destroy;
end;

function TdxBordersBase<T>.GetBottomBorder: TdxBorderBase;
begin
  Result := FBottomBorder;
end;

function TdxBordersBase<T>.GetLeftBorder: TdxBorderBase;
begin
  Result := FLeftBorder;
end;

function TdxBordersBase<T>.GetRightBorder: TdxBorderBase;
begin
  Result := FRightBorder;
end;

function TdxBordersBase<T>.GetTopBorder: TdxBorderBase;
begin
  Result := FTopBorder;
end;

function TdxBordersBase<T>.GetUseBottomBorder: Boolean;
begin
  Result := FOwner.GetUse(UseBottomBorderMask);
end;

function TdxBordersBase<T>.GetUseInsideHorizontalBorder: Boolean;
begin
  Result := FOwner.GetUse(UseInsideHorizontalBorderMask);
end;

function TdxBordersBase<T>.GetUseInsideVerticalBorder: Boolean;
begin
  Result := FOwner.GetUse(UseInsideVerticalBorderMask);
end;

function TdxBordersBase<T>.GetUseLeftBorder: Boolean;
begin
  Result := FOwner.GetUse(UseLeftBorderMask);
end;

function TdxBordersBase<T>.GetUseRightBorder: Boolean;
begin
  Result := FOwner.GetUse(UseRightBorderMask);
end;

function TdxBordersBase<T>.GetUseTopBorder: Boolean;
begin
  Result := FOwner.GetUse(UseTopBorderMask);
end;

procedure TdxBordersBase<T>.Merge(ABorders: TdxBordersBase<T>);
begin
  if not UseLeftBorder and ABorders.UseLeftBorder then
    LeftBorder.CopyFrom(ABorders.LeftBorder);
  if not UseRightBorder and ABorders.UseRightBorder then
    RightBorder.CopyFrom(ABorders.RightBorder);
  if not UseTopBorder and ABorders.UseTopBorder then
    TopBorder.CopyFrom(ABorders.TopBorder);
  if not UseBottomBorder and ABorders.UseBottomBorder then
    BottomBorder.CopyFrom(ABorders.BottomBorder);
  if not UseInsideHorizontalBorder and ABorders.UseInsideHorizontalBorder then
    InsideHorizontalBorder.CopyFrom(ABorders.InsideHorizontalBorder);
  if not UseInsideVerticalBorder and ABorders.UseInsideVerticalBorder then
    InsideVerticalBorder.CopyFrom(ABorders.InsideVerticalBorder);
end;

{ TdxTableCellBorders }

procedure TdxTableCellBorders.CopyFromCore(const ABorders: TdxBordersBase<Integer>);
var
  ACellBorders: TdxTableCellBorders;
begin
  inherited CopyFromCore(ABorders);

  if not (ABorders is TdxTableCellBorders) then
    Exit;
  ACellBorders := TdxTableCellBorders(ABorders);
  if LeftBorder.DocumentModel = ABorders.LeftBorder.DocumentModel then
  begin
    if ACellBorders.UseTopLeftDiagonalBorder then
      TopLeftDiagonalBorder.CopyFrom(ACellBorders.TopLeftDiagonalBorder);
    if ACellBorders.UseTopRightDiagonalBorder then
      TopRightDiagonalBorder.CopyFrom(ACellBorders.TopRightDiagonalBorder);
  end
  else
  begin
    if ACellBorders.UseTopLeftDiagonalBorder then
      TopLeftDiagonalBorder.CopyFrom(ACellBorders.TopLeftDiagonalBorder.Info);
    if ACellBorders.UseTopRightDiagonalBorder then
      TopRightDiagonalBorder.CopyFrom(ACellBorders.TopRightDiagonalBorder.Info);
  end;
end;

constructor TdxTableCellBorders.Create(APieceTable: TObject; AOwner: IdxCellPropertiesContainer);
begin
  inherited Create(APieceTable, AOwner);
  FTopLeftDiagonalBorder := TdxTopLeftDiagonalBorder.Create(APieceTable, AOwner);
  FTopRightDiagonalBorder := TdxTopRightDiagonalBorder.Create(APieceTable, AOwner);
end;

destructor TdxTableCellBorders.Destroy;
begin
  FTopRightDiagonalBorder.Free;
  FTopLeftDiagonalBorder.Free;
  inherited Destroy;
end;

function TdxTableCellBorders.GetUseTopLeftDiagonalBorder: Boolean;
begin
  Result := Owner.GetUse(TdxTableCellPropertiesOptions.MaskUseTopLeftDiagonalBorder);
end;

function TdxTableCellBorders.GetUseTopRightDiagonalBorder: Boolean;
begin
  Result := Owner.GetUse(TdxTableCellPropertiesOptions.MaskUseTopRightDiagonalBorder);
end;

procedure TdxTableCellBorders.Merge(ABorders: TdxBordersBase<Integer>);
var
  ACellBorders: TdxTableCellBorders;
begin
  inherited Merge(ABorders);
  if not (ABorders is TdxTableCellBorders) then
    Exit;
  ACellBorders := TdxTableCellBorders(ABorders);
  if not UseTopLeftDiagonalBorder and ACellBorders.UseTopLeftDiagonalBorder then
    TopLeftDiagonalBorder.CopyFrom(ACellBorders.TopLeftDiagonalBorder);
  if not UseTopRightDiagonalBorder and ACellBorders.UseTopRightDiagonalBorder then
    TopRightDiagonalBorder.CopyFrom(ACellBorders.TopRightDiagonalBorder);
end;

function TdxTableCellBorders.UseBottomBorderMask: Integer;
begin
  Result := TdxTableCellPropertiesOptions.MaskUseBottomBorder;
end;

function TdxTableCellBorders.UseInsideHorizontalBorderMask: Integer;
begin
  Result := TdxTableCellPropertiesOptions.MaskUseInsideHorizontalBorder;
end;

function TdxTableCellBorders.UseInsideVerticalBorderMask: Integer;
begin
  Result := TdxTableCellPropertiesOptions.MaskUseInsideVerticalBorder;
end;

function TdxTableCellBorders.UseLeftBorderMask: Integer;
begin
  Result := TdxTableCellPropertiesOptions.MaskUseLeftBorder;
end;

function TdxTableCellBorders.UseRightBorderMask: Integer;
begin
  Result := TdxTableCellPropertiesOptions.MaskUseRightBorder;
end;

function TdxTableCellBorders.UseTopBorderMask: Integer;
begin
  Result := TdxTableCellPropertiesOptions.MaskUseTopBorder;
end;

{ TdxTableBorders }

function TdxTableBorders.UseBottomBorderMask: Integer;
begin
  Result := TdxTablePropertiesOptions.MaskUseBottomBorder;
end;

function TdxTableBorders.UseInsideHorizontalBorderMask: Integer;
begin
  Result := TdxTablePropertiesOptions.MaskUseInsideHorizontalBorder;
end;

function TdxTableBorders.UseInsideVerticalBorderMask: Integer;
begin
  Result := TdxTablePropertiesOptions.MaskUseInsideVerticalBorder;
end;

function TdxTableBorders.UseLeftBorderMask: Integer;
begin
 Result := TdxTablePropertiesOptions.MaskUseLeftBorder;
end;

function TdxTableBorders.UseRightBorderMask: Integer;
begin
  Result := TdxTablePropertiesOptions.MaskUseRightBorder;
end;

function TdxTableBorders.UseTopBorderMask: Integer;
begin
  Result := TdxTablePropertiesOptions.MaskUseTopBorder;
end;

{ TdxTableCellPropertiesOptions }

constructor TdxTableCellPropertiesOptions.Create(AValue: Integer = 0);
begin
  inherited Create;
  FValue := AValue;
end;

function TdxTableCellPropertiesOptions.GetVal(AMask: Integer): Boolean;
begin
  Result := (FValue and AMask) <> 0;
end;

procedure TdxTableCellPropertiesOptions.SetVal(AMask: Integer; ABitVal: Boolean);
begin
  if ABitVal then
    FValue := FValue or AMask
  else
    FValue := FValue and not AMask;
end;

function TdxTableCellPropertiesOptions.Clone: TdxTableCellPropertiesOptions;
begin
  Result := TdxTableCellPropertiesOptions.Create;
  Result.CopyFrom(Self);
end;

procedure TdxTableCellPropertiesOptions.CopyFrom(const AOptions: TdxTableCellPropertiesOptions);
begin
  FValue := AOptions.FValue;
end;

function TdxTableCellPropertiesOptions.Equals(AObject: TObject): Boolean;
begin
  Result := AObject is TdxTableCellPropertiesOptions;
  if Result then
    Result := Value = TdxTableCellPropertiesOptions(AObject).Value;
end;

function TdxTableCellPropertiesOptions.GetHashCode: Integer;
begin
  Result := Value;
end;

function TdxTableCellPropertiesOptions.GetUseBackgroundColor: Boolean;
begin
  Result := GetVal(MaskUseBackgroundColor);
end;

function TdxTableCellPropertiesOptions.GetUseBottomBorder: Boolean;
begin
  Result := GetVal(MaskUseBottomBorder);
end;

function TdxTableCellPropertiesOptions.GetUseBottomMargin: Boolean;
begin
  Result := GetVal(MaskUseBottomMargin);
end;

function TdxTableCellPropertiesOptions.GetUseCellConditionalFormatting: Boolean;
begin
  Result := GetVal(MaskUseCellConditionalFormatting);
end;

function TdxTableCellPropertiesOptions.GetUseFitText: Boolean;
begin
  Result := GetVal(MaskUseFitText);
end;

function TdxTableCellPropertiesOptions.GetUseHideCellMark: Boolean;
begin
  Result := GetVal(MaskUseHideCellMark);
end;

function TdxTableCellPropertiesOptions.GetUseInsideHorizontalBorder: Boolean;
begin
  Result := GetVal(MaskUseInsideHorizontalBorder);
end;

function TdxTableCellPropertiesOptions.GetUseInsideVerticalBorder: Boolean;
begin
  Result := GetVal(MaskUseInsideVerticalBorder);
end;

function TdxTableCellPropertiesOptions.GetUseLeftBorder: Boolean;
begin
  Result := GetVal(MaskUseLeftBorder);
end;

function TdxTableCellPropertiesOptions.GetUseLeftMargin: Boolean;
begin
  Result := GetVal(MaskUseLeftMargin);
end;

function TdxTableCellPropertiesOptions.GetUseNoWrap: Boolean;
begin
  Result := GetVal(MaskUseNoWrap);
end;

function TdxTableCellPropertiesOptions.GetUsePreferredWidth: Boolean;
begin
  Result := GetVal(MaskUsePreferredWidth);
end;

function TdxTableCellPropertiesOptions.GetUseRightBorder: Boolean;
begin
  Result := GetVal(MaskUseRightBorder);
end;

function TdxTableCellPropertiesOptions.GetUseRightMargin: Boolean;
begin
  Result := GetVal(MaskUseRightMargin);
end;

function TdxTableCellPropertiesOptions.GetUseTextDirection: Boolean;
begin
  Result := GetVal(MaskUseTextDirection);
end;

function TdxTableCellPropertiesOptions.GetUseTopBorder: Boolean;
begin
  Result := GetVal(MaskUseTopBorder);
end;

function TdxTableCellPropertiesOptions.GetUseTopLeftDiagonalBorder: Boolean;
begin
  Result := GetVal(MaskUseTopLeftDiagonalBorder);
end;

function TdxTableCellPropertiesOptions.GetUseTopMargin: Boolean;
begin
  Result := GetVal(MaskUseTopMargin);
end;

function TdxTableCellPropertiesOptions.GetUseTopRightDiagonalBorder: Boolean;
begin
  Result := GetVal(MaskUseTopRightDiagonalBorder);
end;

function TdxTableCellPropertiesOptions.GetUseVerticalAlignment: Boolean;
begin
  Result := GetVal(MaskUseVerticalAlignment);
end;

procedure TdxTableCellPropertiesOptions.SetUseBackgroundColor(const Value: Boolean);
begin
  SetVal(MaskUseBackgroundColor, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseBottomBorder(const Value: Boolean);
begin
  SetVal(MaskUseBottomBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseBottomMargin(const Value: Boolean);
begin
  SetVal(MaskUseBottomMargin, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseCellConditionalFormatting(const Value: Boolean);
begin
  SetVal(MaskUseCellConditionalFormatting, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseFitText(const Value: Boolean);
begin
  SetVal(MaskUseFitText, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseHideCellMark(const Value: Boolean);
begin
  SetVal(MaskUseHideCellMark, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseInsideHorizontalBorder(const Value: Boolean);
begin
  SetVal(MaskUseInsideHorizontalBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseInsideVerticalBorder(const Value: Boolean);
begin
  SetVal(MaskUseInsideVerticalBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseLeftBorder(const Value: Boolean);
begin
  SetVal(MaskUseLeftBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseLeftMargin(const Value: Boolean);
begin
  SetVal(MaskUseLeftMargin, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseNoWrap(const Value: Boolean);
begin
  SetVal(MaskUseNoWrap, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUsePreferredWidth(const Value: Boolean);
begin
  SetVal(MaskUsePreferredWidth, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseRightBorder(const Value: Boolean);
begin
  SetVal(MaskUseRightBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseRightMargin(const Value: Boolean);
begin
  SetVal(MaskUseRightMargin, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseTextDirection(const Value: Boolean);
begin
  SetVal(MaskUseTextDirection, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseTopBorder(const Value: Boolean);
begin
  SetVal(MaskUseTopBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseTopLeftDiagonalBorder(const Value: Boolean);
begin
  SetVal(MaskUseTopLeftDiagonalBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseTopMargin(const Value: Boolean);
begin
  SetVal(MaskUseTopMargin, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseTopRightDiagonalBorder(const Value: Boolean);
begin
  SetVal(MaskUseTopRightDiagonalBorder, Value);
end;

procedure TdxTableCellPropertiesOptions.SetUseVerticalAlignment(const Value: Boolean);
begin
  SetVal(MaskUseVerticalAlignment, Value);
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseBackgroundColor(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseBackgroundColor;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseBottomBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseBottomBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseBottomMargin(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseBottomMargin;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseCellConditionalFormatting(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseCellConditionalFormatting;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseColumnSpan(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := True;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseFitText(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseFitText;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseHideCellMark(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseHideCellMark;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseInsideHorizontalBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseInsideHorizontalBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseInsideVerticalBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseInsideVerticalBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseLeftBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseLeftBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseLeftMargin(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseLeftMargin;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseNoWrap(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseNoWrap;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUsePreferredWidth(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UsePreferredWidth;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseRightBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseRightBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseRightMargin(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseRightMargin;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseTextDirection(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseTextDirection;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseTopBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseTopBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseTopLeftDiagonalBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseTopLeftDiagonalBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseTopMargin(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseTopMargin;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseTopRightDiagonalBorder(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseTopRightDiagonalBorder;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseVerticalAlignment(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := AOptions.UseVerticalAlignment;
end;

class function TdxTableCellPropertiesOptions.GetOptionsUseVerticalMerging(
  AOptions: TdxTableCellPropertiesOptions): Boolean;
begin
  Result := True;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseBackgroundColor(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseBackgroundColor := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseBottomBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseBottomBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseBottomMargin(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseBottomMargin := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseCellConditionalFormatting(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseCellConditionalFormatting := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseColumnSpan(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseFitText(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseFitText := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseHideCellMark(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseHideCellMark := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseInsideHorizontalBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseInsideHorizontalBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseInsideVerticalBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseInsideVerticalBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseLeftBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseLeftBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseLeftMargin(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseLeftMargin := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseNoWrap(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseNoWrap := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUsePreferredWidth(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UsePreferredWidth := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseRightBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseRightBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseRightMargin(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseRightMargin := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseTextDirection(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseTextDirection := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseTopBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseTopBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseTopLeftDiagonalBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseTopLeftDiagonalBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseTopMargin(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseTopMargin := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseTopRightDiagonalBorder(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseTopRightDiagonalBorder := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseVerticalAlignment(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseVerticalAlignment := AValue;
end;

class procedure TdxTableCellPropertiesOptions.SetOptionsUseVerticalMerging(
  AOptions: TdxTableCellPropertiesOptions; const AValue: Boolean);
begin
//do nothing
end;

{ TdxTableCellPropertiesMerger }

constructor TdxTableCellPropertiesMerger.Create(AInitialProperties: TdxTableCellProperties);
begin
  inherited Create(TdxMergedTableCellProperties.Create(TdxCombinedCellPropertiesInfo.Create(AInitialProperties), AInitialProperties.Info));
end;

constructor TdxTableCellPropertiesMerger.Create(AInitialProperties: TdxMergedTableCellProperties);
begin
  inherited Create(TdxMergedTableCellProperties.Create(AInitialProperties.Info, AInitialProperties.Options));
end;

procedure TdxTableCellPropertiesMerger.Merge(AProperties: TdxTableCellProperties);
begin
  MergeCore(TdxCombinedCellPropertiesInfo.Create(AProperties), AProperties.Info);
end;

procedure TdxTableCellPropertiesMerger.MergeCore(const AInfo: TdxCombinedCellPropertiesInfo;
  const AOptions: TdxTableCellPropertiesOptions);
var
  AOwnInfo: TdxCombinedCellPropertiesInfo;
  AOwnOptions: TdxTableCellPropertiesOptions;
begin
  AOwnInfo := OwnInfo;
  AOwnOptions := OwnOptions;
  if not AOwnOptions.UseBottomBorder and AOptions.UseBottomBorder then
  begin
    AOwnInfo.Borders.BottomBorder.CopyFrom(AInfo.Borders.BottomBorder);
    AOwnOptions.UseBottomBorder := True;
  end;
  if not AOwnOptions.UseTopBorder and AOptions.UseTopBorder then
  begin
    AOwnInfo.Borders.TopBorder.CopyFrom(AInfo.Borders.TopBorder);
    AOwnOptions.UseTopBorder := True;
  end;
  if not AOwnOptions.UseLeftBorder and AOptions.UseLeftBorder then
  begin
    AOwnInfo.Borders.LeftBorder.CopyFrom(AInfo.Borders.LeftBorder);
    AOwnOptions.UseLeftBorder := True;
  end;
  if not AOwnOptions.UseRightBorder and AOptions.UseRightBorder then
  begin
    AOwnInfo.Borders.RightBorder.CopyFrom(AInfo.Borders.RightBorder);
    AOwnOptions.UseRightBorder := True;
  end;
  if not AOwnOptions.UseInsideHorizontalBorder and AOptions.UseInsideHorizontalBorder then
  begin
    AOwnInfo.Borders.InsideHorizontalBorder.CopyFrom(AInfo.Borders.InsideHorizontalBorder);
    AOwnOptions.UseInsideHorizontalBorder := True;
  end;
  if not AOwnOptions.UseInsideVerticalBorder and AOptions.UseInsideVerticalBorder then
  begin
    AOwnInfo.Borders.InsideVerticalBorder.CopyFrom(AInfo.Borders.InsideVerticalBorder);
    AOwnOptions.UseInsideVerticalBorder := True;
  end;
  if not AOwnOptions.UseTopLeftDiagonalBorder and AOptions.UseTopLeftDiagonalBorder then
  begin
    AOwnInfo.Borders.TopLeftDiagonalBorder.CopyFrom(AInfo.Borders.TopLeftDiagonalBorder);
    AOwnOptions.UseTopLeftDiagonalBorder := True;
  end;
  if not AOwnOptions.UseTopRightDiagonalBorder and AOptions.UseTopRightDiagonalBorder then
  begin
    AOwnInfo.Borders.TopRightDiagonalBorder.CopyFrom(AInfo.Borders.TopRightDiagonalBorder);
    AOwnOptions.UseTopRightDiagonalBorder := True;
  end;
  if not AOwnOptions.UseTopMargin and AOptions.UseTopMargin then
  begin
    AOwnInfo.CellMargins.Top.CopyFrom(AInfo.CellMargins.Top);
    AOwnOptions.UseTopMargin := True;
  end;
  if not AOwnOptions.UseLeftMargin and AOptions.UseLeftMargin then
  begin
    AOwnInfo.CellMargins.Left.CopyFrom(AInfo.CellMargins.Left);
    AOwnOptions.UseLeftMargin := True;
  end;
  if not AOwnOptions.UseRightMargin and AOptions.UseRightMargin then
  begin
    AOwnInfo.CellMargins.Right.CopyFrom(AInfo.CellMargins.Right);
    AOwnOptions.UseRightMargin := True;
  end;
  if not AOwnOptions.UseBottomMargin and AOptions.UseBottomMargin then
  begin
    AOwnInfo.CellMargins.Bottom.CopyFrom(AInfo.CellMargins.Bottom);
    AOwnOptions.UseBottomMargin := True;
  end;
  if not AOwnOptions.UseBackgroundColor and AOptions.UseBackgroundColor then
  begin
    AOwnInfo.GeneralSettings.BackgroundColor := AInfo.GeneralSettings.BackgroundColor;
    AOwnOptions.UseBackgroundColor := True;
  end;
  if not AOwnOptions.UseCellConditionalFormatting and AOptions.UseCellConditionalFormatting then
  begin
    AOwnInfo.GeneralSettings.CellConditionalFormatting := AInfo.GeneralSettings.CellConditionalFormatting;
    AOwnOptions.UseCellConditionalFormatting := True;
  end;
  if not AOwnOptions.UseFitText and AOptions.UseFitText then
  begin
    AOwnInfo.GeneralSettings.FitText := AInfo.GeneralSettings.FitText;
    AOwnOptions.UseFitText := True;
  end;
  if not AOwnOptions.UseHideCellMark and AOptions.UseHideCellMark then
  begin
    AOwnInfo.GeneralSettings.HideCellMark := AInfo.GeneralSettings.HideCellMark;
    AOwnOptions.UseHideCellMark := True;
  end;
  if not AOwnOptions.UseNoWrap and AOptions.UseNoWrap then
  begin
    AOwnInfo.GeneralSettings.NoWrap := AInfo.GeneralSettings.NoWrap;
    AOwnOptions.UseNoWrap := True;
  end;
  if not AOwnOptions.UseTextDirection and AOptions.UseTextDirection then
  begin
    AOwnInfo.GeneralSettings.TextDirection := AInfo.GeneralSettings.TextDirection;
    AOwnOptions.UseTextDirection := True;
  end;
  if not AOwnOptions.UseVerticalAlignment and AOptions.UseVerticalAlignment then
  begin
    AOwnInfo.GeneralSettings.VerticalAlignment := AInfo.GeneralSettings.VerticalAlignment;
    AOwnOptions.UseVerticalAlignment := True;
  end;
  if not AOwnOptions.UsePreferredWidth and AOptions.UsePreferredWidth then
  begin
    AOwnInfo.PreferredWidth.CopyFrom(AInfo.PreferredWidth);
    AOwnOptions.UsePreferredWidth := True;
  end;
end;

{ TdxTableCellProperties }

constructor TdxTableCellProperties.Create(APieceTable: TObject; AOwner: IdxCellPropertiesOwner);
begin
  inherited Create(APieceTable as TdxPieceTable);
  Assert(AOwner <> nil);
  FOwner := AOwner;
  FPreferredWidth := TdxPreferredWidth.Create(APieceTable, Self);
  FCellMargins := TdxCellMargins.Create(APieceTable, Self);
  FBorders := TdxTableCellBorders.Create(APieceTable, Self);
  FGeneralSettings := TdxTableCellGeneralSettings.Create(APieceTable, Self);
end;

destructor TdxTableCellProperties.Destroy;
begin
  FreeAndNil(FGeneralSettings);
  FreeAndNil(FBorders);
  FreeAndNil(FCellMargins);
  FreeAndNil(FPreferredWidth);
  inherited Destroy;
end;

class constructor TdxTableCellProperties.Initialize;
begin
  FAccessorTable := CreateAccessorTable;
end;

class destructor TdxTableCellProperties.Finalize;
begin
  FreeAndNil(FAccessorTable);
end;

class function TdxTableCellProperties.CreateAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>>;
begin
  Result := TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>>.Create;
  Result.Add(TdxProperties.CellConditionalFormatting, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseCellConditionalFormatting, TdxTableCellPropertiesOptions.SetOptionsUseCellConditionalFormatting));
  Result.Add(TdxProperties.PreferredWidth, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUsePreferredWidth, TdxTableCellPropertiesOptions.SetOptionsUsePreferredWidth));
  Result.Add(TdxProperties.HideCellMark, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseHideCellMark, TdxTableCellPropertiesOptions.SetOptionsUseHideCellMark));
  Result.Add(TdxProperties.NoWrap, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseNoWrap, TdxTableCellPropertiesOptions.SetOptionsUseNoWrap));
  Result.Add(TdxProperties.FitText, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseFitText, TdxTableCellPropertiesOptions.SetOptionsUseFitText));

  Result.Add(TdxProperties.TopMargin, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseTopMargin, TdxTableCellPropertiesOptions.SetOptionsUseTopMargin));
  Result.Add(TdxProperties.BottomMargin, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseBottomMargin, TdxTableCellPropertiesOptions.SetOptionsUseBottomMargin));
  Result.Add(TdxProperties.LeftMargin, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseLeftMargin, TdxTableCellPropertiesOptions.SetOptionsUseLeftMargin));
  Result.Add(TdxProperties.RightMargin, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseRightMargin, TdxTableCellPropertiesOptions.SetOptionsUseRightMargin));

  Result.Add(TdxProperties.TextDirection, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseTextDirection, TdxTableCellPropertiesOptions.SetOptionsUseTextDirection));
  Result.Add(TdxProperties.VerticalAlignment, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseVerticalAlignment, TdxTableCellPropertiesOptions.SetOptionsUseVerticalAlignment));
  Result.Add(TdxProperties.VerticalMerging, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseVerticalMerging, TdxTableCellPropertiesOptions.SetOptionsUseVerticalMerging));
  Result.Add(TdxProperties.ColumnSpan, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseColumnSpan, TdxTableCellPropertiesOptions.SetOptionsUseColumnSpan));
  Result.Add(TdxProperties.BackgroundColor, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseBackgroundColor, TdxTableCellPropertiesOptions.SetOptionsUseBackgroundColor));

  Result.Add(TdxProperties.LeftBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseLeftBorder, TdxTableCellPropertiesOptions.SetOptionsUseLeftBorder));
  Result.Add(TdxProperties.RightBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseRightBorder, TdxTableCellPropertiesOptions.SetOptionsUseRightBorder));
  Result.Add(TdxProperties.TopBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseTopBorder, TdxTableCellPropertiesOptions.SetOptionsUseTopBorder));
  Result.Add(TdxProperties.BottomBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseBottomBorder, TdxTableCellPropertiesOptions.SetOptionsUseBottomBorder));
  Result.Add(TdxProperties.InsideHorizontalBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseInsideHorizontalBorder, TdxTableCellPropertiesOptions.SetOptionsUseInsideHorizontalBorder));
  Result.Add(TdxProperties.InsideVerticalBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseInsideVerticalBorder, TdxTableCellPropertiesOptions.SetOptionsUseInsideVerticalBorder));
  Result.Add(TdxProperties.TopLeftDiagonalBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseTopLeftDiagonalBorder, TdxTableCellPropertiesOptions.SetOptionsUseTopLeftDiagonalBorder));
  Result.Add(TdxProperties.TopRightDiagonalBorder, TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>.Create(TdxTableCellPropertiesOptions.GetOptionsUseTopRightDiagonalBorder, TdxTableCellPropertiesOptions.SetOptionsUseTopRightDiagonalBorder));
end;

procedure TdxTableCellProperties.CopyFrom(AProperties: TdxTableCellProperties);
var
  AIsDeferredInfo: Boolean;
  AInfo: TdxTableCellPropertiesOptions;
  AContainer: IdxPropertiesContainer;
begin
  AContainer := Self as IdxPropertiesContainer;
  DocumentModel.BeginUpdate;
  AContainer.BeginPropertiesUpdate;
  try
    if DocumentModel = AProperties.DocumentModel then
    begin
      GeneralSettings.CopyFrom(AProperties.GeneralSettings);
      Borders.CopyFrom(AProperties.Borders);
      CellMargins.CopyFrom(AProperties.CellMargins);
      PreferredWidth.CopyFrom(AProperties.PreferredWidth);
      AInfo := GetInfoForModification(AIsDeferredInfo);
      AInfo.CopyFrom(AProperties.Info);
      ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
      if not AIsDeferredInfo then AInfo.Free;
    end
    else
    begin
      GeneralSettings.CopyFrom(AProperties.GeneralSettings.Info);
      Borders.CopyFrom(AProperties.Borders);
      CellMargins.CopyFrom(AProperties.CellMargins);
      PreferredWidth.CopyFrom(AProperties.PreferredWidth.Info);
      Info.CopyFrom(AProperties.Info);
    end;
  finally
    AContainer.EndPropertiesUpdate;
    DocumentModel.EndUpdate;
  end;
end;

procedure TdxTableCellProperties.CopyFrom(AProperties: TdxMergedTableCellProperties);
var
  AContainer: IdxPropertiesContainer;
begin
  Supports(Self, IdxPropertiesContainer, AContainer);
  AContainer.BeginPropertiesUpdate;
  try
    GeneralSettings.CopyFrom(AProperties.Info.GeneralSettings);
    Borders.CopyFrom(AProperties.Info.Borders);
    CellMargins.CopyFrom(AProperties.Info.CellMargins);
    PreferredWidth.CopyFrom(AProperties.Info.PreferredWidth);
    Info.CopyFrom(AProperties.Options);
  finally
    AContainer.EndPropertiesUpdate;
  end;
end;

function TdxTableCellProperties.GetBackgroundColor: TColor;
begin
  Result := GeneralSettings.BackgroundColor;
end;

function TdxTableCellProperties.GetAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableCellPropertiesOptions>>;
begin
  Result := FAccessorTable;
end;

function TdxTableCellProperties.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableCellPropertiesOptions>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TableCellPropertiesOptionsCache;
end;

function TdxTableCellProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [];
end;

function TdxTableCellProperties.CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := FOwner.CreateCellPropertiesChangedHistoryItem(Self);
end;

function TdxTableCellProperties.GetCellConditionalFormatting: TdxConditionalTableStyleFormattingTypes;
begin
  Result := GeneralSettings.CellConditionalFormatting;
end;

function TdxTableCellProperties.GetColumnSpan: Integer;
begin
  Result := GeneralSettings.ColumnSpan;
end;

function TdxTableCellProperties.GetFitText: Boolean;
begin
  Result := GeneralSettings.FitText;
end;

function TdxTableCellProperties.GetHideCellMark: Boolean;
begin
  Result := GeneralSettings.HideCellMark;
end;

function TdxTableCellProperties.GetNoWrap: Boolean;
begin
  Result := GeneralSettings.NoWrap;
end;

function TdxTableCellProperties.GetTextDirection: TdxTextDirection;
begin
  Result := GeneralSettings.TextDirection;
end;

function TdxTableCellProperties.GetUse(AMask: Integer): Boolean;
begin
  Result := Info.GetVal(AMask);
end;

function TdxTableCellProperties.GetUseBackgroundColor: Boolean;
begin
  Result := Info.UseBackgroundColor;
end;

function TdxTableCellProperties.GetUseBottomMargin: Boolean;
begin
  Result := Info.UseBottomMargin;
end;

function TdxTableCellProperties.GetUseCellConditionalFormatting: Boolean;
begin
  Result := Info.UseCellConditionalFormatting;
end;

function TdxTableCellProperties.GetUseFitText: Boolean;
begin
  Result := Info.UseFitText;
end;

function TdxTableCellProperties.GetUseHideCellMark: Boolean;
begin
  Result := Info.UseHideCellMark;
end;

function TdxTableCellProperties.GetUseLeftMargin: Boolean;
begin
  Result := Info.UseLeftMargin;
end;

function TdxTableCellProperties.GetUseNoWrap: Boolean;
begin
  Result := Info.UseNoWrap;
end;

function TdxTableCellProperties.GetUsePreferredWidth: Boolean;
begin
  Result := Info.UsePreferredWidth;
end;

function TdxTableCellProperties.GetUseRightMargin: Boolean;
begin
  Result := Info.UseRightMargin;
end;

function TdxTableCellProperties.GetUseTextDirection: Boolean;
begin
  Result := Info.UseTextDirection;
end;

function TdxTableCellProperties.GetUseTopMargin: Boolean;
begin
  Result := Info.UseTopMargin;
end;

function TdxTableCellProperties.GetUseVerticalAlignment: Boolean;
begin
  Result := Info.UseVerticalAlignment;
end;

function TdxTableCellProperties.GetVerticalAlignment: TdxVerticalAlignment;
begin
  Result := GeneralSettings.VerticalAlignment;
end;

function TdxTableCellProperties.GetVerticalMerging: TdxMergingState;
begin
  Result := GeneralSettings.VerticalMerging;
end;

procedure TdxTableCellProperties.Merge(AProperties: TdxTableCellProperties);
var
  AContainer: IdxPropertiesContainer;
begin
  Supports(Self, IdxPropertiesContainer, AContainer);
  AContainer.BeginPropertiesUpdate;
  try
    Borders.Merge(AProperties.Borders);
    if not UseCellConditionalFormatting and AProperties.UseCellConditionalFormatting then
      CellConditionalFormatting := AProperties.CellConditionalFormatting;
    CellMargins.Merge(AProperties.CellMargins);
    if not UseBackgroundColor and AProperties.UseBackgroundColor then
      BackgroundColor := AProperties.BackgroundColor;
    if not UseFitText and AProperties.UseFitText then
      FitText := AProperties.FitText;
    if not UseHideCellMark and AProperties.UseHideCellMark then
      HideCellMark := AProperties.HideCellMark;
    if not UseNoWrap and AProperties.UseNoWrap then
      NoWrap := AProperties.NoWrap;
    if not UsePreferredWidth and AProperties.UsePreferredWidth then
      PreferredWidth.CopyFrom(AProperties.PreferredWidth);
    if not UseTextDirection and AProperties.UseTextDirection then
      TextDirection := AProperties.TextDirection;
    if not UseVerticalAlignment and AProperties.UseVerticalAlignment then
      VerticalAlignment := AProperties.VerticalAlignment;
  finally
    AContainer.EndPropertiesUpdate;
  end;
end;

procedure TdxTableCellProperties.RaisePropertiesChanged;
begin
  FPropertiesChanged.Invoke(Self, nil);
end;

procedure TdxTableCellProperties.Reset;
begin
  DocumentModel.History.BeginTransaction;
  try
    CopyFrom(TdxDocumentModel(DocumentModel).DefaultTableCellProperties);
    ResetAllUse;
  finally
    DocumentModel.History.EndTransaction;
  end;
end;

procedure TdxTableCellProperties.ResetAllUse;
begin
  ReplaceInfo(GetCache(DocumentModel)[TdxTableCellPropertiesOptionsCache.EmptyCellPropertiesOptionsItem], GetBatchUpdateChangeActions);
end;

procedure TdxTableCellProperties.ResetUse(AMask: Integer);
var
  ANewOptions: TdxTableCellPropertiesOptions;
begin
  ANewOptions := TdxTableCellPropertiesOptions.Create(Info.Value and not AMask);
  try
    ReplaceInfo(ANewOptions, GetBatchUpdateChangeActions);
  finally
    ANewOptions.Free;
  end;
end;

procedure TdxTableCellProperties.SetBackgroundColor(const Value: TColor);
begin
  GeneralSettings.BackgroundColor := Value;
end;

procedure TdxTableCellProperties.SetCellConditionalFormatting(const Value: TdxConditionalTableStyleFormattingTypes);
begin
  GeneralSettings.CellConditionalFormatting := Value;
end;

procedure TdxTableCellProperties.SetColumnSpan(const Value: Integer);
begin
  GeneralSettings.ColumnSpan := Value;
end;

procedure TdxTableCellProperties.SetFitText(const Value: Boolean);
begin
  GeneralSettings.FitText := Value;
end;

procedure TdxTableCellProperties.SetHideCellMark(const Value: Boolean);
begin
  GeneralSettings.HideCellMark := Value;
end;

procedure TdxTableCellProperties.SetNoWrap(const Value: Boolean);
begin
  GeneralSettings.NoWrap := Value;
end;

procedure TdxTableCellProperties.SetTextDirection(const Value: TdxTextDirection);
begin
  GeneralSettings.TextDirection := Value;
end;

procedure TdxTableCellProperties.SetVerticalAlignment(const Value: TdxVerticalAlignment);
begin
  GeneralSettings.VerticalAlignment := Value;
end;

procedure TdxTableCellProperties.SetVerticalMerging(const Value: TdxMergingState);
begin
  GeneralSettings.VerticalMerging := Value;
end;

{ TdxTableCellBorders }

procedure TdxTableCellBorders.CopyFrom(const ABorders: TdxCombinedCellBordersInfo);
begin
  Owner.BeginPropertiesUpdate;
  try
    TopBorder.CopyFrom(ABorders.TopBorder);
    LeftBorder.CopyFrom(ABorders.LeftBorder);
    RightBorder.CopyFrom(ABorders.RightBorder);
    BottomBorder.CopyFrom(ABorders.BottomBorder);
    InsideHorizontalBorder.CopyFrom(ABorders.InsideHorizontalBorder);
    InsideVerticalBorder.CopyFrom(ABorders.InsideVerticalBorder);
    TopLeftDiagonalBorder.CopyFrom(ABorders.TopLeftDiagonalBorder);
    TopRightDiagonalBorder.CopyFrom(ABorders.TopRightDiagonalBorder);
  finally
    Owner.EndPropertiesUpdate;
  end;
end;

{ TdxCombinedCellBordersInfo }

constructor TdxCombinedCellBordersInfo.Create;
begin
  inherited Create;
  FBottomBorder := TdxBorderInfo.Create;
  FLeftBorder := TdxBorderInfo.Create;
  FRightBorder := TdxBorderInfo.Create;
  FTopBorder := TdxBorderInfo.Create;
  FInsideHorizontalBorder := TdxBorderInfo.Create;
  FInsideVerticalBorder := TdxBorderInfo.Create;
  FTopLeftDiagonalBorder := TdxBorderInfo.Create;
  FTopRightDiagonalBorder := TdxBorderInfo.Create;
end;

constructor TdxCombinedCellBordersInfo.Create(ATableBorders: TdxTableCellBorders);
begin
  inherited Create;
  FBottomBorder := ATableBorders.BottomBorder.Info;
  FLeftBorder := ATableBorders.LeftBorder.Info;
  FRightBorder := ATableBorders.RightBorder.Info;
  FTopBorder := ATableBorders.TopBorder.Info;
  FInsideHorizontalBorder := ATableBorders.InsideHorizontalBorder.Info;
  FInsideVerticalBorder := ATableBorders.InsideVerticalBorder.Info;
  FTopLeftDiagonalBorder := ATableBorders.TopLeftDiagonalBorder.Info;
  FTopRightDiagonalBorder := ATableBorders.TopRightDiagonalBorder.Info;
end;

function TdxCombinedCellBordersInfo.Clone: TdxCombinedCellBordersInfo;
begin
  Result := TdxCombinedCellBordersInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxCombinedCellBordersInfo.CopyFrom(const AInfo: TdxCombinedCellBordersInfo);
begin
  FBottomBorder.CopyFrom(AInfo.BottomBorder);
  FLeftBorder.CopyFrom(AInfo.LeftBorder);
  FRightBorder.CopyFrom(AInfo.RightBorder);
  FTopBorder.CopyFrom(AInfo.TopBorder);
  FInsideHorizontalBorder.CopyFrom(AInfo.InsideHorizontalBorder);
  FInsideVerticalBorder.CopyFrom(AInfo.InsideVerticalBorder);
  FTopLeftDiagonalBorder.CopyFrom(AInfo.TopLeftDiagonalBorder);
  FTopRightDiagonalBorder.CopyFrom(AInfo.TopRightDiagonalBorder);
end;

{ TdxCombinedTableBordersInfo }

constructor TdxCombinedTableBordersInfo.Create;
begin
  inherited Create;
  FBottomBorder := TdxBorderInfo.Create;
  FLeftBorder := TdxBorderInfo.Create;
  FRightBorder := TdxBorderInfo.Create;
  FTopBorder := TdxBorderInfo.Create;
  FInsideHorizontalBorder := TdxBorderInfo.Create;
  FInsideVerticalBorder := TdxBorderInfo.Create;
end;

constructor TdxCombinedTableBordersInfo.Create(ATableBorders: TdxTableBorders);
begin
  FBottomBorder := ATableBorders.BottomBorder.Info;
  FLeftBorder := ATableBorders.LeftBorder.Info;
  FRightBorder := ATableBorders.RightBorder.Info;
  FTopBorder := ATableBorders.TopBorder.Info;
  FInsideHorizontalBorder := ATableBorders.InsideHorizontalBorder.Info;
  FInsideVerticalBorder := ATableBorders.InsideVerticalBorder.Info;
end;

function TdxCombinedTableBordersInfo.Clone: TdxCombinedTableBordersInfo;
begin
  Result := TdxCombinedTableBordersInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxCombinedTableBordersInfo.CopyFrom(const AInfo: TdxCombinedTableBordersInfo);
begin
  FBottomBorder.CopyFrom(AInfo.BottomBorder);
  FLeftBorder.CopyFrom(AInfo.LeftBorder);
  FRightBorder.CopyFrom(AInfo.RightBorder);
  FTopBorder.CopyFrom(AInfo.TopBorder);
  FInsideHorizontalBorder.CopyFrom(AInfo.InsideHorizontalBorder);
  FInsideVerticalBorder.CopyFrom(AInfo.InsideVerticalBorder);
end;

{ TdxCombinedTablePropertiesInfo }

constructor TdxCombinedTablePropertiesInfo.Create;
begin
  inherited Create;
  FCellMargins := TdxCombinedCellMarginsInfo.Create;
  FBorders := TdxCombinedTableBordersInfo.Create;
  FCellSpacing := TdxWidthUnitInfo.Create;
  FIndent := TdxWidthUnitInfo.Create;
  FPreferredWidth := TdxWidthUnitInfo.Create;
  FGeneralSettings := TdxTableGeneralSettingsInfo.Create;
  FFloatingPosition := TdxTableFloatingPositionInfo.Create;
end;

constructor TdxCombinedTablePropertiesInfo.Create(ATableProperties: TdxTableProperties);
begin
  inherited Create;
  FCellMargins := TdxCombinedCellMarginsInfo.Create(ATableProperties.CellMargins);
  FBorders := TdxCombinedTableBordersInfo.Create(ATableProperties.Borders);
  FCellSpacing := ATableProperties.CellSpacing.Info;
  FIndent := ATableProperties.TableIndent.Info;
  FPreferredWidth := ATableProperties.PreferredWidth.Info;
  FGeneralSettings := ATableProperties.GeneralSettings.Info;
  FFloatingPosition := ATableProperties.FloatingPosition.Info;
end;

destructor TdxCombinedTablePropertiesInfo.Destroy;
begin
  FreeAndNil(FCellMargins);
  FreeAndNil(FBorders);
  FreeAndNil(FCellSpacing);
  FreeAndNil(FIndent);
  FreeAndNil(FPreferredWidth);
  FreeAndNil(FGeneralSettings);
  FreeAndNil(FFloatingPosition);
  inherited Destroy;
end;

function TdxCombinedTablePropertiesInfo.Clone: TdxCombinedTablePropertiesInfo;
begin
  Result := TdxCombinedTablePropertiesInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxCombinedTablePropertiesInfo.CopyFrom(AInfo: TdxCombinedTablePropertiesInfo);
begin
  CellMargins.CopyFrom(AInfo.CellMargins);
  Borders.CopyFrom(AInfo.Borders);
  CellSpacing.CopyFrom(AInfo.CellSpacing);
  TableIndent.CopyFrom(AInfo.TableIndent);
  PreferredWidth.CopyFrom(AInfo.PreferredWidth);
  GeneralSettings.CopyFrom(AInfo.GeneralSettings);
  FloatingPosition.CopyFrom(AInfo.FloatingPosition);
end;

{ TdxTablePropertiesMerger }

constructor TdxTablePropertiesMerger.Create(AInitialProperties: TdxTableProperties);
begin
  inherited Create(TdxMergedTableProperties.Create(TdxCombinedTablePropertiesInfo.Create(AInitialProperties), AInitialProperties.Info));
end;

constructor TdxTablePropertiesMerger.Create(AInitialProperties: TdxMergedTableProperties);
begin
  inherited Create(TdxMergedTableProperties.Create(AInitialProperties.Info, AInitialProperties.Options));
end;

procedure TdxTablePropertiesMerger.Merge(AProperties: TdxTableProperties);
begin
  MergeCore(TdxCombinedTablePropertiesInfo.Create(AProperties), AProperties.Info);
end;

procedure TdxTablePropertiesMerger.MergeCore(const AInfo: TdxCombinedTablePropertiesInfo;
  const AOptions: TdxTablePropertiesOptions);
var
  AOwnOptions: TdxTablePropertiesOptions;
  AOwnInfo: TdxCombinedTablePropertiesInfo;
begin
  AOwnInfo := OwnInfo;
  AOwnOptions := OwnOptions;
  if not AOwnOptions.UseBottomBorder and AOptions.UseBottomBorder then
  begin
    AOwnInfo.Borders.BottomBorder.CopyFrom(AInfo.Borders.BottomBorder);
    AOwnOptions.UseBottomBorder := True;
  end;
  if not AOwnOptions.UseTopBorder and AOptions.UseTopBorder then
  begin
    AOwnInfo.Borders.TopBorder.CopyFrom(AInfo.Borders.TopBorder);
    AOwnOptions.UseTopBorder := True;
  end;
  if not AOwnOptions.UseLeftBorder and AOptions.UseLeftBorder then
  begin
    AOwnInfo.Borders.LeftBorder.CopyFrom(AInfo.Borders.LeftBorder);
    AOwnOptions.UseLeftBorder := True;
  end;
  if not AOwnOptions.UseRightBorder and AOptions.UseRightBorder then
  begin
    AOwnInfo.Borders.RightBorder.CopyFrom(AInfo.Borders.RightBorder);
    AOwnOptions.UseRightBorder := True;
  end;
  if not AOwnOptions.UseInsideHorizontalBorder and AOptions.UseInsideHorizontalBorder then
  begin
    AOwnInfo.Borders.InsideHorizontalBorder.CopyFrom(AInfo.Borders.InsideHorizontalBorder);
    AOwnOptions.UseInsideHorizontalBorder := True;
  end;
  if not AOwnOptions.UseInsideVerticalBorder and AOptions.UseInsideVerticalBorder then
  begin
    AOwnInfo.Borders.InsideVerticalBorder.CopyFrom(AInfo.Borders.InsideVerticalBorder);
    AOwnOptions.UseInsideVerticalBorder := True;
  end;
  if not AOwnOptions.UseLeftBorder and AOptions.UseLeftBorder then
  begin
    AOwnInfo.CellMargins.Left.CopyFrom(AInfo.CellMargins.Left);
    AOwnOptions.UseLeftBorder := True;
  end;
  if not AOwnOptions.UseRightBorder and AOptions.UseRightBorder then
  begin
    AOwnInfo.CellMargins.Right.CopyFrom(AInfo.CellMargins.Right);
    AOwnOptions.UseRightBorder := True;
  end;
  if not AOwnOptions.UseTopBorder and AOptions.UseTopBorder then
  begin
    AOwnInfo.CellMargins.Top.CopyFrom(AInfo.CellMargins.Top);
    AOwnOptions.UseTopBorder := True;
  end;

  if not AOwnOptions.UseBottomBorder and AOptions.UseBottomBorder then
  begin
    AOwnInfo.CellMargins.Bottom.CopyFrom(AInfo.CellMargins.Bottom);
    AOwnOptions.UseBottomBorder := True;
  end;
  if not AOwnOptions.UseCellSpacing and AOptions.UseCellSpacing then
  begin
    AOwnInfo.CellSpacing.CopyFrom(AInfo.CellSpacing);
    AOwnOptions.UseCellSpacing := True;
  end;
  if not AOwnOptions.UseFloatingPosition and AOptions.UseFloatingPosition then
  begin
    AOwnInfo.FloatingPosition.CopyFrom(AInfo.FloatingPosition);
    AOwnOptions.UseFloatingPosition := True;
  end;
  if not AOwnOptions.UseIsTableOverlap and AOptions.UseIsTableOverlap then
  begin
    AOwnInfo.GeneralSettings.IsTableOverlap := AInfo.GeneralSettings.IsTableOverlap;
    AOwnOptions.UseIsTableOverlap := True;
  end;
  if not AOwnOptions.UseTableLayout and AOptions.UseTableLayout then
  begin
    AOwnInfo.GeneralSettings.TableLayout := AInfo.GeneralSettings.TableLayout;
    AOwnOptions.UseTableLayout := True;
  end;
  if not AOwnOptions.UseTableLook and AOptions.UseTableLook then
  begin
    AOwnInfo.GeneralSettings.TableLook := AInfo.GeneralSettings.TableLook;
    AOwnOptions.UseTableLook := True;
  end;
  if not AOwnOptions.UseTableStyleColBandSize and AOptions.UseTableStyleColBandSize then
  begin
    AOwnInfo.GeneralSettings.TableStyleColBandSize := AInfo.GeneralSettings.TableStyleColBandSize;
    AOwnOptions.UseTableStyleColBandSize := True;
  end;
  if not AOwnOptions.UseTableStyleRowBandSize and AOptions.UseTableStyleRowBandSize then
  begin
    AOwnInfo.GeneralSettings.TableStyleRowBandSize := AInfo.GeneralSettings.TableStyleRowBandSize;
    AOwnOptions.UseTableStyleRowBandSize := True;
  end;
  if not AOwnOptions.UseTableIndent and AOptions.UseTableIndent then
  begin
    AOwnInfo.TableIndent.CopyFrom(AInfo.TableIndent);
    AOwnOptions.UseTableIndent := True;
  end;
  if not AOwnOptions.UsePreferredWidth and AOptions.UsePreferredWidth then
  begin
    AOwnInfo.PreferredWidth.CopyFrom(AInfo.PreferredWidth);
    AOwnOptions.UsePreferredWidth := True;
  end;
end;

{ TdxTableCellGeneralSettingsInfo }

function TdxTableCellGeneralSettingsInfo.Clone: TdxTableCellGeneralSettingsInfo;
begin
  Result := TdxTableCellGeneralSettingsInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxTableCellGeneralSettingsInfo.CopyFrom(const AInfo: TdxTableCellGeneralSettingsInfo);
begin
  HideCellMark := AInfo.HideCellMark;
  NoWrap := AInfo.NoWrap;
  FitText := AInfo.FitText;
  TextDirection := AInfo.TextDirection;
  VerticalAlignment := AInfo.VerticalAlignment;
  ColumnSpan := AInfo.ColumnSpan;
  HorizontalMerging := AInfo.HorizontalMerging;
  VerticalMerging := AInfo.VerticalMerging;
  CellConditionalFormatting := AInfo.CellConditionalFormatting;
  BackgroundColor := AInfo.BackgroundColor;
end;

constructor TdxTableCellGeneralSettingsInfo.Create;
begin
  inherited Create;
  FColumnSpan := 1;
end;

function TdxTableCellGeneralSettingsInfo.Equals(AObject: TObject): Boolean;
var
  AInfo: TdxTableCellGeneralSettingsInfo;
begin
  if AObject is TdxTableCellGeneralSettingsInfo then
  begin
    AInfo := TdxTableCellGeneralSettingsInfo(AObject);
    Result := (HideCellMark = AInfo.HideCellMark) and
      (NoWrap = AInfo.NoWrap) and
      (FitText = AInfo.FitText) and
      (TextDirection = AInfo.TextDirection) and
      (VerticalAlignment = AInfo.VerticalAlignment) and
      (ColumnSpan = AInfo.ColumnSpan) and
      (HorizontalMerging = AInfo.HorizontalMerging) and
      (VerticalMerging = AInfo.VerticalMerging) and
      (CellConditionalFormatting = AInfo.CellConditionalFormatting) and
      (BackgroundColor = AInfo.BackgroundColor);
  end
  else
    Result := False;
end;

{ TdxTableCellGeneralSettings }

procedure TdxTableCellGeneralSettings.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
begin
  Owner.ApplyChanges(AChangeActions);
end;

procedure TdxTableCellGeneralSettings.BeginChanging(AChangedProperty: TdxProperties);
begin
  Owner.BeginChanging(AChangedProperty);
end;

procedure TdxTableCellGeneralSettings.CopyFrom(ANewSettings: TdxTableCellGeneralSettings);
begin
  Owner.BeginPropertiesUpdate;
  try
    BeginUpdate;
    try
      HideCellMark := ANewSettings.HideCellMark;
      NoWrap := ANewSettings.NoWrap;
      FitText := ANewSettings.FitText;
      TextDirection := ANewSettings.TextDirection;
      VerticalAlignment := ANewSettings.VerticalAlignment;
      ColumnSpan := ANewSettings.ColumnSpan;
      VerticalMerging := ANewSettings.VerticalMerging;
      CellConditionalFormatting := ANewSettings.CellConditionalFormatting;
      BackgroundColor := ANewSettings.BackgroundColor;
    finally
      EndUpdate;
    end;
  finally
    Owner.EndPropertiesUpdate;
  end;
end;

constructor TdxTableCellGeneralSettings.Create(APieceTable: TObject; AOwner: IdxPropertiesContainer);
begin
  inherited Create(APieceTable as TdxPieceTable);
  Assert(AOwner <> nil); 
  FOwner := AOwner;
end;

procedure TdxTableCellGeneralSettings.EndChanging;
begin
  Owner.EndChanging;
end;

function TdxTableCellGeneralSettings.GetBackgroundColor: TColor;
begin
  Result := Info.BackgroundColor;
end;

function TdxTableCellGeneralSettings.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.BatchUpdate);
end;

function TdxTableCellGeneralSettings.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableCellGeneralSettingsInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TableCellGeneralSettingsInfoCache;
end;

function TdxTableCellGeneralSettings.GetCellConditionalFormatting: TdxConditionalTableStyleFormattingTypes;
begin
  Result := Info.CellConditionalFormatting;
end;

function TdxTableCellGeneralSettings.GetColumnSpan: Integer;
begin
  Result := Info.ColumnSpan;
end;

function TdxTableCellGeneralSettings.GetFitText: Boolean;
begin
  Result := Info.FitText;
end;

function TdxTableCellGeneralSettings.GetHideCellMark: Boolean;
begin
  Result := Info.HideCellMark;
end;

function TdxTableCellGeneralSettings.GetNoWrap: Boolean;
begin
  Result := Info.NoWrap;
end;

function TdxTableCellGeneralSettings.GetTextDirection: TdxTextDirection;
begin
  Result := Info.TextDirection;
end;

function TdxTableCellGeneralSettings.GetVerticalAlignment: TdxVerticalAlignment;
begin
  Result := Info.VerticalAlignment;
end;

function TdxTableCellGeneralSettings.GetVerticalMerging: TdxMergingState;
begin
  Result := Info.VerticalMerging;
end;

procedure TdxTableCellGeneralSettings.InnerSetBackgroundColor(const Value: TColor);
begin
  BeginChanging(TdxProperties.BackgroundColor);
  if Value = Info.BackgroundColor then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<TColor>(SetBackgroundColor, Value);
  EndChanging;
end;

procedure TdxTableCellGeneralSettings.InnerSetCellConditionalFormatting(
  const Value: TdxConditionalTableStyleFormattingTypes);
begin
  BeginChanging(TdxProperties.CellConditionalFormatting);
  if Value = Info.CellConditionalFormatting then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<TdxConditionalTableStyleFormattingTypes>(SetCellConditionalFormatting, Value);
  EndChanging;
end;

function TdxTableCellGeneralSettings.SetBackgroundColor(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: TColor): TdxDocumentModelChangeActions;
begin
  AInfo.BackgroundColor := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.BackgroundColor);
end;

function TdxTableCellGeneralSettings.SetCellConditionalFormatting(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: TdxConditionalTableStyleFormattingTypes): TdxDocumentModelChangeActions;
begin
  AInfo.CellConditionalFormatting := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.ConditionalFormatting);
end;

function TdxTableCellGeneralSettings.SetColumnSpan(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.ColumnSpan := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.ColumnSpan);
end;

procedure TdxTableCellGeneralSettings.InnerSetColumnSpan(const Value: Integer);
begin
  if Value <= 0 then
    raise Exception.Create('Error Message'); 
  BeginChanging(TdxProperties.ColumnSpan);
  if Value = Info.ColumnSpan then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<Integer>(SetColumnSpan, Value);
  EndChanging;
end;

function TdxTableCellGeneralSettings.SetFitText(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.FitText := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.FitText);
end;

procedure TdxTableCellGeneralSettings.InnerSetFitText(const Value: Boolean);
begin
  BeginChanging(TdxProperties.FitText);
  if Value = Info.FitText then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<Boolean>(SetFitText, Value);
  EndChanging;
end;

function TdxTableCellGeneralSettings.SetHideCellMark(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.HideCellMark := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.HideCellMark);
end;

function TdxTableCellGeneralSettings.SetNoWrap(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.NoWrap := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.NoWrap);
end;

function TdxTableCellGeneralSettings.SetTextDirection(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: TdxTextDirection): TdxDocumentModelChangeActions;
begin
  AInfo.TextDirection := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.TextDirection);
end;

procedure TdxTableCellGeneralSettings.InnerSetHideCellMark(const Value: Boolean);
begin
  BeginChanging(TdxProperties.HideCellMark);
  if Value = Info.HideCellMark then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<Boolean>(SetHideCellMark, Value);
  EndChanging;
end;

procedure TdxTableCellGeneralSettings.InnerSetNoWrap(const Value: Boolean);
begin
  BeginChanging(TdxProperties.NoWrap);
  if Value = Info.NoWrap then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<Boolean>(SetNoWrap, Value);
  EndChanging;
end;

procedure TdxTableCellGeneralSettings.InnerSetTextDirection(const Value: TdxTextDirection);
begin
  BeginChanging(TdxProperties.TextDirection);
  if Value = Info.TextDirection then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<TdxTextDirection>(SetTextDirection, Value);
  EndChanging;
end;

procedure TdxTableCellGeneralSettings.InnerSetVerticalAlignment(const Value: TdxVerticalAlignment);
begin
  BeginChanging(TdxProperties.VerticalAlignment);
  if Value = Info.VerticalAlignment then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<TdxVerticalAlignment>(SetVerticalAlignment, Value);
  EndChanging;
end;

function TdxTableCellGeneralSettings.SetVerticalAlignment(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: TdxVerticalAlignment): TdxDocumentModelChangeActions;
begin
  AInfo.VerticalAlignment := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.VerticalAlignment);
end;

function TdxTableCellGeneralSettings.SetVerticalMerging(const AInfo: TdxTableCellGeneralSettingsInfo;
  const AValue: TdxMergingState): TdxDocumentModelChangeActions;
begin
  AInfo.VerticalMerging := AValue;
  Result := TdxTableCellChangeActionsCalculator.CalculateChangeActions(TdxTableCellChangeType.VerticalMerging);
end;

procedure TdxTableCellGeneralSettings.InnerSetVerticalMerging(const Value: TdxMergingState);
begin
  BeginChanging(TdxProperties.VerticalMerging);
  if Value = Info.VerticalMerging then
  begin
    EndChanging;
    Exit;
  end;
  SetPropertyValue<TdxMergingState>(SetVerticalMerging, Value);
  EndChanging;
end;

procedure TdxTableCellGeneralSettings.RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
begin
  Owner.RaiseObtainAffectedRange(AArgs);
end;

{ TdxTableCellChangeActionsCalculator }

class function TdxTableCellChangeActionsCalculator.CalculateChangeActions(
  AChange: TdxTableCellChangeType): TdxDocumentModelChangeActions;
const
  ATableCellChangeActionsTable: array[TdxTableCellChangeType] of TdxDocumentModelChangeActions = (
    [], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler, TdxDocumentModelChangeAction.ForceResetVerticalRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler, TdxDocumentModelChangeAction.ForceResetVerticalRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
     TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSecondaryLayout,
      TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler, TdxDocumentModelChangeAction.ForceResetVerticalRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler, TdxDocumentModelChangeAction.ForceResetVerticalRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler, TdxDocumentModelChangeAction.ForceResetVerticalRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler, TdxDocumentModelChangeAction.ForceResetVerticalRuler], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSecondaryLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ForceResetHorizontalRuler, TdxDocumentModelChangeAction.ForceResetVerticalRuler] 
  );
begin
  Result := ATableCellChangeActionsTable[AChange];
end;

{ TdxTableCellGeneralSettingsInfoCache }

function TdxTableCellGeneralSettingsInfoCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableCellGeneralSettingsInfo;
begin
  Result := TdxTableCellGeneralSettingsInfo.Create;
  Result.TextDirection := TdxTextDirection.LeftToRightTopToBottom;
  Result.VerticalAlignment := TdxVerticalAlignment.Top;
  Result.ColumnSpan := 1;
  Result.HorizontalMerging := TdxMergingState.None;
  Result.VerticalMerging := TdxMergingState.None;
  Result.CellConditionalFormatting := TdxConditionalTableStyleFormattingTypes.WholeTable;
  Result.BackgroundColor := TdxColor.Empty;
end;

{ TdxTableRowPropertiesOptions }

constructor TdxTableRowPropertiesOptions.Create(AValue: Integer = 0);
begin
  inherited Create;
  FValue := AValue;
end;

function TdxTableRowPropertiesOptions.GetVal(AMask: Integer): Boolean;
begin
  Result := (FValue and AMask) <> 0;
end;

procedure TdxTableRowPropertiesOptions.SetVal(AMask: Integer; ABitValue: Boolean);
begin
  if ABitValue then
    FValue := FValue or AMask
  else
    FValue := FValue and not AMask;
end;

function TdxTableRowPropertiesOptions.Clone: TdxTableRowPropertiesOptions;
begin
  Result := TdxTableRowPropertiesOptions.Create;
  Result.CopyFrom(Self);
end;

procedure TdxTableRowPropertiesOptions.CopyFrom(const AOptions: TdxTableRowPropertiesOptions);
begin
  FValue := AOptions.FValue;
end;

function TdxTableRowPropertiesOptions.Equals(AObject: TObject): Boolean;
begin
  if AObject is TdxTableRowPropertiesOptions then
    Result := Value = TdxTableRowPropertiesOptions(AObject).Value
  else
    Result := False;
end;

function TdxTableRowPropertiesOptions.GetHashCode: Integer;
begin
  Result := Value;
end;

function TdxTableRowPropertiesOptions.GetUseCantSplit: Boolean;
begin
  Result := GetVal(MaskUseCantSplit);
end;

function TdxTableRowPropertiesOptions.GetUseCellSpacing: Boolean;
begin
  Result := GetVal(MaskUseCellSpacing);
end;

function TdxTableRowPropertiesOptions.GetUseGridAfter: Boolean;
begin
  Result := GetVal(MaskUseGridAfter);
end;

function TdxTableRowPropertiesOptions.GetUseGridBefore: Boolean;
begin
  Result := GetVal(MaskUseGridBefore);
end;

function TdxTableRowPropertiesOptions.GetUseHeader: Boolean;
begin
  Result := GetVal(MaskUseHeader);
end;

function TdxTableRowPropertiesOptions.GetUseHeight: Boolean;
begin
  Result := GetVal(MaskUseHeight);
end;

function TdxTableRowPropertiesOptions.GetUseHideCellMark: Boolean;
begin
  Result := GetVal(MaskUseHideCellMark);
end;

function TdxTableRowPropertiesOptions.GetUseTableRowAlignment: Boolean;
begin
  Result := GetVal(MaskUseTableRowAlignment);
end;

function TdxTableRowPropertiesOptions.GetUseWidthAfter: Boolean;
begin
  Result := GetVal(MaskUseWidthAfter);
end;

function TdxTableRowPropertiesOptions.GetUseWidthBefore: Boolean;
begin
  Result := GetVal(MaskUseWidthBefore);
end;

procedure TdxTableRowPropertiesOptions.SetUseCantSplit(const AValue: Boolean);
begin
  SetVal(MaskUseCantSplit, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseCellSpacing(const AValue: Boolean);
begin
  SetVal(MaskUseCellSpacing, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseGridAfter(const AValue: Boolean);
begin
  SetVal(MaskUseGridAfter, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseGridBefore(const AValue: Boolean);
begin
  SetVal(MaskUseGridBefore, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseHeader(const AValue: Boolean);
begin
  SetVal(MaskUseHeader, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseHeight(const AValue: Boolean);
begin
  SetVal(MaskUseHeight, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseHideCellMark(const AValue: Boolean);
begin
  SetVal(MaskUseHideCellMark, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseTableRowAlignment(const AValue: Boolean);
begin
  SetVal(MaskUseTableRowAlignment, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseWidthAfter(const AValue: Boolean);
begin
  SetVal(MaskUseWidthAfter, AValue);
end;

procedure TdxTableRowPropertiesOptions.SetUseWidthBefore(const AValue: Boolean);
begin
  SetVal(MaskUseWidthBefore, AValue);
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseCantSplit(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseCantSplit;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseCellSpacing(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseCellSpacing;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseGridAfter(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseGridAfter;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseGridBefore(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseGridBefore;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseHeader(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseHeader;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseHeight(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseHeight;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseHideCellMark(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseHideCellMark;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseTableRowAlignment(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseTableRowAlignment;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseWidthAfter(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseWidthAfter;
end;

class function TdxTableRowPropertiesOptions.GetOptionsUseWidthBefore(
  AOptions: TdxTableRowPropertiesOptions): Boolean;
begin
  Result := AOptions.UseWidthBefore;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseCantSplit(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseCantSplit := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseCellSpacing(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseCellSpacing := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseGridAfter(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseGridAfter := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseGridBefore(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseGridBefore := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseHeader(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseHeader := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseHeight(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseHeight := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseHideCellMark(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseHideCellMark := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseTableRowAlignment(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseTableRowAlignment := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseWidthAfter(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseWidthAfter := AValue;
end;

class procedure TdxTableRowPropertiesOptions.SetOptionsUseWidthBefore(
  AOptions: TdxTableRowPropertiesOptions; const AValue: Boolean);
begin
  AOptions.UseWidthBefore := AValue;
end;

{ TdxCombinedTableRowPropertiesInfo }

constructor TdxCombinedTableRowPropertiesInfo.Create;
begin
  inherited Create;
  FHeight := TdxHeightUnitInfo.Create;
  FWidthBefore := TdxWidthUnitInfo.Create;
  FWidthAfter := TdxWidthUnitInfo.Create;
  FCellSpacing := TdxWidthUnitInfo.Create;
  FGeneralSettings := TdxTableRowGeneralSettingsInfo.Create;
end;

constructor TdxCombinedTableRowPropertiesInfo.Create(ARowProperties: TdxTableRowProperties);
begin
  inherited Create;
  FHeight := ARowProperties.Height.Info;
  FWidthBefore := ARowProperties.WidthBefore.Info;
  FWidthAfter := ARowProperties.WidthAfter.Info;
  FCellSpacing := ARowProperties.CellSpacing.Info;
  FGeneralSettings := ARowProperties.GeneralSettings.Info;
end;

procedure TdxCombinedTableRowPropertiesInfo.CopyFrom(const AValue: TdxCombinedTableRowPropertiesInfo);
begin
  Height.CopyFrom(AValue.Height);
  WidthBefore.CopyFrom(AValue.WidthBefore);
  WidthAfter.CopyFrom(AValue.WidthAfter);
  CellSpacing.CopyFrom(AValue.CellSpacing);
  GeneralSettings.CopyFrom(AValue.GeneralSettings);
end;

function TdxCombinedTableRowPropertiesInfo.Clone: TdxCombinedTableRowPropertiesInfo;
begin
  Result := TdxCombinedTableRowPropertiesInfo.Create;
  Result.CopyFrom(Self);
end;

{ TdxTableRowPropertiesMerger }

constructor TdxTableRowPropertiesMerger.Create(AInitialProperties: TdxTableRowProperties);
begin
  inherited Create(TdxMergedTableRowProperties.Create(TdxCombinedTableRowPropertiesInfo.Create(AInitialProperties), AInitialProperties.Info));
end;

constructor TdxTableRowPropertiesMerger.Create(AInitialProperties: TdxMergedTableRowProperties);
begin
  inherited Create(TdxMergedTableRowProperties.Create(AInitialProperties.Info, AInitialProperties.Options));
end;

procedure TdxTableRowPropertiesMerger.Merge(AProperties: TdxTableRowProperties);
begin
  MergeCore(TdxCombinedTableRowPropertiesInfo.Create(AProperties), AProperties.Info);
end;

procedure TdxTableRowPropertiesMerger.MergeCore(const AInfo: TdxCombinedTableRowPropertiesInfo;
  const AOptions: TdxTableRowPropertiesOptions);
begin
  if not OwnOptions.UseCellSpacing and AOptions.UseCellSpacing then
  begin
    OwnInfo.CellSpacing.CopyFrom(AInfo.CellSpacing);
    OwnOptions.UseCellSpacing := True;
  end;
  if not OwnOptions.UseHeight and AOptions.UseHeight then
  begin
    OwnInfo.Height.CopyFrom(AInfo.Height);
    OwnOptions.UseHeight := True;
  end;
  if not OwnOptions.UseWidthBefore and AOptions.UseWidthBefore then
  begin
    OwnInfo.WidthBefore.CopyFrom(AInfo.WidthBefore);
    OwnOptions.UseWidthBefore := True;
  end;
  if not OwnOptions.UseWidthAfter and AOptions.UseWidthAfter then
  begin
    OwnInfo.WidthAfter.CopyFrom(AInfo.WidthAfter);
    OwnOptions.UseWidthAfter := True;
  end;
  if not OwnOptions.UseCantSplit and AOptions.UseCantSplit then
  begin
    OwnInfo.GeneralSettings.CantSplit := AInfo.GeneralSettings.CantSplit;
    OwnOptions.UseCantSplit := True;
  end;
  if not OwnOptions.UseGridAfter and AOptions.UseGridAfter then
  begin
    OwnInfo.GeneralSettings.GridAfter := AInfo.GeneralSettings.GridAfter;
    OwnOptions.UseGridAfter := True;
  end;
  if not OwnOptions.UseGridBefore and AOptions.UseGridBefore then
  begin
    OwnInfo.GeneralSettings.GridBefore := AInfo.GeneralSettings.GridBefore;
    OwnOptions.UseGridBefore := True;
  end;
  if not OwnOptions.UseHeader and AOptions.UseHeader then
  begin
    OwnInfo.GeneralSettings.Header := AInfo.GeneralSettings.Header;
    OwnOptions.UseHeader := True;
  end;
  if not OwnOptions.UseHideCellMark and AOptions.UseHideCellMark then
  begin
    OwnInfo.GeneralSettings.HideCellMark := AInfo.GeneralSettings.HideCellMark;
    OwnOptions.UseHideCellMark := True;
  end;
  if not OwnOptions.UseTableRowAlignment and AOptions.UseTableRowAlignment then
  begin
    OwnInfo.GeneralSettings.TableRowAlignment := AInfo.GeneralSettings.TableRowAlignment;
    OwnOptions.UseTableRowAlignment := True;
  end;
end;

{ TdxTableRowProperties }

procedure TdxTableRowProperties.CopyFrom(AProperties: TdxTableRowProperties);
var
  AContainer: IdxPropertiesContainer;
  AInfo: TdxTableRowPropertiesOptions;
  AIsDeferredInfo: Boolean;
begin
  Supports(Self, IdxPropertiesContainer, AContainer);
  AContainer.BeginPropertiesUpdate;
  try
    if DocumentModel = AProperties.DocumentModel then
    begin
      Height.CopyFrom(AProperties.Height);
      WidthBefore.CopyFrom(AProperties.WidthBefore);
      WidthAfter.CopyFrom(AProperties.WidthAfter);
      CellSpacing.CopyFrom(AProperties.CellSpacing);
      GeneralSettings.CopyFrom(AProperties.GeneralSettings);
      AInfo := GetInfoForModification(AIsDeferredInfo);
      AInfo.CopyFrom(AProperties.Info);
      ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
      if not AIsDeferredInfo then AInfo.Free;
    end
    else
    begin
      Height.CopyFrom(AProperties.Height.Info);
      WidthBefore.CopyFrom(AProperties.WidthBefore.Info);
      WidthAfter.CopyFrom(AProperties.WidthAfter.Info);
      CellSpacing.CopyFrom(AProperties.CellSpacing.Info);
      GeneralSettings.CopyFrom(AProperties.GeneralSettings.Info);
      Info.CopyFrom(AProperties.Info);
    end;
  finally
    AContainer.EndPropertiesUpdate;
  end;
end;

constructor TdxTableRowProperties.Create(const ADocumentModelPart: TdxDocumentModelPart);
begin
  inherited Create(ADocumentModelPart as TdxPieceTable);
  FHeight := TdxRowHeight.Create(PieceTable, Self);
  FWidthBefore := TdxWidthBefore.Create(PieceTable, Self);
  FWidthAfter := TdxWidthAfter.Create(PieceTable, Self);
  FCellSpacing := TdxCellSpacing.Create(PieceTable, Self);
  FGeneralSettings := TdxTableRowGeneralSettings.Create(PieceTable, Self);
end;

destructor TdxTableRowProperties.Destroy;
begin
  FGeneralSettings.Free;
  FCellSpacing.Free;
  FWidthAfter.Free;
  FWidthBefore.Free;
  FHeight.Free;
  inherited Destroy;
end;

class constructor TdxTableRowProperties.Initialize;
begin
  FAccessorTable := CreateAccessorTable;
end;

class destructor TdxTableRowProperties.Finalize;
begin
  FreeAndNil(FAccessorTable);
end;

class function TdxTableRowProperties.CreateAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>>;
begin
  Result := TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>>.Create;
  Result.Add(TdxProperties.CantSplit, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseCantSplit, TdxTableRowPropertiesOptions.SetOptionsUseCantSplit));
  Result.Add(TdxProperties.CellSpacing, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseCellSpacing, TdxTableRowPropertiesOptions.SetOptionsUseCellSpacing));
  Result.Add(TdxProperties.GridAfter, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseGridAfter, TdxTableRowPropertiesOptions.SetOptionsUseGridAfter));
  Result.Add(TdxProperties.GridBefore, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseGridBefore, TdxTableRowPropertiesOptions.SetOptionsUseGridBefore));
  Result.Add(TdxProperties.Header, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseHeader, TdxTableRowPropertiesOptions.SetOptionsUseHeader));
  Result.Add(TdxProperties.RowHeight, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseHeight, TdxTableRowPropertiesOptions.SetOptionsUseHeight));
  Result.Add(TdxProperties.HideCellMark, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseHideCellMark, TdxTableRowPropertiesOptions.SetOptionsUseHideCellMark));
  Result.Add(TdxProperties.TableRowAlignment, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseTableRowAlignment, TdxTableRowPropertiesOptions.SetOptionsUseTableRowAlignment));
  Result.Add(TdxProperties.WidthAfter, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseWidthAfter, TdxTableRowPropertiesOptions.SetOptionsUseWidthAfter));
  Result.Add(TdxProperties.WidthBefore, TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>.Create(TdxTableRowPropertiesOptions.GetOptionsUseWidthBefore, TdxTableRowPropertiesOptions.SetOptionsUseWidthBefore));
end;

function TdxTableRowProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [];
end;

function TdxTableRowProperties.GetAccessorTable: TdxPropertiesDictionary<TdxBoolPropertyAccessor<TdxTableRowPropertiesOptions>>;
begin
  Result := FAccessorTable;
end;

function TdxTableRowProperties.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableRowPropertiesOptions>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TableRowPropertiesOptionsCache;
end;

function TdxTableRowProperties.GetCantSplit: Boolean;
begin
  Result := GeneralSettings.CantSplit;
end;

function TdxTableRowProperties.GetGridAfter: Integer;
begin
  Result := GeneralSettings.GridAfter;
end;

function TdxTableRowProperties.GetGridBefore: Integer;
begin
  Result := GeneralSettings.GridBefore;
end;

function TdxTableRowProperties.GetHeader: Boolean;
begin
  Result := GeneralSettings.Header;
end;

function TdxTableRowProperties.GetHideCellMark: Boolean;
begin
  Result := GeneralSettings.HideCellMark;
end;

function TdxTableRowProperties.GetTableRowAlignment: TdxTableRowAlignment;
begin
  Result := GeneralSettings.TableRowAlignment;
end;

function TdxTableRowProperties.GetUse(AMask: Integer): Boolean;
begin
  Result := Info.GetVal(AMask);
end;

function TdxTableRowProperties.GetUseCantSplit: Boolean;
begin
  Result := Info.UseCantSplit;
end;

function TdxTableRowProperties.GetUseCellSpacing: Boolean;
begin
  Result := Info.UseCellSpacing;
end;

function TdxTableRowProperties.GetUseGridAfter: Boolean;
begin
  Result := Info.UseGridAfter;
end;

function TdxTableRowProperties.GetUseGridBefore: Boolean;
begin
  Result := Info.UseGridBefore;
end;

function TdxTableRowProperties.GetUseHeader: Boolean;
begin
  Result := Info.UseHeader;
end;

function TdxTableRowProperties.GetUseHeight: Boolean;
begin
  Result := Info.UseHeight;
end;

function TdxTableRowProperties.GetUseHideCellMark: Boolean;
begin
  Result := Info.UseHideCellMark;
end;

function TdxTableRowProperties.GetUseTableRowAlignment: Boolean;
begin
  Result := Info.UseTableRowAlignment;
end;

function TdxTableRowProperties.GetUseWidthAfter: Boolean;
begin
  Result := Info.UseWidthAfter;
end;

function TdxTableRowProperties.GetUseWidthBefore: Boolean;
begin
  Result := Info.UseWidthBefore;
end;

procedure TdxTableRowProperties.Merge(AProperties: TdxTableRowProperties);
var
  AContainer: IdxPropertiesContainer;
begin
  Supports(Self, IdxPropertiesContainer, AContainer);
  AContainer.BeginPropertiesUpdate;
  try
    if not UseCantSplit and AProperties.UseCantSplit then
      CantSplit := AProperties.CantSplit;
    if not UseCellSpacing and AProperties.UseCellSpacing then
      CellSpacing.CopyFrom(AProperties.CellSpacing);
    if not UseGridAfter and AProperties.UseGridAfter then
      GridAfter := AProperties.GridAfter;
    if not UseGridBefore and AProperties.UseGridBefore then
      GridBefore := AProperties.GridBefore;
    if not UseHeader and AProperties.UseHeader then
      Header := AProperties.Header;
    if not UseHeight and AProperties.UseHeight then
      Height.CopyFrom(AProperties.Height);
    if not UseHideCellMark and AProperties.HideCellMark then
      HideCellMark := AProperties.HideCellMark;
    if not UseTableRowAlignment and AProperties.UseTableRowAlignment then
      TableRowAlignment := AProperties.TableRowAlignment;
    if not UseWidthBefore and AProperties.UseWidthBefore then
      WidthBefore.CopyFrom(AProperties.WidthBefore);
    if not UseWidthAfter and AProperties.UseWidthAfter then
      WidthAfter.CopyFrom(AProperties.WidthAfter);
  finally
    AContainer.EndPropertiesUpdate();
  end;
end;

procedure TdxTableRowProperties.Reset;
begin
  DocumentModel.History.BeginTransaction;
  try
    CopyFrom(TdxDocumentModel(DocumentModel).DefaultTableRowProperties);
    ResetAllUse;
  finally
    DocumentModel.History.EndTransaction;
  end;
end;

procedure TdxTableRowProperties.ResetAllUse;
begin
  ReplaceInfo(GetCache(DocumentModel)[TdxTableRowPropertiesOptionsCache.EmptyRowPropertiesOptionsItem], GetBatchUpdateChangeActions);
end;

procedure TdxTableRowProperties.ResetUse(AMask: Integer);
var
  ANewOptions: TdxTableRowPropertiesOptions;
begin
  ANewOptions := TdxTableRowPropertiesOptions.Create(Info.Value and not AMask);
  try
    ReplaceInfo(ANewOptions, GetBatchUpdateChangeActions);
  finally
    ANewOptions.Free;
  end;
end;

procedure TdxTableRowProperties.SetCantSplit(const Value: Boolean);
begin
  GeneralSettings.CantSplit := Value;
end;

procedure TdxTableRowProperties.SetGridAfter(const Value: Integer);
begin
  GeneralSettings.GridAfter := Value;
end;

procedure TdxTableRowProperties.SetGridBefore(const Value: Integer);
begin
  GeneralSettings.GridBefore := Value;
end;

procedure TdxTableRowProperties.SetHeader(const Value: Boolean);
begin
  GeneralSettings.Header := Value;
end;

procedure TdxTableRowProperties.SetHideCellMark(const Value: Boolean);
begin
  GeneralSettings.HideCellMark := Value;
end;

procedure TdxTableRowProperties.SetTableRowAlignment(const Value: TdxTableRowAlignment);
begin
  GeneralSettings.TableRowAlignment := Value;
end;

{ TdxTableRowGeneralSettingsInfo }

function TdxTableRowGeneralSettingsInfo.Clone: TdxTableRowGeneralSettingsInfo;
begin
  Result := TdxTableRowGeneralSettingsInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxTableRowGeneralSettingsInfo.CopyFrom(const AInfo: TdxTableRowGeneralSettingsInfo);
begin
  CantSplit := AInfo.CantSplit;
  HideCellMark := AInfo.HideCellMark;
  Header := AInfo.Header;
  GridBefore := AInfo.GridBefore;
  GridAfter := AInfo.GridAfter;
  TableRowAlignment := AInfo.TableRowAlignment;
end;

function TdxTableRowGeneralSettingsInfo.Equals(AObject: TObject): Boolean;
var
  AInfo: TdxTableRowGeneralSettingsInfo;
begin
  if AObject is TdxTableRowGeneralSettingsInfo then
  begin
    AInfo := TdxTableRowGeneralSettingsInfo(AObject);
    Result := (CantSplit = AInfo.CantSplit) and
      (HideCellMark = AInfo.HideCellMark) and
      (Header = AInfo.Header) and
      (GridBefore = AInfo.GridBefore) and
      (GridAfter = AInfo.GridAfter) and
      (TableRowAlignment = AInfo.TableRowAlignment);
  end
  else
    Result := False;
end;

{ TdxTableRowGeneralSettingsInfoCache }

function TdxTableRowGeneralSettingsInfoCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableRowGeneralSettingsInfo;
begin
  Result := TdxTableRowGeneralSettingsInfo.Create;
  Result.TableRowAlignment := TdxTableRowAlignment.Left;
end;

{ TdxTableRowGeneralSettings }

procedure TdxTableRowGeneralSettings.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
begin
  Owner.ApplyChanges(AChangeActions);
end;

procedure TdxTableRowGeneralSettings.BeginChanging(AChangedProperty: TdxProperties);
begin
  Owner.BeginChanging(AChangedProperty);
end;

procedure TdxTableRowGeneralSettings.CopyFrom(const ASettings: TdxTableRowGeneralSettings);
begin
  Owner.BeginPropertiesUpdate;
  try
    BeginUpdate;
    try
      CantSplit := ASettings.CantSplit;
      HideCellMark := ASettings.HideCellMark;
      Header := ASettings.Header;
      GridBefore := ASettings.GridBefore;
      GridAfter := ASettings.GridAfter;
      TableRowAlignment := ASettings.TableRowAlignment;
    finally
      EndUpdate;
    end;
  finally
    Owner.EndPropertiesUpdate;
  end;
end;

constructor TdxTableRowGeneralSettings.Create(APieceTable: TObject;
  const AOwner: IdxPropertiesContainer);
begin
  inherited Create(APieceTable as TdxPieceTable);
  Assert(AOwner <> nil);
  FOwner := AOwner;
end;

procedure TdxTableRowGeneralSettings.EndChanging;
begin
  Owner.EndChanging;
end;

function TdxTableRowGeneralSettings.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxTableRowChangeActionsCalculator.CalculateChangeActions(TdxTableRowChangeType.BatchUpdate);
end;

function TdxTableRowGeneralSettings.GetCache(
  const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxTableRowGeneralSettingsInfo>;
begin
  Result := TdxDocumentModel(ADocumentModel).Cache.TableRowGeneralSettingsInfoCache;
end;

function TdxTableRowGeneralSettings.GetCantSplit: Boolean;
begin
  Result := Info.CantSplit;
end;

function TdxTableRowGeneralSettings.GetGridAfter: Integer;
begin
  Result := Info.GridAfter;
end;

function TdxTableRowGeneralSettings.GetGridBefore: Integer;
begin
  Result := Info.GridBefore;
end;

function TdxTableRowGeneralSettings.GetHeader: Boolean;
begin
  Result := Info.Header;
end;

function TdxTableRowGeneralSettings.GetHideCellMark: Boolean;
begin
  Result := Info.HideCellMark;
end;

function TdxTableRowGeneralSettings.GetTableRowAlignment: TdxTableRowAlignment;
begin
  Result := Info.TableRowAlignment;
end;

procedure TdxTableRowGeneralSettings.InnerSetCantSplit(const Value: Boolean);
begin
  BeginChanging(TdxProperties.CantSplit);
  if Value <> Info.CantSplit then
    SetPropertyValue<Boolean>(SetCantSplit, Value);
  EndChanging;
end;

procedure TdxTableRowGeneralSettings.InnerSetGridAfter(const Value: Integer);
begin
  if Value < 0 then
    raise Exception.Create('Error Message'); 
  BeginChanging(TdxProperties.GridAfter);
  if Value <> Info.GridAfter then
    SetPropertyValue<Integer>(SetGridAfter, Value);
  EndChanging;
end;

procedure TdxTableRowGeneralSettings.InnerSetGridBefore(const Value: Integer);
begin
  if Value < 0 then
    raise Exception.Create('Error Message'); 
  BeginChanging(TdxProperties.GridBefore);
  if Value <> Info.GridBefore then
    SetPropertyValue<Integer>(SetGridBefore, Value);
  EndChanging;
end;

procedure TdxTableRowGeneralSettings.InnerSetHeader(const Value: Boolean);
begin
  BeginChanging(TdxProperties.Header);
  if Value <> Info.Header then
    SetPropertyValue<Boolean>(SetHeader, Value);
  EndChanging;
end;

procedure TdxTableRowGeneralSettings.InnerSetHideCellMark(const Value: Boolean);
begin
  BeginChanging(TdxProperties.HideCellMark);
  if Value <> Info.HideCellMark then
    SetPropertyValue<Boolean>(SetHideCellMark, Value);
  EndChanging;
end;

procedure TdxTableRowGeneralSettings.InnerSetTableRowAlignment(const Value: TdxTableRowAlignment);
begin
  BeginChanging(TdxProperties.TableRowAlignment);
  if Value <> Info.TableRowAlignment then
    SetPropertyValue<TdxTableRowAlignment>(SetTableRowAlignment, Value);
  EndChanging;
end;

procedure TdxTableRowGeneralSettings.RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
begin
  Owner.RaiseObtainAffectedRange(AArgs);
end;

function TdxTableRowGeneralSettings.SetCantSplit(const AInfo: TdxTableRowGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.CantSplit := AValue;
  Result := TdxTableRowChangeActionsCalculator.CalculateChangeActions(TdxTableRowChangeType.CantSplit);
end;

function TdxTableRowGeneralSettings.SetGridAfter(const AInfo: TdxTableRowGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.GridAfter := AValue;
  Result := TdxTableRowChangeActionsCalculator.CalculateChangeActions(TdxTableRowChangeType.GridAfter);
end;

function TdxTableRowGeneralSettings.SetGridBefore(const AInfo: TdxTableRowGeneralSettingsInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.GridBefore := AValue;
  Result := TdxTableRowChangeActionsCalculator.CalculateChangeActions(TdxTableRowChangeType.GridBefore);
end;

function TdxTableRowGeneralSettings.SetHeader(const AInfo: TdxTableRowGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.Header := AValue;
  Result := TdxTableRowChangeActionsCalculator.CalculateChangeActions(TdxTableRowChangeType.Header);
end;

function TdxTableRowGeneralSettings.SetHideCellMark(const AInfo: TdxTableRowGeneralSettingsInfo;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.HideCellMark := AValue;
  Result := TdxTableRowChangeActionsCalculator.CalculateChangeActions(TdxTableRowChangeType.HideCellMark);
end;

function TdxTableRowGeneralSettings.SetTableRowAlignment(const AInfo: TdxTableRowGeneralSettingsInfo;
  const AValue: TdxTableRowAlignment): TdxDocumentModelChangeActions;
begin
  AInfo.TableRowAlignment := AValue;
  Result := TdxTableRowChangeActionsCalculator.CalculateChangeActions(TdxTableRowChangeType.TableRowAlignment);
end;

{ TdxRowHeight }

function TdxRowHeight.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetRuler];
end;

procedure TdxRowHeight.OnBeginAssign;
begin
  inherited OnBeginAssign;
  Owner.BeginChanging(TdxProperties.RowHeight);
end;

{ TdxWidthBefore }

procedure TdxWidthBefore.OnBeginAssign;
begin
  inherited OnBeginAssign;
  Owner.BeginChanging(TdxProperties.WidthBefore);
end;

function TdxWidthBefore.SetTypeCore(const AUnit: TdxWidthUnitInfo;
  const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions;
begin
  Result := inherited SetTypeCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

function TdxWidthBefore.SetValueCore(const AUnit: TdxWidthUnitInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  Result := inherited SetValueCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

{ TdxWidthAfter }

procedure TdxWidthAfter.OnBeginAssign;
begin
  inherited OnBeginAssign;
  Owner.BeginChanging(TdxProperties.WidthAfter);
end;

function TdxWidthAfter.SetTypeCore(const AUnit: TdxWidthUnitInfo;
  const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions;
begin
  Result := inherited SetTypeCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

function TdxWidthAfter.SetValueCore(const AUnit: TdxWidthUnitInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  Result := inherited SetValueCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

{ TdxTableRowPropertiesOptionsCache }

procedure TdxTableRowPropertiesOptionsCache.AddRootStyleOptions;
begin
  AppendItem(TdxTableRowPropertiesOptions.Create(TdxTableRowPropertiesOptions.MaskUseAll));
end;

function TdxTableRowPropertiesOptionsCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableRowPropertiesOptions;
begin
  Result := TdxTableRowPropertiesOptions.Create;
end;

procedure TdxTableRowPropertiesOptionsCache.InitItems(const AUnitConverter: IdxDocumentModelUnitConverter);
begin
  inherited InitItems(AUnitConverter);
  AddRootStyleOptions;
end;

{ TdxTableCellPropertiesOptionsCache }

procedure TdxTableCellPropertiesOptionsCache.AddRootStyleOptions;
begin
  AppendItem(TdxTableCellPropertiesOptions.Create(TdxTableCellPropertiesOptions.MaskUseAll));
end;

function TdxTableCellPropertiesOptionsCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxTableCellPropertiesOptions;
begin
  Result := TdxTableCellPropertiesOptions.Create;
end;

procedure TdxTableCellPropertiesOptionsCache.InitItems(const AUnitConverter: IdxDocumentModelUnitConverter);
begin
  inherited InitItems(AUnitConverter);
  AddRootStyleOptions;
end;

{ TdxCellMargins }

procedure TdxCellMargins.CopyFrom(ANewMargins: TdxCellMargins);
begin
  Owner.BeginPropertiesUpdate;
  try
    if Left.DocumentModel = ANewMargins.Left.DocumentModel then
    begin
      Top.CopyFrom(ANewMargins.Top);
      Left.CopyFrom(ANewMargins.Left);
      Right.CopyFrom(ANewMargins.Right);
      Bottom.CopyFrom(ANewMargins.Bottom);
    end
    else
    begin
      Top.CopyFrom(ANewMargins.Top.Info);
      Left.CopyFrom(ANewMargins.Left.Info);
      Right.CopyFrom(ANewMargins.Right.Info);
      Bottom.CopyFrom(ANewMargins.Bottom.Info);
    end;
  finally
    Owner.EndPropertiesUpdate;
  end;
end;

procedure TdxCellMargins.CopyFrom(ANewMargins: TdxCombinedCellMarginsInfo);
begin
  Owner.BeginPropertiesUpdate;
  try
    Top.CopyFrom(ANewMargins.Top);
    Left.CopyFrom(ANewMargins.Left);
    Right.CopyFrom(ANewMargins.Right);
    Bottom.CopyFrom(ANewMargins.Bottom);
  finally
    Owner.EndPropertiesUpdate;
  end;
end;

constructor TdxCellMargins.Create(APieceTable: TObject; AOwner: IdxCellMarginsContainer);
begin
  inherited Create;
  Assert(AOwner <> nil);
  FOwner := AOwner;
  FTop := CreateTopMargin(APieceTable);
  FLeft := CreateLeftMargin(APieceTable);
  FRight := CreateRightMargin(APieceTable);
  FBottom := CreateBottomMargin(APieceTable);
end;

destructor TdxCellMargins.Destroy;
begin
  FBottom.Free;
  FRight.Free;
  FLeft.Free;
  FTop.Free;
  inherited Destroy;
end;

function TdxCellMargins.CreateBottomMargin(APieceTable: TObject): TdxBottomMarginUnit;
begin
  Result := TdxBottomMarginUnit.Create(APieceTable, FOwner);
end;

function TdxCellMargins.CreateLeftMargin(APieceTable: TObject): TdxLeftMarginUnit;
begin
  Result := TdxLeftMarginUnit.Create(APieceTable, FOwner);
end;

function TdxCellMargins.CreateRightMargin(APieceTable: TObject): TdxRightMarginUnit;
begin
  Result := TdxRightMarginUnit.Create(APieceTable, FOwner);
end;

function TdxCellMargins.CreateTopMargin(APieceTable: TObject): TdxTopMarginUnit;
begin
  Result := TdxTopMarginUnit.Create(APieceTable, FOwner);
end;

function TdxCellMargins.GetUseBottomMargin: Boolean;
begin
  Result := Owner.UseBottomMargin;
end;

function TdxCellMargins.GetUseLeftMargin: Boolean;
begin
  Result := Owner.UseLeftMargin;
end;

function TdxCellMargins.GetUseRightMargin: Boolean;
begin
  Result := Owner.UseRightMargin;
end;

function TdxCellMargins.GetUseTopMargin: Boolean;
begin
  Result := Owner.UseTopMargin;
end;

procedure TdxCellMargins.Merge(AMargins: TdxCellMargins);
begin
  if not UseLeftMargin and AMargins.UseLeftMargin then
    Left.CopyFrom(AMargins.Left);
  if not UseRightMargin and AMargins.UseRightMargin then
    Right.CopyFrom(AMargins.Right);
  if not UseTopMargin and AMargins.UseTopMargin then
    Top.CopyFrom(AMargins.Top);
  if not UseBottomMargin and AMargins.UseBottomMargin then
    Bottom.CopyFrom(AMargins.Bottom);
end;

{ TdxTableFloatingPositionChangeActionsCalculator }

class function TdxTableFloatingPositionChangeActionsCalculator.CalculateChangeActions(
  AChange: TdxTableFloatingPositionChangeType): TdxDocumentModelChangeActions;
const
  ATableFloatingPositionChangeActionsTable: array[TdxTableFloatingPositionChangeType] of TdxDocumentModelChangeActions = (
    [],                                                                                                     
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                             
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler]                                                              
  );
begin
  Result := ATableFloatingPositionChangeActionsTable[AChange];
end;

{ TdxCombinedCellMarginsInfo }

function TdxCombinedCellMarginsInfo.Clone: TdxCombinedCellMarginsInfo;
begin
  Result := TdxCombinedCellMarginsInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxCombinedCellMarginsInfo.CopyFrom(AInfo: TdxCombinedCellMarginsInfo);
begin
  FLeft.CopyFrom(AInfo.Left);
  FTop.CopyFrom(AInfo.Top);
  FRight.CopyFrom(AInfo.Right);
  FBottom.CopyFrom(AInfo.Bottom);
end;

constructor TdxCombinedCellMarginsInfo.Create(ACellMargins: TdxCellMargins);
begin
  inherited Create;
  FLeft := ACellMargins.Left.Info;
  FTop := ACellMargins.Top.Info;
  FRight := ACellMargins.Right.Info;
  FBottom := ACellMargins.Bottom.Info;
end;

constructor TdxCombinedCellMarginsInfo.Create;
begin
  inherited Create;
  FLeft := TdxWidthUnitInfo.Create;
  FTop := TdxWidthUnitInfo.Create;
  FRight := TdxWidthUnitInfo.Create;
  FBottom := TdxWidthUnitInfo.Create;
end;

{ TdxTableIndent }

function TdxTableIndent.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
    TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler,
    TdxDocumentModelChangeAction.RaiseContentChanged, TdxDocumentModelChangeAction.ResetSelectionLayout,
    TdxDocumentModelChangeAction.ResetRuler];
end;

procedure TdxTableIndent.OnBeginAssign;
begin
  inherited OnBeginAssign;
  Owner.BeginChanging(TdxProperties.TableIndent);
end;

function TdxTableIndent.SetTypeCore(const AUnit: TdxWidthUnitInfo;
  const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions;
begin
  Result := inherited SetTypeCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

function TdxTableIndent.SetValueCore(const AUnit: TdxWidthUnitInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  Result := inherited SetValueCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

{ TdxCellSpacing }

function TdxCellSpacing.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
    TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetRuler,
    TdxDocumentModelChangeAction.RaiseContentChanged, TdxDocumentModelChangeAction.ResetSelectionLayout,
    TdxDocumentModelChangeAction.ResetRuler];
end;

procedure TdxCellSpacing.OnBeginAssign;
begin
  inherited OnBeginAssign;
  Owner.BeginChanging(TdxProperties.CellSpacing);
end;

function TdxCellSpacing.SetTypeCore(const AUnit: TdxWidthUnitInfo;
  const AValue: TdxWidthUnitType): TdxDocumentModelChangeActions;
begin
  Result := inherited SetTypeCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

function TdxCellSpacing.SetValueCore(const AUnit: TdxWidthUnitInfo;
  const AValue: Integer): TdxDocumentModelChangeActions;
begin
  Result := inherited SetValueCore(AUnit, AValue) + [TdxDocumentModelChangeAction.Redraw,
    TdxDocumentModelChangeAction.ResetPrimaryLayout, TdxDocumentModelChangeAction.ResetSecondaryLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.RaiseContentChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout, TdxDocumentModelChangeAction.ResetRuler];
end;

{ TdxBottomMarginUnit }

function TdxBottomMarginUnit.GetProperty: TdxProperties;
begin
  Result := TdxProperties.BottomMargin;
end;

{ TdxTopMarginUnit }

function TdxTopMarginUnit.GetProperty: TdxProperties;
begin
  Result := TdxProperties.TopMargin;
end;

{ TdxRightMarginUnit }

function TdxRightMarginUnit.GetProperty: TdxProperties;
begin
  Result := TdxProperties.RightMargin;
end;

{ TdxLeftMarginUnit }

function TdxLeftMarginUnit.GetProperty: TdxProperties;
begin
  Result := TdxProperties.LeftMargin;
end;

{ TdxCombinedCellPropertiesInfo }

constructor TdxCombinedCellPropertiesInfo.Create(ACellProperties: TdxTableCellProperties);
begin
  inherited Create;
  FPreferredWidth := ACellProperties.PreferredWidth.Info;
  FCellMargins := TdxCombinedCellMarginsInfo(ACellProperties.CellMargins);
  FBorders := TdxCombinedCellBordersInfo(ACellProperties.Borders);
  FGeneralSettings := ACellProperties.GeneralSettings.Info;
end;

function TdxCombinedCellPropertiesInfo.Clone: TdxCombinedCellPropertiesInfo;
begin
  Result := TdxCombinedCellPropertiesInfo.Create;
  Result.CopyFrom(Self);
end;

procedure TdxCombinedCellPropertiesInfo.CopyFrom(const AValue: TdxCombinedCellPropertiesInfo);
begin
  PreferredWidth.CopyFrom(AValue.PreferredWidth);
  CellMargins.CopyFrom(AValue.CellMargins);
  Borders.CopyFrom(AValue.Borders);
  GeneralSettings.CopyFrom(AValue.GeneralSettings);
end;

constructor TdxCombinedCellPropertiesInfo.Create;
begin
  FPreferredWidth := TdxWidthUnitInfo.Create;
  FCellMargins := TdxCombinedCellMarginsInfo.Create;
  FBorders := TdxCombinedCellBordersInfo.Create;
  FGeneralSettings := TdxTableCellGeneralSettingsInfo.Create;
end;

{ TdxTableRowChangeActionsCalculator }

class function TdxTableRowChangeActionsCalculator.CalculateChangeActions(
  AChange: TdxTableRowChangeType): TdxDocumentModelChangeActions;
const
  ATableRowChangeActionsTable: array[TdxTableRowChangeType] of TdxDocumentModelChangeActions = (
    [],                                                                                                      
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                              
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout], 
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                              
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler],                                                              
    [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
      TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
      TdxDocumentModelChangeAction.ResetRuler]                                                               
  );
begin
  Result := ATableRowChangeActionsTable[AChange];
end;


end.
