﻿{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Html;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Classes, dxRichEdit.Options,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.Core;

type
  { TdxDocumentModelImporter }

  TdxDocumentModelImporter = class abstract
  strict private
    FDocumentModel: TdxCustomDocumentModel;
  public
    constructor Create(ADocumentModel: TdxCustomDocumentModel);

    procedure ThrowInvalidFile; virtual; abstract;

    property DocumentModel: TdxCustomDocumentModel read FDocumentModel;
  end;

  { TdxRichEditDocumentModelImporter }

  TdxRichEditDocumentModelImporter = class abstract(TdxDocumentModelImporter)
  strict private
    FDocumentModel: TdxDocumentModel;
    FOptions: TdxDocumentImporterOptions;
  private
    function GetUnitConverter: TdxDocumentModelUnitConverter;
  public
    constructor Create(ADocumentModel: TdxDocumentModel; AOptions: TdxDocumentImporterOptions);

    property DocumentModel: TdxDocumentModel read FDocumentModel;
    property Options: TdxDocumentImporterOptions read FOptions;
    property UnitConverter: TdxDocumentModelUnitConverter read GetUnitConverter;
  end;

  { TdxHtmlImporter }

  TdxHtmlImporter = class(TdxRichEditDocumentModelImporter)
  public
    procedure ThrowInvalidFile; override;
  end;

implementation

{ TdxDocumentModelImporter }

constructor TdxDocumentModelImporter.Create(ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
end;

{ TdxRichEditDocumentModelImporter }

constructor TdxRichEditDocumentModelImporter.Create(
  ADocumentModel: TdxDocumentModel; AOptions: TdxDocumentImporterOptions);
begin
  inherited Create(ADocumentModel);
  FDocumentModel := ADocumentModel;
  FOptions := AOptions;
end;

function TdxRichEditDocumentModelImporter.GetUnitConverter: TdxDocumentModelUnitConverter;
begin
  Result := DocumentModel.UnitConverter;
end;

{ TdxHtmlImporter }

procedure TdxHtmlImporter.ThrowInvalidFile;
begin
  NotImplemented;
end;

end.
