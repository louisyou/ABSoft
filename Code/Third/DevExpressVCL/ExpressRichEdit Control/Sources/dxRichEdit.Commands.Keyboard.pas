{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Commands.Keyboard;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Graphics, Windows, Controls, Generics.Collections, dxRichEdit.Commands.Insert,
  dxCoreClasses, dxRichEdit.DocumentModel.Core, dxRichEdit.Utils.DataObject, dxRichEdit.DocumentModel.Commands,
  dxRichEdit.View.ViewInfo, dxRichEdit.Control, dxRichEdit.View.Core, dxRichEdit.DocumentModel.PieceTableIterators,
  dxRichEdit.Control.HitTest, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Commands, dxRichEdit.Commands.IDs,
  dxRichEdit.Utils.OfficeImage, dxRichEdit.Options, dxRichEdit.Commands.MultiCommand;

type
  { TdxEnterKeyCommand }

  TdxEnterKeyCommand = class(TdxMultiCommand)
  protected
    procedure CreateCommands; override;
    function ExecutionMode: TdxMultiCommandExecutionMode; override;
    function UpdateUIStateMode: TdxMultiCommandUpdateUIStateMode; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxOpenHyperlinkAtCaretPositionCommand }

  TdxOpenHyperlinkAtCaretPositionCommand = class(TdxRichEditMenuItemSimpleCommand)
  strict private
    FHyperlinkField: TdxField;
  private
    function GetHyperlinkField: TdxField;
  protected
    function GetHyperlinkFieldAtCaretPosition: TdxField;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
    property HyperlinkField: TdxField read GetHyperlinkField;
  public
    procedure ExecuteCore; override;
  end;

  { TdxInsertCopyrightSymbolCommand }

  TdxInsertCopyrightSymbolCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertCopyrightSymbolCoreCommand }

  TdxInsertCopyrightSymbolCoreCommand = class(TdxInsertSpecialCharacterCommandBase)
  protected
    function GetCharacter: Char; override;
  end;

  { TdxInsertRegisteredTrademarkSymbolCommand }

  TdxInsertRegisteredTrademarkSymbolCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertRegisteredTrademarkSymbolCoreCommand }

  TdxInsertRegisteredTrademarkSymbolCoreCommand = class(TdxInsertSpecialCharacterCommandBase)
  protected
    function GetCharacter: Char; override;
  end;

  { TdxInsertTrademarkSymbolCommand }

  TdxInsertTrademarkSymbolCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertTrademarkSymbolCoreCommand }

  TdxInsertTrademarkSymbolCoreCommand = class(TdxInsertSpecialCharacterCommandBase)
  protected
    function GetCharacter: Char; override;
  end;

  { TdxInsertEllipsisCommand }

  TdxInsertEllipsisCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertEllipsisCoreCommand }

  TdxInsertEllipsisCoreCommand = class(TdxInsertSpecialCharacterCommandBase)
  protected
    function GetCharacter: Char; override;
  end;

  { TdxToggleOvertypeCommand }

  TdxToggleOvertypeCommand = class(TdxRichEditMenuItemSimpleCommand)
  protected
    procedure ExecuteCore; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxInsertTabCoreCommand }

  TdxInsertTabCoreCommand = class(TdxInsertSpecialCharacterCommandBase)
  protected
    function GetCharacter: Char; override;
    function GetInsertedText: string; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  end;

  { TdxInsertTabCommand }

  TdxInsertTabCommand = class(TdxTransactedInsertObjectCommand)
  protected
    function CreateInsertObjectCommand: TdxRichEditCommand; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

implementation

uses
  Math, dxRichEdit.DocumentModel.TabFormatting, dxRichEdit.DocumentModel.ParagraphRange, dxRichEdit.View.PageViewInfoGenerator,
  dxRichEdit.DocumentLayout.Position, dxRichEdit.DocumentModel.Styles, dxRichEdit.Utils.Characters, dxRichEdit.DocumentLayout.UnitConverter,
  cxGeometry, dxTypeHelpers, dxRichEdit.Utils.CheckSumStream,
  dxRichEdit.Commands.Selection, dxRichEdit.Commands.Delete, dxRichEdit.Commands.CopyAndPaste;

{ TdxEnterKeyCommand }

procedure TdxEnterKeyCommand.CreateCommands;
begin
  Commands.Add(TdxOpenHyperlinkAtCaretPositionCommand.Create(RichEditControl));
  Commands.Add(TdxInsertParagraphCommand.Create(RichEditControl));
end;

function TdxEnterKeyCommand.ExecutionMode: TdxMultiCommandExecutionMode;
begin
  Result := TdxMultiCommandExecutionMode.ExecuteFirstAvailable;
end;

class function TdxEnterKeyCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.EnterKey;
end;

function TdxEnterKeyCommand.UpdateUIStateMode: TdxMultiCommandUpdateUIStateMode;
begin
  Result := TdxMultiCommandUpdateUIStateMode.EnableIfAnyAvailable;
end;

{ TdxOpenHyperlinkAtCaretPositionCommand }

procedure TdxOpenHyperlinkAtCaretPositionCommand.ExecuteCore;
begin
  if HyperlinkField = nil then
    Exit;
Assert(False, 'not implemented');
end;

function TdxOpenHyperlinkAtCaretPositionCommand.GetHyperlinkField: TdxField;
begin
  if FHyperlinkField = nil then
    FHyperlinkField := GetHyperlinkFieldAtCaretPosition;
  Result := FHyperlinkField;
end;

function TdxOpenHyperlinkAtCaretPositionCommand.GetHyperlinkFieldAtCaretPosition: TdxField;
var
  ALayoutPosition: TdxDocumentLayoutPosition;
  ARunIndex: TdxRunIndex;
  AField: TdxField;
begin
  Result := nil;
  ALayoutPosition := ActiveView.CaretPosition.LayoutPosition;
  if ALayoutPosition.DetailsLevel = TdxDocumentLayoutDetailsLevel.Character then
  begin
    ARunIndex := ALayoutPosition.Character.StartPos.RunIndex;
    AField := ActivePieceTable.GetHyperlinkField(ARunIndex);
    if (AField <> nil)  then
    begin
      Assert(False, 'not implemented');
    end;
  end;
end;

procedure TdxOpenHyperlinkAtCaretPositionCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  AState.Enabled := HyperlinkField <> nil;
  AState.Visible := True;
end;

{ TdxInsertCopyrightSymbolCommand }

function TdxInsertCopyrightSymbolCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertCopyrightSymbolCoreCommand.Create(RichEditControl);
end;

class function TdxInsertCopyrightSymbolCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.InsertCopyrightSymbol;
end;

{ TdxInsertCopyrightSymbolCoreCommand }

function TdxInsertCopyrightSymbolCoreCommand.GetCharacter: Char;
begin
  Result := TdxCharacters.CopyrightSymbol;
end;

{ TdxInsertRegisteredTrademarkSymbolCommand }

function TdxInsertRegisteredTrademarkSymbolCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertRegisteredTrademarkSymbolCoreCommand.Create(RichEditControl);
end;

class function TdxInsertRegisteredTrademarkSymbolCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.InsertRegisteredTrademarkSymbol;
end;

{ TdxInsertRegisteredTrademarkSymbolCoreCommand }

function TdxInsertRegisteredTrademarkSymbolCoreCommand.GetCharacter: Char;
begin
  Result := TdxCharacters.RegisteredTrademarkSymbol;
end;

{ TdxInsertTrademarkSymbolCommand }

function TdxInsertTrademarkSymbolCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertTrademarkSymbolCoreCommand.Create(RichEditControl);
end;

class function TdxInsertTrademarkSymbolCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.InsertTrademarkSymbol;
end;

{ TdxInsertTrademarkSymbolCoreCommand }

function TdxInsertTrademarkSymbolCoreCommand.GetCharacter: Char;
begin
  Result := TdxCharacters.TrademarkSymbol;
end;

{ TdxInsertEllipsisCommand }

function TdxInsertEllipsisCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertEllipsisCoreCommand.Create(RichEditControl);
end;

class function TdxInsertEllipsisCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.InsertEllipsis;
end;

{ TdxInsertEllipsisCoreCommand }

function TdxInsertEllipsisCoreCommand.GetCharacter: Char;
begin
  Result := TdxCharacters.Ellipsis;
end;

{ TdxToggleOvertypeCommand }

procedure TdxToggleOvertypeCommand.ExecuteCore;
begin
  RichEditControl.Overtype := not RichEditControl.Overtype;
end;

class function TdxToggleOvertypeCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ToggleOvertype;
end;

procedure TdxToggleOvertypeCommand.UpdateUIStateCore(
  const AState: IdxCommandUIState);
begin
  AState.Enabled := Options.Behavior.OvertypeAllowed;
  AState.Visible := True;
  AState.Checked := RichEditControl.Overtype;
end;

{ TdxInsertTabCoreCommand }

function TdxInsertTabCoreCommand.GetCharacter: Char;
begin
  Result := TdxCharacters.TabMark;
end;

function TdxInsertTabCoreCommand.GetInsertedText: string;
begin
  Result := InnerControl.Options.Behavior.TabMarker;
end;

procedure TdxInsertTabCoreCommand.UpdateUIStateCore(
  const AState: IdxCommandUIState);
begin
  inherited UpdateUIStateCore(AState);
  ApplyCommandRestrictionOnEditableControl(AState, DocumentModel.DocumentCapabilities.TabSymbol, AState.Enabled); //TODO:DEN: Control.isEdit not nessesary
  ApplyDocumentProtectionToSelectedCharacters(AState);
end;

{ TdxInsertTabCommand }

function TdxInsertTabCommand.CreateInsertObjectCommand: TdxRichEditCommand;
begin
  Result := TdxInsertTabCoreCommand.Create(RichEditControl);
end;

class function TdxInsertTabCommand.Id: TdxRichEditCommandId;
begin
    Result := TdxRichEditCommandId.InsertTab;
end;

end.
