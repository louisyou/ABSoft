{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.DataObject;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, Classes, Windows, ActiveX, Clipbrd, Generics.Collections, dxCoreClasses;

type
  IdxDataObject = interface(IDataObject)
  ['{7DAB74C2-BC05-417B-BDA4-73DEC63D6D26}']
    function ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal;
    procedure SetData(const AFormat: string; const AData); overload;
    procedure SetData(const AFormat: string; AutoConvert: Boolean; const AData); overload;
    function GetFormats(AutoConvert: Boolean = True): TArray<string>;
    procedure Open;
    procedure Close;
  end;

  TdxOfficeDataFormats = class
  public
  const
    Text = 'Text';
    UnicodeText = 'UnicodeText';
    Dib = 'DeviceIndependentBitmap';
    Bitmap = 'Bitmap';
    EnhancedMetafile = 'EnhancedMetafile';
    MetafilePict = 'MetaFilePict';
    SilverlightXaml = 'com.microsoft.xaml.silverlight';
    SymbolicLink = 'SymbolicLink';
    SuppressStoreImageSize = 'SuppressStoreImageSize';
    Dif = 'DataInterchangeFormat';
    Tiff = 'TaggedImageFileFormat';
    OemText = 'OEMText';
    Palette = 'Palette';
    PenData = 'PenData';
    Riff = 'RiffAudio';
    WaveAudio = 'WaveAudio';
    FileDrop = 'FileDrop';
    Locale = 'Locale';
    Html = 'HTML Format';
    Rtf = 'Rich Text Format';
    RtfWithoutObjects = 'Rich Text Format Without Objects';
    CommaSeparatedValue = 'Csv';
    XMLSpreadsheet = 'XML Spreadsheet';
    MsSourceUrl = 'msSourceUrl';
  end;

  { TdxDataObject }

  TdxDataObject = class(TInterfacedObject, IdxDataObject, IDataObject)
  strict private
    FInnerData: IdxDataObject;
    //IDataObject
    function GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult; overload; stdcall;
    function GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult; stdcall;
    function QueryGetData(const formatetc: TFormatEtc): HResult; stdcall;
    function GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult; stdcall;
    function SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult; overload; stdcall;
    function EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult; stdcall;
    function DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult; stdcall;
    function DUnadvise(dwConnection: Longint): HResult; stdcall;
    function EnumDAdvise(out enumAdvise: IEnumStatData): HResult; stdcall;
  public
    constructor Create;
    destructor Destroy; override;

    //IdxDataObject
    function ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; overload;
    procedure SetData(const AFormat: string; const AData); overload;
    procedure SetData(const AFormat: string; AutoConvert: Boolean; const AData); overload;
    function GetFormats(AutoConvert: Boolean = True): TArray<string>;
    procedure Open;
    procedure Close;
  end;

  { TdxDataObjectItem}

  TdxDataObjectItem = class abstract
  strict private
    FSize: Cardinal;
    FData: Pointer;
  protected
    function GetSize(const AData): Cardinal; virtual; abstract;
    // ansistring
    function AnsiStringSupports: Boolean; virtual;
    procedure SetAsAnsiString(const S: AnsiString);
    function GetAsAnsiString: AnsiString; virtual;

    function StringSupports: Boolean; virtual;
    procedure SetAsString(const S: string);
    function GetAsString: string;
  public
    destructor Destroy; override;

    procedure CopyData(P: Pointer; ASize: Cardinal);
    function GetAsText: string;
    procedure GetData(AData: Pointer);
    procedure SetData(const AData);

    class function CreateDataObjectItem(const AFormat: string): TdxDataObjectItem;

    property Size: Cardinal read FSize;
    property Data: Pointer read FData;
  end;
  TdxDataObjectItemClass = class of TdxDataObjectItem;

  { TdxDataObjectAnsiItem }

  TdxDataObjectAnsiItem = class(TdxDataObjectItem)
  protected
    function GetSize(const AData): Cardinal; override;
    function AnsiStringSupports: Boolean; override;
  end;

  { TdxDataObjectUnicodeItem }

  TdxDataObjectUnicodeItem = class(TdxDataObjectItem)
  protected
    function GetSize(const AData): Cardinal; override;
    function StringSupports: Boolean; override;
  end;

  { TdxDragAndDropDataObject }

  TdxDragAndDropDataObject = class(TInterfacedObject, IdxDataObject, IDataObject)
  strict private
    FLockCount: Integer;
    FData: TObjectDictionary<string, TdxDataObjectItem>;
    procedure Check;
  protected
    //IDataObject
    function GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult; overload; stdcall;
    function GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult; stdcall;
    function QueryGetData(const formatetc: TFormatEtc): HResult; stdcall;
    function GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult; stdcall;
    function SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult; overload; stdcall;
    function EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult; stdcall;
    function DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult; stdcall;
    function DUnadvise(dwConnection: Longint): HResult; stdcall;
    function EnumDAdvise(out enumAdvise: IEnumStatData): HResult; stdcall;
  public
    constructor Create;
    destructor Destroy; override;
    //IdxDataObject
    function ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; overload;
    procedure SetData(const AFormat: string; const AData); overload;
    procedure SetData(const AFormat: string; AutoConvert: Boolean; const AData); overload;
    function GetFormats(AutoConvert: Boolean = True): TArray<string>;
    procedure Open;
    procedure Close;
  end;

  { TdxDragAndDropExternalDataObject }

  TdxDragAndDropExternalDataObject = class(TInterfacedObject, IdxDataObject, IDataObject)
  strict private
    FDataObject: IDataObject;
    function GetFormatetc(const AFormat: string): TFormatEtc;
    //IDataObject
    function GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult; overload; stdcall;
    function GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult; stdcall;
    function QueryGetData(const formatetc: TFormatEtc): HResult; stdcall;
    function GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult; stdcall;
    function SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult; overload; stdcall;
    function EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult; stdcall;
    function DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult; stdcall;
    function DUnadvise(dwConnection: Longint): HResult; stdcall;
    function EnumDAdvise(out enumAdvise: IEnumStatData): HResult; stdcall;
  public
    constructor Create(const ADataObject: IDataObject);
    //IdxDataObject
    function ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; overload;
    procedure SetData(const AFormat: string; const AData); overload;
    procedure SetData(const AFormat: string; AutoConvert: Boolean; const AData); overload;
    function GetFormats(AutoConvert: Boolean = True): TArray<string>;
    procedure Open;
    procedure Close;
  end;

  { TdxDataStore }

  TdxDataStore = class(TInterfacedObject, IdxDataObject, IDataObject)
  protected
    //IDataObject
    function GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult; overload; stdcall;
    function GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult; stdcall;
    function QueryGetData(const formatetc: TFormatEtc): HResult; stdcall;
    function GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult; stdcall;
    function SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult; overload; stdcall;
    function EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult; stdcall;
    function DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult; stdcall;
    function DUnadvise(dwConnection: Longint): HResult; stdcall;
    function EnumDAdvise(out enumAdvise: IEnumStatData): HResult; stdcall;
  public
    //IdxDataObject
    function ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; overload;
    procedure SetData(const AFormat: string; const AData); overload;
    procedure SetData(const AFormat: string; AutoConvert: Boolean; const AData); overload;
    function GetFormats(AutoConvert: Boolean = True): TArray<string>;
    procedure Open;
    procedure Close;
  end;

  { IdxPasteSource }

  IdxPasteSource = interface
  ['{E9F7028F-CA43-4313-A359-55423530D104}']
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal;
    function GetDataAsText(const AFormat: string; AutoConvert: Boolean = False): string;
    function ContainsData(const AFormat: string; AutoConvert: Boolean = False): Boolean;
  end;

  { TdxPasteSource }

  TdxPasteSource = class abstract(TInterfacedObject, IdxPasteSource)
  public
    //IdxPasteSource
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; virtual; abstract;
    function GetDataAsText(const AFormat: string; AutoConvert: Boolean = False): string; virtual;
    function ContainsData(const AFormat: string; AutoConvert: Boolean = False): Boolean; virtual; abstract;
  end;

  TdxDataObjectPasteSource = class(TdxPasteSource)
  private
    FDataObject: IdxDataObject;
  public
    constructor Create(const ADataObject: IdxDataObject);

    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; override;
    function ContainsData(const AFormat: string; AutoConvert: Boolean = False): Boolean; override;

    property DataObject: IdxDataObject read FDataObject write FDataObject;
  end;

  { TdxClipboardPasteSource }

  TdxClipboardPasteSource = class(TdxPasteSource)
  public
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; override;
    function GetDataAsText(const AFormat: string; AutoConvert: Boolean = False): string; override;
    function ContainsData(const AFormat: string; AutoConvert: Boolean = False): Boolean; override;
  end;

  { TdxEmptyPasteSource }

  TdxEmptyPasteSource = class(TdxPasteSource)
  public
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal; override;
    function ContainsData(const AFormat: string; AutoConvert: Boolean = False): Boolean; override;
  end;

  { TdxClipboard}

  TdxClipboard = class helper for TClipboard
  private
    function CanGetDataAsText(const AFormat: string): Boolean;
    function GetRtf: AnsiString;
    function GetRtfWithoutObjects: AnsiString;
    function GetSuppressStoreImageSize: AnsiString;
    function GetUnicodeText: string;
    procedure SetRtf(const Value: AnsiString);
    procedure SetRtfWithoutObjects(const Value: AnsiString);
    procedure SetSuppressStoreImageSize(const Value: AnsiString);
    procedure SetUnicoteText(const Value: string);
  protected
    function GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal;
    function GetDataAsText(const AFormat: string; AutoConvert: Boolean = False): string;
    function ContainsData(const AFormat: string): Boolean; overload;
    function ContainsData(const AFormat: Word): Boolean; overload;
  public
    property Rtf: AnsiString read GetRtf write SetRtf;
    property RtfWithoutObjects: AnsiString read GetRtfWithoutObjects write SetRtfWithoutObjects;
    property SuppressStoreImageSize: AnsiString read GetSuppressStoreImageSize write SetSuppressStoreImageSize;
    property UnicodeText: string read GetUnicodeText write SetUnicoteText;
  end;

  { TdxClipboardStringContent }

  TdxClipboardStringContent = class(TInterfacedObject)
  private
    FStringContent: string;
  public
    constructor Create(const AContent: string);

    property StringContent: string read FStringContent write FStringContent;
  end;

const
  {$EXTERNALSYM CF_RTF}
  CF_RTF: Word = $FF;
  {$EXTERNALSYM CF_RTFWithoutObjects}
  CF_RTFWithoutObjects: Word = $FF;
  {$EXTERNALSYM CF_XMLSpreadsheet}
  CF_XMLSpreadsheet: Word = $FF;
  {$EXTERNALSYM CF_SuppressStoreImageSize}
  CF_SuppressStoreImageSize: Word = $FF;
  {$EXTERNALSYM CF_MsSourceUrl}
  CF_MsSourceUrl: Word = $FF;

implementation

uses
  dxCore;

var
  FFormats: TDictionary<string, Word>;
  FFormatDataObjectItems: TDictionary<string, TdxDataObjectItemClass>;

function CFToFormat(ACF: Word; var AFormat: string): Boolean;
var
  S: string;
begin
  Result := FFormats.ContainsValue(ACF);
  if Result then
  begin
    for S in FFormats.Keys do
      if FFormats[S] = ACF then
        Break;
    AFormat := S;
  end;
end;

type
  TFormatList = array[0..255] of TFormatEtc;

  TdxEnumFormatEtc = class(TInterfacedObject, IEnumFORMATETC)
  private
    FDataObject: IdxDataObject;
    FDataObjectFormats: TArray<TFormatEtc>;
    FIndex: Integer;
    function GetCount: Integer;
  protected
    procedure PopulateDataObjectFormats;
    property Count: Integer read GetCount;
  public
    constructor Create(const ADataObject: IdxDataObject);

    //TdxEnumFormatEtc
    function Next(celt: Longint; out elt; pceltFetched: PLongint): HResult; stdcall;
    function Skip(celt: Longint): HResult; stdcall;
    function Reset: HResult; stdcall;
    function Clone(out Enum: IEnumFormatEtc): HResult; stdcall;
  end;

{ TdxEnumFormatEtc }

constructor TdxEnumFormatEtc.Create(const ADataObject: IdxDataObject);
begin
  inherited Create;
  FDataObject := ADataObject;
  PopulateDataObjectFormats;
  Reset;
end;

function TdxEnumFormatEtc.Next(celt: Longint; out elt; pceltFetched: PLongint): HResult;
var
  I: Integer;
begin
  I := 0;
  while (I < celt) and (FIndex < Count) do
  begin
    TFormatList(elt)[I] := FDataObjectFormats[FIndex];
    Inc(FIndex);
    Inc(I);
  end;
  if pceltFetched <> nil then pceltFetched^ := I;
  if I = celt then Result := S_OK else Result := S_FALSE;
end;

function TdxEnumFormatEtc.Skip(celt: Longint): HResult;
begin
  if celt <= Count - FIndex then
  begin
    FIndex := FIndex + celt;
    Result := S_OK;
  end
  else
  begin
    FIndex := Count;
    Result := S_FALSE;
  end;
end;

function TdxEnumFormatEtc.Reset: HResult;
begin
  FIndex := 0;
  Result := S_OK;
end;

function TdxEnumFormatEtc.Clone(out Enum: IEnumFormatEtc): HResult;
begin
  Enum := TdxEnumFormatEtc.Create(FDataObject);
  Result := S_OK;
end;

procedure TdxEnumFormatEtc.PopulateDataObjectFormats;
var
  AFormats: TArray<string>;
  I, J: Integer;
  ACount: Integer;
begin
  AFormats := FDataObject.GetFormats;
  ACount := Length(AFormats);
  SetLength(FDataObjectFormats, ACount);
  J := 0;
  for I := 0 to ACount - 1 do
  begin
    if not FFormats.ContainsKey(AFormats[I]) then
      Continue;
    FDataObjectFormats[J].cfFormat := FFormats[AFormats[I]];
    FDataObjectFormats[J].dwAspect := DVASPECT_CONTENT;
    FDataObjectFormats[J].lindex := -1;
    FDataObjectFormats[J].tymed := TYMED_HGLOBAL;
    Inc(J);
  end;
  SetLength(FDataObjectFormats, J);
end;

function TdxEnumFormatEtc.GetCount: Integer;
begin
  Result := Length(FDataObjectFormats);
end;

{ TdxDataObject }

constructor TdxDataObject.Create;
begin
  inherited Create;
  FInnerData := TdxDataStore.Create;
end;

destructor TdxDataObject.Destroy;
begin
  FInnerData := nil;
  inherited;
end;

function TdxDataObject.GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult;
begin
  Result := IDataObject(FInnerData).GetData(formatetcIn, medium);
end;

function TdxDataObject.GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult;
begin
  Result := IDataObject(FInnerData).GetDataHere(formatetc, medium);
end;

function TdxDataObject.QueryGetData(const formatetc: TFormatEtc): HResult;
begin
  Result := IDataObject(FInnerData).QueryGetData(formatetc);
end;

function TdxDataObject.GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult;
begin
  Result := IDataObject(FInnerData).GetCanonicalFormatEtc(formatetc, formatetcOut);
end;

function TdxDataObject.SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult;
begin
  Result := IDataObject(FInnerData).SetData(formatetc, medium, fRelease);
end;

function TdxDataObject.EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult;
begin
  Result := IDataObject(FInnerData).EnumFormatEtc(dwDirection, enumFormatEtc);
end;

function TdxDataObject.DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult;
begin
  Result := IDataObject(FInnerData).DAdvise(formatetc, advf, advSink, dwConnection);
end;

function TdxDataObject.DUnadvise(dwConnection: Longint): HResult;
begin
  Result := IDataObject(FInnerData).DUnadvise(dwConnection);
end;

function TdxDataObject.EnumDAdvise(out enumAdvise: IEnumStatData): HResult;
begin
  Result := IDataObject(FInnerData).EnumDAdvise(enumAdvise);
end;

procedure TdxDataObject.Close;
begin
  FInnerData.Close;
end;

function TdxDataObject.ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
begin
  Result := FInnerData.ContainsData(AFormat, AutoConvert);
end;

function TdxDataObject.GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal;
begin
  Result := 0;
end;

function TdxDataObject.GetFormats(AutoConvert: Boolean): TArray<string>;
begin
  Result := nil;
end;

procedure TdxDataObject.SetData(const AFormat: string; const AData);
begin
  SetData(AFormat, True, AData);
end;

procedure TdxDataObject.SetData(const AFormat: string; AutoConvert: Boolean; const AData);
begin
  FInnerData.SetData(AFormat, AutoConvert, AData);
end;

procedure TdxDataObject.Open;
begin
  FInnerData.Open;
end;

{ TdxDataObjectItem}

destructor TdxDataObjectItem.Destroy;
begin
  FreeMem(FData);
  inherited Destroy;
end;

function TdxDataObjectItem.GetAsText: string;
begin
  if StringSupports then
    Result := GetAsString
  else
  if AnsiStringSupports then
    Result := dxAnsiStringToString(GetAsAnsiString)
  else
    Assert(False);
end;

class function TdxDataObjectItem.CreateDataObjectItem(const AFormat: string): TdxDataObjectItem;
var
  AItemClass: TdxDataObjectItemClass;
begin
  if FFormatDataObjectItems.ContainsKey(AFormat) then
    AItemClass := FFormatDataObjectItems[AFormat]
  else
    AItemClass := TdxDataObjectUnicodeItem;
  Result := AItemClass.Create;
end;

procedure TdxDataObjectItem.GetData(AData: Pointer);
begin
  Move(FData^, AData^, FSize);
end;

procedure TdxDataObjectItem.SetData(const AData);
var
  P: Pointer absolute AData;
begin
  CopyData(P, GetSize(AData));
end;

procedure TdxDataObjectItem.CopyData(P: Pointer; ASize: Cardinal);
begin
  FSize := ASize;
  FreeMem(FData);
  FData := AllocMem(FSize);
  Move(P^, FData^, FSize);
end;

function TdxDataObjectItem.AnsiStringSupports: Boolean;
begin
  Result := False;
end;

procedure TdxDataObjectItem.SetAsAnsiString(const S: AnsiString);
begin
  FSize := Length(S) * SizeOf(AnsiChar);
  CopyData(@S[1], FSize);
end;

function TdxDataObjectItem.GetAsAnsiString: AnsiString;
begin
  SetString(Result, PAnsiChar(FData), Size);
end;

function TdxDataObjectItem.StringSupports: Boolean;
begin
  Result := False;
end;

procedure TdxDataObjectItem.SetAsString(const S: string);
begin
  FSize := Length(S) * SizeOf(Char);
  CopyData(@S[1], FSize);
end;

function TdxDataObjectItem.GetAsString: string;
begin
  SetString(Result, PChar(FData), FSize);
end;

{ TdxDataObjectAnsiItem }

function TdxDataObjectAnsiItem.GetSize(const AData): Cardinal;
begin
  Result := Length(PAnsiChar(AData)) * SizeOf(AnsiChar);
end;

function TdxDataObjectAnsiItem.AnsiStringSupports: Boolean;
begin
  Result := True;
end;

{ TdxDataObjectUnicodeItem }

function TdxDataObjectUnicodeItem.GetSize(const AData): Cardinal;
begin
  Result := Length(PChar(AData)) * SizeOf(Char);
end;

function TdxDataObjectUnicodeItem.StringSupports: Boolean;
begin
  Result := True;
end;

{ TdxDataStore }

function TdxDataStore.GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.QueryGetData(const formatetc: TFormatEtc): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.DUnadvise(dwConnection: Longint): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.EnumDAdvise(out enumAdvise: IEnumStatData): HResult;
begin
  Result := S_FALSE;
  Assert(False, 'not implemented');
end;

function TdxDataStore.ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
begin
  Result := Clipboard.ContainsData(AFormat);
end;

function TdxDataStore.GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal;
begin
  Result := 0;
  Assert(False, 'not implemented');
end;

procedure TdxDataStore.Close;
begin
  Clipboard.Close;
end;

procedure TdxDataStore.Open;
begin
  Clipboard.Open;
end;

function TdxDataStore.GetFormats(AutoConvert: Boolean = True): TArray<string>;
begin
  Result := nil;
end;

procedure TdxDataStore.SetData(const AFormat: string; AutoConvert: Boolean;
  const AData);
begin
  if AFormat = TdxOfficeDataFormats.Rtf then
    Clipboard.Rtf := AnsiString(AData)
  else
  if AFormat = TdxOfficeDataFormats.UnicodeText then
    Clipboard.UnicodeText := string(AData)
  else
  if AFormat = TdxOfficeDataFormats.RtfWithoutObjects then
    Clipboard.RtfWithoutObjects := AnsiString(AData)
  else
    Assert(False);
end;

procedure TdxDataStore.SetData(const AFormat: string; const AData);
begin
  SetData(AFormat, True, AData);
end;

{ TdxEmptyPasteSource }

function TdxEmptyPasteSource.ContainsData(const AFormat: string;
  AutoConvert: Boolean): Boolean;
begin
  Result := False;
end;

function TdxEmptyPasteSource.GetData(const AFormat: string; AData: Pointer;
  AutoConvert: Boolean = False): Cardinal;
begin
  Result := 0;
end;

{ TdxClipboard }

function TdxClipboard.GetRtf: AnsiString;
var
  AData: THandle;
begin
  Open;
  AData := GetClipboardData(CF_RTF);
  try
    if AData <> 0 then
      Result := PAnsiChar(GlobalLock(AData))
    else
      Result := '';
  finally
    if AData <> 0 then
      GlobalUnlock(AData);
    Close;
  end;
end;

function TdxClipboard.GetRtfWithoutObjects: AnsiString;
var
  AData: THandle;
begin
  Open;
  AData := GetClipboardData(CF_RTFWithoutObjects);
  try
    if AData <> 0 then
      Result := PAnsiChar(GlobalLock(AData))
    else
      Result := '';
  finally
    if AData <> 0 then
      GlobalUnlock(AData);
    Close;
  end;
end;

function TdxClipboard.GetSuppressStoreImageSize: AnsiString;
var
  AData: THandle;
  ADataSize: Cardinal;
begin
  Open;
  try
    AData := GetClipboardData(CF_SuppressStoreImageSize);
    try
      if AData <> 0 then
      begin
        ADataSize := GlobalSize(AData);
        SetString(Result, PAnsiChar(GlobalLock(AData)), ADataSize);
      end
      else
        Result := '';
    finally
      if AData <> 0 then
        GlobalUnlock(AData);
    end;
  finally
    Close;
  end;
end;

function TdxClipboard.GetUnicodeText: string;
var
  AData: THandle;
begin
  Open;
  AData := GetClipboardData(CF_UNICODETEXT);
  try
    if AData <> 0 then
      Result := PChar(GlobalLock(AData))
    else
      Result := '';
  finally
    if AData <> 0 then
      GlobalUnlock(AData);
    Close;
  end;
end;

function TdxClipboard.CanGetDataAsText(const AFormat: string): Boolean;
begin
  Result := (AFormat = TdxOfficeDataFormats.Text) or (AFormat = TdxOfficeDataFormats.UnicodeText) or
    (AFormat = TdxOfficeDataFormats.Rtf) or (AFormat = TdxOfficeDataFormats.Text) or
    (AFormat = TdxOfficeDataFormats.SuppressStoreImageSize);
end;

procedure TdxClipboard.SetRtf(const Value: AnsiString);
begin
  SetBuffer(CF_RTF, PAnsiChar(Value)^, Length(Value) + SizeOf(AnsiChar));
end;

procedure TdxClipboard.SetRtfWithoutObjects(const Value: AnsiString);
begin
  SetBuffer(CF_RTFWithoutObjects, PAnsiChar(Value)^, Length(Value) + SizeOf(AnsiChar));
end;

procedure TdxClipboard.SetSuppressStoreImageSize(const Value: AnsiString);
begin
  SetBuffer(CF_SuppressStoreImageSize, PAnsiChar(Value)^, Length(Value) + SizeOf(Char));
end;

procedure TdxClipboard.SetUnicoteText(const Value: string);
begin
  SetBuffer(CF_UNICODETEXT, PChar(Value)^, ByteLength(Value) + SizeOf(Char));
end;

function TdxClipboard.GetData(const AFormat: string; AData: Pointer;
  AutoConvert: Boolean = False): Cardinal;
var
  S: string;
  AHandle: THandle;
begin
  Result := 0;
  if not ContainsData(AFormat) then
    Exit;
  if CanGetDataAsText(AFormat) then
  begin
    S := GetDataAsText(AFormat, AutoConvert);
    Result := Length(S) * SizeOf(Char);
    if AData = nil then
      Exit;
    Move(S[1], AData^, Result);
  end
  else
  begin
    Open;
    AHandle := GetClipboardData(FFormats[AFormat]);
    try
      if AHandle <> 0 then
      begin
        Result := SizeOf(AData);
        if AData <> nil then
          Move(AHandle, AData^, SizeOf(AHandle));
      end
      else
        Result := 0;
    finally
      if AHandle <> 0 then
        GlobalUnlock(AHandle);
      Close;
    end;
  end;
end;

function TdxClipboard.GetDataAsText(const AFormat: string; AutoConvert: Boolean = False): string;
begin
  if AFormat = TdxOfficeDataFormats.Text then
    Result := AsText
  else
    if AFormat = TdxOfficeDataFormats.UnicodeText then
      Result := UnicodeText
    else
      if AFormat = TdxOfficeDataFormats.Rtf then
        Result := dxAnsiStringToString(Rtf)
      else
        if AFormat = TdxOfficeDataFormats.SuppressStoreImageSize then
          Result := dxAnsiStringToString(SuppressStoreImageSize)
        else
          Result := '';
end;

function TdxClipboard.ContainsData(const AFormat: string): Boolean;
begin
  Result := FFormats.ContainsKey(AFormat);
  Result := Result and ContainsData(FFormats[AFormat]);
end;

function TdxClipboard.ContainsData(const AFormat: Word): Boolean;
begin
  Result := HasFormat(AFormat);
end;

{ TdxClipboardPasteSource }

function TdxClipboardPasteSource.GetData(const AFormat: string; AData: Pointer;
  AutoConvert: Boolean = False): Cardinal;
begin
  Result := Clipboard.GetData(AFormat, AData, AutoConvert);
end;

function TdxClipboardPasteSource.GetDataAsText(const AFormat: string; AutoConvert: Boolean = False): string;
begin
  if Clipboard.CanGetDataAsText(AFormat) then
    Result := Clipboard.GetDataAsText(AFormat, AutoConvert)
  else
    Result := inherited GetDataAsText(AFormat, AutoConvert);
end;

function TdxClipboardPasteSource.ContainsData(const AFormat: string;
  AutoConvert: Boolean): Boolean;
begin
  Result := Clipboard.ContainsData(AFormat);
end;

{ TdxClipboardStringContent }

constructor TdxClipboardStringContent.Create(const AContent: string);
begin
  inherited Create;
  FStringContent := AContent;
end;

{ TdxPasteSource }

function TdxPasteSource.GetDataAsText(const AFormat: string; AutoConvert: Boolean = False): string;
var
  ASize: Cardinal;
  AData: Pointer;
  AItem: TdxDataObjectItem;
begin
  ASize := GetData(AFormat, nil);
  if ASize > 0 then
  begin
    GetMem(AData, ASize);
    try
      GetData(AFormat, AData);
      AItem := TdxDataObjectItem.CreateDataObjectItem(AFormat);
      try
        if AItem <> nil then
        begin
          AItem.CopyData(AData, ASize);
          Result := AItem.GetAsText;
        end
        else
          SetString(Result, PChar(AData), ASize div SizeOf(Char));
      finally
        AItem.Free;
      end;
    finally
      FreeMem(AData);
    end;
  end
  else
    Result := '';
end;

{ TdxPlatformIndependentDataObject }

constructor TdxDragAndDropDataObject.Create;
begin
  inherited Create;
  FData := TObjectDictionary<string, TdxDataObjectItem>.Create([doOwnsValues]);
end;

destructor TdxDragAndDropDataObject.Destroy;
begin
  FreeAndNil(FData);
  inherited Destroy;
end;

function TdxDragAndDropDataObject.GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult;
var
  S: string;
  ALength: Cardinal;
  AItem: TdxDataObjectItem;
  APointer: Pointer;
begin
  Result := S_FALSE;
  if (QueryGetData(formatetcIn) = S_OK) and CFToFormat(formatetcIn.cfFormat, S) then
  begin
    FillChar(medium, SizeOf(medium), 0);
    medium.tymed := TYMED_HGLOBAL;
    AItem := FData[S];
    ALength := AItem.Size;
    medium.hGlobal := GlobalAlloc(GMEM_MOVEABLE, ALength);
    APointer := GlobalLock(medium.hGlobal);
    try
      AItem.GetData(APointer);
      Result := S_OK;
    finally
      GlobalUnlock(medium.hGlobal);
    end;
  end;
end;

function TdxDragAndDropDataObject.GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult;
begin
  Result := E_NOTIMPL;
end;

function TdxDragAndDropDataObject.QueryGetData(const formatetc: TFormatEtc): HResult;
const
  AResultMap: array [Boolean] of HResult = (S_FALSE, S_OK);
var
  S: string;
begin
  if not CFToFormat(formatetc.cfFormat, S) then
    Result := S_FALSE
  else
  begin
    Result := AResultMap[ContainsData(S)];
  end;
end;

function TdxDragAndDropDataObject.GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult;
begin
  Result := E_NOTIMPL;
end;

function TdxDragAndDropDataObject.SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult;
var
  AFormat: string;
begin
  if CFToFormat(formatetc.cfFormat, AFormat) then
  begin
    Assert(false, 'not implemetned');
    Result := S_OK;
  end
  else
    Result := E_UNEXPECTED;
end;

function TdxDragAndDropDataObject.EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult;
begin
  if dwDirection = DATADIR_GET then
  begin
    enumFormatEtc := TdxEnumFormatEtc.Create(Self);
    Result := S_OK;
  end
  else
  begin
    enumFormatEtc := nil;
    Result := E_NOTIMPL;
  end;
end;

function TdxDragAndDropDataObject.DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult;
begin
  Result := E_NOTIMPL;
end;

function TdxDragAndDropDataObject.DUnadvise(dwConnection: Longint): HResult;
begin
  Result := E_NOTIMPL;
end;

function TdxDragAndDropDataObject.EnumDAdvise(out enumAdvise: IEnumStatData): HResult;
begin
  Result := E_NOTIMPL;
end;

procedure TdxDragAndDropDataObject.Check;
begin
  if FLockCount = 0 then
    FData.Clear;
end;

procedure TdxDragAndDropDataObject.Close;
begin
  Dec(FLockCount);
end;

function TdxDragAndDropDataObject.ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
begin
  Result := FData.ContainsKey(AFormat);
end;

function TdxDragAndDropDataObject.GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal;
var
  AItem: TdxDataObjectItem;
begin
  if FData.ContainsKey(AFormat) then
  begin
    AItem := FData[AFormat];
    Result := AItem.Size;
    if AData <> nil then
      AItem.GetData(AData);
  end
  else
    Result := 0;
end;

function TdxDragAndDropDataObject.GetFormats(
  AutoConvert: Boolean = True): TArray<string>;
begin
  Result := FData.Keys.ToArray;
end;

procedure TdxDragAndDropDataObject.Open;
begin
  Inc(FLockCount);
end;

procedure TdxDragAndDropDataObject.SetData(const AFormat: string;
  AutoConvert: Boolean; const AData);
var
  AItem: TdxDataObjectItem;
  P: Pointer absolute AData;
begin
  if P = nil then
    Exit;
  Check;
  AItem := TdxDataObjectItem.CreateDataObjectItem(AFormat);
  AItem.SetData(AData);
  if FData.ContainsKey(AFormat) then
  begin
    FData[AFormat].Free;
    FData.Remove(AFormat);
  end;
  FData.Add(AFormat, AItem);
end;

procedure TdxDragAndDropDataObject.SetData(const AFormat: string;
  const AData);
begin
  SetData(AFormat, True, AData);
end;

{ TdxDragAndDropExternalDataObject }

constructor TdxDragAndDropExternalDataObject.Create(const ADataObject: IDataObject);
begin
  inherited Create;
  FDataObject := ADataObject;
end;

function TdxDragAndDropExternalDataObject.GetFormatetc(const AFormat: string): TFormatEtc;
begin
  Result.cfFormat := FFormats[AFormat];
  Result.ptd := nil;
  Result.dwAspect := DVASPECT_CONTENT;
  Result.lindex := -1;
  Result.tymed := TYMED_HGLOBAL;
end;

function TdxDragAndDropExternalDataObject.GetData(const formatetcIn: TFormatEtc; out medium: TStgMedium): HResult;
begin
  Result := FDataObject.GetData(formatetcIn, medium);
end;

function TdxDragAndDropExternalDataObject.GetDataHere(const formatetc: TFormatEtc; out medium: TStgMedium): HResult;
begin
  Result := FDataObject.GetDataHere(formatetc, medium);
end;

function TdxDragAndDropExternalDataObject.QueryGetData(const formatetc: TFormatEtc): HResult;
begin
  Result := FDataObject.QueryGetData(formatetc);
end;

function TdxDragAndDropExternalDataObject.GetCanonicalFormatEtc(const formatetc: TFormatEtc; out formatetcOut: TFormatEtc): HResult;
begin
  Result := FDataObject.GetCanonicalFormatEtc(formatetc, formatetcOut);
end;

function TdxDragAndDropExternalDataObject.SetData(const formatetc: TFormatEtc; var medium: TStgMedium; fRelease: BOOL): HResult;
begin
  Result := FDataObject.SetData(formatetc, medium, fRelease);
end;

function TdxDragAndDropExternalDataObject.EnumFormatEtc(dwDirection: Longint; out enumFormatEtc: IEnumFormatEtc): HResult;
begin
  Result := FDataObject.EnumFormatEtc(dwDirection, enumFormatEtc);
end;

function TdxDragAndDropExternalDataObject.DAdvise(const formatetc: TFormatEtc; advf: Longint; const advSink: IAdviseSink; out dwConnection: Longint): HResult;
begin
  Result := FDataObject.DAdvise(formatetc, advf, advSink, dwConnection);
end;

function TdxDragAndDropExternalDataObject.DUnadvise(dwConnection: Longint): HResult;
begin
  Result := FDataObject.DUnadvise(dwConnection);
end;

function TdxDragAndDropExternalDataObject.EnumDAdvise(out enumAdvise: IEnumStatData): HResult;
begin
  Result := FDataObject.EnumDAdvise(enumAdvise);
end;

function TdxDragAndDropExternalDataObject.ContainsData(const AFormat: string; AutoConvert: Boolean = True): Boolean;
var
  AFormatetc: TFormatEtc;
begin
  Result := FFormats.ContainsKey(AFormat);
  if Result then
  begin
    AFormatetc := GetFormatetc(AFormat);
    Result := QueryGetData(AFormatetc) = S_OK;
  end;
end;

function TdxDragAndDropExternalDataObject.GetData(const AFormat: string; AData: Pointer; AutoConvert: Boolean = False): Cardinal;
var
  AFormatetc: TFormatEtc;
  AMedium: TStgMedium;
  ABytes: Pointer;
begin
  if not ContainsData(AFormat, AutoConvert) then
    Result := 0
  else
  begin
    AFormatetc := GetFormatetc(AFormat);
    FillChar(AMedium, SizeOf(AMedium), 0);
    try
      if GetData(AFormatetc, AMedium) = S_OK then
      begin
        Result := GlobalSize(AMedium.hGlobal);
        if (Result > 0) and (AData <> nil) then
        begin
          try
            ABytes := GlobalLock(AMedium.hGlobal);
            Move(ABytes^, AData^, Result);
          finally
            GlobalUnlock(AMedium.hGlobal);
          end;
        end;
      end
      else
        Result := 0;
    finally
      ReleaseStgMedium(AMedium);
    end;
  end;
end;

procedure TdxDragAndDropExternalDataObject.SetData(const AFormat: string; const AData);
begin
  Assert(False, 'not used');
end;

procedure TdxDragAndDropExternalDataObject.SetData(const AFormat: string; AutoConvert: Boolean; const AData);
begin
  Assert(False, 'not used');
end;

function TdxDragAndDropExternalDataObject.GetFormats(AutoConvert: Boolean = True): TArray<string>;
begin
  Assert(False, 'not used');
end;

procedure TdxDragAndDropExternalDataObject.Open;
begin
  Assert(False, 'not used');
end;

procedure TdxDragAndDropExternalDataObject.Close;
begin
  Assert(False, 'not used');
end;

{ TdxDataObjectPasteSource }

constructor TdxDataObjectPasteSource.Create(const ADataObject: IdxDataObject);
begin
  inherited Create;
  FDataObject := ADataObject;
end;

function TdxDataObjectPasteSource.ContainsData(const AFormat: string;
  AutoConvert: Boolean): Boolean;
begin
  Result := (DataObject <> nil) and DataObject.ContainsData(AFormat, AutoConvert);
end;

function TdxDataObjectPasteSource.GetData(const AFormat: string; AData: Pointer;
  AutoConvert: Boolean = False): Cardinal;
begin
  if FDataObject = nil then
    Result := 0
  else
    Result := FDataObject.GetData(AFormat, AData, AutoConvert);
end;

procedure PopulateFormats;
begin
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Text, CF_TEXT);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.UnicodeText, CF_UNICODETEXT);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Dib, CF_DIB);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Bitmap, CF_BITMAP);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.EnhancedMetafile, CF_ENHMETAFILE);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.MetafilePict, CF_METAFILEPICT);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.SuppressStoreImageSize, CF_SuppressStoreImageSize);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Dif, CF_DIF);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Tiff, CF_TIFF);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.OemText, CF_OEMTEXT);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Palette, CF_PALETTE);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.PenData, CF_PENDATA);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Riff, CF_RIFF);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.WaveAudio, CF_WAVE);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Locale, CF_LOCALE);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.Rtf, CF_RTF);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.RtfWithoutObjects, CF_RTFWithoutObjects);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.XMLSpreadsheet, CF_XMLSpreadsheet);
  FFormats.AddOrSetValue(TdxOfficeDataFormats.MsSourceUrl, CF_MsSourceUrl);
end;

procedure PopulateFormatDataObjectItems;
begin
  FFormatDataObjectItems.AddOrSetValue(TdxOfficeDataFormats.Text, TdxDataObjectAnsiItem);
  FFormatDataObjectItems.AddOrSetValue(TdxOfficeDataFormats.UnicodeText, TdxDataObjectUnicodeItem);
  FFormatDataObjectItems.AddOrSetValue(TdxOfficeDataFormats.SuppressStoreImageSize, TdxDataObjectAnsiItem);
  FFormatDataObjectItems.AddOrSetValue(TdxOfficeDataFormats.Rtf, TdxDataObjectAnsiItem);
  FFormatDataObjectItems.AddOrSetValue(TdxOfficeDataFormats.RtfWithoutObjects, TdxDataObjectAnsiItem);
end;

initialization
  OleInitialize(nil);
  CF_RTF := RegisterClipboardFormat(TdxOfficeDataFormats.Rtf);
  CF_XMLSpreadsheet := RegisterClipboardFormat(TdxOfficeDataFormats.XMLSpreadsheet);
  CF_SuppressStoreImageSize := RegisterClipboardFormat(TdxOfficeDataFormats.SuppressStoreImageSize);
  CF_RTFWithoutObjects := RegisterClipboardFormat(TdxOfficeDataFormats.RtfWithoutObjects);
  CF_MsSourceUrl := RegisterClipboardFormat(TdxOfficeDataFormats.MsSourceUrl);
  FFormats := TDictionary<string, Word>.Create;
  PopulateFormats;
  FFormatDataObjectItems := TDictionary<string, TdxDataObjectItemClass>.Create;
  PopulateFormatDataObjectItems;

finalization
  FreeAndNil(FFormats);
  FreeAndNil(FFormatDataObjectItems);
  OleUninitialize;

end.
