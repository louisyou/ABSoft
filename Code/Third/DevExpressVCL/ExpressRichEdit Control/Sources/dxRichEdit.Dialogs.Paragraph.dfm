inherited dxRichEditParagraphDialogForm: TdxRichEditParagraphDialogForm
  AutoSize = True
  ClientHeight = 361
  AutoScroll = False
  ClientWidth = 497
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 497
    Height = 361
    TabOrder = 0
    AutoSize = True
    LayoutLookAndFeel = dxLayoutCxLookAndFeel1
    object lbGeneral: TcxLabel
      Left = 20
      Top = 44
      AutoSize = False
      Caption = 'General'
      Style.HotTrack = False
      Properties.LineOptions.Visible = True
      Transparent = True
      Height = 17
      Width = 443
    end
    object edtAlignment: TcxComboBox
      Left = 104
      Top = 67
      Properties.DropDownListStyle = lsFixedList
      Style.HotTrack = False
      TabOrder = 1
      Width = 121
    end
    object ccbOutlinelevel: TcxComboBox
      Left = 104
      Top = 94
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 10
      Style.HotTrack = False
      TabOrder = 2
      Width = 121
    end
    object lbIndentation: TcxLabel
      Left = 20
      Top = 121
      AutoSize = False
      Caption = 'Indentation'
      Style.HotTrack = False
      Properties.LineOptions.Visible = True
      Transparent = True
      Height = 17
      Width = 443
    end
    object seLeftIndent: TdxMeasurementUnitEdit
      Left = 104
      Top = 144
      TabOrder = 4
      Width = 121
    end
    object seRightIndent: TdxMeasurementUnitEdit
      Left = 104
      Top = 171
      TabOrder = 5
      Width = 121
    end
    object ccbFirstLineIndentType: TcxComboBox
      Left = 231
      Top = 171
      Properties.DropDownListStyle = lsFixedList
      Style.HotTrack = False
      TabOrder = 6
      Width = 121
    end
    object seFirstLineIndent: TdxMeasurementUnitEdit
      Left = 358
      Top = 171
      TabOrder = 7
      Width = 105
    end
    object lbSpacing: TcxLabel
      Left = 20
      Top = 198
      AutoSize = False
      Caption = 'Spacing'
      Style.HotTrack = False
      Properties.LineOptions.Visible = True
      Transparent = True
      Height = 17
      Width = 443
    end
    object cbContextualSpacing: TcxCheckBox
      Left = 36
      Top = 275
      Caption = 'Don'#39't add spa&ce between paragraphs of the same style'
      Style.HotTrack = False
      TabOrder = 13
      Transparent = True
      Width = 427
    end
    object seSpacingBefore: TdxMeasurementUnitEdit
      Left = 104
      Top = 221
      TabOrder = 9
      Width = 121
    end
    object ccbLineSpacing: TcxComboBox
      Left = 231
      Top = 248
      Properties.DropDownListStyle = lsFixedList
      Style.HotTrack = False
      TabOrder = 11
      Width = 121
    end
    object seSpacingAfter: TdxMeasurementUnitEdit
      Left = 104
      Top = 248
      TabOrder = 10
      Width = 121
    end
    object lbPagination: TcxLabel
      Left = 10000
      Top = 10000
      AutoSize = False
      Caption = 'Pagination'
      Style.HotTrack = False
      Properties.LineOptions.Visible = True
      Transparent = True
      Visible = False
      Height = 17
      Width = 437
    end
    object cbPageBreakBefore: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Page &break before'
      Style.HotTrack = False
      TabOrder = 16
      Transparent = True
      Visible = False
      Width = 121
    end
    object cbKeepLinesTogether: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = '&Keep lines together'
      Style.HotTrack = False
      TabOrder = 15
      Transparent = True
      Visible = False
      Width = 121
    end
    object btnTabs: TcxButton
      Left = 10
      Top = 312
      Width = 75
      Height = 25
      Caption = '&Tabs...'
      TabOrder = 17
      OnClick = btnTabsClick
    end
    object btnOK: TcxButton
      Left = 317
      Top = 312
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 18
      OnClick = btnOKClick
    end
    object btnCancel: TcxButton
      Left = 398
      Top = 312
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 19
    end
    object seAtSpacing: TdxMeasurementUnitEdit
      Left = 358
      Top = 248
      TabOrder = 12
      Width = 94
    end
    object lcMainGroup_Root: TdxLayoutGroup
      AlignHorz = ahLeft
      AlignVert = avTop
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object dxLayoutControl1Group1: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 0
    end
    object lcgIndentsAndSpacing: TdxLayoutGroup
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = '&Indents and Spacing'
      ButtonOptions.Buttons = <>
      ItemIndex = 5
      Index = 0
    end
    object lcgLineAndPageBreaks: TdxLayoutGroup
      Parent = dxLayoutControl1Group1
      CaptionOptions.Text = 'Line and &Page Breaks'
      ButtonOptions.Buttons = <>
      Index = 1
    end
    object dxLayoutControl1Item3: TdxLayoutItem
      Parent = lcgIndentsAndSpacing
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lbGeneral
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group3: TdxLayoutGroup
      Parent = lcgIndentsAndSpacing
      AlignHorz = ahLeft
      CaptionOptions.Visible = False
      Offsets.Left = 16
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 1
    end
    object lciAlignment: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      CaptionOptions.Text = 'Ali&gnment:'
      Control = edtAlignment
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciOutlinelevel: TdxLayoutItem
      Parent = dxLayoutControl1Group3
      CaptionOptions.Text = '&Outline level:'
      Control = ccbOutlinelevel
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item4: TdxLayoutItem
      Parent = lcgIndentsAndSpacing
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lbIndentation
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Group4: TdxLayoutGroup
      Parent = lcgIndentsAndSpacing
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      Offsets.Left = 16
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 3
    end
    object lciLeft: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      AlignHorz = ahLeft
      CaptionOptions.Text = '&Left:'
      Control = seLeftIndent
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciRight: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      CaptionOptions.Text = '&Right:'
      Control = seRightIndent
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group5: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group4
      AlignHorz = ahLeft
      Index = 0
      AutoCreated = True
    end
    object lciSpecial: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      AlignVert = avBottom
      CaptionOptions.Text = '&Special:'
      CaptionOptions.Layout = clTop
      Control = ccbFirstLineIndentType
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciBy: TdxLayoutItem
      Parent = dxLayoutControl1Group4
      AlignVert = avBottom
      CaptionOptions.Text = 'B&y:'
      CaptionOptions.Layout = clTop
      Control = seFirstLineIndent
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item9: TdxLayoutItem
      Parent = lcgIndentsAndSpacing
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lbSpacing
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object dxLayoutControl1Group6: TdxLayoutGroup
      Parent = lcgIndentsAndSpacing
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      Offsets.Left = 16
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 5
    end
    object dxLayoutControl1Group7: TdxLayoutGroup
      Parent = dxLayoutControl1Group6
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item14: TdxLayoutItem
      Parent = dxLayoutControl1Group6
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = cbContextualSpacing
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciBefore: TdxLayoutItem
      Parent = dxLayoutControl1Group8
      AlignHorz = ahLeft
      CaptionOptions.Text = '&Before:'
      Control = seSpacingBefore
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciLineSpacing: TdxLayoutItem
      Parent = dxLayoutControl1Group7
      AlignHorz = ahLeft
      AlignVert = avBottom
      CaptionOptions.Text = 'Li&ne Spacing:'
      CaptionOptions.Layout = clTop
      Control = ccbLineSpacing
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciAfter: TdxLayoutItem
      Parent = dxLayoutControl1Group8
      AlignHorz = ahLeft
      CaptionOptions.Text = 'Aft&er:'
      Control = seSpacingAfter
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group8: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group7
      AlignHorz = ahLeft
      Index = 0
      AutoCreated = True
    end
    object dxLayoutControl1Item10: TdxLayoutItem
      Parent = lcgLineAndPageBreaks
      CaptionOptions.Visible = False
      Control = lbPagination
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group9: TdxLayoutGroup
      Parent = lcgLineAndPageBreaks
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      Offsets.Left = 16
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item17: TdxLayoutItem
      Parent = dxLayoutControl1Group9
      CaptionOptions.Text = 'cxCheckBox3'
      CaptionOptions.Visible = False
      Control = cbPageBreakBefore
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item11: TdxLayoutItem
      Parent = dxLayoutControl1Group9
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxCheckBox2'
      CaptionOptions.Visible = False
      Control = cbKeepLinesTogether
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group10: TdxLayoutGroup
      Parent = lcMainGroup_Root
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object lciTabs: TdxLayoutItem
      Parent = dxLayoutControl1Group10
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnTabs
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Group2: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group10
      AlignHorz = ahRight
      LayoutDirection = ldHorizontal
      Index = 1
      AutoCreated = True
    end
    object dxLayoutControl1Item2: TdxLayoutItem
      Parent = dxLayoutControl1Group2
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnOK
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item1: TdxLayoutItem
      Parent = dxLayoutControl1Group2
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group11: TdxLayoutGroup
      Parent = dxLayoutControl1Group7
      AlignHorz = ahClient
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 3
    end
    object lciAtSpacing: TdxLayoutItem
      Parent = dxLayoutControl1Group7
      AlignHorz = ahClient
      AlignVert = avBottom
      CaptionOptions.Text = 'At:'
      CaptionOptions.Layout = clTop
      Control = seAtSpacing
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
  object ActionList1: TActionList
    Left = 408
    Top = 32
  end
  object dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    Left = 400
    object dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel
    end
  end
end
