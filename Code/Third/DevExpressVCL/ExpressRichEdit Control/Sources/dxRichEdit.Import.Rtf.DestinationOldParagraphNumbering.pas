{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationOldParagraphNumbering;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, Math, SysUtils, Graphics, dxCoreClasses, dxRichEdit.Import, dxRichEdit.Import.Rtf,
  dxRichEdit.Import.Rtf.DestinationPieceTable, dxRichEdit.DocumentModel.ParagraphFormatting,
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.DocumentModel.Section, dxRichEdit.DocumentModel.Numbering;

type
  TdxTextAfterDestination = class(TdxStringValueDestination);
  TdxTextBeforeDestination = class(TdxStringValueDestination);

  TdxDestinationOldParagraphNumberingBase = class(TdxRichEditRtfDestinationBase)
  private
    class procedure SetUnderlineType(AImporter: TdxRichEditDocumentModelRtfImporter; AUnderlineType: TdxUnderlineType); inline; static;

    class procedure AllCapsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure CardinalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure CenterAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ContinuousUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DashDotDottedUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DashDottedUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DashedUndrelineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DecimalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DottedUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure DoubleUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FontBoldKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FontNumberKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure FontSizeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ForegroundColorKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure HairlineUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure HangingIndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure IndentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ItalicKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LeftAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LowerCaseAlphabeticalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure LowerCaseRomanKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure NoneUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure NumberingInCircleKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure OrdinalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure OrdinalTextKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure RestartOnSectionBreakKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure RightAlignmentKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SmallCapsKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SpaceKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure StartAtKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure StrikeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); static;
    class procedure TextAfterKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure TextBeforeKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ThickUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure UpperCaseAlphabeticalKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure UpperCaseRomanKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure UsePrevKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure WaveUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure WordUnderlineKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    FTextAfter: string;
    FTextBefore: string;

    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;

    procedure AppendParagraphNumberingDescKeywords(ATable: TdxKeywordTranslatorTable);
    class procedure SetNumberingListFormat(AImporter: TdxRichEditDocumentModelRtfImporter; AFormat: TdxNumberingFormat); inline; static;
  public
    procedure BeforePopRtfState; override;
    procedure NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase); override;
  end;

  { TdxDestinationOldParagraphNumbering }

  TdxDestinationOldParagraphNumbering = class(TdxDestinationOldParagraphNumberingBase)
  private
    FIsOldNumberingListCreated: Boolean;
    FListLevelIndex: Integer;
    FMultiLevelListIndex: TdxNumberingListIndex;
    FOldLevelNumber: Integer;
    FSimpleList: Boolean;
    FSimpleListIndex: TdxNumberingListIndex;
    FSkipNumbering: Boolean;
    procedure AddNumberingListUsingHistory;
    procedure CreateBulletedListLevels;
    procedure CreateMultilevelListLevels;
    procedure CreateNewList;
    procedure CreateSimpleNumberingListLevels;
    procedure SetLegacyProperties(ALevel: IdxListLevel; ALegacyIndent, ALegacySpace: Integer);

    class procedure BulletedParagraphKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ListOverrideKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure ParagraphLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SimpleNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure SkipNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;

    function IsMultilevelList: Boolean;
    function IsNewListLevelInfoPresent: Boolean;
    function IsSimpleList: Boolean;
    function IsSkipNumbering: Boolean;
    function SectionMultiLevelListCreated: Boolean;
    function ShouldCreateNewAbstractSimpleList: Boolean;
    function ShouldCreateNewList: Boolean;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
    procedure AfterPopRtfState; override;
    procedure BeforePopRtfState; override;

    property IsOldNumberingListCreated: Boolean read FIsOldNumberingListCreated write FIsOldNumberingListCreated;
    property ListLevelIndex: Integer read FListLevelIndex write FListLevelIndex;
    property MultiLevelListIndex: TdxNumberingListIndex read FMultiLevelListIndex write FMultiLevelListIndex;
    property OldLevelNumber: Integer read FOldLevelNumber write FOldLevelNumber;
		property SimpleList: Boolean read FSimpleList write FSimpleList;
    property SimpleListIndex: TdxNumberingListIndex read FSimpleListIndex write FSimpleListIndex;
    property SkipNumbering: Boolean read FSkipNumbering write FSkipNumbering;
  end;

implementation

uses
  dxRichEdit.Utils.PredefinedFontSizeCollection;

{ TdxDestinationOldParagraphNumberingBase }

procedure TdxDestinationOldParagraphNumberingBase.BeforePopRtfState;
begin
  inherited BeforePopRtfState;
  Importer.Position.OldListLevelInfo.TextAfter := FTextAfter;
  Importer.Position.OldListLevelInfo.TextBefore := FTextBefore;
end;

function TdxDestinationOldParagraphNumberingBase.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxDestinationOldParagraphNumberingBase.NestedGroupFinished(ADestination: TdxRichEditRtfDestinationBase);
begin
  inherited NestedGroupFinished(ADestination);
  if ADestination is TdxTextBeforeDestination then
    FTextBefore := TdxTextBeforeDestination(ADestination).Value
  else
    if ADestination is TdxTextAfterDestination then
      FTextAfter := TdxTextAfterDestination(ADestination).Value;
end;

procedure TdxDestinationOldParagraphNumberingBase.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  inherited PopulateKeywordTable(ATable);
  AppendParagraphNumberingDescKeywords(ATable);
end;

procedure TdxDestinationOldParagraphNumberingBase.AppendParagraphNumberingDescKeywords(
  ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('pncard', CardinalKeywordHandler);
  ATable.Add('pndec', DecimalKeywordHandler);
  ATable.Add('pnucltr', UpperCaseAlphabeticalKeywordHandler);
  ATable.Add('pnucrm', UpperCaseRomanKeywordHandler);
  ATable.Add('pnlcltr', LowerCaseAlphabeticalKeywordHandler);
  ATable.Add('pnlcrm', LowerCaseRomanKeywordHandler);
  ATable.Add('pnord', OrdinalKeywordHandler);
  ATable.Add('pnordt', OrdinalTextKeywordHandler);
  ATable.Add('pncnum', NumberingInCircleKeywordHandler);
  ATable.Add('pnuldash', DashedUndrelineKeywordHandler);
  ATable.Add('pnuldashd', DashDottedUnderlineKeywordHandler);
  ATable.Add('pnuldashdd', DashDotDottedUnderlineKeywordHandler);
  ATable.Add('pnulhair', HairlineUnderlineKeywordHandler);
  ATable.Add('pnulth', ThickUnderlineKeywordHandler);
  ATable.Add('pnulwave', WaveUnderlineKeywordHandler);
  ATable.Add('pnul', ContinuousUnderlineKeywordHandler);
  ATable.Add('pnuld', DottedUnderlineKeywordHandler);
  ATable.Add('pnuldb', DoubleUnderlineKeywordHandler);
  ATable.Add('pnulnone', NoneUnderlineKeywordHandler);
  ATable.Add('pnulw', WordUnderlineKeywordHandler);
  ATable.Add('pnf', FontNumberKeywordHandler);
  ATable.Add('pnfs', FontSizeKeywordHandler);
  ATable.Add('pnb', FontBoldKeywordHandler);
  ATable.Add('pni', ItalicKeywordHandler);
  ATable.Add('pncaps', AllCapsKeywordHandler);
  ATable.Add('pnscaps', SmallCapsKeywordHandler);
  ATable.Add('pnstrike', StrikeKeywordHandler);
  ATable.Add('pncf', ForegroundColorKeywordHandler);
  ATable.Add('pnindent', IndentKeywordHandler);
  ATable.Add('pnsp', SpaceKeywordHandler);
  ATable.Add('pnprev', UsePrevKeywordHandler);
  ATable.Add('pnstart', StartAtKeywordHandler);
  ATable.Add('pnhang', HangingIndentKeywordHandler);
  ATable.Add('pnrestart', RestartOnSectionBreakKeywordHandler);
  ATable.Add('pnqc', CenterAlignmentKeywordHandler);
  ATable.Add('pnql', LeftAlignmentKeywordHandler);
  ATable.Add('pnqr', RightAlignmentKeywordHandler);
  ATable.Add('pntxtb', TextBeforeKeywordHandler);
  ATable.Add('pntxta', TextAfterKeywordHandler);
end;

class procedure TdxDestinationOldParagraphNumberingBase.SetNumberingListFormat(AImporter: TdxRichEditDocumentModelRtfImporter; AFormat: TdxNumberingFormat);
begin
  AImporter.Position.OldListLevelInfo.ListLevelProperties.Format := AFormat;
end;

class procedure TdxDestinationOldParagraphNumberingBase.SetUnderlineType(AImporter: TdxRichEditDocumentModelRtfImporter; AUnderlineType: TdxUnderlineType);
begin
  AImporter.Position.CharacterFormatting.FontUnderlineType := AUnderlineType;
end;

class procedure TdxDestinationOldParagraphNumberingBase.CardinalKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.CardinalText);
end;

class procedure TdxDestinationOldParagraphNumberingBase.CenterAlignmentKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.OldListLevelInfo.ListLevelProperties.Alignment := TdxListNumberAlignment.Center;
end;

class procedure TdxDestinationOldParagraphNumberingBase.ContinuousUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.UnderlineWordsOnly := False;
end;

class procedure TdxDestinationOldParagraphNumberingBase.DashDotDottedUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.DashDotDotted);
end;

class procedure TdxDestinationOldParagraphNumberingBase.DashDottedUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.DashDotted);
end;

class procedure TdxDestinationOldParagraphNumberingBase.DashedUndrelineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.Dashed);
end;

class procedure TdxDestinationOldParagraphNumberingBase.DecimalKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.Decimal);
end;

class procedure TdxDestinationOldParagraphNumberingBase.DottedUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.Dotted);
end;

class procedure TdxDestinationOldParagraphNumberingBase.DoubleUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.Double);
end;

class procedure TdxDestinationOldParagraphNumberingBase.FontBoldKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.FontBold := not AHasParameter or
    (AParameterValue <> 0);
end;

class procedure TdxDestinationOldParagraphNumberingBase.FontNumberKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := AImporter.DocumentProperties.DefaultFontNumber;
  AImporter.SetFont(AImporter.DocumentProperties.Fonts.GetRtfFontInfoById(AParameterValue));
end;

class procedure TdxDestinationOldParagraphNumberingBase.FontSizeKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  if not AHasParameter then
    AParameterValue := 24;
  AImporter.Position.CharacterFormatting.DoubleFontSize := Max(TdxPredefinedFontSizeCollection.MinFontSize, AParameterValue);
end;

class procedure TdxDestinationOldParagraphNumberingBase.ForegroundColorKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  AColor: TColor;
begin
  if not AHasParameter then
    AParameterValue := 0;
  AColor := AImporter.DocumentProperties.Colors[AParameterValue];
  AImporter.Position.CharacterFormatting.ForeColor := AColor;
end;

class procedure TdxDestinationOldParagraphNumberingBase.AllCapsKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.AllCaps := not AHasParameter or
    (AParameterValue > 0);
end;

class procedure TdxDestinationOldParagraphNumberingBase.HairlineUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.ThickSingle);
end;

class procedure TdxDestinationOldParagraphNumberingBase.HangingIndentKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.FirstLineIndentType := TdxParagraphFirstLineIndent.Hanging;
end;

class procedure TdxDestinationOldParagraphNumberingBase.IndentKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
var
  AInfo: TdxRtfParagraphFormattingInfo;
begin
  AInfo := AImporter.Position.ParagraphFormattingInfo;
  if AParameterValue < 0 then
  begin
    AInfo.FirstLineIndent := AImporter.UnitConverter.TwipsToModelUnits(-AParameterValue);
    AInfo.FirstLineIndentType := TdxParagraphFirstLineIndent.Hanging;
  end
  else
  begin
    AInfo.FirstLineIndent := AImporter.UnitConverter.TwipsToModelUnits(AParameterValue);
    AInfo.FirstLineIndentType := TdxParagraphFirstLineIndent.Indented;
  end;
end;

class procedure TdxDestinationOldParagraphNumberingBase.ItalicKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.FontItalic := not AHasParameter or
    (AParameterValue <> 0);
end;

class procedure TdxDestinationOldParagraphNumberingBase.LeftAlignmentKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.OldListLevelInfo.ListLevelProperties.Alignment := TdxListNumberAlignment.Left;
end;

class procedure TdxDestinationOldParagraphNumberingBase.LowerCaseAlphabeticalKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.LowerLetter);
end;

class procedure TdxDestinationOldParagraphNumberingBase.LowerCaseRomanKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.LowerRoman);
end;

class procedure TdxDestinationOldParagraphNumberingBase.NoneUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.None);
end;

class procedure TdxDestinationOldParagraphNumberingBase.NumberingInCircleKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.DecimalEnclosedCircle);
end;

class procedure TdxDestinationOldParagraphNumberingBase.OrdinalKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.Ordinal);
end;

class procedure TdxDestinationOldParagraphNumberingBase.OrdinalTextKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.OrdinalText);
end;

class procedure TdxDestinationOldParagraphNumberingBase.RestartOnSectionBreakKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxDestinationOldParagraphNumberingBase.RightAlignmentKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.OldListLevelInfo.ListLevelProperties.Alignment := TdxListNumberAlignment.Right;
end;

class procedure TdxDestinationOldParagraphNumberingBase.SmallCapsKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.AllCaps := not AHasParameter or
    (AParameterValue <> 0);
end;

class procedure TdxDestinationOldParagraphNumberingBase.SpaceKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
//do nothing
end;

class procedure TdxDestinationOldParagraphNumberingBase.StartAtKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.OldListLevelInfo.ListLevelProperties.Start := AParameterValue;
end;

class procedure TdxDestinationOldParagraphNumberingBase.StrikeKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
const
  AStrikeoutTypeMap: array[Boolean] of TdxStrikeoutType = (TdxStrikeoutType.Single, TdxStrikeoutType.None);
begin
  AImporter.Position.CharacterFormatting.FontStrikeoutType := AStrikeoutTypeMap[not AHasParameter or (AParameterValue <> 0)];
end;

class procedure TdxDestinationOldParagraphNumberingBase.TextAfterKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Destination := TdxTextAfterDestination.Create(AImporter);
end;

class procedure TdxDestinationOldParagraphNumberingBase.TextBeforeKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Destination := TdxTextBeforeDestination.Create(AImporter);
end;

class procedure TdxDestinationOldParagraphNumberingBase.ThickUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.ThickSingle);
end;

class procedure TdxDestinationOldParagraphNumberingBase.UpperCaseAlphabeticalKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.UpperLetter);
end;

class procedure TdxDestinationOldParagraphNumberingBase.UpperCaseRomanKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.UpperRoman);
end;

class procedure TdxDestinationOldParagraphNumberingBase.UsePrevKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.OldListLevelInfo.IncludeInformationFromPreviousLevel := True;
end;

class procedure TdxDestinationOldParagraphNumberingBase.WaveUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  SetUnderlineType(AImporter, TdxUnderlineType.Wave);
end;

class procedure TdxDestinationOldParagraphNumberingBase.WordUnderlineKeywordHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Position.CharacterFormatting.UnderlineWordsOnly := True;
end;

{ TdxDestinationOldParagraphNumbering }

constructor TdxDestinationOldParagraphNumbering.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FOldLevelNumber := -1;
  if Importer.Position.OldListLevelInfo = nil then
    Importer.Position.OldListLevelInfo := TdxRtfOldListLevelInfo.Create(Importer.DocumentModel);
  FMultiLevelListIndex := NumberingListIndexListIndexNotSetted;
  FSimpleListIndex := NumberingListIndexListIndexNotSetted;
end;

procedure TdxDestinationOldParagraphNumbering.AddNumberingListUsingHistory;
var
  ADocumentModel: TdxDocumentModel;
  ANumberingList: TdxNumberingList;
begin
  ADocumentModel := Importer.DocumentModel;
  ANumberingList := TdxNumberingList.Create(ADocumentModel, ADocumentModel.AbstractNumberingLists.Count - 1);
  ADocumentModel.AddNumberingListUsingHistory(ANumberingList);
end;

procedure TdxDestinationOldParagraphNumbering.AfterPopRtfState;
var
  AActualNumberingListIndex: TdxNumberingListIndex;
begin
  inherited AfterPopRtfState;
  Importer.Position.ParagraphFormattingInfo.ListLevelIndex := ListLevelIndex;
  AActualNumberingListIndex := IfThen(IsSimpleList, SimpleListIndex, MultiLevelListIndex);
  Importer.Position.ParagraphFormattingInfo.NumberingListIndex := IfThen(not SkipNumbering, AActualNumberingListIndex, NumberingListIndexNoNumberingList);
  Importer.Position.CurrentOldListSkipNumbering := SkipNumbering;
  if IsOldNumberingListCreated then
  begin
    Importer.Position.CurrentOldMultiLevelListIndex := MultiLevelListIndex;
    Importer.Position.CurrentOldSimpleListIndex := SimpleListIndex;
    Importer.Position.CurrentOldSimpleList := SimpleList;
  end;
end;

procedure TdxDestinationOldParagraphNumbering.BeforePopRtfState;
begin
  inherited BeforePopRtfState;
  if ShouldCreateNewList then
    CreateNewList
  else
  begin
    SkipNumbering := IsSkipNumbering;
    SimpleListIndex := Importer.Position.CurrentOldSimpleListIndex;
    MultiLevelListIndex := Importer.Position.CurrentOldMultiLevelListIndex;
  end;
  ListLevelIndex := IfThen(OldLevelNumber >= 0, OldLevelNumber - 1, 0);
end;

procedure TdxDestinationOldParagraphNumbering.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  inherited PopulateKeywordTable(ATable);
  ATable.Add('pnlvl', ParagraphLevelKeywordHandler);
  ATable.Add('pnlvlblt', BulletedParagraphKeywordHandler);
  ATable.Add('pnlvlbody', SimpleNumberingKeywordHandler);
  ATable.Add('pnlvlcont', SkipNumberingKeywordHandler);
  ATable.Add('ilvl', ListLevelKeywordHandler);
  ATable.Add('ls', ListOverrideKeywordHandler);
end;

function TdxDestinationOldParagraphNumbering.IsMultilevelList: Boolean;
begin
  Result := (OldLevelNumber >= 0) and not SimpleList;
end;

function TdxDestinationOldParagraphNumbering.IsNewListLevelInfoPresent: Boolean;
begin
  if IsSimpleList then
    Result := SimpleListIndex >= NumberingListIndexMinValue
  else
    Result := MultiLevelListIndex >= NumberingListIndexMinValue;
end;

function TdxDestinationOldParagraphNumbering.IsSimpleList: Boolean;
begin
  Result := SimpleList;
end;

function TdxDestinationOldParagraphNumbering.IsSkipNumbering: Boolean;
begin
  Result := Importer.Position.OldListLevelInfo.SkipNumbering;
end;

function TdxDestinationOldParagraphNumbering.SectionMultiLevelListCreated: Boolean;
begin
  Result := Importer.Position.CurrentOldMultiLevelListIndex >= NumberingListIndexMinValue;
end;

function TdxDestinationOldParagraphNumbering.ShouldCreateNewAbstractSimpleList: Boolean;
begin
  Result := not (Importer.Position.CurrentOldSimpleList and (Importer.Position.CurrentOldSimpleListIndex >= NumberingListIndexMinValue));
end;

function TdxDestinationOldParagraphNumbering.ShouldCreateNewList: Boolean;
begin
  Result := not IsNewListLevelInfoPresent;
  if Result then
  begin
    if IsMultilevelList then
      Result := not SectionMultiLevelListCreated
    else
      Result := IsSimpleList or not IsSkipNumbering;
  end;
end;

procedure TdxDestinationOldParagraphNumbering.CreateNewList;
var
  AOldListLevelInfo: TdxRtfOldListLevelInfo;
begin
  AOldListLevelInfo := Importer.Position.OldListLevelInfo;
  if SimpleList then
  begin
    CreateSimpleNumberingListLevels;
    SimpleListIndex := Importer.DocumentModel.NumberingLists.Count - 1;
    MultiLevelListIndex := Importer.Position.CurrentOldMultiLevelListIndex;
  end
  else
    if AOldListLevelInfo.ListLevelProperties.Format = TdxNumberingFormat.Bullet then
    begin
      CreateBulletedListLevels;
      MultiLevelListIndex := Importer.DocumentModel.NumberingLists.Count - 1;
      SimpleListIndex := Importer.Position.CurrentOldSimpleListIndex;
    end
    else
    begin
      CreateMultilevelListLevels;
      MultiLevelListIndex := Importer.DocumentModel.NumberingLists.Count - 1;
      SimpleListIndex := Importer.Position.CurrentOldSimpleListIndex;
    end;
end;

procedure TdxDestinationOldParagraphNumbering.CreateBulletedListLevels;
var
  ADocumentModel: TdxDocumentModel;
  ALevelOffset: Integer;
  AbstractNumberingList: TdxAbstractNumberingList;
  I: Integer;
  ALevel: TdxListLevel;
  AFirstLineIndent: Integer;
begin
  ADocumentModel := Importer.DocumentModel;
  ALevelOffset := ADocumentModel.UnitConverter.DocumentsToModelUnits(150);
  AbstractNumberingList := TdxAbstractNumberingList.Create(ADocumentModel);
  ADocumentModel.AddAbstractNumberingListUsingHistory(AbstractNumberingList);
  for I := 0 to AbstractNumberingList.Levels.Count - 1 do
  begin
    ALevel := TdxListLevel.Create(ADocumentModel);
    ALevel.CharacterProperties.CopyFrom(Importer.Position.CharacterFormatting);
    AFirstLineIndent := ALevelOffset * I + Importer.Position.ParagraphFormattingInfo.FirstLineIndent;
    ALevel.ParagraphProperties.LeftIndent := AFirstLineIndent;
    ALevel.ListLevelProperties.DisplayFormatString := Format('%s%s', [Importer.Position.OldListLevelInfo.TextBefore, Importer.Position.OldListLevelInfo.TextAfter]);
    ALevel.ListLevelProperties.TemplateCode := TdxNumberingListHelper.GenerateNewTemplateCode(ADocumentModel);
    SetLegacyProperties(ALevel, 0, 0);
    AbstractNumberingList.Levels[I] := ALevel;
  end;
  FIsOldNumberingListCreated := True;
  AddNumberingListUsingHistory;
end;

procedure TdxDestinationOldParagraphNumbering.CreateMultilevelListLevels;
var
  I: Integer;
  ADocumentModel: TdxDocumentModel;
  AbstractNumberingList: TdxAbstractNumberingList;
  ALevelInfo: TdxRtfOldListLevelInfo;
  ALevel: TdxListLevel;
  AFirstLineIndent: Integer;
  AFormatString: string;
begin
  ADocumentModel := Importer.DocumentModel;
  AbstractNumberingList := TdxAbstractNumberingList.Create(ADocumentModel);
  ADocumentModel.AddAbstractNumberingListUsingHistory(AbstractNumberingList);
  for I := 0 to AbstractNumberingList.Levels.Count - 1 do
  begin
    ALevelInfo := Importer.Position.OldListLevelInfoCollection[I + 1];
    ALevel := TdxListLevel.Create(ADocumentModel);
    AFirstLineIndent := ALevelInfo.Indent;
    ALevel.ParagraphProperties.LeftIndent := AFirstLineIndent;
    ALevel.CharacterProperties.CopyFrom(Importer.Position.CharacterFormatting);
    AFormatString := Format('%s%%%d:d%s', [ALevelInfo.TextBefore, I, ALevelInfo.TextAfter]);
    if (I > 0) and ALevelInfo.IncludeInformationFromPreviousLevel then
      AFormatString := (AbstractNumberingList.Levels[I - 1] as TdxListLevel).ListLevelProperties.DisplayFormatString + AFormatString;
    ALevel.ListLevelProperties.DisplayFormatString;
    SetLegacyProperties(ALevel, 0, 0);
    AbstractNumberingList.Levels[I] := ALevel;
  end;
  FIsOldNumberingListCreated := True;
  AddNumberingListUsingHistory;
end;

procedure TdxDestinationOldParagraphNumbering.CreateSimpleNumberingListLevels;
var
  ALevel: TdxListLevel;
  AListLevel: IdxListLevel;
  ANewList, APrevNumberingList: TdxNumberingList;
  ADocumentModel: TdxDocumentModel;
  I, AFirstLineIndent, AStart: Integer;
  ANumberingFormat: TdxNumberingFormat;
  AOverrideListLevel: TdxOverrideListLevel;
  AAbstractNumberingList: TdxAbstractNumberingList;
  AAbstractNumberingListIndex: TdxAbstractNumberingListIndex;
begin 
  ADocumentModel := Importer.DocumentModel;
  if ShouldCreateNewAbstractSimpleList then
  begin
    AAbstractNumberingList := TdxAbstractNumberingList.Create(ADocumentModel);
    ADocumentModel.AddAbstractNumberingListUsingHistory(AAbstractNumberingList);
    for I := 0 to AAbstractNumberingList.Levels.Count - 1 do
    begin
      ALevel := TdxListLevel.Create(ADocumentModel);
      AAbstractNumberingList.Levels[I] := ALevel;
      ALevel.CharacterProperties.CopyFrom(Importer.Position.CharacterFormatting);
      AFirstLineIndent := Importer.Position.ParagraphFormattingInfo.FirstLineIndent + 150 * I;
      ALevel.ParagraphProperties.LeftIndent := AFirstLineIndent;
      ALevel.ListLevelProperties.DisplayFormatString :=
        Format('%s%%%d:s%s', [Importer.Position.OldListLevelInfo.TextBefore, I, Importer.Position.OldListLevelInfo.TextAfter]);
      ALevel.ListLevelProperties.TemplateCode := TdxNumberingListHelper.GenerateNewTemplateCode(ADocumentModel);
      SetLegacyProperties(ALevel, 0, 0);
      if Importer.Position.OldListLevelInfo.ListLevelProperties.Start > 0 then
        ALevel.ListLevelProperties.Start := Importer.Position.OldListLevelInfo.ListLevelProperties.Start;
      ANumberingFormat := Importer.Position.OldListLevelInfo.ListLevelProperties.Format;
      if ANumberingFormat <> TdxNumberingFormat.Decimal then
        ALevel.ListLevelProperties.Format := ANumberingFormat;
    end;
    FIsOldNumberingListCreated := True;
    AddNumberingListUsingHistory;
  end
  else
  begin
    AAbstractNumberingListIndex := ADocumentModel.NumberingLists[Importer.Position.CurrentOldSimpleListIndex].AbstractNumberingListIndex;
    APrevNumberingList := ADocumentModel.NumberingLists.Last;

    ANewList := TdxNumberingList.Create(ADocumentModel, AAbstractNumberingListIndex);
    AOverrideListLevel := TdxOverrideListLevel.Create(ADocumentModel);
    AStart := Importer.Position.OldListLevelInfo.ListLevelProperties.Start;
    if AStart >= 0 then
      AOverrideListLevel.ListLevelProperties.Start := AStart;
    ANumberingFormat := Importer.Position.OldListLevelInfo.ListLevelProperties.Format;
    if ANumberingFormat <> TdxNumberingFormat.Decimal then
      AOverrideListLevel.ListLevelProperties.Format := ANumberingFormat;
    if (APrevNumberingList <> nil) then
    begin
      if not Supports(APrevNumberingList.Levels[0], IdxListLevel, AListLevel) then
        Assert(False);
      if AListLevel.ListLevelProperties.Format <> AOverrideListLevel.ListLevelProperties.Format then
        AOverrideListLevel.SetOverrideStart(True);
    end;
    AOverrideListLevel.CharacterProperties.CopyFrom(Importer.Position.CharacterFormatting);
    AFirstLineIndent := Importer.Position.ParagraphFormattingInfo.FirstLineIndent;
    AOverrideListLevel.ParagraphProperties.LeftIndent := AFirstLineIndent;
    AOverrideListLevel.ListLevelProperties.DisplayFormatString :=
      Format('%s%%%d:s%s', [Importer.Position.OldListLevelInfo.TextBefore, 0, Importer.Position.OldListLevelInfo.TextAfter]);
    AOverrideListLevel.ListLevelProperties.TemplateCode := TdxNumberingListHelper.GenerateNewTemplateCode(ADocumentModel);
    SetLegacyProperties(AOverrideListLevel, 0, 0);
    ANewList.Levels[0] := AOverrideListLevel;
    ADocumentModel.AddNumberingListUsingHistory(ANewList);
  end;
end;

procedure TdxDestinationOldParagraphNumbering.SetLegacyProperties(ALevel: IdxListLevel; ALegacyIndent, ALegacySpace: Integer);
begin
  ALevel.ListLevelProperties.Legacy := True;
  ALevel.ListLevelProperties.LegacySpace := ALegacyIndent;
  ALevel.ListLevelProperties.LegacyIndent := ALegacySpace;
end;

class procedure TdxDestinationOldParagraphNumbering.BulletedParagraphKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  SetNumberingListFormat(AImporter, TdxNumberingFormat.Bullet);
end;

class procedure TdxDestinationOldParagraphNumbering.ListLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.ListLevelIndex := AParameterValue;
end;

class procedure TdxDestinationOldParagraphNumbering.ListOverrideKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.ParagraphFormattingInfo.NumberingListIndex := AParameterValue - 1;
end;

class procedure TdxDestinationOldParagraphNumbering.ParagraphLevelKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  TdxDestinationOldParagraphNumbering(AImporter.Destination).OldLevelNumber := AParameterValue;
  TdxDestinationOldParagraphNumbering(AImporter.Destination).SimpleList := False;
  AImporter.Position.OldListLevelInfo.CopyFrom(AImporter.Position.OldListLevelInfoCollection[AParameterValue]);
end;

class procedure TdxDestinationOldParagraphNumbering.SimpleNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  TdxDestinationOldParagraphNumbering(AImporter.Destination).SimpleList := True;
end;

class procedure TdxDestinationOldParagraphNumbering.SkipNumberingKeywordHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean);
begin
  AImporter.Position.OldListLevelInfo.SkipNumbering := True;
end;

end.
