{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationColorTable;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, Windows, Generics.Collections,
  dxCoreClasses, dxRichEdit.Import.Rtf;

type
  TdxColorTableDestination = class(TdxRichEditRtfDestinationBase)
  const
    AutoColor: TColor = clNone;
  private
    R: Integer;
    G: Integer;
    B: Integer;
    FWasColor: Boolean;
    function IsColorValid: Boolean;
    procedure Reset;
  protected
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    function GetKeywordHT: TdxKeywordTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;

    function ProcessKeywordCore(const AKeyword: string; AParameterValue: Integer;
      AHasParameter: Boolean): Boolean; override;
    procedure ProcessCharCore(AChar: Char); override;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
  end;

implementation

uses
  dxCore;

var
  FDefaultMSWordColor: TColors;

constructor TdxColorTableDestination.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
end;

function TdxColorTableDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

function TdxColorTableDestination.GetKeywordHT: TdxKeywordTranslatorTable;
begin
  Result := nil;
end;

procedure TdxColorTableDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
// do nothing
end;

procedure TdxColorTableDestination.Reset;
begin
  FWasColor := False;
end;

function TdxColorTableDestination.ProcessKeywordCore(const AKeyword: string;
  AParameterValue: Integer; AHasParameter: Boolean): Boolean;
begin
  if not AHasParameter  then
    AParameterValue := 0;
  Result := True;
  if AKeyword = 'bin' then
    Result := inherited ProcessKeywordCore(AKeyword, AParameterValue, AHasParameter)
  else
    if AKeyword = 'red' then
    begin
      R := AParameterValue;
      FWasColor := True;
    end
    else
      if AKeyword = 'green' then
      begin
        G := AParameterValue;
        FWasColor := True;
      end
      else
        if AKeyword = 'blue' then
        begin
          B := AParameterValue;
          FWasColor := True;
        end
        else
          Result := False;
end;

function TdxColorTableDestination.IsColorValid: Boolean;

  function IsValid(Value: Integer): Boolean;
  begin
    Result := (Value >= 0) and (Value <= 255);
  end;

begin
  Result := IsValid(R) and IsValid(G) and IsValid(B);
end;

procedure TdxColorTableDestination.ProcessCharCore(AChar: Char);
var
  AColor: TColor;
  ANewColorIndex: Integer;
  AColors: TList<TColor>;
begin
  if (AChar = ';') then
  begin
    if FWasColor then
    begin
      Assert(IsColorValid);
      Importer.DocumentProperties.Colors.Add(RGB(R, G, B));
    end
    else
    begin
      AColors := Importer.DocumentProperties.Colors;
      ANewColorIndex := AColors.Count;
      if ANewColorIndex < Length(FDefaultMSWordColor) then
      begin
        AColor := FDefaultMSWordColor[ANewColorIndex];
        Importer.DocumentProperties.Colors.Add(AColor);
      end;
    end;
    Reset;
  end;
end;

procedure PopulateColors;
begin
  SetLength(FDefaultMSWordColor, 16);
  FDefaultMSWordColor[0] := RGB(0, 0, 0);
  FDefaultMSWordColor[1] := RGB(0, 0, 255);
  FDefaultMSWordColor[2] := RGB(0, 255, 255);
  FDefaultMSWordColor[3] := RGB(0, 255, 0);
  FDefaultMSWordColor[4] := RGB(255, 0, 255);
  FDefaultMSWordColor[5] := RGB(255, 0, 0);
  FDefaultMSWordColor[6] := RGB(255, 255, 0);
  FDefaultMSWordColor[7] := RGB(255, 255, 255);
  FDefaultMSWordColor[8] := RGB(0, 0, 128);
  FDefaultMSWordColor[9] := RGB(0, 128, 128);
  FDefaultMSWordColor[10] := RGB(0, 128, 0);
  FDefaultMSWordColor[11] := RGB(128, 0, 128);
  FDefaultMSWordColor[12] := RGB(128, 0, 0);
  FDefaultMSWordColor[13] := RGB(128, 128, 0);
  FDefaultMSWordColor[14] := RGB(128, 128, 128);
  FDefaultMSWordColor[15] := RGB(192, 192, 192);
end;

initialization
  PopulateColors;

finalization
  SetLength(FDefaultMSWordColor, 0);

end.
