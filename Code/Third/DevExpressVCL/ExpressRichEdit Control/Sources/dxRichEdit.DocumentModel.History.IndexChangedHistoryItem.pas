{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.History.IndexChangedHistoryItem;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  SysUtils, dxRichEdit.DocumentModel.Core;

type
  { TdxIndexChangedHistoryItemCore }

  TdxIndexChangedHistoryItemCore<TActions> = class(TdxHistoryItem)
  private
    FOldIndex: Integer;
    FNewIndex: Integer;
    FChangeActions: TActions;
  protected
    procedure UndoCore; override;
    procedure RedoCore; override;
  public
    function GetObject: IdxIndexBasedObject<TActions>; virtual; abstract;

    property OldIndex: Integer read FOldIndex write FOldIndex;
    property NewIndex: Integer read FNewIndex write FNewIndex;
    property ChangeActions: TActions read FChangeActions write FChangeActions;
  end;

  { TdxIndexChangedHistoryItem }

  TdxIndexChangedHistoryItem<TActions> = class(TdxIndexChangedHistoryItemCore<TActions>)
  private
    FObj: IdxIndexBasedObject<TActions>;
  public
    constructor Create(const ADocumentModelPart: TdxDocumentModelPart; const AObj: IdxIndexBasedObject<TActions>); reintroduce; virtual;
    function GetObject: IdxIndexBasedObject<TActions>; override;
  end;

implementation

{ TdxIndexChangedHistoryItemCore<TActions> }

procedure TdxIndexChangedHistoryItemCore<TActions>.RedoCore;
var
  AObj: IdxIndexBasedObject<TActions>;
begin
  AObj := GetObject;
  AObj.SetIndex(NewIndex, ChangeActions);
end;

procedure TdxIndexChangedHistoryItemCore<TActions>.UndoCore;
var
  AObj: IdxIndexBasedObject<TActions>;
begin
  AObj := GetObject;
  AObj.SetIndex(OldIndex, ChangeActions);
end;

{ TdxIndexChangedHistoryItem<TActions> }

constructor TdxIndexChangedHistoryItem<TActions>.Create(
  const ADocumentModelPart: TdxDocumentModelPart;
  const AObj: IdxIndexBasedObject<TActions>);
begin
  inherited Create(ADocumentModelPart);
  FObj := AObj;
end;

function TdxIndexChangedHistoryItem<TActions>.GetObject: IdxIndexBasedObject<TActions>;
begin
  Result := FObj;
end;


end.
