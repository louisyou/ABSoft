{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Commands;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Graphics, Controls, Generics.Collections, dxRichEdit.Options,
  dxCoreClasses, dxRichEdit.DocumentModel.ParagraphFormatting, dxRichEdit.DocumentModel.CharacterFormatting,
  dxRichEdit.View.ViewInfo, dxRichEdit.Control, dxRichEdit.View.Core, dxRichEdit.DocumentModel.PieceTableIterators,
  dxRichEdit.Control.HitTest, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.View.PageViewInfoGenerator,
  dxRichEdit.Commands.IDs, dxRichEdit.Utils.FastComparer, dxRichEdit.DocumentModel.Styles,
  dxRichEdit.Utils.PredefinedFontSizeCollection, dxRichEdit.DocumentModel.TabFormatting, dxRichEdit.InnerControl,
  dxRichEdit.DocumentModel.Core, dxRichEdit.Platform.Font, dxRichEdit.DocumentLayout.Position;

type
  TdxCommand = class;

  TdxCommandSourceType = (Unknown, Menu, Keyboard, Mouse);

  { IdxCommandUIState }

  IdxCommandUIState = interface 
  ['{999C52BA-7260-4AFC-90AF-301D267711CC}']
    function GetEnabled: Boolean;
    function GetVisible: Boolean;
    function GetChecked: Boolean;
    procedure SetEnabled(const Value: Boolean);
    procedure SetVisible(const Value: Boolean);
    procedure SetChecked(const Value: Boolean);

    property Enabled: Boolean read GetEnabled write SetEnabled; 
    property Visible: Boolean read GetVisible write SetVisible; 
    property Checked: Boolean read GetChecked write SetChecked; 
  end;

  { IdxCommandExecutionListenerService }

  IdxCommandExecutionListenerService = interface
  ['{8E5060BE-CCB1-442E-8C4E-A27C7D2C8345}']
    procedure BeginCommandExecution(ACommand: TdxCommand; AState: IdxCommandUIState);
		procedure EndCommandExecution(ACommand: TdxCommand; AState: IdxCommandUIState);
  end;

  { TdxDefaultCommandUIState }

  TdxDefaultCommandUIState = class(TInterfacedObject, IdxCommandUIState) 
  private
    FIsEnabled: Boolean; 
    FIsChecked: Boolean; 
    FIsVisible: Boolean; 
  protected
    function GetEnabled: Boolean; virtual;
    function GetVisible: Boolean; virtual;
    function GetChecked: Boolean; virtual;
    procedure SetEnabled(const Value: Boolean); virtual;
    procedure SetVisible(const Value: Boolean); virtual;
    procedure SetChecked(const Value: Boolean); virtual;
  public
    constructor Create;

    property Enabled: Boolean read GetEnabled write SetEnabled; 
    property Visible: Boolean read GetVisible write SetVisible; 
    property Checked: Boolean read GetChecked write SetChecked; 
  end;

  { TdxCommand }

  TdxCommand = class abstract
  private
    FCommandSourceType: TdxCommandSourceType;
    FHideDisabled: Boolean;
  protected
    function GetCommandSourceType: TdxCommandSourceType; virtual;
    procedure SetCommandSourceType(const Value: TdxCommandSourceType); virtual;
    function ShouldBeExecutedOnKeyUpInSilverlightEnvironment: Boolean; virtual;
    function ShowsModalDialog: Boolean; virtual;

    procedure UpdateUIStateCore(const AState: IdxCommandUIState); virtual; abstract;
    procedure UpdateUIStateViaService(const AState: IdxCommandUIState); virtual;
    function ServiceProvider: IdxServiceProvider; virtual;
  public
    procedure Execute; virtual;

    function CanExecute: Boolean; virtual;
    function CreateDefaultCommandUIState: IdxCommandUIState; virtual;
    procedure ForceExecute(const AState: IdxCommandUIState); virtual; abstract;
    procedure UpdateUIState(const AState: IdxCommandUIState); virtual;

    property CommandSourceType: TdxCommandSourceType read GetCommandSourceType write SetCommandSourceType;
    property HideDisabled: Boolean read FHideDisabled write FHideDisabled;
  end;

  { TdxControlCommand }

  TdxControlCommand = class abstract(TdxCommand)
  private
    FRichEditControl: IdxRichEditControl;

    function GetControl: TCustomControl;
  protected
    function GetCommandExecutionListener: IdxCommandExecutionListenerService; virtual;
    procedure NotifyBeginCommandExecution(AState: IdxCommandUIState); virtual;
    procedure NotifyEndCommandExecution(AState: IdxCommandUIState); virtual;

    property Control: TCustomControl read GetControl;
  public
    constructor Create(const ARichEditControl: IdxRichEditControl); virtual;

    property RichEditControl: IdxRichEditControl read FRichEditControl;
  end;

  { TdxRichEditCommandBase }

  TdxRichEditCommandBase = class abstract(TdxControlCommand)
  private
    function GetActivePieceTable: TdxPieceTable;
    function GetDocumentModel: TdxDocumentModel;
    function GetInnerControl: IdxInnerControl;
    function GetOptions: TdxRichEditControlOptionsBase;
  protected
    function GetActiveView: TdxRichEditView;

    procedure CheckExecutedAtUIThread; virtual;

    procedure ApplyCommandsRestriction(const AState: IdxCommandUIState; AOption: TdxDocumentCapability; AdditionEnabledCondition: Boolean = True);
    procedure ApplyCommandRestrictionOnEditableControl(const AState: IdxCommandUIState; AOption: TdxDocumentCapability); overload; virtual;
    procedure ApplyCommandRestrictionOnEditableControl(const AState: IdxCommandUIState; AOption: TdxDocumentCapability; AdditionEnabledCondition: Boolean); overload; virtual;
    procedure ApplyDocumentProtectionToSelectedCharacters(const AState: IdxCommandUIState); virtual;
    procedure ApplyDocumentProtectionToSelectedParagraphs(const AState: IdxCommandUIState);
    function CanEditSelection: Boolean; virtual;

    function IsContentEditable: Boolean; virtual;
    function ExtendSelectionToParagraphBoundary: TObjectList<TdxSelectionItem>;
    function ExtendToParagraphBoundary(ASelectionItem: TdxSelectionItem): TdxSelectionItem;

    property ActivePieceTable: TdxPieceTable read GetActivePieceTable;
    property ActiveView: TdxRichEditView read GetActiveView;
    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property InnerControl: IdxInnerControl read GetInnerControl;
  public
    class function Id: TdxRichEditCommandId; virtual;


    property Options: TdxRichEditControlOptionsBase read GetOptions;


  end;

  { TdxRichEditCommand }

  TdxRichEditCommand = class abstract(TdxRichEditCommandBase)
  end;
  TdxRichEditCommandClass = class of TdxRichEditCommand;

  { TdxRichEditMenuItemSimpleCommand }

  TdxRichEditMenuItemSimpleCommand = class(TdxRichEditCommand)
  protected
    procedure ExecuteCore; virtual; abstract;
    function GetForceVisible: Boolean;
  public
    procedure ForceExecute(const AState: IdxCommandUIState); override;
  end;

  { TdxHistoryCommandBase }

  TdxHistoryCommandBase = class abstract(TdxRichEditMenuItemSimpleCommand)
  protected
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

    function CanPerformHistoryOperation(AHistory: TdxDocumentHistory): Boolean; virtual; abstract;
    procedure InvalidateDocumentLayout;
    procedure PerformHistoryOperation(AHistory: TdxDocumentHistory); virtual; abstract;
  public
    procedure ExecuteCore; override;
  end;

  { TdxUndoCommand }

  TdxUndoCommand = class(TdxHistoryCommandBase)
  protected
    function CanPerformHistoryOperation(AHistory: TdxDocumentHistory): Boolean; override;
    procedure PerformHistoryOperation(AHistory: TdxDocumentHistory); override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxRedoCommand }

  TdxRedoCommand = class(TdxHistoryCommandBase)
  protected
    function CanPerformHistoryOperation(AHistory: TdxDocumentHistory): Boolean; override;
    procedure PerformHistoryOperation(AHistory: TdxDocumentHistory); override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxRichEditCaretBasedCommand }

  TdxRichEditCaretBasedCommand = class(TdxRichEditMenuItemSimpleCommand)
  strict private
    function GetCaretPosition: TdxCaretPosition;
  protected
    procedure ApplyLayoutPreferredPageIndex(APreferredPageIndex: Integer); virtual;
    procedure UpdateCaretPosition(ADetailDevel: TdxDocumentLayoutDetailsLevel); virtual;

    property CaretPosition: TdxCaretPosition read GetCaretPosition;
  end;

  { TdxEnsureCaretVisibleHorizontallyCommand }

  TdxEnsureCaretVisibleHorizontallyCommand = class(TdxRichEditCaretBasedCommand)
  protected
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

    function CalculateOptimalCaretHorizontalPosition(const ALogicalVisibleBounds, ACaretBounds: TRect): Integer;
    function IsCaretVisible(const ALogicalVisibleBounds, ACaretBounds: TRect): Boolean;
    procedure ScrollToMakeCaretVisible(const ALogicalVisibleBounds, ACaretBounds: TRect);
  public
    procedure ExecuteCore; override;

  end;

  { TdxEnsureCaretVisibleVerticallyCommand }

  TdxCaretScrollDirection = (None, Unknown, Up, Down);
  TdxSelectionMovementVerticalDirection = (Up, Down);

  TdxEnsureCaretVisibleVerticallyCommand = class(TdxRichEditCaretBasedCommand)
  private
    FCurrentRow: TdxRow;
    FCurrentPage: TdxPage;
		FCurrentPageViewInfo: TdxPageViewInfo;
		FRelativeCaretPosition: Double;
    procedure SetRelativeCaretPosition(Value: Double);
  protected
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

    procedure AdjustAndSaveCaretPosition;
    function BothPositionsWillValid(ACurrentCaretPosition, AAdjustedCaretPosition: TdxCaretPosition): Boolean;
    function CalculateActualInvisiblePageBounds(APageViewInfo: TdxPageViewInfo; ADirection: TdxSelectionMovementVerticalDirection): TRect;
    function CalculateDirectionToInvisibleCaret: TdxSelectionMovementVerticalDirection;
    function CalculateOffsetToVisibleArea(AInitialCaretPhysicalTop: Integer): Integer;
    function CalculatePhysicalOffsetToCaret: Integer;
    function CalculatePhysicalOffsetToCaretAtVisiblePage: Integer;
    function CalculatePhysicalOffsetToCaretAtInvisiblePage: Integer;
    function CalculatePhysicalOffsetForCaretPositionRowVisibilityCore(APageViewInfo: TdxPageViewInfo; ARow: TdxRow): Integer;
    function CalculatePhysicalOffsetForRowBoundsVisibility(const AViewBounds, APhysicalRowBounds: TRect): Integer; virtual;
    function CalculateScrollDirectionToCaret(APos: TdxCaretPosition): TdxCaretScrollDirection;
    function CalculateTargetRowLogicalBounds(APageViewInfo: TdxPageViewInfo; ARow: TdxRow): TRect; virtual;
    function CreateAdjustedCaretPosition: TdxCaretPosition;
    function IsCaretVisible: Boolean;
    function LookupInvisiblePageWithCaret(ADirection: TdxSelectionMovementVerticalDirection): TdxPageViewInfo;
    procedure SaveCaretPosition(APos: TdxCaretPosition);
    procedure ScrollToAlreadyFormattedContentCore(AVerticalOffset: Integer);
    procedure ScrollToMakeCaretVisible;
    procedure ScrollToSetRelativeCaretPosition;
    procedure ScrollVerticallyByPhysicalOffset(AOffset: Integer);
    function ShouldAdjustCaretPosition: Boolean;
    function ValidateRowBounds(APageViewInfo: TdxPageViewInfo; const ARowBounds: TRect): TRect;

    property CurrentRow: TdxRow read FCurrentRow;
    property CurrentPage: TdxPage read FCurrentPage;
		property CurrentPageViewInfo: TdxPageViewInfo read FCurrentPageViewInfo;
  public
    constructor Create(const ARichEditControl: IdxRichEditControl); override;
    procedure ExecuteCore; override;

    property RelativeCaretPosition: Double read FRelativeCaretPosition write SetRelativeCaretPosition;
  end;

  TdxEnsureCaretVisibleVerticallyForMovePrevNextPageCommand = class(TdxEnsureCaretVisibleVerticallyCommand)
  private
    FPhysicalOffsetAboveTargetRow: Integer;
  protected
    function CalculatePhysicalOffsetForRowBoundsVisibility(const AViewBounds: TRect;
      const APhysicalRowBounds: TRect): Integer; override;
    function CalculateTargetRowLogicalBounds(APageViewInfo: TdxPageViewInfo;
      ARow: TdxRow): TRect; override;
    function CalculateTopmostRow(APage: TdxPage): TdxRow;
  public
    property PhysicalOffsetAboveTargetRow: Integer read FPhysicalOffsetAboveTargetRow write FPhysicalOffsetAboveTargetRow;
  end;

  { TdxPlaceCaretToPhysicalPointCommand }

  TdxPlaceCaretToPhysicalPointCommand = class(TdxRichEditCaretBasedCommand)
  private
    FPhysicalPoint: TPoint;
    FUpdateCaretX: Boolean;
    function CalculatePreferredPageIndex(AHitTestResult: TdxRichEditHitTestResult): Integer;
  protected
    procedure ChangeSelection(ASelection: TdxSelection;
      ALogPosition: TdxDocumentLogPosition; AHitTestResult: TdxRichEditHitTestResult); virtual;
    function ChangeSelectionEnd(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition; AHitTestResult: TdxRichEditHitTestResult): Boolean; virtual;
    procedure ChangeSelectionStart(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition; AHitTestResult: TdxRichEditHitTestResult); virtual;
    function SelectToTheEndOfRow(ASelection: TdxSelection; ARow: TdxRow; AExtendSelection: Boolean): Boolean;
    procedure ValidateSelection; virtual;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

		function ExtendSelection: Boolean; virtual;
    function HitTestOnlyInPageClientBounds: Boolean; virtual;
  public
    constructor Create(const ARichEditControl: IdxRichEditControl); override;
    procedure ExecuteCore; override;

    property PhysicalPoint: TPoint read FPhysicalPoint write FPhysicalPoint;
    property UpdateCaretX: Boolean read FUpdateCaretX write FUpdateCaretX;

  end;

  { TdxPlaceCaretToPhysicalPointCommand2 }

  TdxPlaceCaretToPhysicalPointCommand2 = class(TdxPlaceCaretToPhysicalPointCommand)
  protected
    function ChangeSelectionEnd(ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition;
      AHitTestResult: TdxRichEditHitTestResult): Boolean; override;
  end;

  { TdxScrollVerticallyByPhysicalOffsetCommandBase }

  TdxScrollVerticallyByPhysicalOffsetCommandBase = class(TdxRichEditMenuItemSimpleCommand) 
  private
    FUpdateScrollBarBeforeExecution: Boolean; 
    FExecutedSuccessfully: Boolean; 
  protected
    function GetAbsolutePhysicalVerticalOffset: Integer; virtual; abstract;
    procedure GeneratePagesToEnsureScrollingSuccessfull(ADelta: LongInt); virtual; 
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override; 
    function CreateScrollDeltaCalculator(AScrollUp: Boolean): TdxScrollByPhysicalHeightCalculator; virtual; 

    property View: TdxRichEditView read GetActiveView; 
    property ExecutedSuccessfully: Boolean read FExecutedSuccessfully; 
  public
    constructor Create(const AControl: IdxRichEditControl); override;
    procedure ExecuteCore; override;

    property AbsolutePhysicalVerticalOffset: Integer read GetAbsolutePhysicalVerticalOffset; 
    property UpdateScrollBarBeforeExecution: Boolean read FUpdateScrollBarBeforeExecution write FUpdateScrollBarBeforeExecution; 
  end;

  { TdxScrollVerticallyByLogicalOffsetCommand }

  TdxScrollVerticallyByLogicalOffsetCommand = class(TdxScrollVerticallyByPhysicalOffsetCommandBase)
  private
    FLogicalOffset: Integer;
  protected
    function GetAbsolutePhysicalVerticalOffset: Integer; override;
  public
    constructor Create(const AControl: IdxRichEditControl); override;

    property LogicalOffset: Integer read FLogicalOffset write FLogicalOffset;
  end;

  { TdxScrollVerticallyByPhysicalOffsetCommand }

  TdxScrollVerticallyByPhysicalOffsetCommand = class(TdxScrollVerticallyByPhysicalOffsetCommandBase) 
  private
    FPhysicalOffset: Integer; 
  protected
    function GetAbsolutePhysicalVerticalOffset: Integer; override;  
  public
    constructor Create(const AControl: IdxRichEditControl); override; 

    property PhysicalOffset: Integer read FPhysicalOffset write FPhysicalOffset; 
  end;

  { TdxScrollVerticallyByPhysicalOffsetEnsurePageGenerationCommand }

  TdxScrollVerticallyByPhysicalOffsetEnsurePageGenerationCommand = class(TdxScrollVerticallyByPhysicalOffsetCommand)
  protected
    procedure GeneratePagesToEnsureScrollingSuccessfull(ADelta: Integer); override;
    function CalculateNewTopInvisibleHeight(ADelta: Int64): Int64;
  end;

  { TdxScrollHorizontallyByPhysicalOffsetCommand }

  TdxScrollHorizontallyByPhysicalOffsetCommand = class(TdxRichEditMenuItemSimpleCommand)
  strict private
    FPhysicalOffset: Integer;
    FExecutedSuccessfully: Boolean;
    function GetActiveView: TdxRichEditView;
  protected
    procedure ExecuteCore; override;
    procedure PerformScroll;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

    property ExecutedSuccessfully: Boolean read FExecutedSuccessfully;
    property View: TdxRichEditView read GetActiveView;
  public
    constructor Create(const AControl: IdxRichEditControl); override;
    property PhysicalOffset: Integer read FPhysicalOffset write FPhysicalOffset;
  end;

  { TdxSelectionValidator }

  TdxSelectionValidator = class abstract
  strict private
    FPieceTable: TdxPieceTable;
  private
    function GetDocumentModel: TdxDocumentModel;
  protected
    function CalculateOffset(AParagraphIndex: TdxParagraphIndex; AIncrementIndex: Boolean): Integer;
    procedure ChangeFieldSelection(ASelection: TdxSelection; ASelectedInterval: TdxRunInfo; AField: TdxField);
    procedure ChangeFieldSelectionCore(ASelection: TdxSelection; AField: TdxField); virtual; abstract;
    function IsSelectedItemCovered(AItem: TdxSelectionItem): Boolean;
    function ShouldChangeInterval(AInterval: TdxRunInfo; AStart, AEnd: TdxRunIndex): Boolean; virtual;
    function ShouldExtendInterval(AInterval: TdxRunInfo; AField: TdxField): Boolean; virtual;
    procedure ValidateTableActiveSelection(ASelection: TdxSelection); virtual;
    procedure ValidateFieldSelectionCore(ASelection: TdxSelection); virtual;
    procedure ValidateTableSelectionCore(ASelection: TdxSelection); virtual;
  public
    constructor Create(APieceTable: TdxPieceTable); virtual;

    procedure ValidateSelection(ASelection: TdxSelection); virtual;

    property DocumentModel: TdxDocumentModel read GetDocumentModel;
    property PieceTable: TdxPieceTable read FPieceTable;
  end;

  { TdxFieldIsSelectValidator }

  TdxFieldIsSelectValidator = class(TdxSelectionValidator)
  protected
    procedure ChangeFieldSelectionCore(ASelection: TdxSelection;
      AField: TdxField); override;
  end;

  { TdxFieldIsUnselectValidator }

  TdxFieldIsUnselectValidator = class(TdxSelectionValidator)
  protected
    procedure ChangeFieldSelectionCore(ASelection: TdxSelection;
      AField: TdxField); override;
  end;

  { TdxNextCaretPositionVerticalDirectionCalculator }

  TdxNextCaretPositionVerticalDirectionCalculator = class abstract
  strict private
    FControl: IdxRichEditControl;
    FFixedDirection: Boolean;
  private
    function GetActivePieceTable: TdxPieceTable;
    function GetView: TdxRichEditView;
  protected
    function CalculateFirstInvisiblePageIndex: Integer; virtual; abstract;
    function CalculateFirstInvalidPageIndex: Integer; virtual; abstract;
    function CreateInvisiblePageRowsGenerator: TdxInvisiblePageRowsGenerator;
    procedure CorrectRowAndCellViewInfo(APos: TdxCaretPosition; var ARowIndexInColumn: Integer;
      var ASourceCellViewInfo: TdxTableCellViewInfo);
    procedure CorrectGeneratedRowPagesHorizontalLocation(ALayoutManager: TdxPageGeneratorLayoutManager;
      ARow: TdxPageViewInfoRow); virtual; abstract;
    procedure CorrectCaretXOnExtendSelection(ASourceCellViewInfo: TdxTableCellViewInfo; var ACaretX: Integer);
    function GenerateTargetPageViewInfoRow: TdxPageViewInfoRow;
    function GetDefaultPosition: TdxDocumentModelPosition; virtual;
    function GetPageViewInfoRowIndex(APageRows: TdxPageViewInfoRowCollection; APageViewInfo: TdxPageViewInfo): Integer;
    function GetTargetLayoutRowObtainTargetPageViewInfo(APos: TdxCaretPosition; var ACaretX: Integer): TdxRow;
    function GetTargetLayoutRowObtainTargetPageViewInfoCore(APageViewInfo: TdxPageViewInfo;
      var ACaretX: Integer; AColumn: TdxColumn; ARowIndexInColumn: Integer; ASourceCellViewInfo: TdxTableCellViewInfo): TdxRow;
    function GetNextLayoutRowByDirection(ARows: TdxRowCollection; ASourceRowIndex: INteger): TdxRow;
    function IsBoundaryTableCellRowOnCurrentPage(ACell: TdxTableCellViewInfo; AColumn: TdxColumn;
      ASourceRowIndexInColumn, ACaretX: Integer): Boolean;
    function IsCaretAtTheBegginingOfAnotherCell(APos: TdxCaretPosition; ADocumentModel: TdxDocumentModel): Boolean;
    function IsBoundaryRowInCell(AColumn: TdxColumn; ACell: TdxTableCellViewInfo; ASourceRow: TdxRow): Boolean; virtual; abstract;
    function IsCellInBoundaryRow(AColumn: TdxColumn; ACell: TdxTableCellViewInfo; ACaretX: Integer): Boolean; virtual; abstract;
    function ObtainTargetLayoutRow(ARowIndexInColumn, ACaretX: Integer; AColumn: TdxColumn;
      ASourceCell: TdxTableCellViewInfo): TdxRow;
    function ObtainTargetLayoutRowFromNextCellInCurrentTable(ARowIndexInColumn: Integer; ACaretX: Integer;
      AColumn: TdxColumn; ASourceCellViewInfo: TdxTableCellViewInfo): TdxRow;
    function ObtainExistingTargetPageViewInfoRow(APageRows: TdxPageViewInfoRowCollection; ACurrentRowIndex: Integer): TdxPageViewInfoRow; virtual; abstract;
    function ShouldObtainTargetPageViewInfo(ASourceRowIndex, ASourceRowCount: Integer): Boolean; virtual; abstract;
    function ObtainTargetPageViewInfo(ACurrentPageViewInfo: TdxPageViewInfo; APhysicalCaretX: Integer): TdxPageViewInfo; virtual;
    function GetTargetPageArea(APage: TdxPage): TdxPageArea; virtual; abstract;
    function GetTargetLayoutRowCore(ARows: TdxRowCollection): TdxRow; virtual; abstract;
    function GetNextLayoutRowIndexInDirection(ASourceRowIndex: Integer): Integer; virtual; abstract;
    function GetTargetLayoutRowInCell(ARows: TdxRowCollection): TdxRow; virtual; abstract;
    function GetBoundaryRowInColumn(AColumn: TdxColumn): TdxRow; virtual; abstract;
    function GetCellBoundaryRowIndex(AColumn: TdxColumn; AParentCell: TdxTableCellViewInfo): Integer; virtual; abstract;

    property View: TdxRichEditView read GetView;
  public
    constructor Create(AControl: IdxRichEditControl);

    function CalculateNextPosition(ACaretPosition: TdxCaretPosition): TdxDocumentModelPosition;
    function GetTargetCharacter(ARow: TdxRow; ACaretX: Integer): TdxCharacterBox;
    function GetTargetColumn(APage: TdxPage; ACaretX: Integer): TdxColumn;
    function ObtainTargetRowFromCurrentPageViewInfoAndCaretX(APageViewInfo: TdxPageViewInfo;
      ACellViewInfoFromPrevoiusTableViewInfo: TdxTableCellViewInfo; var ACaretX: Integer): TdxRow;

    property ActivePieceTable: TdxPieceTable read GetActivePieceTable;
    property Control: IdxRichEditControl read FControl;
    property FixedDirection: Boolean read FFixedDirection write FFixedDirection;

  end;

  { TdxNextCaretPositionUpDirectionCalculator }

  TdxNextCaretPositionUpDirectionCalculator = class abstract(TdxNextCaretPositionVerticalDirectionCalculator)
  protected
    function GetBoundaryRowInColumn(AColumn: TdxColumn): TdxRow; override; 
    function IsBoundaryRowInCell(AColumn: TdxColumn; ACell: TdxTableCellViewInfo; ASourceRow: TdxRow): Boolean; override; 
    function GetCellBoundaryRowIndex(AColumn: TdxColumn; AParentCell: TdxTableCellViewInfo): Integer; override; 

    function IsCellInBoundaryRow(AColumn: TdxColumn;
      ACell: TdxTableCellViewInfo; ACaretX: Integer): Boolean; override;
    procedure CorrectGeneratedRowPagesHorizontalLocation(ALayoutManager: TdxPageGeneratorLayoutManager;
      ARow: TdxPageViewInfoRow); override;
    function CalculateFirstInvalidPageIndex: Integer; override;
    function CalculateFirstInvisiblePageIndex: Integer; override;
    function GetTargetPageArea(APage: TdxPage): TdxPageArea; override;
    function GetTargetLayoutRowCore(ARows: TdxRowCollection): TdxRow; override;
  end;

  TdxNextCaretPositionDownDirectionCalculator = class abstract(TdxNextCaretPositionVerticalDirectionCalculator)
  protected
    function GetBoundaryRowInColumn(AColumn: TdxColumn): TdxRow; override; 
    function IsBoundaryRowInCell(AColumn: TdxColumn; ACell: TdxTableCellViewInfo; ASourceRow: TdxRow): Boolean; override; 
    function GetCellBoundaryRowIndex(AColumn: TdxColumn; AParentCell: TdxTableCellViewInfo): Integer; override; 
    function GetTargetPageArea(APage: TdxPage): TdxPageArea; override;
    function GetTargetLayoutRowCore(ARows: TdxRowCollection): TdxRow; override;
    function GetTargetLayoutRowInCell(ARows: TdxRowCollection): TdxRow; override;
    function IsCellInBoundaryRow(AColumn: TdxColumn;
      ACell: TdxTableCellViewInfo; ACaretX: Integer): Boolean; override;
    function CalculateFirstInvalidPageIndex: Integer; override;
    function CalculateFirstInvisiblePageIndex: Integer; override;
    procedure CorrectGeneratedRowPagesHorizontalLocation(ALayoutManager: TdxPageGeneratorLayoutManager;
      ARow: TdxPageViewInfoRow); override;
  end;

  { TdxNextCaretPositionLineUpCalculator }

  TdxNextCaretPositionLineUpCalculator = class(TdxNextCaretPositionUpDirectionCalculator)
  protected
    function ShouldObtainTargetPageViewInfo(ASourceRowIndex: Integer;
      ASourceRowCount: Integer): Boolean; override;
    function GetNextLayoutRowIndexInDirection(ASourceRowIndex: Integer): Integer; override;
    function GetTargetLayoutRowInCell(ARows: TdxRowCollection): TdxRow; override;
    function ObtainExistingTargetPageViewInfoRow(APageRows: TdxPageViewInfoRowCollection;
      ACurrentRowIndex: Integer): TdxPageViewInfoRow; override;
  end;

  { TdxExtendNextCaretPositionLineUpCalculator }

  TdxExtendNextCaretPositionLineUpCalculator = class(TdxNextCaretPositionLineUpCalculator)
  end;

  { TdxNextCaretPositionLineDownCalculator }

  TdxNextCaretPositionLineDownCalculator = class(TdxNextCaretPositionDownDirectionCalculator)
  protected
    function GetNextLayoutRowIndexInDirection(ASourceRowIndex: Integer): Integer; override;
    function ShouldObtainTargetPageViewInfo(ASourceRowIndex: Integer;
      ASourceRowCount: Integer): Boolean; override;
    function ObtainExistingTargetPageViewInfoRow(APageRows: TdxPageViewInfoRowCollection;
      ACurrentRowIndex: Integer): TdxPageViewInfoRow; override;
  end;

  { TdxExtendNextCaretPositionLineDownCalculator }

  TdxExtendNextCaretPositionLineDownCalculator = class(TdxNextCaretPositionLineDownCalculator)
  end;

  { TdxExtendKeybordSelectionHorizontalDirectionCalculator }

  TdxExtendKeybordSelectionHorizontalDirectionCalculator = class
  strict private
    FDocumentModel: TdxDocumentModel;
  public
    constructor Create(ADocumentModel: TdxDocumentModel);

    function CalculateNextPosition(ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
    function CalculatePrevPosition(ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;

    property DocumentModel: TdxDocumentModel read FDocumentModel;
  end;

  { TdxToggleShowWhitespaceCommand }

  TdxToggleShowWhitespaceCommand = class(TdxRichEditMenuItemSimpleCommand)
  protected
    procedure ExecuteCore; override;
    procedure EnsureSelectionVisible; virtual;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxChangeIndentCommand }

  TdxChangeIndentCommand = class abstract(TdxRichEditMenuItemSimpleCommand)
  private
    function GetStartIndex: TdxParagraphIndex;
    function GetEndIndex: TdxParagraphIndex;
  protected
    function GetEndParagraphIndex: TdxParagraphIndex;
    function SelectedOnlyParagraphWithNumeration: Boolean; virtual;
    function SelectedFirstParagraphInList: Boolean; virtual;
    function GetStartParagraphLayoutPosition: TdxDocumentLayoutPosition;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

    property StartIndex: TdxParagraphIndex read GetStartIndex;
    property EndIndex: TdxParagraphIndex read GetEndIndex;

  public
    function SelectionBeginFirstRowStartPos: Boolean; virtual;

  end;

  { TdxIncrementIndentCommand }

  TdxIncrementIndentCommand = class(TdxChangeIndentCommand)
  protected
    procedure ExecuteCore; override;
    procedure ProcessNumerationParagraph; virtual;
    procedure IncrementNumerationParagraphIndent; virtual;
    procedure IncrementNumerationFromParagraph; virtual;
    procedure IncrementParagraphIndent; virtual;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxDecrementIndentCommand }

  TdxDecrementIndentCommand = class(TdxChangeIndentCommand)
  protected
    procedure ExecuteCore; override;
    procedure ProcessNumerationParagraph; virtual;
    procedure DecrementNumerationParagraphIndent; virtual;
    procedure DecrementNumerationFromParagraph; virtual;
    procedure DecrementParagraphIndent; virtual;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

implementation

uses
  Math, dxTypeHelpers,
  dxRichEdit.Utils.ChunkedStringBuilder,
  dxRichEdit.Utils.Characters,
  dxRichEdit.Utils.Colors,
  dxRichEdit.DocumentModel.FieldRange,
  dxRichEdit.DocumentModel.ParagraphRange,
  dxRichEdit.DocumentModel.TextRange,
  dxRichEdit.LayoutEngine.Formatter,
  dxRichEdit.Commands.Selection,
  dxRichEdit.Control.Mouse,
  dxRichEdit.Commands.Numbering,
  dxRichEdit.Commands.ChangeProperties;

{ TdxDefaultCommandUIState }

constructor TdxDefaultCommandUIState.Create;
begin
  inherited Create;
  FIsEnabled := True; 
  FIsChecked := False; 
  FIsVisible := True; 
end;

function TdxDefaultCommandUIState.GetChecked: Boolean;
begin
  Result := FIsChecked;
end;

function TdxDefaultCommandUIState.GetEnabled: Boolean;
begin
  Result := FIsEnabled;
end;

function TdxDefaultCommandUIState.GetVisible: Boolean;
begin
  Result := FIsVisible;
end;

procedure TdxDefaultCommandUIState.SetChecked(const Value: Boolean);
begin
  FIsChecked := Value;
end;

procedure TdxDefaultCommandUIState.SetEnabled(const Value: Boolean);
begin
  FIsEnabled := Value;
end;

procedure TdxDefaultCommandUIState.SetVisible(const Value: Boolean);
begin
  FIsVisible := Value;
end;

{ TdxCommand }

procedure TdxCommand.Execute;
var
  AState: IdxCommandUIState;
begin
  AState := CreateDefaultCommandUIState;
  UpdateUIState(AState);
  if AState.Visible and AState.Enabled then
    ForceExecute(AState);
end;

function TdxCommand.GetCommandSourceType: TdxCommandSourceType;
begin
  Result := FCommandSourceType;
end;

function TdxCommand.ServiceProvider: IdxServiceProvider;
begin
  Result := nil;
end;

procedure TdxCommand.SetCommandSourceType(const Value: TdxCommandSourceType);
begin
  FCommandSourceType := Value;
end;

function TdxCommand.ShouldBeExecutedOnKeyUpInSilverlightEnvironment: Boolean;
begin
  Result := False;
end;

function TdxCommand.ShowsModalDialog: Boolean;
begin
  Result := False;
end;

function TdxCommand.CanExecute: Boolean;
var
  AState: IdxCommandUIState;
begin
  AState := CreateDefaultCommandUIState;
  UpdateUIState(AState);
  Result := AState.Visible and AState.Enabled;
end;

function TdxCommand.CreateDefaultCommandUIState: IdxCommandUIState;
begin
  Result := TdxDefaultCommandUIState.Create;
end;

procedure TdxCommand.UpdateUIState(const AState: IdxCommandUIState);
begin
  UpdateUIStateCore(AState);
  UpdateUIStateViaService(AState);
  if HideDisabled and not AState.Enabled then
    AState.Visible := False;
end;

procedure TdxCommand.UpdateUIStateViaService(const AState: IdxCommandUIState);
var
  AServiceProvider: IdxServiceProvider;
begin
  AServiceProvider := ServiceProvider;
  if AServiceProvider = nil then
    Exit;
Assert(False, 'not implemented');

end;

{ TdxControlCommand }

constructor TdxControlCommand.Create(const ARichEditControl: IdxRichEditControl);
begin
  inherited Create;
  FRichEditControl := ARichEditControl;
end;

function TdxControlCommand.GetCommandExecutionListener: IdxCommandExecutionListenerService;
begin
  Result := nil;
end;

function TdxControlCommand.GetControl: TCustomControl;
begin
  Result := TCustomControl(RichEditControl.GetRichEditControl);
end;

procedure TdxControlCommand.NotifyBeginCommandExecution(
  AState: IdxCommandUIState);
var
  AListener: IdxCommandExecutionListenerService;
begin
  AListener := GetCommandExecutionListener;
  if AListener <> nil then
    AListener.BeginCommandExecution(Self, AState);
end;

procedure TdxControlCommand.NotifyEndCommandExecution(
  AState: IdxCommandUIState);
var
  AListener: IdxCommandExecutionListenerService;
begin
  AListener := GetCommandExecutionListener;
  if AListener <> nil then
    AListener.EndCommandExecution(Self, AState);
end;

{ TdxRichEditCommandBase }

function TdxRichEditCommandBase.CanEditSelection: Boolean;
begin
  Result := ActivePieceTable.CanEditSelection;
end;

function TdxRichEditCommandBase.IsContentEditable: Boolean;
begin
  Result := InnerControl.IsEditable;
end;

procedure TdxRichEditCommandBase.CheckExecutedAtUIThread;
begin
  ActiveView.CheckExecutedAtUIThread;
end;

function TdxRichEditCommandBase.ExtendSelectionToParagraphBoundary: TObjectList<TdxSelectionItem>;
var
  AItems: TdxSelectionItems;
  I: Integer;
begin
  Result := TObjectList<TdxSelectionItem>.Create;
  AItems := DocumentModel.Selection.Items;
  for I := 0 to AItems.Count - 1 do
    Result.Add(ExtendToParagraphBoundary(AItems[I]));
end;

function TdxRichEditCommandBase.ExtendToParagraphBoundary(
  ASelectionItem: TdxSelectionItem): TdxSelectionItem;
var
  APieceTable: TdxPieceTable;
  ALastParagraph: TdxParagraph;
begin
  APieceTable := ASelectionItem.PieceTable;
  Result := TdxSelectionItem.Create(APieceTable);
  Result.Start := APieceTable.Paragraphs[ASelectionItem.Interval.NormalizedStart.ParagraphIndex].LogPosition;
  ALastParagraph := APieceTable.Paragraphs[ASelectionItem.Interval.NormalizedEnd.ParagraphIndex];
  if ALastParagraph.Index >= APieceTable.Paragraphs.Count - 1 then
    Result.&End := ALastParagraph.LogPosition + ALastParagraph.Length - 1
  else
    Result.&End := ALastParagraph.LogPosition + ALastParagraph.Length;
end;

procedure TdxRichEditCommandBase.ApplyCommandsRestriction(const AState: IdxCommandUIState;
  AOption: TdxDocumentCapability; AdditionEnabledCondition: Boolean  = True);
begin
  AState.Enabled := AdditionEnabledCondition and (AOption <> TdxDocumentCapability.Disabled) and (AOption <> TdxDocumentCapability.Hidden);
  AState.Visible := (AOption <> TdxDocumentCapability.Hidden);
end;

procedure TdxRichEditCommandBase.ApplyCommandRestrictionOnEditableControl(const AState: IdxCommandUIState;
  AOption: TdxDocumentCapability);
begin
  AState.Enabled := IsContentEditable and (AOption <> TdxDocumentCapability.Disabled) and
    (AOption <> TdxDocumentCapability.Hidden);
  AState.Visible := AOption <> TdxDocumentCapability.Hidden;
end;

procedure TdxRichEditCommandBase.ApplyCommandRestrictionOnEditableControl(const AState: IdxCommandUIState;
  AOption: TdxDocumentCapability; AdditionEnabledCondition: Boolean);
begin
  ApplyCommandRestrictionOnEditableControl(AState, AOption);
  AState.Enabled := AState.Enabled and AdditionEnabledCondition;
end;

procedure TdxRichEditCommandBase.ApplyDocumentProtectionToSelectedCharacters(const AState: IdxCommandUIState);
begin
  AState.Enabled := AState.Enabled and CanEditSelection;
end;

procedure TdxRichEditCommandBase.ApplyDocumentProtectionToSelectedParagraphs(const AState: IdxCommandUIState);
var
  AItems: TObjectList<TdxSelectionItem>;
begin
  if AState.Enabled then
  begin
    if DocumentModel.IsDocumentProtectionEnabled then
    begin
      AItems := ExtendSelectionToParagraphBoundary;
      try
        AState.Enabled := ActivePieceTable.CanEditSelectionItems(AItems);
      finally
        AItems.Free;
      end;
    end;
  end;
end;

function TdxRichEditCommandBase.GetActivePieceTable: TdxPieceTable;
begin
  Result := DocumentModel.ActivePieceTable;
end;

function TdxRichEditCommandBase.GetActiveView: TdxRichEditView;
begin
  Result := InnerControl.ActiveView;
end;

function TdxRichEditCommandBase.GetDocumentModel: TdxDocumentModel;
begin
  Result := InnerControl.DocumentModel;
end;

class function TdxRichEditCommandBase.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.None;
end;

function TdxRichEditCommandBase.GetInnerControl: IdxInnerControl;
begin
  Result := RichEditControl.InnerControl;
end;

function TdxRichEditCommandBase.GetOptions: TdxRichEditControlOptionsBase;
begin
  Result := InnerControl.Options;
end;

{ TdxRichEditCaretBasedCommand }

procedure TdxRichEditCaretBasedCommand.ApplyLayoutPreferredPageIndex(
  APreferredPageIndex: Integer);
const
  AChangeActions = [TdxDocumentModelChangeAction.RaiseSelectionChanged,
    TdxDocumentModelChangeAction.ResetSelectionLayout,
    TdxDocumentModelChangeAction.ResetRuler, TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting,
    TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSpellingCheck];
begin
  if APreferredPageIndex < 0 then
    Exit;
  CaretPosition.PreferredPageIndex := APreferredPageIndex;
  if CaretPosition is TdxHeaderFooterCaretPosition then
  begin
    if DocumentModel.IsUpdateLockedOrOverlapped then
      TdxHeaderFooterCaretPosition(CaretPosition).LayoutPosition.PieceTable.ApplyChangesCore(
        AChangeActions, -1, -1);
  end;
  ActiveView.SelectionLayout.PreferredPageIndex := APreferredPageIndex;
end;

function TdxRichEditCaretBasedCommand.GetCaretPosition: TdxCaretPosition;
begin
  Result := ActiveView.CaretPosition;
end;

procedure TdxRichEditCaretBasedCommand.UpdateCaretPosition(
  ADetailDevel: TdxDocumentLayoutDetailsLevel);
begin
  CaretPosition.Update(ADetailDevel);
end;

{ TdxPlaceCaretToPhysicalPointCommand }

function TdxPlaceCaretToPhysicalPointCommand.CalculatePreferredPageIndex(
  AHitTestResult: TdxRichEditHitTestResult): Integer;
var
  ARequest: TdxRichEditHitTestRequest;
  AResult: TdxRichEditHitTestResult;
begin
  if AHitTestResult.PieceTable.IsMain and AHitTestResult.IsValid(TdxDocumentLayoutDetailsLevel.Page) then
    Exit(AHitTestResult.Page.PageIndex);
  ARequest := TdxRichEditHitTestRequest.Create(DocumentModel.MainPieceTable);
  try
    ARequest.PhysicalPoint := PhysicalPoint;
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Page;
    ARequest.Accuracy := NearestPage or NearestPageArea or NearestColumn or NearestTableRow or
      NearestTableCell or NearestRow or NearestBox or NearestCharacter;
    if HitTestOnlyInPageClientBounds then
      ARequest.Accuracy := ARequest.Accuracy or ActiveView.DefaultHitTestPageAccuracy;
    AResult := ActiveView.HitTestCore(ARequest, HitTestOnlyInPageClientBounds);
    try
      if not AResult.IsValid(TdxDocumentLayoutDetailsLevel.Page) then
        Result := 0
      else
        Result := AResult.Page.PageIndex;
    finally
      AResult.Free;
    end;
  finally
    ARequest.Free;
  end;
end;

procedure TdxPlaceCaretToPhysicalPointCommand.ChangeSelection(
  ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition;
  AHitTestResult: TdxRichEditHitTestResult);
var
  AUsePreviousBoxBounds: Boolean;
begin
  AUsePreviousBoxBounds := ChangeSelectionEnd(ASelection, ALogPosition, AHitTestResult);
  ChangeSelectionStart(ASelection, ALogPosition, AHitTestResult);
  ASelection.UsePreviousBoxBounds := AUsePreviousBoxBounds;
  if ExtendSelection then
    ValidateSelection;
end;

function TdxPlaceCaretToPhysicalPointCommand.ChangeSelectionEnd(
  ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition;
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  ASelectionManager := TdxEnhancedSelectionManager.Create(ActivePieceTable);
  try
    Result := ASelectionManager.ShouldSelectToTheEndOfRow(AHitTestResult) and
      SelectToTheEndOfRow(ASelection, AHitTestResult.Row, ExtendSelection);
    if not Result then
    begin
      ASelection.&End := ALogPosition;
      if ExtendSelection then
        ASelection.UpdateTableSelectionEnd(ALogPosition);
    end;
  finally
    ASelectionManager.Free;
  end;
end;

procedure TdxPlaceCaretToPhysicalPointCommand.ChangeSelectionStart(
  ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition;
  AHitTestResult: TdxRichEditHitTestResult);
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  if not ExtendSelection then
  begin
    ASelection.Start := ASelection.&End;
    ASelection.SetStartCell(ALogPosition);
  end
  else
  begin
    ASelectionManager := TdxEnhancedSelectionManager.Create(ActivePieceTable);
    try
      ASelection.Start := ASelectionManager.ExtendSelectionStartToParagraphMark(ASelection, ALogPosition);
      ASelection.UpdateTableSelectionStart(ALogPosition);
    finally
      ASelectionManager.Free;
    end;
  end;
end;

constructor TdxPlaceCaretToPhysicalPointCommand.Create(
  const ARichEditControl: IdxRichEditControl);
begin
  inherited Create(ARichEditControl);
  FUpdateCaretX := True;
end;

procedure TdxPlaceCaretToPhysicalPointCommand.ExecuteCore;
var
  ARequest: TdxRichEditHitTestRequest;
  AResult: TdxRichEditHitTestResult;
  ALogPosition: TdxDocumentLogPosition;
  ASelection: TdxSelection;
  APageViewInfo: TdxPageViewInfo;
  ACell: TdxTableCellViewInfo;
begin
  CheckExecutedAtUIThread;
  ARequest := TdxRichEditHitTestRequest.Create(DocumentModel.ActivePieceTable);
  try
    ARequest.PhysicalPoint := PhysicalPoint;
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Character;
    ARequest.Accuracy := NearestPage or NearestPageArea or NearestColumn or NearestTableRow or
      NearestTableCell or NearestRow or NearestBox or NearestCharacter;
    if HitTestOnlyInPageClientBounds then
      ARequest.Accuracy := ARequest.Accuracy or ActiveView.DefaultHitTestPageAccuracy;
    AResult := ActiveView.HitTestCore(ARequest, HitTestOnlyInPageClientBounds);
    try
      if not AResult.IsValid(TdxDocumentLayoutDetailsLevel.Character) then
        FreeAndNil(AResult)
      else
      begin
        ALogPosition := AResult.Character.GetFirstPosition(AResult.PieceTable).LogPosition;
        ASelection := DocumentModel.Selection;
        DocumentModel.BeginUpdate;
        try
          ChangeSelection(ASelection, ALogPosition, AResult);
          if UpdateCaretX then
            ApplyLayoutPreferredPageIndex(CalculatePreferredPageIndex(AResult));
        finally
          DocumentModel.EndUpdate;
        end;
        if UpdateCaretX then
        begin
          CaretPosition.Update(TdxDocumentLayoutDetailsLevel.Character);
          Assert(CaretPosition.LayoutPosition.IsValid(TdxDocumentLayoutDetailsLevel.Character));
          APageViewInfo := CaretPosition.PageViewInfo;
          if APageViewInfo <> nil then
          begin
            CaretPosition.X := ActiveView.CreateLogicalPoint(APageViewInfo.ClientBounds, PhysicalPoint).X;
            ACell := CaretPosition.LayoutPosition.TableCell;
            if ACell <> nil then
            begin
              Assert(False, 'not implemented');
            end
            else
              CaretPosition.TableCellTopAnchorIndex := -1;
          end;
        end;
      end;
    finally
      AResult.Free;
    end;
  finally
    ARequest.Free;
  end;
end;

function TdxPlaceCaretToPhysicalPointCommand.ExtendSelection: Boolean;
begin
  Result := False;
end;

function TdxPlaceCaretToPhysicalPointCommand.HitTestOnlyInPageClientBounds: Boolean;
begin
  Result := True;
end;

function TdxPlaceCaretToPhysicalPointCommand.SelectToTheEndOfRow(
  ASelection: TdxSelection; ARow: TdxRow; AExtendSelection: Boolean): Boolean;
var
  ALogPosition: TdxDocumentLogPosition;
  APos: TdxDocumentModelPosition;
  ARun: TdxTextRunBase;
  ALineBreak: Boolean;
begin
  if AExtendSelection then
  begin
    ALogPosition := ARow.GetLastPosition(ActivePieceTable).LogPosition;
    ASelection.&End := ALogPosition + 1;
    ASelection.UpdateTableSelectionEnd(ALogPosition);
    Result := True;
  end
  else
  begin
    ARun := ARow.Boxes.Last.GetRun(ActivePieceTable);
    Result := not (ARun is TdxParagraphRun);
    if Result then
    begin
      APos := ARow.GetLastPosition(ActivePieceTable);
      ALineBreak := ActivePieceTable.TextBuffer[ARun.StartIndex + APos.RunOffset] = TdxCharacters.LineBreak;
      ALogPosition := APos.LogPosition;
      ASelection.&End := ALogPosition;
      if not ALineBreak then
        ASelection.&End := ASelection.&End + 1;
      ASelection.UpdateTableSelectionEnd(ALogPosition);
      Result := not ALineBreak;
    end;
  end;
end;

procedure TdxPlaceCaretToPhysicalPointCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  AState.Enabled := True;
  AState.Visible := True;
end;

procedure TdxPlaceCaretToPhysicalPointCommand.ValidateSelection;
var
  AValidator: TdxFieldIsSelectValidator;
begin
  AValidator := TdxFieldIsSelectValidator.Create(ActivePieceTable);
  try
    AValidator.ValidateSelection(DocumentModel.Selection);
  finally
    AValidator.Free;
  end;
end;

{ TdxPlaceCaretToPhysicalPointCommand2 }

function TdxPlaceCaretToPhysicalPointCommand2.ChangeSelectionEnd(
  ASelection: TdxSelection; ALogPosition: TdxDocumentLogPosition;
  AHitTestResult: TdxRichEditHitTestResult): Boolean;
var
  ASelectionManager: TdxEnhancedSelectionManager;
begin
  ASelectionManager := TdxEnhancedSelectionManager.Create(AHitTestResult.PieceTable);
  try
    Result := (ASelectionManager.ShouldSelectEntireRow(AHitTestResult) and
      SelectToTheEndOfRow(ASelection, AHitTestResult.Row, true)) or
      inherited ChangeSelectionEnd(ASelection, ALogPosition, AHitTestResult)
  finally
    ASelectionManager.Free;
  end;
end;

{ TdxScrollVerticallyByPhysicalOffsetCommand }

constructor TdxScrollVerticallyByPhysicalOffsetCommand.Create(const AControl: IdxRichEditControl);
begin
  inherited Create(AControl);
  FPhysicalOffset := DocumentModel.LayoutUnitConverter.DocumentsToLayoutUnits(50);
end;

function TdxScrollVerticallyByPhysicalOffsetCommand.GetAbsolutePhysicalVerticalOffset: Integer;
begin
  Result := PhysicalOffset;
end;

{ TdxScrollVerticallyByPhysicalOffsetCommandBase }

constructor TdxScrollVerticallyByPhysicalOffsetCommandBase.Create(const AControl: IdxRichEditControl);
begin
  inherited Create(AControl);
  FUpdateScrollBarBeforeExecution := True;
end;

function TdxScrollVerticallyByPhysicalOffsetCommandBase.CreateScrollDeltaCalculator(
  AScrollUp: Boolean): TdxScrollByPhysicalHeightCalculator;
begin
  if AScrollUp then
    Result := TdxScrollUpByPhysicalHeightCalculator.Create(View)
  else
    Result := TdxScrollDownByPhysicalHeightCalculator.Create(View);
end;

procedure TdxScrollVerticallyByPhysicalOffsetCommandBase.ExecuteCore;

  function Sign(Value: Int64): Integer;
  begin
    if Value = 0 then
      Result := 0
    else
      if Value < 0 then
        Result := -1
      else
        Result := 1;
  end;

var
  AOffset: Integer;
  ACalculator: TdxScrollByPhysicalHeightCalculator;
  ADelta, APreviousValue: Int64;
begin
  View.CheckExecutedAtUIThread;
  if UpdateScrollBarBeforeExecution then
    View.VerticalScrollController.UpdateScrollBar;
  AOffset := AbsolutePhysicalVerticalOffset;
  ACalculator := CreateScrollDeltaCalculator(AOffset < 0);
  try
    ADelta := ACalculator.CalculateScrollDelta(Abs(AOffset));
    if ADelta = 0 then
      Exit;
    APreviousValue := View.VerticalScrollController.ScrollBarAdapter.Value;
    GeneratePagesToEnsureScrollingSuccessfull(ADelta * Sign(AOffset));
    View.VerticalScrollController.ScrollByTopInvisibleHeightDelta(ADelta * Sign(AOffset));
    View.OnVerticalScroll;
    FExecutedSuccessfully := APreviousValue <> View.VerticalScrollController.ScrollBarAdapter.Value;
  finally
    ACalculator.Free;
  end;
end;

procedure TdxScrollVerticallyByPhysicalOffsetCommandBase.GeneratePagesToEnsureScrollingSuccessfull(ADelta: Integer);
begin
end;

procedure TdxScrollVerticallyByPhysicalOffsetCommandBase.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  AState.Enabled := True;
  AState.Visible := True;
end;

{ TdxScrollVerticallyByLogicalOffsetCommand }

constructor TdxScrollVerticallyByLogicalOffsetCommand.Create(const AControl: IdxRichEditControl);
begin
  inherited Create(AControl);
  FLogicalOffset := DocumentModel.UnitConverter.DocumentsToModelUnits(150);
end;

function TdxScrollVerticallyByLogicalOffsetCommand.GetAbsolutePhysicalVerticalOffset: Integer;
begin
  Result := Round(LogicalOffset * ActiveView.ZoomFactor); 
end;

{ TdxRichEditMenuItemSimpleCommand }

procedure TdxRichEditMenuItemSimpleCommand.ForceExecute(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  NotifyBeginCommandExecution(AState);
  try
    ExecuteCore;
  finally
    NotifyEndCommandExecution(AState);
  end;
end;

function TdxRichEditMenuItemSimpleCommand.GetForceVisible: Boolean;
begin
  Result := ((CommandSourceType = TdxCommandSourceType.Keyboard) or
    (CommandSourceType = TdxCommandSourceType.Menu) or
    (CommandSourceType = TdxCommandSourceType.Mouse)) and
    not DocumentModel.FormattingMarkVisibilityOptions.ShowHiddenText;
end;

{ TdxScrollHorizontallyByPhysicalOffsetCommand }

constructor TdxScrollHorizontallyByPhysicalOffsetCommand.Create(const AControl: IdxRichEditControl);
begin
  inherited Create(AControl);
  FPhysicalOffset := DocumentModel.LayoutUnitConverter.DocumentsToLayoutUnits(50);
end;

procedure TdxScrollHorizontallyByPhysicalOffsetCommand.ExecuteCore;
begin
  View.CheckExecutedAtUIThread;
  PerformScroll;
  View.OnHorizontalScroll;
end;

function TdxScrollHorizontallyByPhysicalOffsetCommand.GetActiveView: TdxRichEditView;
begin
  Result := InnerControl.ActiveView;
end;

procedure TdxScrollHorizontallyByPhysicalOffsetCommand.PerformScroll;
var
  APreviousValue: Int64;
begin
  APreviousValue := View.HorizontalScrollController.ScrollBarAdapter.Value;
  View.HorizontalScrollController.ScrollByLeftInvisibleWidthDelta(PhysicalOffset);
  FExecutedSuccessfully := APreviousValue <> View.HorizontalScrollController.ScrollBarAdapter.Value;
end;

procedure TdxScrollHorizontallyByPhysicalOffsetCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  AState.Enabled := True;
  AState.Visible := True;
end;

{ TdxSelectionValidator }

constructor TdxSelectionValidator.Create(APieceTable: TdxPieceTable);
begin
  inherited Create;
  FPieceTable := APieceTable;
end;

function TdxSelectionValidator.CalculateOffset(
  AParagraphIndex: TdxParagraphIndex; AIncrementIndex: Boolean): Integer;
var
  ACell: TdxTableCell;
begin
  Result := 0;
  while True do
  begin
    ACell := PieceTable.Paragraphs[AParagraphIndex].GetCell;
    if ACell = nil then
      Break;
    Assert(False, 'not implemented');
  end;
end;

procedure TdxSelectionValidator.ChangeFieldSelection(ASelection: TdxSelection;
  ASelectedInterval: TdxRunInfo; AField: TdxField);
begin
  if ShouldExtendInterval(ASelectedInterval, AField) then
    ChangeFieldSelectionCore(ASelection, AField);
end;

function TdxSelectionValidator.IsSelectedItemCovered(AItem: TdxSelectionItem): Boolean;
var
  I: TdxDocumentLogPosition;
  ACell: TdxTableCell;
begin
  Result := True;
  for I := AItem.NormalizedStart to AItem.NormalizedEnd - 1 do
  begin
    ACell := PieceTable.FindParagraph(I).GetCell;
    if ACell = nil then
    begin
      Result := False;
      Break;
    end
    else
    begin
       Assert(False, 'not implemented');
    end;
  end;
end;

function TdxSelectionValidator.ShouldChangeInterval(AInterval: TdxRunInfo;
  AStart, AEnd: TdxRunIndex): Boolean;
begin
  Result := ((AInterval.Start.RunIndex > AStart) and (AInterval.&End.RunIndex >= AEnd)) or
    ((AInterval.Start.RunIndex <= AStart) and (AInterval.&End.RunIndex < AEnd));
end;

function TdxSelectionValidator.ShouldExtendInterval(AInterval: TdxRunInfo;
  AField: TdxField): Boolean;
begin
  Result := Boolean(NotImplemented);
  Assert(False, 'need implemented TdxField');
end;

function TdxSelectionValidator.GetDocumentModel: TdxDocumentModel;
begin
  Result := PieceTable.DocumentModel;
end;

procedure TdxSelectionValidator.ValidateFieldSelectionCore(
  ASelection: TdxSelection);
var
  ASelectedInterval: TdxRunInfo;
  ALastField, AFirstField: TdxField;
begin
  ASelectedInterval := PieceTable.FindRunInfo(ASelection.NormalizedStart, ASelection.Length);
  try
    ALastField := PieceTable.FindFieldByRunIndex(ASelectedInterval.&End.RunIndex);
    if ALastField <> nil then
      ChangeFieldSelection(ASelection, ASelectedInterval, ALastField);
    AFirstField := PieceTable.FindFieldByRunIndex(ASelectedInterval.Start.RunIndex);
    if (AFirstField <> ALastField) and (AFirstField <> nil) then
      ChangeFieldSelection(ASelection, ASelectedInterval, AFirstField);
  finally
    ASelectedInterval.Free;
  end;
end;

procedure TdxSelectionValidator.ValidateSelection(ASelection: TdxSelection);
begin
  if ASelection.Length = 0 then
    ValidateTableActiveSelection(ASelection)
  else
  begin
    ValidateFieldSelectionCore(ASelection);
    ValidateTableSelectionCore(ASelection);
  end;
end;

procedure TdxSelectionValidator.ValidateTableActiveSelection(
  ASelection: TdxSelection);
var
  AActiveSelection: TdxSelectionItem;
  AParagraphIndex: TdxParagraphIndex;
  AOffset: Integer;
begin
  if ASelection.Items.Count <> 1 then
    Exit;
  AActiveSelection := ASelection.ActiveSelection;
  AParagraphIndex := PieceTable.FindParagraphIndex(AActiveSelection.Start);
  AOffset := CalculateOffset(AParagraphIndex, True);
  AActiveSelection.LeftOffset := AOffset;
  AActiveSelection.RightOffset := -AOffset;
end;

procedure TdxSelectionValidator.ValidateTableSelectionCore(
  ASelection: TdxSelection);
var
  ASelectionsCount: Integer;
  I: Integer;
  ASelectionItem: TdxSelectionItem;
  AParagraphIndex: TdxParagraphIndex;
  ALeftOffset, ARightOffset: Integer;
begin
  ASelectionsCount := ASelection.Items.Count;
  for I := 0 to ASelectionsCount - 1 do
  begin
    ASelectionItem := ASelection.Items[i];
    ASelectionItem.IsCovered := False;
    if (ASelectionsCount > 1) and IsSelectedItemCovered(ASelectionItem) then
    begin
      ASelectionItem.IsCovered := True;
      Continue;
    end;
    AParagraphIndex := PieceTable.FindParagraphIndex(ASelectionItem.NormalizedStart);
    ALeftOffset := CalculateOffset(AParagraphIndex, True);
    AParagraphIndex := PieceTable.FindParagraphIndex(ASelectionItem.NormalizedEnd);
    ARightOffset := CalculateOffset(AParagraphIndex, False);
    ASelectionItem.LeftOffset := ALeftOffset;
    ASelectionItem.RightOffset := ARightOffset;
  end;
end;

{ TdxFieldIsSelectValidator }

procedure TdxFieldIsSelectValidator.ChangeFieldSelectionCore(
  ASelection: TdxSelection; AField: TdxField);
begin
  Assert(False, 'not implemented need TdxField');
end;

{ TdxFieldIsUnselectValidator }

procedure TdxFieldIsUnselectValidator.ChangeFieldSelectionCore(
  ASelection: TdxSelection; AField: TdxField);
begin
Assert(False, 'not implemented need TdxField');
  if ASelection.&End > ASelection.Start then
  begin
  end
  else
    if ASelection.&End < ASelection.Start then
    begin
    end;
end;

{ TdxEnsureCaretVisibleHorizontallyCommand }

function TdxEnsureCaretVisibleHorizontallyCommand.CalculateOptimalCaretHorizontalPosition(
  const ALogicalVisibleBounds, ACaretBounds: TRect): Integer;
var
  AOptimalLogicalVisibleBounds: TRect;
  AColumnBounds: TRect;
  APageBounds: TRect;
begin
  AOptimalLogicalVisibleBounds := ALogicalVisibleBounds;
  AOptimalLogicalVisibleBounds.MoveToLeft(ACaretBounds.Left - 2 * ALogicalVisibleBounds.Width div 3);
  AColumnBounds := CaretPosition.LayoutPosition.Column.Bounds;
  if AOptimalLogicalVisibleBounds.Left < AColumnBounds.Left then
    AOptimalLogicalVisibleBounds.MoveToLeft(AColumnBounds.Left);
  APageBounds := CaretPosition.LayoutPosition.Page.Bounds;
  if AOptimalLogicalVisibleBounds.Right > APageBounds.Right then
    AOptimalLogicalVisibleBounds.MoveToRight(APageBounds.Right);
  Result := AOptimalLogicalVisibleBounds.Left;
end;

procedure TdxEnsureCaretVisibleHorizontallyCommand.ExecuteCore;
var
  ALogicalVisibleBounds: TRect;
  ACaretBounds: TRect;
begin
  CheckExecutedAtUIThread;
  UpdateCaretPosition(TdxDocumentLayoutDetailsLevel.Character);
  Assert(CaretPosition.PageViewInfo <> nil);
  if CaretPosition.PageViewInfo = nil then
    Exit;
  ALogicalVisibleBounds := ActiveView.CreateLogicalRectangle(CaretPosition.PageViewInfo, ActiveView.Bounds);
  if CaretPosition.LayoutPosition.DetailsLevel >= TdxDocumentLayoutDetailsLevel.Character then
  begin
    ACaretBounds := CaretPosition.CalculateCaretBounds;
    if not IsCaretVisible(ALogicalVisibleBounds, ACaretBounds) then
      ScrollToMakeCaretVisible(ALogicalVisibleBounds, ACaretBounds);
  end;
end;

function TdxEnsureCaretVisibleHorizontallyCommand.IsCaretVisible(
  const ALogicalVisibleBounds, ACaretBounds: TRect): Boolean;
begin
  Assert(CaretPosition.PageViewInfo <> nil);
  Result := ALogicalVisibleBounds.Contains(ACaretBounds);
end;

procedure TdxEnsureCaretVisibleHorizontallyCommand.ScrollToMakeCaretVisible(
  const ALogicalVisibleBounds, ACaretBounds: TRect);
var
  AOptimalHorizontalPosition: Integer;
  AScrollBarAdapter: TdxScrollBarAdapter;
begin
  AOptimalHorizontalPosition := CalculateOptimalCaretHorizontalPosition(ALogicalVisibleBounds, ACaretBounds);
  AScrollBarAdapter := ActiveView.HorizontalScrollController.ScrollBarAdapter;
  if AScrollBarAdapter.Value <> AOptimalHorizontalPosition then
  begin
    AScrollBarAdapter.Value := AOptimalHorizontalPosition;
    AScrollBarAdapter.ApplyValuesToScrollBar;
    ActiveView.PageViewInfoGenerator.LeftInvisibleWidth := ActiveView.HorizontalScrollController.GetLeftInvisibleWidth;
    ActiveView.OnHorizontalScroll;
  end;
end;

procedure TdxEnsureCaretVisibleHorizontallyCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  AState.Enabled := True;
  AState.Visible := True;
  AState.Checked := False;
end;

{ TdxEnsureCaretVisibleVerticallyCommand }

procedure TdxEnsureCaretVisibleVerticallyCommand.AdjustAndSaveCaretPosition;
var
  APos: TdxCaretPosition;
begin
  if ShouldAdjustCaretPosition then
  begin
    APos := CreateAdjustedCaretPosition;
    SaveCaretPosition(APos);
  end
  else
    SaveCaretPosition(CaretPosition);
end;

function TdxEnsureCaretVisibleVerticallyCommand.BothPositionsWillValid(
  ACurrentCaretPosition, AAdjustedCaretPosition: TdxCaretPosition): Boolean;
var
  APhysicalCaretRowBounds, APhysicalAdjustedCaretRowBounds, AViewBounds: TRect;
begin
  if AAdjustedCaretPosition.PageViewInfo = nil then
    Result := False
  else
  begin
    APhysicalCaretRowBounds := ActiveView.CreatePhysicalRectangle(ACurrentCaretPosition.PageViewInfo, ACurrentCaretPosition.LayoutPosition.Row.Bounds);
    APhysicalAdjustedCaretRowBounds := ActiveView.CreatePhysicalRectangle(AAdjustedCaretPosition.PageViewInfo, AAdjustedCaretPosition.LayoutPosition.Row.Bounds);
    AViewBounds := ActiveView.Bounds;
    Assert(APhysicalAdjustedCaretRowBounds.Top < AViewBounds.Top);
    Result := APhysicalCaretRowBounds.Top + AViewBounds.Top - APhysicalAdjustedCaretRowBounds.Top <= AViewBounds.Bottom;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculateActualInvisiblePageBounds(
  APageViewInfo: TdxPageViewInfo;
  ADirection: TdxSelectionMovementVerticalDirection): TRect;
var
  APageOffset: Integer;
begin
  if ADirection = TdxSelectionMovementVerticalDirection.Down then
    APageOffset := ActiveView.PageViewInfoGenerator.ActiveGenerator.PageRows.Last.Bounds.Bottom
  else
    APageOffset := ActiveView.PageViewInfoGenerator.ActiveGenerator.PageRows.First.Bounds.Top -
      2 * APageViewInfo.Bounds.Bottom + APageViewInfo.Bounds.Height;
  Result := ActiveView.PageViewInfoGenerator.OffsetRectangle(APageViewInfo.Bounds, 0, APageOffset);
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculateDirectionToInvisibleCaret: TdxSelectionMovementVerticalDirection;
var
  APages: TdxPageCollection;
  ACaretPageIndex: Integer;
  ACalculator: TdxNextCaretPositionLineUpCalculator;
  AFirstVisiblePageIndex: Integer;
begin
  APages := ActiveView.FormattingController.PageController.Pages;
  ACaretPageIndex := APages.IndexOf(CurrentPage);
  ACalculator := TdxNextCaretPositionLineUpCalculator.Create(RichEditControl);
  try
    ACalculator.FixedDirection := False;
    AFirstVisiblePageIndex := ACalculator.CalculateFirstInvisiblePageIndex + 1;
    Assert(ACaretPageIndex >= 0);
    Assert(AFirstVisiblePageIndex >= 0);
    Assert(ACaretPageIndex <> AFirstVisiblePageIndex);
    if ACaretPageIndex < AFirstVisiblePageIndex then
      Result := TdxSelectionMovementVerticalDirection.Up
    else
      Result := TdxSelectionMovementVerticalDirection.Down;
  finally
    ACalculator.Free;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculateOffsetToVisibleArea(
  AInitialCaretPhysicalTop: Integer): Integer;
var
  AViewBounds, APhysicalCaretRowBounds: TRect;
  ATargetY: Integer;
begin
  if RelativeCaretPosition < 0 then
    Result := 0
  else
  begin
    AViewBounds := ActiveView.Bounds;
    APhysicalCaretRowBounds := ActiveView.CreatePhysicalRectangle(Rect(0, 0, 100, 100), CurrentRow.Bounds);
    AViewBounds.Height := AViewBounds.Height - APhysicalCaretRowBounds.Height;
    if AViewBounds.Height <= 0 then
      Result := 0
    else
    begin
      ATargetY := Round(AViewBounds.Height * RelativeCaretPosition);
			Result := AInitialCaretPhysicalTop - ATargetY;
    end;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculatePhysicalOffsetForCaretPositionRowVisibilityCore(
  APageViewInfo: TdxPageViewInfo; ARow: TdxRow): Integer;
var
  ALogicalRowBounds, APhysicalRowBounds: TRect;
begin
  ALogicalRowBounds := CalculateTargetRowLogicalBounds(APageViewInfo, ARow);
  APhysicalRowBounds := ActiveView.CreatePhysicalRectangle(APageViewInfo, ALogicalRowBounds);
  Result := CalculatePhysicalOffsetForRowBoundsVisibility(ActiveView.Bounds, APhysicalRowBounds);
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculatePhysicalOffsetForRowBoundsVisibility(
  const AViewBounds, APhysicalRowBounds: TRect): Integer;
begin
  if (AViewBounds.Top <= APhysicalRowBounds.Top) and (APhysicalRowBounds.Bottom <= AViewBounds.Bottom) then
    Result := 0
  else
  begin
    if APhysicalRowBounds.Top < AViewBounds.Top then
      Result := APhysicalRowBounds.Top - AViewBounds.Top
    else
      Result := APhysicalRowBounds.Bottom - AViewBounds.Bottom;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculatePhysicalOffsetToCaret: Integer;
begin
  if CurrentPageViewInfo <> nil then
    Result := CalculatePhysicalOffsetToCaretAtVisiblePage
  else
    Result := CalculatePhysicalOffsetToCaretAtInvisiblePage;
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculatePhysicalOffsetToCaretAtInvisiblePage: Integer;
var
  APageViewInfo: TdxPageViewInfo;
  ADirection: TdxSelectionMovementVerticalDirection;
begin
  ADirection := CalculateDirectionToInvisibleCaret;
  APageViewInfo := LookupInvisiblePageWithCaret(ADirection);
  APageViewInfo.Bounds := CalculateActualInvisiblePageBounds(APageViewInfo, ADirection);
  ActiveView.PageViewInfoGenerator.UpdatePageClientBounds(APageViewInfo);
  Result := CalculatePhysicalOffsetForCaretPositionRowVisibilityCore(APageViewInfo, CurrentRow);
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculatePhysicalOffsetToCaretAtVisiblePage: Integer;
begin
  Result := CalculatePhysicalOffsetForCaretPositionRowVisibilityCore(CurrentPageViewInfo, CurrentRow);
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculateScrollDirectionToCaret(
  APos: TdxCaretPosition): TdxCaretScrollDirection;
var
  APhysicalCaretRowBounds, AViewBounds: TRect;

begin
  if APos.PageViewInfo = nil then
    Result := TdxCaretScrollDirection.Unknown
  else
  begin
    APhysicalCaretRowBounds := ActiveView.CreatePhysicalRectangle(APos.PageViewInfo,
      APos.LayoutPosition.Row.Bounds);
    AViewBounds := ActiveView.Bounds;
    if APhysicalCaretRowBounds.Top < AViewBounds.Top then
      Result := TdxCaretScrollDirection.Up
    else
			if APhysicalCaretRowBounds.Bottom > AViewBounds.Bottom then
        Result := TdxCaretScrollDirection.Down
      else
        Result := TdxCaretScrollDirection.None;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.CalculateTargetRowLogicalBounds(
  APageViewInfo: TdxPageViewInfo; ARow: TdxRow): TRect;
begin
  Result := ValidateRowBounds(APageViewInfo, ARow.Bounds);
end;

constructor TdxEnsureCaretVisibleVerticallyCommand.Create(
  const ARichEditControl: IdxRichEditControl);
begin
  inherited Create(ARichEditControl);
  FRelativeCaretPosition := -1;
end;

function TdxEnsureCaretVisibleVerticallyCommand.CreateAdjustedCaretPosition: TdxCaretPosition;
var
  ALogPosition: TdxDocumentLogPosition;
begin
  ALogPosition := ActivePieceTable.NavigationVisibleTextFilter.GetPrevVisibleLogPosition(CaretPosition.LogPosition, False);
  Result := CaretPosition.CreateExplicitCaretPosition(ALogPosition);
  Result.Update(TdxDocumentLayoutDetailsLevel.Row);
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.ExecuteCore;
begin
  CheckExecutedAtUIThread;
  UpdateCaretPosition(TdxDocumentLayoutDetailsLevel.Row);
  if CaretPosition.LayoutPosition.DetailsLevel >= TdxDocumentLayoutDetailsLevel.Row then
  begin
    if not IsCaretVisible then
      ScrollToMakeCaretVisible
    else
    begin
      if RelativeCaretPosition >= 0 then
        ScrollToSetRelativeCaretPosition;
    end;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.IsCaretVisible: Boolean;
var
  ACaretScrollDirection: TdxCaretScrollDirection;
  APos: TdxCaretPosition;
begin
  ACaretScrollDirection := CalculateScrollDirectionToCaret(CaretPosition);
  if (ACaretScrollDirection = TdxCaretScrollDirection.Down) or (ACaretScrollDirection = TdxCaretScrollDirection.Unknown) then
  begin
    SaveCaretPosition(CaretPosition);
    Exit(False);
  end;
  if ACaretScrollDirection = TdxCaretScrollDirection.Up then
  begin
    AdjustAndSaveCaretPosition;
    Exit(False);
  end;
  Assert(ACaretScrollDirection = TdxCaretScrollDirection.None);
  Assert(CaretPosition.PageViewInfo <> nil);
  if not ShouldAdjustCaretPosition then
  begin
    SaveCaretPosition(CaretPosition);
    Exit(True);
  end;
  APos := CreateAdjustedCaretPosition;
  try
    ACaretScrollDirection := CalculateScrollDirectionToCaret(APos);
  finally
    APos.Free;
  end;
  if ACaretScrollDirection = TdxCaretScrollDirection.None then
  begin
    SaveCaretPosition(CaretPosition);
    Exit(True);
  end
  else
  begin
    if (ACaretScrollDirection = TdxCaretScrollDirection.Up) or (ACaretScrollDirection = TdxCaretScrollDirection.Down) then
    begin
      if BothPositionsWillValid(CaretPosition, APos) then
        SaveCaretPosition(APos)
      else
        SaveCaretPosition(CaretPosition);
    end
    else
      SaveCaretPosition(CaretPosition);
      Result := False;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.LookupInvisiblePageWithCaret(
  ADirection: TdxSelectionMovementVerticalDirection): TdxPageViewInfo;
var
  ACalculator: TdxNextCaretPositionVerticalDirectionCalculator;
  AGenerator: TdxInvisiblePageRowsGenerator;
  ARow: TdxPageViewInfoRow;
  I: Integer;
  APageViewInfo: TdxPageViewInfo;
begin
  if ADirection = TdxSelectionMovementVerticalDirection.Up then
    ACalculator := TdxNextCaretPositionLineUpCalculator.Create(RichEditControl)
  else
    ACalculator := TdxNextCaretPositionLineDownCalculator.Create(RichEditControl);
  try
    ACalculator.FixedDirection := False;
    AGenerator := ACalculator.CreateInvisiblePageRowsGenerator;
    try
      while True do
      begin
        ARow := AGenerator.GenerateNextRow;
        Assert(ARow <> nil);
        for I := 0 to ARow.Count - 1 do
        begin
          APageViewInfo := ARow[I];
          if APageViewInfo.Page = CurrentPage then
            Exit(APageViewInfo);
        end;
      end;
    finally
      AGenerator.Free;
    end;
  finally
    ACalculator.Free;
  end;
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.SaveCaretPosition(
  APos: TdxCaretPosition);
begin
  FCurrentPage := APos.LayoutPosition.Page;
  FCurrentRow := APos.LayoutPosition.Row;
  FCurrentPageViewInfo := APos.PageViewInfo;
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.ScrollToAlreadyFormattedContentCore(
  AVerticalOffset: Integer);
var
  APhysicalCaretRowBounds: TRect;
  APageIndex, AOffset: Integer;
  APageViewInfoGenerator: TdxPageViewInfoGenerator;
begin
  ActiveView.ResetPages(TdxPageGenerationStrategyType.FirstPageOffset);
  APhysicalCaretRowBounds := ActiveView.CreatePhysicalRectangle(CurrentPageViewInfo, Rect(0, AVerticalOffset, 100, 100));
  APageIndex := ActiveView.FormattingController.PageController.Pages.IndexOf(CurrentPage);
  Assert(APageIndex >= 0);
  APageViewInfoGenerator := ActiveView.PageViewInfoGenerator;
  APageViewInfoGenerator.FirstPageOffsetAnchor.PageIndex := APageIndex;
  AOffset := CalculateOffsetToVisibleArea(APhysicalCaretRowBounds.Top);
  APageViewInfoGenerator.FirstPageOffsetAnchor.VerticalOffset := Max(0, APageViewInfoGenerator.FirstPageOffsetAnchor.VerticalOffset + AOffset);
  ActiveView.GeneratePages;
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.ScrollToMakeCaretVisible;
var
  APageViewInfoGenerator: TdxPageViewInfoGenerator;
  AOffset: Integer;
begin
  InnerControl.BeginDocumentRendering;
  try
    ActiveView.VerticalScrollController.UpdateScrollBar;
    APageViewInfoGenerator := ActiveView.PageViewInfoGenerator;
    if APageViewInfoGenerator.TopInvisibleHeight >= APageViewInfoGenerator.TotalHeight then
      ScrollToAlreadyFormattedContentCore(CurrentRow.Bounds.Top)
    else
    begin
      AOffset := CalculatePhysicalOffsetToCaret;
      if AOffset < 0 then
        AOffset := AOffset + CalculateOffsetToVisibleArea(0)
      else
        AOffset := AOffset + CalculateOffsetToVisibleArea(ActiveView.Bounds.Height - ActiveView.CreatePhysicalRectangle(Rect(0, 0, 100, 100), CurrentRow.Bounds).Height);
      ScrollVerticallyByPhysicalOffset(AOffset);
    end;
  finally
    InnerControl.EndDocumentRendering;
  end;
  UpdateCaretPosition(TdxDocumentLayoutDetailsLevel.Row);
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.ScrollToSetRelativeCaretPosition;
var
  APhysicalCaretRowBounds: TRect;
  AOffset: Integer;
begin
  if CurrentPageViewInfo = nil then
    Exit;
  APhysicalCaretRowBounds := ActiveView.CreatePhysicalRectangle(CurrentPageViewInfo, CurrentRow.Bounds);
  AOffset := CalculateOffsetToVisibleArea(APhysicalCaretRowBounds.Top);
  if AOffset = 0 then
    Exit;
  ActiveView.VerticalScrollController.UpdateScrollBar;
  ScrollVerticallyByPhysicalOffset(AOffset);
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.ScrollVerticallyByPhysicalOffset(
  AOffset: Integer);
var
  ACommand: TdxScrollVerticallyByPhysicalOffsetEnsurePageGenerationCommand;
begin
  if AOffset <> 0 then
  begin
    ACommand := TdxScrollVerticallyByPhysicalOffsetEnsurePageGenerationCommand.Create(RichEditControl);
    try
      ACommand.PhysicalOffset := AOffset;
      ACommand.Execute;
    finally
      ACommand.Free;
    end;
  end;
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.SetRelativeCaretPosition(
  Value: Double);
begin
  if FRelativeCaretPosition <> Value then
  begin
    if Value >= 0 then
      Value := Min(1, Value);
    FRelativeCaretPosition := Value;
  end;
end;

function TdxEnsureCaretVisibleVerticallyCommand.ShouldAdjustCaretPosition: Boolean;
begin
  if DocumentModel.Selection.&End <= DocumentModel.Selection.Start then
    Result := False
  else
    Result := CaretPosition.LogPosition = CaretPosition.LayoutPosition.Row.GetFirstPosition(ActivePieceTable).LogPosition;
end;

procedure TdxEnsureCaretVisibleVerticallyCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  AState.Enabled := True;
  AState.Visible := True;
  AState.Checked := False;
end;

function TdxEnsureCaretVisibleVerticallyCommand.ValidateRowBounds(
  APageViewInfo: TdxPageViewInfo; const ARowBounds: TRect): TRect;
var
  APageBottom: Integer;
begin
  Result := ARowBounds;
  APageBottom := ApageViewInfo.Page.Bounds.Bottom;
  if ARowBounds.Bottom > APageBottom then
    Result.Height := APageBottom - ARowBounds.Top;
end;

{ TdxNextCaretPositionVerticalDirectionCalculator }

procedure TdxNextCaretPositionVerticalDirectionCalculator.CorrectCaretXOnExtendSelection(
  ASourceCellViewInfo: TdxTableCellViewInfo; var ACaretX: Integer);
begin
  if ASourceCellViewInfo = nil then
    Exit;
  Assert(False);
end;

procedure TdxNextCaretPositionVerticalDirectionCalculator.CorrectRowAndCellViewInfo(
  APos: TdxCaretPosition; var ARowIndexInColumn: Integer;
  var ASourceCellViewInfo: TdxTableCellViewInfo);
var
  ADocumentModel: TdxDocumentModel;
begin
  ADocumentModel := APos.DocumentModel;
  if IsCaretAtTheBegginingOfAnotherCell(APos, ADocumentModel) then
  begin
    if ASourceCellViewInfo = nil then
      Exit;
    Assert(False);
  end;
end;

constructor TdxNextCaretPositionVerticalDirectionCalculator.Create(
  AControl: IdxRichEditControl);
begin
  inherited Create;
  FControl := AControl;
  FFixedDirection := True;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.CalculateNextPosition(
  ACaretPosition: TdxCaretPosition): TdxDocumentModelPosition;
var
  X: Integer;
  ATargetRow: TdxRow;
  ACharacter: TdxCharacterBox;
  ACenter: Integer;
begin
  View.CheckExecutedAtUIThread;
  X := ACaretPosition.X;
  Assert(ACaretPosition.PageViewInfo <> nil);
  ATargetRow := GetTargetLayoutRowObtainTargetPageViewInfo(ACaretPosition, X);
  if ATargetRow = nil then
    Exit(GetDefaultPosition);
  if ActivePieceTable <> ATargetRow.Paragraph.PieceTable then
    Exit(TdxDocumentModelPosition.Null);
  ACharacter := GetTargetCharacter(ATargetRow, X);
  try
    Result := ACharacter.GetFirstPosition(ActivePieceTable);
    if (ACharacter.Bounds.Right > X) and (ACharacter.Bounds.Left <= X) then
    begin
      ACenter := (ACharacter.Bounds.Right + ACharacter.Bounds.Left) div 2;
      if ACenter < X then
        Result.LogPosition := Result.LogPosition + 1;
    end;
  finally
    ACharacter.Free;
  end;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.CreateInvisiblePageRowsGenerator: TdxInvisiblePageRowsGenerator;
var
  ABounds: TRect;
begin
  Result := TdxInvisiblePageRowsGenerator.Create(View.FormattingController.PageController.Pages, View.PageViewInfoGenerator);
  Result.FirstPageIndex := CalculateFirstInvisiblePageIndex;
  if (Result.FirstPageIndex < 0) and not FixedDirection then
  begin
    Result.FirstPageIndex := 0;
    Result.FirstInvalidPageIndex := View.FormattingController.PageController.Pages.Count;
  end
  else
    Result.FirstInvalidPageIndex := CalculateFirstInvalidPageIndex;
  ABounds := Result.LayoutManager.ViewPortBounds;
  ABounds.MoveToTop(0);
  Result.LayoutManager.ViewPortBounds := ABounds;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GenerateTargetPageViewInfoRow: TdxPageViewInfoRow;
var
  AGenerator: TdxInvisiblePageRowsGenerator;
begin
  AGenerator := CreateInvisiblePageRowsGenerator;
  try
    Result := AGenerator.GenerateNextRow;
    if Result <> nil then
      CorrectGeneratedRowPagesHorizontalLocation(AGenerator.LayoutManager, Result);
  finally
    AGenerator.Free;
  end;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetActivePieceTable: TdxPieceTable;
begin
  Result := Control.InnerControl.DocumentModel.ActivePieceTable;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetDefaultPosition: TdxDocumentModelPosition;
begin
  Result := TdxDocumentModelPosition.Null;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetNextLayoutRowByDirection(
  ARows: TdxRowCollection; ASourceRowIndex: Integer): TdxRow;
var
  ANewIndex: Integer;
begin
  ANewIndex := GetNextLayoutRowIndexInDirection(ASourceRowIndex);
  Assert(ANewIndex < ARows.Count);
  Result := ARows[ANewIndex];
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetPageViewInfoRowIndex(
  APageRows: TdxPageViewInfoRowCollection;
  APageViewInfo: TdxPageViewInfo): Integer;
begin
  for Result := 0 to APageRows.Count - 1 do
  begin
    if APageRows[Result].IndexOf(APageViewInfo) >= 0 then
      Exit;
  end;
  Result := -1;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetTargetCharacter(
  ARow: TdxRow; ACaretX: Integer): TdxCharacterBox;
var
  ARequest: TdxRichEditHitTestRequest;
  AResult: TdxRichEditHitTestResult;
  AHitTestCalculator: TdxBoxHitTestCalculator;
  ABoxIndex: Integer;
  AComparable: TdxBoxAndPointXComparable;
  ADetailRow: TdxDetailRow;
begin
  ARequest := TdxRichEditHitTestRequest.Create(ActivePieceTable);
  try
    ARequest.LogicalPoint := Point(ACaretX, ARow.Bounds.Top + ARow.BaseLineOffset);
    AResult := TdxRichEditHitTestResult.Create(View.DocumentLayout, ActivePieceTable);
    try
      AHitTestCalculator := View.CreateHitTestCalculator(ARequest, AResult);
      try
        AComparable := TdxBoxAndPointXComparable.Create(ARequest.LogicalPoint);
        try
          ABoxIndex := AHitTestCalculator.FastHitTestIndexCore(ARow.Boxes, AComparable, False);
        finally
          AComparable.Free;
        end;
        Assert(ABoxIndex >= 0);
        ADetailRow := View.DocumentLayout.CreateDetailRowForBox(ARow, ARow.Boxes[ABoxIndex]);
        try
          AHitTestCalculator.FastHitTestCharacter(ADetailRow.Characters, False);
          Assert(AResult.IsValid(TdxDocumentLayoutDetailsLevel.Character));
          Result := AResult.Character.Clone;
        finally
          ADetailRow.Free;
        end;
      finally
        AHitTestCalculator.Free;
      end;
    finally
      AResult.Free;
    end;
  finally
    ARequest.Free;
  end;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetTargetColumn(
  APage: TdxPage; ACaretX: Integer): TdxColumn;
var
  ARequest: TdxRichEditHitTestRequest;
  AResult: TdxRichEditHitTestResult;
  APageArea: TdxPageArea;
  ACalculator: TdxBoxHitTestCalculator;
begin
  ARequest := TdxRichEditHitTestRequest.Create(ActivePieceTable);
  try
    ARequest.LogicalPoint := Point(ACaretX, 0);
    ARequest.DetailsLevel := TdxDocumentLayoutDetailsLevel.Column;
    ARequest.Accuracy := NearestColumn;
    APageArea := GetTargetPageArea(APage);
    AResult := TdxRichEditHitTestResult.Create(View.DocumentLayout, ActivePieceTable);
    try
      ACalculator := View.CreateHitTestCalculator(ARequest, AResult);
      try
        ACalculator.FastHitTestAssumingArrangedHorizontally(APageArea.Columns, False);
      finally
        ACalculator.Free;
      end;
      Assert(AResult.Column <> nil);
      Result := AResult.Column;
    finally
      AResult.Free;
    end;
  finally
    ARequest.Free;
  end;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetTargetLayoutRowObtainTargetPageViewInfo(
  APos: TdxCaretPosition; var ACaretX: Integer): TdxRow;
var
  ALayoutPosition: TdxDocumentLayoutPosition;
  ALayoutPositionRow: TdxRow;
  ARowIndexInColumn: Integer;
  ASourceCellViewInfo: TdxTableCellViewInfo;
begin
  ALayoutPosition := APos.LayoutPosition;
  ALayoutPositionRow := ALayoutPosition.Row;
  ARowIndexInColumn := ALayoutPosition.Column.Rows.IndexOf(ALayoutPositionRow);
  Assert(ARowIndexInColumn >= 0);
  ASourceCellViewInfo := ALayoutPosition.TableCell;
  CorrectRowAndCellViewInfo(APos, ARowIndexInColumn, ASourceCellViewInfo);
  CorrectCaretXOnExtendSelection(ASourceCellViewInfo, ACaretX);
  Result := GetTargetLayoutRowObtainTargetPageViewInfoCore(APos.PageViewInfo, ACaretX, ALayoutPosition.Column,
    ARowIndexInColumn, ASourceCellViewInfo);
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetTargetLayoutRowObtainTargetPageViewInfoCore(
  APageViewInfo: TdxPageViewInfo; var ACaretX: Integer; AColumn: TdxColumn;
  ARowIndexInColumn: Integer;
  ASourceCellViewInfo: TdxTableCellViewInfo): TdxRow;
begin
  if ShouldObtainTargetPageViewInfo(ARowIndexInColumn, AColumn.Rows.Count) or
      IsBoundaryTableCellRowOnCurrentPage(ASourceCellViewInfo, AColumn, ARowIndexInColumn, ACaretX) then
    Result := ObtainTargetRowFromCurrentPageViewInfoAndCaretX(APageViewInfo, ASourceCellViewInfo, ACaretX)
  else
    Result := ObtainTargetLayoutRow(ARowIndexInColumn, ACaretX, AColumn, ASourceCellViewInfo);
end;

function TdxNextCaretPositionVerticalDirectionCalculator.GetView: TdxRichEditView;
begin
  Result := Control.InnerControl.ActiveView;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.IsBoundaryTableCellRowOnCurrentPage(
  ACell: TdxTableCellViewInfo; AColumn: TdxColumn; ASourceRowIndexInColumn,
  ACaretX: Integer): Boolean;
var
  ASourceRowIsLastInCurrentCell: Boolean;
  ASourceRow, ALastRowInCurrentCell, ARow: TdxRow;
  ACurrentCell, AParentCell, ALastRowCellViewInfo: TdxTableCellViewInfo;
  ABoundaryRowInColumn, ALastRowInParentCell: TdxTableCellRow;
  AOuterTableViewInfo, ABoundaryOuterTableViewInfo: TdxTableViewInfo;
begin
  if not IsCellInBoundaryRow(AColumn, ACell, ACaretX) then
    Exit(False);
  ASourceRow := AColumn.Rows[ASourceRowIndexInColumn];
  if not IsBoundaryRowInCell(AColumn, ACell, ASourceRow) then
    Exit(False);
  if not (GetBoundaryRowInColumn(AColumn) is TdxTableCellRow) then
    Exit(False);
  ABoundaryRowInColumn := TdxTableCellRow(GetBoundaryRowInColumn(AColumn));

  ACurrentCell := ACell;
  while True do
  begin
    AParentCell := ACurrentCell.TableViewInfo.ParentTableCellViewInfo;
    if AParentCell = nil then
      Break;

    ALastRowInCurrentCell := ACurrentCell.GetRows(AColumn).Last;
    ASourceRowIsLastInCurrentCell := ALastRowInCurrentCell = ASourceRow;
    if not ASourceRowIsLastInCurrentCell and (GetCellBoundaryRowIndex(AColumn, AParentCell) <> ASourceRowIndexInColumn) then
      Exit(False);
    if not IsCellInBoundaryRow(AColumn, AParentCell, ACaretX) then
      Exit(False)
    else
      if ASourceRowIsLastInCurrentCell then
      begin
        ARow := AParentCell.GetRows(AColumn).Last;
        if ARow is TdxTableCellRow then
        begin
          ALastRowInParentCell := TdxTableCellRow(ARow);
          ALastRowCellViewInfo := ALastRowInParentCell.CellViewInfo;
          while (ALastRowCellViewInfo.TableViewInfo <> AParentCell.TableViewInfo) and (ALastRowCellViewInfo.TableViewInfo <> ACurrentCell.TableViewInfo) do
            ALastRowCellViewInfo := ALastRowCellViewInfo.TableViewInfo.ParentTableCellViewInfo;
          if ALastRowCellViewInfo.TableViewInfo = AParentCell.TableViewInfo then
            Exit(False);
        end;
      end;
    ACurrentCell := AParentCell;
  end;
  AOuterTableViewInfo := ACurrentCell.TableViewInfo;
  ABoundaryOuterTableViewInfo := ABoundaryRowInColumn.CellViewInfo.TableViewInfo;
  while ABoundaryOuterTableViewInfo.ParentTableCellViewInfo <> nil do
    ABoundaryOuterTableViewInfo := ABoundaryOuterTableViewInfo.ParentTableCellViewInfo.TableViewInfo;
  Result := AOuterTableViewInfo = ABoundaryOuterTableViewInfo;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.IsCaretAtTheBegginingOfAnotherCell(
  APos: TdxCaretPosition; ADocumentModel: TdxDocumentModel): Boolean;
var
  ASelection: TdxSelection;
begin
  ASelection := ADocumentModel.Selection;
  Result := (ASelection.Length > 0) and (ASelection.&End > ASelection.Start) and
    (APos.LayoutPosition.Row.GetFirstPosition(ADocumentModel.ActivePieceTable).LogPosition = APos.LogPosition);
end;

function TdxNextCaretPositionVerticalDirectionCalculator.ObtainTargetLayoutRow(
  ARowIndexInColumn, ACaretX: Integer; AColumn: TdxColumn;
  ASourceCell: TdxTableCellViewInfo): TdxRow;
var
  ANextCell: TdxTableCellViewInfo;
begin
  ANextCell := ASourceCell;
  if ASourceCell = nil then
  begin
    Result := GetNextLayoutRowByDirection(AColumn.Rows, ARowIndexInColumn);
    ANextCell := Result.GetCellViewInfo;
    if ANextCell <> nil then
    begin
      Assert(False);
    end;
  end
  else
    Result := ObtainTargetLayoutRowFromNextCellInCurrentTable(ARowIndexInColumn, ACaretX, AColumn, ANextCell);
end;

function TdxNextCaretPositionVerticalDirectionCalculator.ObtainTargetLayoutRowFromNextCellInCurrentTable(
  ARowIndexInColumn, ACaretX: Integer; AColumn: TdxColumn;
  ASourceCellViewInfo: TdxTableCellViewInfo): TdxRow;
begin
  Result := NotImplemented;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.ObtainTargetPageViewInfo(
  ACurrentPageViewInfo: TdxPageViewInfo;
  APhysicalCaretX: Integer): TdxPageViewInfo;
var
  APageRows: TdxPageViewInfoRowCollection;
  ARowIndex: Integer;
  APageRow: TdxPageViewInfoRow;
begin
  Assert(ACurrentPageViewInfo <> nil);
  APageRows := View.PageViewInfoGenerator.ActiveGenerator.PageRows;
  ARowIndex := GetPageViewInfoRowIndex(APageRows, ACurrentPageViewInfo);
  Assert(ARowIndex >= 0);
  if ShouldObtainTargetPageViewInfo(ARowIndex, APageRows.Count) then
    APageRow := GenerateTargetPageViewInfoRow
  else
    APageRow := ObtainExistingTargetPageViewInfoRow(APageRows, ARowIndex);
  if APageRow <> nil then
  begin
    Result := APageRow.GetPageAtPoint(Point(APhysicalCaretX, 0), False);
    Assert(Result <> nil);
  end
  else
    Result := nil;
end;

function TdxNextCaretPositionVerticalDirectionCalculator.ObtainTargetRowFromCurrentPageViewInfoAndCaretX(
  APageViewInfo: TdxPageViewInfo;
  ACellViewInfoFromPrevoiusTableViewInfo: TdxTableCellViewInfo;
  var ACaretX: Integer): TdxRow;
var
  APhysicalCaretPoint, ALogicalCaretPoint: TPoint;
  ATargetPageViewInfo: TdxPageViewInfo;
  AColumn: TdxColumn;
  AFirstColumnRow: TdxRow;
  AFirstColumnRowCell: TdxTableCellViewInfo;
begin
  APhysicalCaretPoint := View.CreatePhysicalPoint(APageViewInfo, Point(ACaretX, 0));
  ATargetPageViewInfo := ObtainTargetPageViewInfo(APageViewInfo, APhysicalCaretPoint.X);
  if ATargetPageViewInfo = nil then
    Exit(nil);
  ALogicalCaretPoint := View.CreateLogicalPoint(ATargetPageViewInfo.ClientBounds, Point(APhysicalCaretPoint.X, 0));
  ACaretX := ALogicalCaretPoint.X;
  AColumn := GetTargetColumn(ATargetPageViewInfo.Page, ALogicalCaretPoint.X);
  AFirstColumnRow := GetTargetLayoutRowCore(AColumn.Rows);
  AFirstColumnRowCell := AFirstColumnRow.GetCellViewInfo;
  if AFirstColumnRowCell = nil then
    Result := GetTargetLayoutRowCore(AColumn.Rows)
  else
  begin
    Result := NotImplemented;
  end;
end;

{ TdxNextCaretPositionLineUpCalculator }

function TdxNextCaretPositionLineUpCalculator.GetNextLayoutRowIndexInDirection(
  ASourceRowIndex: Integer): Integer;
begin
  Result := Max(ASourceRowIndex - 1, 0);
end;

function TdxNextCaretPositionLineUpCalculator.GetTargetLayoutRowInCell(
  ARows: TdxRowCollection): TdxRow;
begin
  Result := ARows.First;
end;

function TdxNextCaretPositionLineUpCalculator.ObtainExistingTargetPageViewInfoRow(
  APageRows: TdxPageViewInfoRowCollection;
  ACurrentRowIndex: Integer): TdxPageViewInfoRow;
begin
  Result := APageRows[ACurrentRowIndex - 1];
end;

function TdxNextCaretPositionLineUpCalculator.ShouldObtainTargetPageViewInfo(
  ASourceRowIndex, ASourceRowCount: Integer): Boolean;
begin
  Assert(ASourceRowIndex >= 0);
  Result := ASourceRowIndex = 0;
end;

{ TdxNextCaretPositionUpDirectionCalculator }

function TdxNextCaretPositionUpDirectionCalculator.CalculateFirstInvalidPageIndex: Integer;
begin
  Result := -1;
end;

function TdxNextCaretPositionUpDirectionCalculator.CalculateFirstInvisiblePageIndex: Integer;
begin
  Result := View.CalculateFirstInvisiblePageIndexBackward;
end;

procedure TdxNextCaretPositionUpDirectionCalculator.CorrectGeneratedRowPagesHorizontalLocation(
  ALayoutManager: TdxPageGeneratorLayoutManager;
  ARow: TdxPageViewInfoRow);
var
  X: Integer;
  I: Integer;
  APageViewInfo: TdxPageViewInfo;
  ABounds: TRect;
begin
  Assert(ARow.Count > 0);
  X := ARow[0].Bounds.Left;
  for I := ARow.Count - 1 downto 0 do
  begin
    APageViewInfo := ARow[I];
    ABounds := APageViewInfo.Bounds;
    ABounds.MoveToLeft(X);
    APageViewInfo.Bounds := ABounds;
    ALayoutManager.UpdatePageClientBounds(APageViewInfo);
    Inc(X, ABounds.Width);
  end;
end;

function TdxNextCaretPositionUpDirectionCalculator.GetBoundaryRowInColumn(AColumn: TdxColumn): TdxRow;
begin
  Result := AColumn.Rows.First;
end;

function TdxNextCaretPositionUpDirectionCalculator.GetCellBoundaryRowIndex(AColumn: TdxColumn;
  AParentCell: TdxTableCellViewInfo): Integer;
begin
  Result := AParentCell.GetFirstRowIndex(AColumn);
end;

function TdxNextCaretPositionUpDirectionCalculator.GetTargetLayoutRowCore(
  ARows: TdxRowCollection): TdxRow;
begin
  Result := ARows.Last;
end;

function TdxNextCaretPositionUpDirectionCalculator.GetTargetPageArea(
  APage: TdxPage): TdxPageArea;
begin
  Result := APage.Areas.Last;
end;

function TdxNextCaretPositionUpDirectionCalculator.IsBoundaryRowInCell(AColumn: TdxColumn; ACell: TdxTableCellViewInfo;
  ASourceRow: TdxRow): Boolean;
begin
  Result := ACell.GetRows(AColumn).First = ASourceRow;
end;

function TdxNextCaretPositionUpDirectionCalculator.IsCellInBoundaryRow(
  AColumn: TdxColumn; ACell: TdxTableCellViewInfo; ACaretX: Integer): Boolean;
begin
  Result := ACell <> nil;
  if Result then
    Result := ACell.TopAnchorIndex = 0;
end;

{ TdxNextCaretPositionLineDownCalculator }

function TdxNextCaretPositionLineDownCalculator.GetNextLayoutRowIndexInDirection(
  ASourceRowIndex: Integer): Integer;
begin
  Result := ASourceRowIndex + 1;
end;

function TdxNextCaretPositionLineDownCalculator.ObtainExistingTargetPageViewInfoRow(
  APageRows: TdxPageViewInfoRowCollection;
  ACurrentRowIndex: Integer): TdxPageViewInfoRow;
begin
  Result := APageRows[ACurrentRowIndex + 1];
end;

function TdxNextCaretPositionLineDownCalculator.ShouldObtainTargetPageViewInfo(
  ASourceRowIndex, ASourceRowCount: Integer): Boolean;
begin
  Assert(ASourceRowIndex >= 0);
  Assert(ASourceRowIndex + 1 <= ASourceRowCount);
  Result := ASourceRowIndex + 1 = ASourceRowCount;
end;

{ TdxNextCaretPositionDownDirectionCalculator }

function TdxNextCaretPositionDownDirectionCalculator.CalculateFirstInvalidPageIndex: Integer;
begin
  Result := View.FormattingController.PageController.Pages.Count;
end;

function TdxNextCaretPositionDownDirectionCalculator.CalculateFirstInvisiblePageIndex: Integer;
begin
  Result := View.CalculateFirstInvisiblePageIndexForward;
end;

procedure TdxNextCaretPositionDownDirectionCalculator.CorrectGeneratedRowPagesHorizontalLocation(
  ALayoutManager: TdxPageGeneratorLayoutManager; ARow: TdxPageViewInfoRow);
begin
end;

function TdxNextCaretPositionDownDirectionCalculator.GetBoundaryRowInColumn(AColumn: TdxColumn): TdxRow;
begin
  Result := AColumn.Rows.Last;
end;

function TdxNextCaretPositionDownDirectionCalculator.GetCellBoundaryRowIndex(AColumn: TdxColumn;
  AParentCell: TdxTableCellViewInfo): Integer;
begin
  Result := AParentCell.GetLastRowIndex(AColumn);
end;

function TdxNextCaretPositionDownDirectionCalculator.GetTargetLayoutRowCore(
  ARows: TdxRowCollection): TdxRow;
begin
 Result := ARows.First;
end;

function TdxNextCaretPositionDownDirectionCalculator.GetTargetLayoutRowInCell(
  ARows: TdxRowCollection): TdxRow;
begin
 Result := ARows.Last;
end;

function TdxNextCaretPositionDownDirectionCalculator.GetTargetPageArea(APage: TdxPage): TdxPageArea;
begin
  Result := APage.Areas.First;
end;

function TdxNextCaretPositionDownDirectionCalculator.IsBoundaryRowInCell(AColumn: TdxColumn;
  ACell: TdxTableCellViewInfo; ASourceRow: TdxRow): Boolean;
begin
  Result := ACell.GetRows(AColumn).Last = ASourceRow;
end;

function TdxNextCaretPositionDownDirectionCalculator.IsCellInBoundaryRow(
  AColumn: TdxColumn; ACell: TdxTableCellViewInfo; ACaretX: Integer): Boolean;
begin
  Result := (ACell <> nil) and (ACell.BottomAnchorIndex + 1 = ACell.TableViewInfo.Anchors.Count);
end;

{ TdxScrollVerticallyByPhysicalOffsetEnsurePageGenerationCommand }

function TdxScrollVerticallyByPhysicalOffsetEnsurePageGenerationCommand.CalculateNewTopInvisibleHeight(
  ADelta: Int64): Int64;
begin
  Result := View.PageViewInfoGenerator.TopInvisibleHeight + ADelta;
end;

procedure TdxScrollVerticallyByPhysicalOffsetEnsurePageGenerationCommand.GeneratePagesToEnsureScrollingSuccessfull(
  ADelta: Integer);
var
  ANewTopInvisibleHeight: Int64;
  APageViewInfoGenerator: TdxPageViewInfoGenerator;
begin
  ANewTopInvisibleHeight := CalculateNewTopInvisibleHeight(ADelta);
  APageViewInfoGenerator := View.PageViewInfoGenerator;
  if ANewTopInvisibleHeight <= APageViewInfoGenerator.TotalHeight - APageViewInfoGenerator.VisibleHeight + 1 then
    Exit;
  View.ResetPages(TdxPageGenerationStrategyType.RunningHeight);
  APageViewInfoGenerator.TopInvisibleHeight := ANewTopInvisibleHeight;
  View.GeneratePages;
  Assert(ANewTopInvisibleHeight < APageViewInfoGenerator.TotalHeight);
end;

{ TdxExtendKeybordSelectionHorizontalDirectionCalculator }

constructor TdxExtendKeybordSelectionHorizontalDirectionCalculator.Create(
  ADocumentModel: TdxDocumentModel);
begin
  inherited Create;
  FDocumentModel := ADocumentModel;
end;

function TdxExtendKeybordSelectionHorizontalDirectionCalculator.CalculateNextPosition(
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
begin
  Result := ALogPosition;
end;

function TdxExtendKeybordSelectionHorizontalDirectionCalculator.CalculatePrevPosition(
  ALogPosition: TdxDocumentLogPosition): TdxDocumentLogPosition;
begin
  Result := ALogPosition;
end;

{ TdxEnsureCaretVisibleVerticallyForMovePrevNextPageCommand }

function TdxEnsureCaretVisibleVerticallyForMovePrevNextPageCommand.CalculatePhysicalOffsetForRowBoundsVisibility(
  const AViewBounds, APhysicalRowBounds: TRect): Integer;
begin
  if (AViewBounds.Top <= APhysicalRowBounds.Top) and (APhysicalRowBounds.Bottom <= AViewBounds.Bottom) then
    Result := 0
  else
    Result := APhysicalRowBounds.Top - AViewBounds.Top;
end;

function TdxEnsureCaretVisibleVerticallyForMovePrevNextPageCommand.CalculateTargetRowLogicalBounds(
  APageViewInfo: TdxPageViewInfo; ARow: TdxRow): TRect;
var
  ALogicalRowBounds, ATopmostRowBounds: TRect;
begin
  ALogicalRowBounds := ValidateRowBounds(APageViewInfo, ARow.Bounds);
  ATopmostRowBounds := ValidateRowBounds(APageViewInfo, CalculateTopmostRow(APageViewInfo.Page).Bounds);
  ALogicalRowBounds.MoveToTop(ALogicalRowBounds.Top - Round(PhysicalOffsetAboveTargetRow * ActiveView.ZoomFactor));
  ALogicalRowBounds.MoveToTop(Max(0, ALogicalRowBounds.Top));
  if ATopmostRowBounds.Top < ALogicalRowBounds.Top then
    Result := ATopmostRowBounds
  else
    Result := ALogicalRowBounds;
end;

function TdxEnsureCaretVisibleVerticallyForMovePrevNextPageCommand.CalculateTopmostRow(
  APage: TdxPage): TdxRow;
var
  AColumns: TdxColumnCollection;
  I: Integer;
  ARow: TdxRow;
begin
  AColumns := APage.Areas.First.Columns;
  Result := AColumns[0].Rows.First;
  for I := 1 to AColumns.Count - 1 do
  begin
    ARow := AColumns[I].Rows.First;
    if ARow.Bounds.Top < Result.Bounds.Top then
      Result := ARow;
  end;
end;

{ TdxHistoryCommandBase }

procedure TdxHistoryCommandBase.ExecuteCore;
var
  ADocumentModel: TdxDocumentModel;
begin
  CheckExecutedAtUIThread;
  ADocumentModel := DocumentModel;
  ADocumentModel.BeginUpdate;
  try
    PerformHistoryOperation(ADocumentModel.History);
    InvalidateDocumentLayout;
  finally
    ADocumentModel.EndUpdate;
  end;
end;

procedure TdxHistoryCommandBase.InvalidateDocumentLayout;
begin
  DocumentModel.InvalidateDocumentLayout;
end;

procedure TdxHistoryCommandBase.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  AState.Enabled := IsContentEditable and CanPerformHistoryOperation(DocumentModel.History);
  AState.Visible := True;
end;

{ TdxUndoCommand }

function TdxUndoCommand.CanPerformHistoryOperation(
  AHistory: TdxDocumentHistory): Boolean;
begin
  Result := AHistory.CanUndo;
end;

class function TdxUndoCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.Undo;
end;

procedure TdxUndoCommand.PerformHistoryOperation(AHistory: TdxDocumentHistory);
begin
  AHistory.Undo;
end;

procedure TdxUndoCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  ApplyCommandRestrictionOnEditableControl(AState, DocumentModel.DocumentCapabilities.Undo, CanPerformHistoryOperation(DocumentModel.History));
end;

{ TdxRedoCommand }

function TdxRedoCommand.CanPerformHistoryOperation(
  AHistory: TdxDocumentHistory): Boolean;
begin
  Result := AHistory.CanRedo;
end;

class function TdxRedoCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.Redo;
end;

procedure TdxRedoCommand.PerformHistoryOperation(AHistory: TdxDocumentHistory);
begin
  AHistory.Redo;
end;

procedure TdxRedoCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  CheckExecutedAtUIThread;
  ApplyCommandRestrictionOnEditableControl(AState, DocumentModel.DocumentCapabilities.Undo, CanPerformHistoryOperation(DocumentModel.History));
end;

{ TdxToggleShowWhitespaceCommand }

procedure TdxToggleShowWhitespaceCommand.EnsureSelectionVisible;
var
  ASelectionInterval: TdxRunInfo;
  AStart, AEnd: TdxDocumentModelPosition;
  ATextFilter: IVisibleTextFilter;
  AStartLogPosition, AEndLogPosition: TdxDocumentLogPosition;
begin
  ASelectionInterval := DocumentModel.Selection.Interval;
  AStart := ASelectionInterval.Start;
  AEnd := ASelectionInterval.&End;
  AStartLogPosition := AStart.LogPosition;
  AEndLogPosition := AEnd.LogPosition;

  ATextFilter := ActivePieceTable.NavigationVisibleTextFilter;
  if not ATextFilter.IsRunVisible(AStart.RunIndex) then
    AStartLogPosition := ATextFilter.GetVisibleLogPosition(AStart);
  if not ATextFilter.IsRunVisible(AEnd.RunIndex) then
    AEndLogPosition := ATextFilter.GetVisibleLogPosition(AEnd);
  DocumentModel.Selection.Start := AStartLogPosition;
  DocumentModel.Selection.&End := AEndLogPosition;
end;

procedure TdxToggleShowWhitespaceCommand.ExecuteCore;
begin
    DocumentModel.BeginUpdate;
    try
      DocumentModel.FormattingMarkVisibilityOptions.ShowHiddenText := not DocumentModel.FormattingMarkVisibilityOptions.ShowHiddenText;
      EnsureSelectionVisible;
    finally
      DocumentModel.EndUpdate;
    end;
end;

class function TdxToggleShowWhitespaceCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ToggleShowWhitespace;
end;

procedure TdxToggleShowWhitespaceCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  AState.Enabled := True;
  AState.Visible := True;
  AState.Checked := DocumentModel.FormattingMarkVisibilityOptions.ShowHiddenText;
end;

{ TdxChangeIndentCommand }

function TdxChangeIndentCommand.GetEndIndex: TdxParagraphIndex;
begin
  Result := DocumentModel.Selection.Interval.NormalizedEnd.ParagraphIndex;
end;

function TdxChangeIndentCommand.GetEndParagraphIndex: TdxParagraphIndex;
var
  ASelection: TdxSelection;
begin
  ASelection := DocumentModel.Selection;
  if (ASelection.NormalizedEnd > 0) and (ASelection.NormalizedEnd <> ASelection.NormalizedStart) then
    Result := ActivePieceTable.FindParagraphIndex(ASelection.NormalizedEnd - 1, False)
  else
    Result := ActivePieceTable.FindParagraphIndex(ASelection.NormalizedEnd, False);
end;

function TdxChangeIndentCommand.GetStartIndex: TdxParagraphIndex;
begin
  Result := DocumentModel.Selection.Interval.NormalizedStart.ParagraphIndex;
end;

function TdxChangeIndentCommand.GetStartParagraphLayoutPosition: TdxDocumentLayoutPosition;
var
  AParagraph: TdxParagraph;
begin
  AParagraph := ActivePieceTable.Paragraphs[StartIndex];
  Result := TdxDocumentLayoutPosition(ActiveView.DocumentLayout.CreateLayoutPosition(ActivePieceTable,
    AParagraph.LogPosition, 0));
  Result.Update(ActiveView.DocumentLayout.Pages, TdxDocumentLayoutDetailsLevel.Row);
end;

function TdxChangeIndentCommand.SelectedFirstParagraphInList: Boolean;
var
  I: Integer;
  AParagraph: TdxParagraph;
  AListIndex: TdxNumberingListIndex;
  AParagraphs: TdxParagraphCollection;
begin
  AParagraphs := ActivePieceTable.Paragraphs;
  AListIndex := AParagraphs[StartIndex].GetNumberingListIndex;
  if StartIndex = 0 then
    Exit(True);
  for I := StartIndex - 1 downto 0 do
  begin
    AParagraph := AParagraphs[I];
    if AParagraph.IsInList and (AParagraph.GetNumberingListIndex = AListIndex) then
      Exit(False);
  end;
  Result := True;
end;

function TdxChangeIndentCommand.SelectedOnlyParagraphWithNumeration: Boolean;
var
  I: Integer;
  AEndIndex: TdxParagraphIndex;
  AParagraphs: TdxParagraphCollection;
begin
  AParagraphs := ActivePieceTable.Paragraphs;
  AEndIndex := GetEndParagraphIndex;
  for I := StartIndex to AEndIndex do
    if not AParagraphs[I].IsInList then
      Exit(False);
  Result := True;
end;

function TdxChangeIndentCommand.SelectionBeginFirstRowStartPos: Boolean;
var
  AParagraph: TdxParagraph;
begin
  if DocumentModel.Selection.Length = 0 then
  begin
    AParagraph := ActivePieceTable.Paragraphs[StartIndex];
    Result := DocumentModel.Selection.Start = AParagraph.LogPosition;
  end
  else
    Result := False;
end;

procedure TdxChangeIndentCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  ApplyCommandRestrictionOnEditableControl(AState, DocumentModel.DocumentCapabilities.ParagraphFormatting);
  ApplyDocumentProtectionToSelectedParagraphs(AState);
end;

{ TdxIncrementIndentCommand }

procedure TdxIncrementIndentCommand.ExecuteCore;
begin
  if SelectedOnlyParagraphWithNumeration then
    ProcessNumerationParagraph
  else
    IncrementParagraphIndent;
end;

class function TdxIncrementIndentCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.IncreaseIndent;
end;

procedure TdxIncrementIndentCommand.IncrementNumerationFromParagraph;
var
  ACommand: TdxIncrementNumerationFromParagraphCommand;
begin
  ACommand := TdxIncrementNumerationFromParagraphCommand.Create(RichEditControl);
  try
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

procedure TdxIncrementIndentCommand.IncrementNumerationParagraphIndent;
var
  ACommand: TdxIncrementNumerationParagraphIndentCommand;
begin
  ACommand := TdxIncrementNumerationParagraphIndentCommand.Create(RichEditControl);
  try
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

procedure TdxIncrementIndentCommand.IncrementParagraphIndent;
var
  ACommand: TdxIncrementParagraphLeftIndentCommand;
begin
  ACommand := TdxIncrementParagraphLeftIndentCommand.Create(RichEditControl);
  try
    ACommand.Execute;
  finally
    ACommand.Free;
  end;
end;

procedure TdxIncrementIndentCommand.ProcessNumerationParagraph;
begin
  if not SelectedFirstParagraphInList then
    IncrementNumerationFromParagraph
  else
    IncrementNumerationParagraphIndent;
end;

{ TdxDecrementIndentCommand }

procedure TdxDecrementIndentCommand.DecrementNumerationFromParagraph;
var
  ACommand: TdxDecrementNumerationFromParagraphCommand;
begin
  ACommand := TdxDecrementNumerationFromParagraphCommand.Create(RichEditControl);
  try
    ACommand.ForceExecute(CreateDefaultCommandUIState);
  finally
    ACommand.Free;
  end;
end;

procedure TdxDecrementIndentCommand.DecrementNumerationParagraphIndent;
var
  ACommand: TdxDecrementNumerationParagraphIndentCommand;
begin
  ACommand := TdxDecrementNumerationParagraphIndentCommand.Create(RichEditControl);
  try
    ACommand.ForceExecute(CreateDefaultCommandUIState);
  finally
    ACommand.Free;
  end;
end;

procedure TdxDecrementIndentCommand.DecrementParagraphIndent;
var
  ACommand: TdxDecrementParagraphLeftIndentCommand;
begin
  ACommand := TdxDecrementParagraphLeftIndentCommand.Create(RichEditControl);
  try
    ACommand.ForceExecute(CreateDefaultCommandUIState);
  finally
    ACommand.Free;
  end;
end;

procedure TdxDecrementIndentCommand.ExecuteCore;
begin
  if SelectedOnlyParagraphWithNumeration then
    ProcessNumerationParagraph
  else
    DecrementParagraphIndent;
end;

class function TdxDecrementIndentCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.DecreaseIndent;
end;

procedure TdxDecrementIndentCommand.ProcessNumerationParagraph;
begin
  if not SelectedFirstParagraphInList then
    DecrementNumerationFromParagraph
  else
    DecrementNumerationParagraphIndent;
end;

end.
