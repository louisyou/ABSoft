{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.IndexBasedObject;

{$I cxVer.inc}

{$SCOPEDENUMS ON}
{.$DEFINE USESINGLETONFACTORY}

interface

uses
  SysUtils, Classes, Types, dxCore, dxCoreClasses, dxRichEdit.Utils.Types, dxRichEdit.Utils.BatchUpdateHelper,
  Generics.Collections, dxRichEdit.DocumentModel.UnitConverter, dxRichEdit.DocumentModel.History.IndexChangedHistoryItem, Generics.Defaults,
  dxRichEdit.DocumentModel.Core;

type

  TdxRunIndex = Integer;

  TdxObtainAffectedRangeEventArgs = class;

  TdxObtainAffectedRangeEventHandler = procedure(Sender: TObject; E: TdxObtainAffectedRangeEventArgs) of object; 

  { IdxObtainAffectedRangeListener }

  IdxObtainAffectedRangeListener = interface
  ['{25DEF6AE-E37F-482D-86F8-FCEDD1768410}']
    procedure NotifyObtainAffectedRange(AArgs: TdxObtainAffectedRangeEventArgs);
  end;

  { TdxObtainAffectedRangeEventArgs }

  TdxObtainAffectedRangeEventArgs = class 
  private
    FEnd: TdxRunIndex; 
    FStart: TdxRunIndex; 
  public
    constructor Create;

    property Start: TdxRunIndex read FStart write FStart; 
    property &End: TdxRunIndex read FEnd write FEnd; 
  end;

  { TdxItemsListComparer }

  TdxItemsListComparer<T: class> = class(TComparer<T>)
  public
    function Compare(const Left, Right: T): Integer; override;
  end;

  { TdxItemsDictionaryComparer }

  TdxItemsDictionaryComparer<T: class> = class(TEqualityComparer<T>)
  public
    function Equals(const Left, Right: T): Boolean; override;
    function GetHashCode(const Value: T): Integer; override;
  end;

  { TdxUniqueItemsCache }

  TdxUniqueItemsCache<T: class, IdxCloneable<T>, IdxSupportsSizeOf> = class(TcxIUnknownObject, IdxSupportsSizeOf)
  private
    FItems: TdxFastObjectList;
    FDocumentModel: TdxCustomDocumentModel;
    function GetCount: Integer; inline;
    function GetItem(Index: Integer): T; inline;
    function GetDefaultItem: T;
  protected
    procedure InitItems(const AUnitConverter: IdxDocumentModelUnitConverter); virtual; 
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): T; virtual; abstract; 
    function AddItemCore(const AItem: T): Integer; virtual; 
    function LookupItem(const AItem: T): Integer; 
    function AppendItem(const AItem: T): Integer; 

    property Items: TdxFastObjectList read FItems;
  public
    constructor Create(const AUnitConverter: IdxDocumentModelUnitConverter; const ADocumentModel: TdxCustomDocumentModel = nil);
    destructor Destroy; override;
    function IsIndexValid(AIndex: Integer): Boolean; 
    function GetItemIndex(const AItem: T): Integer; 
    function AddItem(const AItem: T): Integer; 
    property Count: Integer read GetCount;
    property DefaultItem: T read GetDefaultItem; 
    property Items[Index: Integer]: T read GetItem; default; 
  end;

  { TdxIndexBasedObject }

  TdxIndexBasedObjectPropertyValueSetter<TInfo: class; TValue> = procedure (const AInfo: TInfo; const ANewValue: TValue) of object;
  TdxIndexBasedObjectSetOptionsValueDelegate<TOptions: class> = procedure (const AOptions: TOptions; const ANewValue: Boolean) of object;

  TdxIndexBasedObject<
    TInfo: class, IdxCloneable<TInfo>, IdxSupportsCopyFrom<TInfo>, IdxSupportsSizeOf;
    TOptions: class, IdxCloneable<TOptions>, IdxSupportsCopyFrom<TOptions>, IdxSupportsSizeOf> =
    class(TcxIUnknownObject, IdxBatchUpdateable, IdxBatchUpdateHandler, IdxSupportsSizeOf)
  private
    FInfoIndex: Integer;
    FOptionsIndex: Integer;
    FDocumentModel: TdxCustomDocumentModel;
    FDocumentModelInstance: TObject;
    FBatchUpdateHelper: TdxBatchUpdateHelper<TInfo, TOptions>;
    function GetInfo: TInfo; inline;
    function GetOptions: TOptions; inline;
    function GetInfoCore: TInfo; inline;
    function GetOptionsCore: TOptions; inline;
  protected
    procedure CopyFromCore(const ANewInfo: TInfo; const ANewOptions: TOptions);
    procedure ReplaceInfoCore(ANewInfoIndex, ANewOptionsIndex: Integer);

    function GetInfoIndex(const AValue: TInfo): Integer;
    function GetOptionsIndex(const AValue: TOptions): Integer;

    function GetInfoForModification(out AIsDeferred: Boolean): TInfo;
    function GetOptionsForModification(out AIsDeferred: Boolean): TOptions;

    procedure SetPropertyValue<TValue>(ASetter: TdxIndexBasedObjectPropertyValueSetter<TInfo, TValue>;
      const ANewValue: TValue; AOptionsSetter: TdxIndexBasedObjectSetOptionsValueDelegate<TOptions>);

    //IdxBatchUpdateable
    function GetBatchUpdateHelper: TdxBatchUpdateHelper; inline;

    // IdxBatchUpdateHandler
    procedure OnFirstBeginUpdate;
    procedure OnBeginUpdate;
    procedure OnEndUpdate;
    procedure OnLastEndUpdate;
    procedure OnCancelUpdate;
    procedure OnLastCancelUpdate;

    function GetInfoCache: TdxUniqueItemsCache<TInfo>; virtual; abstract;
    function GetIsUpdateLocked: Boolean; inline;
    function GetOptionsCache: TdxUniqueItemsCache<TOptions>; virtual; abstract;

    function PropertyEquals(const AOther: TdxIndexBasedObject<TInfo, TOptions>): Boolean; virtual; abstract;
  public
    constructor Create(const ADocumentModel: TdxCustomDocumentModel; AFormattingInfoIndex, AFormattingOptionsIndex: Integer);
    destructor Destroy; override;

    procedure ReplaceInfo(const ANewInfo: TInfo; const ANewOptions: TOptions);

    procedure BeginUpdate;
    procedure EndUpdate;
    procedure CancelUpdate;
    procedure CopyFrom(const Source: TdxIndexBasedObject<TInfo, TOptions>);
    function Equals(Obj: TObject): Boolean; override;
    function GetHashCode: Integer; override;

    property Info: TInfo read GetInfo;
    property Options: TOptions read GetOptions;
    property DocumentModel: TdxCustomDocumentModel read FDocumentModel;
    property DocumentModelInstance: TObject read FDocumentModelInstance;
    property IsUpdateLocked: Boolean read GetIsUpdateLocked;

    property InfoCore: TInfo read GetInfoCore;
    property OptionsCore: TOptions read GetOptionsCore;
    property InfoCache: TdxUniqueItemsCache<TInfo> read GetInfoCache;
    property OptionsCache: TdxUniqueItemsCache<TOptions> read GetOptionsCache;
    property InfoIndex: Integer read FInfoIndex write FInfoIndex;
    property OptionsIndex: Integer read FOptionsIndex write FOptionsIndex;
    property BatchUpdateHelper: TdxBatchUpdateHelper read GetBatchUpdateHelper;
  end;

  { TdxIndexBasedObjectB }

  TdxIndexBasedObjectB<
      TInfo: class, IdxCloneable<TInfo>, IdxSupportsCopyFrom<TInfo>, IdxSupportsSizeOf;
      TOptions: class, IdxCloneable<TOptions>, IdxSupportsCopyFrom<TOptions>, IdxSupportsSizeOf> =
    class(TdxIndexBasedObject<TInfo, TOptions>)
  private
    FPieceTable: TObject;
  public
    constructor Create(APieceTable: TObject; const ADocumentModel: TdxCustomDocumentModel;
      AFormattingInfoIndex, AFormattingOptionsIndex: Integer); virtual;

    property PieceTable: TObject read FPieceTable;
  end;


  { TdxUndoableIndexBasedObject }

  TdxUndoableIndexBasedObjectValueSetter<TInfo: class; TValue; TActions> = function (const AInfo: TInfo; const ANewValue: TValue): TActions of object;

  TdxUndoableIndexBasedObject<
      T: class, IdxCloneable<T>, IdxSupportsCopyFrom<T>, IdxSupportsSizeOf;
      TActions> =
    class(TcxIUnknownObject, IdxIndexBasedObject<TActions>, IdxBatchUpdateable, IdxBatchUpdateHandler, IdxBatchInit, IdxBatchInitHandler, IdxSupportsSizeOf)
  private
    FDocumentModelPart: TdxDocumentModelPart;
		FIndex: Integer;
    FBatchUpdateHelper: TdxBatchUpdateHelper<T>;
    function ChangeIndex(AIndex: Integer; const AChangeActions: TActions): Boolean; inline;
    function GetDeferredInfo: T; inline;
    function GetDocumentModel: TdxCustomDocumentModel; inline;
    function GetIsUpdateLocked: Boolean; inline;
    function GetInfo: T; inline;
    function GetInfoCore: T; inline;
    function GetIsDirectNotificationsEnabled: Boolean; inline;
  protected
    procedure ApplyChanges(const AChangeActions: TActions); virtual; abstract;
    procedure ChangeIndexCore(ANewIndex: Integer; const AChangeActions: TActions); virtual;
    function CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TActions>; virtual;
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<T>; virtual; abstract;
    function GetInfoIndex(const AValue: T): Integer;
    function GetInfoForModification(out AIsDeferred: Boolean): T;
    procedure NotifyFakeAssign;

    procedure SetPropertyValue<TValue>(ASetter: TdxUndoableIndexBasedObjectValueSetter<T, TValue, TActions>; const ANewValue: TValue);

    procedure OnFirstBeginUpdateCore; virtual;
    procedure OnIndexChanging; virtual;
    procedure OnIndexChanged; virtual;
    procedure OnLastEndUpdateCore; virtual;
    procedure OnLastCancelUpdateCore; virtual;

    procedure OnBeginAssign; virtual;
    procedure OnEndAssign; virtual;

    //IdxIndexBasedObject
    function GetDocumentModelPart: TdxDocumentModelPart; inline;
    function GetIndex: Integer; inline;
    procedure SetIndex(AIndex: Integer; const AChangeActions: TActions);

    //IdxBatchUpdateable
    function GetBatchUpdateHelper: TdxBatchUpdateHelper; inline;

    //IdxBatchInitHandler
    procedure OnBeginInit;
    procedure OnEndInit;
    procedure OnFirstBeginInit;
    procedure OnLastEndInit;
    procedure OnCancelInit;
    procedure OnLastCancelInit;

    // IdxBatchUpdateHandler
    procedure OnFirstBeginUpdate;
    procedure OnBeginUpdate;
    procedure OnEndUpdate;
    procedure OnLastEndUpdate;
    procedure OnCancelUpdate;
    procedure OnLastCancelUpdate;

    function GetBatchUpdateChangeActions: TActions; virtual; abstract;

    property DeferredInfo: T read GetDeferredInfo;

    property IsDirectNotificationsEnabled: Boolean read GetIsDirectNotificationsEnabled; 
  public
    constructor Create(const ADocumentModelPart: TdxDocumentModelPart); virtual;
    destructor Destroy; override;

    procedure AssignCore(const Source: TdxUndoableIndexBasedObject<T, TActions>);
    procedure CopyFrom(const Source: TdxUndoableIndexBasedObject<T, TActions>); overload;
    procedure CopyFrom(const Source: T); overload;
    function ReplaceInfo(const ANewValue: T; const AChangeActions: TActions): Boolean;

    //IdxBatchUpdateable
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure CancelUpdate;

    //IdxBatchInit
    procedure BeginInit;
    procedure EndInit;
    procedure CancelInit;

    procedure SuppressDirectNotifications;
    procedure ResumeDirectNotifications;
    procedure SuppressIndexRecalculationOnEndInit;
    procedure ResumeIndexRecalculationOnEndInit;
    procedure SetIndexInitial(AValue: Integer);

    property DocumentModelPart: TdxDocumentModelPart read FDocumentModelPart;
    property DocumentModel: TdxCustomDocumentModel read GetDocumentModel;

    property BatchUpdateHelper: TdxBatchUpdateHelper read GetBatchUpdateHelper;
    property Index: Integer read FIndex;
    property Info: T read GetInfo;
    property InfoCore: T read GetInfoCore;
    property IsUpdateLocked: Boolean read GetIsUpdateLocked;
  end;

  { TdxRichEditIndexBasedObject }

  TdxRichEditIndexBasedObject<T: class, IdxCloneable<T>, IdxSupportsCopyFrom<T>, IdxSupportsSizeOf> =
    class(TdxUndoableIndexBasedObject<T, TdxDocumentModelChangeActions>)
  private
    FOnObtainAffectedRange: TdxMulticastMethod<TdxObtainAffectedRangeEventHandler>;
  protected
    procedure ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions); override;
    procedure RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs); virtual; 
    function GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener; virtual; 
  public
    property OnObtainAffectedRange: TdxMulticastMethod<TdxObtainAffectedRangeEventHandler> read FOnObtainAffectedRange; 
  end;

  { TdxSingletonFactory }

  TdxSingletonFactory = class
  type
    TItem = record
      &Class: TClass;
      ThreadID: THandle;
      Instance: TObject;
      constructor Create(AClass: TClass; AThreadID: THandle; AInstance: TObject);
      procedure Dispose;
    end;
  private
    FItems: TList<TItem>;
  public
    function GetObject<T: class, IdxCloneable<T>, IdxSupportsCopyFrom<T>>(APattern: T): T;
    constructor Create;
    destructor Destroy; override;
  end;

var
  SingletonFactory: TdxSingletonFactory;

implementation

uses
  RTLConsts, dxThreading, Windows;

{ TdxObtainAffectedRangeEventArgs }

constructor TdxObtainAffectedRangeEventArgs.Create;
begin
  inherited Create;
  FEnd := -1;
  FStart := -1;
end;

{ TdxEqualsComparer }

function TdxItemsListComparer<T>.Compare(const Left, Right: T): Integer;
begin
  if Left.Equals(Right) then
    Result := 0
  else
    Result := -1;
end;

{ TdxItemsDictionaryComparer }

function TdxItemsDictionaryComparer<T>.Equals(const Left, Right: T): Boolean;
begin
  Result := Left.Equals(Right);
end;

function TdxItemsDictionaryComparer<T>.GetHashCode(const Value: T): Integer;
begin
  Result := Value.GetHashCode;
end;

{ TdxUniqueItemsCache }

constructor TdxUniqueItemsCache<T>.Create(const AUnitConverter: IdxDocumentModelUnitConverter;
  const ADocumentModel: TdxCustomDocumentModel = nil);
begin
  Assert(AUnitConverter <> nil, 'AUnitConverter');
  inherited Create;
  FItems := TdxFastObjectList.Create(True, 256);
  FDocumentModel := ADocumentModel;
  InitItems(AUnitConverter);
end;

destructor TdxUniqueItemsCache<T>.Destroy;
begin
  FreeAndNil(FItems);
  inherited Destroy;
end;

function TdxUniqueItemsCache<T>.GetItem(Index: Integer): T;
begin
  Result := T(FItems[Index]);
end;


function TdxUniqueItemsCache<T>.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TdxUniqueItemsCache<T>.AddItem(const AItem: T): Integer;
begin
  Result := LookupItem(AItem);
  if Result < 0 then
    Result := AddItemCore(AItem);
end;

function TdxUniqueItemsCache<T>.AddItemCore(const AItem: T): Integer;
begin
  Result := -1;
  if AItem <> nil then
    Result := AppendItem(AItem.Clone);
end;

function TdxUniqueItemsCache<T>.AppendItem(const AItem: T): Integer;
begin
  Result := Count;
  FItems.Add(AItem);
end;

function TdxUniqueItemsCache<T>.GetDefaultItem: T;
begin
  Result := T(FItems.First);
end;

function TdxUniqueItemsCache<T>.GetItemIndex(const AItem: T): Integer;
begin
  Result := LookupItem(AItem);
  if Result < 0 then
    Result := AddItemCore(AItem);
end;

procedure TdxUniqueItemsCache<T>.InitItems(const AUnitConverter: IdxDocumentModelUnitConverter);
var
  ADefaultItem: T;
begin
  ADefaultItem := CreateDefaultItem(AUnitConverter);
  if ADefaultItem <> nil then
    AppendItem(ADefaultItem);
end;

function TdxUniqueItemsCache<T>.IsIndexValid(AIndex: Integer): Boolean;
begin
  Result := (AIndex >= 0) and (AIndex < FItems.Count);
end;

function TdxUniqueItemsCache<T>.LookupItem(const AItem: T): Integer;
var
  I: Integer;
begin
  begin
    for I := FItems.Count - 1 downto 0 do
      if AItem.Equals(FItems.List[I]) then
        Exit(I);
    Result := -1;
  end;
end;

{ TdxIndexBasedObject }

constructor TdxIndexBasedObject<TInfo, TOptions>.Create(const ADocumentModel: TdxCustomDocumentModel;
  AFormattingInfoIndex, AFormattingOptionsIndex: Integer);
begin
  inherited Create;
  Assert(ADocumentModel <> nil, 'ADocumentModel');
  FDocumentModel := ADocumentModel;
  FDocumentModelInstance := TObject(ADocumentModel);


  FInfoIndex := AFormattingInfoIndex;
  FOptionsIndex := AFormattingOptionsIndex;
end;

destructor TdxIndexBasedObject<TInfo, TOptions>.Destroy;
begin
  FreeAndNil(FBatchUpdateHelper);
  inherited Destroy;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetIsUpdateLocked: Boolean;
begin
  Result := (FBatchUpdateHelper <> nil) and FBatchUpdateHelper.IsUpdateLocked;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetInfo: TInfo;
begin
  if IsUpdateLocked then
    Result := FBatchUpdateHelper.DeferredInfoNotifications
  else
    Result := InfoCore;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetInfoCore: TInfo;
begin
  Result := InfoCache[InfoIndex];
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetOptions: TOptions;
begin
  if IsUpdateLocked then
    Result := FBatchUpdateHelper.DeferredOptionsNotifications
  else
    Result := OptionsCore;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetOptionsCore: TOptions;
begin
  Result := OptionsCache[OptionsIndex];
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.ReplaceInfo(const ANewInfo: TInfo; const ANewOptions: TOptions);
begin
  if not IsUpdateLocked then
    ReplaceInfoCore(GetInfoIndex(ANewInfo), GetOptionsIndex(ANewOptions));
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.BeginUpdate;
begin
  if FBatchUpdateHelper = nil then
    FBatchUpdateHelper := TdxBatchUpdateHelper<TInfo, TOptions>.Create(Self,
      TdxBatchUpdateHelper.TLifetimeStrategy.FreeAfterTransaction);
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.CancelUpdate;
begin
  FBatchUpdateHelper.CancelUpdate;
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.CopyFrom(const Source: TdxIndexBasedObject<TInfo, TOptions>);
begin
  CopyFromCore(Source.Info, Source.Options);
end;

function TdxIndexBasedObject<TInfo, TOptions>.Equals(Obj: TObject): Boolean;
var
  AOther: TdxIndexBasedObject<TInfo, TOptions>;
begin
  if Obj is TdxIndexBasedObject<TInfo, TOptions> then
  begin
    AOther := TdxIndexBasedObject<TInfo, TOptions>(Obj);
    if AOther.DocumentModel = DocumentModel then
      Result := (InfoIndex = AOther.InfoIndex) and (OptionsIndex = AOther.OptionsIndex)
    else
      Result := PropertyEquals(AOther);
  end
  else
    Result := False;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetHashCode: Integer;
begin
  Result := InfoIndex xor OptionsIndex;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetInfoIndex(const AValue: TInfo): Integer;
begin
  Result := InfoCache.GetItemIndex(AValue);
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetOptionsIndex(const AValue: TOptions): Integer;
begin
  Result := OptionsCache.GetItemIndex(AValue);
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.ReplaceInfoCore(ANewInfoIndex, ANewOptionsIndex: Integer);
begin
  FOptionsIndex := ANewOptionsIndex;
  FInfoIndex := ANewInfoIndex;
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetInfoForModification(out AIsDeferred: Boolean): TInfo;
begin
  AIsDeferred := IsUpdateLocked;
  if AIsDeferred then
    Result := FBatchUpdateHelper.DeferredInfoNotifications
  else
  {$IFDEF USESINGLETONFACTORY}
    Result := SingletonFactory.GetObject<TInfo>(Info);
  {$ELSE}
    Result := Info.Clone;
  {$ENDIF}
end;

function TdxIndexBasedObject<TInfo, TOptions>.GetOptionsForModification(out AIsDeferred: Boolean): TOptions;
begin
  AIsDeferred := IsUpdateLocked;
  if AIsDeferred then
    Result := FBatchUpdateHelper.DeferredOptionsNotifications
  else
  {$IFDEF USESINGLETONFACTORY}
    Result := SingletonFactory.GetObject<TOptions>(Options);
  {$ELSE}
    Result := Options.Clone;
  {$ENDIF}
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.SetPropertyValue<TValue>(ASetter: TdxIndexBasedObjectPropertyValueSetter<TInfo, TValue>;
  const ANewValue: TValue; AOptionsSetter: TdxIndexBasedObjectSetOptionsValueDelegate<TOptions>);
var
  AInfo: TInfo;
  AOptions: TOptions;
  AIsDeferredInfo, AIsDeferredOptions: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AOptions := GetOptionsForModification(AIsDeferredOptions);
  ASetter(AInfo, ANewValue);
  AOptionsSetter(AOptions, True);
  ReplaceInfo(AInfo, AOptions);
  if not AIsDeferredInfo then AInfo.Free;
  if not AIsDeferredOptions then AOptions.Free;
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.CopyFromCore(const ANewInfo: TInfo; const ANewOptions: TOptions);
var
  AInfo: TInfo;
  AOptions: TOptions;
  AIsDeferredInfo, AIsDeferredOptions: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AOptions := GetOptionsForModification(AIsDeferredOptions);
  AInfo.CopyFrom(ANewInfo);
  AOptions.CopyFrom(ANewOptions);
  ReplaceInfo(AInfo, AOptions);
  if not AIsDeferredInfo then AInfo.Free;
  if not AIsDeferredOptions then AOptions.Free;
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.OnFirstBeginUpdate;
begin
  FBatchUpdateHelper.DeferredInfoNotifications := InfoCore.Clone;
  FBatchUpdateHelper.DeferredOptionsNotifications := OptionsCore.Clone;
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.OnBeginUpdate;
begin
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.OnEndUpdate;
begin
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.OnLastEndUpdate;
begin
  ReplaceInfo(FBatchUpdateHelper.DeferredInfoNotifications, FBatchUpdateHelper.DeferredOptionsNotifications);
  FreeAndNil(FBatchUpdateHelper);
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.OnCancelUpdate;
begin
end;

procedure TdxIndexBasedObject<TInfo, TOptions>.OnLastCancelUpdate;
begin
  FreeAndNil(FBatchUpdateHelper);
end;

{ TdxIndexBasedObjectB }

constructor TdxIndexBasedObjectB<TInfo, TOptions>.Create(APieceTable: TObject;
  const ADocumentModel: TdxCustomDocumentModel; AFormattingInfoIndex, AFormattingOptionsIndex: Integer);
begin
  inherited Create(ADocumentModel, AFormattingInfoIndex, AFormattingOptionsIndex);
  FPieceTable := APieceTable;
end;

{ TdxUndoableIndexBasedObject }

constructor TdxUndoableIndexBasedObject<T, TActions>.Create(const ADocumentModelPart: TdxDocumentModelPart);
begin
  inherited Create;
  Assert(ADocumentModelPart <> nil);
  FDocumentModelPart := ADocumentModelPart;
end;

destructor TdxUndoableIndexBasedObject<T, TActions>.Destroy;
begin
  FreeAndNil(FBatchUpdateHelper);
  inherited Destroy;
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetIndex: Integer;
begin
  Result := FIndex;
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetBatchUpdateHelper: TdxBatchUpdateHelper;
begin
  Result := FBatchUpdateHelper;
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetDeferredInfo: T;
begin
  Result := FBatchUpdateHelper.DeferredNotifications;
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetDocumentModel: TdxCustomDocumentModel;
begin
  Result := FDocumentModelPart.DocumentModel;
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetIsUpdateLocked: Boolean;
begin
  Result := (FBatchUpdateHelper <> nil) and FBatchUpdateHelper.IsUpdateLocked;
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetInfoCore: T;
begin
  Result := GetCache(DocumentModel)[FIndex];
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetInfo: T;
begin
  if IsUpdateLocked then
    Result := FBatchUpdateHelper.DeferredNotifications
  else
    Result := InfoCore;
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetIsDirectNotificationsEnabled: Boolean;
begin
  Result := not IsUpdateLocked or FBatchUpdateHelper.IsDirectNotificationsEnabled;
end;

function TdxUndoableIndexBasedObject<T, TActions>.ChangeIndex(AIndex: Integer;
  const AChangeActions: TActions): Boolean;
begin
  Assert(AIndex < GetCache(DocumentModel).Count);
  Result := FIndex <> AIndex;
  if Result then
    ChangeIndexCore(AIndex, AChangeActions);
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.AssignCore(const Source: TdxUndoableIndexBasedObject<T, TActions>);
begin
  if Index <> Source.Index then
    SetIndex(Source.Index, GetBatchUpdateChangeActions);
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.CopyFrom(const Source: TdxUndoableIndexBasedObject<T, TActions>);
begin
  Assert(DocumentModel = Source.DocumentModel);
  if Index <> Source.Index then
    ChangeIndex(Source.Index, GetBatchUpdateChangeActions)
  else
    NotifyFakeAssign;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.CopyFrom(const Source: T);
var
  AInfo: T;
  AIsDeferredInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AInfo.CopyFrom(Source);
  if not ReplaceInfo(AInfo, GetBatchUpdateChangeActions) then
    NotifyFakeAssign;
  if not AIsDeferredInfo then AInfo.Free;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.BeginUpdate;
begin
  if FBatchUpdateHelper = nil then
    FBatchUpdateHelper := TdxBatchUpdateHelper<T>.Create(Self, TdxBatchUpdateHelper.TLifetimeStrategy.FreeAfterTransaction);
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.EndUpdate;
begin
  FBatchUpdateHelper.EndUpdate;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.CancelUpdate;
begin
  Assert(FBatchUpdateHelper is TdxBatchUpdateHelper<T>);
  FBatchUpdateHelper.CancelUpdate;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.SuppressDirectNotifications;
begin
  Assert(IsUpdateLocked);
  FBatchUpdateHelper.SuppressDirectNotifications;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.ResumeDirectNotifications;
begin
  Assert(IsUpdateLocked);
  FBatchUpdateHelper.ResumeDirectNotifications;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.SuppressIndexRecalculationOnEndInit;
begin
  Assert(IsUpdateLocked);
  FBatchUpdateHelper.SuppressIndexRecalculationOnEndInit;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.ResumeIndexRecalculationOnEndInit;
begin
  Assert(IsUpdateLocked);
  FBatchUpdateHelper.ResumeIndexRecalculationOnEndInit;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.SetIndexInitial(AValue: Integer);
begin
  FIndex := AValue;
  Assert(FIndex < GetCache(DocumentModel).Count);
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetInfoIndex(const AValue: T): Integer;
begin
  Result := GetCache(DocumentModel).GetItemIndex(AValue);
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetInfoForModification(out AIsDeferred: Boolean): T;
begin
  AIsDeferred := IsUpdateLocked;
  if AIsDeferred then
    Result := FBatchUpdateHelper.DeferredNotifications
  else
  {$IFDEF USESINGLETONFACTORY}
  begin
    Result := SingletonFactory.GetObject<T>(Info);
    AIsDeferred := True;
  end;
  {$ELSE}
    Result := Info.Clone;
  {$ENDIF}
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.ChangeIndexCore(ANewIndex: Integer;
  const AChangeActions: TActions);
var
  ADocumentModel: TdxCustomDocumentModel;
  AItem: TdxIndexChangedHistoryItemCore<TActions>;
begin
  ADocumentModel := DocumentModel;
  ADocumentModel.BeginUpdate;
  try
    OnBeginAssign;
    try
      AItem := CreateIndexChangedHistoryItem;
      AItem.OldIndex := Index;
      AItem.NewIndex := ANewIndex;
      AItem.ChangeActions := AChangeActions;
      DocumentModel.AddHistoryItem(AItem);
      AItem.Execute;
    finally
      OnEndAssign;
    end;
  finally
    ADocumentModel.EndUpdate;
  end;
end;

function TdxUndoableIndexBasedObject<T, TActions>.CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TActions>;
begin
  Result := TdxIndexChangedHistoryItem<TActions>.Create(DocumentModelPart, Self);
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.NotifyFakeAssign;
begin
  if IsUpdateLocked then
    FBatchUpdateHelper.FakeAssignDetected := True
  else
  begin
    DocumentModel.BeginUpdate;
    try
      OnBeginAssign;
      try
      finally
        OnEndAssign;
      end;
    finally
      DocumentModel.EndUpdate;
    end;
  end;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.SetPropertyValue<TValue>(ASetter: TdxUndoableIndexBasedObjectValueSetter<T, TValue, TActions>; const ANewValue: TValue);
var
  AInfo: T;
  AChangeActions: TActions;
  AIsDeferredInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AChangeActions := ASetter(AInfo, ANewValue);
  ReplaceInfo(AInfo, AChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnFirstBeginUpdateCore;
begin
  FBatchUpdateHelper.DeferredNotifications := InfoCore.Clone
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnIndexChanging;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnIndexChanged;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnLastEndUpdateCore;
begin
  Assert(FBatchUpdateHelper is TdxBatchUpdateHelper<T>);
  if not ReplaceInfo(FBatchUpdateHelper.DeferredNotifications, GetBatchUpdateChangeActions) then
    if FBatchUpdateHelper.FakeAssignDetected then
      NotifyFakeAssign;
  FreeAndNil(FBatchUpdateHelper);
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnLastCancelUpdateCore;
begin
  Assert(FBatchUpdateHelper is TdxBatchUpdateHelper<T>);
  FreeAndNil(FBatchUpdateHelper);
end;

function TdxUndoableIndexBasedObject<T, TActions>.ReplaceInfo(const ANewValue: T;
  const AChangeActions: TActions): Boolean;
var
  AIndex: Integer;
begin
  if IsUpdateLocked then
    Result := False
  else
  begin
    AIndex := GetInfoIndex(ANewValue);
    Result := ChangeIndex(AIndex, AChangeActions);
  end;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnBeginAssign;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnEndAssign;
begin
end;

function TdxUndoableIndexBasedObject<T, TActions>.GetDocumentModelPart: TdxDocumentModelPart;
begin
  Result := FDocumentModelPart;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.SetIndex(AIndex: Integer;
  const AChangeActions: TActions);
begin
  if FIndex <> AIndex then
  begin
    OnIndexChanging;
    FIndex := AIndex;
    Assert(FIndex < GetCache(DocumentModel).Count);
    ApplyChanges(AChangeActions);
    OnIndexChanged;
  end;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.BeginInit;
begin
  if FBatchUpdateHelper = nil then
    FBatchUpdateHelper := TdxBatchInitHelper<T>.Create(Self);
  Assert(FBatchUpdateHelper is TdxBatchInitHelper<T>);
  FBatchUpdateHelper.BeginUpdate;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.EndInit;
begin
  Assert(FBatchUpdateHelper is TdxBatchInitHelper<T>);
  FBatchUpdateHelper.EndUpdate;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.CancelInit;
begin
  Assert(FBatchUpdateHelper is TdxBatchInitHelper<T>);
  FBatchUpdateHelper.CancelUpdate;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnBeginInit;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnEndInit;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnFirstBeginInit;
begin
  FBatchUpdateHelper.DeferredNotifications := InfoCore.Clone
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnLastEndInit;
begin
  if FBatchUpdateHelper.IsIndexRecalculationOnEndInitEnabled then
    FIndex := GetInfoIndex(FBatchUpdateHelper.DeferredNotifications);
  Assert(FIndex < GetCache(DocumentModel).Count);
  FreeAndNil(FBatchUpdateHelper);
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnCancelInit;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnLastCancelInit;
begin
  if FBatchUpdateHelper.IsIndexRecalculationOnEndInitEnabled then
    FIndex := GetInfoIndex(FBatchUpdateHelper.DeferredNotifications);
  Assert(FIndex < GetCache(DocumentModel).Count);
  FreeAndNil(FBatchUpdateHelper);
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnFirstBeginUpdate;
begin
  OnFirstBeginUpdateCore;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnBeginUpdate;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnEndUpdate;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnLastEndUpdate;
begin
  OnLastEndUpdateCore;
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnCancelUpdate;
begin
end;

procedure TdxUndoableIndexBasedObject<T, TActions>.OnLastCancelUpdate;
begin
  OnLastCancelUpdateCore;
end;

{ TdxRichEditIndexBasedObject }

procedure TdxRichEditIndexBasedObject<T>.ApplyChanges(const AChangeActions: TdxDocumentModelChangeActions);
var
  AArgs: TdxObtainAffectedRangeEventArgs;
  AListener: IdxObtainAffectedRangeListener;
begin
  AArgs := TdxObtainAffectedRangeEventArgs.Create;
  try
    AListener := GetObtainAffectedRangeListener;
    if (AListener <> nil) and IsDirectNotificationsEnabled then
      AListener.NotifyObtainAffectedRange(AArgs)
    else
      RaiseObtainAffectedRange(AArgs);
    if AArgs.Start >= 0 then
      DocumentModelPart.ApplyChangesCore(AChangeActions, AArgs.Start, AArgs.&End);
  finally
    FreeAndNil(AArgs);
  end;
end;

procedure TdxRichEditIndexBasedObject<T>.RaiseObtainAffectedRange(const AArgs: TdxObtainAffectedRangeEventArgs);
begin
  FOnObtainAffectedRange.Invoke(Self, AArgs);
end;

function TdxRichEditIndexBasedObject<T>.GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener;
begin
  Result := nil;
end;

{ TdxSingletonFactory }

constructor TdxSingletonFactory.Create;
begin
  inherited Create;
  FItems := TList<TItem>.Create;
  FItems.Capacity := 32;
end;

destructor TdxSingletonFactory.Destroy;
var
  I: Integer;
begin
  for I := 0 to FItems.Count - 1 do
    FItems[I].Dispose;
  FreeAndNil(FItems);
  inherited Destroy;
end;

function TdxSingletonFactory.GetObject<T>(APattern: T): T;
var
  I: Integer;
  AItem: TItem;
  ACurrentThreadID: THandle;
begin
  TMonitor.Enter(FItems);
  try
    ACurrentThreadID := GetCurrentThreadId;
    for I := FItems.Count - 1 downto 0 do
    begin
      AItem := FItems[I];
      if (AItem.ThreadID = ACurrentThreadID) and (APattern.ClassType = AItem.&Class) then
      begin
        Result := T(AItem.Instance);
        Result.CopyFrom(APattern);
        Exit;
      end;
    end;
    Result := APattern.Clone;
    AItem := TItem.Create(APattern.ClassType, ACurrentThreadID, Result);
    FItems.Add(AItem);
  finally
    TMonitor.Exit(FItems);
  end;
end;

{ TdxSingletonFactory.TItem }

constructor TdxSingletonFactory.TItem.Create(AClass: TClass; AThreadID: THandle;
  AInstance: TObject);
begin
  &Class := AClass;
  ThreadID := AThreadID;
  Instance := AInstance;
end;

procedure TdxSingletonFactory.TItem.Dispose;
begin
  FreeAndNil(Instance);
end;

{$IFDEF USESINGLETONFACTORY}
initialization
  SingletonFactory := TdxSingletonFactory.Create;

finalization
  FreeAndNil(SingletonFactory);
{$ENDIF}

end.
