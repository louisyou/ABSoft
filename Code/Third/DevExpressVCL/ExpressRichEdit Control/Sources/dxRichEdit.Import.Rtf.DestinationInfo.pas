{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Import.Rtf.DestinationInfo;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, dxCoreClasses, dxRichEdit.Import.Rtf, dxRichEdit.DocumentModel.PieceTable;

type
  TdxInfoDestination = class(TdxRichEditRtfDestinationBase)
  private
    class procedure LegacyPasswordHashHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
    class procedure PasswordHashHandler(AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer; AHasParameter: Boolean); inline; static;
  protected
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    procedure PopulateKeywordTable(ATable: TdxKeywordTranslatorTable); override;
  public
  end;

  TdxHexContentDestination = class(TdxRichEditRtfDestinationBase)
  private
    FFirstPosition: Boolean;
    FValue: Integer;
  protected
    function GetControlCharHT: TdxControlCharTranslatorTable; override;
    function GetKeywordHT: TdxKeywordTranslatorTable; override;
    procedure ProcessCharCore(AChar: Char); override;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
  end;

  TdxHexByteArrayDestination = class(TdxHexContentDestination)
  private
    FValue: TBytes;
  protected
    procedure ProcessBinCharCore(AChar: Char); override;
    property Value: TBytes read FValue;
  public
    destructor Destroy; override;
  end;

  TdxHexStreamDestination = class(TdxHexContentDestination)
  private
    FValue: TStream;
  protected
    procedure ProcessBinCharCore(AChar: Char); override;
    property Value: TStream read FValue;
  public
    constructor Create(AImporter: TdxRichEditDocumentModelRtfImporter); override;
    destructor Destroy; override;
  end;

  TdxLegacyPasswordHashDestination = class(TdxHexStreamDestination)
  private
    procedure ReadPasswordHash;
  public
    procedure AfterPopRtfState; override;
  end;

  TdxPasswordHashDestination = class(TdxHexStreamDestination)
  private
    procedure ReadPasswordHash;
  public
    procedure AfterPopRtfState; override;
  end;

implementation

{ TdxInfoDestination }

function TdxInfoDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

procedure TdxInfoDestination.PopulateKeywordTable(ATable: TdxKeywordTranslatorTable);
begin
  ATable.Add('password', LegacyPasswordHashHandler);
  ATable.Add('passwordhash', PasswordHashHandler);
end;

class procedure TdxInfoDestination.LegacyPasswordHashHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Destination := TdxLegacyPasswordHashDestination.Create(AImporter);
end;

class procedure TdxInfoDestination.PasswordHashHandler(
  AImporter: TdxRichEditDocumentModelRtfImporter; AParameterValue: Integer;
  AHasParameter: Boolean);
begin
  AImporter.Destination := TdxPasswordHashDestination.Create(AImporter);
end;

{ TdxHexContentDestination }

constructor TdxHexContentDestination.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FFirstPosition := True;
end;

function TdxHexContentDestination.GetControlCharHT: TdxControlCharTranslatorTable;
begin
  Result := nil;
end;

function TdxHexContentDestination.GetKeywordHT: TdxKeywordTranslatorTable;
begin
  Result := nil;
end;

procedure TdxHexContentDestination.ProcessCharCore(AChar: Char);
var
  AHex: Integer;
begin
  if AChar <> ' ' then
  begin
    AHex := dxHexToInt(AChar);
    if FFirstPosition then
      FValue := AHex shl 4
    else
    begin
      FValue := FValue + AHex;
      ProcessBinChar(Char(FValue));
      FValue := 0
    end;
    FFirstPosition := not FFirstPosition;
  end;
end;

{ TdxHexByteArrayDestination }

destructor TdxHexByteArrayDestination.Destroy;
begin
  SetLength(FValue, 0);
  inherited Destroy;
end;

procedure TdxHexByteArrayDestination.ProcessBinCharCore(AChar: Char);
var
  ACount: Integer;
begin
  ACount := Length(FValue);
  SetLength(FValue, ACount + 1);
  FValue[ACount] := Ord(AChar);
end;

{ TdxHexStreamDestination }

constructor TdxHexStreamDestination.Create(AImporter: TdxRichEditDocumentModelRtfImporter);
begin
  inherited Create(AImporter);
  FValue := TMemoryStream.Create;
end;

destructor TdxHexStreamDestination.Destroy;
begin
  FreeAndNil(FValue);
  inherited;
end;

procedure TdxHexStreamDestination.ProcessBinCharCore(AChar: Char);
var
  AOrd: Byte;
begin
  AOrd := Ord(AChar);
  FValue.Write(AOrd, 1);
end;

{ TdxLegacyPasswordHashDestination }

procedure TdxLegacyPasswordHashDestination.AfterPopRtfState;
begin
  Value.Position := 0;
  ReadPasswordHash;
end;

procedure TdxLegacyPasswordHashDestination.ReadPasswordHash;
begin
end;

{ TdxPasswordHashDestination }

procedure TdxPasswordHashDestination.AfterPopRtfState;
begin
  Value.Position := 0;
  ReadPasswordHash;
end;

procedure TdxPasswordHashDestination.ReadPasswordHash;
begin
  Assert(False, 'not implemented');
end;

end.


