{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Commands.Tab;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  dxRichEdit.Commands, dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Commands.ChangeProperties,
  dxRichEdit.Commands.IDs, dxRichEdit.Commands.Keyboard, dxRichEdit.DocumentModel.Core;

type
  { TdxTabKeyCommand }

  TdxTabKeyCommand = class(TdxRichEditCaretBasedCommand)
  protected
    procedure ExecuteCore; override;
    procedure UpdateUIStateCore(const AState: IdxCommandUIState); override;

    procedure ChangeIndent; virtual;
    function CreateIncrementIndentByTheTabCommand: TdxIncrementIndentByTheTabCommand; virtual;
    function CreateInsertTabCommand: TdxInsertTabCommand; virtual;
    function GetNextCell(ACurrentCell: TdxTableCell): TdxTableCell; virtual;
    function IsSelectedOneParagraphWithTab: Boolean;
    function IsSelectionStartFromBeginRow: Boolean;
    function IsLastCellInDirection(ACell: TdxTableCell): Boolean; virtual;
    procedure ModifyCaretPositionCellTopAnchor; virtual;
    procedure ProcessSingleCell(ACell: TdxTableCell); virtual;
    function SelectedWholeRow: Boolean;
    function ShouldAppledTableRowAtBottom(ACurrentCell: TdxTableCell): Boolean; virtual;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

  { TdxShiftTabKeyCommand }

  TdxShiftTabKeyCommand = class(TdxTabKeyCommand)
  protected
    procedure ChangeIndent; override;

    function CreateDecrementIndentByTheTabCommand: TdxDecrementIndentByTheTabCommand; virtual;
    function GetNextCell(ACurrentCell: TdxTableCell): TdxTableCell; override;
    function IsLastCellInDirection(ACell: TdxTableCell): Boolean; override;
    procedure ModifyCaretPositionCellTopAnchor; override;
    procedure ProcessSingleCell(ACell: TdxTableCell); override;
    function ShouldAppledTableRowAtBottom(ACurrentCell: TdxTableCell): Boolean; override;
  public
    class function Id: TdxRichEditCommandId; override;
  end;

implementation

uses
  RTLConsts, dxCoreClasses, 
  dxRichEdit.View.Core;

{ TdxTabKeyCommand }

procedure TdxTabKeyCommand.ChangeIndent;
var
  AOldInputPosition: TdxInputPosition;
  AIncrementCommand: TdxIncrementIndentByTheTabCommand;
  AState: IdxCommandUIState;
  ANewInputPosition: TdxInputPosition;
begin
  AOldInputPosition := nil;
  if DocumentModel.Selection.Length = 0 then
    AOldInputPosition := CaretPosition.GetInputPosition.Clone;
  try
    AIncrementCommand := CreateIncrementIndentByTheTabCommand;
    try
      AState := CreateDefaultCommandUIState;
      AIncrementCommand.ForceExecute(AState);
      if AOldInputPosition <> nil then
      begin
        UpdateCaretPosition(TdxDocumentLayoutDetailsLevel.Character);
        ANewInputPosition := CaretPosition.GetInputPosition;
        ANewInputPosition.CopyFormattingFrom(AOldInputPosition);
      end;
    finally
      AIncrementCommand.Free;
    end;
  finally
    AOldInputPosition.Free;
  end;
end;

function TdxTabKeyCommand.CreateIncrementIndentByTheTabCommand: TdxIncrementIndentByTheTabCommand;
begin
  Result := TdxIncrementIndentByTheTabCommand.Create(RichEditControl);
end;

function TdxTabKeyCommand.CreateInsertTabCommand: TdxInsertTabCommand;
begin
  Result := TdxInsertTabCommand.Create(RichEditControl);
end;

procedure TdxTabKeyCommand.ExecuteCore;
var
  ASelection: TdxSelection;
  AInsertCommand: TdxInsertTabCommand;
  AState: IdxCommandUIState;
begin
  ASelection := DocumentModel.Selection;
  if ASelection.IsWholeSelectionInOneTable then
  begin
    Assert(False, 'not implemented');
    ASelection.ClearMultiSelection;
    ASelection.SetStartCell(ASelection.NormalizedStart);
  end
  else
  begin
    if IsSelectionStartFromBeginRow and SelectedWholeRow and
      not IsSelectedOneParagraphWithTab and DocumentModel.DocumentCapabilities.ParagraphFormattingAllowed then
        ChangeIndent
    else
    begin
      AInsertCommand := CreateInsertTabCommand;
      try
        AState := CreateDefaultCommandUIState;
        AInsertCommand.ForceExecute(AState);
      finally
        AInsertCommand.Free;
      end;
    end;
  end;
end;

function TdxTabKeyCommand.GetNextCell(ACurrentCell: TdxTableCell): TdxTableCell;
begin
  Result := ACurrentCell.Next;
end;

class function TdxTabKeyCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.TabKey;
end;

function TdxTabKeyCommand.IsLastCellInDirection(ACell: TdxTableCell): Boolean;
begin
  Result := ACell.IsLastCellInRow;
end;

function TdxTabKeyCommand.IsSelectedOneParagraphWithTab: Boolean;
var
  AStartParagraphIndex: TdxParagraphIndex;
  AEndParagraphIndex: TdxParagraphIndex;
  AStartParagraph: TdxParagraph;
begin
  AStartParagraphIndex := DocumentModel.Selection.Interval.Start.ParagraphIndex;
  AEndParagraphIndex := DocumentModel.Selection.Interval.&End.ParagraphIndex;
  AStartParagraph := ActivePieceTable.Paragraphs[AStartParagraphIndex];
  Result := (AStartParagraphIndex = AEndParagraphIndex) and
    (AStartParagraph.Tabs.Info.Count > 0) and not AStartParagraph.IsInList;
end;

function TdxTabKeyCommand.IsSelectionStartFromBeginRow: Boolean;
var
  ASelectionLayout: TdxSelectionLayout;
begin
  ASelectionLayout := ActiveView.SelectionLayout;
  Result := ASelectionLayout.IsSelectionStartFromBeginRow;
end;

procedure TdxTabKeyCommand.ModifyCaretPositionCellTopAnchor;
begin
  CaretPosition.TableCellTopAnchorIndex := CaretPosition.TableCellTopAnchorIndex + 1;
end;

procedure TdxTabKeyCommand.ProcessSingleCell(ACell: TdxTableCell);
begin
Assert(False, 'not implemented');
end;

function TdxTabKeyCommand.SelectedWholeRow: Boolean;
var
  ASelectionLayout: TdxSelectionLayout;
  AStartParagraph: TdxParagraph;
  AEqualsStartAndEndRow: Boolean;
  ACharacter: TdxCharacterBox;
  ASelectedUpToEndRow: Boolean;
begin
  ASelectionLayout := ActiveView.SelectionLayout;
  AStartParagraph := ActivePieceTable.Paragraphs[DocumentModel.Selection.Interval.Start.ParagraphIndex];
  if DocumentModel.Selection.Length = 0 then
    Result := (AStartParagraph.Length > 1) or AStartParagraph.IsInList
  else
  begin
    AEqualsStartAndEndRow := ASelectionLayout.StartLayoutPosition.Row.Equals(ASelectionLayout.EndLayoutPosition.Row);
    ACharacter := ASelectionLayout.EndLayoutPosition.Character;
    ASelectedUpToEndRow := (ACharacter <> nil) and
      ACharacter.EndPos.AreEqual(ASelectionLayout.EndLayoutPosition.Row.Boxes.Last.EndPos);
    Result := not AEqualsStartAndEndRow or ASelectedUpToEndRow;
  end;
end;

function TdxTabKeyCommand.ShouldAppledTableRowAtBottom(
  ACurrentCell: TdxTableCell): Boolean;
begin
  Result := ACurrentCell.IsLastCellInTable;
end;

procedure TdxTabKeyCommand.UpdateUIStateCore(const AState: IdxCommandUIState);
begin
  AState.Enabled := IsContentEditable;
  AState.Visible := True;
end;

{ TdxShiftTabKeyCommand }

procedure TdxShiftTabKeyCommand.ChangeIndent;
var
  ADecrementCommand: TdxDecrementIndentByTheTabCommand;
begin
  ADecrementCommand := CreateDecrementIndentByTheTabCommand;
  try
    ADecrementCommand.ForceExecute(CreateDefaultCommandUIState);
  finally
    ADecrementCommand.Free;
  end;
end;

function TdxShiftTabKeyCommand.CreateDecrementIndentByTheTabCommand: TdxDecrementIndentByTheTabCommand;
begin
  Result := TdxDecrementIndentByTheTabCommand.Create(RichEditControl);
end;

function TdxShiftTabKeyCommand.GetNextCell(
  ACurrentCell: TdxTableCell): TdxTableCell;
begin
  Result := ACurrentCell.Previous;
end;

class function TdxShiftTabKeyCommand.Id: TdxRichEditCommandId;
begin
  Result := TdxRichEditCommandId.ShiftTabKey;
end;

function TdxShiftTabKeyCommand.IsLastCellInDirection(
  ACell: TdxTableCell): Boolean;
begin
  Result := ACell.IsFirstCellInRow;
end;

procedure TdxShiftTabKeyCommand.ModifyCaretPositionCellTopAnchor;
begin
  CaretPosition.TableCellTopAnchorIndex := CaretPosition.TableCellTopAnchorIndex - 1;
end;

procedure TdxShiftTabKeyCommand.ProcessSingleCell(ACell: TdxTableCell);
begin
  if ACell.IsFirstCellInTable and (CaretPosition.TableCellTopAnchorIndex = 0) then
    Exit;
  inherited ProcessSingleCell(ACell);
end;

function TdxShiftTabKeyCommand.ShouldAppledTableRowAtBottom(
  ACurrentCell: TdxTableCell): Boolean;
begin
  Result := False;
end;

end.
