{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentLayout.UnitConverter;

interface

{$I cxVer.inc}

{$SCOPEDENUMS ON}

uses
  Types, cxGeometry;

type
  TdxDocumentLayoutUnit = (Document, Twip, Pixel);

  TdxLayoutGraphicsUnit = (Pixel = 2, Point = 3, Document = 5);

  TdxDocumentLayoutUnitConverter = class abstract
  protected
    function GetDpi: Single; virtual; abstract;
    function GetFontSizeScale: Single; virtual; abstract;
    function GetFontUnit: TdxLayoutGraphicsUnit; virtual; abstract;
    function GetGraphicsPageScale: Single; virtual; abstract;
    function GetGraphicsPageUnit: TdxLayoutGraphicsUnit; virtual; abstract;
  public
    class function CreateConverter(const AUnit: TdxDocumentLayoutUnit; const ADpi: Single): TdxDocumentLayoutUnitConverter;
    function DocumentsToFontUnitsF(const AValue: Single): Single; virtual; abstract;
    function DocumentsToLayoutUnits(const AValue: Integer): Integer; overload; virtual; abstract;
    function DocumentsToLayoutUnits(const AValue: TdxRectF): TdxRectF; overload; virtual; abstract;
    function DocumentsToLayoutUnits(const AValue: TRect): TRect; overload; virtual; abstract;
    function InchesToFontUnitsF(const AValue: Single): Single; virtual; abstract;
    function LayoutUnitsToDocuments(const AValue: TdxRectF): TdxRectF; overload; virtual; abstract;
    function LayoutUnitsToDocuments(const AValue: TRect): TRect; overload; virtual; abstract;
    function LayoutUnitsToHundredthsOfInch(const AValue: Integer): Integer; overload; virtual; abstract;
    function LayoutUnitsToHundredthsOfInch(const AValue: TSize): TSize; overload; virtual; abstract;
    function LayoutUnitsToPixels(const AValue: Integer; const ADpi: Single): Integer; overload; virtual; abstract;
    function LayoutUnitsToPixels(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; virtual; abstract;
    function LayoutUnitsToPixels(const AValue: TPoint; const ADpiX, ADpiY: Single): TPoint; overload; virtual; abstract;
    function LayoutUnitsToPixels(const AValue: TRect; const ADpiX, ADpiY: Single): TRect; overload; virtual; abstract;
    function LayoutUnitsToPixelsF(const AValue: Single; const ADpi: Single): Single; virtual; abstract;
    function LayoutUnitsToPointsF(const AValue: Single): Single; virtual; abstract;
    function LayoutUnitsToTwips(const AValue: Int64): Int64; overload; virtual; abstract;
    function LayoutUnitsToTwips(const AValue: Integer): Integer; overload; virtual; abstract;
    function MillimetersToFontUnitsF(const AValue: Single): Single; virtual; abstract;
    function PixelsToLayoutUnits(const AValue: Integer; const ADpi: Single): Integer; overload; virtual; abstract;
    function PixelsToLayoutUnits(const AValue: TSize; const ADpiX, ADpiY: Single): TSize; overload; virtual; abstract;
    function PixelsToLayoutUnits(const AValue: TRect; const ADpiX, ADpiY: Single): TRect; overload; virtual; abstract;
    function PixelsToLayoutUnitsF(const AValue: Single; const ADpi: Single): Single; virtual; abstract;
    function PointsToFontUnits(const AValue: Integer): Integer; virtual; abstract;
    function PointsToFontUnitsF(const AValue: Single): Single; virtual; abstract;
    function PointsToLayoutUnits(const AValue: Integer): Integer; virtual; abstract;
    function PointsToLayoutUnitsF(const AValue: Single): Single; virtual; abstract;
    function SnapToPixels(const AValue: Integer; const ADpi: Single): Integer; virtual; abstract;
    function TwipsToLayoutUnits(const AValue: Int64): Int64; overload; virtual; abstract;
    function TwipsToLayoutUnits(const AValue: Integer): Integer; overload; virtual; abstract;

    property Dpi: Single read GetDpi;
    property FontSizeScale: Single read GetFontSizeScale;
    property FontUnit: TdxLayoutGraphicsUnit read GetFontUnit;
    property GraphicsPageScale: Single read GetGraphicsPageScale;
    property GraphicsPageUnit: TdxLayoutGraphicsUnit read GetGraphicsPageUnit;
  end;

implementation

uses
  dxRichEdit.DocumentLayout.UnitDocumentConverter, dxRichEdit.DocumentLayout.UnitPixelsConverter,
  dxRichEdit.DocumentLayout.UnitTwipsConverter;

{ TdxDocumentLayoutUnitConverter }

class function TdxDocumentLayoutUnitConverter.CreateConverter(
  const AUnit: TdxDocumentLayoutUnit; const ADpi: Single): TdxDocumentLayoutUnitConverter;
begin
  case AUnit of
    TdxDocumentLayoutUnit.Document:
      Result := TdxDocumentLayoutUnitDocumentConverter.Create;
    TdxDocumentLayoutUnit.Twip:
      Result := TdxDocumentLayoutUnitTwipsConverter.Create;
    else
      Result := TdxDocumentLayoutUnitPixelsConverter.Create(ADpi);
  end;
end;

end.
