{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Control.DragAndDrop.Types;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, SysUtils, Classes, ActiveX, Windows, dxRichEdit.Utils.DataObject;

type

  TdxDragDropEffect = (None, Copy, Link, Move, Scroll);
  TdxDragDropEffects = set of TdxDragDropEffect;

  { TdxDragEventArgs }

  TdxDragEventArgs = record
    AllowedEffect: TdxDragDropEffects;
    Data: IDataObject;
    Effect: TdxDragDropEffects;
    KeyState: TShiftState;
    case Integer of
      0: (X, Y: Longint);
      1: (P: TPoint);
  end;
  PdxDragEventArgs = ^TdxDragEventArgs;

  { TdxDragEventArgsHelper }

  TdxDragEventArgsHelper = record helper for TdxDragEventArgs
    class function CreateFormOle(const dataObj: IDataObject; grfKeyState: Integer; pt: TPoint; var dwEffect: Integer): TdxDragEventArgs; static;
  end;

  TdxDragAction = (Cancel, Continue, Drop);

  { TdxQueryContinueDragEventArgs }

  TdxQueryContinueDragEventArgs = record
    Action: TdxDragAction;
    EscapePressed: Boolean;
    KeyStates: TShiftState;
    constructor Create(AAction: TdxDragAction; AEscapePressed: Boolean; AKeyStates: TShiftState); overload;
    constructor Create(fEscapePressed: BOOL; grfKeyState: Longint); overload;
  end;
  PdxQueryContinueDragEventArgs = ^TdxQueryContinueDragEventArgs;

  TdxGiveFeedbackEventArgs = record
    Effect: TdxDragDropEffects;
    UseDefaultCursors: Boolean;
    constructor Create(AEffect: TdxDragDropEffects; AUseDefaultCursors: Boolean); overload;
    constructor Create(dwEffect: Longint); overload;
  end;
  PdxGiveFeedbackEventArgs = ^TdxGiveFeedbackEventArgs;

function dxDragDropEffectsToOleDragDropEffects(const Value: TdxDragDropEffects): LongWord;
function dxOleDragDropEffectsToDragDropEffects(const Value: LongWord): TdxDragDropEffects;

implementation

uses
  Forms;

function dxDragDropEffectsToOleDragDropEffects(const Value: TdxDragDropEffects): LongWord;
begin
  Result := DROPEFFECT_NONE;
  if TdxDragDropEffect.Copy in Value then
    Result := Result or DROPEFFECT_COPY;
  if TdxDragDropEffect.Link in Value then
    Result := Result or DROPEFFECT_LINK;
  if TdxDragDropEffect.Move in Value then
    Result := Result or DROPEFFECT_MOVE;
  if TdxDragDropEffect.Scroll in Value then
    Result := Result or DROPEFFECT_SCROLL;
end;

function dxOleDragDropEffectsToDragDropEffects(const Value: LongWord): TdxDragDropEffects;
begin
  Result := [];
  if Value and DROPEFFECT_COPY = DROPEFFECT_COPY then
    Include(Result, TdxDragDropEffect.Copy);
  if Value and DROPEFFECT_LINK = DROPEFFECT_LINK then
    Include(Result, TdxDragDropEffect.Link);
  if Value and DROPEFFECT_MOVE = DROPEFFECT_MOVE then
    Include(Result, TdxDragDropEffect.Move);
  if Value and DROPEFFECT_SCROLL = DROPEFFECT_SCROLL then
    Include(Result, TdxDragDropEffect.Scroll);
end;

{ TdxDragEventArgsHelper }

class function TdxDragEventArgsHelper.CreateFormOle(const dataObj: IDataObject;
  grfKeyState: Integer; pt: TPoint; var dwEffect: Integer): TdxDragEventArgs;
begin
  Result.Data := dataObj;
  Result.KeyState := KeyDataToShiftState(grfKeyState);
  Result.AllowedEffect := dxOleDragDropEffectsToDragDropEffects(dwEffect);
  Result.Effect := [];
  Result.P := pt;
end;

{ TdxQueryContinueDragEventArgs }

constructor TdxQueryContinueDragEventArgs.Create(AAction: TdxDragAction;
  AEscapePressed: Boolean; AKeyStates: TShiftState);
begin
  Action := AAction;
  EscapePressed := AEscapePressed;
  KeyStates := AKeyStates;
end;

constructor TdxQueryContinueDragEventArgs.Create(fEscapePressed: BOOL;
  grfKeyState: Integer);
begin
  KeyStates := KeysToShiftState(grfKeyState);
  Action := TdxDragAction.Continue;
  EscapePressed := fEscapePressed;
  if EscapePressed then
    Action := TdxDragAction.Cancel
  else
    if [ssLeft, ssRight] * KeyStates = [] then
      Action := TdxDragAction.Drop;
end;

{ TdxGiveFeedbackEventArgs }

constructor TdxGiveFeedbackEventArgs.Create(AEffect: TdxDragDropEffects;
  AUseDefaultCursors: Boolean);
begin

end;

constructor TdxGiveFeedbackEventArgs.Create(dwEffect: Integer);
begin
  Effect := dxOleDragDropEffectsToDragDropEffects(dwEffect);
  UseDefaultCursors := True;
end;

end.
