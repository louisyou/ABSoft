{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.Utils.PredefinedFontSizeCollection;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Generics.Collections;

type
  TdxPredefinedFontSizeCollection = class(TList<Integer>)
  public const
    MinFontSize = 2; 
    MaxFontSize = 1500;
  protected
    procedure CreateDefaultContent; virtual;
    function CalculatePreviousFontSizeCore(AFontSize: Integer): Integer; virtual;
    function CalculateNextFontSizeCore(AFontSize: Integer): Integer; virtual;
    function CalcPrevTen(AValue: Integer): Integer;
    function CalcNextTen(AValue: Integer): Integer;
  public
    constructor Create;
    class function ValidateFontSize(AValue: Integer): Integer; static;
    function CalculatePreviousFontSize(AFontSize: Integer): Integer;
    function CalculateNextFontSize(AFontSize: Integer): Integer;
  end;

implementation

uses
  Math;

constructor TdxPredefinedFontSizeCollection.Create;
begin
  inherited Create;
  CreateDefaultContent;
end;

class function TdxPredefinedFontSizeCollection.ValidateFontSize(AValue: Integer): Integer;
begin
  Result := Max(Min(AValue, MaxFontSize), MinFontSize);
end;

procedure TdxPredefinedFontSizeCollection.CreateDefaultContent;
begin
  Add(8);
  Add(9);
  Add(10);
  Add(11);
  Add(12);
  Add(14);
  Add(16);
  Add(18);
  Add(20);
  Add(22);
  Add(24);
  Add(26);
  Add(28);
  Add(36);
  Add(48);
  Add(72);
end;

function TdxPredefinedFontSizeCollection.CalculateNextFontSize(AFontSize: Integer): Integer;
begin
  Result := ValidateFontSize(CalculateNextFontSizeCore(AFontSize));
end;

function TdxPredefinedFontSizeCollection.CalculateNextFontSizeCore(AFontSize: Integer): Integer;
var
  AFontSizeIndex: Integer;
begin
  if Count = 0 then
    Exit(AFontSize + 1);

  if AFontSize < First then  
    Exit(AFontSize + 1);

  if BinarySearch(AFontSize, AFontSizeIndex) then
    Inc(AFontSizeIndex);

  if AFontSizeIndex < Count then
    Result := Items[AFontSizeIndex]
  else
    Result := CalcNextTen(AFontSize);
end;

function TdxPredefinedFontSizeCollection.CalculatePreviousFontSize(AFontSize: Integer): Integer;
begin
  Result := ValidateFontSize(CalculatePreviousFontSizeCore(AFontSize));
end;

function TdxPredefinedFontSizeCollection.CalculatePreviousFontSizeCore(AFontSize: Integer): Integer;
var
  AFontSizeIndex: Integer;
begin
  if Count = 0 then
    Exit(AFontSize - 1);

  if AFontSize <= First then
    Exit(AFontSize - 1);

  if BinarySearch(AFontSize, AFontSizeIndex) then
    Exit(Items[AFontSizeIndex - 1]);

  if AFontSizeIndex <> Count then
    Exit(Items[AFontSizeIndex - 1]);

  if AFontSize > CalcNextTen(Last) then
    Result := CalcPrevTen(AFontSize)
  else
    Result := Items[AFontSizeIndex - 1];
end;

function TdxPredefinedFontSizeCollection.CalcNextTen(AValue: Integer): Integer;
begin
  Result := AValue + (10 - (AValue mod 10));
end;

function TdxPredefinedFontSizeCollection.CalcPrevTen(AValue: Integer): Integer;
begin
  if (AValue mod 10) <> 0 then
    Result := AValue - (AValue mod 10)
  else
    Result := AValue - 10;
end;

end.
