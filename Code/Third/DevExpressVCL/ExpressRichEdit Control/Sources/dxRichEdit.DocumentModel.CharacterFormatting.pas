{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressRichEditControl                                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRichEdit.DocumentModel.CharacterFormatting;

{$I cxVer.inc}

{$SCOPEDENUMS ON}

interface

uses
  Types, Classes, SysUtils, Graphics, dxCoreClasses,
  dxRichEdit.Utils.Types,
  dxRichEdit.DocumentModel.Core,
  dxRichEdit.DocumentModel.IndexBasedObject,
  dxRichEdit.DocumentModel.UnitConverter,
  dxRichEdit.DocumentModel.MergedProperties,
  dxRichEdit.DocumentModel.History.IndexChangedHistoryItem,
  dxRichEdit.Platform.Font;

type
  TdxStrikeoutType = (
    None = 0,
    Single = 1,
    Double = 2
  );

  TdxUnderlineType = (
    None = 0,
    Single = 1,
    Dotted = 2,
    Dashed = 3,
    DashDotted = 4,
    DashDotDotted = 5,
    Double = 6,
    HeavyWave = 7,
    LongDashed = 8,
    ThickSingle = 9,
    ThickDotted = 10,
    ThickDashed = 11,
    ThickDashDotted = 12,
    ThickDashDotDotted = 13,
    ThickLongDashed = 14,
    DoubleWave = 15,
    Wave = 16,
    DashSmallGap = 17
  );

  TdxMergedCharacterProperties = class;
  TdxCharacterFormattingOptionsCache = class;
  TdxCharacterFormattingInfoCache = class;

  IdxCharacterProperties = interface
  ['{EEA58078-8864-4D28-8A6C-115CA48255C6}']
    function GetAllCaps: Boolean;
    function GetBackColor: TColor;
    function GetDoubleFontSize: Integer;
    function GetFontBold: Boolean;
    function GetForeColor: TColor;
    function GetFontItalic: Boolean;
    function GetFontName: string;
    function GetFontUnderlineType: TdxUnderlineType;
    function GetFontStrikeoutType: TdxStrikeoutType;
    function GetHidden: Boolean;
    function GetNoProof: Boolean;
    function GetScript: TdxCharacterFormattingScript;
    function GetStrikeoutColor: TColor;
    function GetStrikeoutWordsOnly: Boolean;
    function GetUnderlineColor: TColor;
    function GetUnderlineWordsOnly: Boolean;

    procedure SetAllCaps(const Value: Boolean);
    procedure SetBackColor(const Value: TColor);
    procedure SetDoubleFontSize(const Value: Integer);
    procedure SetFontBold(const Value: Boolean);
    procedure SetForeColor(const Value: TColor);
    procedure SetFontItalic(const Value: Boolean);
    procedure SetFontName(const Value: string);
    procedure SetFontUnderlineType(const Value: TdxUnderlineType);
    procedure SetFontStrikeoutType(const Value: TdxStrikeoutType);
    procedure SetHidden(const Value: Boolean);
    procedure SetNoProof(const Value: Boolean);
    procedure SetScript(const Value: TdxCharacterFormattingScript);
    procedure SetStrikeoutColor(const Value: TColor);
    procedure SetStrikeoutWordsOnly(const Value: Boolean);
    procedure SetUnderlineColor(const Value: TColor);
    procedure SetUnderlineWordsOnly(const Value: Boolean);

    property AllCaps: Boolean read GetAllCaps write SetAllCaps;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property DoubleFontSize: Integer read GetDoubleFontSize write SetDoubleFontSize;
    property FontBold: Boolean read GetFontBold write SetFontBold;
    property ForeColor: TColor read GetForeColor write SetForeColor;
    property FontItalic: Boolean read GetFontItalic write SetFontItalic;
    property FontName: string read GetFontName write SetFontName;
    property FontUnderlineType: TdxUnderlineType read GetFontUnderlineType write SetFontUnderlineType;
    property FontStrikeoutType: TdxStrikeoutType read GetFontStrikeoutType write SetFontStrikeoutType;
    property Hidden: Boolean read GetHidden write SetHidden;
    property NoProof: Boolean read GetNoProof write SetNoProof;
    property Script: TdxCharacterFormattingScript read GetScript write SetScript;
    property StrikeoutColor: TColor read GetStrikeoutColor write SetStrikeoutColor;
    property StrikeoutWordsOnly: Boolean read GetStrikeoutWordsOnly write SetStrikeoutWordsOnly;
    property UnderlineColor: TColor read GetUnderlineColor write SetUnderlineColor;
    property UnderlineWordsOnly: Boolean read GetUnderlineWordsOnly write SetUnderlineWordsOnly;
  end;

  IdxCharacterPropertiesContainer = interface 
  ['{6F091A9C-85EB-4768-82EB-4D0D7554A507}']
    function GetPieceTable: TObject;
    function CreateCharacterPropertiesChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
    procedure OnCharacterPropertiesChanged;

    property PieceTable: TObject read GetPieceTable;
  end;

  { TdxCharacterFormattingInfo }

  TdxCharacterFormattingInfo = class(TcxIUnknownObject,
    IdxCloneable<TdxCharacterFormattingInfo>,
    IdxSupportsCopyFrom<TdxCharacterFormattingInfo>,
    IdxSupportsSizeOf, IdxCharacterProperties)
  private const
    MaskUnderlineType             = $00000001F; 
    MaskStrikeoutType             = $000000060; 
    MaskCharacterFormattingScript = $000000180; 
    MaskFontBold                  = $000000200;
    MaskFontItalic                = $000000400;
    MaskAllCaps                   = $000000800;
    MaskUnderlineWordsOnly        = $000001000;
    MaskStrikeoutWordsOnly        = $000002000;
    MaskHidden                    = $000004000;
    MaskNoProof                   = $000008000;
    MaskDoubleFontSize            = $0FFFF0000; 
  private
    FPackedValues: Cardinal;
    FBackColor: TColor;
    FForeColor: TColor;
    FStrikeoutColor: TColor;
    FUnderlineColor: TColor;
    FFontName: string;
    function GetBooleanValue(AMask: Cardinal): Boolean; inline;
    procedure SetBooleanValue(AMask: Cardinal; AValue: Boolean); inline;

    //IdxCharacterProperties
    function GetAllCaps: Boolean; inline;
    function GetBackColor: TColor; inline;
    function GetFontBold: Boolean; inline;
    function GetFontItalic: Boolean; inline;
    function GetFontName: string; inline;
    function GetDoubleFontSize: Integer; inline;
    function GetFontStrikeoutType: TdxStrikeoutType; inline;
    function GetFontUnderlineType: TdxUnderlineType; inline;
    function GetForeColor: TColor; inline;
    function GetHidden: Boolean; inline;
    function GetNoProof: Boolean;
    function GetScript: TdxCharacterFormattingScript; inline;
    function GetStrikeoutColor: TColor; inline;
    function GetStrikeoutWordsOnly: Boolean; inline;
    function GetUnderlineColor: TColor; inline;
    function GetUnderlineWordsOnly: Boolean; inline;
    procedure SetAllCaps(const Value: Boolean); inline;
    procedure SetBackColor(const Value: TColor); inline;
    procedure SetFontBold(const Value: Boolean); inline;
    procedure SetFontItalic(const Value: Boolean); inline;
    procedure SetFontName(const Value: string); inline;
    procedure SetDoubleFontSize(const Value: Integer); inline;
    procedure SetFontStrikeoutType(const Value: TdxStrikeoutType); inline;
    procedure SetFontUnderlineType(const Value: TdxUnderlineType); inline;
    procedure SetForeColor(const Value: TColor); inline;
    procedure SetHidden(const Value: Boolean); inline;
    procedure SetNoProof(const Value: Boolean);
    procedure SetScript(const Value: TdxCharacterFormattingScript); inline;
    procedure SetStrikeoutColor(const Value: TColor); inline;
    procedure SetStrikeoutWordsOnly(const Value: Boolean); inline;
    procedure SetUnderlineColor(const Value: TColor); inline;
    procedure SetUnderlineWordsOnly(const Value: Boolean); inline;
  protected
    property PackedValues: Cardinal read FPackedValues;
  public
    constructor Create;
    //IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxCharacterFormattingInfo);
    //IdxCloneable
    function Clone: TdxCharacterFormattingInfo;

    function Equals(Obj: TObject): Boolean; override;
    function GetHashCode: Integer; override;

    property AllCaps: Boolean read GetAllCaps write SetAllCaps;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property DoubleFontSize: Integer read GetDoubleFontSize write SetDoubleFontSize;
    property FontBold: Boolean read GetFontBold write SetFontBold;
    property FontItalic: Boolean read GetFontItalic write SetFontItalic;
    property FontName: string read GetFontName write SetFontName;
    property FontStrikeoutType: TdxStrikeoutType read GetFontStrikeoutType write SetFontStrikeoutType;
    property FontUnderlineType: TdxUnderlineType read GetFontUnderlineType write SetFontUnderlineType;
    property ForeColor: TColor read GetForeColor write SetForeColor;
    property Hidden: Boolean read GetHidden write SetHidden;
    property NoProof: Boolean read GetNoProof write SetNoProof;
    property Script: TdxCharacterFormattingScript read GetScript write SetScript;
    property StrikeoutColor: TColor read GetStrikeoutColor write SetStrikeoutColor;
    property StrikeoutWordsOnly: Boolean read GetStrikeoutWordsOnly write SetStrikeoutWordsOnly;
    property UnderlineColor: TColor read GetUnderlineColor write SetUnderlineColor;
    property UnderlineWordsOnly: Boolean read GetUnderlineWordsOnly write SetUnderlineWordsOnly;
  end;
  TdxCharacterFormattingInfoClass = class of TdxCharacterFormattingInfo;

  TdxUsedCharacterFormattingOption = (UseFontName, UseDoubleFontSize, UseFontBold,
    UseFontItalic, UseFontStrikeoutType, UseFontUnderlineType, UseAllCaps,
    UseForeColor, UseBackColor, UseUnderlineColor, UseStrikeoutColor,
    UseUnderlineWordsOnly, UseStrikeoutWordsOnly, UseScript, UseHidden, UseNoProof);

  TdxUsedCharacterFormattingOptions = set of TdxUsedCharacterFormattingOption;

  TdxCharacterFormattingOptions = class(TcxIUnknownObject,
    IdxCloneable<TdxCharacterFormattingOptions>,
    IdxSupportsCopyFrom<TdxCharacterFormattingOptions>,
    IdxSupportsSizeOf)
  const
    MaskUseAll = [
      TdxUsedCharacterFormattingOption.UseFontName,
      TdxUsedCharacterFormattingOption.UseDoubleFontSize,
      TdxUsedCharacterFormattingOption.UseFontBold,
      TdxUsedCharacterFormattingOption.UseFontItalic,
      TdxUsedCharacterFormattingOption.UseFontStrikeoutType,
      TdxUsedCharacterFormattingOption.UseFontUnderlineType,
      TdxUsedCharacterFormattingOption.UseAllCaps,
      TdxUsedCharacterFormattingOption.UseForeColor,
      TdxUsedCharacterFormattingOption.UseBackColor,
      TdxUsedCharacterFormattingOption.UseUnderlineColor,
      TdxUsedCharacterFormattingOption.UseStrikeoutColor,
      TdxUsedCharacterFormattingOption.UseUnderlineWordsOnly,
      TdxUsedCharacterFormattingOption.UseStrikeoutWordsOnly,
      TdxUsedCharacterFormattingOption.UseScript,
      TdxUsedCharacterFormattingOption.UseHidden,
      TdxUsedCharacterFormattingOption.UseNoProof];
  private
    FValue: TdxUsedCharacterFormattingOptions;
    function GetValue(const AOption: TdxUsedCharacterFormattingOption): Boolean; inline;
    procedure SetValue(const AOption: TdxUsedCharacterFormattingOption; const Value: Boolean); inline;
  public
    constructor Create(AUsedValues: TdxUsedCharacterFormattingOptions = []);
    procedure CopyFrom(const Source: TdxCharacterFormattingOptions);
    function Clone: TdxCharacterFormattingOptions;
    function Equals(Obj: TObject): Boolean; override;
    function GetHashCode: Integer; override;

    procedure ResetUse(AOptions: TdxUsedCharacterFormattingOptions); inline;

    property Value: TdxUsedCharacterFormattingOptions read FValue write FValue;
    property UseFontName: Boolean index TdxUsedCharacterFormattingOption.UseFontName read GetValue write SetValue;
    property UseDoubleFontSize: Boolean index TdxUsedCharacterFormattingOption.UseDoubleFontSize read GetValue write SetValue;
    property UseFontBold: Boolean index TdxUsedCharacterFormattingOption.UseFontBold read GetValue write SetValue;
    property UseFontItalic: Boolean index TdxUsedCharacterFormattingOption.UseFontItalic read GetValue write SetValue;
    property UseFontStrikeoutType: Boolean index TdxUsedCharacterFormattingOption.UseFontStrikeoutType read GetValue write SetValue;
    property UseFontUnderlineType: Boolean index TdxUsedCharacterFormattingOption.UseFontUnderlineType read GetValue write SetValue;
    property UseAllCaps: Boolean index TdxUsedCharacterFormattingOption.UseFontName read GetValue write SetValue;
    property UseForeColor: Boolean index TdxUsedCharacterFormattingOption.UseForeColor read GetValue write SetValue;
    property UseBackColor: Boolean index TdxUsedCharacterFormattingOption.UseBackColor read GetValue write SetValue;
    property UseUnderlineColor: Boolean index TdxUsedCharacterFormattingOption.UseUnderlineColor read GetValue write SetValue;
    property UseStrikeoutColor: Boolean index TdxUsedCharacterFormattingOption.UseStrikeoutColor read GetValue write SetValue;
    property UseUnderlineWordsOnly: Boolean index TdxUsedCharacterFormattingOption.UseUnderlineWordsOnly read GetValue write SetValue;
    property UseStrikeoutWordsOnly: Boolean index TdxUsedCharacterFormattingOption.UseStrikeoutColor read GetValue write SetValue;
    property UseScript: Boolean index TdxUsedCharacterFormattingOption.UseScript read GetValue write SetValue;
    property UseHidden: Boolean index TdxUsedCharacterFormattingOption.UseHidden read GetValue write SetValue;
    property UseNoProof: Boolean index TdxUsedCharacterFormattingOption.UseNoProof read GetValue write SetValue;
  end;

  { TdxCharacterFormattingBase }

  TdxCharacterFormattingBase = class(TdxIndexBasedObjectB<TdxCharacterFormattingInfo, TdxCharacterFormattingOptions>,
    IdxCloneable<TdxCharacterFormattingBase>, IdxSupportsCopyFrom<TdxCharacterFormattingBase>, IdxCharacterProperties)
  private
    //IdxCharacterProperties
    function GetAllCaps: Boolean; inline;
    function GetBackColor: TColor; inline;
    function GetFontBold: Boolean; inline;
    function GetForeColor: TColor; inline;
    function GetFontItalic: Boolean; inline;
    function GetFontName: string; inline;
    function GetDoubleFontSize: Integer; inline;
    function GetFontUnderlineType: TdxUnderlineType; inline;
    function GetFontStrikeoutType: TdxStrikeoutType; inline;
    function GetHidden: Boolean; inline;
    function GetNoProof: Boolean;
    function GetScript: TdxCharacterFormattingScript; inline;
    function GetStrikeoutColor: TColor; inline;
    function GetStrikeoutWordsOnly: Boolean; inline;
    function GetUnderlineColor: TColor; inline;
    function GetUnderlineWordsOnly: Boolean; inline;

    procedure SetAllCaps(const Value: Boolean);
    procedure SetAllCapsCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean); inline;
    procedure SetUseAllCapsCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetBackColor(const Value: TColor);
    procedure SetBackColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor); inline;
    procedure SetUseBackColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetDoubleFontSize(const Value: Integer);
    procedure SetDoubleFontSizeCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Integer); overload; inline;
    procedure SetUseDoubleFontSizeCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetFontBold(const Value: Boolean);
    procedure SetFontBoldCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean); inline;
    procedure SetUseFontBoldCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetForeColor(const Value: TColor);
    procedure SetForeColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor); inline;
    procedure SetUseForeColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetFontItalic(const Value: Boolean);
    procedure SetFontItalicCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean); inline;
    procedure SetUseFontItalicCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetFontName(const Value: string);
    procedure SetFontNameCore(const AInfo: TdxCharacterFormattingInfo; const AValue: string); inline;
    procedure SetUseFontNameCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetFontUnderlineType(const Value: TdxUnderlineType);
    procedure SetFontUnderlineTypeCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TdxUnderlineType); inline;
    procedure SetUseFontUnderlineTypeCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetFontStrikeoutType(const Value: TdxStrikeoutType);
    procedure SetFontStrikeoutTypeCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TdxStrikeoutType); inline;
    procedure SetUseFontStrikeoutTypeCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetHidden(const Value: Boolean);
    procedure SetHiddenCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean); inline;
    procedure SetUseHiddenCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetNoProof(const Value: Boolean);
    procedure SetNoProofCore(const AInfo: TdxCharacterFormattingInfo; const Value: Boolean); inline;
    procedure SetUseNoProofCore(const AOptions: TdxCharacterFormattingOptions; const Value: Boolean); inline;
    procedure SetScript(const Value: TdxCharacterFormattingScript);
    procedure SetScriptCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TdxCharacterFormattingScript); inline;
    procedure SetUseScriptCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetStrikeoutColor(const Value: TColor);
    procedure SetStrikeoutColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor); inline;
    procedure SetUseStrikeoutColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetStrikeoutWordsOnly(const Value: Boolean);
    procedure SetStrikeoutWordsOnlyCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean); inline;
    procedure SetUseStrikeoutWordsOnlyCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetUnderlineColor(const Value: TColor);
    procedure SetUnderlineColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor); inline;
    procedure SetUseUnderlineColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
    procedure SetUnderlineWordsOnly(const Value: Boolean);
    procedure SetUnderlineWordsOnlyCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean); inline;
    procedure SetUseUnderlineWordsOnlyCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean); inline;
  protected
    function GetInfoCache: TdxUniqueItemsCache<TdxCharacterFormattingInfo>; override;
    function GetOptionsCache: TdxUniqueItemsCache<TdxCharacterFormattingOptions>; override;
    function PropertyEquals(const AOther: TdxIndexBasedObject<TdxCharacterFormattingInfo, TdxCharacterFormattingOptions>): Boolean; override;
  public
    procedure CopyFrom(const AInfo: TdxCharacterFormattingInfo; const AOptions: TdxCharacterFormattingOptions); overload;
    procedure ResetUse(AResetOptions: TdxUsedCharacterFormattingOptions);
    //IdxSupportsCopyFrom
    procedure CopyFrom(const Source: TdxCharacterFormattingBase); overload;
    //IdxCloneable
    function Clone: TdxCharacterFormattingBase;

    property AllCaps: Boolean read GetAllCaps write SetAllCaps;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property DoubleFontSize: Integer read GetDoubleFontSize write SetDoubleFontSize;
    property FontBold: Boolean read GetFontBold write SetFontBold;
    property ForeColor: TColor read GetForeColor write SetForeColor;
    property FontItalic: Boolean read GetFontItalic write SetFontItalic;
    property FontName: string read GetFontName write SetFontName;
    property FontUnderlineType: TdxUnderlineType read GetFontUnderlineType write SetFontUnderlineType;
    property FontStrikeoutType: TdxStrikeoutType read GetFontStrikeoutType write SetFontStrikeoutType;
    property Hidden: Boolean read GetHidden write SetHidden;
    property NoProof: Boolean read GetNoProof write SetNoProof;

    property Script: TdxCharacterFormattingScript read GetScript write SetScript;
    property StrikeoutColor: TColor read GetStrikeoutColor write SetStrikeoutColor;
    property StrikeoutWordsOnly: Boolean read GetStrikeoutWordsOnly write SetStrikeoutWordsOnly;
    property UnderlineColor: TColor read GetUnderlineColor write SetUnderlineColor;
    property UnderlineWordsOnly: Boolean read GetUnderlineWordsOnly write SetUnderlineWordsOnly;
  end;

  { TdxCharacterProperties }

  TdxCharacterProperties = class(TdxRichEditIndexBasedObject<TdxCharacterFormattingBase>, IdxCharacterProperties)
  private
    FOwner: IdxCharacterPropertiesContainer;
    function GetPieceTable(const AOwner: IdxCharacterPropertiesContainer): TObject;

    function GetUseFontName: Boolean; inline;
    function GetUseDoubleFontSize: Boolean; inline;
    function GetUseFontBold: Boolean; inline;
    function GetUseFontItalic: Boolean; inline;
    function GetUseFontStrikeoutType: Boolean; inline;
    function GetUseFontUnderlineType: Boolean; inline;
    function GetUseAllCaps: Boolean; inline;
    function GetUseForeColor: Boolean; inline;
    function GetUseBackColor: Boolean; inline;
    function GetUseUnderlineColor: Boolean; inline;
    function GetUseStrikeoutColor: Boolean; inline;
    function GetUseUnderlineWordsOnly: Boolean; inline;
    function GetUseStrikeoutWordsOnly: Boolean; inline;
    function GetUseScript: Boolean; inline;
    function GetUseHidden: Boolean; inline;
    //IdxCharacterProperties
    function GetAllCaps: Boolean; inline;
    function GetBackColor: TColor; inline;
    function GetFontBold: Boolean; inline;
    function GetFontItalic: Boolean; inline;
    function GetFontName: string; inline;
    function GetDoubleFontSize: Integer; inline;
    function GetFontStrikeoutType: TdxStrikeoutType; inline;
    function GetFontUnderlineType: TdxUnderlineType; inline;
    function GetForeColor: TColor; inline;
    function GetHidden: Boolean; inline;
    function GetNoProof: Boolean;
    function GetScript: TdxCharacterFormattingScript; inline;
    function GetStrikeoutColor: TColor; inline;
    function GetStrikeoutWordsOnly: Boolean; inline;
    function GetUnderlineColor: TColor; inline;
    function GetUnderlineWordsOnly: Boolean; inline;
    procedure SetAllCaps(const Value: Boolean);
    function SetAllCapsCore(const AInfo: TdxCharacterFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions; inline;
    procedure SetBackColor(const Value: TColor);
    function SetBackColorCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetDoubleFontSize(const Value: Integer);
    function SetDoubleFontSizeCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetFontBold(const Value: Boolean);
    function SetFontBoldCore(const AInfo: TdxCharacterFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions; inline;
    procedure SetFontItalic(const Value: Boolean);
    function SetFontItalicCore(const AInfo: TdxCharacterFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions; inline;
    procedure SetFontName(const Value: string);
    function SetFontNameCore(const AInfo: TdxCharacterFormattingBase; const Value: string): TdxDocumentModelChangeActions; inline;
    procedure SetFontStrikeoutType(const Value: TdxStrikeoutType);
    function SetFontStrikeoutTypeCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetFontUnderlineType(const Value: TdxUnderlineType);
    function SetFontUnderlineTypeCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetForeColor(const Value: TColor);
    function SetForeColorCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetHidden(const Value: Boolean);
    function SetHiddenCore(const AInfo: TdxCharacterFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions; inline;
    procedure SetScript(const Value: TdxCharacterFormattingScript);
    function SetScriptCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetStrikeoutColor(const Value: TColor);
    function SetStrikeoutColorCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetStrikeoutWordsOnly(const Value: Boolean);
    function SetStrikeoutWordsOnlyCore(const AInfo: TdxCharacterFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions; inline;
    procedure SetUnderlineColor(const Value: TColor);
    function SetUnderlineColorCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions; inline;
    procedure SetUnderlineWordsOnly(const Value: Boolean);
    function SetUnderlineWordsOnlyCore(const AInfo: TdxCharacterFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions; inline;
    procedure SetNoProof(const Value: Boolean);
    function SetNoProofCore(const AInfo: TdxCharacterFormattingBase; const AValue: Boolean): TdxDocumentModelChangeActions; inline;
  protected
    procedure CopyFrom(const ACharacterProperties: TdxMergedProperties<TdxCharacterFormattingInfo, TdxCharacterFormattingOptions>); overload;
    function CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>; override; 
    function GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxCharacterFormattingBase>; override;
    procedure OnIndexChanged; override;

    property Owner: IdxCharacterPropertiesContainer read FOwner;
  public
    constructor Create(const AOwner: IdxCharacterPropertiesContainer); reintroduce;
    destructor Destroy; override;

    class procedure ApplyPropertiesDiff(ATarget: TdxCharacterProperties; ATargetMergedInfo, ASourceMergedInfo: TdxCharacterFormattingInfo);
    procedure CopyFrom(const AProperties: TdxMergedCharacterProperties); overload;
    procedure Merge(const AProperties: TdxCharacterProperties);
    procedure Reset;
    procedure ResetAllUse;
    procedure ResetUse(const AResetOption: TdxUsedCharacterFormattingOption); overload;
    procedure ResetUse(const AResetOptions: TdxUsedCharacterFormattingOptions); overload;
    function UseVal(AMask: TdxUsedCharacterFormattingOption): Boolean; 

    function GetBatchUpdateChangeActions: TdxDocumentModelChangeActions; override;
    function GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener; override; 

    property AllCaps: Boolean read GetAllCaps write SetAllCaps;
    property BackColor: TColor read GetBackColor write SetBackColor;
    property DoubleFontSize: Integer read GetDoubleFontSize write SetDoubleFontSize;
    property FontBold: Boolean read GetFontBold write SetFontBold;
    property FontItalic: Boolean read GetFontItalic write SetFontItalic;
    property FontName: string read GetFontName write SetFontName;
    property FontStrikeoutType: TdxStrikeoutType read GetFontStrikeoutType write SetFontStrikeoutType;
    property FontUnderlineType: TdxUnderlineType read GetFontUnderlineType write SetFontUnderlineType;
    property ForeColor: TColor read GetForeColor write SetForeColor;
    property Hidden: Boolean read GetHidden write SetHidden;
    property NoProof: Boolean read GetNoProof write SetNoProof;
    property Script: TdxCharacterFormattingScript read GetScript write SetScript;
    property StrikeoutColor: TColor read GetStrikeoutColor write SetStrikeoutColor;
    property StrikeoutWordsOnly: Boolean read GetStrikeoutWordsOnly write SetStrikeoutWordsOnly;
    property UnderlineColor: TColor read GetUnderlineColor write SetUnderlineColor;
    property UnderlineWordsOnly: Boolean read GetUnderlineWordsOnly write SetUnderlineWordsOnly;

    property UseAllCaps: Boolean read GetUseAllCaps;
    property UseForeColor: Boolean read GetUseForeColor;
    property UseBackColor: Boolean read GetUseBackColor;
    property UseDoubleFontSize: Boolean read GetUseDoubleFontSize;
    property UseFontName: Boolean read GetUseFontName;
    property UseFontBold: Boolean read GetUseFontBold;
    property UseFontItalic: Boolean read GetUseFontItalic;
    property UseFontStrikeoutType: Boolean read GetUseFontStrikeoutType;
    property UseFontUnderlineType: Boolean read GetUseFontUnderlineType;
    property UseUnderlineColor: Boolean read GetUseUnderlineColor;
    property UseStrikeoutColor: Boolean read GetUseStrikeoutColor;
		property UseUnderlineWordsOnly: Boolean read GetUseUnderlineWordsOnly;
    property UseStrikeoutWordsOnly: Boolean read GetUseStrikeoutWordsOnly;
    property UseScript: Boolean read GetUseScript;
    property UseHidden: Boolean read GetUseHidden;
  end;

  { TdxMergedCharacterProperties }

  TdxMergedCharacterProperties = class(TdxMergedProperties<TdxCharacterFormattingInfo, TdxCharacterFormattingOptions>);

  { TdxCharacterPropertiesMerger }

  TdxCharacterPropertiesMerger = class(TdxPropertiesMergerBase<TdxCharacterFormattingInfo, TdxCharacterFormattingOptions, TdxMergedCharacterProperties>)
  protected
    procedure MergeCore(const AInfo: TdxCharacterFormattingInfo; const AOptions: TdxCharacterFormattingOptions); override;
  public
    constructor Create(const AProperties: TdxCharacterProperties); overload;

    procedure Merge(const AProperties: TdxCharacterProperties); overload;
  end;

  { TdxCharacterFormattingOptionsCache }

  TdxCharacterFormattingOptionsCache = class(TdxUniqueItemsCache<TdxCharacterFormattingOptions>)
  const
    EmptyCharacterFormattingOptionIndex = 0;
    RootCharacterFormattingOptionIndex  = 1;
  public
    procedure InitItems(const AUnitConverter: IdxDocumentModelUnitConverter); override;
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxCharacterFormattingOptions; override;
    procedure AddRootStyleOptions;
  end;

  { TdxCharacterFormattingInfoCache }

  TdxCharacterFormattingInfoCache = class(TdxUniqueItemsCache<TdxCharacterFormattingInfo>) 
  const
    DefaultItemIndex = 0;
  public
    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxCharacterFormattingInfo; override; 
  end;

  { TdxCharacterFormattingCache }

  TdxCharacterFormattingCache = class(TdxUniqueItemsCache<TdxCharacterFormattingBase>)
  const
    RootCharacterFormattingIndex  = 1;
    EmptyCharacterFormattingIndex = 0;
  private
    FDocumentModel: TdxCustomDocumentModel;
  public
    constructor Create(const ADocumentModel: TdxCustomDocumentModel);

    function CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxCharacterFormattingBase; override;

    property DocumentModel: TdxCustomDocumentModel read FDocumentModel;
  end;

  { TdxCharacterPropertiesFontAssignmentHelper }

  TdxCharacterPropertiesFontAssignmentHelper = class 
  public
    class procedure AssignFont(ACharacterProperties: IdxCharacterProperties; AFont: TFont); 
  end;

  TdxCharacterFormattingChangeType = (None, FontName, FontSize, FontBold,
    FontItalic, FontUnderlineType, FontStrikeoutType, AllCaps, ForeColor,
    BackColor, UnderlineColor, StrikeoutColor, UnderlineWordsOnly,
    StrikeoutWordsOnly, Script, CharacterStyle, Hidden, NoProof, BatchUpdate);

  { TdxCharacterFormattingChangeActionsCalculator }

	TdxCharacterFormattingChangeActionsCalculator = class
  public
    class function CalculateChangeActions(const AChange: TdxCharacterFormattingChangeType): TdxDocumentModelChangeActions;
  end;

  { TdxParagraphMergedCharacterPropertiesCachedResult }

  TdxParagraphMergedCharacterPropertiesCachedResult = class
  strict private
    FParagraphStyleIndex: Integer;
    FTableCell: TObject; 
    FMergedCharacterProperties: TdxMergedCharacterProperties;
  private
    procedure SetMergedCharacterProperties(
      const Value: TdxMergedCharacterProperties);
  public
    constructor Create; virtual;
    destructor Destroy; override;

    property ParagraphStyleIndex: Integer read FParagraphStyleIndex write FParagraphStyleIndex;
    property TableCell: TObject read FTableCell write FTableCell; 
    property MergedCharacterProperties: TdxMergedCharacterProperties read FMergedCharacterProperties write SetMergedCharacterProperties;
  end;

  { TdxRunMergedCharacterPropertiesCachedResult }

  TdxRunMergedCharacterPropertiesCachedResult = class(TdxParagraphMergedCharacterPropertiesCachedResult)
  strict private
    FCharacterStyleIndex: Integer;
    FCharacterPropertiesIndex: Integer;
  public
    constructor Create; override;
    property CharacterStyleIndex: Integer read FCharacterStyleIndex write FCharacterStyleIndex;
    property CharacterPropertiesIndex: Integer read FCharacterPropertiesIndex write FCharacterPropertiesIndex;
  end;

implementation

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  dxRichEdit.Utils.BatchUpdateHelper, RTLConsts, 
  dxRichEdit.DocumentModel.PieceTable, dxRichEdit.Utils.Colors, dxHashUtils;

{ TdxCharacterFormattingInfo }

constructor TdxCharacterFormattingInfo.Create;
begin
  inherited Create;
  FForeColor := TdxColor.Empty;
  FBackColor := TdxColor.Transparent;
  FUnderlineColor := TdxColor.Empty;
  FStrikeoutColor := TdxColor.Empty;
end;

function TdxCharacterFormattingInfo.GetBooleanValue(AMask: Cardinal): Boolean;
begin
  Result := FPackedValues and AMask <> 0;
end;

procedure TdxCharacterFormattingInfo.SetBooleanValue(AMask: Cardinal; AValue: Boolean);
begin
  if AValue then
    FPackedValues := FPackedValues or AMask
  else
    FPackedValues := FPackedValues and not AMask;
end;

function TdxCharacterFormattingInfo.GetAllCaps: Boolean;
begin
  Result := GetBooleanValue(MaskAllCaps);
end;

function TdxCharacterFormattingInfo.GetBackColor: TColor;
begin
  Result := FBackColor;
end;

function TdxCharacterFormattingInfo.GetFontBold: Boolean;
begin
  Result := GetBooleanValue(MaskFontBold);
end;

function TdxCharacterFormattingInfo.GetFontItalic: Boolean;
begin
  Result := GetBooleanValue(MaskFontItalic);
end;

function TdxCharacterFormattingInfo.GetFontName: string;
begin
  Result := FFontName;
end;

function TdxCharacterFormattingInfo.GetDoubleFontSize: Integer;
begin
  Result := LongRec(FPackedValues).Hi;
end;

function TdxCharacterFormattingInfo.GetFontStrikeoutType: TdxStrikeoutType;
begin
  Result := TdxStrikeoutType((FPackedValues and MaskStrikeoutType) shr 5);
end;

function TdxCharacterFormattingInfo.GetFontUnderlineType: TdxUnderlineType;
begin
  Result := TdxUnderlineType(FPackedValues and MaskUnderlineType);
end;

function TdxCharacterFormattingInfo.GetForeColor: TColor;
begin
  Result := FForeColor;
end;

function TdxCharacterFormattingInfo.GetHidden: Boolean;
begin
  Result := GetBooleanValue(MaskHidden);
end;

function TdxCharacterFormattingInfo.GetNoProof: Boolean;
begin
  Result := GetBooleanValue(MaskNoProof);
end;

function TdxCharacterFormattingInfo.GetScript: TdxCharacterFormattingScript;
begin
  Result := TdxCharacterFormattingScript((FPackedValues and MaskCharacterFormattingScript) shr 7);
end;

function TdxCharacterFormattingInfo.GetStrikeoutColor: TColor;
begin
  Result := FStrikeoutColor;
end;

function TdxCharacterFormattingInfo.GetStrikeoutWordsOnly: Boolean;
begin
  Result := GetBooleanValue(MaskStrikeoutWordsOnly);
end;

function TdxCharacterFormattingInfo.GetUnderlineColor: TColor;
begin
  Result := FUnderlineColor;
end;

function TdxCharacterFormattingInfo.GetUnderlineWordsOnly: Boolean;
begin
  Result := GetBooleanValue(MaskUnderlineWordsOnly);
end;

procedure TdxCharacterFormattingInfo.SetAllCaps(const Value: Boolean);
begin
  SetBooleanValue(MaskAllCaps, Value);
end;

procedure TdxCharacterFormattingInfo.SetBackColor(const Value: TColor);
begin
  FBackColor := Value;
end;

procedure TdxCharacterFormattingInfo.SetFontBold(const Value: Boolean);
begin
  SetBooleanValue(MaskFontBold, Value);
end;

procedure TdxCharacterFormattingInfo.SetFontItalic(const Value: Boolean);
begin
  SetBooleanValue(MaskFontItalic, Value);
end;

procedure TdxCharacterFormattingInfo.SetFontName(const Value: string);
begin
  FFontName := Value;
end;

procedure TdxCharacterFormattingInfo.SetDoubleFontSize(const Value: Integer);
begin
  Assert(Value > 0, 'Bad font size');
  LongRec(FPackedValues).Hi := Word(Value);
end;

procedure TdxCharacterFormattingInfo.SetFontStrikeoutType(const Value: TdxStrikeoutType);
begin
  FPackedValues := FPackedValues and not MaskStrikeoutType;
  FPackedValues := FPackedValues or ((Cardinal(Ord(Value)) shl 5) and MaskStrikeoutType);;
end;

procedure TdxCharacterFormattingInfo.SetFontUnderlineType(const Value: TdxUnderlineType);
begin
  FPackedValues := FPackedValues and not MaskUnderlineType;
  FPackedValues := FPackedValues or (Cardinal(Ord(Value)) and MaskUnderlineType);
end;

procedure TdxCharacterFormattingInfo.SetForeColor(const Value: TColor);
begin
  FForeColor := Value;
end;

procedure TdxCharacterFormattingInfo.SetHidden(const Value: Boolean);
begin
  SetBooleanValue(MaskHidden, Value);
end;

procedure TdxCharacterFormattingInfo.SetNoProof(const Value: Boolean);
begin
  SetBooleanValue(MaskNoProof, Value);
end;

procedure TdxCharacterFormattingInfo.SetScript(const Value: TdxCharacterFormattingScript);
begin
  FPackedValues := FPackedValues and not MaskCharacterFormattingScript;
  FPackedValues := FPackedValues or ((Cardinal(Ord(Value)) shl 7) and MaskCharacterFormattingScript);
end;

procedure TdxCharacterFormattingInfo.SetStrikeoutColor(const Value: TColor);
begin
  FStrikeoutColor := Value;
end;

procedure TdxCharacterFormattingInfo.SetStrikeoutWordsOnly(const Value: Boolean);
begin
  SetBooleanValue(MaskStrikeoutWordsOnly, Value);
end;

procedure TdxCharacterFormattingInfo.SetUnderlineColor(const Value: TColor);
begin
  FUnderlineColor := Value;
end;

procedure TdxCharacterFormattingInfo.SetUnderlineWordsOnly(const Value: Boolean);
begin
  SetBooleanValue(MaskUnderlineWordsOnly, Value);
end;

procedure TdxCharacterFormattingInfo.CopyFrom(const Source: TdxCharacterFormattingInfo);
begin
  FPackedValues := Source.PackedValues;

  BackColor := Source.BackColor;
  FontName := Source.FontName;
  ForeColor := Source.ForeColor;
  StrikeoutColor := Source.StrikeoutColor;
  UnderlineColor := Source.UnderlineColor;
end;

function TdxCharacterFormattingInfo.Clone: TdxCharacterFormattingInfo;
begin
  Result := TdxCharacterFormattingInfoClass(ClassType).Create;
  Result.CopyFrom(Self);
end;

function TdxCharacterFormattingInfo.Equals(Obj: TObject): Boolean;
var
  AInfo: TdxCharacterFormattingInfo absolute Obj;
begin
  Assert(Obj is TdxCharacterFormattingInfo);
  Result :=
    (FPackedValues = AInfo.FPackedValues) and
    (FForeColor = AInfo.FForeColor) and
    (FBackColor = AInfo.FBackColor) and
    (FUnderlineColor = AInfo.FUnderlineColor) and
    (FStrikeoutColor = AInfo.FStrikeoutColor) and
    AnsiSameText(FFontName, AInfo.FFontName); 
end;

function TdxCharacterFormattingInfo.GetHashCode: Integer;
var
  ABuffer: array[0..512] of Char;
begin
  Result := Integer(FPackedValues xor (Cardinal(ForeColor) shl 8) xor (Cardinal(BackColor) shl 8) xor
    Cardinal(dxElfHash(PWideChar(FFontName), Length(FFontName), PWideChar(@ABuffer), 512)));


end;

{ TdxCharacterFormattingOptions }

constructor TdxCharacterFormattingOptions.Create(AUsedValues: TdxUsedCharacterFormattingOptions = []);
begin
  inherited Create;
  FValue := AUsedValues;
end;

procedure TdxCharacterFormattingOptions.CopyFrom(const Source: TdxCharacterFormattingOptions);
begin
  Assert(Source is TdxCharacterFormattingOptions);
  FValue := Source.FValue;
end;

function TdxCharacterFormattingOptions.Clone: TdxCharacterFormattingOptions;
begin
  Result := TdxCharacterFormattingOptions.Create;
  Result.CopyFrom(Self);
end;

function TdxCharacterFormattingOptions.GetHashCode: Integer;
begin
  Result := Word(FValue);
end;

function TdxCharacterFormattingOptions.Equals(Obj: TObject): Boolean;
begin
  Assert(Obj is TdxCharacterFormattingOptions);
  Result := FValue = TdxCharacterFormattingOptions(Obj).FValue;
end;

{ TdxCharacterFormattingOptionsCache }

procedure TdxCharacterFormattingOptionsCache.AddRootStyleOptions;
begin
  AppendItem(TdxCharacterFormattingOptions.Create(TdxCharacterFormattingOptions.MaskUseAll));
end;

function TdxCharacterFormattingOptionsCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxCharacterFormattingOptions;
begin
  Result := TdxCharacterFormattingOptions.Create;
end;

procedure TdxCharacterFormattingOptionsCache.InitItems(const AUnitConverter: IdxDocumentModelUnitConverter);
begin
  inherited InitItems(AUnitConverter);
  AddRootStyleOptions;
end;

procedure TdxCharacterFormattingOptions.ResetUse(AOptions: TdxUsedCharacterFormattingOptions);
begin
  FValue := FValue - AOptions;
end;

function TdxCharacterFormattingOptions.GetValue(const AOption: TdxUsedCharacterFormattingOption): Boolean;
begin
  Result := AOption in FValue;
end;

procedure TdxCharacterFormattingOptions.SetValue(const AOption: TdxUsedCharacterFormattingOption; const Value: Boolean);
begin
  if Value then
    Include(FValue, AOption)
  else
    Exclude(FValue, AOption);
end;

{ TdxCharacterFormattingBase }

procedure TdxCharacterFormattingBase.ResetUse(AResetOptions: TdxUsedCharacterFormattingOptions);
var
  AOptions: TdxCharacterFormattingOptions;
  AInfo: TdxCharacterFormattingInfo;
  AIsDeferredInfo, AIsDeferredOptions: Boolean;
begin
  AOptions := GetOptionsForModification(AIsDeferredOptions);
  AOptions.ResetUse(AResetOptions);
  AInfo := GetInfoForModification(AIsDeferredInfo);
  ReplaceInfo(AInfo, AOptions);
  if not AIsDeferredInfo then AInfo.Free;
  if not AIsDeferredOptions then AOptions.Free;
end;

procedure TdxCharacterFormattingBase.CopyFrom(const Source: TdxCharacterFormattingBase);
begin
  CopyFrom(Source.Info, Source.Options);
end;

procedure TdxCharacterFormattingBase.CopyFrom(const AInfo: TdxCharacterFormattingInfo; const AOptions: TdxCharacterFormattingOptions);
begin
  CopyFromCore(AInfo, AOptions);
end;

function TdxCharacterFormattingBase.Clone: TdxCharacterFormattingBase;
begin
  Result := TdxCharacterFormattingBase.Create(PieceTable, DocumentModel, InfoIndex, OptionsIndex);
end;

function TdxCharacterFormattingBase.GetInfoCache: TdxUniqueItemsCache<TdxCharacterFormattingInfo>;
begin
  Result := TdxDocumentModel(DocumentModel).Cache.CharacterFormattingInfoCache;
end;

function TdxCharacterFormattingBase.GetNoProof: Boolean;
begin
  Result := Info.NoProof;
end;

function TdxCharacterFormattingBase.GetOptionsCache: TdxUniqueItemsCache<TdxCharacterFormattingOptions>;
begin
  Result := TdxDocumentModel(DocumentModel).Cache.CharacterFormattingOptionsCache;
end;

function TdxCharacterFormattingBase.PropertyEquals(const AOther: TdxIndexBasedObject<TdxCharacterFormattingInfo, TdxCharacterFormattingOptions>): Boolean;
begin
  Result := (Options.Value = AOther.Options.Value) and Info.Equals(AOther.Info);
end;

function TdxCharacterFormattingBase.GetAllCaps: Boolean;
begin
  Result := Info.AllCaps;
end;

function TdxCharacterFormattingBase.GetBackColor: TColor;
begin
  Result := Info.BackColor;
end;

function TdxCharacterFormattingBase.GetFontBold: Boolean;
begin
  Result := Info.FontBold;
end;

function TdxCharacterFormattingBase.GetForeColor: TColor;
begin
  Result := Info.ForeColor;
end;

function TdxCharacterFormattingBase.GetFontItalic: Boolean;
begin
  Result := Info.FontItalic;
end;

function TdxCharacterFormattingBase.GetFontName: string;
begin
  Result := Info.FontName;
end;

function TdxCharacterFormattingBase.GetDoubleFontSize: Integer;
begin
  Result := Info.DoubleFontSize;
end;

function TdxCharacterFormattingBase.GetFontUnderlineType: TdxUnderlineType;
begin
  Result := Info.FontUnderlineType;
end;

function TdxCharacterFormattingBase.GetFontStrikeoutType: TdxStrikeoutType;
begin
  Result := Info.FontStrikeoutType;
end;

function TdxCharacterFormattingBase.GetHidden: Boolean;
begin
  Result := Info.Hidden;
end;

function TdxCharacterFormattingBase.GetScript: TdxCharacterFormattingScript;
begin
  Result := Info.Script;
end;

function TdxCharacterFormattingBase.GetStrikeoutColor: TColor;
begin
  Result := Info.StrikeoutColor;
end;

function TdxCharacterFormattingBase.GetStrikeoutWordsOnly: Boolean;
begin
  Result := Info.StrikeoutWordsOnly;
end;

function TdxCharacterFormattingBase.GetUnderlineColor: TColor;
begin
  Result := Info.UnderlineColor;
end;

function TdxCharacterFormattingBase.GetUnderlineWordsOnly: Boolean;
begin
  Result := Info.UnderlineWordsOnly;
end;

procedure TdxCharacterFormattingBase.SetAllCapsCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean);
begin
  AInfo.AllCaps := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseAllCapsCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseAllCaps := AValue;
end;

procedure TdxCharacterFormattingBase.SetAllCaps(const Value: Boolean);
begin
  if (Info.AllCaps = Value) and Options.UseAllCaps then
    Exit;
  SetPropertyValue<Boolean>(SetAllCapsCore, Value, SetUseAllCapsCore);
end;

procedure TdxCharacterFormattingBase.SetBackColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor);
begin
  AInfo.BackColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseBackColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseBackColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetBackColor(const Value: TColor);
begin
  if (Info.BackColor = Value) and Options.UseBackColor then
    Exit;
  SetPropertyValue<TColor>(SetBackColorCore, Value, SetUseBackColorCore);
end;

procedure TdxCharacterFormattingBase.SetDoubleFontSizeCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Integer);
begin
  AInfo.DoubleFontSize := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseDoubleFontSizeCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseDoubleFontSize := AValue;
end;

procedure TdxCharacterFormattingBase.SetDoubleFontSize(const Value: Integer);
begin
  if (Info.DoubleFontSize = Value) and Options.UseDoubleFontSize then
    Exit;
  SetPropertyValue<Integer>(SetDoubleFontSizeCore, Value, SetUseDoubleFontSizeCore);
end;

procedure TdxCharacterFormattingBase.SetFontBoldCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean);
begin
  AInfo.FontBold := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseFontBoldCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseFontBold := AValue;
end;

procedure TdxCharacterFormattingBase.SetFontBold(const Value: Boolean);
begin
  if (Info.FontBold = Value) and Options.UseFontBold then
    Exit;
  SetPropertyValue<Boolean>(SetFontBoldCore, Value, SetUseFontBoldCore);
end;

procedure TdxCharacterFormattingBase.SetForeColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor);
begin
  AInfo.ForeColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseForeColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseForeColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetForeColor(const Value: TColor);
begin
  if (Info.ForeColor = Value) and Options.UseForeColor then
    Exit;
  SetPropertyValue<TColor>(SetForeColorCore, Value, SetUseForeColorCore);
end;

procedure TdxCharacterFormattingBase.SetFontItalicCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean);
begin
  AInfo.FontItalic := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseFontItalicCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseFontItalic := AValue;
end;

procedure TdxCharacterFormattingBase.SetFontItalic(const Value: Boolean);
begin
  if (Info.FontItalic = Value) and Options.UseFontItalic then
    Exit;
  SetPropertyValue<Boolean>(SetFontItalicCore, Value, SetUseFontItalicCore);
end;

procedure TdxCharacterFormattingBase.SetFontNameCore(const AInfo: TdxCharacterFormattingInfo; const AValue: string);
begin
  AInfo.FontName := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseFontNameCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseFontName := AValue;
end;

procedure TdxCharacterFormattingBase.SetFontName(const Value: string);
begin
  if Options.UseFontName and (Info.FontName = Value) then
    Exit;
  SetPropertyValue<string>(SetFontNameCore, Value, SetUseFontNameCore);
end;

procedure TdxCharacterFormattingBase.SetFontUnderlineTypeCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TdxUnderlineType);
begin
  AInfo.FontUnderlineType := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseFontUnderlineTypeCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseFontUnderlineType := AValue;
end;

procedure TdxCharacterFormattingBase.SetFontUnderlineType(const Value: TdxUnderlineType);
begin
  if (Info.FontUnderlineType = Value) and Options.UseFontUnderlineType then
    Exit;
  SetPropertyValue<TdxUnderlineType>(SetFontUnderlineTypeCore, Value, SetUseFontUnderlineTypeCore);
end;

procedure TdxCharacterFormattingBase.SetFontStrikeoutTypeCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TdxStrikeoutType);
begin
  AInfo.FontStrikeoutType := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseFontStrikeoutTypeCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseFontStrikeoutType := AValue;
end;

procedure TdxCharacterFormattingBase.SetFontStrikeoutType(const Value: TdxStrikeoutType);
begin
  if (Info.FontStrikeoutType = Value) and Options.UseFontStrikeoutType then
    Exit;
  SetPropertyValue<TdxStrikeoutType>(SetFontStrikeoutTypeCore, Value, SetUseFontStrikeoutTypeCore);
end;

procedure TdxCharacterFormattingBase.SetHiddenCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean);
begin
  AInfo.Hidden := AValue;
end;

procedure TdxCharacterFormattingBase.SetNoProof(const Value: Boolean);
begin
  if (Info.NoProof = Value) and Options.UseNoProof then
    Exit;
  SetPropertyValue<Boolean>(SetNoProofCore, Value, SetUseNoProofCore);
end;

procedure TdxCharacterFormattingBase.SetNoProofCore(const AInfo: TdxCharacterFormattingInfo; const Value: Boolean);
begin
  AInfo.NoProof := Value;
end;

procedure TdxCharacterFormattingBase.SetUseHiddenCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseHidden := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseNoProofCore(const AOptions: TdxCharacterFormattingOptions;
  const Value: Boolean);
begin
  AOptions.UseNoProof := Value;
end;

procedure TdxCharacterFormattingBase.SetHidden(const Value: Boolean);
begin
  if (Info.Hidden = Value) and Options.UseHidden then
    Exit;
  SetPropertyValue<Boolean>(SetHiddenCore, Value, SetUseHiddenCore);
end;

procedure TdxCharacterFormattingBase.SetScriptCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TdxCharacterFormattingScript);
begin
  AInfo.Script := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseScriptCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseScript := AValue;
end;

procedure TdxCharacterFormattingBase.SetScript(const Value: TdxCharacterFormattingScript);
begin
  if (Info.Script = Value) and Options.UseScript then
    Exit;
  SetPropertyValue<TdxCharacterFormattingScript>(SetScriptCore, Value, SetUseScriptCore);
end;

procedure TdxCharacterFormattingBase.SetStrikeoutColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor);
begin
  AInfo.StrikeoutColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseStrikeoutColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseStrikeoutColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetStrikeoutColor(const Value: TColor);
begin
  if (Info.StrikeoutColor = Value) and Options.UseStrikeoutColor then
    Exit;
  SetPropertyValue<TColor>(SetStrikeoutColorCore, Value, SetUseStrikeoutColorCore);
end;

procedure TdxCharacterFormattingBase.SetStrikeoutWordsOnlyCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean);
begin
  AInfo.StrikeoutWordsOnly := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseStrikeoutWordsOnlyCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseStrikeoutWordsOnly := AValue;
end;

procedure TdxCharacterFormattingBase.SetStrikeoutWordsOnly(const Value: Boolean);
begin
  if (Info.StrikeoutWordsOnly = Value) and Options.UseStrikeoutWordsOnly then
    Exit;
  SetPropertyValue<Boolean>(SetStrikeoutWordsOnlyCore, Value, SetUseStrikeoutWordsOnlyCore);
end;

procedure TdxCharacterFormattingBase.SetUnderlineColorCore(const AInfo: TdxCharacterFormattingInfo; const AValue: TColor);
begin
  AInfo.UnderlineColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseUnderlineColorCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseUnderlineColor := AValue;
end;

procedure TdxCharacterFormattingBase.SetUnderlineColor(const Value: TColor);
begin
  if (Info.UnderlineColor = Value) and Options.UseUnderlineColor then
    Exit;
  SetPropertyValue<TColor>(SetUnderlineColorCore, Value, SetUseUnderlineColorCore);
end;

procedure TdxCharacterFormattingBase.SetUnderlineWordsOnlyCore(const AInfo: TdxCharacterFormattingInfo; const AValue: Boolean);
begin
  AInfo.UnderlineWordsOnly := AValue;
end;

procedure TdxCharacterFormattingBase.SetUseUnderlineWordsOnlyCore(const AOptions: TdxCharacterFormattingOptions; const AValue: Boolean);
begin
  AOptions.UseUnderlineWordsOnly := AValue;
end;

procedure TdxCharacterFormattingBase.SetUnderlineWordsOnly(const Value: Boolean);
begin
  if (Info.UnderlineWordsOnly = Value) and Options.UseUnderlineWordsOnly then
    Exit;
  SetPropertyValue<Boolean>(SetUnderlineWordsOnlyCore, Value, SetUseUnderlineWordsOnlyCore);
end;

{ TdxCharacterProperties }

constructor TdxCharacterProperties.Create(const AOwner: IdxCharacterPropertiesContainer);
begin
  inherited Create(TdxPieceTable(GetPieceTable(AOwner)));
  FOwner := AOwner;
end;

destructor TdxCharacterProperties.Destroy;
begin
  inherited Destroy;
end;

class procedure TdxCharacterProperties.ApplyPropertiesDiff(ATarget: TdxCharacterProperties; ATargetMergedInfo,
  ASourceMergedInfo: TdxCharacterFormattingInfo);
begin
  if ATargetMergedInfo.AllCaps <> ASourceMergedInfo.AllCaps then
    ATarget.AllCaps := ASourceMergedInfo.AllCaps;

  if ATargetMergedInfo.BackColor <> ASourceMergedInfo.BackColor then
    ATarget.BackColor := ASourceMergedInfo.BackColor;

  if ATargetMergedInfo.FontBold <> ASourceMergedInfo.FontBold then
    ATarget.FontBold := ASourceMergedInfo.FontBold;

  if ATargetMergedInfo.FontItalic <> ASourceMergedInfo.FontItalic then
    ATarget.FontItalic := ASourceMergedInfo.FontItalic;

  if ATargetMergedInfo.FontName <> ASourceMergedInfo.FontName then
    ATarget.FontName := ASourceMergedInfo.FontName;

  if ATargetMergedInfo.DoubleFontSize <> ASourceMergedInfo.DoubleFontSize then
    ATarget.DoubleFontSize := ASourceMergedInfo.DoubleFontSize;

  if ATargetMergedInfo.FontStrikeoutType <> ASourceMergedInfo.FontStrikeoutType then
    ATarget.FontStrikeoutType := ASourceMergedInfo.FontStrikeoutType;

  if ATargetMergedInfo.FontUnderlineType <> ASourceMergedInfo.FontUnderlineType then
    ATarget.FontUnderlineType := ASourceMergedInfo.FontUnderlineType;

  if ATargetMergedInfo.ForeColor <> ASourceMergedInfo.ForeColor then
    ATarget.ForeColor := ASourceMergedInfo.ForeColor;

  if ATargetMergedInfo.Hidden <> ASourceMergedInfo.Hidden then
    ATarget.Hidden := ASourceMergedInfo.Hidden;

  if ATargetMergedInfo.Script <> ASourceMergedInfo.Script then
    ATarget.Script := ASourceMergedInfo.Script;

  if ATargetMergedInfo.StrikeoutColor <> ASourceMergedInfo.StrikeoutColor then
    ATarget.StrikeoutColor := ASourceMergedInfo.StrikeoutColor;

  if ATargetMergedInfo.StrikeoutWordsOnly <> ASourceMergedInfo.StrikeoutWordsOnly then
    ATarget.StrikeoutWordsOnly := ASourceMergedInfo.StrikeoutWordsOnly;

  if ATargetMergedInfo.UnderlineColor <> ASourceMergedInfo.UnderlineColor then
    ATarget.UnderlineColor := ASourceMergedInfo.UnderlineColor;

  if ATargetMergedInfo.UnderlineWordsOnly <> ASourceMergedInfo.UnderlineWordsOnly then
    ATarget.UnderlineWordsOnly := ASourceMergedInfo.UnderlineWordsOnly;

  if ATargetMergedInfo.NoProof <> ASourceMergedInfo.NoProof then
    ATarget.NoProof := ASourceMergedInfo.NoProof;
end;

procedure TdxCharacterProperties.CopyFrom(const AProperties: TdxMergedCharacterProperties);
var
  AInfo: TdxCharacterFormattingBase;
  AIsDeferredInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AInfo.CopyFrom(AProperties.Info, AProperties.Options);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
end;

procedure TdxCharacterProperties.Merge(const AProperties: TdxCharacterProperties);
var
  AMerger: TdxCharacterPropertiesMerger;
begin
  AMerger := TdxCharacterPropertiesMerger.Create(Self);
  try
    AMerger.Merge(AProperties);
    CopyFrom(AMerger.MergedProperties);
  finally
    AMerger.Free;
  end;
end;

procedure TdxCharacterProperties.Reset;
var
  AInfo: TdxCharacterFormattingBase;
  AEmptyInfo: TdxCharacterFormattingBase;
  AIsDeferredInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AEmptyInfo := GetCache(DocumentModel)[TdxCharacterFormattingCache.EmptyCharacterFormattingIndex];
  AInfo.ReplaceInfo(AEmptyInfo.Info, AEmptyInfo.Options);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
end;

procedure TdxCharacterProperties.ResetAllUse;
var
  AInfo: TdxCharacterFormattingBase;
  AOptions: TdxCharacterFormattingOptions;
  AFormattingInfo: TdxCharacterFormattingInfo;
  AIsDeferredInfo, AIsDeferredOptions, AIsDefferedFormattingInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AOptions := AInfo.GetOptionsForModification(AIsDeferredOptions);
  AOptions.FValue := [];
  AFormattingInfo := AInfo.GetInfoForModification(AIsDefferedFormattingInfo);
  AInfo.ReplaceInfo(AFormattingInfo, AOptions);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
  if not AIsDeferredOptions then AOptions.Free;
  if not AIsDefferedFormattingInfo then AFormattingInfo.Free;
end;

procedure TdxCharacterProperties.ResetUse(const AResetOption: TdxUsedCharacterFormattingOption);
var
  AOptions: TdxUsedCharacterFormattingOptions;
begin
  AOptions := [];
  Include(AOptions, AResetOption);
  ResetUse(AOptions);
end;

procedure TdxCharacterProperties.ResetUse(const AResetOptions: TdxUsedCharacterFormattingOptions);
var
  AInfo: TdxCharacterFormattingBase;
  AOptions: TdxCharacterFormattingOptions;
  AFormattingInfo: TdxCharacterFormattingInfo;
  AIsDeferredInfo, AIsDeferredOptions, AIsDefferedFormattingInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AOptions := AInfo.GetOptionsForModification(AIsDeferredOptions);
  AOptions.ResetUse(AResetOptions);
  AFormattingInfo := AInfo.GetInfoForModification(AIsDefferedFormattingInfo);
  AInfo.ReplaceInfo(AFormattingInfo, AOptions);
  ReplaceInfo(AInfo, GetBatchUpdateChangeActions);
  if not AIsDeferredInfo then AInfo.Free;
  if not AIsDeferredOptions then AOptions.Free;
  if not AIsDefferedFormattingInfo then AFormattingInfo.Free;
end;

function TdxCharacterProperties.GetBatchUpdateChangeActions: TdxDocumentModelChangeActions;
begin
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.BatchUpdate);
end;

function TdxCharacterProperties.GetObtainAffectedRangeListener: IdxObtainAffectedRangeListener;
begin
  if not Supports(FOwner, IdxObtainAffectedRangeListener, Result) then
    Result := nil;
end;

procedure TdxCharacterProperties.CopyFrom(const ACharacterProperties: TdxMergedProperties<TdxCharacterFormattingInfo, TdxCharacterFormattingOptions>);
var
  AInfo: TdxCharacterFormattingBase;
  AIsDeferredInfo: Boolean;
begin
  AInfo := GetInfoForModification(AIsDeferredInfo);
  AInfo.CopyFromCore(ACharacterProperties.Info, ACharacterProperties.Options);
  if not AIsDeferredInfo then AInfo.Free;
end;

function TdxCharacterProperties.CreateIndexChangedHistoryItem: TdxIndexChangedHistoryItemCore<TdxDocumentModelChangeActions>;
begin
  Result := FOwner.CreateCharacterPropertiesChangedHistoryItem;
end;

function TdxCharacterProperties.GetCache(const ADocumentModel: TdxCustomDocumentModel): TdxUniqueItemsCache<TdxCharacterFormattingBase>;
begin
  Result := TdxDocumentModel(DocumentModel).Cache.CharacterFormattingCache;
end;

procedure TdxCharacterProperties.OnIndexChanged;
begin
  Owner.OnCharacterPropertiesChanged;
end;

function TdxCharacterProperties.GetPieceTable(const AOwner: IdxCharacterPropertiesContainer): TObject;
begin
  Result := AOwner.PieceTable;
end;

function TdxCharacterProperties.GetUseFontName: Boolean;
begin
  Result := Info.Options.UseFontName;
end;

function TdxCharacterProperties.GetUseDoubleFontSize: Boolean;
begin
  Result := Info.Options.UseDoubleFontSize;
end;

function TdxCharacterProperties.GetUseFontBold: Boolean;
begin
  Result := Info.Options.UseFontBold;
end;

function TdxCharacterProperties.GetUseFontItalic: Boolean;
begin
  Result := Info.Options.UseFontItalic;
end;

function TdxCharacterProperties.GetUseFontStrikeoutType: Boolean;
begin
  Result := Info.Options.UseFontStrikeoutType;
end;

function TdxCharacterProperties.GetUseFontUnderlineType: Boolean;
begin
  Result := Info.Options.UseFontUnderlineType;
end;

function TdxCharacterProperties.GetUseAllCaps: Boolean;
begin
  Result := Info.Options.UseAllCaps;
end;

function TdxCharacterProperties.GetUseForeColor: Boolean;
begin
  Result := Info.Options.UseForeColor;
end;

function TdxCharacterProperties.GetUseBackColor: Boolean;
begin
  Result := Info.Options.UseBackColor;
end;

function TdxCharacterProperties.GetUseUnderlineColor: Boolean;
begin
  Result := Info.Options.UseUnderlineColor;
end;

function TdxCharacterProperties.GetUseStrikeoutColor: Boolean;
begin
  Result := Info.Options.UseStrikeoutColor;
end;

function TdxCharacterProperties.GetUseUnderlineWordsOnly: Boolean;
begin
  Result := Info.Options.UseUnderlineWordsOnly;
end;

function TdxCharacterProperties.GetUseStrikeoutWordsOnly: Boolean;
begin
  Result := Info.Options.UseStrikeoutWordsOnly;
end;

function TdxCharacterProperties.GetUseScript: Boolean;
begin
  Result := Info.Options.UseScript;
end;

function TdxCharacterProperties.GetUseHidden: Boolean;
begin
  Result := Info.Options.UseHidden;
end;

function TdxCharacterProperties.GetAllCaps: Boolean;
begin
  Result := Info.AllCaps;
end;

function TdxCharacterProperties.GetBackColor: TColor;
begin
  Result := Info.BackColor;
end;

function TdxCharacterProperties.GetFontBold: Boolean;
begin
  Result := Info.FontBold;
end;

function TdxCharacterProperties.GetFontItalic: Boolean;
begin
  Result := Info.FontItalic;
end;

function TdxCharacterProperties.GetFontName: string;
begin
  Result := Info.FontName;
end;

function TdxCharacterProperties.GetDoubleFontSize: Integer;
begin
  Result := Info.DoubleFontSize;
end;

function TdxCharacterProperties.GetFontStrikeoutType: TdxStrikeoutType;
begin
  Result := Info.FontStrikeoutType;
end;

function TdxCharacterProperties.GetFontUnderlineType: TdxUnderlineType;
begin
  Result := Info.FontUnderlineType;
end;

function TdxCharacterProperties.GetForeColor: TColor;
begin
  Result := Info.ForeColor;
end;

function TdxCharacterProperties.GetHidden: Boolean;
begin
  Result := Info.Hidden;
end;

function TdxCharacterProperties.GetNoProof: Boolean;
begin
  Result := Info.NoProof;
end;

function TdxCharacterProperties.GetScript: TdxCharacterFormattingScript;
begin
  Result := Info.Script;
end;

function TdxCharacterProperties.GetStrikeoutColor: TColor;
begin
  Result := Info.StrikeoutColor;
end;

function TdxCharacterProperties.GetStrikeoutWordsOnly: Boolean;
begin
  Result := Info.StrikeoutWordsOnly;
end;

function TdxCharacterProperties.GetUnderlineColor: TColor;
begin
  Result := Info.UnderlineColor;
end;

function TdxCharacterProperties.GetUnderlineWordsOnly: Boolean;
begin
  Result := Info.UnderlineWordsOnly;
end;

procedure TdxCharacterProperties.SetAllCaps(const Value: Boolean);
begin
  SetPropertyValue<Boolean>(SetAllCapsCore, Value);
end;

function TdxCharacterProperties.SetAllCapsCore(const AInfo: TdxCharacterFormattingBase; const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.AllCaps := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.AllCaps);
end;

procedure TdxCharacterProperties.SetBackColor(const Value: TColor);
begin
  SetPropertyValue<Integer>(SetBackColorCore, Value);
end;

function TdxCharacterProperties.SetBackColorCore(const AInfo: TdxCharacterFormattingBase; const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.BackColor := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.BackColor);
end;

procedure TdxCharacterProperties.SetFontBold(const Value: Boolean);
begin
  SetPropertyValue<Boolean>(SetFontBoldCore, Value);
end;

function TdxCharacterProperties.SetFontBoldCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.FontBold := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.FontBold);
end;

procedure TdxCharacterProperties.SetFontItalic(const Value: Boolean);
begin
  SetPropertyValue<Boolean>(SetFontItalicCore, Value);
end;

function TdxCharacterProperties.SetFontItalicCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.FontItalic := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.FontItalic);
end;

procedure TdxCharacterProperties.SetFontName(const Value: string);
begin
  SetPropertyValue<string>(SetFontNameCore, Value);
end;

function TdxCharacterProperties.SetFontNameCore(const AInfo: TdxCharacterFormattingBase;
  const Value: string): TdxDocumentModelChangeActions;
begin
  AInfo.FontName := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.FontName);
end;

procedure TdxCharacterProperties.SetDoubleFontSize(const Value: Integer);

  procedure Error;
  begin
    raise Exception.Create('DoubleFontSize < 0');
  end;

begin
  if Value < 0 then
    Error;
  SetPropertyValue<Integer>(SetDoubleFontSizeCore, Value);
end;

function TdxCharacterProperties.SetDoubleFontSizeCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.DoubleFontSize := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.FontSize);
end;

procedure TdxCharacterProperties.SetFontStrikeoutType(const Value: TdxStrikeoutType);
begin
  SetPropertyValue<Integer>(SetFontStrikeoutTypeCore, Ord(Value));
end;

function TdxCharacterProperties.SetFontStrikeoutTypeCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.FontStrikeoutType := TdxStrikeoutType(Value);
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.FontStrikeoutType);
end;

procedure TdxCharacterProperties.SetFontUnderlineType(const Value: TdxUnderlineType);
begin
  SetPropertyValue<Integer>(SetFontUnderlineTypeCore, Ord(Value));
end;

function TdxCharacterProperties.SetFontUnderlineTypeCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.FontUnderlineType := TdxUnderlineType(Value);
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.FontUnderlineType);
end;

procedure TdxCharacterProperties.SetForeColor(const Value: TColor);
begin
  SetPropertyValue<Integer>(SetForeColorCore, Value);
end;

function TdxCharacterProperties.SetForeColorCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.ForeColor := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.ForeColor);
end;

procedure TdxCharacterProperties.SetHidden(const Value: Boolean);
begin
  SetPropertyValue<Boolean>(SetHiddenCore, Value);
end;

function TdxCharacterProperties.SetHiddenCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.Hidden := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.Hidden);
end;

procedure TdxCharacterProperties.SetNoProof(const Value: Boolean);
begin
  SetPropertyValue<Boolean>(SetNoProofCore, Value);
end;

function TdxCharacterProperties.SetNoProofCore(const AInfo: TdxCharacterFormattingBase;
  const AValue: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.NoProof := AValue;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.NoProof);
end;

procedure TdxCharacterProperties.SetScript(const Value: TdxCharacterFormattingScript);
begin
  SetPropertyValue<Integer>(SetScriptCore, Ord(Value));
end;

function TdxCharacterProperties.SetScriptCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.Script := TdxCharacterFormattingScript(Value);
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.Script);
end;

procedure TdxCharacterProperties.SetStrikeoutColor(const Value: TColor);
begin
  SetPropertyValue<Integer>(SetStrikeoutColorCore, Value);
end;

function TdxCharacterProperties.SetStrikeoutColorCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.StrikeoutColor := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.StrikeoutColor);
end;

procedure TdxCharacterProperties.SetStrikeoutWordsOnly(const Value: Boolean);
begin
  SetPropertyValue<Boolean>(SetStrikeoutWordsOnlyCore, Value);
end;

function TdxCharacterProperties.SetStrikeoutWordsOnlyCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.StrikeoutWordsOnly := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.StrikeoutWordsOnly);
end;

procedure TdxCharacterProperties.SetUnderlineColor(const Value: TColor);
begin
  SetPropertyValue<Integer>(SetUnderlineColorCore, Value);
end;

function TdxCharacterProperties.SetUnderlineColorCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Integer): TdxDocumentModelChangeActions;
begin
  AInfo.UnderlineColor := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.UnderlineColor);
end;

procedure TdxCharacterProperties.SetUnderlineWordsOnly(const Value: Boolean);
begin
  SetPropertyValue<Boolean>(SetUnderlineWordsOnlyCore, Value);
end;

function TdxCharacterProperties.SetUnderlineWordsOnlyCore(const AInfo: TdxCharacterFormattingBase;
  const Value: Boolean): TdxDocumentModelChangeActions;
begin
  AInfo.UnderlineWordsOnly := Value;
  Result := TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(TdxCharacterFormattingChangeType.UnderlineWordsOnly);
end;

function TdxCharacterProperties.UseVal(AMask: TdxUsedCharacterFormattingOption): Boolean;
begin
  Result := AMask in Info.Options.Value;
end;

{ TdxCharacterPropertiesMerger }

constructor TdxCharacterPropertiesMerger.Create(const AProperties: TdxCharacterProperties);
var
  AInfo: TdxCharacterFormattingBase;
begin
  AInfo := AProperties.Info;
  inherited Create(TdxMergedCharacterProperties.Create(AInfo.Info, AInfo.Options));
end;

procedure TdxCharacterPropertiesMerger.Merge(const AProperties: TdxCharacterProperties);
var
  AInfo: TdxCharacterFormattingBase;
begin
  AInfo := AProperties.Info;
  MergeCore(AInfo.Info, AInfo.Options);
end;

procedure TdxCharacterPropertiesMerger.MergeCore(const AInfo: TdxCharacterFormattingInfo; const AOptions: TdxCharacterFormattingOptions);
var
  AOwnInfo: TdxCharacterFormattingInfo;
  AOwnOptions: TdxCharacterFormattingOptions;
begin
  AOwnInfo := OwnInfo;
  AOwnOptions := OwnOptions;
  if not AOwnOptions.UseAllCaps and AOptions.UseAllCaps then
  begin
    AOwnInfo.AllCaps := AInfo.AllCaps;
    AOwnOptions.UseAllCaps := True;
  end;
  if not AOwnOptions.UseBackColor and AOptions.UseBackColor then
  begin
    AOwnInfo.BackColor := AInfo.BackColor;
    AOwnOptions.UseBackColor := True;
  end;
  if not AOwnOptions.UseFontBold and AOptions.UseFontBold then
  begin
    AOwnInfo.FontBold := AInfo.FontBold;
    AOwnOptions.UseFontBold := True;
  end;
  if not AOwnOptions.UseFontItalic and AOptions.UseFontItalic then
  begin
    AOwnInfo.FontItalic := AInfo.FontItalic;
    AOwnOptions.UseFontItalic := True;
  end;
  if not AOwnOptions.UseFontName and AOptions.UseFontName then
  begin
    AOwnInfo.FontName := AInfo.FontName;
    AOwnOptions.UseFontName := True;
  end;
  if not AOwnOptions.UseDoubleFontSize and AOptions.UseDoubleFontSize then
  begin
    AOwnInfo.DoubleFontSize := AInfo.DoubleFontSize;
    AOwnOptions.UseDoubleFontSize := True;
  end;
  if not AOwnOptions.UseFontStrikeoutType and AOptions.UseFontStrikeoutType then
  begin
    AOwnInfo.FontStrikeoutType := AInfo.FontStrikeoutType;
    AOwnOptions.UseFontStrikeoutType := True;
  end;
  if not AOwnOptions.UseFontUnderlineType and AOptions.UseFontUnderlineType then
  begin
    AOwnInfo.FontUnderlineType := AInfo.FontUnderlineType;
    AOwnOptions.UseFontUnderlineType := True;
  end;
  if not AOwnOptions.UseForeColor and AOptions.UseForeColor then
  begin
    AOwnInfo.ForeColor := AInfo.ForeColor;
    AOwnOptions.UseForeColor := True;
  end;
  if not AOwnOptions.UseScript and AOptions.UseScript then
  begin
    AOwnInfo.Script := AInfo.Script;
    AOwnOptions.UseScript := True;
  end;
  if not AOwnOptions.UseStrikeoutColor and AOptions.UseStrikeoutColor then
  begin
    AOwnInfo.StrikeoutColor := AInfo.StrikeoutColor;
    AOwnOptions.UseStrikeoutColor := True;
  end;
  if not AOwnOptions.UseStrikeoutWordsOnly and AOptions.UseStrikeoutWordsOnly then
  begin
    AOwnInfo.StrikeoutWordsOnly := AInfo.StrikeoutWordsOnly;
    AOwnOptions.UseStrikeoutWordsOnly := True;
  end;
  if not AOwnOptions.UseUnderlineColor and AOptions.UseUnderlineColor then
  begin
    AOwnInfo.UnderlineColor := AInfo.UnderlineColor;
    AOwnOptions.UseUnderlineColor := True;
  end;
  if not AOwnOptions.UseUnderlineWordsOnly and AOptions.UseUnderlineWordsOnly then
  begin
    AOwnInfo.UnderlineWordsOnly := AInfo.UnderlineWordsOnly;
    AOwnOptions.UseUnderlineWordsOnly := True;
  end;
  if not AOwnOptions.UseHidden and AOptions.UseHidden then
  begin
    AOwnInfo.Hidden := AInfo.Hidden;
    AOwnOptions.UseHidden := True;
  end;
end;

{ TdxCharacterFormattingInfoCache }

function TdxCharacterFormattingInfoCache.CreateDefaultItem(const AUnitConverter: IdxDocumentModelUnitConverter): TdxCharacterFormattingInfo;
begin
  Result := TdxCharacterFormattingInfo.Create;
  Result.FontName := TdxRichEditControlCompatibility.DefaultFontName;
  Result.DoubleFontSize := TdxRichEditControlCompatibility.DefaultDoubleFontSize;
end;

{ TdxCharacterFormattingCache }

constructor TdxCharacterFormattingCache.Create(const ADocumentModel: TdxCustomDocumentModel);
begin
  inherited Create(TdxDocumentModel(ADocumentModel).UnitConverter);
  FDocumentModel := ADocumentModel;
  AppendItem(TdxCharacterFormattingBase.Create(TdxDocumentModel(DocumentModel).MainPieceTable, DocumentModel,
    TdxCharacterFormattingInfoCache.DefaultItemIndex,
    TdxCharacterFormattingOptionsCache.EmptyCharacterFormattingOptionIndex));
  AppendItem(TdxCharacterFormattingBase.Create(TdxDocumentModel(DocumentModel).MainPieceTable, DocumentModel,
    TdxCharacterFormattingInfoCache.DefaultItemIndex,
    TdxCharacterFormattingOptionsCache.RootCharacterFormattingOptionIndex));
end;

function TdxCharacterFormattingCache.CreateDefaultItem(
  const AUnitConverter: IdxDocumentModelUnitConverter): TdxCharacterFormattingBase;
begin
  Result := nil;
end;

{ TdxCharacterFormattingChangeActionsCalculator }

class function TdxCharacterFormattingChangeActionsCalculator.CalculateChangeActions(
  const AChange: TdxCharacterFormattingChangeType): TdxDocumentModelChangeActions;
const
  ACharacterFormattingChangeActionsTable: array[TdxCharacterFormattingChangeType] of TdxDocumentModelChangeActions =
    (
      [], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck,
       TdxDocumentModelChangeAction.SplitRunByCharset], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting,
       TdxDocumentModelChangeAction.ResetSecondaryLayout], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSecondaryLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetSecondaryLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck,
       TdxDocumentModelChangeAction.SplitRunByCharset], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck,
       TdxDocumentModelChangeAction.ValidateSelectionInterval], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck,
       TdxDocumentModelChangeAction.ValidateSelectionInterval], 
      [TdxDocumentModelChangeAction.Redraw, TdxDocumentModelChangeAction.ResetPrimaryLayout,
       TdxDocumentModelChangeAction.ResetSecondaryLayout, TdxDocumentModelChangeAction.ResetSelectionLayout,
       TdxDocumentModelChangeAction.ResetCaretInputPositionFormatting, TdxDocumentModelChangeAction.ResetSpellingCheck,
       TdxDocumentModelChangeAction.ValidateSelectionInterval] 
    );
begin
  Result := ACharacterFormattingChangeActionsTable[AChange];
end;

class procedure TdxCharacterPropertiesFontAssignmentHelper.AssignFont(ACharacterProperties: IdxCharacterProperties; AFont: TFont);
begin
  ACharacterProperties.FontName := AFont.Name;
  ACharacterProperties.DoubleFontSize := Round(AFont.Size * 2); 
  ACharacterProperties.FontBold := fsBold in AFont.Style;
  ACharacterProperties.FontItalic := fsItalic in AFont.Style;
  if fsUnderline in AFont.Style then 
    ACharacterProperties.FontUnderlineType := TdxUnderlineType.Single
  else
    ACharacterProperties.FontUnderlineType := TdxUnderlineType.None;
  if fsStrikeOut in AFont.Style then 
    ACharacterProperties.FontStrikeoutType := TdxStrikeoutType.Single
  else
    ACharacterProperties.FontStrikeoutType := TdxStrikeoutType.None;
end;

{ TdxParagraphMergedCharacterPropertiesCachedResult }

constructor TdxParagraphMergedCharacterPropertiesCachedResult.Create;
begin
  inherited Create;
  FParagraphStyleIndex := -1;
end;

destructor TdxParagraphMergedCharacterPropertiesCachedResult.Destroy;
begin
  FreeAndNil(FMergedCharacterProperties);
  inherited Destroy;
end;

procedure TdxParagraphMergedCharacterPropertiesCachedResult.SetMergedCharacterProperties(
  const Value: TdxMergedCharacterProperties);
begin
  if FMergedCharacterProperties <> Value then
  begin
    FMergedCharacterProperties.Free;
    FMergedCharacterProperties := Value;
  end;
end;

{ TdxRunMergedCharacterPropertiesCachedResult }

constructor TdxRunMergedCharacterPropertiesCachedResult.Create;
begin
  inherited Create;
  FCharacterStyleIndex := -1;
  FCharacterPropertiesIndex := -1;
end;

end.
