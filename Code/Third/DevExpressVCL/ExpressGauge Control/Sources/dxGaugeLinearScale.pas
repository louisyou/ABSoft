{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeLinearScale;

{$I cxVer.inc}

interface

uses
  Classes, Graphics, dxCore, cxGeometry, cxGraphics, dxXMLDoc, dxGDIPlusClasses, dxCompositeShape, dxGaugeCustomScale,
  dxGaugeQuantitativeScale;

type
  TdxGaugeCustomLinearScale = class;
  TdxGaugeLinearScaleAlignElements = TLeftRight;
  TdxGaugeLinearScaleLabelOrientation = TdxTextOrientation;

  { TdxGaugeLinearScaleDefaultParameters }

  TdxGaugeLinearScaleDefaultParameters = class(TdxGaugeQuantitativeScaleDefaultParameters)
  public
    LevelBarStartPosition: TdxPointF;
    LevelBarEndPosition: TdxPointF;
    LevelBarsScaleFactor: TdxPointF;
    RotateTicks: Boolean;
  end;

  { TdxGaugeLinearScaleParameters }

  TdxGaugeLinearScaleParameters = class(TdxGaugeQuantitativeScaleParameters)
  public
    AlignElements: TdxGaugeLinearScaleAlignElements;
    LabelOrientation: TdxGaugeLinearScaleLabelOrientation;
    RotationAngle: TcxRotationAngle;
  end;

  { TdxGaugeLinearRangeParameters }

  TdxGaugeLinearRangeParameters = class(TdxGaugeCustomRangeParameters)
  public
    CenterPositionFactor: Single;
    LevelBarLine: TdxRectF;
  end;

  { TdxGaugeLinearScaleOptionsView }

  TdxGaugeLinearScaleOptionsView = class(TdxGaugeQuantitativeScaleOptionsView)
  private
    function GetAlignElements: TdxGaugeLinearScaleAlignElements;
    function GetLabelOrientation: TdxGaugeLinearScaleLabelOrientation;
    function GetRotationAngle: TcxRotationAngle;
    procedure SetAlignElements(const AValue: TdxGaugeLinearScaleAlignElements);
    procedure SetLabelOrientation(const AValue: TdxGaugeLinearScaleLabelOrientation);
    procedure SetRotationAngle(const AValue: TcxRotationAngle);

    function GetScale: TdxGaugeCustomLinearScale;
  published
    property AlignElements: TdxGaugeLinearScaleAlignElements read GetAlignElements write SetAlignElements
      default taLeftJustify;
    property LabelOrientation: TdxGaugeLinearScaleLabelOrientation read GetLabelOrientation write SetLabelOrientation
      default toLeftToRight;
    property ShowLevelBar: Boolean read GetShowValueIndicator write SetShowValueIndicator default True;
    property RotationAngle: TcxRotationAngle read GetRotationAngle write SetRotationAngle default raMinus90;
  end;

  { TdxGaugeCustomLinearScaleViewInfo }

  TdxGaugeCustomLinearScaleViewInfo = class(TdxGaugeQuantitativeScaleViewInfo)
  private
    FBarStart: TdxCompositeShape;
    FBarEmpty: TdxCompositeShape;
    FBarEnd: TdxCompositeShape;
    FBarPacked: TdxCompositeShape;

    FLevelBarLine: TdxRectF;

    function GetScaleDefaultParameters: TdxGaugeLinearScaleDefaultParameters;
    function GetScaleParameters: TdxGaugeLinearScaleParameters;
    function ValueToPoint(AValue: Single): TdxPointF;
    procedure CalculateLevelBarLine;

    function GetLabelTextAlignment: TAlignment;
    function GetLabelTextRotationAngle: Single;
    function GetRotationAngle: Single;
    function GetScaledImageRect(AImage: TGraphic): TdxRectF;
    function NeedRotateBounds: Boolean;
    procedure RotateContent(AGPGraphics: TdxGPGraphics; AAngle: Single; const APivotPoint: TdxPointF);

    procedure DrawLevelBarEnd(AGPGraphics: TdxGPGraphics);
    procedure DrawLevelBarMiddle(AGPGraphics: TdxGPGraphics);
    procedure DrawLevelBarStart(AGPGraphics: TdxGPGraphics);
    procedure DrawLevelBarBackground(AGPGraphics: TdxGPGraphics);
  protected
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;

    function GetContentBounds: TdxRectF; override;
    function GetContentOriginalSize: TdxSizeF; override;
    function GetMajorTickStartPosition: Single; override;
    function GetMajorTicksPositionStep: Single; override;
    function GetMaxLabelValue: Single; override;
    function GetMinorTicksPositionStep: Single; override;
    procedure CalculateReferenceParameter; override;
    procedure CalculateScaleFactor; override;
    procedure DoDrawValueIndicator(AGPGraphics: TdxGPGraphics); override;
    procedure DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single); override;
    procedure DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single;
      const ATickDrawParameters: TdxGaugeScaleTickDrawParameters); override;
    procedure DrawBackground(AGPGraphics: TdxGPGraphics); override;
    procedure DrawElements(AGPGraphics: TdxGPGraphics); override;
    procedure LoadScaleElements; override;

    procedure BeginDraw(AGPGraphics: TdxGPGraphics);
    procedure EndDraw(AGPGraphics: TdxGPGraphics);

    property LevelBarLine: TdxRectF read FLevelBarLine;
  end;

  { TdxGaugeCustomLinearScale }

  TdxGaugeCustomLinearScale = class(TdxGaugeQuantitativeScale)
  private
    function GetAlignElements: TdxGaugeLinearScaleAlignElements;
    function GetLabelOrientation: TdxGaugeLinearScaleLabelOrientation;
    function GetOptionsView: TdxGaugeLinearScaleOptionsView;
    function GetRotationAngle: TcxRotationAngle;
    procedure SetAlignElements(const AValue: TdxGaugeLinearScaleAlignElements);
    procedure SetLabelOrientation(const AValue: TdxGaugeLinearScaleLabelOrientation);
    procedure SetOptionsView(const AValue: TdxGaugeLinearScaleOptionsView);
    procedure SetRotationAngle(const AValue: TcxRotationAngle);

    function GetScaleParameters: TdxGaugeLinearScaleParameters;
    function GetViewInfo: TdxGaugeCustomLinearScaleViewInfo;
  protected
    class function GetLayerCount: Integer; override;
    class function GetScaleName: string; override;
    class function GetScaleType: TdxGaugeScaleType; override;

    function GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass; override;
    function GetRangeClass: TdxGaugeCustomRangeClass; override;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); override;
    procedure InitScaleParameters; override;
    procedure PopulateRangeScaleInfo(ARangeIndex: Integer); override;

    property OptionsView: TdxGaugeLinearScaleOptionsView read GetOptionsView write SetOptionsView;
  end;

  { TdxGaugeLinearScale }

  TdxGaugeLinearScale = class(TdxGaugeCustomLinearScale)
  published
    property AnchorScaleIndex;
    property OptionsLayout;
    property OptionsView;
    property StyleName;
    property Visible;

    property Ranges;
    property Value;
  end;

  { TdxGaugeCustomLinearScaleStyleReader }

  TdxGaugeCustomLinearScaleStyleReader = class(TdxGaugeQuantitativeScaleStyleReader)
  protected
    class function GetResourceNamePreffix: string; override;
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadCommonScaleParameters(ANode: TdxXMLNode;
      AParameters: TdxGaugeQuantitativeScaleDefaultParameters); override;
  end;

  { TdxGaugeLinearScaleRangeViewInfo }

  TdxGaugeLinearScaleRangeViewInfo = class(TdxGaugeCustomRangeViewInfo)
  private
    function GetRangeParameters: TdxGaugeLinearRangeParameters;
  protected
    function GetRangeParametersClass: TdxGaugeCustomRangeParametersClass; override;
    procedure CalculateBounds; override;
    procedure DoDraw(AGPGraphics: TdxGPGraphics); override;
  end;

  { TdxGaugeLinearScaleRange }

  TdxGaugeLinearScaleRange = class(TdxGaugeCustomRange)
  private
    function GetCenterPositionFactor: Single;
    procedure SetCenterPositionFactor(const AValue: Single);

    function GetRangeParameters: TdxGaugeLinearRangeParameters;

    procedure ReadCenterPositionFactor(AReader: TReader);
    procedure WriteCenterPositionFactor(AWriter: TWriter);

    function IsCenterPositionFactorStored: Boolean;
  protected
    function GetRangeParametersClass: TdxGaugeCustomRangeParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomRangeViewInfoClass; override;
    procedure DefineProperties(AFiler: TFiler); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property CenterPositionFactor: Single read GetCenterPositionFactor write SetCenterPositionFactor
      stored IsCenterPositionFactorStored;
    property Color;
    property ValueEnd;
    property ValueStart;
    property Visible;
    property WidthFactor;
  end;

implementation

uses
  Math, dxGaugeUtils, dxCoreGraphics;

const
  dxGaugeLinearRangeCenterPositionFactor = 0.5;

type
  TdxGaugeScaleStyleAccess = class(TdxGaugeScaleStyle);

function dxGaugeLinearScaleValueToPoint(AValue, AMinValue: Single; const ALevelBarLine: TdxRectF;
  AValueRange: Single): TdxPointF;
begin
  Result.X := ALevelBarLine.Left;
  Result.Y := ALevelBarLine.Bottom - (AValue - AMinValue) * ALevelBarLine.Height / AValueRange;
end;

{ TdxGaugeLinearScaleOptionsView }

function TdxGaugeLinearScaleOptionsView.GetAlignElements: TdxGaugeLinearScaleAlignElements;
begin
  Result := GetScale.GetAlignElements;
end;

function TdxGaugeLinearScaleOptionsView.GetLabelOrientation: TdxGaugeLinearScaleLabelOrientation;
begin
  Result := GetScale.GetLabelOrientation;
end;

function TdxGaugeLinearScaleOptionsView.GetRotationAngle: TcxRotationAngle;
begin
  Result := GetScale.GetRotationAngle;
end;

procedure TdxGaugeLinearScaleOptionsView.SetAlignElements(const AValue: TdxGaugeLinearScaleAlignElements);
begin
  GetScale.SetAlignElements(AValue);
end;

procedure TdxGaugeLinearScaleOptionsView.SetLabelOrientation(const AValue: TdxGaugeLinearScaleLabelOrientation);
begin
  GetScale.SetLabelOrientation(AValue);
end;

procedure TdxGaugeLinearScaleOptionsView.SetRotationAngle(const AValue: TcxRotationAngle);
begin
  GetScale.SetRotationAngle(AValue);
end;

function TdxGaugeLinearScaleOptionsView.GetScale: TdxGaugeCustomLinearScale;
begin
  Result := inherited GetScale as TdxGaugeCustomLinearScale;
end;

{ TdxGaugeCustomLinearScaleViewInfo }

function TdxGaugeCustomLinearScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeLinearScaleParameters;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetContentBounds: TdxRectF;
begin
  Result := GetScaleRect(GetElementImageOriginalSize(FBackground));
  if NeedRotateBounds then
    Result := GetScaleRect(dxSizeF(Result.Height, Result.Width), False);
end;

function TdxGaugeCustomLinearScaleViewInfo.GetContentOriginalSize: TdxSizeF;
begin
  Result := GetElementImageOriginalSize(FBackground);
  if NeedRotateBounds then
    Result := dxSizeF(Result.cy, Result.cx);
end;

function TdxGaugeCustomLinearScaleViewInfo.GetMajorTickStartPosition: Single;
begin
  Result := FLevelBarLine.Bottom;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetMajorTicksPositionStep: Single;
begin
  Result := GetMajorTickStartPosition - ValueToPoint(GetValueStep + GetScaleParameters.MinValue).Y;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetMaxLabelValue: Single;
begin
  Result := GetMaxLenghtLabelValue;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetMinorTicksPositionStep: Single;
begin
  Result := GetMajorTicksPositionStep / GetMinorTickCount;
end;

procedure TdxGaugeCustomLinearScaleViewInfo.CalculateReferenceParameter;
begin
  CalculateLevelBarLine;
end;

procedure TdxGaugeCustomLinearScaleViewInfo.CalculateScaleFactor;
begin
  inherited CalculateScaleFactor;
  if NeedRotateBounds then
    FScaleFactor := dxPointF(FScaleFactor.Y, FScaleFactor.X);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DoDrawValueIndicator(AGPGraphics: TdxGPGraphics);

  procedure DrawValueBar;
  var
    ABarBounds: TdxRectF;
  begin
    ABarBounds := GetScaledImageRect(FBarPacked);
    ABarBounds.Bottom := FLevelBarLine.Bottom + 1;
    ABarBounds.Top := ValueToPoint(GetScaleValue).Y;
    dxGaugeDrawImage(AGPGraphics, FBarPacked, ABarBounds);
  end;

begin
  DrawLevelBarBackground(AGPGraphics);
  DrawValueBar;
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single);
const
  ElementOffsetSing: array[TdxGaugeLinearScaleAlignElements] of Integer = (-1, 1);
var
  P: TdxPointF;
  R: TdxRectF;
begin
  P := dxPointF(FLevelBarLine.Left + ElementOffsetSing[GetScaleParameters.AlignElements] *
    (GetScaleDefaultParameters.LabelOffset * FScaleFactor.X + GetMaxLabelSize.cx / 2), AOffset);
  R := cxRectInflate(cxRectF(P, P), GetMaxLabelSize.cx / 2, GetMaxLabelSize.cy / 2);
  dxGaugeDrawText(AGPGraphics, GetLabelText(AValue), R, Font, GetLabelTextRotationAngle, GetLabelTextAlignment);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single;
  const ATickDrawParameters: TdxGaugeScaleTickDrawParameters);

  function GetTickRect(const ASize: TdxSizeF): TdxRectF;
  begin
    if GetScaleParameters.AlignElements = taRightJustify then
      Result.Left := FLevelBarLine.Left + ATickDrawParameters.Offset * FScaleFactor.X
    else
      Result.Left := FLevelBarLine.Left - ATickDrawParameters.Offset * FScaleFactor.X - ASize.cx;
    Result.Right := Result.Left + ASize.cx;
    Result.Top := ValueToPoint(AValue).Y - ASize.cy / 2;
    Result.Bottom := Result.Top + ASize.cy;
  end;

const
  TickRotationAngleMap: array[TdxGaugeLinearScaleAlignElements] of Single = (180, 0);
var
  ATickRect: TdxRectF;
begin
  ATickRect := GetTickRect(GetScaledSize(ATickDrawParameters.OriginalSize));
  if ATickDrawParameters.ShowTick then
  begin
    AGPGraphics.SaveWorldTransfrom;
    if GetScaleDefaultParameters.RotateTicks then
      AGPGraphics.RotateWorldTransform(-TickRotationAngleMap[GetScaleParameters.AlignElements], cxRectCenter(ATickRect));
    dxGaugeDrawImage(AGPGraphics, ATickDrawParameters.Image, ATickRect);
    AGPGraphics.RestoreWorldTransfrom;
  end;
  if CanDrawLabel(ATickDrawParameters) then
    DrawLabel(AGPGraphics, AValue, ValueToPoint(AValue).Y);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawBackground(AGPGraphics: TdxGPGraphics);
begin
  if GetScaleParameters.ShowBackground then
    dxGaugeDrawImage(AGPGraphics, FBackground, GetScaleRect(GetElementImageOriginalSize(FBackground)));
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawElements(AGPGraphics: TdxGPGraphics);
begin
  if GetScaleParameters.ShowValueIndicator then
    DrawLevelBarBackground(AGPGraphics);
  inherited DrawElements(AGPGraphics);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.LoadScaleElements;
begin
  inherited LoadScaleElements;
  FBarStart := TdxGaugeScaleStyleAccess(Style).GetElement(etLinearBarStart);
  FBarEnd := TdxGaugeScaleStyleAccess(Style).GetElement(etLinearBarEnd);
  FBarEmpty := TdxGaugeScaleStyleAccess(Style).GetElement(etLinearBarEmpty);
  FBarPacked := TdxGaugeScaleStyleAccess(Style).GetElement(etLinearBarPacked);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.BeginDraw(AGPGraphics: TdxGPGraphics);
begin
  AGPGraphics.SaveWorldTransfrom;
  RotateContent(AGPGraphics, -GetRotationAngle, cxRectCenter(GetContentBounds));
end;

procedure TdxGaugeCustomLinearScaleViewInfo.EndDraw(AGPGraphics: TdxGPGraphics);
begin
  AGPGraphics.RestoreWorldTransfrom;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetScaleDefaultParameters: TdxGaugeLinearScaleDefaultParameters;
begin
  Result := ScaleDefaultParameters as TdxGaugeLinearScaleDefaultParameters;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetScaleParameters: TdxGaugeLinearScaleParameters;
begin
  Result := FScaleParameters as TdxGaugeLinearScaleParameters;
end;

function TdxGaugeCustomLinearScaleViewInfo.ValueToPoint(AValue: Single): TdxPointF;
begin
  Result := dxGaugeLinearScaleValueToPoint(AValue, GetScaleParameters.MinValue, FLevelBarLine, GetValueRange);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.CalculateLevelBarLine;
begin
  FLevelBarLine := GetScaleRect(dxSizeF(0, GetScaleDefaultParameters.LevelBarStartPosition.Y -
    GetScaleDefaultParameters.LevelBarEndPosition.Y));
end;

function TdxGaugeCustomLinearScaleViewInfo.GetLabelTextAlignment: TAlignment;
type
  TdxRotationAngleSet = set of TcxRotationAngle;
  function GetTextAlignment(AValidAngles: TdxRotationAngleSet; ARotationAngle: TcxRotationAngle):
    TAlignment;
  begin
    Result := taCenter;
    if GetScaleParameters.RotationAngle in AValidAngles then
      if GetScaleParameters.RotationAngle = ARotationAngle then
        Result := GetScaleParameters.AlignElements
      else
        if GetScaleParameters.AlignElements = taLeftJustify then
          Result := taRightJustify
        else
          Result := taLeftJustify;
  end;

begin
  Result := taCenter;
  case GetScaleParameters.LabelOrientation of
    toLeftToRight:
      Result := GetTextAlignment([raPlus90, raMinus90], raPlus90);
    toTopToBottom:
      Result := GetTextAlignment([ra0, ra180], ra0);
    toBottomToTop:
      Result := GetTextAlignment([ra0, ra180], ra180);
  end;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetLabelTextRotationAngle: Single;
var
  ADelta: Integer;
begin
  case GetScaleParameters.LabelOrientation of
    toTopToBottom:
      ADelta := 90;
    toBottomToTop:
      ADelta := -90;
    else
      ADelta := 0;
  end;
  Result := GetRotationAngle + ADelta;
end;

function TdxGaugeCustomLinearScaleViewInfo.GetRotationAngle: Single;
const
  RotationAngleMap: array[TcxRotationAngle] of Integer = (90, 180, 0, -90);
begin
  Result := RotationAngleMap[GetScaleParameters.RotationAngle];
end;

function TdxGaugeCustomLinearScaleViewInfo.GetScaledImageRect(AImage: TGraphic): TdxRectF;
begin
  Result := cxRectInflate(GetScaleRect(GetElementImageOriginalSize(AImage)),
    GetScaleDefaultParameters.LevelBarsScaleFactor.X * FScaleFactor.X)
end;

function TdxGaugeCustomLinearScaleViewInfo.NeedRotateBounds: Boolean;
begin
  Result := GetScaleParameters.RotationAngle in [ra0, ra180];
end;

procedure TdxGaugeCustomLinearScaleViewInfo.RotateContent(AGPGraphics: TdxGPGraphics; AAngle: Single;
  const APivotPoint: TdxPointF);
begin
  AGPGraphics.RotateWorldTransform(AAngle, APivotPoint);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawLevelBarEnd(AGPGraphics: TdxGPGraphics);
var
  ABarBounds: TdxRectF;
begin
  ABarBounds := GetScaledImageRect(FBarEnd);
  ABarBounds.Top := FLevelBarLine.Top - ABarBounds.Height;
  ABarBounds.Bottom := FLevelBarLine.Top + 1;
  dxGaugeDrawImage(AGPGraphics, FBarEnd, ABarBounds);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawLevelBarMiddle(AGPGraphics: TdxGPGraphics);
var
  ABarBounds: TdxRectF;
begin
  ABarBounds := GetScaledImageRect(FBarEmpty);
  ABarBounds.Top := FLevelBarLine.Top;
  ABarBounds.Bottom := FLevelBarLine.Bottom + 1;
  dxGaugeDrawImage(AGPGraphics, FBarEmpty, ABarBounds);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawLevelBarStart(AGPGraphics: TdxGPGraphics);
var
  ABarBounds: TdxRectF;
begin
  ABarBounds := GetScaledImageRect(FBarStart);
  ABarBounds.Bottom := FLevelBarLine.Bottom + ABarBounds.Height;
  ABarBounds.Top := FLevelBarLine.Bottom;
  dxGaugeDrawImage(AGPGraphics, FBarStart, ABarBounds);
end;

procedure TdxGaugeCustomLinearScaleViewInfo.DrawLevelBarBackground(AGPGraphics: TdxGPGraphics);
begin
  DrawLevelBarEnd(AGPGraphics);
  DrawLevelBarMiddle(AGPGraphics);
  DrawLevelBarStart(AGPGraphics);
end;

{ TdxGaugeCustomLinearScale }

class function TdxGaugeCustomLinearScale.GetLayerCount: Integer;
begin
  Result := 5;
end;

class function TdxGaugeCustomLinearScale.GetScaleName: string;
begin
  Result := 'Linear';
end;

class function TdxGaugeCustomLinearScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stLinearScale;
end;

function TdxGaugeCustomLinearScale.GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass;
begin
  Result := TdxGaugeLinearScaleOptionsView;
end;

function TdxGaugeCustomLinearScale.GetRangeClass: TdxGaugeCustomRangeClass;
begin
  Result := TdxGaugeLinearScaleRange;
end;

function TdxGaugeCustomLinearScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeLinearScaleParameters;
end;

function TdxGaugeCustomLinearScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeCustomLinearScaleViewInfo;
end;

procedure TdxGaugeCustomLinearScale.DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  GetViewInfo.BeginDraw(AGPGraphics);
  inherited DrawLayer(AGPGraphics, ALayerIndex);
  GetViewInfo.EndDraw(AGPGraphics);
end;

procedure TdxGaugeCustomLinearScale.InitScaleParameters;
begin
  inherited InitScaleParameters;
  GetScaleParameters.RotationAngle := raMinus90;
end;

procedure TdxGaugeCustomLinearScale.PopulateRangeScaleInfo(ARangeIndex: Integer);

  function GetRangeParameters: TdxGaugeLinearRangeParameters;
  begin
    Result := TdxGaugeLinearScaleRange(Ranges[ARangeIndex]).GetRangeParameters;
  end;

var
  AParameters: TdxGaugeLinearRangeParameters;
begin
  inherited PopulateRangeScaleInfo(ARangeIndex);
  AParameters := GetRangeParameters;
  AParameters.LevelBarLine := TdxGaugeCustomLinearScaleViewInfo(ViewInfo).LevelBarLine;
  if GetViewInfo.NeedRotateBounds then
    AParameters.ScaleBounds := GetViewInfo.GetScaleRect(dxSizeF(AParameters.ScaleBounds.Height,
      AParameters.ScaleBounds.Width), False);
end;

function TdxGaugeCustomLinearScale.GetAlignElements: TdxGaugeLinearScaleAlignElements;
begin
  Result := GetScaleParameters.AlignElements;
end;

function TdxGaugeCustomLinearScale.GetLabelOrientation: TdxGaugeLinearScaleLabelOrientation;
begin
  Result := GetScaleParameters.LabelOrientation;
end;

function TdxGaugeCustomLinearScale.GetOptionsView: TdxGaugeLinearScaleOptionsView;
begin
  Result := FOptionsView as TdxGaugeLinearScaleOptionsView;
end;

function TdxGaugeCustomLinearScale.GetRotationAngle: TcxRotationAngle;
begin
  Result := GetScaleParameters.RotationAngle;
end;

procedure TdxGaugeCustomLinearScale.SetAlignElements(const AValue: TdxGaugeLinearScaleAlignElements);
var
  AScaleParameters: TdxGaugeLinearScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.AlignElements <> AValue then
  begin
    AScaleParameters.AlignElements := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomLinearScale.SetLabelOrientation(const AValue: TdxGaugeLinearScaleLabelOrientation);
var
  AScaleParameters: TdxGaugeLinearScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.LabelOrientation <> AValue then
  begin
    AScaleParameters.LabelOrientation := AValue;
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeCustomLinearScale.SetOptionsView(const AValue: TdxGaugeLinearScaleOptionsView);
begin
  FOptionsView.Assign(AValue);
end;

procedure TdxGaugeCustomLinearScale.SetRotationAngle(const AValue: TcxRotationAngle);
var
  AScaleParameters: TdxGaugeLinearScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.RotationAngle <> AValue then
  begin
    AScaleParameters.RotationAngle := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

function TdxGaugeCustomLinearScale.GetScaleParameters: TdxGaugeLinearScaleParameters;
begin
  Result := ScaleParameters as TdxGaugeLinearScaleParameters;
end;

function TdxGaugeCustomLinearScale.GetViewInfo: TdxGaugeCustomLinearScaleViewInfo;
begin
  Result := ViewInfo as TdxGaugeCustomLinearScaleViewInfo;
end;

{ TdxGaugeCustomLinearScaleStyleReader }

class function TdxGaugeCustomLinearScaleStyleReader.GetResourceNamePreffix: string;
begin
  Result := 'Linear';
end;

function TdxGaugeCustomLinearScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeLinearScaleDefaultParameters;
end;

procedure TdxGaugeCustomLinearScaleStyleReader.ReadCommonScaleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  ALinearParameters: TdxGaugeLinearScaleDefaultParameters;
begin
  inherited ReadCommonScaleParameters(ANode, AParameters);
  ALinearParameters := AParameters as TdxGaugeLinearScaleDefaultParameters;
  ALinearParameters.LevelBarStartPosition := GetAttributeValueAsPointF(GetChildNode(ANode, 'LevelBarStartPosition'),
    'Value');
  ALinearParameters.LevelBarEndPosition := GetAttributeValueAsPointF(GetChildNode(ANode, 'LevelBarEndPosition'),
    'Value');
  ALinearParameters.RotateTicks := GetAttributeValueAsBoolean(GetChildNode(ANode, 'RotateTicks'), 'Value');
  ALinearParameters.LevelBarsScaleFactor := GetAttributeValueAsPointF(GetChildNode(ANode, 'LevelBarsScaleFactor'),
    'Value');
end;

{ TdxGaugeLinearScaleRangeViewInfo }

function TdxGaugeLinearScaleRangeViewInfo.GetRangeParametersClass: TdxGaugeCustomRangeParametersClass;
begin
  Result := TdxGaugeLinearRangeParameters;
end;

procedure TdxGaugeLinearScaleRangeViewInfo.CalculateBounds;

  function ValueToPoint(AValue: Single): TdxPointF;
  begin
    Result := dxGaugeLinearScaleValueToPoint(AValue, GetRangeParameters.ScaleMinValue,
      GetRangeParameters.LevelBarLine, GetValueRange);
  end;

var
  AParameters: TdxGaugeLinearRangeParameters;
  AWidth: Single;
begin
  AParameters := GetRangeParameters;
  AWidth := AParameters.ScaleBounds.Width * AParameters.WidthFactor;
  FBounds.Top := ValueToPoint(AParameters.ValueEnd).Y;
  FBounds.Bottom := ValueToPoint(AParameters.ValueStart).Y;
  FBounds.Left := AParameters.ScaleBounds.Left + AParameters.ScaleBounds.Width * AParameters.CenterPositionFactor
    - AWidth / 2;
  FBounds.Right := FBounds.Left + AWidth;
  FBounds := cxRectAdjustF(FBounds);
end;

procedure TdxGaugeLinearScaleRangeViewInfo.DoDraw(AGPGraphics: TdxGPGraphics);
begin
  AGPGraphics.Rectangle(FBounds, GetRangeParameters.Color, GetRangeParameters.Color);
end;

function TdxGaugeLinearScaleRangeViewInfo.GetRangeParameters: TdxGaugeLinearRangeParameters;
begin
  Result := FRangeParameters as TdxGaugeLinearRangeParameters;
end;

{ TdxGaugeLinearScaleRange }

constructor TdxGaugeLinearScaleRange.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  GetRangeParameters.CenterPositionFactor := dxGaugeLinearRangeCenterPositionFactor;
end;

function TdxGaugeLinearScaleRange.GetRangeParametersClass: TdxGaugeCustomRangeParametersClass;
begin
  Result := TdxGaugeLinearRangeParameters;
end;

function TdxGaugeLinearScaleRange.GetViewInfoClass: TdxGaugeCustomRangeViewInfoClass;
begin
  Result := TdxGaugeLinearScaleRangeViewInfo;
end;

procedure TdxGaugeLinearScaleRange.DefineProperties(AFiler: TFiler);
begin
  inherited DefineProperties(AFiler);
  AFiler.DefineProperty('CenterPositionFactor', ReadCenterPositionFactor, WriteCenterPositionFactor,
    IsCenterPositionFactorStored);
end;

function TdxGaugeLinearScaleRange.GetCenterPositionFactor: Single;
begin
  Result := GetRangeParameters.CenterPositionFactor;
end;

procedure TdxGaugeLinearScaleRange.SetCenterPositionFactor(const AValue: Single);
begin
  if not SameValue(GetRangeParameters.CenterPositionFactor, AValue) then
  begin
    GetRangeParameters.CenterPositionFactor := Max(Min(AValue, 1), 0);
    Changed(False);
  end;
end;

function TdxGaugeLinearScaleRange.GetRangeParameters: TdxGaugeLinearRangeParameters;
begin
  Result := RangeParameters as TdxGaugeLinearRangeParameters;
end;

procedure TdxGaugeLinearScaleRange.ReadCenterPositionFactor(AReader: TReader);
begin
  CenterPositionFactor := AReader.ReadDouble;
end;

procedure TdxGaugeLinearScaleRange.WriteCenterPositionFactor(AWriter: TWriter);
begin
  AWriter.WriteDouble(CenterPositionFactor);
end;

function TdxGaugeLinearScaleRange.IsCenterPositionFactorStored: Boolean;
begin
  Result := not SameValue(GetRangeParameters.CenterPositionFactor, dxGaugeLinearRangeCenterPositionFactor);
end;

initialization
  dxGaugeScaleFactory.RegisterScale(TdxGaugeLinearScale);

finalization
  dxGaugeUnregisterScales;

end.
