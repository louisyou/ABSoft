{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeCircularScale;

{$I cxVer.inc}

interface

uses
  Classes, Graphics, cxGeometry, dxCoreGraphics, cxGraphics, dxXMLDoc, dxGDIPlusClasses, dxCompositeShape,
  dxGaugeCustomScale, dxGaugeQuantitativeScale;

type
  TdxGaugeArcQuantitativeScale = class;
  TdxGaugeArcQuantitativeScaleParameterValue = (spvAngleEnd, spvAngleStart);
  TdxGaugeArcQuantitativeScaleParameterValues = set of TdxGaugeArcQuantitativeScaleParameterValue;

  { TdxGaugeArcQuantitativeScaleDefaultParameters }

  TdxGaugeArcQuantitativeScaleDefaultParameters = class(TdxGaugeQuantitativeScaleDefaultParameters)
  public
    AngleStart: Integer;
    AngleEnd: Integer;
    ScaleArcRadiusFactor: Single;
    ValueIndicatorScaleFactor: TdxPointF;
  end;

  { TdxGaugeCircularHalfScaleDefaultParameters }

  TdxGaugeCircularHalfScaleDefaultParameters = class(TdxGaugeArcQuantitativeScaleDefaultParameters)
  public
    CenterPointPositionFactor: TdxPointF;
  end;

  { TdxGaugeArcQuantitativeScaleParameters }

  TdxGaugeArcQuantitativeScaleParameters = class(TdxGaugeQuantitativeScaleParameters)
  public
    AngleStart: Integer;
    AngleEnd: Integer;
    Radius: Single;
    RadiusFactor: Single;
    ShowSpindleCap: Boolean;
  end;

  { TdxGaugeCustomRangeParameters }

  TdxGaugeCircularRangeParameters = class(TdxGaugeCustomRangeParameters)
  public
    RadiusFactor: Single;
    AngleStart: Single;
    AngleRange: Single;
    MinValue: Single;
    RotatePoint: TdxPointF;
  end;

  { TdxGaugeScaleOptionsArcLayout }

  TdxGaugeScaleOptionsArcLayout = class(TdxGaugeScaleOptionsCustomLayout)
  private
    function GetRadius: Integer;
    function GetRadiusFactor: Single;
    procedure SetRadius(const AValue: Integer);
    procedure SetRadiusFactor(const AValue: Single);

    function GetScale: TdxGaugeArcQuantitativeScale;
    function IsRadiusFactorStored: Boolean;
  published
    property Radius: Integer read GetRadius write SetRadius default 0;
    property RadiusFactor: Single read GetRadiusFactor write SetRadiusFactor stored IsRadiusFactorStored;
  end;

  { TdxGaugeArcQuantitativeScaleOptionsView }

  TdxGaugeArcQuantitativeScaleOptionsView = class(TdxGaugeQuantitativeScaleOptionsView)
  private
    function GetAngleEnd: Integer;
    function GetAngleStart: Integer;
    function GetShowSpindleCap: Boolean;
    procedure SetAngleEnd(const AValue: Integer);
    procedure SetAngleStart(const AValue: Integer);
    procedure SetShowSpindleCap(const AValue: Boolean);

    function GetScale: TdxGaugeArcQuantitativeScale;
    function IsAngleEndStored: Boolean;
    function IsAngleStartStored: Boolean;
  published
    property AngleEnd: Integer read GetAngleEnd write SetAngleEnd stored IsAngleEndStored;
    property AngleStart: Integer read GetAngleStart write SetAngleStart stored IsAngleStartStored;
    property ShowNeedle: Boolean read GetShowValueIndicator write SetShowValueIndicator default True;
    property ShowSpindleCap: Boolean read GetShowSpindleCap write SetShowSpindleCap default True;
  end;

  { TdxGaugeArcQuantitativeScaleViewInfo }

  TdxGaugeArcQuantitativeScaleViewInfo = class(TdxGaugeQuantitativeScaleViewInfo)
  private
    FNeedle: TdxCompositeShape;
    FSpindleCap: TdxCompositeShape;

    function GetAngleRange: Single;
    function GetMajorTicksAngleStep: Single;
    function GetMinorTicksAngleStep: Single;
    function GetScaleDefaultParameters: TdxGaugeArcQuantitativeScaleDefaultParameters;
    function GetScaleParameters: TdxGaugeArcQuantitativeScaleParameters;
    function ValueToAngle(AValue: Single): Single;
  protected
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;

    function GetMajorTickStartPosition: Single; override;
    function GetMajorTicksPositionStep: Single; override;
    function GetMinorTicksPositionStep: Single; override;
    function GetScaleSize(const ABounds: TdxRectF): TdxSizeF; override;
    procedure CalculateReferenceParameter; override;
    procedure DoDrawValueIndicator(AGPGraphics: TdxGPGraphics); override;
    procedure DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single); override;
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); override;
    procedure DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single;
      const ATickDrawParameters: TdxGaugeScaleTickDrawParameters); override;
    procedure LoadScaleElements; override;

    procedure CalculateScaleRadius; virtual; abstract;
    function GetRotatePoint: TdxPointF; virtual;

    function GetScaleRadius(const AContentSize: TdxSizeF): Single;
    procedure DrawSpindleCap(AGPGraphics: TdxGPGraphics);
  end;

  { TdxGaugeArcQuantitativeScale }

  TdxGaugeArcQuantitativeScale = class(TdxGaugeQuantitativeScale)
  private
    FAssignedValues: TdxGaugeArcQuantitativeScaleParameterValues;

    function GetAngleEnd: Integer;
    function GetAngleStart: Integer;
    function GetOptionsLayout: TdxGaugeScaleOptionsArcLayout;
    function GetOptionsView: TdxGaugeArcQuantitativeScaleOptionsView;
    function GetRadius: Integer;
    function GetRadiusFactor: Single;
    function GetShowSpindleCap: Boolean;
    procedure SetAngleEnd(const AValue: Integer);
    procedure SetAngleStart(const AValue: Integer);
    procedure SetOptionsLayout(const AValue: TdxGaugeScaleOptionsArcLayout);
    procedure SetOptionsView(const AValue: TdxGaugeArcQuantitativeScaleOptionsView);
    procedure SetRadius(const AValue: Integer);
    procedure SetRadiusFactor(const AValue: Single);
    procedure SetShowSpindleCap(const AValue: Boolean);

    function GetScaleParameters: TdxGaugeArcQuantitativeScaleParameters;
    function GetViewInfo: TdxGaugeArcQuantitativeScaleViewInfo;

    function IsAngleEndStored: Boolean;
    function IsAngleStartStored: Boolean;
  protected
    class function GetLayerCount: Integer; override;
    class function GetScaleName: string; override;

    function GetOptionsLayoutClass: TdxGaugeScaleOptionsCustomLayoutClass; override;
    function GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass; override;
    function GetRangeClass: TdxGaugeCustomRangeClass; override;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;

    procedure ApplyStyleParameters; override;
    procedure DoAssign(AScale: TdxGaugeCustomScale); override;
    procedure InitScaleParameters; override;
    procedure InternalRestoreStyleParameters; override;
    procedure PopulateRangeScaleInfo(ARangeIndex: Integer); override;

    procedure SetValidAngleEnd(const AValue: Integer); virtual;
    procedure SetValidAngleStart(const AValue: Integer); virtual;

    property OptionsLayout: TdxGaugeScaleOptionsArcLayout read GetOptionsLayout write SetOptionsLayout;
    property OptionsView: TdxGaugeArcQuantitativeScaleOptionsView read GetOptionsView write SetOptionsView;
  end;

  { TdxGaugeCustomCircularScaleViewInfo }

  TdxGaugeCustomCircularScaleViewInfo = class(TdxGaugeArcQuantitativeScaleViewInfo)
  protected
    procedure CalculateBounds(const ABounds: TdxRectF); override;
    procedure CalculateScaleRadius; override;
  end;

  { TdxGaugeCustomCircularScale }

  TdxGaugeCustomCircularScale = class(TdxGaugeArcQuantitativeScale)
  protected
    class function GetScaleType: TdxGaugeScaleType; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;
  end;

  { TdxGaugeCircularScale }

  TdxGaugeCircularScale = class(TdxGaugeCustomCircularScale)
  published
    property AnchorScaleIndex;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Value;
    property Visible;
  end;

  { TdxGaugeCustomCircularHalfScaleViewInfo }

  TdxGaugeCustomCircularHalfScaleViewInfo = class(TdxGaugeArcQuantitativeScaleViewInfo)
  private
    function GetScaleDefaultParameters: TdxGaugeCircularHalfScaleDefaultParameters;
  protected
    function GetReferencePoint: TdxPointF; virtual;
    function GetRotatePoint: TdxPointF; override;
    procedure CalculateScaleRadius; override;
  end;

  { TdxGaugeCustomCircularHalfScale }

  TdxGaugeCustomCircularHalfScale = class(TdxGaugeArcQuantitativeScale)
  protected
    class function GetScaleName: string; override;
    class function GetScaleType: TdxGaugeScaleType; override;

    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;
    procedure SetValidAngleEnd(const AValue: Integer); override;
    procedure SetValidAngleStart(const AValue: Integer); override;

    function GetValidAngleValue(const AValue: Integer): Integer; virtual;
  end;

  { TdxGaugeCircularHalfScale }

  TdxGaugeCircularHalfScale = class(TdxGaugeCustomCircularHalfScale)
  published
    property AnchorScaleIndex;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Value;
    property Visible;
  end;

  { TdxGaugeCustomCircularQuarterLeftScale }

  TdxGaugeCustomCircularQuarterLeftScale = class(TdxGaugeCustomCircularHalfScale)
  protected
    class function GetScaleName: string; override;
    class function GetScaleType: TdxGaugeScaleType; override;

    function GetValidAngleValue(const AValue: Integer): Integer; override;
  end;

  { TdxGaugeCircularQuarterLeftScale }

  TdxGaugeCircularQuarterLeftScale = class(TdxGaugeCustomCircularQuarterLeftScale)
  published
    property AnchorScaleIndex;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Value;
    property Visible;
  end;

  { TdxGaugeCustomCircularQuarterRightScaleViewInfo }

  TdxGaugeCustomCircularQuarterRightScaleViewInfo = class(TdxGaugeCustomCircularHalfScaleViewInfo)
  protected
    function GetReferencePoint: TdxPointF; override;
  end;

  { TdxGaugeCustomCircularQuarterRightScale }

  TdxGaugeCustomCircularQuarterRightScale = class(TdxGaugeCustomCircularHalfScale)
  protected
    class function GetScaleName: string; override;
    class function GetScaleType: TdxGaugeScaleType; override;

    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;
    function GetValidAngleValue(const AValue: Integer): Integer; override;
  end;

  { TdxGaugeCircularQuarterRightScale }

  TdxGaugeCircularQuarterRightScale = class(TdxGaugeCustomCircularQuarterRightScale)
  published
    property AnchorScaleIndex;
    property OptionsLayout;
    property OptionsView;
    property Ranges;
    property StyleName;
    property Value;
    property Visible;
  end;

  { TdxGaugeArcQuantitativeScaleStyleReader }

  TdxGaugeArcQuantitativeScaleStyleReader = class(TdxGaugeQuantitativeScaleStyleReader)
  private
    procedure ReadNeedleParameters(ANode: TdxXMLNode; AParameters: TdxGaugeArcQuantitativeScaleDefaultParameters);
    procedure ReadSpindleCapParameters(ANode: TdxXMLNode; AParameters: TdxGaugeArcQuantitativeScaleDefaultParameters);
  protected
    class function GetResourceNamePreffix: string; override;
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadCommonScaleParameters(ANode: TdxXMLNode;
      AParameters: TdxGaugeQuantitativeScaleDefaultParameters); override;
  end;

  { TdxGaugeCustomCircularHalfScaleStyleReader }

  TdxGaugeCustomCircularHalfScaleStyleReader = class(TdxGaugeArcQuantitativeScaleStyleReader)
  protected
    class function GetResourceNamePreffix: string; override;
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadCommonScaleParameters(ANode: TdxXMLNode;
      AParameters: TdxGaugeQuantitativeScaleDefaultParameters); override;
  end;

  { TdxGaugeCircularScaleRangeViewInfo }

  TdxGaugeCircularScaleRangeViewInfo = class(TdxGaugeCustomRangeViewInfo)
  private
    FWidth: Single;

    function GetRangeParameters: TdxGaugeCircularRangeParameters;
  protected
    function GetRangeParametersClass: TdxGaugeCustomRangeParametersClass; override;
    procedure CalculateBounds; override;
    procedure DoDraw(AGPGraphics: TdxGPGraphics); override;
  end;

  { TdxGaugeCircularScaleRange }

  TdxGaugeCircularScaleRange = class(TdxGaugeCustomRange)
  private
    function GetRadiusFactor: Single;
    procedure SetRadiusFactor(const AValue: Single);

    function GetRangeParameters: TdxGaugeCircularRangeParameters;

    procedure ReadRadiusFactor(AReader: TReader);
    procedure WriteRadiusFactor(AWriter: TWriter);

    function IsRadiusFactorStored: Boolean;
  protected
    function GetRangeParametersClass: TdxGaugeCustomRangeParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomRangeViewInfoClass; override;
    procedure DefineProperties(AFiler: TFiler); override;

    function GetViewInfo: TdxGaugeCircularScaleRangeViewInfo;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Color;
    property RadiusFactor: Single read GetRadiusFactor write SetRadiusFactor stored IsRadiusFactorStored;
    property ValueEnd;
    property ValueStart;
    property Visible;
    property WidthFactor;
  end;

implementation

uses
  SysUtils, Math, dxGaugeUtils;

const
  dxGaugeCircularScaleRadiusFactor = 0.5;
  dxGaugeCircularScaleRangeRadiusFactor = 0.5;

type
  TdxGaugeCircularScaleRangeAccess = class(TdxGaugeCircularScaleRange);
  TdxGaugeCircularScaleRangeViewInfoAccess = class(TdxGaugeCircularScaleRangeViewInfo);
  TdxGaugeScaleStyleAccess = class(TdxGaugeScaleStyle);

function dxGaugeCisrcularScaleValueToAngle(AValue, AMinValue, AValueRange, AAngleStart, AAngleRange: Single): Single;
begin
  Result := AAngleStart - (AValue - AMinValue) * AAngleRange / AValueRange;
end;

{ TdxGaugeScaleOptionsArcLayout }

function TdxGaugeScaleOptionsArcLayout.GetRadius: Integer;
begin
  Result := GetScale.GetRadius;
end;

function TdxGaugeScaleOptionsArcLayout.GetRadiusFactor: Single;
begin
  Result := GetScale.GetRadiusFactor;
end;

procedure TdxGaugeScaleOptionsArcLayout.SetRadius(const AValue: Integer);
begin
  GetScale.SetRadius(AValue);
end;

procedure TdxGaugeScaleOptionsArcLayout.SetRadiusFactor(const AValue: Single);
begin
  GetScale.SetRadiusFactor(AValue);
end;

function TdxGaugeScaleOptionsArcLayout.GetScale: TdxGaugeArcQuantitativeScale;
begin
  Result := inherited GetScale as TdxGaugeArcQuantitativeScale;
end;

function TdxGaugeScaleOptionsArcLayout.IsRadiusFactorStored: Boolean;
begin
  Result := not SameValue(RadiusFactor, dxGaugeCircularScaleRadiusFactor);
end;

{ TdxGaugeArcQuantitativeScaleOptionsView }

function TdxGaugeArcQuantitativeScaleOptionsView.GetAngleEnd: Integer;
begin
  Result := GetScale.GetAngleEnd;
end;

function TdxGaugeArcQuantitativeScaleOptionsView.GetAngleStart: Integer;
begin
  Result := GetScale.GetAngleStart;
end;

function TdxGaugeArcQuantitativeScaleOptionsView.GetShowSpindleCap: Boolean;
begin
  Result := (GetScale as TdxGaugeArcQuantitativeScale).GetShowSpindleCap;
end;

procedure TdxGaugeArcQuantitativeScaleOptionsView.SetAngleEnd(const AValue: Integer);
begin
  GetScale.SetAngleEnd(AValue);
end;

procedure TdxGaugeArcQuantitativeScaleOptionsView.SetAngleStart(const AValue: Integer);
begin
  GetScale.SetAngleStart(AValue);
end;

procedure TdxGaugeArcQuantitativeScaleOptionsView.SetShowSpindleCap(const AValue: Boolean);
begin
  (GetScale as TdxGaugeArcQuantitativeScale).SetShowSpindleCap(AValue);
end;

function TdxGaugeArcQuantitativeScaleOptionsView.IsAngleEndStored: Boolean;
begin
  Result := GetScale.IsAngleEndStored;
end;

function TdxGaugeArcQuantitativeScaleOptionsView.IsAngleStartStored: Boolean;
begin
  Result := GetScale.IsAngleStartStored;
end;

function TdxGaugeArcQuantitativeScaleOptionsView.GetScale: TdxGaugeArcQuantitativeScale;
begin
  Result := inherited GetScale as TdxGaugeArcQuantitativeScale;
end;

{ TdxGaugeArcQuantitativeScaleViewInfo }

function TdxGaugeArcQuantitativeScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeArcQuantitativeScaleParameters;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetMajorTickStartPosition: Single;
begin
  Result := GetScaleParameters.AngleStart;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetMajorTicksPositionStep: Single;
begin
  Result := GetMajorTicksAngleStep;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetMinorTicksPositionStep: Single;
begin
  Result := GetMinorTicksAngleStep;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetScaleSize(const ABounds: TdxRectF): TdxSizeF;
var
  AScaleParameters: TdxGaugeArcQuantitativeScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.Radius <> 0 then
  begin
    Result.cx := AScaleParameters.Radius * 2;
    Result.cy := Result.cx;
  end
  else
  begin
    Result.cx := AScaleParameters.RadiusFactor * ABounds.Width * 2;
    Result.cy := AScaleParameters.RadiusFactor * ABounds.Height * 2;
  end;
end;

procedure TdxGaugeArcQuantitativeScaleViewInfo.CalculateReferenceParameter;
begin
  CalculateScaleRadius;
end;

procedure TdxGaugeArcQuantitativeScaleViewInfo.DoDrawValueIndicator(AGPGraphics: TdxGPGraphics);

  function GetStartOffset: Single;
  begin
    Result := GetScaleDefaultParameters.ValueIndicatorStartOffset * FScaleFactor.X;
  end;

  function GetImageSize(AImage: TGraphic): TdxSizeF;
  begin
    Result.cx := (GetScaleParameters.Radius - GetScaleDefaultParameters.ValueIndicatorEndOffset *
      FScaleFactor.X - GetStartOffset) * GetScaleDefaultParameters.ValueIndicatorScaleFactor.X;
    Result.cy := (AImage.Height * Result.cx / AImage.Width) * GetScaleDefaultParameters.ValueIndicatorScaleFactor.Y;
  end;

begin
  if (FNeedle <> nil) and not FNeedle.Empty then
    dxGaugeRotateAndDrawImage(AGPGraphics, FNeedle, GetImageSize(FNeedle), GetRotatePoint, GetStartOffset,
      ValueToAngle(GetScaleValue));
end;

procedure TdxGaugeArcQuantitativeScaleViewInfo.DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single);
begin
  if GetScaleParameters.ShowLabels then
    dxGaugeDrawText(AGPGraphics, GetLabelText(AValue), GetRotatePoint, AOffset, ValueToAngle(AValue), Font);
end;

procedure TdxGaugeArcQuantitativeScaleViewInfo.DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  inherited DrawLayer(AGPGraphics, ALayerIndex);
  if ALayerIndex = 2 then
    DrawSpindleCap(AGPGraphics);
end;

procedure TdxGaugeArcQuantitativeScaleViewInfo.DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single;
  const ATickDrawParameters: TdxGaugeScaleTickDrawParameters);
var
  AScaleParameters: TdxGaugeArcQuantitativeScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if ATickDrawParameters.ShowTick then
    dxGaugeRotateAndDrawImage(AGPGraphics, ATickDrawParameters.Image, ATickDrawParameters.Size, GetRotatePoint,
      AScaleParameters.Radius + ATickDrawParameters.Offset * FScaleFactor.X, ValueToAngle(AValue));

  if CanDrawLabel(ATickDrawParameters) then
    DrawLabel(AGPGraphics, AValue, AScaleParameters.Radius + GetLabelOffset);
end;

procedure TdxGaugeArcQuantitativeScaleViewInfo.LoadScaleElements;
begin
  inherited LoadScaleElements;
  FNeedle := TdxGaugeScaleStyleAccess(Style).GetElement(etNeedle);
  FSpindleCap := TdxGaugeScaleStyleAccess(Style).GetElement(etSpindleCap);
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetRotatePoint: TdxPointF;
begin
  Result := cxRectCenter(FBounds);
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetScaleRadius(const AContentSize: TdxSizeF): Single;
var
  ADefaultParameters: TdxGaugeArcQuantitativeScaleDefaultParameters;
begin
  ADefaultParameters := GetScaleDefaultParameters;
  Result := Min(AContentSize.cx * ADefaultParameters.ScaleArcRadiusFactor,
    AContentSize.cy * ADefaultParameters.ScaleArcRadiusFactor);
end;

procedure TdxGaugeArcQuantitativeScaleViewInfo.DrawSpindleCap(AGPGraphics: TdxGPGraphics);
var
  R: TdxRectF;
  AScaleDefaultParameters: TdxGaugeArcQuantitativeScaleDefaultParameters;
begin
  if GetScaleParameters.ShowSpindleCap then
  begin
    AScaleDefaultParameters := GetScaleDefaultParameters;
    R := cxRectInflate(cxRectF(GetRotatePoint, GetRotatePoint), AScaleDefaultParameters.SpindleCapSize / 2 * FScaleFactor.X,
      AScaleDefaultParameters.SpindleCapSize / 2 * FScaleFactor.X);
    dxGaugeDrawImage(AGPGraphics, FSpindleCap, R);
  end;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetAngleRange: Single;
begin
  Result := GetScaleParameters.AngleStart - GetScaleParameters.AngleEnd;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetMajorTicksAngleStep: Single;
begin
  Result := GetAngleRange / GetMajorTickCount;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetMinorTicksAngleStep: Single;
begin
  Result := GetMajorTicksAngleStep / GetMinorTickCount;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetScaleDefaultParameters: TdxGaugeArcQuantitativeScaleDefaultParameters;
begin
  Result := ScaleDefaultParameters as TdxGaugeArcQuantitativeScaleDefaultParameters;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.GetScaleParameters: TdxGaugeArcQuantitativeScaleParameters;
begin
  Result := FScaleParameters as TdxGaugeArcQuantitativeScaleParameters;
end;

function TdxGaugeArcQuantitativeScaleViewInfo.ValueToAngle(AValue: Single): Single;
var
  AScaleParameters: TdxGaugeArcQuantitativeScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  Result := dxGaugeCisrcularScaleValueToAngle(AValue, AScaleParameters.MinValue, GetValueRange,
    AScaleParameters.AngleStart, GetAngleRange)
end;

{ TdxGaugeArcQuantitativeScale }

class function TdxGaugeArcQuantitativeScale.GetLayerCount: Integer;
begin
  Result := 4;
end;

class function TdxGaugeArcQuantitativeScale.GetScaleName: string;
begin
  Result := 'Circular';
end;

function TdxGaugeArcQuantitativeScale.GetOptionsLayoutClass: TdxGaugeScaleOptionsCustomLayoutClass;
begin
  Result := TdxGaugeScaleOptionsArcLayout;
end;

function TdxGaugeArcQuantitativeScale.GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass;
begin
  Result := TdxGaugeArcQuantitativeScaleOptionsView;
end;

function TdxGaugeArcQuantitativeScale.GetRangeClass: TdxGaugeCustomRangeClass;
begin
  Result := TdxGaugeCircularScaleRange;
end;

function TdxGaugeArcQuantitativeScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeArcQuantitativeScaleParameters;
end;

function TdxGaugeArcQuantitativeScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeArcQuantitativeScaleViewInfo;
end;

procedure TdxGaugeArcQuantitativeScale.ApplyStyleParameters;
var
  AParameters: TdxGaugeArcQuantitativeScaleParameters;
  ADefaultParameters: TdxGaugeArcQuantitativeScaleDefaultParameters;
begin
  inherited ApplyStyleParameters;
  AParameters := GetScaleParameters;
  ADefaultParameters := GetViewInfo.GetScaleDefaultParameters;
  if not(spvAngleEnd in FAssignedValues) then
    AParameters.AngleEnd := ADefaultParameters.AngleEnd;
  if not(spvAngleStart in FAssignedValues) then
    AParameters.AngleStart := ADefaultParameters.AngleStart;
end;

procedure TdxGaugeArcQuantitativeScale.DoAssign(AScale: TdxGaugeCustomScale);
begin
  inherited DoAssign(AScale);
  FAssignedValues := (AScale as TdxGaugeArcQuantitativeScale).FAssignedValues;
end;

procedure TdxGaugeArcQuantitativeScale.InitScaleParameters;
begin
  inherited InitScaleParameters;
  GetScaleParameters.RadiusFactor := dxGaugeCircularScaleRadiusFactor;
  GetScaleParameters.ShowSpindleCap := True;
end;

procedure TdxGaugeArcQuantitativeScale.InternalRestoreStyleParameters;
begin
  FAssignedValues := [];
  GetScaleParameters.ShowSpindleCap := True;
  inherited InternalRestoreStyleParameters;
end;

procedure TdxGaugeArcQuantitativeScale.PopulateRangeScaleInfo(ARangeIndex: Integer);

  function GetRangeParameters(AIndex: Integer): TdxGaugeCircularRangeParameters;
  begin
    Result := TdxGaugeCircularScaleRangeAccess(Ranges[AIndex]).GetRangeParameters;
  end;

begin
  inherited PopulateRangeScaleInfo(ARangeIndex);
  GetRangeParameters(ARangeIndex).AngleStart := GetScaleParameters.AngleStart;
  GetRangeParameters(ARangeIndex).AngleRange := GetViewInfo.GetAngleRange;
  GetRangeParameters(ARangeIndex).RotatePoint := GetViewInfo.GetRotatePoint;
  GetRangeParameters(ARangeIndex).MinValue := GetViewInfo.GetScaleParameters.MinValue;
end;

procedure TdxGaugeArcQuantitativeScale.SetValidAngleEnd(const AValue: Integer);
begin
  GetScaleParameters.AngleEnd := AValue;
  if GetScaleParameters.AngleStart = GetScaleParameters.AngleEnd then
    GetScaleParameters.AngleEnd := GetScaleParameters.AngleEnd - 360;
end;

procedure TdxGaugeArcQuantitativeScale.SetValidAngleStart(const AValue: Integer);
begin
  GetScaleParameters.AngleStart := AValue;
  if GetScaleParameters.AngleStart = GetScaleParameters.AngleEnd then
    GetScaleParameters.AngleStart := GetScaleParameters.AngleStart - 360;
end;

function TdxGaugeArcQuantitativeScale.GetAngleEnd: Integer;
begin
  Result := GetScaleParameters.AngleEnd;
end;

function TdxGaugeArcQuantitativeScale.GetAngleStart: Integer;
begin
  Result := GetScaleParameters.AngleStart;
end;

function TdxGaugeArcQuantitativeScale.GetOptionsLayout: TdxGaugeScaleOptionsArcLayout;
begin
  Result := FOptionsLayout as TdxGaugeScaleOptionsArcLayout;
end;

function TdxGaugeArcQuantitativeScale.GetOptionsView: TdxGaugeArcQuantitativeScaleOptionsView;
begin
  Result := FOptionsView as TdxGaugeArcQuantitativeScaleOptionsView;
end;

function TdxGaugeArcQuantitativeScale.GetRadius: Integer;
begin
  Result := Round(GetScaleParameters.Radius);
end;

function TdxGaugeArcQuantitativeScale.GetRadiusFactor: Single;
begin
  Result := GetScaleParameters.RadiusFactor;
end;

function TdxGaugeArcQuantitativeScale.GetShowSpindleCap: Boolean;
begin
  Result := GetScaleParameters.ShowSpindleCap;
end;

procedure TdxGaugeArcQuantitativeScale.SetAngleEnd(const AValue: Integer);
begin
  if GetScaleParameters.AngleEnd <> AValue then
  begin
    SetValidAngleEnd(AValue);
    if GetScaleParameters.AngleEnd = GetViewInfo.GetScaleDefaultParameters.AngleEnd then
      Exclude(FAssignedValues, spvAngleEnd)
    else
      Include(FAssignedValues, spvAngleEnd);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeArcQuantitativeScale.SetAngleStart(const AValue: Integer);
begin
  if GetScaleParameters.AngleStart <> AValue then
  begin
    SetValidAngleStart(AValue);
    if GetScaleParameters.AngleStart = GetViewInfo.GetScaleDefaultParameters.AngleStart then
      Exclude(FAssignedValues, spvAngleStart)
    else
      Include(FAssignedValues, spvAngleStart);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeArcQuantitativeScale.SetOptionsLayout(const AValue: TdxGaugeScaleOptionsArcLayout);
begin
  FOptionsLayout.Assign(AValue);
end;

procedure TdxGaugeArcQuantitativeScale.SetOptionsView(const AValue: TdxGaugeArcQuantitativeScaleOptionsView);
begin
  FOptionsView.Assign(AValue);
end;

procedure TdxGaugeArcQuantitativeScale.SetRadius(const AValue: Integer);
begin
  if (GetScaleParameters.Radius <> AValue) and (AValue >= 0) then
  begin
    GetScaleParameters.Radius := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeArcQuantitativeScale.SetRadiusFactor(const AValue: Single);
begin
  if not SameValue(GetScaleParameters.RadiusFactor, AValue) and (AValue > 0) then
  begin
    GetScaleParameters.RadiusFactor := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeArcQuantitativeScale.SetShowSpindleCap(const AValue: Boolean);
begin
  if GetScaleParameters.ShowSpindleCap <> AValue then
  begin
    GetScaleParameters.ShowSpindleCap := AValue;
    ScaleChanged([sclDynamicLayer]);
  end;
end;

function TdxGaugeArcQuantitativeScale.GetScaleParameters: TdxGaugeArcQuantitativeScaleParameters;
begin
  Result := ScaleParameters as TdxGaugeArcQuantitativeScaleParameters;
end;

function TdxGaugeArcQuantitativeScale.GetViewInfo: TdxGaugeArcQuantitativeScaleViewInfo;
begin
  Result := ViewInfo as TdxGaugeArcQuantitativeScaleViewInfo;
end;

function TdxGaugeArcQuantitativeScale.IsAngleEndStored: Boolean;
begin
  Result := spvAngleEnd in FAssignedValues;
end;

function TdxGaugeArcQuantitativeScale.IsAngleStartStored: Boolean;
begin
  Result := spvAngleStart in FAssignedValues;
end;

{ TdxGaugeCustomCircularScaleViewInfo }

procedure TdxGaugeCustomCircularScaleViewInfo.CalculateBounds(const ABounds: TdxRectF);
var
  ASize: Single;
begin
  inherited CalculateBounds(ABounds);
  ASize := Min(FBounds.Width / 2, FBounds.Height / 2);
  FBounds := cxRectInflate(cxRectF(cxRectCenter(FBounds), cxRectCenter(FBounds)), ASize, ASize);
end;

procedure TdxGaugeCustomCircularScaleViewInfo.CalculateScaleRadius;
begin
  GetScaleParameters.Radius := GetScaleRadius(dxSizeF(FBounds.Width / 2, FBounds.Height / 2));
end;

{ TdxGaugeCustomCircularScale }

class function TdxGaugeCustomCircularScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stCircularScale;
end;

function TdxGaugeCustomCircularScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeCustomCircularScaleViewInfo;
end;

{ TdxGaugeCustomCircularHalfScaleViewInfo }

function TdxGaugeCustomCircularHalfScaleViewInfo.GetReferencePoint: TdxPointF;
begin
  Result := GetContentBounds.TopLeft;
end;

function TdxGaugeCustomCircularHalfScaleViewInfo.GetRotatePoint: TdxPointF;
var
  AContentBounds: TdxRectF;
begin
  AContentBounds := GetContentBounds;
  Result.X := AContentBounds.Left + AContentBounds.Width * GetScaleDefaultParameters.CenterPointPositionFactor.X;
  Result.Y := AContentBounds.Top + AContentBounds.Height * GetScaleDefaultParameters.CenterPointPositionFactor.Y;
end;

procedure TdxGaugeCustomCircularHalfScaleViewInfo.CalculateScaleRadius;
var
  ASize: TdxSizeF;
begin
  ASize.cx := Max(GetRotatePoint.X, GetReferencePoint.X) - Min(GetRotatePoint.X, GetReferencePoint.X);
  ASize.cy := Max(GetRotatePoint.Y, GetReferencePoint.Y) - Min(GetRotatePoint.Y, GetReferencePoint.Y);
  GetScaleParameters.Radius := GetScaleRadius(ASize);
end;

function TdxGaugeCustomCircularHalfScaleViewInfo.GetScaleDefaultParameters: TdxGaugeCircularHalfScaleDefaultParameters;
begin
  Result := ScaleDefaultParameters as TdxGaugeCircularHalfScaleDefaultParameters;
end;

{ TdxGaugeCustomCircularHalfScale }

class function TdxGaugeCustomCircularHalfScale.GetScaleName: string;
begin
  Result := inherited GetScaleName + ' Half';
end;

class function TdxGaugeCustomCircularHalfScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stCircularHalfScale;
end;

function TdxGaugeCustomCircularHalfScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeCustomCircularHalfScaleViewInfo;
end;

procedure TdxGaugeCustomCircularHalfScale.SetValidAngleEnd(const AValue: Integer);
begin
  GetScaleParameters.AngleEnd := GetValidAngleValue(AValue);
end;

procedure TdxGaugeCustomCircularHalfScale.SetValidAngleStart(const AValue: Integer);
begin
  GetScaleParameters.AngleStart := GetValidAngleValue(AValue);
end;

function TdxGaugeCustomCircularHalfScale.GetValidAngleValue(const AValue: Integer): Integer;
begin
  Result := Max(Min(AValue, 180), 0);
end;

{ TdxGaugeCustomCircularQuarterLeftScale }

class function TdxGaugeCustomCircularQuarterLeftScale.GetScaleName: string;
begin
  Result := 'Circular Quarter Left';
end;

class function TdxGaugeCustomCircularQuarterLeftScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stCircularQuarterLeftScale;
end;

function TdxGaugeCustomCircularQuarterLeftScale.GetValidAngleValue(const AValue: Integer): Integer;
begin
  Result := Max(Min(AValue, 180), 90);
end;

{ TdxGaugeCustomCircularQuarterRightScaleViewInfo }

function TdxGaugeCustomCircularQuarterRightScaleViewInfo.GetReferencePoint: TdxPointF;
begin
  Result := dxPointF(GetContentBounds.Right, GetContentBounds.Top);
end;

{ TdxGaugeCustomCircularQuarterRightScale }

class function TdxGaugeCustomCircularQuarterRightScale.GetScaleName: string;
begin
  Result := 'Circular Quarter Right';
end;

class function TdxGaugeCustomCircularQuarterRightScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stCircularQuarterRightScale;
end;

function TdxGaugeCustomCircularQuarterRightScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeCustomCircularQuarterRightScaleViewInfo;
end;

function TdxGaugeCustomCircularQuarterRightScale.GetValidAngleValue(const AValue: Integer): Integer;
begin
  Result := Max(Min(AValue, 90), 0);
end;

{ TdxGaugeArcQuantitativeScaleStyleReader }

class function TdxGaugeArcQuantitativeScaleStyleReader.GetResourceNamePreffix: string;
begin
  Result := 'Circular';
end;

function TdxGaugeArcQuantitativeScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeArcQuantitativeScaleDefaultParameters;
end;

procedure TdxGaugeArcQuantitativeScaleStyleReader.ReadCommonScaleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  ACustomCircularParameters: TdxGaugeArcQuantitativeScaleDefaultParameters;
begin
  inherited ReadCommonScaleParameters(ANode, AParameters);
  ACustomCircularParameters := AParameters as TdxGaugeArcQuantitativeScaleDefaultParameters;
  ACustomCircularParameters.AngleEnd := GetAttributeValueAsInteger(GetChildNode(ANode, 'AngleEnd'), 'Value');
  ACustomCircularParameters.AngleStart := GetAttributeValueAsInteger(GetChildNode(ANode, 'AngleStart'), 'Value');
  ACustomCircularParameters.ScaleArcRadiusFactor := GetAttributeValueAsDouble(GetChildNode(ANode, 'ArcRadiusFactor'),
    'Value');
  ReadNeedleParameters(ANode, ACustomCircularParameters);
  ReadSpindleCapParameters(ANode, ACustomCircularParameters);
end;

procedure TdxGaugeArcQuantitativeScaleStyleReader.ReadNeedleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeArcQuantitativeScaleDefaultParameters);
var
  ANeedleNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'Needle', ANeedleNode) then
  begin
    AParameters.ValueIndicatorStartOffset := GetAttributeValueAsDouble(GetChildNode(ANeedleNode, 'StartOffset'), 'Value');
    AParameters.ValueIndicatorEndOffset := GetAttributeValueAsDouble(GetChildNode(ANeedleNode, 'EndOffset'), 'Value');
    if GetChildNode(ANeedleNode, 'ScaleFactor') <> nil then
      AParameters.ValueIndicatorScaleFactor := GetAttributeValueAsPointF(GetChildNode(ANeedleNode, 'ScaleFactor'), 'Value')
    else
      AParameters.ValueIndicatorScaleFactor := dxPointF(1, 1);
  end;
end;

procedure TdxGaugeArcQuantitativeScaleStyleReader.ReadSpindleCapParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeArcQuantitativeScaleDefaultParameters);
var
  ASpindleCapNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'SpindleCap', ASpindleCapNode) then
    AParameters.SpindleCapSize := GetAttributeValueAsDouble(GetChildNode(ASpindleCapNode, 'Size'), 'Value');
end;

{ TdxGaugeCustomCircularHalfScaleStyleReader }

class function TdxGaugeCustomCircularHalfScaleStyleReader.GetResourceNamePreffix: string;
begin
  Result := 'CircularHalf';
end;

function TdxGaugeCustomCircularHalfScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeCircularHalfScaleDefaultParameters;
end;

procedure TdxGaugeCustomCircularHalfScaleStyleReader.ReadCommonScaleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
begin
  inherited ReadCommonScaleParameters(ANode, AParameters);
  (AParameters as TdxGaugeCircularHalfScaleDefaultParameters).CenterPointPositionFactor :=
    GetAttributeValueAsPointF(GetChildNode(ANode,'CenterPointPositionFactor'), 'Value');
end;

{ TdxGaugeCircularScaleRangeViewInfo }

function TdxGaugeCircularScaleRangeViewInfo.GetRangeParametersClass: TdxGaugeCustomRangeParametersClass;
begin
  Result := TdxGaugeCircularRangeParameters;
end;

procedure TdxGaugeCircularScaleRangeViewInfo.CalculateBounds;

  function GetBounds: TdxRectF;
  begin
    Result := GetRangeParameters.ScaleBounds;
    if GetRangeParameters.RotatePoint.Y <= cxRectCenter(GetRangeParameters.ScaleBounds).Y then
      Result.Top := GetRangeParameters.RotatePoint.Y
    else
      Result.Bottom := GetRangeParameters.RotatePoint.Y;
    if GetRangeParameters.RotatePoint.X <= cxRectCenter(GetRangeParameters.ScaleBounds).X then
      Result.Left := GetRangeParameters.RotatePoint.X
    else
      Result.Right := GetRangeParameters.RotatePoint.X;
  end;

  function GetRadius(const ABounds: TdxRectF): Single;
  begin
    Result := Min(ABounds.Width, ABounds.Height) * GetRangeParameters.RadiusFactor;
  end;

var
  ABounds: TdxRectF;
begin
  ABounds := GetBounds;
  FBounds := cxRectInflate(cxRectF(GetRangeParameters.RotatePoint, GetRangeParameters.RotatePoint),
    GetRadius(ABounds), GetRadius(ABounds));
  FWidth := Min(ABounds.Width, ABounds.Height) * GetRangeParameters.WidthFactor;
end;

procedure TdxGaugeCircularScaleRangeViewInfo.DoDraw(AGPGraphics: TdxGPGraphics);

  function ValueToAngle(AValue: Single): Single;
  var
    ARangeParameters: TdxGaugeCircularRangeParameters;
  begin
    ARangeParameters := GetRangeParameters;
    Result := dxGaugeCisrcularScaleValueToAngle(AValue, ARangeParameters.MinValue, GetValueRange,
      ARangeParameters.AngleStart, ARangeParameters.AngleRange);
  end;

var
  ARangeParameters: TdxGaugeCircularRangeParameters;
  AStartAngle: Single;
begin
  ARangeParameters := GetRangeParameters;
  AStartAngle := ValueToAngle(ARangeParameters.ValueStart);
  dxGaugeDrawSector(AGPGraphics, FBounds, FWidth, AStartAngle,
    ValueToAngle(ARangeParameters.ValueEnd) - AStartAngle, ARangeParameters.Color, ARangeParameters.Color);
end;

function TdxGaugeCircularScaleRangeViewInfo.GetRangeParameters: TdxGaugeCircularRangeParameters;
begin
  Result := FRangeParameters as TdxGaugeCircularRangeParameters;
end;

{ TdxGaugeCircularScaleRange }

constructor TdxGaugeCircularScaleRange.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  GetRangeParameters.RadiusFactor := dxGaugeCircularScaleRangeRadiusFactor;
end;

function TdxGaugeCircularScaleRange.GetRangeParametersClass: TdxGaugeCustomRangeParametersClass;
begin
  Result := TdxGaugeCircularRangeParameters;
end;

function TdxGaugeCircularScaleRange.GetViewInfoClass: TdxGaugeCustomRangeViewInfoClass;
begin
  Result := TdxGaugeCircularScaleRangeViewInfo;
end;

procedure TdxGaugeCircularScaleRange.DefineProperties(AFiler: TFiler);
begin
  inherited DefineProperties(AFiler);
  AFiler.DefineProperty('RadiusFactor', ReadRadiusFactor, WriteRadiusFactor, SameValue(RadiusFactor, 0));
end;

function TdxGaugeCircularScaleRange.GetViewInfo: TdxGaugeCircularScaleRangeViewInfo;
begin
  Result := ViewInfo as TdxGaugeCircularScaleRangeViewInfo;
end;

function TdxGaugeCircularScaleRange.GetRadiusFactor: Single;
begin
  Result := GetRangeParameters.RadiusFactor;
end;

procedure TdxGaugeCircularScaleRange.SetRadiusFactor(const AValue: Single);
begin
  if GetRangeParameters.RadiusFactor <> AValue then
  begin
    GetRangeParameters.RadiusFactor := Min(Max(AValue, 0), 1);
    Changed(False);
  end;
end;

function TdxGaugeCircularScaleRange.GetRangeParameters: TdxGaugeCircularRangeParameters;
begin
  Result := RangeParameters as TdxGaugeCircularRangeParameters;
end;

procedure TdxGaugeCircularScaleRange.ReadRadiusFactor(AReader: TReader);
begin
  RadiusFactor := AReader.ReadDouble;
end;

procedure TdxGaugeCircularScaleRange.WriteRadiusFactor(AWriter: TWriter);
begin
  AWriter.WriteDouble(RadiusFactor);
end;

function TdxGaugeCircularScaleRange.IsRadiusFactorStored: Boolean;
begin
  Result := not SameValue(GetRangeParameters.RadiusFactor, dxGaugeCircularScaleRangeRadiusFactor);
end;

initialization
  dxGaugeScaleFactory.RegisterScale(TdxGaugeCircularScale);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeCircularHalfScale);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeCircularQuarterLeftScale);
  dxGaugeScaleFactory.RegisterScale(TdxGaugeCircularQuarterRightScale);

finalization
  dxGaugeUnregisterScales;

end.
