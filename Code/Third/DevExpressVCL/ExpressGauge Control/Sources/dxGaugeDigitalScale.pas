{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeDigitalScale;

{$I cxVer.inc}

interface

uses
  Classes, Generics.Collections, cxGeometry, dxCoreGraphics, cxGraphics, dxGDIPlusClasses, dxXMLDoc, dxCompositeShape,
  dxGaugeCustomScale;

type
  TdxGaugeCustomDigitalScale = class;
  TdxGaugeDigitalScaleDisplayMode = (sdmFourteenSegment, sdmSevenSegment);
  TdxGaugeDigitalScaleCustomSectionsController = class;
  TdxGaugeDigitalScaleCustomSectionsControllerClass = class of TdxGaugeDigitalScaleCustomSectionsController;
  TdxGaugeDigitalScaleCustomSectionCalculator = class;
  TdxGaugeDigitalScaleCustomSectionCalculatorClass = class of TdxGaugeDigitalScaleCustomSectionCalculator;

  { TdxGaugeDigitalScaleSection }

  TdxGaugeDigitalScaleSection = record
    Bounds: TdxRectF;
    Mask: Integer;
  end;

  TdxGaugeDigitalScaleSections = array of TdxGaugeDigitalScaleSection;

  { TdxGaugeDigitalScaleDefaultParameters }

  TdxGaugeDigitalScaleDefaultParameters = class(TdxGaugeCustomScaleDefaultParameters)
  public
    SegmentColorOff: TdxAlphaColor;
    SegmentColorOn: TdxAlphaColor;
  end;

  { TdxGaugeDigitalScaleParameters }

  TdxGaugeDigitalScaleParameters = class(TdxGaugeCustomScaleParameters)
  public
    DigitCount: Integer;
    DisplayMode: TdxGaugeDigitalScaleDisplayMode;
    SegmentColorOff: TdxAlphaColor;
    SegmentColorOn: TdxAlphaColor;
  end;

  { TdxGaugeDigitalScaleOptionsView }

  TdxGaugeDigitalScaleOptionsView = class(TdxGaugeCustomScaleOptionsView)
  private
    function GetDigitCount: Integer;
    function GetDisplayMode: TdxGaugeDigitalScaleDisplayMode;
    function GetSegmentColorOff: TdxAlphaColor;
    function GetSegmentColorOn: TdxAlphaColor;
    procedure SetDigitCount(const AValue: Integer);
    procedure SetDisplayMode(const AValue: TdxGaugeDigitalScaleDisplayMode);
    procedure SetSegmentColorOff(const AValue: TdxAlphaColor);
    procedure SetSegmentColorOn(const AValue: TdxAlphaColor);

    function GetScale: TdxGaugeCustomDigitalScale;
    function IsSegmentColorOffStored: Boolean;
    function IsSegmentColorOnStored: Boolean;
  published
    property DigitCount: Integer read GetDigitCount write SetDigitCount default 5;
    property DisplayMode: TdxGaugeDigitalScaleDisplayMode read GetDisplayMode write SetDisplayMode
      default sdmFourteenSegment;
    property SegmentColorOff: TdxAlphaColor read GetSegmentColorOff write SetSegmentColorOff stored IsSegmentColorOffStored
      default dxacDefault;
    property SegmentColorOn: TdxAlphaColor read GetSegmentColorOn write SetSegmentColorOn stored IsSegmentColorOnStored
      default dxacDefault;
  end;

  { TdxGaugeDigitalScaleViewInfo }

  TdxGaugeDigitalScaleViewInfo = class(TdxGaugeCustomScaleViewInfo)
  private
    FSectionsController: TdxGaugeDigitalScaleCustomSectionsController;

    FBackgroundLeftPart: TdxCompositeShape;
    FBackgroundMiddlePart: TdxCompositeShape;
    FBackgroundRightPart: TdxCompositeShape;

    FBackgroundLeftPartBounds: TdxRectF;
    FBackgroundMiddlePartBounds: TdxRectF;
    FBackgroundRightPartBounds: TdxRectF;

    function GetSectionsControllerClass(ADisplayMode: TdxGaugeDigitalScaleDisplayMode):
      TdxGaugeDigitalScaleCustomSectionsControllerClass;

    function GetRealDigitCount: Integer;
    function GetScaleDefaultParameters: TdxGaugeDigitalScaleDefaultParameters;
    function GetScaleParameters: TdxGaugeDigitalScaleParameters;
    function GetSegmentColorOff: TdxAlphaColor;
    function GetSegmentColorOn: TdxAlphaColor;
    function NeedRecalculateBackground: Boolean;
    procedure CalculateBackground;
    procedure CalculateProportionalBackground;
    procedure CalculateSections;
    procedure CalculateStretchableBackground;
    procedure CreateSectionsController(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
    procedure DrawSections(AGPGraphics: TdxGPGraphics);
    procedure RecalculateBackground;
    procedure RecreateSectionController(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
  protected
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;

    function GetContentBounds: TdxRectF; override;
    function GetContentOriginalSize: TdxSizeF; override;
    procedure CalculateContent; override;
    procedure DrawBackground(AGPGraphics: TdxGPGraphics); override;
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); override;
    procedure LoadScaleElements; override;
  public
    constructor Create(AScaleType: TdxGaugeScaleType; const AStyleName: string); override;
    destructor Destroy; override;
  end;

  { TdxGaugeCustomDigitalScale }

  TdxGaugeCustomDigitalScale = class(TdxGaugeCustomScale)
  private
    function GetDigitCount: Integer;
    function GetDisplayMode: TdxGaugeDigitalScaleDisplayMode;
    function GetOptionsView: TdxGaugeDigitalScaleOptionsView;
    function GetSegmentColorOff: TdxAlphaColor;
    function GetSegmentColorOn: TdxAlphaColor;
    function GetValue: string;
    procedure SetDigitCount(const AValue: Integer);
    procedure SetDisplayMode(const AValue: TdxGaugeDigitalScaleDisplayMode);
    procedure SetOptionsView(const AValue: TdxGaugeDigitalScaleOptionsView);
    procedure SetSegmentColorOff(const AValue: TdxAlphaColor);
    procedure SetSegmentColorOn(const AValue: TdxAlphaColor);
    procedure SetValue(const AValue: string);

    function GetScaleParameters: TdxGaugeDigitalScaleParameters;
    function GetViewInfo: TdxGaugeDigitalScaleViewInfo;
    function IsDefaultColor(const AColor: TdxAlphaColor): Boolean;

    function IsSegmentColorOffStored: Boolean;
    function IsSegmentColorOnStored: Boolean;
    function IsValueStored: Boolean;
  protected
    class function GetLayerCount: Integer; override;
    class function GetScaleName: string; override;
    class function GetScaleType: TdxGaugeScaleType; override;

    function GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass; override;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;

    procedure DoAssign(AScale: TdxGaugeCustomScale); override;
    procedure InitScaleParameters; override;
    procedure InternalRestoreStyleParameters; override;

    property OptionsView: TdxGaugeDigitalScaleOptionsView read GetOptionsView write SetOptionsView;
    property Value: string read GetValue write SetValue stored IsValueStored;
  public
    constructor Create(AOwner: TComponent); override;
  end;

  { TdxGaugeDigitalScale }

  TdxGaugeDigitalScale = class(TdxGaugeCustomDigitalScale)
  published
    property AnchorScaleIndex;
    property OptionsLayout;
    property OptionsView;
    property StyleName;
    property Value;
    property Visible;
  end;

  { TdxGaugeCustomDigitalScaleStyleReader }

  TdxGaugeCustomDigitalScaleStyleReader = class(TdxGaugeCustomScaleStyleReader)
  protected
    class function GetResourceNamePreffix: string; override;
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters); override;
  end;

  { TdxGaugeDigitalScaleCustomSectionCalculator }

  TdxGaugeDigitalScaleCustomSectionCalculator = class
  private
    FCharset: TDictionary<Char, Integer>;

    function GetCurrentChar(const AText: string; AIndex: Integer): Char;
    function GetPreviousChar(const AText: string; AIndex: Integer): Char;
    function GetNextChar(const AText: string; AIndex: Integer): Char;
    function IsBottomPoint(AChar: Char): Boolean;
    function IsColon(AChar: Char): Boolean;
    function IsEmpty(AChar: Char): Boolean;
    function IsPointOrColon(AChar: Char): Boolean;
    function SetBitState(const AFlags: Integer; AIndex: Integer): Integer;
  protected
    function AcceptChar(APreviousChar, ACurrentChar, ANextChar: Char): Boolean; virtual;
    function GetMask(APreviousChar, ACurrentChar, ANextChar: Char; var ACharIndex: Integer): Integer; virtual;
    function IsNonZeroLengthSymbol(APreviousChar, ACurrentChar, ANextChar: Char): Boolean; virtual;
    function IsPoint(AChar: Char): Boolean; virtual;
    procedure AppendColonLeftPartMask(var AMask: Integer); virtual; abstract;
    procedure AppendColonRightPartMask(var AMask: Integer); virtual; abstract;
    procedure AppendBottomPointMask(var AMask: Integer); virtual; abstract;
    procedure InitCharset; virtual;

    procedure Calculate(const AText: string; var AMasks: array of Integer);
  public
    constructor Create;
    destructor Destroy; override;
  end;

  { TdxGaugeDigitalScaleSevenSegmentSectionCalculator }

  TdxGaugeDigitalScaleSevenSegmentSectionCalculator = class(TdxGaugeDigitalScaleCustomSectionCalculator)
  protected
    procedure AppendColonLeftPartMask(var AMask: Integer); override;
    procedure AppendColonRightPartMask(var AMask: Integer); override;
    procedure AppendBottomPointMask(var AMask: Integer); override;
    procedure InitCharset; override;
  end;

  { TdxGaugeDigitalScaleFourteenSegmentSectionCalculator }

  TdxGaugeDigitalScaleFourteenSegmentSectionCalculator = class(TdxGaugeDigitalScaleCustomSectionCalculator)
  private
    function IsTopPoint(AChar: Char): Boolean;
    procedure AppendTopPointMask(var AMask: Integer);
  protected
    function AcceptChar(APreviousChar, ACurrentChar, ANextChar: Char): Boolean; override;
    function GetMask(APreviousChar, ACurrentChar, ANextChar: Char; var ACharIndex: Integer): Integer; override;
    function IsNonZeroLengthSymbol(APreviousChar, ACurrentChar, ANextChar: Char): Boolean; override;
    function IsPoint(AChar: Char): Boolean; override;
    procedure AppendColonLeftPartMask(var AMask: Integer); override;
    procedure AppendColonRightPartMask(var AMask: Integer); override;
    procedure AppendBottomPointMask(var AMask: Integer); override;
    procedure InitCharset; override;
  end;

  { TdxGaugeDigitalScaleCustomSectionsController }

  TdxGaugeDigitalScaleCustomSectionsController = class
  private
    FDigit: TdxCompositeShape;
    FSections: TdxGaugeDigitalScaleSections;

    function GetBitState(const AFlags: Integer; AIndex: Integer): Boolean;
    function GetDigitSize: TdxSizeF;
    function GetOffset: Single;
    function GetSectionSize: TdxSizeF;
    procedure LoadDigit;
  protected
    function GetCalculatorClass: TdxGaugeDigitalScaleCustomSectionCalculatorClass; virtual; abstract;

    function GetDigitResourceName: string; virtual; abstract;
    function GetSegmentCount: Integer; virtual; abstract;
    function GetSegmentName(ASegmentIndex: Integer): string; virtual; abstract;

    function GetBackgroundOriginalSize(ASectionCount: Integer): TdxSizeF;
    function GetBackgroundPartWidth(AScaleFactor: TdxPointF): Single;
    procedure CalculateSectionsBounds(const ABounds: TdxRectF; ASectionCount: Integer; AStretchFactor: Single);
    procedure CalculateSectionsMasks(const AScaleTextValue: string);
    procedure ColorizeDigit(const ASectionMask: Integer; AColorOn, AColorOff: TdxAlphaColor);
    procedure DrawSections(AGPGraphics: TdxGPGraphics; AColorOn, AColorOff: TdxAlphaColor);
  public
    constructor Create;
    destructor Destroy; override;
  end;

  { TdxGaugeDigitalScaleSevenSegmentSectionsController }

  TdxGaugeDigitalScaleSevenSegmentSectionsController = class(TdxGaugeDigitalScaleCustomSectionsController)
  protected
    function GetCalculatorClass: TdxGaugeDigitalScaleCustomSectionCalculatorClass; override;
    function GetDigitResourceName: string; override;
    function GetSegmentCount: Integer; override;
    function GetSegmentName(ASegmentIndex: Integer): string; override;
  end;

  { TdxGaugeDigitalScaleFourteenSegmentSectionsController }

  TdxGaugeDigitalScaleFourteenSegmentSectionsController = class(TdxGaugeDigitalScaleCustomSectionsController)
  protected
    function GetCalculatorClass: TdxGaugeDigitalScaleCustomSectionCalculatorClass; override;
    function GetDigitResourceName: string; override;
    function GetSegmentCount: Integer; override;
    function GetSegmentName(ASegmentIndex: Integer): string; override;
  end;

implementation

uses
  Types, SysUtils, Math, dxCore, dxGaugeUtils;

{$R DigitalSegments.res}

const
  dxGaugeDigitalScaleDefaultValue = '0';
  dxGaugeDigitalScaleDigitCount = 5;
  dxGaugeDigitalScaleDigitOffset = 0.35;
  dxGaugeDigitalScaleMaxDigitCount = 255;

type
  TdxCompositeShapeAccess = class(TdxCompositeShape);
  TdxGaugeScaleStyleAccess = class(TdxGaugeScaleStyle);

{ TdxGaugeDigitalScaleOptionsView }

function TdxGaugeDigitalScaleOptionsView.GetDigitCount: Integer;
begin
  Result := GetScale.GetDigitCount;
end;

function TdxGaugeDigitalScaleOptionsView.GetDisplayMode: TdxGaugeDigitalScaleDisplayMode;
begin
  Result := GetScale.GetDisplayMode;
end;

function TdxGaugeDigitalScaleOptionsView.GetSegmentColorOff: TdxAlphaColor;
begin
  Result := GetScale.GetSegmentColorOff;
end;

function TdxGaugeDigitalScaleOptionsView.GetSegmentColorOn: TdxAlphaColor;
begin
  Result := GetScale.GetSegmentColorOn;
end;

procedure TdxGaugeDigitalScaleOptionsView.SetDigitCount(const AValue: Integer);
begin
  GetScale.SetDigitCount(AValue);
end;

procedure TdxGaugeDigitalScaleOptionsView.SetDisplayMode(const AValue: TdxGaugeDigitalScaleDisplayMode);
begin
  GetScale.SetDisplayMode(AValue);
end;

procedure TdxGaugeDigitalScaleOptionsView.SetSegmentColorOff(const AValue: TdxAlphaColor);
begin
  GetScale.SetSegmentColorOff(AValue);
end;

procedure TdxGaugeDigitalScaleOptionsView.SetSegmentColorOn(const AValue: TdxAlphaColor);
begin
  GetScale.SetSegmentColorOn(AValue);
end;

function TdxGaugeDigitalScaleOptionsView.GetScale: TdxGaugeCustomDigitalScale;
begin
  Result := inherited GetScale as TdxGaugeCustomDigitalScale;
end;

function TdxGaugeDigitalScaleOptionsView.IsSegmentColorOffStored: Boolean;
begin
  Result := GetScale.IsSegmentColorOffStored;
end;

function TdxGaugeDigitalScaleOptionsView.IsSegmentColorOnStored: Boolean;
begin
  Result := GetScale.IsSegmentColorOnStored;
end;

{ TdxGaugeDigitalScaleViewInfo }

constructor TdxGaugeDigitalScaleViewInfo.Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  inherited Create(AScaleType, AStyleName);
  CreateSectionsController(GetScaleParameters.DisplayMode);
end;

destructor TdxGaugeDigitalScaleViewInfo.Destroy;
begin
  FreeAndNil(FSectionsController);
  inherited Destroy;
end;

function TdxGaugeDigitalScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeDigitalScaleParameters;
end;

function TdxGaugeDigitalScaleViewInfo.GetContentBounds: TdxRectF;
begin
  Result := cxRectUnion(cxRectUnion(FBackgroundLeftPartBounds, FBackgroundMiddlePartBounds), FBackgroundRightPartBounds);
end;

function TdxGaugeDigitalScaleViewInfo.GetContentOriginalSize: TdxSizeF;
begin
  Result := FSectionsController.GetBackgroundOriginalSize(GetRealDigitCount);
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateContent;
begin
  inherited CalculateContent;
  CalculateBackground;
  if NeedRecalculateBackground then
    RecalculateBackground;
  CalculateSections;
end;

procedure TdxGaugeDigitalScaleViewInfo.DrawBackground(AGPGraphics: TdxGPGraphics);
begin
  if GetScaleParameters.ShowBackground then
  begin
    dxGaugeDrawImage(AGPGraphics, FBackgroundLeftPart, FBackgroundLeftPartBounds);
    dxGaugeDrawImage(AGPGraphics, FBackgroundMiddlePart, FBackgroundMiddlePartBounds);
    dxGaugeDrawImage(AGPGraphics, FBackgroundRightPart, FBackgroundRightPartBounds);
  end;
end;

procedure TdxGaugeDigitalScaleViewInfo.DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  inherited DrawLayer(AGPGraphics, ALayerIndex);
  if ALayerIndex = 2 then
    DrawSections(AGPGraphics);
end;

procedure TdxGaugeDigitalScaleViewInfo.LoadScaleElements;
begin
  inherited LoadScaleElements;
  FBackgroundLeftPart := TdxGaugeScaleStyleAccess(Style).GetElement(etDigitalBackgroundStart);
  FBackgroundMiddlePart := TdxGaugeScaleStyleAccess(Style).GetElement(etDigitalBackgroundMiddle);
  FBackgroundRightPart := TdxGaugeScaleStyleAccess(Style).GetElement(etDigitalBackgroundEnd);
end;

function TdxGaugeDigitalScaleViewInfo.GetSectionsControllerClass(ADisplayMode: TdxGaugeDigitalScaleDisplayMode):
  TdxGaugeDigitalScaleCustomSectionsControllerClass;
begin
  case ADisplayMode of
    sdmSevenSegment:
      Result := TdxGaugeDigitalScaleSevenSegmentSectionsController;
    sdmFourteenSegment:
      Result := TdxGaugeDigitalScaleFourteenSegmentSectionsController;
    else
      Result := nil;
  end;
end;

function TdxGaugeDigitalScaleViewInfo.GetRealDigitCount: Integer;

  function GetText: string;
  begin
    Result := dxVariantToString(GetScaleParameters.Value);
    if Length(Result) = 0 then
      Result := ' ';
  end;

begin
  if GetScaleParameters.DigitCount = 0 then
    Result := Min(Length(GetText), dxGaugeDigitalScaleMaxDigitCount)
  else
    Result := GetScaleParameters.DigitCount;
end;

function TdxGaugeDigitalScaleViewInfo.GetScaleDefaultParameters: TdxGaugeDigitalScaleDefaultParameters;
begin
  Result := ScaleDefaultParameters as TdxGaugeDigitalScaleDefaultParameters;
end;

function TdxGaugeDigitalScaleViewInfo.GetScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  Result := FScaleParameters as TdxGaugeDigitalScaleParameters;
end;

function TdxGaugeDigitalScaleViewInfo.GetSegmentColorOff: TdxAlphaColor;
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.SegmentColorOff = dxacDefault then
    Result := GetScaleDefaultParameters.SegmentColorOff
  else
    Result := AScaleParameters.SegmentColorOff;
end;

function TdxGaugeDigitalScaleViewInfo.GetSegmentColorOn: TdxAlphaColor;
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.SegmentColorOn = dxacDefault then
    Result := GetScaleDefaultParameters.SegmentColorOn
  else
    Result := AScaleParameters.SegmentColorOn;
end;

function TdxGaugeDigitalScaleViewInfo.NeedRecalculateBackground: Boolean;
begin
  Result := GetContentBounds.Width > FBounds.Width;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateBackground;
begin
  if not GetScaleParameters.Stretch then
    CalculateProportionalBackground
  else
    CalculateStretchableBackground;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateProportionalBackground;

  function GetBackgroundPartOriginalSizeRatio(ABackgroundPart: TdxCompositeShape): Single;
  begin
    Result := TdxCompositeShapeAccess(ABackgroundPart).WidthF / TdxCompositeShapeAccess(ABackgroundPart).HeightF;
  end;

var
  AScaledBounds: TdxRectF;
  ABackgroundPartWidth: Single;
begin
  AScaledBounds := GetScaleRect(GetContentOriginalSize);
  ABackgroundPartWidth := FSectionsController.GetBackgroundPartWidth(FScaleFactor);

  FBackgroundMiddlePartBounds := AScaledBounds;
  FBackgroundMiddlePartBounds.Left := FBackgroundMiddlePartBounds.Left + ABackgroundPartWidth;
  FBackgroundMiddlePartBounds.Right := FBackgroundMiddlePartBounds.Right - ABackgroundPartWidth;

  FBackgroundLeftPartBounds := FBackgroundMiddlePartBounds;
  FBackgroundLeftPartBounds.Left := FBackgroundLeftPartBounds.Left - AScaledBounds.Height *
    GetBackgroundPartOriginalSizeRatio(FBackgroundLeftPart);

  FBackgroundLeftPartBounds.Right := FBackgroundMiddlePartBounds.Left + 1;

  FBackgroundRightPartBounds := FBackgroundMiddlePartBounds;
  FBackgroundRightPartBounds.Right := FBackgroundRightPartBounds.Right + AScaledBounds.Height *
    GetBackgroundPartOriginalSizeRatio(FBackgroundRightPart);

  FBackgroundRightPartBounds.Left := FBackgroundMiddlePartBounds.Right - 1;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateSections;

  function GetStretchFactor: Single;
  begin
    if GetScaleParameters.Stretch then
      Result := FScaleFactor.Y
    else
      Result := 0;
  end;

begin
  FSectionsController.CalculateSectionsBounds(FBackgroundMiddlePartBounds, GetRealDigitCount, GetStretchFactor);
  FSectionsController.CalculateSectionsMasks(dxVariantToString(GetScaleParameters.Value));
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateStretchableBackground;
var
  AScaledBounds: TdxRectF;
  ABackgroundPartWidth: Single;
begin
  AScaledBounds := GetScaleRect(GetContentOriginalSize);
  ABackgroundPartWidth := FSectionsController.GetBackgroundPartWidth(FScaleFactor);

  FBackgroundLeftPartBounds := AScaledBounds;
  FBackgroundLeftPartBounds.Right := FBackgroundLeftPartBounds.Left + ABackgroundPartWidth;

  FBackgroundRightPartBounds := AScaledBounds;
  FBackgroundRightPartBounds.Left := FBackgroundRightPartBounds.Right - ABackgroundPartWidth;

  FBackgroundMiddlePartBounds := AScaledBounds;
  FBackgroundMiddlePartBounds.Left := FBackgroundLeftPartBounds.Right - 1;
  FBackgroundMiddlePartBounds.Right := FBackgroundRightPartBounds.Left + 1;
end;

procedure TdxGaugeDigitalScaleViewInfo.CreateSectionsController(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
begin
  FreeAndNil(FSectionsController);
  FSectionsController := GetSectionsControllerClass(ADisplayMode).Create;
end;

procedure TdxGaugeDigitalScaleViewInfo.DrawSections(AGPGraphics: TdxGPGraphics);
begin
  FSectionsController.DrawSections(AGPGraphics, GetSegmentColorOn, GetSegmentColorOff);
end;

procedure TdxGaugeDigitalScaleViewInfo.RecalculateBackground;
var
  ARation: Single;
begin
  ARation := FBounds.Width / GetContentBounds.Width;
  FScaleFactor.X := FScaleFactor.X * ARation;
  FScaleFactor.Y := FScaleFactor.Y * ARation;
  CalculateBackground;
end;

procedure TdxGaugeDigitalScaleViewInfo.RecreateSectionController(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
begin
  FreeAndNil(FSectionsController);
  CreateSectionsController(ADisplayMode);
end;

{ TdxGaugeCustomDigitalScale }

constructor TdxGaugeCustomDigitalScale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  GetScaleParameters.Value := dxGaugeDigitalScaleDefaultValue;
end;

class function TdxGaugeCustomDigitalScale.GetLayerCount: Integer;
begin
  Result := 3;
end;

class function TdxGaugeCustomDigitalScale.GetScaleName: string;
begin
  Result := 'Digital';
end;

class function TdxGaugeCustomDigitalScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stDigitalScale;
end;

function TdxGaugeCustomDigitalScale.GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass;
begin
  Result := TdxGaugeDigitalScaleOptionsView;
end;

function TdxGaugeCustomDigitalScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeDigitalScaleParameters;
end;

function TdxGaugeCustomDigitalScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeDigitalScaleViewInfo;
end;

procedure TdxGaugeCustomDigitalScale.DoAssign(AScale: TdxGaugeCustomScale);
begin
  OptionsView.DisplayMode := (AScale as TdxGaugeCustomDigitalScale).OptionsView.DisplayMode;
  inherited DoAssign(AScale);
end;

procedure TdxGaugeCustomDigitalScale.InitScaleParameters;
begin
  inherited InitScaleParameters;
  GetScaleParameters.SegmentColorOff := dxacDefault;
  GetScaleParameters.SegmentColorOn := dxacDefault;
  GetScaleParameters.DigitCount := dxGaugeDigitalScaleDigitCount;
end;

procedure TdxGaugeCustomDigitalScale.InternalRestoreStyleParameters;
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if not IsDefaultColor(AScaleParameters.SegmentColorOff) then
    AScaleParameters.SegmentColorOff := dxacDefault;
  if not IsDefaultColor(AScaleParameters.SegmentColorOn) then
    AScaleParameters.SegmentColorOn := dxacDefault;
  inherited InternalRestoreStyleParameters;
end;

function TdxGaugeCustomDigitalScale.GetDigitCount: Integer;
begin
  Result := GetScaleParameters.DigitCount;
end;

function TdxGaugeCustomDigitalScale.GetDisplayMode: TdxGaugeDigitalScaleDisplayMode;
begin
  Result := GetScaleParameters.DisplayMode;
end;

function TdxGaugeCustomDigitalScale.GetOptionsView: TdxGaugeDigitalScaleOptionsView;
begin
  Result := FOptionsView as TdxGaugeDigitalScaleOptionsView;
end;

function TdxGaugeCustomDigitalScale.GetSegmentColorOff: TdxAlphaColor;
begin
  Result := GetScaleParameters.SegmentColorOff;
end;

function TdxGaugeCustomDigitalScale.GetSegmentColorOn: TdxAlphaColor;
begin
  Result := GetScaleParameters.SegmentColorOn;
end;

function TdxGaugeCustomDigitalScale.GetValue: string;
begin
  Result := dxVariantToString(GetScaleParameters.Value);
end;

procedure TdxGaugeCustomDigitalScale.SetDigitCount(const AValue: Integer);
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if (AScaleParameters.DigitCount <> AValue) and (AValue >= 0) then
  begin
    AScaleParameters.DigitCount := Max(Min(AValue, dxGaugeDigitalScaleMaxDigitCount), 0);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomDigitalScale.SetDisplayMode(const AValue: TdxGaugeDigitalScaleDisplayMode);
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.DisplayMode <> AValue then
  begin
    AScaleParameters.DisplayMode := AValue;
    GetViewInfo.RecreateSectionController(AScaleParameters.DisplayMode);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomDigitalScale.SetOptionsView(const AValue: TdxGaugeDigitalScaleOptionsView);
begin
  FOptionsView.Assign(AValue);
end;

procedure TdxGaugeCustomDigitalScale.SetSegmentColorOff(const AValue: TdxAlphaColor);
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.SegmentColorOff <> AValue then
  begin
    AScaleParameters.SegmentColorOff := AValue;
    ScaleChanged([sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomDigitalScale.SetSegmentColorOn(const AValue: TdxAlphaColor);
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.SegmentColorOn <> AValue then
  begin
    AScaleParameters.SegmentColorOn := AValue;
    ScaleChanged([sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomDigitalScale.SetValue(const AValue: string);
begin
  if GetScaleParameters.DigitCount = 0 then
    Include(FChangedLayers, sclStaticLayer);
  InternalSetValue(AValue);
end;

function TdxGaugeCustomDigitalScale.GetScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  Result := ScaleParameters as TdxGaugeDigitalScaleParameters;
end;

function TdxGaugeCustomDigitalScale.GetViewInfo: TdxGaugeDigitalScaleViewInfo;
begin
  Result := ViewInfo as TdxGaugeDigitalScaleViewInfo;
end;

function TdxGaugeCustomDigitalScale.IsDefaultColor(const AColor: TdxAlphaColor): Boolean;
begin
  Result := AColor = dxacDefault;
end;

function TdxGaugeCustomDigitalScale.IsSegmentColorOffStored: Boolean;
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  Result := not IsDefaultColor(AScaleParameters.SegmentColorOff) and
    (AScaleParameters.SegmentColorOff <> GetViewInfo.GetScaleDefaultParameters.SegmentColorOff);
end;

function TdxGaugeCustomDigitalScale.IsSegmentColorOnStored: Boolean;
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  Result := not IsDefaultColor(AScaleParameters.SegmentColorOn) and
    (AScaleParameters.SegmentColorOn <> GetViewInfo.GetScaleDefaultParameters.SegmentColorOn);
end;

function TdxGaugeCustomDigitalScale.IsValueStored: Boolean;
begin
  Result := not SameText(dxVariantToString(GetScaleParameters.Value), dxGaugeDigitalScaleDefaultValue);
end;

{ TdxGaugeCustomDigitalScaleStyleReader }

class function TdxGaugeCustomDigitalScaleStyleReader.GetResourceNamePreffix: string;
begin
  Result := 'Digital';
end;

function TdxGaugeCustomDigitalScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeDigitalScaleDefaultParameters;
end;

procedure TdxGaugeCustomDigitalScaleStyleReader.ReadParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCustomScaleDefaultParameters);
var
  AScaleParameters: TdxGaugeDigitalScaleDefaultParameters;
begin
  AScaleParameters := AParameters as TdxGaugeDigitalScaleDefaultParameters;
  AScaleParameters.SegmentColorOff := GetAttributeValueAsAlphaColor(GetChildNode(ANode, 'SegmentColorOff'), 'Value');
  AScaleParameters.SegmentColorOn := GetAttributeValueAsAlphaColor(GetChildNode(ANode, 'SegmentColorOn'), 'Value');
end;

{ TdxGaugeDigitalScaleCustomSectionCalculator }

constructor TdxGaugeDigitalScaleCustomSectionCalculator.Create;
begin
  inherited Create;
  InitCharset;
end;

destructor TdxGaugeDigitalScaleCustomSectionCalculator.Destroy;
begin
  FreeAndNil(FCharset);
  inherited Destroy;
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.AcceptChar(APreviousChar, ACurrentChar, ANextChar: Char): Boolean;
begin
  Result := FCharset.ContainsKey(ACurrentChar) or not (IsEmpty(APreviousChar) and IsColon(ACurrentChar)) and
    (IsNonZeroLengthSymbol(APreviousChar, ACurrentChar, ANextChar) or IsNonZeroLengthSymbol(ANextChar, ACurrentChar,
    APreviousChar));
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.GetMask(APreviousChar, ACurrentChar, ANextChar: Char;
  var ACharIndex: Integer): Integer;
begin
  FCharset.TryGetValue(ACurrentChar, Result);

  if IsBottomPoint(ACurrentChar) then
  begin
    if FCharset.TryGetValue(APreviousChar, Result) then
      Dec(ACharIndex);
    AppendBottomPointMask(Result);
  end;

  if IsBottomPoint(APreviousChar) and not FCharset.ContainsKey(ACurrentChar) or
    IsBottomPoint(ANextChar) and not IsColon(ACurrentChar) then
      AppendBottomPointMask(Result);

  if IsColon(ACurrentChar) then
  begin
    if FCharset.TryGetValue(APreviousChar, Result) or IsBottomPoint(APreviousChar) then
    begin
      Dec(ACharIndex);
      if IsBottomPoint(APreviousChar) then
        AppendBottomPointMask(Result);
    end;
    AppendColonLeftPartMask(Result);
  end;

  if IsColon(ANextChar) then
    AppendColonLeftPartMask(Result);

  if IsColon(APreviousChar) then
    AppendColonRightPartMask(Result);
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.IsNonZeroLengthSymbol(APreviousChar, ACurrentChar,
  ANextChar: Char): Boolean;
begin
  Result := IsBottomPoint(APreviousChar) and IsBottomPoint(ACurrentChar) or
    IsColon(APreviousChar) and IsColon(ACurrentChar) or
    IsColon(APreviousChar) and IsBottomPoint(ACurrentChar) or
    IsColon(APreviousChar) or
    IsBottomPoint(ACurrentChar) and FCharset.ContainsKey(ANextChar) or
    IsPointOrColon(ACurrentChar) and IsEmpty(APreviousChar) and IsEmpty(ANextChar);
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.IsPoint(AChar: Char): Boolean;
begin
  Result := IsBottomPoint(AChar);
end;

procedure TdxGaugeDigitalScaleCustomSectionCalculator.InitCharset;
begin
  FCharset := TDictionary<Char, Integer>.Create;
end;

procedure TdxGaugeDigitalScaleCustomSectionCalculator.Calculate(const AText: string; var AMasks: array of Integer);
var
  S: string;
  ACurrentChar, APreviousChar, ANextChar: Char;
  ACharIndex, AMaskIndex: Integer;
begin
  S := #0 + UpperCase(AText);
  ACharIndex := Length(S);
  AMaskIndex := High(AMasks);
  while (ACharIndex > 0) and (AMaskIndex >= 0) do
  begin
    ACurrentChar := GetCurrentChar(S, ACharIndex);
    APreviousChar := GetPreviousChar(S, ACharIndex);
    ANextChar := GetNextChar(S, ACharIndex);
    if AcceptChar(APreviousChar, ACurrentChar, ANextChar) then
    begin
      AMasks[AMaskIndex] := GetMask(APreviousChar, ACurrentChar, ANextChar, ACharIndex);
      if IsColon(GetPreviousChar(S, ACharIndex)) and (FCharset.ContainsKey(APreviousChar) or IsPoint(APreviousChar)) then
        AppendColonRightPartMask(AMasks[AMaskIndex]);
      Dec(AMaskIndex);
    end;
    Dec(ACharIndex);
  end;
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.GetCurrentChar(const AText: string; AIndex: Integer): Char;
begin
  if (AIndex > 0) and (AIndex <= Length(AText)) then
    Result := AText[AIndex]
  else
    Result := #0;
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.GetPreviousChar(const AText: string; AIndex: Integer): Char;
begin
  if AIndex < 1  then
    Result := #0
  else
    Result := AText[AIndex - 1];
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.GetNextChar(const AText: string; AIndex: Integer): Char;
begin
  if AIndex = Length(AText) then
    Result := #0
  else
    Result := AText[AIndex + 1];
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.IsBottomPoint(AChar: Char): Boolean;
begin
  Result := (AChar = '.') or (AChar = ',');
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.IsColon(AChar: Char): Boolean;
begin
  Result := (AChar = ':') or (AChar = ';');
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.IsEmpty(AChar: Char): Boolean;
begin
  Result := AChar = #0;
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.IsPointOrColon(AChar: Char): Boolean;
begin
  Result := IsPoint(AChar) or IsColon(AChar);
end;

function TdxGaugeDigitalScaleCustomSectionCalculator.SetBitState(const AFlags: Integer; AIndex: Integer): Integer;
begin
  Result := AFlags or (1 shl AIndex);
end;

{ TdxGaugeDigitalScaleSevenSegmentSectionCalculator }

procedure TdxGaugeDigitalScaleSevenSegmentSectionCalculator.AppendColonLeftPartMask(var AMask: Integer);
begin
  AMask := SetBitState(AMask, 10);
  AMask := SetBitState(AMask, 11);
end;

procedure TdxGaugeDigitalScaleSevenSegmentSectionCalculator.AppendColonRightPartMask(var AMask: Integer);
begin
  AMask := SetBitState(AMask, 8);
  AMask := SetBitState(AMask, 9);
end;

procedure TdxGaugeDigitalScaleSevenSegmentSectionCalculator.AppendBottomPointMask(var AMask: Integer);
begin
  AMask := SetBitState(AMask, 7);
end;

procedure TdxGaugeDigitalScaleSevenSegmentSectionCalculator.InitCharset;
begin
  inherited InitCharset;
  FCharset.AddOrSetValue('0', $7E); 
  FCharset.AddOrSetValue('1', $30); 
  FCharset.AddOrSetValue('2', $6D); 
  FCharset.AddOrSetValue('3', $79); 
  FCharset.AddOrSetValue('4', $33); 
  FCharset.AddOrSetValue('5', $5B); 
  FCharset.AddOrSetValue('6', $5F); 
  FCharset.AddOrSetValue('7', $70); 
  FCharset.AddOrSetValue('8', $7F); 
  FCharset.AddOrSetValue('9', $7B); 
  FCharset.AddOrSetValue('-', $1);  
  FCharset.AddOrSetValue('_', $8);  
  FCharset.AddOrSetValue(' ', $0);  
end;

{ TdxGaugeDigitalScaleFourteenSegmentSectionCalculator }

function TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.AcceptChar(APreviousChar, ACurrentChar,
  ANextChar: Char): Boolean;
begin
  Result := inherited AcceptChar(APreviousChar, ACurrentChar, ANextChar) and
    not (IsEmpty(ANextChar) and IsColon(ACurrentChar) and IsTopPoint(APreviousChar) or
    IsEmpty(APreviousChar) and IsEmpty(ACurrentChar) and IsTopPoint(ANextChar));
end;

function TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.GetMask(APreviousChar, ACurrentChar, ANextChar: Char;
  var ACharIndex: Integer): Integer;
begin
  Result := inherited GetMask(APreviousChar, ACurrentChar, ANextChar, ACharIndex);

  if IsTopPoint(ACurrentChar) and IsEmpty(ANextChar) and IsBottomPoint(APreviousChar) then
  begin
    Result := 0;
    AppendTopPointMask(Result);
  end;

  if IsTopPoint(ACurrentChar) or IsTopPoint(APreviousChar) or
    IsTopPoint(ACurrentChar) and IsColon(APreviousChar) and IsEmpty(ANextChar) then
  begin
    AppendTopPointMask(Result);
    if IsTopPoint(ACurrentChar) and IsColon(APreviousChar) then
      AppendColonRightPartMask(Result);
  end;

  if IsTopPoint(APreviousChar) and (IsColon(ACurrentChar) or FCharset.ContainsKey(ACurrentChar)) or
    IsTopPoint(ACurrentChar) and IsEmpty(APreviousChar) and IsEmpty(ANextChar) or
    IsTopPoint(ANextChar) and IsEmpty(ACurrentChar) and IsEmpty(APreviousChar) then
  begin
    AppendTopPointMask(Result);
    Dec(ACharIndex);
  end;
end;

function TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.IsNonZeroLengthSymbol(APreviousChar,
  ACurrentChar, ANextChar: Char): Boolean;
begin
  Result := inherited IsNonZeroLengthSymbol(APreviousChar, ACurrentChar, ANextChar) or
    IsBottomPoint(ACurrentChar) or IsTopPoint(APreviousChar) or
    IsTopPoint(ACurrentChar) and (IsEmpty(ANextChar) or IsEmpty(APreviousChar));
end;

function TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.IsPoint(AChar: Char): Boolean;
begin
  Result := inherited IsBottomPoint(AChar) or IsTopPoint(AChar);
end;

function TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.IsTopPoint(AChar: Char): Boolean;
begin
  Result := (AChar = '"') or (AChar = '''') or (AChar = '*') or (AChar = '`');
end;

procedure TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.AppendTopPointMask(var AMask: Integer);
begin
  AMask := SetBitState(AMask, 15);
end;

procedure TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.AppendColonLeftPartMask(var AMask: Integer);
begin
  AMask := SetBitState(AMask, 16);
  AMask := SetBitState(AMask, 17);
end;

procedure TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.AppendColonRightPartMask(var AMask: Integer);
begin
  AMask := SetBitState(AMask, 18);
  AMask := SetBitState(AMask, 19);
end;

procedure TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.AppendBottomPointMask(var AMask: Integer);
begin
  AMask := SetBitState(AMask, 14);
end;

procedure TdxGaugeDigitalScaleFourteenSegmentSectionCalculator.InitCharset;
begin
  inherited InitCharset;
  FCharset.AddOrSetValue('0', $3F22); 
  FCharset.AddOrSetValue('1', $1800); 
  FCharset.AddOrSetValue('2', $3611); 
  FCharset.AddOrSetValue('3', $3C11); 
  FCharset.AddOrSetValue('4', $1911); 
  FCharset.AddOrSetValue('5', $2D11); 
  FCharset.AddOrSetValue('6', $2F11); 
  FCharset.AddOrSetValue('7', $3800); 
  FCharset.AddOrSetValue('8', $3F11); 
  FCharset.AddOrSetValue('9', $3D11); 
  FCharset.AddOrSetValue('-', $11);   
  FCharset.Add('~', $11);             
  FCharset.AddOrSetValue('+', $55);   
  FCharset.AddOrSetValue('_', $400);  
  FCharset.AddOrSetValue('/', $22);   
  FCharset.AddOrSetValue('\', $88);   
  FCharset.AddOrSetValue('|', $44);   
  FCharset.AddOrSetValue(' ', $0);    
  FCharset.AddOrSetValue('Q', $3F08); 
  FCharset.AddOrSetValue('W', $1B0A); 
  FCharset.AddOrSetValue('E', $2711); 
  FCharset.AddOrSetValue('R', $3319); 
  FCharset.AddOrSetValue('T', $2044); 
  FCharset.AddOrSetValue('Y', $1115); 
  FCharset.AddOrSetValue('U', $1F00); 
  FCharset.AddOrSetValue('I', $2444); 
  FCharset.Add('O', $3F00);           
  FCharset.AddOrSetValue('P', $3311); 
  FCharset.AddOrSetValue('A', $3B11); 
  FCharset.Add('S', $2D11);           
  FCharset.AddOrSetValue('D', $3C44); 
  FCharset.AddOrSetValue('F', $2311); 
  FCharset.AddOrSetValue('G', $2F10); 
  FCharset.AddOrSetValue('H', $1B11); 
  FCharset.AddOrSetValue('J', $1E00); 
  FCharset.AddOrSetValue('K', $329);  
  FCharset.AddOrSetValue('L', $700);  
  FCharset.AddOrSetValue('Z', $2422); 
  FCharset.AddOrSetValue('X', $AA);   
  FCharset.AddOrSetValue('C', $2700); 
  FCharset.AddOrSetValue('V', $322);  
  FCharset.AddOrSetValue('B', $3C54); 
  FCharset.AddOrSetValue('N', $1B88); 
  FCharset.AddOrSetValue('M', $1BA0); 
  FCharset.AddOrSetValue('(', $2700); 
  FCharset.Add('[', $2700);           
  FCharset.Add('{', $2700);           
  FCharset.AddOrSetValue(')', $3C00); 
  FCharset.Add(']', $3C00);           
  FCharset.Add('}', $3C00);           
  FCharset.AddOrSetValue('>', $82);   
  FCharset.AddOrSetValue('<', $28);   
  FCharset.AddOrSetValue('=', $411);  
end;

{ TdxGaugeDigitalScaleCustomSectionsController }

constructor TdxGaugeDigitalScaleCustomSectionsController.Create;
begin
  inherited Create;
  LoadDigit;
end;

destructor TdxGaugeDigitalScaleCustomSectionsController.Destroy;
begin
  FreeAndNil(FDigit);
  inherited Destroy;
end;

function TdxGaugeDigitalScaleCustomSectionsController.GetBackgroundOriginalSize(ASectionCount: Integer): TdxSizeF;
begin
  Result.cx := GetDigitSize.cx * ASectionCount + GetOffset;
  Result.cy := GetSectionSize.cy;
end;

function TdxGaugeDigitalScaleCustomSectionsController.GetBackgroundPartWidth(AScaleFactor: TdxPointF): Single;
var
  ADigitSize: TdxSizeF;
begin
  ADigitSize := GetDigitSize;
  Result := GetSectionSize.cx * AScaleFactor.X * dxGaugeDigitalScaleDigitOffset * ADigitSize.cx / ADigitSize.cy;
end;

procedure TdxGaugeDigitalScaleCustomSectionsController.CalculateSectionsBounds(const ABounds: TdxRectF;
  ASectionCount: Integer; AStretchFactor: Single);
var
  I: Integer;
  ASize: TdxSizeF;
  R: TdxRectF;
begin
  R := cxRectContent(ABounds, cxRectF(1, 0, 1, 0));
  ASize.cx := R.Width / ASectionCount;
  if AStretchFactor = 0 then
    ASize.cy := ASize.cx / GetDigitSize.cx * GetDigitSize.cy
  else
    ASize.cy := GetDigitSize.cy * AStretchFactor;
  SetLength(FSections, ASectionCount);
  for I := Low(FSections) to High(FSections) do
  begin
    FSections[I].Bounds.Top := cxRectCenter(R).Y - ASize.cy / 2;
    FSections[I].Bounds.Left := R.Left + ASize.cx * I;
    FSections[I].Bounds.Right := FSections[I].Bounds.Left + ASize.cx;
    FSections[I].Bounds.Bottom := FSections[I].Bounds.Top + ASize.cy;
  end;
end;

procedure TdxGaugeDigitalScaleCustomSectionsController.CalculateSectionsMasks(const AScaleTextValue: string);
var
  I: Integer;
  ASectionMasks: array of Integer;
  ASectionCalculator: TdxGaugeDigitalScaleCustomSectionCalculator;
begin
  SetLength(ASectionMasks, Length(FSections));
  ASectionCalculator := GetCalculatorClass.Create;
  try
    ASectionCalculator.Calculate(AScaleTextValue, ASectionMasks);
    for I := Low(FSections) to High(FSections) do
      FSections[I].Mask := ASectionMasks[I];
  finally
    ASectionCalculator.Free;
  end;
end;

procedure TdxGaugeDigitalScaleCustomSectionsController.ColorizeDigit(const ASectionMask: Integer; AColorOn, AColorOff:
  TdxAlphaColor);
var
  I: Integer;
  AColor: TdxAlphaColor;
begin
  for I := 0 to GetSegmentCount do
  begin
    if GetBitState(ASectionMask, I) then
      AColor := AColorOn
    else
      AColor := AColorOff;
    TdxCompositeShapeAccess(FDigit).SetShapesColor(AColor, GetSegmentName(I));
  end;
end;

procedure TdxGaugeDigitalScaleCustomSectionsController.DrawSections(AGPGraphics: TdxGPGraphics; AColorOn, AColorOff: TdxAlphaColor);
var
  I: Integer;
begin
  for I := Low(FSections) to High(FSections) do
  begin
    ColorizeDigit(FSections[I].Mask, AColorOn, AColorOff);
    dxGaugeDrawImage(AGPGraphics, FDigit, FSections[I].Bounds);
  end;
end;

function TdxGaugeDigitalScaleCustomSectionsController.GetBitState(const AFlags: Integer; AIndex: Integer): Boolean;
begin
  Result := (AFlags and (1 shl AIndex)) <> 0;
end;

function TdxGaugeDigitalScaleCustomSectionsController.GetDigitSize: TdxSizeF;
begin
  Result.cx := TdxCompositeShapeAccess(FDigit).WidthF;
  Result.cy := TdxCompositeShapeAccess(FDigit).HeightF;
end;

function TdxGaugeDigitalScaleCustomSectionsController.GetOffset: Single;
begin
  Result := 2 * GetDigitSize.cy * dxGaugeDigitalScaleDigitOffset;
end;

function TdxGaugeDigitalScaleCustomSectionsController.GetSectionSize: TdxSizeF;
var
  ADigitSize: TdxSizeF;
  AOffset: Single;
begin
  ADigitSize := GetDigitSize;
  AOffset := GetOffset;
  Result.cx := ADigitSize.cx + AOffset;
  Result.cy := ADigitSize.cy + AOffset;
end;

procedure TdxGaugeDigitalScaleCustomSectionsController.LoadDigit;
var
  AStream: TResourceStream;
begin
  AStream := TResourceStream.Create(HInstance, GetDigitResourceName, RT_RCDATA);
  try
    FDigit := TdxCompositeShape.Create;
    FDigit.LoadFromStream(AStream);
    TdxCompositeShapeAccess(FDigit).NeedNormalize := True;
  finally
    AStream.Free;
  end;
end;

{ TdxGaugeDigitalScaleSevenSegmentSectionsController }

function TdxGaugeDigitalScaleSevenSegmentSectionsController.GetCalculatorClass: TdxGaugeDigitalScaleCustomSectionCalculatorClass;
begin
  Result := TdxGaugeDigitalScaleSevenSegmentSectionCalculator;
end;

function TdxGaugeDigitalScaleSevenSegmentSectionsController.GetDigitResourceName: string;
begin
  Result := 'DigitalScaleSevenSegments';
end;

function TdxGaugeDigitalScaleSevenSegmentSectionsController.GetSegmentCount: Integer;
begin
  Result := 11;
end;

function TdxGaugeDigitalScaleSevenSegmentSectionsController.GetSegmentName(ASegmentIndex: Integer): string;
const
  SegmentNameMap: array[0..11] of string = ('Path_0', 'Path_1', 'Path_2', 'Path_3', 'Path_4', 'Path_5',
    'Path_6', 'Path_7', 'Path_8_1', 'Path_8_2', 'Path_9_1', 'Path_9_2');
begin
  Result := SegmentNameMap[ASegmentIndex];
end;

{ TdxGaugeDigitalScaleFourteenSegmentSectionsController }

function TdxGaugeDigitalScaleFourteenSegmentSectionsController.GetCalculatorClass: TdxGaugeDigitalScaleCustomSectionCalculatorClass;
begin
  Result := TdxGaugeDigitalScaleFourteenSegmentSectionCalculator;
end;

function TdxGaugeDigitalScaleFourteenSegmentSectionsController.GetDigitResourceName: string;
begin
  Result := 'DigitalScaleFourteenSegments';
end;

function TdxGaugeDigitalScaleFourteenSegmentSectionsController.GetSegmentCount: Integer;
begin
  Result := 19;
end;

function TdxGaugeDigitalScaleFourteenSegmentSectionsController.GetSegmentName(ASegmentIndex: Integer): string;
const
  SegmentNameMap: array[0..19] of string = ('Path_0', 'Path_1', 'Path_2', 'Path_3', 'Path_4', 'Path_5',
    'Path_6', 'Path_7', 'Path_8', 'Path_9', 'Path_10', 'Path_11', 'Path_12', 'Path_13', 'Path_14', 'Path_15',
    'Path_16', 'Path_17', 'Path_18', 'Path_19');
begin
  Result := SegmentNameMap[ASegmentIndex];
end;

initialization
  dxGaugeScaleFactory.RegisterScale(TdxGaugeDigitalScale);

finalization
  dxGaugeUnregisterScales;

end.

