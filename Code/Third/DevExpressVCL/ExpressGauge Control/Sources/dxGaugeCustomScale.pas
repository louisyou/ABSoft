{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeCustomScale;

{$I cxVer.inc}

interface

uses
  Windows, Graphics, Classes, Generics.Collections,
  dxCoreClasses, cxClasses, dxCore, cxGeometry, cxGraphics, dxCoreGraphics, dxGDIPlusClasses, dxXMLDoc, cxDataUtils,
  dxCompositeShape, dxShapePrimitives;

const
  dxGaugeAfricaSunsetStyleName = 'AfricaSunset';
  dxGaugeClassicStyleName = 'Classic';
  dxGaugeCleanWhiteStyleName = 'CleanWhite';
  dxGaugeDarkNightStyleName = 'DarkNight';
  dxGaugeDeepFireStyleName = 'DeepFire';
  dxGaugeDiscoStyleName = 'Disco';
  dxGaugeFutureStyleName = 'Future';
  dxGaugeIceColdZoneStyleName = 'IceColdZone';
  dxGaugeiStyleStyleName = 'iStyle';
  dxGaugeMechanicalStyleName = 'Mechanical';
  dxGaugeMilitaryStyleName = 'Military';
  dxGaugeRetroStyleName = 'Retro';
  dxGaugeSmartStyleName = 'Smart';
  dxGaugeSportCarStyleName = 'SportCar';
  dxGaugeYellowSubmarinStyleName = 'YellowSubmarine';
  dxGaugeWhiteStyleName = 'White';

type
  TdxGaugeCustomScale = class;
  TdxGaugeCustomScaleClass = class of TdxGaugeCustomScale;
  TdxGaugeCustomScaleDefaultParameters = class;
  TdxGaugeCustomScaleDefaultParametersClass = class of TdxGaugeCustomScaleDefaultParameters;
  TdxGaugeScaleOptionsCustomLayout = class;
  TdxGaugeScaleOptionsCustomLayoutClass = class of TdxGaugeScaleOptionsCustomLayout;
  TdxGaugeCustomScaleOptionsView = class;
  TdxGaugeCustomScaleOptionsViewClass = class of TdxGaugeCustomScaleOptionsView;
  TdxGaugeCustomScaleParameters = class;
  TdxGaugeCustomScaleParametersClass = class of TdxGaugeCustomScaleParameters;
  TdxGaugeCustomScaleViewInfo = class;
  TdxGaugeCustomScaleViewInfoClass = class of TdxGaugeCustomScaleViewInfo;
  TdxGaugeScaleStyle = class;
  TdxGaugeScaleStyleClass = class of TdxGaugeScaleStyle;
  TdxGaugeCustomScaleStyleReader = class;
  TdxGaugeCustomScaleStyleReaderClass = class of TdxGaugeCustomScaleStyleReader;

  TdxGaugeScaleChangedLayer = (sclStaticLayer, sclDynamicLayer);
  TdxGaugeScaleChangedLayers = set of TdxGaugeScaleChangedLayer;

  TdxGaugeElementType = (etBackground1, etBackground2, etDigitalBackgroundStart, etDigitalBackgroundMiddle,
    etDigitalBackgroundEnd, etTick1, etTick2, etTick3, etTick4, etTick5, etSpindleCap, etNeedle,
    etLinearBarStart, etLinearBarEnd, etLinearBarEmpty, etLinearBarPacked);
  TdxGaugeScalePositionType = (sptFactor, sptPixels);
  TdxGaugeScaleType = (stCircularScale, stDigitalScale, stLinearScale, stCircularHalfScale,
    stCircularQuarterLeftScale, stCircularQuarterRightScale);
  TdxGaugeScaleZOrderChangeType = (octBringBackward, octBringForward, octBringToFront, octSendToBack);

  TdxGaugeScaleLayerDrawProc = procedure(AGPGraphics: TdxGPGraphics) of object;

  { TdxGaugeCustomScaleDefaultParameters }

  TdxGaugeCustomScaleDefaultParameters = class(TObject);

  { TdxGaugeCustomScaleParameters }

  TdxGaugeCustomScaleParameters = class
  public
    Value: Variant; // have to be top

    ShowBackground: Boolean;
    Visible: Boolean;

    CenterPosition: TPoint;
    CenterPositionFactor: TdxPointF;
    CenterPositionType: TdxGaugeScalePositionType;

    Height: Integer;
    HeightFactor: Single;
    Width: Integer;
    WidthFactor: Single;
    Stretch: Boolean;

    procedure Assign(ASource: TdxGaugeCustomScaleParameters); virtual;
  end;

  { TdxGaugeScaleOptionsPersistent }

  TdxGaugeScaleOptionsPersistent = class(TcxOwnedPersistent)
  protected
    function GetScale: TdxGaugeCustomScale;
  end;

  { TdxGaugeScaleOptionsCustomLayout }

  TdxGaugeScaleOptionsCustomLayout = class(TdxGaugeScaleOptionsPersistent)
  private
    function GetCenterPositionFactorX: Single;
    function GetCenterPositionFactorY: Single;
    function GetCenterPositionX: Integer;
    function GetCenterPositionY: Integer;
    function GetCenterPositionType: TdxGaugeScalePositionType;
    function GetHeight: Integer;
    function GetHeightFactor: Single;
    function GetStretch: Boolean;
    function GetWidth: Integer;
    function GetWidthFactor: Single;
    procedure SetCenterPositionFactorX(const AValue: Single);
    procedure SetCenterPositionFactorY(const AValue: Single);
    procedure SetCenterPositionX(const AValue: Integer);
    procedure SetCenterPositionY(const AValue: Integer);
    procedure SetCenterPositionType(const AValue: TdxGaugeScalePositionType);
    procedure SetHeight(const AValue: Integer);
    procedure SetHeightFactor(const AValue: Single);
    procedure SetStretch(const AValue: Boolean);
    procedure SetWidth(const AValue: Integer);
    procedure SetWidthFactor(const AValue: Single);

    procedure ReadCenterPositionFactorX(AReader: TReader);
    procedure ReadCenterPositionFactorY(AReader: TReader);
    procedure WriteCenterPositionFactorX(AWriter: TWriter);
    procedure WriteCenterPositionFactorY(AWriter: TWriter);

    function IsCenterPositionFactorXStored: Boolean;
    function IsCenterPositionFactorYStored: Boolean;
    function IsHeightFactorStored: Boolean;
    function IsWidthFactorStored: Boolean;
  protected
    procedure DefineProperties(AFiler: TFiler); override;

    property Height: Integer read GetHeight write SetHeight default 0;
    property HeightFactor: Single read GetHeightFactor write SetHeightFactor stored IsHeightFactorStored;
    property Stretch: Boolean read GetStretch write SetStretch default False;
    property Width: Integer read GetWidth write SetWidth default 0;
    property WidthFactor: Single read GetWidthFactor write SetWidthFactor stored IsWidthFactorStored;
  published
    property CenterPositionX: Integer read GetCenterPositionX write SetCenterPositionX default 0;
    property CenterPositionFactorX: Single read GetCenterPositionFactorX write SetCenterPositionFactorX
      stored IsCenterPositionFactorXStored;
    property CenterPositionY: Integer read GetCenterPositionY write SetCenterPositionY default 0;
    property CenterPositionFactorY: Single read GetCenterPositionFactorY write SetCenterPositionFactorY
      stored IsCenterPositionFactorYStored;
    property CenterPositionType: TdxGaugeScalePositionType read GetCenterPositionType
      write SetCenterPositionType default sptFactor;
  end;

  { TdxGaugeScaleOptionsRectangularLayout }

  TdxGaugeScaleOptionsRectangularLayout = class(TdxGaugeScaleOptionsCustomLayout)
  published
    property Height;
    property HeightFactor;
    property Stretch;
    property Width;
    property WidthFactor;
  end;

  { TdxGaugeCustomScaleOptionsView }

  TdxGaugeCustomScaleOptionsView = class(TdxGaugeScaleOptionsPersistent)
  private
    function GetShowBackground: Boolean;
    procedure SetShowBackground(const AValue: Boolean);
  published
    property ShowBackground: Boolean read GetShowBackground write SetShowBackground default True;
  end;

  { TdxGaugeCustomScaleViewInfo }

  TdxGaugeCustomScaleViewInfo = class
  private
    FStyle: TdxGaugeScaleStyle;

    procedure SetStyle(AStyle: TdxGaugeScaleStyle);

    function GetCenter(const ABounds: TdxRectF): TdxPointF;
    function GetScaleDefaultParameters: TdxGaugeCustomScaleDefaultParameters;
    procedure CreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
    procedure RecreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
  protected
    FBackground: TdxCompositeShape;
    FBounds: TdxRectF;
    FScaleFactor: TdxPointF;
    FScaleParameters: TdxGaugeCustomScaleParameters;

    function GetContentBounds: TdxRectF; virtual;
    function GetContentOriginalSize: TdxSizeF; virtual;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; virtual;
    function GetScaleSize(const ABounds: TdxRectF): TdxSizeF; virtual;
    procedure CalculateBounds(const ABounds: TdxRectF); virtual;
    procedure CalculateContent; virtual;
    procedure CalculateScaleFactor; virtual;
    procedure DrawBackground(AGPGraphics: TdxGPGraphics); virtual;
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); virtual;
    procedure LoadScaleElements; virtual;
    procedure PopulateParameters(AScaleParameters: TdxGaugeCustomScaleParameters); virtual;

    function GetBounds: TdxRectF;
    function GetElementImage(AType: TdxGaugeElementType): TGraphic;
    function GetElementImageOriginalSize(AImage: TGraphic): TdxSizeF;
    function GetElementImageSize(AImage: TGraphic; AImageScaleFactor: TdxPointF): TdxSizeF;
    function GetScaledSize(AOriginalSize: TdxSizeF): TdxSizeF;
    function GetScaleRect(AOriginalRectSize: TdxSizeF; ANeedScaling: Boolean = True): TdxRectF;
    function NeedDraw: Boolean;
    procedure Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF);

    property ScaleDefaultParameters: TdxGaugeCustomScaleDefaultParameters read GetScaleDefaultParameters;
    property Style: TdxGaugeScaleStyle read FStyle write SetStyle;
  public
    constructor Create(AScaleType: TdxGaugeScaleType; const AStyleName: string); virtual;
    destructor Destroy; override;
  end;

  { TdxGaugeCustomScale }

  TdxGaugeCustomScale = class(TcxComponentCollectionItem)
  private
    FAnchorScale: TdxGaugeCustomScale;
    FScaleParameters: TdxGaugeCustomScaleParameters;
    FStoredAnchorScaleIndex: Integer;
    FStyleName: string;
    FViewInfo: TdxGaugeCustomScaleViewInfo;

    function GetAnchorScale: TdxGaugeCustomScale;
    function GetAnchorScaleIndex: Integer;
    function GetBounds: TdxRectF;
    function GetCenterPositionType: TdxGaugeScalePositionType;
    function GetCenterPositionX: Integer;
    function GetCenterPositionFactorX: Single;
    function GetCenterPositionY: Integer;
    function GetCenterPositionFactorY: Single;
    function GetContentBounds: TdxRectF;
    function GetDataBinding: TcxCustomDataBinding;
    function GetHeight: Integer;
    function GetHeightFactor: Single;
    function GetOptionsLayout: TdxGaugeScaleOptionsRectangularLayout;
    function GetShowBackground: Boolean;
    function GetStretch: Boolean;
    function GetValue: Variant;
    function GetVisible: Boolean;
    function GetWidth: Integer;
    function GetWidthFactor: Single;
    function GetZOrder: Integer;
    procedure SetAnchorScale(const AValue: TdxGaugeCustomScale);
    procedure SetAnchorScaleIndex(const AValue: Integer);
    procedure SetCenterPositionType(const AValue: TdxGaugeScalePositionType);
    procedure SetCenterPositionX(const AValue: Integer);
    procedure SetCenterPositionFactorX(const AValue: Single);
    procedure SetCenterPositionY(const AValue: Integer);
    procedure SetCenterPositionFactorY(const AValue: Single);
    procedure SetDataBinding(const AValue: TcxCustomDataBinding);
    procedure SetHeight(const AValue: Integer);
    procedure SetHeightFactor(const AValue: Single);
    procedure SetOptionsLayout(const AValue: TdxGaugeScaleOptionsRectangularLayout);
    procedure SetOptionsView(const AValue: TdxGaugeCustomScaleOptionsView);
    procedure SetShowBackground(const AValue: Boolean);
    procedure SetStretch(const AValue: Boolean);
    procedure SetStyleName(const AValue: string);
    procedure SetValue(const AValue: Variant);
    procedure SetVisible(const AValue: Boolean);
    procedure SetWidth(const AValue: Integer);
    procedure SetWidthFactor(const AValue: Single);

    function GetAnchorScaleByIndex(AAnchorScaleIndex: Integer): TdxGaugeCustomScale;
    procedure DataChangeHandler;
    procedure DataSetChangeHandler;

    procedure ReadZOrder(AReader: TReader);
    procedure WriteZOrder(AWriter: TWriter);

    function IsAnchorScaleIndexStored: Boolean;
    function IsCenterPositionFactorXStored: Boolean;
    function IsCenterPositionFactorYStored: Boolean;
    function IsStyleNameStored: Boolean;
    function IsHeightFactorStored: Boolean;
    function IsWidthFactorStored: Boolean;
  protected
    FChangedLayers: TdxGaugeScaleChangedLayers;
    FDataBinding: TcxCustomDataBinding;
    FOptionsLayout: TdxGaugeScaleOptionsCustomLayout;
    FOptionsView: TdxGaugeCustomScaleOptionsView;
    FZOrder: Integer;

    function GetCollectionFromParent(AParent: TComponent): TcxComponentCollection; override;
    procedure DefineProperties(AFiler: TFiler); override;
    procedure Loaded; override;

    class function GetLayerCount: Integer; virtual;
    class function GetScaleName: string; virtual;
    class function GetScaleType: TdxGaugeScaleType; virtual;

    function GetDataBindingClass: TcxCustomDataBindingClass; virtual;
    function GetOptionsLayoutClass: TdxGaugeScaleOptionsCustomLayoutClass; virtual;
    function GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass; virtual;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; virtual;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; virtual;

    function GetValidValue(const AValue: Variant): Variant; virtual;
    procedure ApplyStyleParameters; virtual;
    procedure Calculate(const AScaleBounds: TdxRectF; const ACacheBounds: TRect); virtual;
    procedure DoAssign(AScale: TdxGaugeCustomScale); virtual;
    procedure DoDrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); virtual;
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer); virtual;
    procedure InitScaleParameters; virtual;
    procedure InternalRestoreStyleParameters; virtual;

    function GetSelectionRect: TRect;
    procedure InternalSetValue(const AValue: Variant);
    procedure ScaleChanged(ALayers: TdxGaugeScaleChangedLayers);
    // Internal properties
    property AnchorScale: TdxGaugeCustomScale read GetAnchorScale write SetAnchorScale;
    property CenterPositionX: Integer read GetCenterPositionX write SetCenterPositionX;
    property CenterPositionFactorX: Single read GetCenterPositionFactorX write SetCenterPositionFactorX;
    property CenterPositionY: Integer read GetCenterPositionY write SetCenterPositionY;
    property CenterPositionFactorY: Single read GetCenterPositionFactorY write SetCenterPositionFactorY;
    property CenterPositionType: TdxGaugeScalePositionType read GetCenterPositionType write SetCenterPositionType;
    property ChangedLayers: TdxGaugeScaleChangedLayers read FChangedLayers write FChangedLayers;
    property ScaleParameters: TdxGaugeCustomScaleParameters read FScaleParameters;
    property ViewInfo: TdxGaugeCustomScaleViewInfo read FViewInfo;
    property ZOrder: Integer read GetZOrder;

    property AnchorScaleIndex: Integer read GetAnchorScaleIndex write SetAnchorScaleIndex
      stored IsAnchorScaleIndexStored;
    property Bounds: TdxRectF read GetBounds;
    property ContentBounds: TdxRectF read GetContentBounds;
    property DataBinding: TcxCustomDataBinding read GetDataBinding write SetDataBinding;
    property Value: Variant read GetValue write SetValue;
    property Visible: Boolean read GetVisible write SetVisible default True;

    property OptionsLayout: TdxGaugeScaleOptionsRectangularLayout read GetOptionsLayout write SetOptionsLayout;
    property OptionsView: TdxGaugeCustomScaleOptionsView read FOptionsView write SetOptionsView;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(ASource: TPersistent); override;
    procedure RestoreStyleParameters;

    property ScaleType: TdxGaugeScaleType read GetScaleType;
    property StyleName: string read FStyleName write SetStyleName stored IsStyleNameStored;
  end;

  { TdxGaugeScaleCollection }

  TdxGaugeScaleCollection = class(TcxComponentCollection)
  private
    FOnAdded: TNotifyEvent;
    FOnDeleted: TNotifyEvent;

    function GetScale(AIndex: Integer): TdxGaugeCustomScale;
    procedure SetScale(AIndex: Integer; const AValue: TdxGaugeCustomScale);
  protected
    function GetItemPrefixName: string; override;
    procedure Notify(AItem: TcxComponentCollectionItem; AAction: TcxComponentCollectionNotification); override;

    property OnAdded: TNotifyEvent read FOnAdded write FOnAdded;
    property OnDeleted: TNotifyEvent read FOnDeleted write FOnDeleted;
  public
    procedure Assign(ASource: TPersistent); override;

    function Add(AScaleClass: TdxGaugeCustomScaleClass): TdxGaugeCustomScale;

    property Items[Index: Integer]: TdxGaugeCustomScale read GetScale write SetScale; default;
  end;

  { TdxGaugeScaleStyle }

  TdxGaugeScaleStyle = class
  private
    FBaseStyleName: string;
    FDefaultParameters: TdxGaugeCustomScaleDefaultParameters;
    FElements: array[TdxGaugeElementType] of TdxCompositeShape;
    FName: string;
    FScaleTypeName: string;
    FShapes: TdxCompositeShape;
  protected
    function GetElement(AElementType: TdxGaugeElementType): TdxCompositeShape;
    procedure AddElement(AElementType: TdxGaugeElementType; AElement: TdxCompositeShape);

    property BaseStyleName: string read FBaseStyleName write FBaseStyleName;
    property DefaultParameters: TdxGaugeCustomScaleDefaultParameters read FDefaultParameters write FDefaultParameters;
    property Name: string read FName write FName;
    property Shapes: TdxCompositeShape read FShapes write FShapes;
    property ScaleTypeName: string read FScaleTypeName write FScaleTypeName;
  public
    constructor Create(const AStyleName: string); overload;
    constructor Create(const AStyleName, ABaseStyleName: string); overload;
    destructor Destroy; override;
  end;

  { TdxGaugeScaleStyleInfo }

  TdxGaugeScaleStyleInfo = class
  public
    BaseStyleName: string;
    IsExternalStyle: Boolean;
    Name: string;
    ResourceName: string;
    ScaleType: TdxGaugeScaleType;
  end;

  { TdxGaugeCustomScaleStyleReader }

  TdxGaugeCustomScaleStyleReader = class
  private
    function GetStyleRootNode(ADocument: TdxXMLDocument): TdxXMLNode;
    function ReadCompositeShape(ADocument: TdxXMLDocument): TdxCompositeShape;
    function ReadDefaultParameters(ADocument: TdxXMLDocument): TdxGaugeCustomScaleDefaultParameters;
  protected
    class function GetParametersNodeName: AnsiString;
    class function GetResourceNamePreffix: string; virtual;

    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; virtual; abstract;
    procedure ReadParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters); virtual; abstract;

    function CreateStyle(ADocument: TdxXMLDocument): TdxGaugeScaleStyle;
    function GetAttributeValueAsAlphaColor(ANode: TdxXMLNode; const AAttributeName: AnsiString): TdxAlphaColor;
    function GetAttributeValueAsBoolean(ANode: TdxXMLNode; const AAttributeName: AnsiString): Boolean;
    function GetAttributeValueAsColor(ANode: TdxXMLNode; const AAttributeName: AnsiString): TColor;
    function GetAttributeValueAsDouble(ANode: TdxXMLNode; const AAttributeName: AnsiString): Double;
    function GetAttributeValueAsElementType(ANode: TdxXMLNode; const AAttributeName: AnsiString): TdxGaugeElementType;
    function GetAttributeValueAsInteger(ANode: TdxXMLNode; const AAttributeName: AnsiString): Integer;
    function GetAttributeValueAsPointF(ANode: TdxXMLNode; const AAttributeName: AnsiString): TdxPointF;
    function GetAttributeValueAsString(ANode: TdxXMLNode; const AAttributeName: AnsiString): string;
    function GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString): TdxXMLNode; overload;
    function GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString; out AChildNode: TdxXMLNode): Boolean; overload;
    function GetElementType(const ATypeName: string): TdxGaugeElementType;
  end;

  { TdxGaugeScaleStyleCreator }

  TdxGaugeScaleStyleCreator = class
  private
    function CreateFromResource(AInstance: THandle; const AResName: string): TdxGaugeScaleStyle;
    function CreateFromStream(AStream: TStream): TdxGaugeScaleStyle;
    function CreateFromStyle(ADestinationStyleInfo, ASourceStyleInfo: TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle;
    function GetStyleReaderClass(ADocument: TdxXMLDocument): TdxGaugeCustomScaleStyleReaderClass;
    function IdentifyReaderClass(ADefaultParametersNode: TdxXMLNode;
      var AReaderClass: TdxGaugeCustomScaleStyleReaderClass): Boolean;
    function InternalCreateStyleDocument(AStream: TStream): TdxXMLDocument;
    function IsStyleDocumentAvailable(ADocument: TdxXMLDocument): Boolean;
  protected
    function CreateStyle(AStyleInfo: TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle; overload;
    function CreateStyle(AInstance: THandle; const AResName: string): TdxXMLDocument; overload;
    function CreateStyle(const AFileName: string): TdxGaugeScaleStyle; overload;
    function CreateStyleDocument(AStyleInfo: TdxGaugeScaleStyleInfo): TdxXMLDocument; overload;
    function CreateStyleDocument(const AFileName: string): TdxXMLDocument; overload;
  end;

 { TdxGaugeScaleStyleCollection }

  TdxGaugeScaleStyleCollection = class
  private
    FPredefinedStyleNames: TStringList;
    FStyleCreator: TdxGaugeScaleStyleCreator;
    FStyles: TDictionary<TdxGaugeScaleType, TStringList>;

    function CreateStyleInfo(const AFileName: string): TdxGaugeScaleStyleInfo;
    function CreateStyleList: TStringList;
    function ExctractStyleToDocument(AScaleType: TdxGaugeScaleType; const AStyleName: string): TdxXMLDocument;
    function GetStyleInfo(AStyles: TStringList; const AStyleName: string): TdxGaugeScaleStyleInfo;
    function InternalCreateStyle(AStyleInfo: TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle;
    function InternalRegisterStyle(AScaleType: TdxGaugeScaleType; AStyleInfo: TdxGaugeScaleStyleInfo): Boolean;
    function InternalUnregisterStyle(AStyles: TStringList; const AStyleName: string): Boolean;
    function IsInheritedStyle(AScaleType: TdxGaugeScaleType): Boolean;
    function IsRegisteredStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string): Boolean;
    procedure DeleteStyle(AStyles: TStringList; AIndex: Integer);
    procedure InitPredefinedStyleNames;
    procedure InternalUnregisterStyles;
    procedure ProcessError(AStyleInfo: TdxGaugeScaleStyleInfo; AErrorCode: Integer);
  protected
    function CreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string): TdxGaugeScaleStyle;
    function CreateStyleFromFile(const AFileName: string): TdxGaugeScaleStyle;
    function GetFirstStyleName(AScaleType: TdxGaugeScaleType): string;
    function GetStyles(AScaleType: TdxGaugeScaleType): TStringList;
    function HasStyles(AScaleType: TdxGaugeScaleType): Boolean;
    procedure RegisterPredefinedStyles(AScaleClass: TdxGaugeCustomScaleClass);
    procedure UnregisterPredefinedStyles(AScaleClass: TdxGaugeCustomScaleClass);
  public
    constructor Create;
    destructor Destroy; override;

    function RegisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string): Boolean;
    function RegisterStyleFromFile(AScaleType: TdxGaugeScaleType; const AFileName: string): Boolean;
    procedure ExtractStyleToFile(AScaleType: TdxGaugeScaleType; const AStyleName, AFileName: string);
    procedure ExtractStyleToStream(AScaleType: TdxGaugeScaleType; const AStyleName: string; AStream: TStream);
    procedure GetPredefinedStyleNames(AList: TStringList);
    procedure UnregisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
    procedure UnregisterStyles(AScaleType: TdxGaugeScaleType);
  end;

  { TdxGaugeScaleFactory }

  TdxGaugeScaleFactory = class
  private
    FScaleClasses: TStringList;
  protected
    function IsRegisteredScale(AScaleClass: TdxGaugeCustomScaleClass): Boolean;
    procedure UnregisterScales;

    property ScaleClasses: TStringList read FScaleClasses;
  public
    constructor Create;
    destructor Destroy; override;

    procedure RegisterScale(AScaleClass: TdxGaugeCustomScaleClass; ARegisterPredefinedStyle: Boolean = True);
    procedure UnregisterScale(AScaleClass: TdxGaugeCustomScaleClass; AUnregisterPredefinedStyle: Boolean = True);
  end;

function dxGaugeScaleFactory: TdxGaugeScaleFactory;
function dxGaugeScaleStyleCollection: TdxGaugeScaleStyleCollection;
procedure dxGaugeUnregisterScales;

implementation

uses
  Types, SysUtils, Math, Forms, Variants, cxControls, dxGaugeControl, dxGaugeUtils, dxGaugeQuantitativeScale,
  dxGaugeCircularScale, dxGaugeDigitalScale, dxGaugeLinearScale, dxGaugeDBScale;

{$R GaugeStyles.res}

const
  dxGaugeBackground1 = 'BackgroundLayer1';
  dxGaugeBackground2 = 'BackgroundLayer2';
  dxGaugeDigitalBackgroundStart = 'Back_Start';
  dxGaugeDigitalBackgroundMiddle = 'Back';
  dxGaugeDigitalBackgroundEnd = 'Back_End';
  dxGaugeLinearBarStart = 'BarStart';
  dxGaugeLinearBarEnd = 'BarEnd';
  dxGaugeLinearBarPacked = 'BarPacked';
  dxGaugeLinearBarEmpty = 'BarEmpty';
  dxGaugeTick1 = 'Tick1';
  dxGaugeTick2 = 'Tick2';
  dxGaugeTick3 = 'Tick3';
  dxGaugeTick4 = 'Tick4';
  dxGaugeTick5 = 'Tick5';
  dxGaugeSpindleCap = 'SpindleCap';
  dxGaugeNeedle = 'Needle';
  dxGaugeStyleElementNameMap: array[TdxGaugeElementType] of string = (dxGaugeBackground1, dxGaugeBackground2,
    dxGaugeDigitalBackgroundStart, dxGaugeDigitalBackgroundMiddle, dxGaugeDigitalBackgroundEnd,
    dxGaugeTick1, dxGaugeTick2, dxGaugeTick3, dxGaugeTick4, dxGaugeTick5, dxGaugeSpindleCap, dxGaugeNeedle,
    dxGaugeLinearBarStart, dxGaugeLinearBarEnd, dxGaugeLinearBarEmpty, dxGaugeLinearBarPacked);
  dxGaugeScaleDefaultValue = 0;
  dxGaugeScaleWidthFactor = 1;
  dxGaugeScaleHeightFactor = 1;
  dxGaugeScaleCenterPositionFactor = 0.5;

  sdxGaugeScaleStyleCollectionInvalidStyleFileFormat =
    'The format of the style file does not correspond to the specified scale type';
  sdxGaugeScaleStyleCollectionInvalidStyleCount = 'No style is registered for the ' + '''' +'%s' + '''' +' scale type';
  sdxGaugeScaleStyleCollectionDublicateStyleName = 'Duplicate style name '  + '''' +'%s' + '''' +' is found for the ' +
    '''' +'%s' + '''' +' scale type';
  sdxGaugeScaleStyleCollectionBaseStyleUnregistered =
    'The '  + '''' +'%s' + '''' +' style used as the base for the ' +  '''' +'%s' + '''' + ' style of the ' +
    '''' +'%s' + '''' + ' scale type is not registered for the ' + '''' + '%s' + '''' +' scale type';
  sdxGaugeScaleFactoryDublicateScaleClassName = 'Duplicate scale class name ' + '''' +'%s' + '''';

type
  TdxCompositeShapeAccess = class(TdxCompositeShape);
  TdxCustomGaugeControlAccess = class(TdxCustomGaugeControl);
  TdxCustomGaugeControlControllerAccess = class(TdxCustomGaugeControlController);

  { TdxGaugeScaleTypeInfo }

  TdxGaugeScaleTypeInfo = class
    ReaderClass: TdxGaugeCustomScaleStyleReaderClass;
    ScaleType: TdxGaugeScaleType;
    ScaleTypeName: string;
  end;

  { TdxGaugeScaleTypeInfos }

  TdxGaugeScaleTypeInfos = class
  private
    FTypeInfos: TObjectList<TdxGaugeScaleTypeInfo>;

    function CreateTypeInfo(AReaderClass: TdxGaugeCustomScaleStyleReaderClass; AType: TdxGaugeScaleType;
    const ATypeName: string): TdxGaugeScaleTypeInfo;
  protected
    function GetTypeCount: Integer;
    function GetTypeByName(const ATypeName: string): TdxGaugeScaleType;
    function GetTypeInfo(AIndex: Integer): TdxGaugeScaleTypeInfo;
    function GetTypeName(AScaleType: TdxGaugeScaleType): string;
    function GetFormatedTypeName(AScaleType: TdxGaugeScaleType): string;
  public
    constructor Create;
    destructor Destroy; override;
  end;

var
  dxgGaugeScaleFactory: TdxGaugeScaleFactory;
  dxgGaugeScaleStyleCollection: TdxGaugeScaleStyleCollection;
  dxgGaugeScaleTypeInfos: TdxGaugeScaleTypeInfos;

function dxGaugeScaleFactory: TdxGaugeScaleFactory;
begin
  if dxgGaugeScaleFactory = nil then
    dxgGaugeScaleFactory := TdxGaugeScaleFactory.Create;
  Result := dxgGaugeScaleFactory;
end;

function dxGaugeScaleStyleCollection: TdxGaugeScaleStyleCollection;
begin
  if dxgGaugeScaleStyleCollection = nil then
    dxgGaugeScaleStyleCollection := TdxGaugeScaleStyleCollection.Create;
  Result := dxgGaugeScaleStyleCollection;
end;

procedure dxGaugeUnregisterScales;
begin
  if dxgGaugeScaleFactory <> nil then
    dxgGaugeScaleFactory.UnregisterScales;
end;

function dxGaugeScaleTypeInfos: TdxGaugeScaleTypeInfos;
begin
  if dxgGaugeScaleTypeInfos = nil then
    dxgGaugeScaleTypeInfos := TdxGaugeScaleTypeInfos.Create;
  Result := dxgGaugeScaleTypeInfos;
end;

function dxGaugeController(ACollection: TdxGaugeScaleCollection): TdxCustomGaugeControlControllerAccess;
var
  AGaugeControl: TdxCustomGaugeControlAccess;
begin
  if ACollection <> nil then
  begin
    AGaugeControl := TdxCustomGaugeControlAccess(ACollection.ParentComponent as TdxCustomGaugeControl);
    Result := TdxCustomGaugeControlControllerAccess(AGaugeControl.Controller);
  end
  else
    Result := nil;
end;

{ TdxGaugeScaleTypeInfos }

constructor TdxGaugeScaleTypeInfos.Create;
begin
  inherited Create;
  FTypeInfos := TObjectList<TdxGaugeScaleTypeInfo>.Create(True);
  FTypeInfos.Add(CreateTypeInfo(TdxGaugeArcQuantitativeScaleStyleReader, stCircularScale, 'Circular'));
  FTypeInfos.Add(CreateTypeInfo(TdxGaugeCustomCircularHalfScaleStyleReader, stCircularHalfScale, 'CircularHalf'));
  FTypeInfos.Add(CreateTypeInfo(TdxGaugeCustomCircularHalfScaleStyleReader, stCircularQuarterLeftScale, 'CircularQuarterLeft'));
  FTypeInfos.Add(CreateTypeInfo(TdxGaugeCustomCircularHalfScaleStyleReader, stCircularQuarterRightScale, 'CircularQuarterRight'));
  FTypeInfos.Add(CreateTypeInfo(TdxGaugeCustomLinearScaleStyleReader, stLinearScale, 'Linear'));
  FTypeInfos.Add(CreateTypeInfo(TdxGaugeCustomDigitalScaleStyleReader, stDigitalScale, 'Digital'));
  if FTypeInfos.Count -1 <> Integer(High(TdxGaugeScaleType)) then
    dxGaugeRaiseException('TypeInfos.Count - 1 <> High(TdxGaugeScaleType)', []);
end;

destructor TdxGaugeScaleTypeInfos.Destroy;
begin
  FreeAndNil(FTypeInfos);
  inherited Destroy;
end;

function TdxGaugeScaleTypeInfos.GetTypeCount: Integer;
begin
  Result := FTypeInfos.Count;
end;

function TdxGaugeScaleTypeInfos.GetTypeByName(const ATypeName: string): TdxGaugeScaleType;
var
  I: Integer;
begin
  Result := stCircularScale;
  for I := 0 to FTypeInfos.Count - 1 do
    if SameText(FTypeInfos[I].ScaleTypeName, ATypeName) then
    begin
      Result := FTypeInfos[I].ScaleType;
      Break;
    end;
end;

function TdxGaugeScaleTypeInfos.GetTypeInfo(AIndex: Integer): TdxGaugeScaleTypeInfo;
begin
  Result := FTypeInfos[AIndex];
end;

function TdxGaugeScaleTypeInfos.GetTypeName(AScaleType: TdxGaugeScaleType): string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FTypeInfos.Count - 1 do
    if FTypeInfos[I].ScaleType = AScaleType then
    begin
      Result := FTypeInfos[I].ScaleTypeName;
      Break;
    end;
end;

function TdxGaugeScaleTypeInfos.GetFormatedTypeName(AScaleType: TdxGaugeScaleType): string;
var
  I: Integer;
  ASourceTypeName: string;
begin
  ASourceTypeName := GetTypeName(AScaleType);
  Result := '';
  for I := 1 to Length(ASourceTypeName) do
  begin
    if (UpperCase(ASourceTypeName[I]) = ASourceTypeName[I]) and (I <> 1) then
      Result := Result + ' ';
     Result := Result + ASourceTypeName[I];
  end;
end;

function TdxGaugeScaleTypeInfos.CreateTypeInfo(AReaderClass: TdxGaugeCustomScaleStyleReaderClass;
  AType: TdxGaugeScaleType; const ATypeName: string): TdxGaugeScaleTypeInfo;
begin
  Result := TdxGaugeScaleTypeInfo.Create;
  Result.ReaderClass := AReaderClass;
  Result.ScaleType := AType;
  Result.ScaleTypeName := ATypeName;
end;

{ TdxGaugeCustomScaleParameters }

procedure TdxGaugeCustomScaleParameters.Assign(ASource: TdxGaugeCustomScaleParameters);

  procedure dxGaugeCopyFieldValues;
  begin
    cxCopyData(ASource, Self, SizeOf(ASource) + SizeOf(Variant), SizeOf(Self) + SizeOf(Variant),
      ASource.InstanceSize -  SizeOf(ASource) - SizeOf(Variant));
  end;

begin
  dxGaugeCopyFieldValues;
  Value := ASource.Value;
end;

{ TdxGaugeScaleOptionsPersistent }

function TdxGaugeScaleOptionsPersistent.GetScale: TdxGaugeCustomScale;
begin
  Result := Owner as TdxGaugeCustomScale;
end;

{ TdxGaugeScaleOptionsCustomLayout }

procedure TdxGaugeScaleOptionsCustomLayout.DefineProperties(AFiler: TFiler);
begin
  inherited DefineProperties(AFiler);
  AFiler.DefineProperty('CenterPositionFactorX', ReadCenterPositionFactorX, WriteCenterPositionFactorX,
    SameValue(CenterPositionFactorX, 0));
  AFiler.DefineProperty('CenterPositionFactorY', ReadCenterPositionFactorY, WriteCenterPositionFactorY,
    SameValue(CenterPositionFactorY, 0));
end;

function TdxGaugeScaleOptionsCustomLayout.GetCenterPositionFactorX: Single;
begin
  Result := GetScale.GetCenterPositionFactorX;
end;

function TdxGaugeScaleOptionsCustomLayout.GetCenterPositionFactorY: Single;
begin
  Result := GetScale.GetCenterPositionFactorY;
end;

function TdxGaugeScaleOptionsCustomLayout.GetCenterPositionX: Integer;
begin
  Result := GetScale.GetCenterPositionX;
end;

function TdxGaugeScaleOptionsCustomLayout.GetCenterPositionY: Integer;
begin
  Result := GetScale.GetCenterPositionY;
end;

function TdxGaugeScaleOptionsCustomLayout.GetCenterPositionType: TdxGaugeScalePositionType;
begin
  Result := GetScale.GetCenterPositionType;
end;

function TdxGaugeScaleOptionsCustomLayout.GetHeight: Integer;
begin
  Result := GetScale.GetHeight;
end;

function TdxGaugeScaleOptionsCustomLayout.GetHeightFactor: Single;
begin
  Result := GetScale.GetHeightFactor;
end;

function TdxGaugeScaleOptionsCustomLayout.GetStretch: Boolean;
begin
  Result := GetScale.GetStretch;
end;

function TdxGaugeScaleOptionsCustomLayout.GetWidth: Integer;
begin
  Result := GetScale.GetWidth;
end;

function TdxGaugeScaleOptionsCustomLayout.GetWidthFactor: Single;
begin
  Result := GetScale.GetWidthFactor;
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetCenterPositionFactorX(const AValue: Single);
begin
  GetScale.SetCenterPositionFactorX(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetCenterPositionFactorY(const AValue: Single);
begin
  GetScale.SetCenterPositionFactorY(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetCenterPositionX(const AValue: Integer);
begin
  GetScale.SetCenterPositionX(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetCenterPositionY(const AValue: Integer);
begin
  GetScale.SetCenterPositionY(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetCenterPositionType(const AValue: TdxGaugeScalePositionType);
begin
  GetScale.SetCenterPositionType(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetHeight(const AValue: Integer);
begin
  GetScale.SetHeight(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetHeightFactor(const AValue: Single);
begin
  GetScale.SetHeightFactor(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetStretch(const AValue: Boolean);
begin
  GetScale.SetStretch(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetWidth(const AValue: Integer);
begin
  GetScale.SetWidth(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.SetWidthFactor(const AValue: Single);
begin
  GetScale.SetWidthFactor(AValue);
end;

procedure TdxGaugeScaleOptionsCustomLayout.ReadCenterPositionFactorX(AReader: TReader);
begin
  CenterPositionFactorX := AReader.ReadDouble;
end;

procedure TdxGaugeScaleOptionsCustomLayout.ReadCenterPositionFactorY(AReader: TReader);
begin
  CenterPositionFactorY := AReader.ReadDouble;
end;

procedure TdxGaugeScaleOptionsCustomLayout.WriteCenterPositionFactorX(AWriter: TWriter);
begin
  AWriter.WriteDouble(CenterPositionFactorX);
end;

procedure TdxGaugeScaleOptionsCustomLayout.WriteCenterPositionFactorY(AWriter: TWriter);
begin
  AWriter.WriteDouble(CenterPositionFactorY);
end;

function TdxGaugeScaleOptionsCustomLayout.IsCenterPositionFactorXStored: Boolean;
begin
  Result := GetScale.IsCenterPositionFactorXStored;
end;

function TdxGaugeScaleOptionsCustomLayout.IsCenterPositionFactorYStored: Boolean;
begin
  Result := GetScale.IsCenterPositionFactorYStored;
end;

function TdxGaugeScaleOptionsCustomLayout.IsHeightFactorStored: Boolean;
begin
  Result := GetScale.IsHeightFactorStored;
end;

function TdxGaugeScaleOptionsCustomLayout.IsWidthFactorStored: Boolean;
begin
  Result := GetScale.IsWidthFactorStored;
end;

{ TdxGaugeCustomScaleOptionsView }

function TdxGaugeCustomScaleOptionsView.GetShowBackground: Boolean;
begin
  Result := GetScale.GetShowBackground;
end;

procedure TdxGaugeCustomScaleOptionsView.SetShowBackground(const AValue: Boolean);
begin
  GetScale.SetShowBackground(AValue);
end;

{ TdxGaugeCustomScaleViewInfo }

constructor TdxGaugeCustomScaleViewInfo.Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  if dxGaugeScaleStyleCollection.HasStyles(AScaleType) then
  begin
    inherited Create;
    FScaleFactor := dxPointF(1, 1);
    FScaleParameters := GetScaleParametersClass.Create;
    CreateStyle(AScaleType, AStyleName);
  end
  else
    dxGaugeRaiseException(sdxGaugeScaleStyleCollectionInvalidStyleCount,
      [dxGaugeScaleTypeInfos.GetFormatedTypeName(AScaleType)]);
end;

destructor TdxGaugeCustomScaleViewInfo.Destroy;
begin
  FreeAndNil(FStyle);
  FreeAndNil(FScaleParameters);
  inherited Destroy;
end;

function TdxGaugeCustomScaleViewInfo.GetContentBounds: TdxRectF;
begin
  Result := GetScaleRect(GetElementImageOriginalSize(FBackground));
end;

function TdxGaugeCustomScaleViewInfo.GetContentOriginalSize: TdxSizeF;
begin
  Result := GetElementImageOriginalSize(FBackground);
end;

function TdxGaugeCustomScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeCustomScaleParameters;
end;

function TdxGaugeCustomScaleViewInfo.GetScaleSize(const ABounds: TdxRectF): TdxSizeF;
begin
  if FScaleParameters.Width <> 0 then
    Result.cx := FScaleParameters.Width
  else
    Result.cx := FScaleParameters.WidthFactor * ABounds.Width;
  if FScaleParameters.Height <> 0 then
    Result.cy := FScaleParameters.Height
  else
    Result.cy := FScaleParameters.HeightFactor * ABounds.Height;
end;

procedure TdxGaugeCustomScaleViewInfo.CalculateBounds(const ABounds: TdxRectF);
var
  ACenter: TdxPointF;
  ASize: TdxSizeF;
  R: TdxRectF;
begin
  R := cxRectContent(ABounds, cxRectF(0, 0, 1, 1));
  ACenter := GetCenter(R);
  ASize := GetScaleSize(R);
  ASize := dxSizeF(ASize.cx / 2, ASize.cy / 2);
  FBounds := cxRectInflate(cxRectF(ACenter, ACenter), dxRectF(ASize.cx, ASize.cy, ASize.cx, ASize.cy));
end;

procedure TdxGaugeCustomScaleViewInfo.CalculateContent;
begin
//do nothing
end;

procedure TdxGaugeCustomScaleViewInfo.CalculateScaleFactor;
var
  AOriginalSize: TdxSizeF;
begin
  AOriginalSize := GetContentOriginalSize;
  FScaleFactor.X := FBounds.Width / AOriginalSize.cx;
  FScaleFactor.Y := FBounds.Height / AOriginalSize.cy;
  if not FScaleParameters.Stretch then
    FScaleFactor := dxPointF(Min(FScaleFactor.X, FScaleFactor.Y),
      Min(FScaleFactor.X, FScaleFactor.Y));
end;

procedure TdxGaugeCustomScaleViewInfo.DrawBackground(AGPGraphics: TdxGPGraphics);
begin
  if FScaleParameters.ShowBackground then
    dxGaugeDrawImage(AGPGraphics, FBackground, GetContentBounds);
end;

procedure TdxGaugeCustomScaleViewInfo.DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  if ALayerIndex = 0 then
    DrawBackground(AGPGraphics);
end;

procedure TdxGaugeCustomScaleViewInfo.LoadScaleElements;
begin
  FBackground := FStyle.GetElement(etBackground1);
end;

procedure TdxGaugeCustomScaleViewInfo.PopulateParameters(AScaleParameters: TdxGaugeCustomScaleParameters);
begin
  FScaleParameters.Assign(AScaleParameters);
end;

function TdxGaugeCustomScaleViewInfo.GetBounds: TdxRectF;
begin
  Result := FBounds;
end;

function TdxGaugeCustomScaleViewInfo.GetElementImage(AType: TdxGaugeElementType): TGraphic;
begin
  Result := FStyle.GetElement(AType);
end;

function TdxGaugeCustomScaleViewInfo.GetElementImageOriginalSize(AImage: TGraphic): TdxSizeF;
begin
  if AImage <> nil then
    if AImage is TdxCompositeShape then
    begin
      Result.cx := TdxCompositeShapeAccess(AImage).WidthF;
      Result.cy := TdxCompositeShapeAccess(AImage).HeightF;
    end
    else
    begin
      Result.cx := AImage.Width;
      Result.cy := AImage.Height;
    end;
end;

function TdxGaugeCustomScaleViewInfo.GetElementImageSize(AImage: TGraphic; AImageScaleFactor: TdxPointF): TdxSizeF;
begin
  if (AImage <> nil) and not AImage.Empty then
  begin
    Result := GetElementImageOriginalSize(AImage);
    Result.cx := Result.cx * FScaleFactor.X * AImageScaleFactor.X;
    Result.cy := Max(Result.cy * FScaleFactor.X * AImageScaleFactor.Y, 1);
  end;
end;

function TdxGaugeCustomScaleViewInfo.GetScaledSize(AOriginalSize: TdxSizeF): TdxSizeF;
begin
  Result.cx := AOriginalSize.cx * FScaleFactor.X;
  Result.cy := AOriginalSize.cy * FScaleFactor.Y;
end;

function TdxGaugeCustomScaleViewInfo.GetScaleRect(AOriginalRectSize: TdxSizeF; ANeedScaling: Boolean = True): TdxRectF;
var
  ASize: TdxSizeF;
begin
  if ANeedScaling then
    ASize := GetScaledSize(AOriginalRectSize)
  else
    ASize := AOriginalRectSize;
  Result := cxRectFBounds(cxRectCenter(FBounds).X - ASize.cx / 2,cxRectCenter(FBounds).Y - ASize.cy / 2, ASize.cx, ASize.cy);
end;

function TdxGaugeCustomScaleViewInfo.NeedDraw: Boolean;
begin
  Result := FScaleParameters.Visible and ((FScaleParameters.Height > 0) or (FScaleParameters.HeightFactor > 0)) and
    ((FScaleParameters.Width > 0) or (FScaleParameters.WidthFactor > 0));
end;

procedure TdxGaugeCustomScaleViewInfo.Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF);
begin
  PopulateParameters(AScaleParameters);
  CalculateBounds(ABounds);
  CalculateScaleFactor;
  CalculateContent;
end;

procedure TdxGaugeCustomScaleViewInfo.SetStyle(AStyle: TdxGaugeScaleStyle);
begin
  FreeAndNil(FStyle);
  FStyle := AStyle;
  LoadScaleElements;
end;

function TdxGaugeCustomScaleViewInfo.GetCenter(const ABounds: TdxRectF): TdxPointF;

  function GetOffset: TdxPointF;
  begin
    if FScaleParameters.CenterPositionType = sptFactor then
    begin
      Result.X := FScaleParameters.CenterPositionFactor.X * ABounds.Width;
      Result.Y := FScaleParameters.CenterPositionFactor.Y * ABounds.Height;
    end
    else
    begin
      Result.X := FScaleParameters.CenterPosition.X;
      Result.Y := FScaleParameters.CenterPosition.Y;
    end;
  end;

begin
  Result := cxPointOffset(ABounds.TopLeft, GetOffset);
end;

function TdxGaugeCustomScaleViewInfo.GetScaleDefaultParameters: TdxGaugeCustomScaleDefaultParameters;
begin
  Result := FStyle.DefaultParameters;
end;

procedure TdxGaugeCustomScaleViewInfo.CreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  FStyle := dxGaugeScaleStyleCollection.CreateStyle(AScaleType, AStyleName);
  LoadScaleElements;
end;

procedure TdxGaugeCustomScaleViewInfo.RecreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  FreeAndNil(FStyle);
  CreateStyle(AScaleType, AStyleName);
end;

{ TdxGaugeCustomScale }

constructor TdxGaugeCustomScale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FAnchorScale := nil;
  FStoredAnchorScaleIndex := -1;
  FChangedLayers := [sclStaticLayer, sclDynamicLayer];
  FStyleName := dxGaugeScaleStyleCollection.GetFirstStyleName(GetScaleType);
  InitScaleParameters;
  FViewInfo := GetViewInfoClass.Create(GetScaleType, FStyleName);
  ApplyStyleParameters;

  FDataBinding := GetDataBindingClass.Create(Self, Self);
  FDataBinding.OnDataChange := DataChangeHandler;
  FDataBinding.OnDataSetChange := DataSetChangeHandler;
  FOptionsLayout := GetOptionsLayoutClass.Create(Self);
  FOptionsView := GetOptionsViewClass.Create(Self);
end;

destructor TdxGaugeCustomScale.Destroy;
begin
  FreeAndNil(FOptionsView);
  FreeAndNil(FOptionsLayout);
  FreeAndNil(FDataBinding);
  FreeAndNil(FViewInfo);
  FreeAndNil(FScaleParameters);
  inherited Destroy;
end;

procedure TdxGaugeCustomScale.Assign(ASource: TPersistent);
begin
  if ASource is TdxGaugeCustomScale then
  begin
    Collection.BeginUpdate;
    try
      DoAssign(TdxGaugeCustomScale(ASource));
    finally
      Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(ASource);
end;

procedure TdxGaugeCustomScale.RestoreStyleParameters;
begin
  InternalRestoreStyleParameters;
end;

function TdxGaugeCustomScale.GetCollectionFromParent(AParent: TComponent): TcxComponentCollection;
begin
  Result := (AParent as TdxCustomGaugeControl).Scales;
end;

procedure TdxGaugeCustomScale.DefineProperties(AFiler: TFiler);
begin
  inherited DefineProperties(AFiler);
  AFiler.DefineProperty('ZOrder', ReadZOrder, WriteZOrder, ZOrder <> 0);
end;

procedure TdxGaugeCustomScale.Loaded;
begin
  inherited Loaded;
  Collection.BeginUpdate;
  AnchorScaleIndex := FStoredAnchorScaleIndex;
  FStoredAnchorScaleIndex := -1;
  Collection.EndUpdate;
end;

class function TdxGaugeCustomScale.GetLayerCount: Integer;
begin
  Result := 0;
end;

class function TdxGaugeCustomScale.GetScaleName: string;
begin
  Result := '';
end;

class function TdxGaugeCustomScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stCircularScale;
end;

function TdxGaugeCustomScale.GetDataBindingClass: TcxCustomDataBindingClass;
begin
  Result := TcxCustomDataBinding;
end;

function TdxGaugeCustomScale.GetOptionsLayoutClass: TdxGaugeScaleOptionsCustomLayoutClass;
begin
  Result := TdxGaugeScaleOptionsRectangularLayout;
end;

function TdxGaugeCustomScale.GetOptionsViewClass: TdxGaugeCustomScaleOptionsViewClass;
begin
  Result := TdxGaugeCustomScaleOptionsView;
end;

function TdxGaugeCustomScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeCustomScaleParameters;
end;

function TdxGaugeCustomScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeCustomScaleViewInfo;
end;

function TdxGaugeCustomScale.GetValidValue(const AValue: Variant): Variant;
begin
  Result := AValue;
end;

procedure TdxGaugeCustomScale.ApplyStyleParameters;
begin
//do nothing
end;

procedure TdxGaugeCustomScale.Calculate(const AScaleBounds: TdxRectF; const ACacheBounds: TRect);
begin
  Collection.BeginUpdate;
  ApplyStyleParameters;
  Collection.EndUpdate(False);
  ViewInfo.Calculate(FScaleParameters, AScaleBounds);
end;

procedure TdxGaugeCustomScale.DoAssign(AScale: TdxGaugeCustomScale);
begin
  AnchorScaleIndex := AScale.AnchorScaleIndex;
  StyleName := AScale.StyleName;
  FScaleParameters.Assign(AScale.FScaleParameters);
end;

procedure TdxGaugeCustomScale.DoDrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  ViewInfo.DrawLayer(AGPGraphics, ALayerIndex);
end;

procedure TdxGaugeCustomScale.DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  if ViewInfo.NeedDraw then
    DoDrawLayer(AGPGraphics, ALayerIndex);
end;

procedure TdxGaugeCustomScale.InitScaleParameters;
begin
  FScaleParameters := GetScaleParametersClass.Create;
  FScaleParameters.WidthFactor := dxGaugeScaleWidthFactor;
  FScaleParameters.HeightFactor := dxGaugeScaleHeightFactor;
  FScaleParameters.CenterPositionFactor.X := dxGaugeScaleCenterPositionFactor;
  FScaleParameters.CenterPositionFactor.Y := dxGaugeScaleCenterPositionFactor;
  FScaleParameters.Value := dxGaugeScaleDefaultValue;
  FScaleParameters.ShowBackground := True;
  FScaleParameters.Visible := True;
end;

procedure TdxGaugeCustomScale.InternalRestoreStyleParameters;
begin
  ScaleChanged([sclStaticLayer, sclDynamicLayer]);
end;

function TdxGaugeCustomScale.GetSelectionRect: TRect;
begin
  Result := cxRect(ViewInfo.GetContentBounds, False);
end;

procedure TdxGaugeCustomScale.InternalSetValue(const AValue: Variant);
var
  AValidValue: Variant;
begin
  AValidValue := GetValidValue(AValue);
  if not VarSameValue(FScaleParameters.Value, AValidValue) then
  begin
    FScaleParameters.Value := AValidValue;
    ScaleChanged([sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.ScaleChanged(ALayers: TdxGaugeScaleChangedLayers);
begin
  if sclStaticLayer in ALayers then
    Include(FChangedLayers, sclStaticLayer);
  if sclDynamicLayer in ALayers then
    Include(FChangedLayers, sclDynamicLayer);
  Changed(False);
end;

function TdxGaugeCustomScale.GetAnchorScale: TdxGaugeCustomScale;
begin
  Result := FAnchorScale;
end;

function TdxGaugeCustomScale.GetAnchorScaleIndex: Integer;
var
  AIndex: Integer;
begin
  if FAnchorScale = nil then
    AIndex := -1
  else
    AIndex := FAnchorScale.Index;
 Result := AIndex;
end;

function TdxGaugeCustomScale.GetBounds: TdxRectF;
begin
  Result := ViewInfo.GetBounds;
end;

function TdxGaugeCustomScale.GetCenterPositionType: TdxGaugeScalePositionType;
begin
  Result := FScaleParameters.CenterPositionType;
end;

function TdxGaugeCustomScale.GetCenterPositionX: Integer;
begin
  Result := FScaleParameters.CenterPosition.X;
end;

function TdxGaugeCustomScale.GetCenterPositionFactorX: Single;
begin
  Result := FScaleParameters.CenterPositionFactor.X;
end;

function TdxGaugeCustomScale.GetCenterPositionY: Integer;
begin
  Result := FScaleParameters.CenterPosition.Y;
end;

function TdxGaugeCustomScale.GetCenterPositionFactorY: Single;
begin
  Result := FScaleParameters.CenterPositionFactor.Y;
end;

function TdxGaugeCustomScale.GetContentBounds: TdxRectF;
begin
  Result := ViewInfo.GetContentBounds;
end;

function TdxGaugeCustomScale.GetDataBinding: TcxCustomDataBinding;
begin
  Result := FDataBinding;
end;

function TdxGaugeCustomScale.GetHeight: Integer;
begin
  Result := FScaleParameters.Height;
end;

function TdxGaugeCustomScale.GetHeightFactor: Single;
begin
  Result := FScaleParameters.HeightFactor;
end;

function TdxGaugeCustomScale.GetOptionsLayout: TdxGaugeScaleOptionsRectangularLayout;
begin
  Result := FOptionsLayout as TdxGaugeScaleOptionsRectangularLayout;
end;

function TdxGaugeCustomScale.GetShowBackground: Boolean;
begin
  Result := FScaleParameters.ShowBackground;
end;

function TdxGaugeCustomScale.GetStretch: Boolean;
begin
  Result := FScaleParameters.Stretch;
end;

function TdxGaugeCustomScale.GetValue: Variant;
begin
  Result := FScaleParameters.Value;
end;

function TdxGaugeCustomScale.GetVisible: Boolean;
begin
  Result := FScaleParameters.Visible;
end;

function TdxGaugeCustomScale.GetWidth: Integer;
begin
  Result := FScaleParameters.Width;
end;

function TdxGaugeCustomScale.GetWidthFactor: Single;
begin
  Result := FScaleParameters.WidthFactor;
end;

function TdxGaugeCustomScale.GetZOrder: Integer;
begin
  if Collection <> nil then
    Result := dxGaugeController(TdxGaugeScaleCollection(Collection)).ZOrders.IndexOf(Self)
  else
    Result := -1;
end;

procedure TdxGaugeCustomScale.SetAnchorScale(const AValue: TdxGaugeCustomScale);
begin
  if (AValue = nil) or dxGaugeController(Collection as TdxGaugeScaleCollection).CanAnchorScale(Index, AValue.Index) then
  begin
    FAnchorScale := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetAnchorScaleIndex(const AValue: Integer);
begin
  if (ComponentState * [csReading, csLoading] <> []) then
    FStoredAnchorScaleIndex := AValue
  else
    if dxGaugeController(Collection as TdxGaugeScaleCollection).CanAnchorScale(Index, AValue) then
    begin
      FAnchorScale := GetAnchorScaleByIndex(AValue);
      ScaleChanged([sclStaticLayer, sclDynamicLayer]);
    end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionType(const AValue: TdxGaugeScalePositionType);
begin
  if FScaleParameters.CenterPositionType <> AValue then
  begin
    FScaleParameters.CenterPositionType := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionX(const AValue: Integer);
begin
  if FScaleParameters.CenterPosition.X <> AValue then
  begin
    FScaleParameters.CenterPosition := Point(AValue, FScaleParameters.CenterPosition.Y);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionFactorX(const AValue: Single);
begin
  if not SameValue(FScaleParameters.CenterPositionFactor.X, AValue) then
  begin
    FScaleParameters.CenterPositionFactor := dxPointF(AValue, FScaleParameters.CenterPositionFactor.Y);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionY(const AValue: Integer);
begin
  if FScaleParameters.CenterPosition.Y <> AValue then
  begin
    FScaleParameters.CenterPosition := Point(FScaleParameters.CenterPosition.X, AValue);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionFactorY(const AValue: Single);
begin
  if not SameValue(FScaleParameters.CenterPositionFactor.Y, AValue) then
  begin
    FScaleParameters.CenterPositionFactor := dxPointF(FScaleParameters.CenterPositionFactor.X, AValue);
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetDataBinding(const AValue: TcxCustomDataBinding);
begin
  FDataBinding.Assign(AValue);
end;

procedure TdxGaugeCustomScale.SetHeight(const AValue: Integer);
begin
  if (FScaleParameters.Height <> AValue) and (AValue >= 0) then
  begin
    FScaleParameters.Height := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetHeightFactor(const AValue: Single);
begin
  if (FScaleParameters.HeightFactor <> AValue) and (AValue > 0) then
  begin
    FScaleParameters.HeightFactor := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetOptionsLayout(const AValue: TdxGaugeScaleOptionsRectangularLayout);
begin
  FOptionsLayout.Assign(AValue);
end;

procedure TdxGaugeCustomScale.SetOptionsView(const AValue: TdxGaugeCustomScaleOptionsView);
begin
  FOptionsView.Assign(AValue);
end;

procedure TdxGaugeCustomScale.SetShowBackground(const AValue: Boolean);
begin
  if FScaleParameters.ShowBackground <> AValue then
  begin
    FScaleParameters.ShowBackground := AValue;
    ScaleChanged([sclStaticLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetStretch(const AValue: Boolean);
begin
  if FScaleParameters.Stretch <> AValue then
  begin
    FScaleParameters.Stretch := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetStyleName(const AValue: string);
begin
  if not SameText(FStyleName, AValue) then
  begin
    FStyleName := AValue;
    Collection.BeginUpdate;
    try
      ViewInfo.RecreateStyle(GetScaleType, FStyleName);
      ApplyStyleParameters;
    finally
      Collection.EndUpdate(False);
    end;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetValue(const AValue: Variant);
begin
  InternalSetValue(AValue);
end;

procedure TdxGaugeCustomScale.SetVisible(const AValue: Boolean);
begin
  if FScaleParameters.Visible <> AValue then
  begin
    FScaleParameters.Visible := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetWidth(const AValue: Integer);
begin
  if (FScaleParameters.Width <> AValue) and (AValue >= 0) then
  begin
    FScaleParameters.Width := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

procedure TdxGaugeCustomScale.SetWidthFactor(const AValue: Single);
begin
  if not SameValue(FScaleParameters.WidthFactor, AValue) and (AValue > 0) then
  begin
    FScaleParameters.WidthFactor := AValue;
    ScaleChanged([sclStaticLayer, sclDynamicLayer]);
  end;
end;

function TdxGaugeCustomScale.GetAnchorScaleByIndex(AAnchorScaleIndex: Integer): TdxGaugeCustomScale;
begin
  if AAnchorScaleIndex <> -1 then
    Result := Collection[AAnchorScaleIndex] as TdxGaugeCustomScale
  else
    Result := nil;
end;

procedure TdxGaugeCustomScale.DataChangeHandler;

  function InternalGetValue: Variant;
  begin
    if FDataBinding.IsDataSourceLive then
    begin
      Result := FDataBinding.GetStoredValue(evsValue, True);
      if VarIsNull(Result) then
        Result := 0;
    end;
  end;

begin
  Value := InternalGetValue;
end;

procedure TdxGaugeCustomScale.DataSetChangeHandler;
begin
  ScaleChanged([sclDynamicLayer]);
end;

procedure TdxGaugeCustomScale.ReadZOrder(AReader: TReader);
begin
  FZOrder := AReader.ReadInteger;
end;

procedure TdxGaugeCustomScale.WriteZOrder(AWriter: TWriter);
begin
  AWriter.WriteInteger(ZOrder);
end;

function TdxGaugeCustomScale.IsAnchorScaleIndexStored: Boolean;
begin
  Result := AnchorScaleIndex <> -1;
end;

function TdxGaugeCustomScale.IsCenterPositionFactorXStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.CenterPositionFactor.X, dxGaugeScaleCenterPositionFactor);
end;

function TdxGaugeCustomScale.IsCenterPositionFactorYStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.CenterPositionFactor.Y, dxGaugeScaleCenterPositionFactor);
end;

function TdxGaugeCustomScale.IsStyleNameStored: Boolean;
begin
  Result := not SameText(FStyleName, dxGaugeScaleStyleCollection.GetFirstStyleName(GetScaleType));
end;

function TdxGaugeCustomScale.IsHeightFactorStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.HeightFactor, dxGaugeScaleHeightFactor);
end;

function TdxGaugeCustomScale.IsWidthFactorStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.WidthFactor, dxGaugeScaleWidthFactor);
end;

{ TdxGaugeScaleCollection }

procedure TdxGaugeScaleCollection.Assign(ASource: TPersistent);
var
  I: Integer;
begin
  if ASource is TdxGaugeScaleCollection then
  begin
    Clear;
    for I := 0 to TdxGaugeScaleCollection(ASource).Count - 1 do
      Add(TdxGaugeCustomScaleClass(TdxGaugeScaleCollection(ASource).Items[I].ClassType));
    for I := 0 to TdxGaugeScaleCollection(ASource).Count - 1 do
      Items[I].Assign(TdxGaugeScaleCollection(ASource).Items[I]);
  end
  else
    inherited Assign(ASource);
end;

function TdxGaugeScaleCollection.Add(AScaleClass: TdxGaugeCustomScaleClass): TdxGaugeCustomScale;
begin
  Result := AScaleClass.Create(ParentComponent.Owner);
  Result.SetParentComponent(ParentComponent);
  SetItemName(Result);
end;

function TdxGaugeScaleCollection.GetItemPrefixName: string;
begin
  Result := 'TdxGauge';
end;

procedure TdxGaugeScaleCollection.Notify(AItem: TcxComponentCollectionItem; AAction: TcxComponentCollectionNotification);
begin
  if (ParentComponent <> nil) and not (csDestroying in ParentComponent.ComponentState) then
    case AAction of
      ccnAdded:
        dxCallNotify(OnAdded, AItem);
      ccnExtracted, ccnDeleting:
        dxCallNotify(OnDeleted, AItem);
    end;
  inherited Notify(AItem, AAction);
end;

function TdxGaugeScaleCollection.GetScale(AIndex: Integer): TdxGaugeCustomScale;
begin
  Result := inherited GetItem(AIndex) as TdxGaugeCustomScale;
end;

procedure TdxGaugeScaleCollection.SetScale(AIndex: Integer; const AValue: TdxGaugeCustomScale);
begin
  inherited SetItem(AIndex, AValue);
end;

{ TdxGaugeScaleStyle }

constructor TdxGaugeScaleStyle.Create(const AStyleName: string);
begin
  inherited Create;
  FName := AStyleName;
end;

constructor TdxGaugeScaleStyle.Create(const AStyleName, ABaseStyleName: string);
begin
  Create(AStyleName);
  FBaseStyleName := ABaseStyleName;
end;

destructor TdxGaugeScaleStyle.Destroy;
var
  I: TdxGaugeElementType;
begin
  for I := Low(FElements) to High(FElements) do
  begin
    FElements[I].Free;
    FElements[I] := nil;
  end;
  FreeAndNil(FDefaultParameters);
  FreeAndNil(FShapes);
  inherited Destroy;
end;

function TdxGaugeScaleStyle.GetElement(AElementType: TdxGaugeElementType): TdxCompositeShape;
begin
  if FElements[AElementType] = nil then
  begin
    FElements[AElementType] := TdxCompositeShape.Create;
    TdxCompositeShapeAccess(FElements[AElementType]).LoadFromShape(FShapes, dxGaugeStyleElementNameMap[AElementType]);
    if FElements[AElementType].Empty then
      FreeAndNil(FElements[AElementType]);
  end;
  Result := FElements[AElementType];
end;

procedure TdxGaugeScaleStyle.AddElement(AElementType: TdxGaugeElementType; AElement: TdxCompositeShape);
begin
  if FElements[AElementType] <> nil then
    FreeAndNil(FElements[AElementType]);
  FElements[AElementType] := AElement;
end;

{ TdxGaugeCustomScaleStyleReader }

class function TdxGaugeCustomScaleStyleReader.GetParametersNodeName: AnsiString;
begin
  Result := dxStringToAnsiString(GetResourceNamePreffix + 'Scale');
end;

class function TdxGaugeCustomScaleStyleReader.GetResourceNamePreffix: string;
begin
  Result := '';
end;

function TdxGaugeCustomScaleStyleReader.CreateStyle(ADocument: TdxXMLDocument): TdxGaugeScaleStyle;
begin
  Result := TdxGaugeScaleStyle.Create(GetAttributeValueAsString(ADocument.Root.First, 'Name'),
    GetAttributeValueAsString(ADocument.Root.First, 'BaseStyle'));
  Result.Shapes := ReadCompositeShape(ADocument);
  Result.DefaultParameters := ReadDefaultParameters(ADocument);
  Result.ScaleTypeName := GetAttributeValueAsString(ADocument.Root.First, 'Type');
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsAlphaColor(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TdxAlphaColor;
begin
  Result := dxColorNameToAlphaColor(ANode.Attributes.GetValueAsString(AAttributeName));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsBoolean(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): Boolean;
begin
  if ANode <> nil then
    Result := ANode.Attributes.GetValueAsBoolean(AAttributeName)
  else
  Result := False;
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsColor(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TColor;
begin
  Result := dxAlphaColorToColor(dxColorNameToAlphaColor(ANode.Attributes.GetValueAsString(AAttributeName)));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsDouble(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): Double;
begin
  Result := ANode.Attributes.GetValueAsFloat(AAttributeName);
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsElementType(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TdxGaugeElementType;
begin
  Result := GetElementType(ANode.Attributes.GetValueAsString(AAttributeName));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsInteger(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): Integer;
begin
  Result := ANode.Attributes.GetValueAsInteger(AAttributeName);
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsPointF(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TdxPointF;
begin
  if ANode <> nil then
    Result := dxStrToPointF(ANode.Attributes.GetValueAsString(AAttributeName))
  else
    Result := cxPointF(0, 0);
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsString(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): string;
begin
  Result := ANode.Attributes.GetValueAsString(AAttributeName);
end;

function TdxGaugeCustomScaleStyleReader.GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString;
  out AChildNode: TdxXMLNode): Boolean;
begin
  AChildNode := GetChildNode(ANode, AChildNodeName);
  Result := AChildNode <> nil;
end;

function TdxGaugeCustomScaleStyleReader.GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString): TdxXMLNode;
var
  AChildNode: TdxXMLNode;
begin
  if ANode.FindChild(AChildNodeName, AChildNode) then
    Result := AChildNode
  else
    Result := nil;
end;

function TdxGaugeCustomScaleStyleReader.GetElementType(const ATypeName: string): TdxGaugeElementType;
var
  I: TdxGaugeElementType;
begin
  Result := etBackground1;
  for I := Low(dxGaugeStyleElementNameMap) to High(dxGaugeStyleElementNameMap) do
    if SameText(dxGaugeStyleElementNameMap[I], ATypeName) then
    begin
      Result := I;
      Break;
    end;
end;

function TdxGaugeCustomScaleStyleReader.GetStyleRootNode(ADocument: TdxXMLDocument): TdxXMLNode;
begin
  Result := GetChildNode(ADocument.Root, 'ScaleStyle');
end;

function TdxGaugeCustomScaleStyleReader.ReadCompositeShape(ADocument: TdxXMLDocument): TdxCompositeShape;
var
  ACompositeShapeNode: TdxXMLNode;
begin
  if GetChildNode(GetStyleRootNode(ADocument), 'CompositeShape', ACompositeShapeNode) then
  begin
    Result := TdxCompositeShape.Create;
    TdxCompositeShapeAccess(Result).LoadFromNode(ACompositeShapeNode);
  end
  else
    Result := nil;
end;

function TdxGaugeCustomScaleStyleReader.ReadDefaultParameters(ADocument: TdxXMLDocument):
  TdxGaugeCustomScaleDefaultParameters;
var
  ADefaultParametersNode: TdxXMLNode;
  ACustomScaleParametersNode: TdxXMLNode;
begin
  if GetChildNode(GetStyleRootNode(ADocument), 'DefaultParameters', ADefaultParametersNode) then
  begin
    Result := GetDefaultParametersClass.Create;
    if ADefaultParametersNode.FindChild(GetParametersNodeName, ACustomScaleParametersNode) then
      ReadParameters(ACustomScaleParametersNode, Result);
  end
  else
    Result := nil;
end;

{ TdxGaugeScaleStyleCreator }

function TdxGaugeScaleStyleCreator.CreateStyle(AStyleInfo: TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle;
begin
  if AStyleInfo <> nil then
    if not AStyleInfo.IsExternalStyle then
      Result := CreateFromResource(HInstance, AStyleInfo.ResourceName)
    else
      Result := CreateStyle(AStyleInfo.ResourceName)
  else
    Result := nil;
end;

function TdxGaugeScaleStyleCreator.CreateStyle(AInstance: THandle; const AResName: string): TdxXMLDocument;
var
  AStream: TResourceStream;
begin
  AStream := TResourceStream.Create(AInstance, AResName, RT_RCDATA);
  try
    Result := InternalCreateStyleDocument(AStream);
  finally
    AStream.Free;
  end;
end;

function TdxGaugeScaleStyleCreator.CreateStyle(const AFileName: string): TdxGaugeScaleStyle;
var
  AStream: TStream;
begin
  AStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  try
    Result := CreateFromStream(AStream);
  finally
    AStream.Free;
  end;
end;

function TdxGaugeScaleStyleCreator.CreateStyleDocument(AStyleInfo: TdxGaugeScaleStyleInfo): TdxXMLDocument;
begin
  if AStyleInfo <> nil then
    if not AStyleInfo.IsExternalStyle then
      Result := CreateStyle(HInstance, AStyleInfo.ResourceName)
    else
      Result := CreateStyleDocument(AStyleInfo.ResourceName)
  else
    Result := nil;
end;

function TdxGaugeScaleStyleCreator.CreateStyleDocument(const AFileName: string): TdxXMLDocument;
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  try
    Result := InternalCreateStyleDocument(AStream);
  finally
    AStream.Free;
  end;
end;

function TdxGaugeScaleStyleCreator.CreateFromResource(AInstance: THandle; const AResName: string): TdxGaugeScaleStyle;
var
  AStream: TResourceStream;
begin
  AStream := TResourceStream.Create(AInstance, AResName, RT_RCDATA);
  try
    Result := CreateFromStream(AStream);
  finally
    AStream.Free;
  end;
end;

function TdxGaugeScaleStyleCreator.CreateFromStream(AStream: TStream): TdxGaugeScaleStyle;
var
  AXMLDocument: TdxXMLDocument;
  AReader: TdxGaugeCustomScaleStyleReader;
begin
  AXMLDocument := InternalCreateStyleDocument(AStream);
  AReader := GetStyleReaderClass(AXMLDocument).Create;
  try
    Result := AReader.CreateStyle(AXMLDocument);
  finally
    AReader.Free;
    AXMLDocument.Free;
  end;
end;

function TdxGaugeScaleStyleCreator.CreateFromStyle(ADestinationStyleInfo,  ASourceStyleInfo:
  TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle;

  procedure LoadElements(ADestinationStyle, ASourceStyle: TdxGaugeScaleStyle);
  var
    I: TdxGaugeElementType;
    AShape: TdxCompositeShape;
  begin
    for I := Low(TdxGaugeElementType) to High(TdxGaugeElementType) do
      if ADestinationStyle.GetElement(I) = nil then
      begin
        AShape := TdxCompositeShape.Create;
        TdxCompositeShapeAccess(AShape).LoadFromShape(ASourceStyle.Shapes, dxGaugeStyleElementNameMap[I]);
        if not AShape.Empty then
          ADestinationStyle.AddElement(I, AShape)
        else
          AShape.Free;
      end;
  end;

var
  ASourceStyle: TdxGaugeScaleStyle;
begin
  ASourceStyle := CreateStyle(ASourceStyleInfo);
  try
    Result := CreateStyle(ADestinationStyleInfo);
    LoadElements(Result, ASourceStyle);
  finally
    ASourceStyle.Free;
  end;
end;

function TdxGaugeScaleStyleCreator.GetStyleReaderClass(ADocument: TdxXMLDocument): TdxGaugeCustomScaleStyleReaderClass;
var
  ANode: TdxXMLNode;
begin
  Result := TdxGaugeCustomScaleStyleReader;
  if not (IsStyleDocumentAvailable(ADocument) and ADocument.Root.FindChild('ScaleStyle', ANode) and
    ANode.FindChild('DefaultParameters', ANode) and IdentifyReaderClass(ANode, Result)) then
    dxGaugeRaiseException(sdxGaugeScaleStyleCollectionInvalidStyleFileFormat, []);
end;

function TdxGaugeScaleStyleCreator.IdentifyReaderClass(ADefaultParametersNode: TdxXMLNode;
  var AReaderClass: TdxGaugeCustomScaleStyleReaderClass): Boolean;
var
  I: Integer;
  ATypeInfo: TdxGaugeScaleTypeInfo;
begin
  Result := False;
  for I := 0 to dxGaugeScaleTypeInfos.GetTypeCount - 1 do
  begin
    ATypeInfo := dxGaugeScaleTypeInfos.GetTypeInfo(I);
    Result := ADefaultParametersNode.FindChild(ATypeInfo.ReaderClass.GetParametersNodeName) <> nil;
    if Result then
    begin
      AReaderClass := ATypeInfo.ReaderClass;
      Break;
    end;
  end;
end;

function TdxGaugeScaleStyleCreator.InternalCreateStyleDocument(AStream: TStream): TdxXMLDocument;
begin
  Result := TdxXMLDocument.Create(nil);
  Result.LoadFromStream(AStream);
  if not IsStyleDocumentAvailable(Result) then
    FreeAndNil(Result);
end;

function TdxGaugeScaleStyleCreator.IsStyleDocumentAvailable(ADocument: TdxXMLDocument): Boolean;
begin
  Result := (ADocument <> nil) and (ADocument.Root <> nil);
end;

{ TdxGaugeScaleStyleCollection }

constructor TdxGaugeScaleStyleCollection.Create;
begin
  inherited Create;
  FStyles := TDictionary<TdxGaugeScaleType, TStringList>.Create;
  FStyleCreator := TdxGaugeScaleStyleCreator.Create;
  FPredefinedStyleNames := TStringList.Create(True);
  InitPredefinedStyleNames;
end;

destructor TdxGaugeScaleStyleCollection.Destroy;
begin
  FreeAndNil(FPredefinedStyleNames);
  FreeAndNil(FStyleCreator);
  InternalUnregisterStyles;
  FreeAndNil(FStyles);
  inherited Destroy;
end;

procedure TdxGaugeScaleStyleCollection.GetPredefinedStyleNames(AList: TStringList);
var
  I: Integer;
begin
  if AList <> nil then
    for I := 0 to FPredefinedStyleNames.Count - 1 do
      AList.Add(FPredefinedStyleNames.Strings[I]);
end;

function TdxGaugeScaleStyleCollection.RegisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string): Boolean;

  function CreateStyleInfo: TdxGaugeScaleStyleInfo; 
  begin                                             
    Result := TdxGaugeScaleStyleInfo.Create;
    Result.Name := AStyleName;
    Result.ScaleType := AScaleType;
    Result.ResourceName := dxGaugeScaleTypeInfos.GetTypeName(AScaleType) + AStyleName;
    Result.IsExternalStyle := False;
    if IsInheritedStyle(Result.ScaleType) then
      Result.BaseStyleName := AStyleName;
    end;

begin
  Result := InternalRegisterStyle(AScaleType, CreateStyleInfo);
end;

function TdxGaugeScaleStyleCollection.RegisterStyleFromFile(AScaleType: TdxGaugeScaleType; const AFileName: string): Boolean;
begin
  Result := InternalRegisterStyle(AScaleType, CreateStyleInfo(AFileName));
end;

procedure TdxGaugeScaleStyleCollection.ExtractStyleToFile(AScaleType: TdxGaugeScaleType; const AStyleName,
  AFileName: string);
var
  AXMLDocument: TdxXMLDocument;
begin
  AXMLDocument := ExctractStyleToDocument(AScaleType, AStyleName);
  try
    AXMLDocument.SaveToFile(AFileName);
  finally
    AXMLDocument.Free;
  end;
end;

procedure TdxGaugeScaleStyleCollection.ExtractStyleToStream(AScaleType: TdxGaugeScaleType; const AStyleName: string;
  AStream: TStream);
var
  AXMLDocument: TdxXMLDocument;
begin
  AXMLDocument := ExctractStyleToDocument(AScaleType, AStyleName);
  try
    AXMLDocument.SaveToStream(AStream);
  finally
    AXMLDocument.Free;
  end;
end;

procedure TdxGaugeScaleStyleCollection.UnregisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  InternalUnregisterStyle(GetStyles(AScaleType), AStyleName);
end;

function TdxGaugeScaleStyleCollection.CreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string):
  TdxGaugeScaleStyle;
begin
  Result := InternalCreateStyle(GetStyleInfo(GetStyles(AScaleType), AStyleName));
end;

function TdxGaugeScaleStyleCollection.CreateStyleFromFile(const AFileName: string): TdxGaugeScaleStyle;
begin
  Result := FStyleCreator.CreateStyle(AFileName);
end;

function TdxGaugeScaleStyleCollection.GetFirstStyleName(AScaleType: TdxGaugeScaleType): string;
begin
  Result := GetStyles(AScaleType).Strings[0];
end;

function TdxGaugeScaleStyleCollection.GetStyles(AScaleType: TdxGaugeScaleType): TStringList;
var
  AList: TStringList;
begin
  if FStyles.TryGetValue(AScaleType, AList) then
    Result := AList
  else
  begin
    FStyles.Add(AScaleType, CreateStyleList);
    Result := GetStyles(AScaleType);
  end;
end;

function TdxGaugeScaleStyleCollection.HasStyles(AScaleType: TdxGaugeScaleType): Boolean;
begin
  Result := GetStyles(AScaleType).Count > 0;
end;

procedure TdxGaugeScaleStyleCollection.RegisterPredefinedStyles(AScaleClass: TdxGaugeCustomScaleClass);
var
  I: Integer;
begin
  for I := 0 to FPredefinedStyleNames.Count - 1 do
    RegisterStyle(AScaleClass.GetScaleType, FPredefinedStyleNames.Strings[I]);
end;

procedure TdxGaugeScaleStyleCollection.UnregisterPredefinedStyles(AScaleClass: TdxGaugeCustomScaleClass);
var
  I: Integer;
begin
  for I := 0 to FPredefinedStyleNames.Count - 1 do
    UnregisterStyle(AScaleClass.GetScaleType, FPredefinedStyleNames.Strings[I]);
end;

procedure TdxGaugeScaleStyleCollection.UnregisterStyles(AScaleType: TdxGaugeScaleType);

  procedure UnregistryStyles(AStyles: TStringList);
  begin
    if AStyles <> nil then
      while AStyles.Count > 0 do
        DeleteStyle(AStyles, 0);
  end;

var
  AStyleList: TStringList;
begin
  if FStyles.TryGetValue(AScaleType, AStyleList) then
    UnregistryStyles(AStyleList);
  FStyles.Remove(AScaleType);
  AStyleList.Free;
end;

function TdxGaugeScaleStyleCollection.CreateStyleInfo(const AFileName: string): TdxGaugeScaleStyleInfo;
var
  AStyle: TdxGaugeScaleStyle;
begin
  AStyle := FStyleCreator.CreateStyle(AFileName);
  try
    Result := TdxGaugeScaleStyleInfo.Create;
    Result.Name := AStyle.Name;
    Result.ResourceName := AFileName;
    Result.IsExternalStyle := True;
    Result.ScaleType := dxGaugeScaleTypeInfos.GetTypeByName(AStyle.ScaleTypeName);
    Result.BaseStyleName := AStyle.BaseStyleName;
  finally
    AStyle.Free;
  end;
end;

function TdxGaugeScaleStyleCollection.CreateStyleList: TStringList;
begin
  Result := TStringList.Create;
  Result.Sorted := True;
  Result.Duplicates := dupError;
end;

function TdxGaugeScaleStyleCollection.ExctractStyleToDocument(AScaleType: TdxGaugeScaleType; const AStyleName: string):
  TdxXMLDocument;
var
  AStyleInfo: TdxGaugeScaleStyleInfo;
begin
  AStyleInfo := GetStyleInfo(GetStyles(AScaleType), AStyleName);
  Result := FStyleCreator.CreateStyleDocument(AStyleInfo);
end;

function TdxGaugeScaleStyleCollection.GetStyleInfo(AStyles: TStringList; const AStyleName: string): TdxGaugeScaleStyleInfo;
var
  AIndex: Integer;
begin
  if AStyles <> nil then
    if AStyles.Find(AStyleName, AIndex) then
      Result := TdxGaugeScaleStyleInfo(AStyles.Objects[AIndex])
    else
      Result := TdxGaugeScaleStyleInfo(AStyles.Objects[0])
  else
    Result := nil;
end;

function TdxGaugeScaleStyleCollection.InternalCreateStyle(AStyleInfo: TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle;
begin
  if not SameText(AStyleInfo.BaseStyleName, '') then
    Result := FStyleCreator.CreateFromStyle(AStyleInfo, GetStyleInfo(GetStyles(stCircularScale), AStyleInfo.BaseStyleName))
  else
    Result := FStyleCreator.CreateStyle(AStyleInfo);
end;

function TdxGaugeScaleStyleCollection.InternalRegisterStyle(AScaleType: TdxGaugeScaleType;
  AStyleInfo: TdxGaugeScaleStyleInfo): Boolean;
var
  AStyles: TStringList;
begin
  AStyles := GetStyles(AStyleInfo.ScaleType);
  if AScaleType = AStyleInfo.ScaleType then
  begin
    if (AStyles <> nil) and (AStyles.IndexOf(AStyleInfo.Name) = -1) then
    begin
      if (AStyleInfo.BaseStyleName = '') then
        AStyles.AddObject(AStyleInfo.Name, TObject(AStyleInfo))
      else
      begin
        if IsRegisteredStyle(stCircularScale, AStyleInfo.BaseStyleName) then
          AStyles.AddObject(AStyleInfo.Name, TObject(AStyleInfo))
        else
          ProcessError(AStyleInfo, 0);
      end;
    end
    else
      ProcessError(AStyleInfo, 2);
  end
  else
    ProcessError(AStyleInfo, 1);
  Result := True;
end;

function TdxGaugeScaleStyleCollection.InternalUnregisterStyle(AStyles: TStringList; const AStyleName: string): Boolean;
var
  AIndex: Integer;
begin
  Result := AStyles.Find(AStyleName, AIndex);
  if Result then
    DeleteStyle(AStyles, AIndex);
end;

function TdxGaugeScaleStyleCollection.IsInheritedStyle(AScaleType: TdxGaugeScaleType): Boolean;
begin
  Result := AScaleType in [stCircularHalfScale, stCircularQuarterLeftScale, stCircularQuarterRightScale];
end;

function TdxGaugeScaleStyleCollection.IsRegisteredStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string): Boolean;
var
  AStyles: TStringList;
begin
  AStyles := GetStyles(AScaleType);
  Result := (AStyles.Count > 0) and (AStyles.IndexOf(AStyleName) <> -1);
end;

procedure TdxGaugeScaleStyleCollection.DeleteStyle(AStyles: TStringList; AIndex: Integer);
begin
  if AStyles <> nil then
  begin
    AStyles.Objects[AIndex].Free;
    AStyles.Delete(AIndex);
  end;
end;

procedure TdxGaugeScaleStyleCollection.InitPredefinedStyleNames;
begin
  FPredefinedStyleNames.Add(dxGaugeAfricaSunsetStyleName);
  FPredefinedStyleNames.Add(dxGaugeClassicStyleName);
  FPredefinedStyleNames.Add(dxGaugeCleanWhiteStyleName);
  FPredefinedStyleNames.Add(dxGaugeDarkNightStyleName);
  FPredefinedStyleNames.Add(dxGaugeDeepFireStyleName);
  FPredefinedStyleNames.Add(dxGaugeDiscoStyleName);
  FPredefinedStyleNames.Add(dxGaugeIceColdZoneStyleName);
  FPredefinedStyleNames.Add(dxGaugeiStyleStyleName);
  FPredefinedStyleNames.Add(dxGaugeFutureStyleName);
  FPredefinedStyleNames.Add(dxGaugeMechanicalStyleName);
  FPredefinedStyleNames.Add(dxGaugeMilitaryStyleName);
  FPredefinedStyleNames.Add(dxGaugeRetroStyleName);
  FPredefinedStyleNames.Add(dxGaugeSmartStyleName);
  FPredefinedStyleNames.Add(dxGaugeSportCarStyleName);
  FPredefinedStyleNames.Add(dxGaugeYellowSubmarinStyleName);
  FPredefinedStyleNames.Add(dxGaugeWhiteStyleName);
end;

procedure TdxGaugeScaleStyleCollection.InternalUnregisterStyles;
var
  AScaleType: TdxGaugeScaleType;
begin
  for AScaleType in FStyles.Keys do
    UnregisterStyles(AScaleType);
end;

procedure TdxGaugeScaleStyleCollection.ProcessError(AStyleInfo: TdxGaugeScaleStyleInfo; AErrorCode: Integer);
var
  ABaseStyleName, AStyleName: string;
  AScaleType: TdxGaugeScaleType;
begin
  AStyleName := AStyleInfo.Name;
  ABaseStyleName := AStyleInfo.BaseStyleName;
  AScaleType := AStyleInfo.ScaleType;
  FreeAndNil(AStyleInfo);
  case AErrorCode of
    0:
      dxGaugeRaiseException(sdxGaugeScaleStyleCollectionBaseStyleUnregistered, [ABaseStyleName,
        AStyleName, dxGaugeScaleTypeInfos.GetFormatedTypeName(AScaleType),
        dxGaugeScaleTypeInfos.GetFormatedTypeName(stCircularScale)]);
    1:
      dxGaugeRaiseException(sdxGaugeScaleStyleCollectionInvalidStyleFileFormat, []);
    2:
      dxGaugeRaiseException(sdxGaugeScaleStyleCollectionDublicateStyleName, [AStyleName,
        dxGaugeScaleTypeInfos.GetFormatedTypeName(AScaleType)]);
  end;
end;

{ TdxGaugeScaleFactory }

constructor TdxGaugeScaleFactory.Create;
begin
  inherited Create;
  FScaleClasses := TStringList.Create;
end;

destructor TdxGaugeScaleFactory.Destroy;
begin
  FreeAndNil(FScaleClasses);
  inherited;
end;

procedure TdxGaugeScaleFactory.RegisterScale(AScaleClass: TdxGaugeCustomScaleClass; ARegisterPredefinedStyle: Boolean = True);
begin
  if FScaleClasses.IndexOfObject(TObject(AScaleClass)) = -1 then
  begin
    if FScaleClasses.IndexOf(AScaleClass.GetScaleName) = -1 then
    begin
      FScaleClasses.AddObject(AScaleClass.GetScaleName, TObject(AScaleClass));
      if ARegisterPredefinedStyle then
        dxGaugeScaleStyleCollection.RegisterPredefinedStyles(AScaleClass);
    end
    else
      dxGaugeRaiseException(sdxGaugeScaleFactoryDublicateScaleClassName, [AScaleClass.GetScaleName]);
  end;
end;

procedure TdxGaugeScaleFactory.UnregisterScale(AScaleClass: TdxGaugeCustomScaleClass;
  AUnregisterPredefinedStyle: Boolean = True);

  function CanUnregisterType(AScaleClass: TdxGaugeCustomScaleClass): Boolean;
  var
    I: Integer;
    AClass: TdxGaugeCustomScaleClass;
  begin
    Result := True;
    for I := 0 to FScaleClasses.Count - 1 do
    begin
      AClass := TdxGaugeCustomScaleClass(FScaleClasses.Objects[I]);
      Result := AClass.GetScaleType <> AScaleClass.GetScaleType;
      if not Result then
        Break;
    end;
  end;

var
  AIndex: Integer;
begin
  AIndex := FScaleClasses.IndexOfObject(TObject(AScaleClass));
  if AIndex <> -1 then
  begin
    UnRegisterClass(AScaleClass);
    FScaleClasses.Delete(AIndex);
    if CanUnregisterType(AScaleClass) and AUnregisterPredefinedStyle then
      dxGaugeScaleStyleCollection.UnregisterPredefinedStyles(AScaleClass);
  end;
end;

function TdxGaugeScaleFactory.IsRegisteredScale(AScaleClass: TdxGaugeCustomScaleClass): Boolean;
begin
  Result := ScaleClasses.IndexOfObject(TObject(AScaleClass)) <> -1;
end;

procedure TdxGaugeScaleFactory.UnregisterScales;
begin
  while FScaleClasses.Count > 0 do
    UnregisterScale(TdxGaugeCustomScaleClass(FScaleClasses.Objects[0]));
end;

initialization

finalization
  FreeAndNil(dxgGaugeScaleTypeInfos);
  FreeAndNil(dxgGaugeScaleStyleCollection);
  FreeAndNil(dxgGaugeScaleFactory);

end.
