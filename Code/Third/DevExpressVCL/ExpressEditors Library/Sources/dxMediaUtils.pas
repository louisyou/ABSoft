unit dxMediaUtils;

{$I cxVer.inc}

interface

uses
  Windows, ActiveX, ComObj;

{$IFNDEF DELPHI14}
const
  VMRMode_Windowless = 2;
  EC_DEVICE_LOST = $1f;

  IID_ICaptureGraphBuilder2: TGUID = '{93E5A4E0-2D50-11d2-ABFA-00A0C9C6E38D}';
  IID_IGraphBuilder: TGUID = '{56A868A9-0AD4-11CE-B03A-0020AF0BA770}';
  IID_ICreateDevEnum: TGUID = '{29840822-5B84-11D0-BD3B-00A0C911CE86}';
  IID_IMediaControl: TGUID = '{56A868B1-0AD4-11CE-B03A-0020AF0BA770}';
  IID_IMediaEventEx: TGUID = '{56A868C0-0AD4-11CE-B03A-0020AF0BA770}';
  IID_IBaseFilter: TGUID = '{56A86895-0AD4-11CE-B03A-0020AF0BA770}';
  IID_ISampleGrabber: TGUID = '{6B652FFF-11FE-4FCE-92AD-0266B5D7C78F}';
  IID_IVMRFilterConfig: TGUID = '{9E5530C5-7034-48B4-BB46-0B8A6EFC8E36}';
  IID_IVMRWindowlessControl: TGUID = '{0EB1088C-4DCD-46F0-878F-39DAE86A51B7}';
  IID_IVideoWindow: TGUID = '{56A868B4-0AD4-11CE-B03A-0020AF0BA770}';

  CLSID_VideoInputDeviceCategory: TGUID = '{860BB310-5D01-11d0-BD3B-00A0C911CE86}';
  CLSID_SystemDeviceEnum: TGUID = '{62BE5D10-60EB-11d0-BD3B-00A0C911CE86}';
  CLSID_CaptureGraphBuilder2: TGUID = '{BF87B6E1-8C27-11d0-B3F0-00AA003761C5}';
  CLSID_FilterGraph: TGUID = '{E436EBB3-524F-11CE-9F53-0020AF0BA770}';
  CLSID_SampleGrabber: TGUID = '{C1F400A0-3F08-11d3-9F0B-006008039E37}';
  CLSID_NullRenderer: TGUID = '{C1F400A4-3F08-11D3-9F0B-006008039E37}';
  CLSID_VideoMixingRenderer: TGUID = '{B87BEB7B-8D29-423F-AE4D-6582C10175AC}';

  MEDIATYPE_Video: TGUID = '{73646976-0000-0010-8000-00AA00389B71}';
  MEDIASUBTYPE_RGB32: TGUID = '{E436EB7E-524F-11CE-9F53-0020AF0BA770}';
  MEDIASUBTYPE_AVI: TGUID = '{E436EB88-524F-11CE-9F53-0020AF0BA770}';
  PIN_CATEGORY_CAPTURE: TGUID = '{FB6C4281-0353-11D1-905F-0000C0CC16BA}';
  PIN_CATEGORY_PREVIEW: TGUID = '{FB6C4282-0353-11D1-905F-0000C0CC16BA}';  

type
  TAMMediaType = record
    majortype: TGUID;
    subtype: TGUID;
    bFixedSizeSamples: BOOL;
    bTemporalCompression: BOOL;
    lSampleSize: ULONG;
    formattype: TGUID;
    pUnk: IUnknown;
    cbFormat: ULONG;
    pbFormat: Pointer;
  end;

  TVideoInfoHeader = record
    rcSource: TRect;
    rcTarget: TRect;
    dwBitRate: DWORD;
    dwBitErrorRate: DWORD;
    AvgTimePerFrame: LONGLONG;
    bmiHeader: TBitmapInfoHeader;
  end;

  IMediaFilter = interface(IPersist)
  ['{56A86899-0AD4-11CE-B03A-0020AF0BA770}']
  end;

  IBaseFilter = interface(IMediaFilter)
  ['{56A86895-0AD4-11CE-B03A-0020AF0BA770}']
  end;

  IFilterGraph = interface(IUnknown)
  ['{56A8689F-0AD4-11CE-B03A-0020AF0BA770}']
    function AddFilter(pFilter: IBaseFilter; pName: PWideChar): HResult; stdcall;
  end;

  IGraphBuilder = interface(IFilterGraph)
  ['{56A868A9-0AD4-11CE-B03A-0020AF0BA770}']
  end;

  ICaptureGraphBuilder2 = interface(IUnknown)
  ['{93E5A4E0-2D50-11d2-ABFA-00A0C9C6E38D}']
    function SetFiltergraph(pfg: IGraphBuilder): HResult; stdcall;
    function GetFiltergraph(out ppfg: IGraphBuilder): HResult; stdcall;
    function SetOutputFileName(const pType: TGUID; lpstrFile: PWideChar; out ppf: IBaseFilter; out ppSink: IUnknown{IFileSinkFilter}): HResult; stdcall;
    function FindInterface(pCategory, pType: PGUID; pf: IBaseFilter; const riid: TGUID; out ppint): HResult; stdcall;
    function RenderStream(pCategory, pType: PGUID; pSource: IUnknown; pIntermediate, pSink: IBaseFilter): HResult; stdcall;
    // other metods
  end;

  ISampleGrabber = interface(IUnknown) // use IMediaSample instead
  ['{6B652FFF-11FE-4FCE-92AD-0266B5D7C78F}']
    function SetOneShot(OneShot: BOOL): HResult; stdcall;
    function SetMediaType(const pType: TAMMediaType): HResult; stdcall;
    function GetConnectedMediaType(out pType: TAMMediaType): HResult; stdcall;
    function SetBufferSamples(BufferThem: BOOL): HResult; stdcall;
    function GetCurrentBuffer(var pBufferSize: Integer; pBuffer: Pointer): HResult; stdcall;
    // other metods
  end;

  IMediaControl = interface(IDispatch)
  ['{56A868B1-0AD4-11CE-B03A-0020AF0BA770}']
    function Run: HResult; stdcall;
    function Pause: HResult; stdcall;
    function Stop: HResult; stdcall;
    // other metods
  end;

  IMediaEvent = interface(IDispatch)
  ['{56A868B6-0AD4-11CE-B03A-0020AF0BA770}']
    function GetEventHandle(out hEvent: Longint): HResult; stdcall;
    function GetEvent(out lEventCode, lParam1, lParam2: Longint; msTimeout: DWORD): HResult; stdcall;
    function WaitForCompletion(msTimeout: DWORD; out pEvCode: Longint): HResult; stdcall;
    function CancelDefaultHandling(lEvCode: Longint): HResult; stdcall;
    function RestoreDefaultHandling(lEvCode: Longint): HResult; stdcall;
    function FreeEventParams(lEvCode, lParam1, lParam2: Longint): HResult; stdcall;
  end;

  IMediaEventEx = interface(IMediaEvent)
  ['{56A868C0-0AD4-11CE-B03A-0020AF0BA770}']
    function SetNotifyWindow(hwnd: HWND; lMsg, lInstanceData: Longint): HResult; stdcall;
    // other metods
  end;

  ICreateDevEnum = interface(IUnknown)
  ['{29840822-5B84-11D0-BD3B-00A0C911CE86}']
    function CreateClassEnumerator(const clsidDeviceClass: TGUID; out ppEnumMoniker: IEnumMoniker; dwFlags: DWORD): HRESULT; stdcall;
  end;

  IVideoWindow = interface(IDispatch)
  ['{56A868B4-0AD4-11CE-B03A-0020AF0BA770}']
  end;
  
  IVMRWindowlessControl = interface(IUnknown)
  ['{0EB1088C-4DCD-46F0-878F-39DAE86A51B7}']
  end;

  IVMRFilterConfig = interface(IUnknown)
  ['{9E5530C5-7034-48B4-BB46-0B8A6EFC8E36}']
  end;
{$ENDIF}

function dxIsCameraAvailable: Boolean;

implementation

uses
  dxCameraControl;

type
  TdxCameraManagerAccess = class(TdxCameraManager);


function dxIsCameraAvailable: Boolean;
begin
  Result := TdxCameraManagerAccess(dxCameraManager).GetDeviceCount > 0;
end;

end.
