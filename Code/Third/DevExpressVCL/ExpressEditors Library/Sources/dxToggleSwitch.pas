{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressEditors                                           }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSEDITORS AND ALL                }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxToggleSwitch;

{$I cxVer.inc}

interface

uses
  Windows, SysUtils, Classes, Controls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxClasses, cxContainer, cxEditConsts, cxEdit, cxCheckBox;

type
  TdxCustomToggleSwitchViewData = class;
  TdxCustomToggleSwitchViewInfo = class;

  { TdxToggleSwitchThumbFadingHelper }

  TdxToggleSwitchThumbFadingHelper = class(TcxCustomCheckControlFadingHelper)
  private
    function GetToggleSwitchViewInfo: TdxCustomToggleSwitchViewInfo;
  protected
    procedure GetFadingImages(out AFadeOutImage, AFadeInImage: TcxBitmap); override;
  public
    procedure Invalidate; override;
  end;

  { TcxCustomToggleSwitchProperties }

  TdxCustomToggleSwitchProperties = class(TcxCustomCheckBoxProperties)
  private
    FSmoothToggle: Boolean;

    procedure SetSmoothToggle(AValue: Boolean);
  protected
    class function GetViewDataClass: TcxCustomEditViewDataClass; override;

    property SmoothToggle: Boolean read FSmoothToggle write SetSmoothToggle default True;
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
    class function GetViewInfoClass: TcxContainerViewInfoClass; override;

    property Alignment default taRightJustify;
  end;

  { TcxCustomToggleSwitchViewData }

  TdxCustomToggleSwitchViewData = class(TcxCustomCheckBoxViewData)
  protected
    function CalculateFocusRect(AViewInfo: TdxCustomToggleSwitchViewInfo): TRect; virtual;
    function CalculateThumbBounds(AViewInfo: TdxCustomToggleSwitchViewInfo): TRect; virtual;
    function CalculateToggleSwitchSize(AViewInfo: TdxCustomToggleSwitchViewInfo): TSize; virtual;
    function CalculateCheckBoxState(AViewInfo: TcxCustomCheckBoxViewInfo; const P: TPoint; AButton: TcxMouseButton;
      AShift: TShiftState): TcxEditCheckState; override;
    function InternalGetEditConstantPartSize(ACanvas: TcxCanvas; AIsInplace: Boolean;
      AEditSizeProperties: TcxEditSizeProperties; var MinContentSize: TSize;
      AViewInfo: TcxCustomEditViewInfo): TSize; override;
    function GetTextGap: Integer; override;
  public
    procedure Calculate(ACanvas: TcxCanvas; const ABounds: TRect;
      const P: TPoint; Button: TcxMouseButton; Shift: TShiftState;
      AViewInfo: TcxCustomEditViewInfo; AIsMouseEvent: Boolean); override;
    function GetBorderExtent: TRect; override;
  end;

  { TdxToggleSwitchViewInfo }

  TdxCustomToggleSwitchViewInfo = class(TcxCustomCheckBoxViewInfo)
  private
    FThumbBounds: TRect;
    FThumbCapture: Boolean;
    FThumbDelta: Integer;
    FToggleSwitchBounds: TRect;

    function GetThumbDelta: Integer;
    function GetThumbMaxDelta: Integer;
    function GetThumbState: TcxButtonState;
    function GetThumbStepAnimation: Integer;
  protected
    procedure DrawThumb(ACanvas: TcxCanvas; AClientRect: TRect; AState: TcxButtonState);
    procedure InternalPaint(ACanvas: TcxCanvas); override;
    function GetCheckControlFadingHelperClass: TcxCheckControlFadingHelperClass; override;

    property ThumbState: TcxButtonState read GetThumbState;
  public
    procedure Offset(DX: Integer; DY: Integer); override;
    property ThumbBounds: TRect read FThumbBounds write FThumbBounds;
    property ThumbCapture: Boolean read FThumbCapture write FThumbCapture;
    property ThumbDelta: Integer read GetThumbDelta write FThumbDelta;
    property ThumbMaxDelta: Integer read GetThumbMaxDelta;
    property ThumbStepAnimation: Integer read GetThumbStepAnimation;
    property ToggleSwitchBounds: TRect read FToggleSwitchBounds write FToggleSwitchBounds;
  end;

  { TdxCustomToggleSwitch }

  TdxCustomToggleSwitch = class(TcxCustomCheckBox)
  private
    FPrevMousePos: Integer;
    FThumbDragged: Boolean;
    FTimer: TcxTimer;

    function GetActiveProperties: TdxCustomToggleSwitchProperties;
    function GetProperties: TdxCustomToggleSwitchProperties;
    function GetViewInfo: TdxCustomToggleSwitchViewInfo;
    procedure SetProperties(Value: TdxCustomToggleSwitchProperties);
  protected
    procedure DoProcessEventsOnViewInfoChanging; override;
    procedure Initialize; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X: Integer; Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X: Integer; Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X: Integer; Y: Integer); override;

    procedure StartAnimation;
    procedure StopAnimation;
    procedure TimerHandler(Sender: TObject);
    function GetShadowBounds: TRect; override;

    property ActiveProperties: TdxCustomToggleSwitchProperties read GetActiveProperties;
    property Properties: TdxCustomToggleSwitchProperties read GetProperties write SetProperties;
    property ViewInfo: TdxCustomToggleSwitchViewInfo read GetViewInfo;
  public
    destructor Destroy; override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  end;


  { TdxToggleSwitchProperties }

  TdxToggleSwitchProperties = class(TdxCustomToggleSwitchProperties)
  published
    property Alignment;
    property AssignedValues;
    property ClearKey;
    property DisplayChecked;
    property DisplayUnchecked;
    property DisplayGrayed;
    property FullFocusRect;
    property ImmediatePost;
    property MultiLine;
    property ReadOnly;
    property ShowEndEllipsis;
    property UseAlignmentWhenInplace;
    property ValidationOptions;
    property ValueChecked;
    property ValueGrayed;
    property ValueUnchecked;
    property OnChange;
    property OnEditValueChanged;
    property OnValidate;
  end;

  { TdxToggleSwitch }

  TdxToggleSwitch = class(TdxCustomToggleSwitch)
  private
    function GetActiveProperties: TdxToggleSwitchProperties;
    function GetProperties: TdxToggleSwitchProperties;
    procedure SetProperties(Value: TdxToggleSwitchProperties);
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
    property ActiveProperties: TdxToggleSwitchProperties read GetActiveProperties;
  published
    property Action;
    property Anchors;
    property AutoSize;
    property Caption;
    property Checked;
    property Constraints;
    property Enabled;
    property ParentBackground;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property Properties: TdxToggleSwitchProperties read GetProperties write SetProperties;
    property ShowHint;
    property Style;
    property StyleDisabled;
    property StyleFocused;
    property StyleHot;
    property TabOrder;
    property TabStop;
    property Transparent;
    property Visible;
    property OnClick;
    property OnContextPopup;
    property OnEditing;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
  end;

implementation

uses
  Types, Variants, Math,
  dxCore, cxGeometry;

const
  dxToggleSwitchDefaultHeight: Integer = 21;
  dxToggleSwitchDefaultWidth: Integer = 70;
  dxToggleSwitchInplaceHeight: Integer = 15;
  dxToggleSwitchTextGap: Integer = 7;

{ TdxToggleSwitchThumbFadingHelper }

procedure TdxToggleSwitchThumbFadingHelper.GetFadingImages(out AFadeOutImage, AFadeInImage: TcxBitmap);

  function PrepareImage(AState: TcxButtonState): TcxBitmap32;
  begin
    Result := TcxBitmap32.CreateSize(GetToggleSwitchViewInfo.ThumbBounds, True);
    GetToggleSwitchViewInfo.DrawThumb(Result.cxCanvas, Result.ClientRect, AState);
  end;

begin
  AFadeInImage := PrepareImage(cxbsHot);
  AFadeOutImage := PrepareImage(cxbsNormal);
end;

procedure TdxToggleSwitchThumbFadingHelper.Invalidate;
begin
  Invalidate(GetToggleSwitchViewInfo.ThumbBounds, False);
end;

function TdxToggleSwitchThumbFadingHelper.GetToggleSwitchViewInfo: TdxCustomToggleSwitchViewInfo;
begin
  Result := TdxCustomToggleSwitchViewInfo(ViewInfo);
end;

{ TdxCustomToggleSwitch }

destructor TdxCustomToggleSwitch.Destroy;
begin
  if not IsInplace then
    StopAnimation;
  inherited;
end;

procedure TdxCustomToggleSwitch.DoProcessEventsOnViewInfoChanging;

  function NeedToggle: Boolean;
  begin
    Result := (FThumbDragged and (ViewInfo.ThumbDelta > Round(ViewInfo.ThumbMaxDelta / 2))) or
      (not FThumbDragged and (ViewInfo.CheckBoxState = ecsHot));
  end;

begin
  if (ViewInfo.CheckBoxLastState = ecsPressed) and (ViewInfo.CheckBoxState <> ecsPressed) and not ActiveProperties.ReadOnly then
  begin
    if not IsInplace and ActiveProperties.SmoothToggle then
    begin
      if (EditValue = Null) then
        Toggle
      else
        if NeedToggle then
        begin
          ViewInfo.ThumbDelta := ViewInfo.ThumbMaxDelta - ViewInfo.ThumbDelta;
          Toggle;
        end;
      FThumbDragged := False;
      StartAnimation;
    end
    else
      if ViewInfo.CheckBoxState = ecsHot then
        Toggle;
  end;
end;

procedure TdxCustomToggleSwitch.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not IsInplace and (Button = mbLeft) then
    StopAnimation;
  inherited MouseDown(Button, Shift, X, Y);
  ViewInfo.ThumbCapture := (Button = mbLeft) and cxRectPtIn(ViewInfo.ThumbBounds, X, Y);
  if ViewInfo.ThumbCapture then
    FPrevMousePos := X;
end;

procedure TdxCustomToggleSwitch.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  if not IsInplace and ActiveProperties.SmoothToggle and ViewInfo.ThumbCapture and (Abs(X - FPrevMousePos) > 0) then
  begin
    if State = cbsUnchecked then
      ViewInfo.ThumbDelta := ViewInfo.ThumbDelta + (X - FPrevMousePos)
    else
      ViewInfo.ThumbDelta := ViewInfo.ThumbDelta - (X - FPrevMousePos);

    Invalidate;
    FPrevMousePos := X;
    FThumbDragged := True;
  end;
  inherited MouseMove(Shift, X, Y);
end;

procedure TdxCustomToggleSwitch.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ViewInfo.ThumbCapture := False;
  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TdxCustomToggleSwitch.StartAnimation;
begin
  FTimer := TcxTimer.Create(nil);
  FTimer.Interval := 10;
  FTimer.OnTimer := TimerHandler;
end;

procedure TdxCustomToggleSwitch.StopAnimation;
begin
  FreeAndNil(FTimer);
  ShortRefreshContainer(False);
end;

procedure TdxCustomToggleSwitch.TimerHandler(Sender: TObject);
begin
  if ViewInfo.ThumbDelta < ViewInfo.ThumbStepAnimation then
  begin
    ViewInfo.ThumbDelta := 0;
    StopAnimation;
  end
  else
  begin
    ViewInfo.ThumbDelta := ViewInfo.ThumbDelta - ViewInfo.ThumbStepAnimation;
    ShortRefreshContainer(False);
  end;
end;

function TdxCustomToggleSwitch.GetActiveProperties: TdxCustomToggleSwitchProperties;
begin
  Result := TdxCustomToggleSwitchProperties(InternalGetActiveProperties);
end;

function TdxCustomToggleSwitch.GetProperties: TdxCustomToggleSwitchProperties;
begin
  Result := TdxCustomToggleSwitchProperties(FProperties);
end;

class function TdxCustomToggleSwitch.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TdxCustomToggleSwitchProperties;
end;

function TdxCustomToggleSwitch.GetShadowBounds: TRect;
begin
  Result := GetControlRect(Self);
end;

function TdxCustomToggleSwitch.GetViewInfo: TdxCustomToggleSwitchViewInfo;
begin
  Result := TdxCustomToggleSwitchViewInfo(FViewInfo);
end;

procedure TdxCustomToggleSwitch.Initialize;
begin
  inherited;
  Width := 161;
end;

procedure TdxCustomToggleSwitch.SetProperties(Value: TdxCustomToggleSwitchProperties);
begin
  FProperties.Assign(Value);
end;

{ TcxCustomToggleSwitchProperties }

constructor TdxCustomToggleSwitchProperties.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  FSmoothToggle := True;
  FAlignment.Horz := taRightJustify;
end;

class function TdxCustomToggleSwitchProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TdxToggleSwitch;
end;

class function TdxCustomToggleSwitchProperties.GetViewInfoClass: TcxContainerViewInfoClass;
begin
  Result := TdxCustomToggleSwitchViewInfo;
end;

class function TdxCustomToggleSwitchProperties.GetViewDataClass: TcxCustomEditViewDataClass;
begin
  Result := TdxCustomToggleSwitchViewData;
end;

procedure TdxCustomToggleSwitchProperties.SetSmoothToggle(AValue: Boolean);
begin
  if AValue <> FSmoothToggle then
  begin
    FSmoothToggle:= AValue;
    Changed;
  end;
end;

{ TdxCustomToggleSwitchViewData }

procedure TdxCustomToggleSwitchViewData.Calculate(ACanvas: TcxCanvas;
  const ABounds: TRect; const P: TPoint; Button: TcxMouseButton;
  Shift: TShiftState; AViewInfo: TcxCustomEditViewInfo;
  AIsMouseEvent: Boolean);
var
  AToggleSwitchViewInfo: TdxCustomToggleSwitchViewInfo;
begin
  inherited;
  AToggleSwitchViewInfo := TdxCustomToggleSwitchViewInfo(AViewInfo);
  AToggleSwitchViewInfo.CheckBoxSize := CalculateToggleSwitchSize(AToggleSwitchViewInfo);
  AToggleSwitchViewInfo.CheckBoxRect := CalculateCheckBoxRect(AToggleSwitchViewInfo);
  AToggleSwitchViewInfo.TextRect := CalculateTextRect(AToggleSwitchViewInfo);
  AToggleSwitchViewInfo.ToggleSwitchBounds := AToggleSwitchViewInfo.CheckBoxRect;
  AToggleSwitchViewInfo.ThumbBounds := CalculateThumbBounds(AToggleSwitchViewInfo);
  AToggleSwitchViewInfo.FocusRect := CalculateFocusRect(AToggleSwitchViewInfo);
end;

function TdxCustomToggleSwitchViewData.CalculateCheckBoxState(AViewInfo: TcxCustomCheckBoxViewInfo; const P: TPoint;
  AButton: TcxMouseButton; AShift: TShiftState): TcxEditCheckState;
begin
  Result := inherited CalculateCheckBoxState(AViewInfo, P, AButton, AShift);
  if TdxCustomToggleSwitchViewInfo(AViewInfo).ThumbCapture then
    Result := ecsPressed;
end;

function TdxCustomToggleSwitchViewData.CalculateFocusRect(AViewInfo: TdxCustomToggleSwitchViewInfo): TRect;
begin
  if Focused and not IsInplace and (AViewInfo.Alignment <> taCenter) and (Length(AViewInfo.Text) <> 0) then
  begin
    Result := AViewInfo.TextRect;
    if not Properties.FullFocusRect then
    begin
      cxScreenCanvas.Font := AViewInfo.Font;
      cxScreenCanvas.TextExtent(AViewInfo.Text, Result, AViewInfo.DrawTextFlags);
      cxScreenCanvas.Dormant;
    end;
    InflateRect(Result, 1, 1);
    Inc(Result.Bottom);
  end
  else
    Result := cxEmptyRect;
end;

function TdxCustomToggleSwitchViewData.CalculateThumbBounds(AViewInfo: TdxCustomToggleSwitchViewInfo): TRect;
var
  ASize: TSize;
begin
  ASize.cy := AViewInfo.CheckBoxSize.cy;
  ASize.cx := Round(AViewInfo.CheckBoxSize.cx * AViewInfo.Painter.GetToggleSwitchThumbPercentsWidth / 100);
  Result := cxRectCenter(AViewInfo.ToggleSwitchBounds, ASize);
  case AViewInfo.State of
    cbsUnchecked:
      Result := cxRectSetLeft(Result, AViewInfo.ToggleSwitchBounds.Left + AViewInfo.ThumbDelta);
    cbsChecked:
      Result := cxRectSetLeft(Result, AViewInfo.ToggleSwitchBounds.Right - ASize.cx - AViewInfo.ThumbDelta);
    else
      Result := cxRectSetLeft(Result, AViewInfo.ToggleSwitchBounds.Left + (cxRectWidth(AViewInfo.ToggleSwitchBounds) - ASize.cx) div 2);
  end;
end;

function TdxCustomToggleSwitchViewData.CalculateToggleSwitchSize(AViewInfo: TdxCustomToggleSwitchViewInfo): TSize;
var
  AWidth, AHeight: Integer;
  AProprtion: Single;
begin
  AWidth := dxToggleSwitchDefaultWidth;
  AHeight := dxToggleSwitchDefaultHeight;
  if IsInplace then
  begin
    AWidth := Min(cxRectWidth(AViewInfo.ClientRect), AWidth);
    AHeight := Min(cxRectHeight(AViewInfo.ClientRect), AHeight);
    AProprtion := Min(AWidth / dxToggleSwitchDefaultWidth, AHeight / dxToggleSwitchDefaultHeight);
    AWidth := Round(dxToggleSwitchDefaultWidth * AProprtion);
    AHeight := Round(dxToggleSwitchDefaultHeight * AProprtion);
  end;
  Result := cxSize(AWidth, AHeight);
end;

function TdxCustomToggleSwitchViewData.GetBorderExtent: TRect;
begin
  if IsInplace then
    Result := inherited GetBorderExtent
  else
    Result := cxNullRect;
end;

function TdxCustomToggleSwitchViewData.GetTextGap: Integer;
begin
  Result := dxToggleSwitchTextGap;
end;

function TdxCustomToggleSwitchViewData.InternalGetEditConstantPartSize(ACanvas: TcxCanvas; AIsInplace: Boolean;
  AEditSizeProperties: TcxEditSizeProperties; var MinContentSize: TSize; AViewInfo: TcxCustomEditViewInfo): TSize;

  function IsCaptionVisible: Boolean;
  begin
    Result := (TcxCustomCheckBoxViewInfo(AViewInfo).Alignment <> taCenter) and not IsInplace and (TcxCustomCheckBoxViewInfo(AViewInfo).Text <> '');
  end;

var
  AExtend: TRect;
  AToggleSwitchViewInfo: TdxCustomToggleSwitchViewInfo;
  ASize1, ASize2: TSize;
  AText: string;
  ARect: TRect;
begin
  MinContentSize := cxNullSize;
  AToggleSwitchViewInfo := TdxCustomToggleSwitchViewInfo(AViewInfo);
  if AIsInplace then
    ASize1 := cxSize(dxToggleSwitchDefaultWidth, dxToggleSwitchInplaceHeight)
  else
    ASize1 := AToggleSwitchViewInfo.CheckBoxSize;

  if cxRectIsEmpty(AViewInfo.Bounds) or cxRectIsEmpty(AViewInfo.ClientRect) then
  begin
    AExtend := GetClientExtent(ACanvas, AViewInfo);
    Result := cxSize(AExtend.Left + AExtend.Right, AExtend.Top + AExtend.Bottom);
  end
  else
  begin
    Result.cx := cxRectWidth(AViewInfo.Bounds) - cxRectWidth(AViewInfo.ClientRect);
    Result.cy := cxRectHeight(AViewInfo.Bounds) - cxRectHeight(AViewInfo.ClientRect);
  end;
  if IsCaptionVisible then
  begin
    AText := RemoveAccelChars(AToggleSwitchViewInfo.Text);

    if not Enabled and not IsNativeStyle(Style.LookAndFeel) then
      Dec(AEditSizeProperties.Width);

    ARect := cxRect(0, 0, AEditSizeProperties.Width - (Result.cx + ASize1.cx + GetTextGap), 1);
    ACanvas.Font := Style.GetVisibleFont;
    cxDrawText(ACanvas.Handle, AText, ARect, DT_CALCRECT or cxFlagsToDTFlags(GetCalculateTextFlags));
    ASize2 := cxSize(ARect);
  end
  else
    ASize2 := cxNullSize;

  if IsCaptionVisible then
    Result.cx := Result.cx + ASize1.cx + GetTextGap + ASize2.cx
  else
    Result.cx := Result.cx + ASize1.cx;

  Result.cy := Result.cy + Max(ASize1.cy, ASize2.cy);
end;

{ TdxCustomToggleSwitchViewInfo }

procedure TdxCustomToggleSwitchViewInfo.DrawThumb(ACanvas: TcxCanvas; AClientRect: TRect; AState: TcxButtonState);
begin
  Painter.DrawToggleSwitchThumb(ACanvas, AClientRect, AState);
end;

procedure TdxCustomToggleSwitchViewInfo.InternalPaint(ACanvas: TcxCanvas);

  function GetInternalState: TcxCheckBoxState;
  begin
    Result := State;
    if ThumbDelta <> 0 then
      Result := cbsGrayed;
  end;

begin
  DrawEditBackground(ACanvas, Bounds, cxNullRect, False);
  if Alignment <> taCenter then
    DrawCheckBoxText(ACanvas, Text, Font, TextColor, TextRect, DrawTextFlags, IsTextEnabled);
  Painter.DrawToggleSwitch(ACanvas, ToggleSwitchBounds, ThumbState, ThumbBounds);
  if not FadingHelper.DrawImage(ACanvas.Handle, ThumbBounds) then
    DrawThumb(ACanvas, ThumbBounds, ThumbState);
  if not IsRectEmpty(FocusRect) then
    ACanvas.DrawFocusRect(FocusRect);
end;

procedure TdxCustomToggleSwitchViewInfo.Offset(DX, DY: Integer);
begin
  inherited;
  OffsetRect(FToggleSwitchBounds, DX, DY);
  OffsetRect(FThumbBounds, DX, DY);
end;

function TdxCustomToggleSwitchViewInfo.GetCheckControlFadingHelperClass: TcxCheckControlFadingHelperClass;
begin
  Result := TdxToggleSwitchThumbFadingHelper;
end;

function TdxCustomToggleSwitchViewInfo.GetThumbDelta: Integer;
begin
  Result := Min(Max(0, FThumbDelta), ThumbMaxDelta);
end;

function TdxCustomToggleSwitchViewInfo.GetThumbMaxDelta: Integer;
begin
  Result := cxRectWidth(ToggleSwitchBounds) - cxRectWidth(ThumbBounds);
end;

function TdxCustomToggleSwitchViewInfo.GetThumbState: TcxButtonState;
begin
  case CheckBoxState of
    ecsHot: Result := cxbsHot;
    ecsPressed: Result := cxbsPressed;
    ecsDisabled: Result := cxbsDisabled;
  else
    Result := cxbsNormal;
  end;
end;

function TdxCustomToggleSwitchViewInfo.GetThumbStepAnimation: Integer;
begin
  Result := ThumbMaxDelta div 8;
end;

{ TdxToggleSwitch }

class function TdxToggleSwitch.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TdxToggleSwitchProperties;
end;

function TdxToggleSwitch.GetActiveProperties: TdxToggleSwitchProperties;
begin
  Result := TdxToggleSwitchProperties(InternalGetActiveProperties);
end;

function TdxToggleSwitch.GetProperties: TdxToggleSwitchProperties;
begin
  Result := TdxToggleSwitchProperties(FProperties);
end;

procedure TdxToggleSwitch.SetProperties(Value: TdxToggleSwitchProperties);
begin
  FProperties.Assign(Value);
end;

initialization
  GetRegisteredEditProperties.Register(TdxToggleSwitchProperties, sdxSEditRepositoryToggleSwitchItem);

end.
