{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressEditors                                           }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSEDITORS AND ALL                }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxEditConsts;

{$I cxVer.inc}

interface

resourcestring
  cxSEditButtonCancel = '取消';
  cxSEditButtonOK = '确定';
  cxSEditDateConvertError = '不能转换为日期';
  cxSEditInvalidRepositoryItem = '资源库项目不合格';
  cxSEditNumericValueConvertError = '不能转换为数值';
  cxSEditPopupCircularReferencingError = '不允许循环引用';
  cxSEditPostError = '错误发生在提交修改值的时候';
  cxSEditTimeConvertError = '不能够转换为时间';
  cxSEditValidateErrorText = '非法输入值，请按ESC键放弃修改';
  cxSEditValueOutOfBounds = '数值越界';

  // TODO
  cxSEditCheckBoxChecked             = '是';
  cxSEditCheckBoxGrayed              = '';
  cxSEditCheckBoxUnchecked           = '否';
  cxSRadioGroupDefaultCaption        = '';

  cxSTextTrue                        = '是';
  cxSTextFalse                       = '否';

  // blob
  cxSBlobButtonOK                    = '确定(&O)';
  cxSBlobButtonCancel                = '取消(&C)';
  cxSBlobButtonClose                 = '关闭(&C)';
  cxSBlobMemo                        = '(内容)';
  cxSBlobMemoEmpty                   = '(内容)';
  cxSBlobPicture                     = '(图片)';
  cxSBlobPictureEmpty                = '(图片)';

  // popup menu items
  cxSMenuItemCaptionCut              = '剪切(&T)';
  cxSMenuItemCaptionCopy             = '复制(&C)';
  cxSMenuItemCaptionPaste            = '粘贴(&P)';
  cxSMenuItemCaptionDelete           = '删除(&D)';
  cxSMenuItemCaptionLoad             = '读取(&L)...';
  cxSMenuItemCaptionSave             = '另存为(&A)...';
  cxSMenuItemCaptionAssignFromWebCam = 'Assign From Ca&mera...';

  // date
  cxSDatePopupClear                  = '清除';
  cxSDatePopupNow                    = '现在';
  cxSDatePopupOK                     = '确定';
  cxSDatePopupToday                  = '今天';
  cxSDateError                       = '无效日期';

  // calculator
  scxSCalcError                      = '错误';

  // HyperLink
  scxSHyperLinkPrefix                = 'http://';
  scxSHyperLinkDoubleSlash           = '//';

  // navigator
  cxNavigatorHint_First = '第一条记录';
  cxNavigatorHint_Prior = '上一条记录';
  cxNavigatorHint_PriorPage = '上页';
  cxNavigatorHint_Next = '下一条记录';
  cxNavigatorHint_NextPage = '下页';
  cxNavigatorHint_Last = '最后一条记录';
  cxNavigatorHint_Insert = '插入记录';
  cxNavigatorHint_Append = '添加记录';
  cxNavigatorHint_Delete = '删除记录';
  cxNavigatorHint_Edit = '修改记录';
  cxNavigatorHint_Post = '提交修改';
  cxNavigatorHint_Cancel = '取消修改';
  cxNavigatorHint_Refresh = '刷新数据';
  cxNavigatorHint_SaveBookmark = '保存书签';
  cxNavigatorHint_GotoBookmark = '转到书签';
  cxNavigatorHint_Filter = '数据过滤';
  cxNavigator_DeleteRecordQuestion = '删除记录？';
  cxNavigatorInfoPanelDefaultDisplayMask = '第[RecordIndex]条，共[RecordCount]条';

  // BreadcrumbEdit
  sdxBreadcrumbEditInvalidPath = '不能找到"%s".检查拼写后再试。';
  sdxBreadcrumbEditInvalidStreamVersion = 'Invalid data version: %d';

  // edit repository
  scxSEditRepositoryBlobItem         = 'BlobEdit|代表 BLOB 编辑器';
  scxSEditRepositoryButtonItem       = 'ButtonEdit|代表嵌入式按钮编辑控件';
  scxSEditRepositoryCalcItem         = 'CalcEdit|代表一个编辑控件和一个下拉列表计算器窗口';
  scxSEditRepositoryCheckBoxItem     = 'CheckBox|代表一个复选框控件';
  scxSEditRepositoryComboBoxItem     = 'ComboBox|代表组合框编辑器';
  scxSEditRepositoryCurrencyItem     = 'CurrencyEdit|代表一个编辑器启动编辑货币数据';
  scxSEditRepositoryDateItem         = 'DateEdit|代表一个编辑控件具有一个下拉日历';
  scxSEditRepositoryHyperLinkItem    = 'HyperLink|岱庙一个具有超链接功能的文本编辑器';
  scxSEditRepositoryImageComboBoxItem = 'ImageComboBox|代表列表中的图像和文本字符串下拉列表窗口内的显示编辑器';
  scxSEditRepositoryImageItem        = 'Image|代表图像编辑器';
  scxSEditRepositoryLookupComboBoxItem = 'LookupComboBox|代表查找组合框控件';
  scxSEditRepositoryMaskItem         = 'MaskEdit|代表一个通用的掩码编辑控件。';
  scxSEditRepositoryMemoItem         = 'Memo|代表允许编辑备注数据编辑控件';
  scxSEditRepositoryMRUItem          = 'MRUEdit|代表下拉列表窗口中显示最近使用的项目 (MRU) 列表中的文本编辑器';
  scxSEditRepositoryPopupItem        = 'PopupEdit|代表一个带下拉列表的编辑控件';
  scxSEditRepositorySpinItem         = 'SpinEdit|代表一个数值调节编辑器';
  scxSEditRepositoryRadioGroupItem   = 'RadioGroup|代表一组单选按钮';
  scxSEditRepositoryTextItem         = 'TextEdit|代表一个单独的一行文本编辑器';
  scxSEditRepositoryTimeItem         = 'TimeEdit|代表时间值显示编辑器';
  sdxSEditRepositoryToggleSwitchItem = 'ToggleSwitch|Represents a toggle switch control that allows selecting an option';

  scxRegExprLine = '行';
  scxRegExprChar = '字符';
  scxRegExprNotAssignedSourceStream = '未分配的源数据流';
  scxRegExprEmptySourceStream = '源数据流为空';
  scxRegExprCantUsePlusQuantifier = '''+''号不能应用在这里';
  scxRegExprCantUseStarQuantifier = '''*''号不能应用在这里';
  scxRegExprCantCreateEmptyAlt = '另一种不应该空';
  scxRegExprCantCreateEmptyBlock = '此块应该为空';
  scxRegExprIllegalSymbol = '非法''%s''';
  scxRegExprIllegalQuantifier = '非法量词 ''%s''';
  scxRegExprNotSupportQuantifier = '不支持的参数量词';
  scxRegExprIllegalIntegerValue = '非法的整形数值';
  scxRegExprTooBigReferenceNumber = '引用数太大';
  scxRegExprCantCreateEmptyEnum = '不能创建空的枚举';
  scxRegExprSubrangeOrder = '子范围的起始字符必须小于结束字符';
  scxRegExprHexNumberExpected0 = '需要十六进制数';
  scxRegExprHexNumberExpected = '输入的 ''%s'' 不是十六进制的数';
  scxRegExprMissing = '丢失 ''%s''';
  scxRegExprUnnecessary = '不需要 ''%s''';
  scxRegExprIncorrectSpace = '''\''后不允许空格';
  scxRegExprNotCompiled = '未编译的正则表达式';
  scxRegExprIncorrectParameterQuantifier = '不正确的参数量词';
  scxRegExprCantUseParameterQuantifier = '参数量词不能应用在这里';
  
  scxMaskEditRegExprError = '正则表达式错误:';
  scxMaskEditInvalidEditValue = '编辑值非法';
  scxMaskEditNoMask = '无';
  scxMaskEditIllegalFileFormat = '文件格式非法';
  scxMaskEditEmptyMaskCollectionFile = '掩码格式文件为空';
  scxMaskEditMaskCollectionFiles = '掩码格式文件';
  cxSSpinEditInvalidNumericValue = '数值无效';

  // AlertWindow
  sdxAlertWindowNavigationPanelDefaultDisplayMask = '第[MessageIndex]条,共[MessageCount]条]';
  sdxAlertWindowPreviousMessage = '上一条消息';
  sdxAlertWindowNextMessage = '下一条消息';
  sdxAlertWindowPin = '固定';
  sdxAlertWindowClose = '关闭';
  sdxAlertWindowDropdown = '显示下拉菜单';

  // dxColorGallery
  sdxColorGalleryThemeColors = '主题颜色';
  sdxColorGalleryStandardColors = '标准颜色';
  // ColorDialog
  sdxColorDialogAddToCustomColors = '&Add to Custom Colors';
  sdxColorDialogApply = '确定';
  sdxColorDialogCancel = '取消';
  sdxColorDialogDefineCustomColor = '&Define Custom Colors >>';
  sdxColorDialogBasicColors = 'Basic Colors';
  sdxColorDialogCaption = 'Color Editor';
  sdxColorDialogCustomColors = 'Custom Colors';

  // ColorPicker
  sdxColorPickerAlphaLabel = 'A:';
  sdxColorPickerBlueLabel = 'B:';
  sdxColorPickerGreenLabel = 'G:';
  sdxColorPickerHexCodeLabel = '#';
  sdxColorPickerHueLabel = 'H:';
  sdxColorPickerLightnessLabel = 'L:';
  sdxColorPickerRedLabel = 'R:';
  sdxColorPickerSaturationLabel = 'S:';

  // ShellBrowser
  scxShellBrowserDlgCaption = 'Browse for Folder';
  scxShellBrowserDlgCurrentFolderCaption = 'Current Folder';

  // CameraControl
  sdxCameraDialogAssign = '&Assign';
  sdxCameraDialogCancel = '&Cancel';
  sdxCameraDialogPause = '&Pause';
  sdxCameraDialogPlay = '&Play';
  sdxCameraDialogCaption = 'Camera Preview';
  sdxCameraInactive = 'Inactive';
  sdxCameraRunning = 'Running';
  sdxCameraPaused = 'Paused';
  sdxCameraNotDetected = 'No camera detected';
  sdxCameraInitializing = 'Initializing...';
  sdxCameraIsBusy = 'Camera is inaccessible.'#13#10'Try closing other programs that might be using your camera';

implementation

uses
  dxCore;

procedure AddEditorsResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('cxSEditButtonCancel', @cxSEditButtonCancel);
  InternalAdd('cxSEditButtonOK', @cxSEditButtonOK);
  InternalAdd('cxSEditDateConvertError', @cxSEditDateConvertError);
  InternalAdd('cxSEditInvalidRepositoryItem', @cxSEditInvalidRepositoryItem);
  InternalAdd('cxSEditNumericValueConvertError', @cxSEditNumericValueConvertError);
  InternalAdd('cxSEditPopupCircularReferencingError', @cxSEditPopupCircularReferencingError);
  InternalAdd('cxSEditPostError', @cxSEditPostError);
  InternalAdd('cxSEditTimeConvertError', @cxSEditTimeConvertError);
  InternalAdd('cxSEditValidateErrorText', @cxSEditValidateErrorText);
  InternalAdd('cxSEditValueOutOfBounds', @cxSEditValueOutOfBounds);
  InternalAdd('cxSEditCheckBoxChecked', @cxSEditCheckBoxChecked);
  InternalAdd('cxSEditCheckBoxGrayed', @cxSEditCheckBoxGrayed);
  InternalAdd('cxSEditCheckBoxUnchecked', @cxSEditCheckBoxUnchecked);
  InternalAdd('cxSRadioGroupDefaultCaption', @cxSRadioGroupDefaultCaption);
  InternalAdd('cxSTextTrue', @cxSTextTrue);
  InternalAdd('cxSTextFalse', @cxSTextFalse);
  InternalAdd('cxSBlobButtonOK', @cxSBlobButtonOK);
  InternalAdd('cxSBlobButtonCancel', @cxSBlobButtonCancel);
  InternalAdd('cxSBlobButtonClose', @cxSBlobButtonClose);
  InternalAdd('cxSBlobMemo', @cxSBlobMemo);
  InternalAdd('cxSBlobMemoEmpty', @cxSBlobMemoEmpty);
  InternalAdd('cxSBlobPicture', @cxSBlobPicture);
  InternalAdd('cxSBlobPictureEmpty', @cxSBlobPictureEmpty);
  InternalAdd('cxSMenuItemCaptionCut', @cxSMenuItemCaptionCut);
  InternalAdd('cxSMenuItemCaptionCopy', @cxSMenuItemCaptionCopy);
  InternalAdd('cxSMenuItemCaptionPaste', @cxSMenuItemCaptionPaste);
  InternalAdd('cxSMenuItemCaptionDelete', @cxSMenuItemCaptionDelete);
  InternalAdd('cxSMenuItemCaptionLoad', @cxSMenuItemCaptionLoad);
  InternalAdd('cxSMenuItemCaptionSave', @cxSMenuItemCaptionSave);
  InternalAdd('cxSMenuItemCaptionAssignFromWebCam', @cxSMenuItemCaptionAssignFromWebCam);
  InternalAdd('cxSDatePopupClear', @cxSDatePopupClear);
  InternalAdd('cxSDatePopupNow', @cxSDatePopupNow);
  InternalAdd('cxSDatePopupOK', @cxSDatePopupOK);
  InternalAdd('cxSDatePopupToday', @cxSDatePopupToday);
  InternalAdd('cxSDateError', @cxSDateError);
  InternalAdd('scxSCalcError', @scxSCalcError);
  InternalAdd('scxSHyperLinkPrefix', @scxSHyperLinkPrefix);
  InternalAdd('scxSHyperLinkDoubleSlash', @scxSHyperLinkDoubleSlash);
  InternalAdd('cxNavigatorHint_First', @cxNavigatorHint_First);
  InternalAdd('cxNavigatorHint_Prior', @cxNavigatorHint_Prior);
  InternalAdd('cxNavigatorHint_PriorPage', @cxNavigatorHint_PriorPage);
  InternalAdd('cxNavigatorHint_Next', @cxNavigatorHint_Next);
  InternalAdd('cxNavigatorHint_NextPage', @cxNavigatorHint_NextPage);
  InternalAdd('cxNavigatorHint_Last', @cxNavigatorHint_Last);
  InternalAdd('cxNavigatorHint_Insert', @cxNavigatorHint_Insert);
  InternalAdd('cxNavigatorHint_Append', @cxNavigatorHint_Append);
  InternalAdd('cxNavigatorHint_Delete', @cxNavigatorHint_Delete);
  InternalAdd('cxNavigatorHint_Edit', @cxNavigatorHint_Edit);
  InternalAdd('cxNavigatorHint_Post', @cxNavigatorHint_Post);
  InternalAdd('cxNavigatorHint_Cancel', @cxNavigatorHint_Cancel);
  InternalAdd('cxNavigatorHint_Refresh', @cxNavigatorHint_Refresh);
  InternalAdd('cxNavigatorHint_SaveBookmark', @cxNavigatorHint_SaveBookmark);
  InternalAdd('cxNavigatorHint_GotoBookmark', @cxNavigatorHint_GotoBookmark);
  InternalAdd('cxNavigatorHint_Filter', @cxNavigatorHint_Filter);
  InternalAdd('cxNavigator_DeleteRecordQuestion', @cxNavigator_DeleteRecordQuestion);
  InternalAdd('cxNavigatorInfoPanelDefaultDisplayMask', @cxNavigatorInfoPanelDefaultDisplayMask);
  InternalAdd('scxSEditRepositoryBlobItem', @scxSEditRepositoryBlobItem);
  InternalAdd('scxSEditRepositoryButtonItem', @scxSEditRepositoryButtonItem);
  InternalAdd('scxSEditRepositoryCalcItem', @scxSEditRepositoryCalcItem);
  InternalAdd('scxSEditRepositoryCheckBoxItem', @scxSEditRepositoryCheckBoxItem);
  InternalAdd('scxSEditRepositoryComboBoxItem', @scxSEditRepositoryComboBoxItem);
  InternalAdd('scxSEditRepositoryCurrencyItem', @scxSEditRepositoryCurrencyItem);
  InternalAdd('scxSEditRepositoryDateItem', @scxSEditRepositoryDateItem);
  InternalAdd('scxSEditRepositoryHyperLinkItem', @scxSEditRepositoryHyperLinkItem);
  InternalAdd('scxSEditRepositoryImageComboBoxItem', @scxSEditRepositoryImageComboBoxItem);
  InternalAdd('scxSEditRepositoryImageItem', @scxSEditRepositoryImageItem);
  InternalAdd('scxSEditRepositoryLookupComboBoxItem', @scxSEditRepositoryLookupComboBoxItem);
  InternalAdd('scxSEditRepositoryMaskItem', @scxSEditRepositoryMaskItem);
  InternalAdd('scxSEditRepositoryMemoItem', @scxSEditRepositoryMemoItem);
  InternalAdd('scxSEditRepositoryMRUItem', @scxSEditRepositoryMRUItem);
  InternalAdd('scxSEditRepositoryPopupItem', @scxSEditRepositoryPopupItem);
  InternalAdd('scxSEditRepositorySpinItem', @scxSEditRepositorySpinItem);
  InternalAdd('scxSEditRepositoryRadioGroupItem', @scxSEditRepositoryRadioGroupItem);
  InternalAdd('scxSEditRepositoryTextItem', @scxSEditRepositoryTextItem);
  InternalAdd('scxSEditRepositoryTimeItem', @scxSEditRepositoryTimeItem);
  InternalAdd('sdxSEditRepositoryToggleSwitchItem', @sdxSEditRepositoryToggleSwitchItem);
  InternalAdd('scxRegExprLine', @scxRegExprLine);
  InternalAdd('scxRegExprChar', @scxRegExprChar);
  InternalAdd('scxRegExprNotAssignedSourceStream', @scxRegExprNotAssignedSourceStream);
  InternalAdd('scxRegExprEmptySourceStream', @scxRegExprEmptySourceStream);
  InternalAdd('scxRegExprCantUsePlusQuantifier', @scxRegExprCantUsePlusQuantifier);
  InternalAdd('scxRegExprCantUseStarQuantifier', @scxRegExprCantUseStarQuantifier);
  InternalAdd('scxRegExprCantCreateEmptyAlt', @scxRegExprCantCreateEmptyAlt);
  InternalAdd('scxRegExprCantCreateEmptyBlock', @scxRegExprCantCreateEmptyBlock);
  InternalAdd('scxRegExprIllegalSymbol', @scxRegExprIllegalSymbol);
  InternalAdd('scxRegExprIllegalQuantifier', @scxRegExprIllegalQuantifier);
  InternalAdd('scxRegExprNotSupportQuantifier', @scxRegExprNotSupportQuantifier);
  InternalAdd('scxRegExprIllegalIntegerValue', @scxRegExprIllegalIntegerValue);
  InternalAdd('scxRegExprTooBigReferenceNumber', @scxRegExprTooBigReferenceNumber);
  InternalAdd('scxRegExprCantCreateEmptyEnum', @scxRegExprCantCreateEmptyEnum);
  InternalAdd('scxRegExprSubrangeOrder', @scxRegExprSubrangeOrder);
  InternalAdd('scxRegExprHexNumberExpected0', @scxRegExprHexNumberExpected0);
  InternalAdd('scxRegExprHexNumberExpected', @scxRegExprHexNumberExpected);
  InternalAdd('scxRegExprMissing', @scxRegExprMissing);
  InternalAdd('scxRegExprUnnecessary', @scxRegExprUnnecessary);
  InternalAdd('scxRegExprIncorrectSpace', @scxRegExprIncorrectSpace);
  InternalAdd('scxRegExprNotCompiled', @scxRegExprNotCompiled);
  InternalAdd('scxRegExprIncorrectParameterQuantifier', @scxRegExprIncorrectParameterQuantifier);
  InternalAdd('scxRegExprCantUseParameterQuantifier', @scxRegExprCantUseParameterQuantifier);
  InternalAdd('scxMaskEditRegExprError', @scxMaskEditRegExprError);
  InternalAdd('scxMaskEditInvalidEditValue', @scxMaskEditInvalidEditValue);
  InternalAdd('scxMaskEditNoMask', @scxMaskEditNoMask);
  InternalAdd('scxMaskEditIllegalFileFormat', @scxMaskEditIllegalFileFormat);
  InternalAdd('scxMaskEditEmptyMaskCollectionFile', @scxMaskEditEmptyMaskCollectionFile);
  InternalAdd('scxMaskEditMaskCollectionFiles', @scxMaskEditMaskCollectionFiles);
  InternalAdd('cxSSpinEditInvalidNumericValue', @cxSSpinEditInvalidNumericValue);
  InternalAdd('sdxAlertWindowNavigationPanelDefaultDisplayMask', @sdxAlertWindowNavigationPanelDefaultDisplayMask);
  InternalAdd('sdxAlertWindowPreviousMessage', @sdxAlertWindowPreviousMessage);
  InternalAdd('sdxAlertWindowNextMessage', @sdxAlertWindowNextMessage);
  InternalAdd('sdxAlertWindowPin', @sdxAlertWindowPin);
  InternalAdd('sdxAlertWindowClose', @sdxAlertWindowClose);
  InternalAdd('sdxAlertWindowDropdown', @sdxAlertWindowDropdown);
  InternalAdd('sdxBreadcrumbEditInvalidPath', @sdxBreadcrumbEditInvalidPath);
  InternalAdd('sdxBreadcrumbEditInvalidStreamVersion', @sdxBreadcrumbEditInvalidStreamVersion);
  InternalAdd('sdxColorGalleryThemeColors', @sdxColorGalleryThemeColors);
  InternalAdd('sdxColorGalleryStandardColors', @sdxColorGalleryStandardColors);
  InternalAdd('sdxColorDialogAddToCustomColors', @sdxColorDialogAddToCustomColors);
  InternalAdd('sdxColorDialogApply', @sdxColorDialogApply);
  InternalAdd('sdxColorDialogCancel', @sdxColorDialogCancel);
  InternalAdd('sdxColorDialogDefineCustomColor', @sdxColorDialogDefineCustomColor);
  InternalAdd('sdxColorDialogBasicColors', @sdxColorDialogBasicColors);
  InternalAdd('sdxColorDialogCustomColors', @sdxColorDialogCustomColors);
  InternalAdd('sdxColorDialogCaption', @sdxColorDialogCaption);
  InternalAdd('scxShellBrowserDlgCaption', @scxShellBrowserDlgCaption);
  InternalAdd('scxShellBrowserDlgCurrentFolderCaption', @scxShellBrowserDlgCurrentFolderCaption);
  InternalAdd('sdxColorPickerAlphaLabel', @sdxColorPickerAlphaLabel);
  InternalAdd('sdxColorPickerBlueLabel', @sdxColorPickerBlueLabel);
  InternalAdd('sdxColorPickerGreenLabel', @sdxColorPickerGreenLabel);
  InternalAdd('sdxColorPickerHexCodeLabel', @sdxColorPickerHexCodeLabel);
  InternalAdd('sdxColorPickerHueLabel', @sdxColorPickerHueLabel);
  InternalAdd('sdxColorPickerLightnessLabel', @sdxColorPickerLightnessLabel);
  InternalAdd('sdxColorPickerRedLabel', @sdxColorPickerRedLabel);
  InternalAdd('sdxColorPickerSaturationLabel', @sdxColorPickerSaturationLabel);
  InternalAdd('sdxCameraDialogAssign', @sdxCameraDialogAssign);
  InternalAdd('sdxCameraDialogCancel', @sdxCameraDialogCancel);
  InternalAdd('sdxCameraDialogPause', @sdxCameraDialogPause);
  InternalAdd('sdxCameraDialogPlay', @sdxCameraDialogPlay);
  InternalAdd('sdxCameraDialogCaption', @sdxCameraDialogCaption);
  InternalAdd('sdxCameraInactive', @sdxCameraInactive);
  InternalAdd('sdxCameraRunning', @sdxCameraRunning);
  InternalAdd('sdxCameraPaused', @sdxCameraPaused);
  InternalAdd('sdxCameraNotDetected', @sdxCameraNotDetected);
  InternalAdd('sdxCameraInitializing', @sdxCameraInitializing);
  InternalAdd('sdxCameraIsBusy', @sdxCameraIsBusy);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressEditors Library', @AddEditorsResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressEditors Library');

end.
