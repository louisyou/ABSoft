{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressEditors                                           }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSEDITORS AND ALL                }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxCameraControl;

{$I cxVer.inc}

interface

uses
  Types, Windows, Messages, SysUtils, Classes, Controls, ActiveX, Contnrs, Menus, Forms, Graphics,
{$IFDEF DELPHI14}
  DirectShow9,
{$ENDIF}
  dxMediaUtils,
  dxCore, dxCoreGraphics, dxMessages, cxGeometry, cxClasses, cxControls, cxGraphics;

const
  DXM_GRAPHNOTIFY = WM_DX + 7;

type
  TdxCameraRenderingMode = (rmNull, rmWindowLess, rmWindowed);

  TdxCamera = class;

  TdxCameraEventWindow = class(TcxMessageWindow)
  private
    FCamera: TdxCamera;
  protected
    procedure WndProc(var Message: TMessage); override;
  end;

  TdxCamera = class(TcxCustomComponent)
  private
    // Graph
    FBuilder: ICaptureGraphBuilder2;
    FGraph: IGraphBuilder;
    FCameraFilter: IBaseFilter;
    FGrabberFilter: IBaseFilter;
    FSampleGrabber: ISampleGrabber;
    FRenderer: IBaseFilter;
    FMux: IBaseFilter;

    // Media
    FMediaControl: IMediaControl;
    FMediaEvent: IMediaEventEx;
    FEventWindow: TdxCameraEventWindow;
    FVideoWindow: IVideoWindow;
    FWindowlessControl: IVMRWindowlessControl;

    // Device
    FDeviceIndex: Integer;
    FDeviceName: OLEvariant;
    FMoniker: IMoniker;

    // Listeners
    FPreviewBitmap: TcxBitmap32;
    FTimer: TcxTimer;
    FListeners: TComponentList;

    // Flags
    FInitialized: Boolean;
    FRendered: Boolean;
    FConnected: Boolean;
    FIsRunning: Boolean;
    FFailsCount: Integer;

    // Events
    FOnDeviceLost: TNotifyEvent;
    FOnDeviceNotUsed: TNotifyEvent;
    FStateChangedHandlers: TcxEventHandlerCollection;

    procedure OnTimer(Sender: TObject);
    procedure NotifyListeners;

    function InitializeGraph: Boolean;
    function InitializeMedia: Boolean;
    function InitializeDevice(ADeviceIndex: Integer): Boolean;
    function InitializeCaptureGrabber: Boolean;

    function InitializeNullRenderer: Boolean;
    function InitializeWindowlessVMR(AHandle: THandle; const R: TRect): Boolean;
    function InitializeWindowedVMR(AHandle: THandle; const R: TRect): Boolean;

    procedure SetConnected(AValue: Boolean);
  protected
    procedure WndProc(var Message: TMessage);

    function Capture(ABitmap: TcxBitmap32): Boolean;
    function Initialize(ADeviceIndex: Integer): Boolean;
    procedure Finalize;
    function RenderStream(ARenderingMode: TdxCameraRenderingMode; AHandle: THandle; const R: TRect; const AOutputFileName: string): Boolean;
    procedure SetVideoPosition(const R: TRect);

    procedure Play(AControl: TWinControl);
    procedure Stop(AControl: TWinControl);

    property PreviewBitmap: TcxBitmap32 read FPreviewBitmap;
    property Connected: Boolean read FConnected write SetConnected;
    property Initialized: Boolean read FInitialized;

    property OnDeviceLost: TNotifyEvent read FOnDeviceLost write FOnDeviceLost;
    property OnDeviceNotUsed: TNotifyEvent read FOnDeviceNotUsed write FOnDeviceNotUsed;
    property StateChangedHandlers: TcxEventHandlerCollection read FStateChangedHandlers;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TdxCameraManager = class
  private
    FDevEnum: ICreateDevEnum;
    FDevices: TStrings;
    FCameraList: TComponentList;
    FRequestList: TComponentList;
    FRequestTimer: TcxTimer;

    procedure RequestTimer(Sender: TObject);
    procedure DeviceLost(Sender: TObject);
  protected
    procedure RefreshDeviceList;

    function GetDevice(AIndex: Integer; out AMoniker: IMoniker): Boolean;
    function GetDeviceCount: Integer;

    procedure AddRequest(AControl: TWinControl; ADeviceIndex: Integer);
    procedure CancelRequest(AControl: TWinControl);

    property Devices: TStrings read FDevices;
  public
    constructor Create;
    destructor Destroy; override;

    function GetCamera(ADeviceIndex: Integer): TdxCamera;
  end;

  TdxCameraControlState = (ccsInactive, ccsRunning, ccsPaused, ccsNoDevice, ccsInitializing, ccsDeviceIsBusy, ccsDesigning);

  TdxCustomCameraControl = class(TcxControl)
  private
    FCamera: TdxCamera;
    FCapturedBitmap: TcxBitmap32;
    FFitMode: TcxImageFitMode;
    FDeviceIndex: Integer;
    FInternalPopupMenu: TPopupMenu;

    FActive: Boolean;
    FPaused: Boolean;

    FOnStateChanged: TNotifyEvent;

    function GetDisplayText: string;

    procedure SetActive(AValue: Boolean);
    procedure SetCamera(AValue: TdxCamera);
    procedure SetFitMode(AValue: TcxImageFitMode);
    function GetDeviceName: string;
    function GetState: TdxCameraControlState;
    procedure InternalMenuClick(Sender: TObject);
    procedure SetDeviceIndex(Value: Integer);

    procedure CameraStateChanged(Sender: TObject; const AEventArgs);
  protected
    // TCustomControl
    procedure Paint; override;
    function GetPopupMenu: TPopupMenu; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    // TcxControl
    function IsDoubleBufferedNeeded: Boolean; override;

    property Camera: TdxCamera read FCamera write SetCamera;
    property DeviceIndex: Integer read FDeviceIndex write SetDeviceIndex;
    property FitMode: TcxImageFitMode read FFitMode write SetFitMode;
    property State: TdxCameraControlState read GetState;

    property OnStateChanged: TNotifyEvent read FOnStateChanged write FOnStateChanged;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Capture;
    procedure Pause;
    procedure Play;
    procedure Stop;

    property Active: Boolean read FActive write SetActive;
    property CapturedBitmap: TcxBitmap32 read FCapturedBitmap;
    property DeviceName: string read GetDeviceName;
  end;

  TdxCameraControl = class(TdxCustomCameraControl)
  published
    property DeviceIndex default 0; 
    property Active default False;
    property Align;
    property Anchors;
    property BorderStyle default cxcbsDefault;
    property Enabled;
    property FitMode default ifmProportionalStretch;
    property Font;
    property LookAndFeel;
    property PopupMenu;
    property Visible;

    property OnContextPopup;
    property OnClick;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;

function dxCameraManager: TdxCameraManager;

implementation

uses
  cxEditConsts;

var
  FCameraManager: TdxCameraManager;

function dxCameraManager: TdxCameraManager;
begin
  if FCameraManager = nil then
    FCameraManager := TdxCameraManager.Create;
  Result := FCameraManager;
end;

{ TdxCamera }

function dxMediaResult(AStatus: HRESULT): Boolean;
begin
  Result := AStatus = S_OK;
end;

procedure dxMediaCheck(AStatus: HRESULT; AMessage: string = '');
begin
  if AMessage = '' then
    AMessage := 'dxMediaCheck fails';
  if not dxMediaResult(AStatus) then
    raise EdxException.Create(AMessage);
end;

constructor TdxCameraManager.Create;
begin
  inherited;

  FDevices := TStringList.Create;
  FCameraList := TComponentList.Create;
  FRequestList := TComponentList.Create(False);
  FRequestTimer := TcxTimer.Create(nil);
  FRequestTimer.Enabled := False;
  FRequestTimer.OnTimer := RequestTimer;

  CoCreateInstance(CLSID_SystemDeviceEnum, nil, CLSCTX_INPROC, IID_ICreateDevEnum, FDevEnum);
end;

destructor TdxCameraManager.Destroy;
begin
  FreeAndNil(FRequestTimer);
  FreeAndNil(FRequestList);
  FreeAndNil(FCameraList);
  FreeAndNil(FDevices);
  inherited;
end;

procedure TdxCameraManager.AddRequest(AControl: TWinControl; ADeviceIndex: Integer);
begin
  if FRequestList.IndexOf(AControl) = -1 then
  begin
    FRequestList.Add(AControl);
    FRequestTimer.Enabled := True;
    FRequestTimer.OnTimer(nil);
  end;
end;

procedure TdxCameraManager.CancelRequest(AControl: TWinControl);
begin
  FRequestList.Remove(AControl);
end;

function TdxCameraManager.GetDevice(AIndex: Integer; out AMoniker: IMoniker): Boolean;
var
  AEnumMoniker: IEnumMoniker;
begin
  AMoniker := nil;

  FDevEnum.CreateClassEnumerator(CLSID_VideoInputDeviceCategory, AEnumMoniker, 0);
  Result := (AEnumMoniker <> nil) and dxMediaResult(AEnumMoniker.Reset);
  Result := Result and (dxMediaResult(AEnumMoniker.Skip(AIndex)) or (AIndex = 0));
  Result := Result and dxMediaResult(AEnumMoniker.Next(1, AMoniker, nil));
end;

function TdxCameraManager.GetDeviceCount: Integer;
begin
  RefreshDeviceList;
  Result := FDevices.Count;
end;

function TdxCameraManager.GetCamera(ADeviceIndex: Integer): TdxCamera;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FCameraList.Count - 1 do
  begin
    Result := TdxCamera(FCameraList[I]);
    if (csDestroying in Result.ComponentState) or (Result.FDeviceIndex <> ADeviceIndex) then
      Result := nil
    else
      Break;
  end;

  if Result = nil then
  begin
    Result := TdxCamera.Create(nil);
    Result.Initialize(ADeviceIndex);
    if Result.Initialized then
    begin
      FCameraList.Add(Result);
      Result.OnDeviceLost := DeviceLost;
      Result.OnDeviceNotUsed := DeviceLost;
    end
    else
      FreeAndNil(Result);
  end;
end;

procedure TdxCameraManager.RefreshDeviceList;
var
  AMoniker: IMoniker;
  APropertyBag: IPropertyBag;
  ADeviceName: OLEvariant;
  AEnumMoniker: IEnumMoniker;
begin
  FDevices.Clear;

  FDevEnum.CreateClassEnumerator(CLSID_VideoInputDeviceCategory, AEnumMoniker, 0);
  if AEnumMoniker <> nil then
  begin
    AEnumMoniker.Reset;
    repeat
      AEnumMoniker.Next(1, AMoniker, nil);
      if AMoniker <> nil then
      begin
        if dxMediaResult(AMoniker.BindToStorage(nil, nil, IPropertyBag, APropertyBag)) then
          if dxMediaResult(APropertyBag.Read('FriendlyName', ADeviceName, nil)) then
            FDevices.Add(ADeviceName);
      end;
    until AMoniker = nil
  end;
end;

procedure TdxCameraManager.RequestTimer(Sender: TObject);
var
  I: Integer;
  AControl: TdxCustomCameraControl;
begin
  RefreshDeviceList;
  for I := FRequestList.Count - 1 downto 0 do
  begin
    AControl := FRequestList[I] as TdxCustomCameraControl;
    AControl.Camera := GetCamera(AControl.DeviceIndex);
  end;

  if FRequestList.Count = 0 then
    FRequestTimer.Enabled := False;
end;

procedure TdxCameraManager.DeviceLost(Sender: TObject);
begin
  Sender.Free;
end;

{ TdxCameraEventWindow }

procedure TdxCameraEventWindow.WndProc(var Message: TMessage);
begin
  FCamera.WndProc(Message);
  inherited;
end;

{ TdxCamera }

constructor TdxCamera.Create(AOwner: TComponent);
begin
  inherited;
  FTimer := TcxTimer.Create(nil);
  FTimer.Interval := 100;   
  FTimer.Enabled := False;
  FTimer.OnTimer := OnTimer;
  FPreviewBitmap := TcxBitmap32.Create;
  FListeners := TComponentList.Create(False);
  FEventWindow := TdxCameraEventWindow.Create;
  FEventWindow.FCamera := Self;
  FStateChangedHandlers := TcxEventHandlerCollection.Create;
end;

destructor TdxCamera.Destroy;
begin
  FreeAndNil(FStateChangedHandlers);
  FreeAndNil(FEventWindow);
  FreeAndNil(FListeners);
  FreeAndNil(FPreviewBitmap);
  FreeAndNil(FTimer);
  Finalize;
  inherited;
end;

function TdxCamera.Capture(ABitmap: TcxBitmap32): Boolean;
var
  ASize: Integer;
  AColors: TRGBColors;
  AMediaType: TAMMediaType;
  AInfoHeader: TVideoInfoHeader;
begin
  Result := False;
  if not FIsRunning then
    Exit;

  if dxMediaResult(FSampleGrabber.GetCurrentBuffer(ASize, nil)) then
    if dxMediaResult(FSampleGrabber.GetConnectedMediaType(AMediaType)) then
    begin
       AInfoHeader := TVideoInfoHeader(AMediaType.pbFormat^);
       if (AInfoHeader.bmiHeader.biWidth * AInfoHeader.bmiHeader.biHeight) * 4 = ASize then
       begin
         SetLength(AColors, ASize div 4);
         if dxMediaResult(FSampleGrabber.GetCurrentBuffer(ASize, @AColors[0])) then
         begin
           ABitmap.SetSize(AInfoHeader.bmiHeader.biWidth, AInfoHeader.bmiHeader.biHeight);
           ABitmap.SetBitmapColors(AColors);
           Connected := True;
           Result := True;
         end;
       end;
    end;

  if not Result then
    ABitmap.SetSize(0, 0);
end;

procedure TdxCamera.OnTimer(Sender: TObject);
begin
  if Capture(FPreviewBitmap) then
    NotifyListeners
  else
  begin
    Inc(FFailsCount);
    if not FRendered and (FFailsCount mod 10 = 0) then
    begin
      RenderStream(rmNull, 0, cxNullRect, '');
      Play(nil);
    end
    else
      if FRendered and (FFailsCount mod 30 = 0) then
      begin
        FMediaControl.Stop;
        FMediaControl.Run;
        NotifyListeners;
      end;
  end;
end;

procedure TdxCamera.NotifyListeners;
var
  I: Integer;
begin
  for I := 0 to FListeners.Count - 1 do
    cxInvalidateRect(FListeners[I] as TWinControl);
end;

function TdxCamera.InitializeGraph: Boolean;
begin
  FBuilder := nil;
  FGraph := nil;

  CoCreateInstance(CLSID_CaptureGraphBuilder2, nil, CLSCTX_INPROC, IID_ICaptureGraphBuilder2, FBuilder);
  CoCreateInstance(CLSID_FilterGraph, nil, CLSCTX_INPROC, IID_IGraphBuilder, FGraph);
  Result := (FBuilder <> nil) and (FGraph <> nil) and dxMediaResult(FBuilder.SetFiltergraph(FGraph));
end;

function TdxCamera.InitializeMedia: Boolean;
begin
  FMediaControl := nil;
  FMediaEvent := nil;

  Result := dxMediaResult(FGraph.QueryInterface(IID_IMediaControl, FMediaControl)) and
   dxMediaResult(FGraph.QueryInterface(IID_IMediaEventEx, FMediaEvent)) and
   dxMediaResult(FMediaEvent.SetNotifyWindow(FEventWindow.Handle, DXM_GRAPHNOTIFY, 0));
end;

function TdxCamera.InitializeDevice(ADeviceIndex: Integer): Boolean;
var
  AFilter: IBaseFilter;
  AMoniker: IMoniker;
  APropertyBag: IPropertyBag;
  ADeviceName: OLEvariant;
begin
  Result := False;
  FCameraFilter := nil;

  if dxCameraManager.GetDevice(ADeviceIndex, AMoniker) then
    if dxMediaResult(AMoniker.BindToObject(nil, nil, IID_IBaseFilter, AFilter)) then
        if dxMediaResult(AMoniker.BindToStorage(nil, nil, IPropertyBag, APropertyBag)) then
          if dxMediaResult(APropertyBag.Read('FriendlyName', ADeviceName, nil)) then
            if dxMediaResult(FGraph.AddFilter(AFilter, 'Video Capture')) then
            begin
              FDeviceIndex := ADeviceIndex;
              FDeviceName := ADeviceName;
              FCameraFilter := AFilter;
              FMoniker := AMoniker;
              Result := True;
            end;
end;

function TdxCamera.InitializeCaptureGrabber: Boolean;
var
  AMediaType: TAMMediaType;
begin
  Result := False;
  FSampleGrabber := nil;

  if dxMediaResult(CoCreateInstance(CLSID_SampleGrabber, nil, CLSCTX_INPROC, IID_IBaseFilter, FGrabberFilter)) then
    if dxMediaResult(FGraph.AddFilter(FGrabberFilter, 'Video Grabber')) then
      if dxMediaResult(FGrabberFilter.QueryInterface(IID_ISampleGrabber, FSampleGrabber)) then
      begin
        dxMediaCheck(FSampleGrabber.SetBufferSamples(True), 'SetBufferSamples fails');

        cxZeroMemory(@AMediaType, SizeOf(TAMMediaType));
        AMediaType.majortype := MEDIATYPE_Video;
        AMediaType.subtype := MEDIASUBTYPE_RGB32;
        dxMediaCheck(FSampleGrabber.SetMediaType(AMediaType), 'SetMediaType fails');

        Result := True;
      end;
end;

function TdxCamera.InitializeNullRenderer: Boolean;
begin
  Result := False;
  if dxMediaResult(CoCreateInstance(CLSID_NullRenderer, nil, CLSCTX_INPROC, IID_IBaseFilter, FRenderer)) then
    Result := dxMediaResult(FGraph.AddFilter(FRenderer, 'Null Filter'));
end;

function TdxCamera.InitializeWindowlessVMR(AHandle: THandle; const R: TRect): Boolean;
{$IFDEF DELPHI14}
var
  AConfig: IVMRFilterConfig;
{$ENDIF}
begin
  Result := False;
{$IFDEF DELPHI14}
  if FWindowlessControl = nil then
    if dxMediaResult(CoCreateInstance(CLSID_VideoMixingRenderer, nil, CLSCTX_INPROC, IID_IBaseFilter, FRenderer)) then
      if dxMediaResult(FGraph.AddFilter(FRenderer, 'Video Mixing Renderer')) then
        if dxMediaResult(FRenderer.QueryInterface(IID_IVMRFilterConfig, AConfig)) then
          if dxMediaResult(AConfig.SetRenderingMode(VMRMode_Windowless)) then
            FRenderer.QueryInterface(IID_IVMRWindowlessControl, FWindowlessControl);

  if FWindowlessControl <> nil then
    if dxMediaResult(FWindowlessControl.SetVideoClippingWindow(AHandle)) then
       Result := dxMediaResult(FWindowlessControl.SetVideoPosition(nil, @R));
{$ENDIF}
end;

function TdxCamera.InitializeWindowedVMR(AHandle: THandle; const R: TRect): Boolean;
begin
  Result := False;
{$IFDEF DELPHI14}
  if FVideoWindow = nil then
    FGraph.QueryInterface(IID_IVideoWindow, FVideoWindow);

  if FVideoWindow <> nil then
    if dxMediaResult(FVideoWindow.put_Owner(AHandle)) then
      if dxMediaResult(FVideoWindow.put_WindowStyle(WS_CHILD or WS_CLIPCHILDREN)) then
        if dxMediaResult(FVideoWindow.SetWindowPosition(R.Left, R.Top, cxRectWidth(R), cxRectHeight(R))) then
          Result := dxMediaResult(FVideoWindow.put_Visible(True));
{$ENDIF}
end;

procedure TdxCamera.SetConnected(AValue: Boolean);
begin
  if FConnected <> AValue then
  begin
    FConnected := AValue;
    FFailsCount := 0;
    StateChangedHandlers.CallEvents(Self, []);
  end;
end;

function TdxCamera.Initialize(ADeviceIndex: Integer): Boolean;
begin
  Result := False;
  if not FInitialized then
  begin
    if InitializeGraph then
      if InitializeMedia then
        if InitializeDevice(ADeviceIndex) then
          FInitialized := InitializeCaptureGrabber;
    if not FInitialized then
      Finalize;
    Result := FInitialized;
  end;
end;

procedure TdxCamera.Finalize;
begin
  Connected := False;
  FInitialized := False;
  FRendered := False;
  FIsRunning := False;

  FMux := nil;
  FRenderer := nil;
  FVideoWindow := nil;
  FWindowlessControl := nil;
  FMediaEvent := nil;
  FMediaControl := nil;
  FMoniker := nil;
  FSampleGrabber := nil;
  FCameraFilter  := nil;
  FGrabberFilter  := nil;
  FGraph := nil;
  FBuilder := nil;
end;

procedure TdxCamera.SetVideoPosition(const R: TRect);
begin
{$IFDEF DELPHI14}
  if FWindowlessControl <> nil then
    FWindowlessControl.SetVideoPosition(nil, @R);
  if FVideoWindow <> nil then
    FVideoWindow.SetWindowPosition(R.Left, R.Top, cxRectWidth(R), cxRectHeight(R));
{$ENDIF}
end;

function TdxCamera.RenderStream(ARenderingMode: TdxCameraRenderingMode; AHandle: THandle; const R: TRect; const AOutputFileName: string): Boolean;
var
{$IFDEF DELPHI14}
  ASink: IFileSinkFilter;
{$ELSE}
  ASink: IUnknown;
{$ENDIF}  
begin
  Result := False;
  if FInitialized and not FRendered then
  begin
    case ARenderingMode of
      rmNull:
        InitializeNullRenderer;
      rmWindowLess:
        InitializeWindowlessVMR(AHandle, R);
      rmWindowed:
        InitializeWindowedVMR(AHandle, R);
    end;
    if AOutputFileName <> '' then
    begin
      FBuilder.SetOutputFileName(MEDIASUBTYPE_Avi, PWideChar(dxStringToWideString(AOutputFileName)), FMux, ASink);
      FRendered := dxMediaResult(FBuilder.RenderStream(@PIN_CATEGORY_CAPTURE, @MEDIATYPE_Video, FCameraFilter, FGrabberFilter, FMux));
      FBuilder.RenderStream(@PIN_CATEGORY_PREVIEW, @MEDIATYPE_Video, FCameraFilter, nil, FRenderer); 
    end
    else
      if FRenderer <> nil then
        FRendered := dxMediaResult(FBuilder.RenderStream(@PIN_CATEGORY_CAPTURE, @MEDIATYPE_Video, FCameraFilter, FGrabberFilter, FRenderer));
    Result := FRendered;
  end;
end;

procedure TdxCamera.WndProc(var Message: TMessage);
var
  ACode: Integer;
{$IFDEF DELPHI17}
  AParam1, AParam2: TdxNativeInt;
{$ELSE}
  AParam1, AParam2: Longint;
{$ENDIF}
begin
  if Message.Msg = DXM_GRAPHNOTIFY then
  begin
    FMediaEvent.GetEvent(ACode, AParam1, AParam2, INFINITE);
    if (ACode = EC_DEVICE_LOST) and (AParam2 = 0) then
    begin
      Finalize;
      CallNotify(OnDeviceLost, Self);
    end;
  end;
end;

procedure TdxCamera.Play(AControl: TWinControl);
begin
  if FInitialized then
  begin
    if not FIsRunning and FRendered then
    begin
      FMediaControl.Run;
      FIsRunning := True;
    end;
    if (AControl <> nil) and (FListeners.IndexOf(AControl) = -1) then
    begin
      cxAddFreeNotification(AControl, Self);
      FListeners.Add(AControl);
      FTimer.Enabled := True;
    end;
  end;
end;

procedure TdxCamera.Stop(AControl: TWinControl);
begin
  if csDestroying in ComponentState then  
    Exit;

  cxRemoveFreeNotification(AControl, Self);
  FListeners.Remove(AControl);
  if FListeners.Count = 0 then
  begin
    FTimer.Enabled := False;
    FMediaControl.Stop;
    FIsRunning := False;
    Connected := False;
    dxCallNotify(OnDeviceNotUsed, Self);
  end;
end;

{ TdxCustomCameraControl }

constructor TdxCustomCameraControl.Create(AOwner: TComponent);
begin
  inherited;

  ControlStyle := ControlStyle - [csParentBackground];
  BorderStyle := cxcbsDefault;

  Width := 200;
  Height := 150;

  FCapturedBitmap := TcxBitmap32.Create;
  FInternalPopupMenu := TPopupMenu.Create(nil);
  FFitMode := ifmProportionalStretch;
end;

destructor TdxCustomCameraControl.Destroy;
begin
  Active := False;
  FreeAndNil(FInternalPopupMenu);
  FreeAndNil(FCapturedBitmap);
  inherited;
end;

procedure TdxCustomCameraControl.Capture;
begin
  FCamera.Capture(FCapturedBitmap);
end;

procedure TdxCustomCameraControl.Paint;

  procedure DrawBackground;
  begin
    Canvas.FillRect(ClientRect, clWhite);
  end;

begin
  inherited;

  DrawBackground;

  case State of
    ccsRunning:
      cxDrawImage(Canvas, ClientRect, FCamera.PreviewBitmap, FFitMode);
    ccsPaused:
      cxDrawImage(Canvas, ClientRect, CapturedBitmap, FFitMode);
    else
      cxDrawMultilineText(Canvas, GetDisplayText, ClientRect, DT_VCENTER or DT_CENTER);
  end;
end;

procedure TdxCustomCameraControl.Pause;
begin
  if State = ccsRunning then
  begin
    Capture;
    FPaused := True;
  end;
end;

procedure TdxCustomCameraControl.Play;
begin
  FPaused := False;
  Active := True;
end;

procedure TdxCustomCameraControl.Stop;
begin
  FPaused := False;
  Active := False;
end;

function TdxCustomCameraControl.GetPopupMenu: TPopupMenu;
var
  I: Integer;
  AItem: TMenuItem;
begin
  Result := inherited GetPopupMenu;
  if Result = nil then
  begin
    dxCameraManager.RefreshDeviceList;
    if Active and (dxCameraManager.GetDeviceCount > 1) then
    begin
      FInternalPopupMenu.Items.Clear;

      for I := 0 to dxCameraManager.FDevices.Count - 1 do
      begin
        AItem := TMenuItem.Create(FInternalPopupMenu);
        AItem.Caption := dxCameraManager.FDevices[I];
        AItem.Checked := I = DeviceIndex;
        AItem.Tag := I;
        AItem.OnClick := InternalMenuClick;
        FInternalPopupMenu.Items.Add(AItem);
      end;
      Result := FInternalPopupMenu;
    end;
  end;
end;

function TdxCustomCameraControl.GetState: TdxCameraControlState;
begin
  if Active and (csDesigning in ComponentState) then
    Result := ccsDesigning
  else
    if Active and FPaused then
      Result := ccsPaused
    else
      if Active and (FCamera = nil) then
        Result := ccsNoDevice
      else
        if Active and (FCamera <> nil) and FCamera.Connected then
          Result := ccsRunning
        else
          if Active and (FCamera <> nil) and not FCamera.Connected and (FCamera.FFailsCount < 30) then
            Result := ccsInitializing
          else
            if Active and (FCamera <> nil) and not FCamera.Connected then
              Result := ccsDeviceIsBusy
            else
              Result := ccsInactive;
end;

procedure TdxCustomCameraControl.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) and (AComponent = FCamera) then
    Camera := nil;
end;

function TdxCustomCameraControl.IsDoubleBufferedNeeded: Boolean;
begin
  Result := True;
end;

function TdxCustomCameraControl.GetDisplayText: string;
begin
  case GetState of
    ccsInactive:
      Result := cxGetResourceString(@sdxCameraInactive);
    ccsRunning:
      Result := cxGetResourceString(@sdxCameraRunning);
    ccsPaused:
      Result := cxGetResourceString(@sdxCameraPaused);
    ccsNoDevice:
      Result := cxGetResourceString(@sdxCameraNotDetected);
    ccsInitializing:
      Result := cxGetResourceString(@sdxCameraInitializing);
    ccsDeviceIsBusy:
      Result := cxGetResourceString(@sdxCameraIsBusy);
    ccsDesigning:
      Result := 'No rendering at design time';
  else
    Result := '';
  end;
end;

procedure TdxCustomCameraControl.SetActive(AValue: Boolean);
begin
  if FActive <> AValue then
  begin
    if csDesigning in ComponentState then
      FActive := AValue
    else
    begin
      FActive := AValue;
      if AValue then
        dxCameraManager.AddRequest(Self, DeviceIndex)
      else
        Camera := nil;
    end;
    cxInvalidateRect(Self);
  end;
end;

procedure TdxCustomCameraControl.SetCamera(AValue: TdxCamera);
begin
  if AValue <> FCamera then
  begin
    if FCamera <> nil then
    begin
      FCamera.StateChangedHandlers.Remove(CameraStateChanged);
      FCamera.Stop(Self);
    end
    else
      dxCameraManager.CancelRequest(Self);

    FCamera := AValue;

    if FCamera <> nil then
    begin
      FCamera.Play(Self);
      FCamera.StateChangedHandlers.Add(CameraStateChanged);
    end
    else
      if Active then
        dxCameraManager.AddRequest(Self, DeviceIndex);

    cxInvalidateRect(Self);
  end;
end;

procedure TdxCustomCameraControl.SetFitMode(AValue: TcxImageFitMode);
begin
  if FitMode <> AValue then
  begin
    FFitMode := AValue;
    cxInvalidateRect(Self);
  end;
end;

function TdxCustomCameraControl.GetDeviceName: string;
begin
  if FCamera <> nil then
    Result := FCamera.FDeviceName
  else
    Result := '';
end;

procedure TdxCustomCameraControl.InternalMenuClick(Sender: TObject);
begin
  DeviceIndex := (Sender as TMenuItem).Tag;
end;

procedure TdxCustomCameraControl.SetDeviceIndex(Value: Integer);
begin
  if Active then
  begin
    Active := False;
    FDeviceIndex := Value;
    Active := True;
  end
  else
    FDeviceIndex := Value;
end;

procedure TdxCustomCameraControl.CameraStateChanged(Sender: TObject; const AEventArgs);
begin
  dxCallNotify(OnStateChanged, Self);
end;

initialization

finalization
  FreeAndNil(FCameraManager);

end.
