{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressMapControl                                        }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSMAPCONTROL AND ALL             }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxMapControlHttpRequest;

interface

{$I cxVer.inc}

uses
  SysUtils, Classes, IdHTTP, IdComponent, IdHTTPHeaderInfo, Forms;

type
  TdxMapControlHttpRequestMode = (mhrmRead, mhrmWrite);
  TdxMapControlHttpRequestEvent = procedure (ASender: TObject;
    ARequestMode: TdxMapControlHttpRequestMode; ACount: Int64; var ACancel: Boolean) of object;

  TdxMapControlHttpRequest = class
  private
    FHasErrors: Boolean;
    FIsCancelled: Boolean;
    FOnRequest: TdxMapControlHttpRequestEvent;
    procedure DoWork(ASender: TObject; AWorkMode: TWorkMode; AWorkCount: Int64);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Get(const AUri: string; AResponseContent: TStream);
    function HasErrors: Boolean;
    function IsCancelled: Boolean;

    property OnRequest: TdxMapControlHttpRequestEvent read FOnRequest write FOnRequest;
  end;

function GetContent(const AUri: string; AResponseContent: TStream): Boolean;
function dxParamsEncode(const ASrc: string): string;
function dxMapControlHttpDefaultProxyInfo: TIdProxyConnectionInfo;

implementation

uses
  IdURI;

var
  FHttpDefaultProxyInfo: TIdProxyConnectionInfo;

function dxMapControlHttpDefaultProxyInfo: TIdProxyConnectionInfo;
begin
  if FHttpDefaultProxyInfo = nil then
    FHttpDefaultProxyInfo := TIdProxyConnectionInfo.Create;
  Result := FHttpDefaultProxyInfo;
end;

procedure SetUserAgent(AHTTP: TIdHTTP);
begin
  AHTTP.Request.UserAgent := StringReplace(AHTTP.Request.UserAgent, 'Indy Library', Application.Title, []);
end;

procedure SetProxyParams(AHTTP: TIdHTTP);
begin
  if FHttpDefaultProxyInfo <> nil then
    AHTTP.ProxyParams.Assign(FHttpDefaultProxyInfo);
end;

function GetContent(const AUri: string; AResponseContent: TStream): Boolean;
var
  AHTTP: TIdHTTP;
begin
  Result := True;
  try
    AHTTP := TIdHTTP.Create(nil);
    try
      SetUserAgent(AHTTP);
      SetProxyParams(AHTTP);
      AHTTP.Get(AUri, AResponseContent);
    finally
      AHTTP.Free;
    end;
  except
    on Exception do
      Result := False;
  end;
end;

function dxParamsEncode(const ASrc: string): string;
begin
  Result := TIdURI.ParamsEncode(ASrc);
end;

{ TdxMapControlHttpRequest }

constructor TdxMapControlHttpRequest.Create;
begin
  inherited Create;

end;

destructor TdxMapControlHttpRequest.Destroy;
begin

  inherited Destroy;
end;

procedure TdxMapControlHttpRequest.Get(const AUri: string;
  AResponseContent: TStream);
var
  AHTTP: TIdHTTP;
begin
  FHasErrors := False;
  FIsCancelled := False;
  try
    AHTTP := TIdHTTP.Create(nil);
    try
      SetUserAgent(AHTTP);
      SetProxyParams(AHTTP);
      AHTTP.OnWork := DoWork;
      AHTTP.Get(AUri, AResponseContent);
      AHTTP.OnWork := nil;
    finally
      AHTTP.Free;
    end;
  except
    on Exception do
      FHasErrors := True;
  end;
end;

function TdxMapControlHttpRequest.HasErrors: Boolean;
begin
  Result := FHasErrors;
end;

function TdxMapControlHttpRequest.IsCancelled: Boolean;
begin
  Result := FIsCancelled;
end;

procedure TdxMapControlHttpRequest.DoWork(ASender: TObject;
  AWorkMode: TWorkMode; AWorkCount: Int64);
const
  WorkModeToRequestMode: array [TWorkMode] of TdxMapControlHttpRequestMode = (mhrmRead, mhrmWrite);
begin
  if Assigned(FOnRequest) then
    FOnRequest(Self, WorkModeToRequestMode[AWorkMode], AWorkCount, FIsCancelled);
  if FIsCancelled then
    (ASender as TIdHttp).Disconnect;
end;

initialization

finalization
  FreeAndNil(FHttpDefaultProxyInfo);

end.
