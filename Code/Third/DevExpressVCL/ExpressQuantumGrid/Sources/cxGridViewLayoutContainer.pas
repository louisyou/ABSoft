{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressQuantumGrid                                       }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSQUANTUMGRID AND ALL            }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxGridViewLayoutContainer;

{$I cxVer.inc}

interface

uses
  Variants, Windows, Classes, Graphics, Controls, Contnrs, ImgList, StdCtrls,
  dxCore, cxClasses, cxGraphics, cxControls, cxStyles, cxLookAndFeelPainters,
  cxGridCommon, cxGrid, dxCoreClasses, dxGDIPlusClasses,
  cxGridCustomView, cxGridCustomTableView, dxLayoutLookAndFeels, cxDataStorage,
  cxCustomData, cxEdit, dxLayoutContainer, dxLayoutSelection,
  dxLayoutCommon, Forms, cxNavigator, cxLookAndFeels, SysUtils;

const
  cxGridLayoutCustomizationFormDefaultWidth = 800;
  cxGridLayoutCustomizationFormDefaultHeight = 500;

type
  TcxGridCustomLayoutItemEditViewInfo = class;
  TcxGridCustomLayoutItemViewInfo = class;
  TcxGridCustomLayoutItem = class;
  TcxGridViewLayoutItemDataCellViewInfo = class;
  TcxGridCustomLayoutController = class;

  TcxGridCustomLayoutItemClass = class of TcxGridCustomLayoutItem;

  { TcxGridCustomLayoutLookAndFeel }

  TcxLayoutViewLookAndFeelItemPadding = class(TdxLayoutLookAndFeelPadding)
  protected
    function GetDefaultValue(Index: Integer): Integer; override;
  end;

  TcxLayoutLayoutViewLookAndFeelItemOptions = class(TdxLayoutLookAndFeelItemOptions)
  protected
    function GetPaddingClass: TdxLayoutLookAndFeelPaddingClass; override;
  end;

  TcxGridCustomLayoutLookAndFeel = class(TdxLayoutCxLookAndFeel)
  private
    FLockCount: Integer;
    FGridView: TcxCustomGridTableView;
  protected
    procedure Changed; override;
    function GetItemOptionsClass: TdxLayoutLookAndFeelItemOptionsClass; override;
  public
    constructor Create(AGridView: TcxCustomGridTableView); reintroduce; virtual;
    procedure BeginUpdate;
    procedure EndUpdate;

    property GridView: TcxCustomGridTableView read FGridView;
  end;

  { TcxGridCustomLayoutItemCaptionPainter }

  TcxGridCustomLayoutItemCaptionPainter = class(TdxCustomLayoutItemCaptionPainter)
  protected
    procedure DrawBackground; override;
  end;

  { TcxGridCustomLayoutItemEditPainter }

  TcxGridCustomLayoutItemEditPainter = class(TdxLayoutControlItemControlPainter)
  private
    function GetViewInfo: TcxGridCustomLayoutItemEditViewInfo;
  protected
    procedure DrawEdit; virtual;
    function GetGridViewItem: TcxCustomGridTableItem; virtual;
    function GetLookAndFeel: TcxLookAndFeel; virtual;
  public
    procedure Paint; override;
    property ViewInfo: TcxGridCustomLayoutItemEditViewInfo read GetViewInfo;
  end;

  { TcxGridCustomLayoutItemPainter }

  TcxGridCustomLayoutItemPainter = class(TdxLayoutControlItemPainter)
  private
    function GetViewInfo: TcxGridCustomLayoutItemViewInfo;
  protected
    function CanPaint: Boolean; override;
    function GetCaptionPainterClass: TdxCustomLayoutItemCaptionPainterClass; override;
    function GetControlPainterClass: TdxLayoutControlItemControlPainterClass; override;

    property ViewInfo: TcxGridCustomLayoutItemViewInfo read GetViewInfo;
  end;

  { TcxGridCustomLayoutItemCaptionViewInfo }

  TcxGridCustomLayoutItemCaptionViewInfo = class(TdxLayoutControlItemCaptionViewInfo)
  private
    function GetItem: TdxLayoutControlItem;{$IFDEF DELPHI9} inline; {$ENDIF}
    function GetItemViewInfo: TcxGridCustomLayoutItemViewInfo;{$IFDEF DELPHI9} inline; {$ENDIF}
  protected
    function CalculatePadding: TRect; override;
    function GetTextColor: TColor; override;

    property Item: TdxLayoutControlItem read GetItem;
    property ItemViewInfo: TcxGridCustomLayoutItemViewInfo read GetItemViewInfo;
  public
    function CalculateHeight: Integer; override;
  end;

  { TcxGridCustomLayoutItemEditViewInfo }

  TcxGridCustomLayoutItemEditViewInfo = class(TdxLayoutControlItemControlViewInfo)
  private
    FDataHeight: Integer;
    function GetDataViewInfo: TcxGridTableDataCellViewInfo;{$IFDEF DELPHI9} inline; {$ENDIF}
    function GetItem: TcxGridCustomLayoutItem;{$IFDEF DELPHI9} inline; {$ENDIF}
    function GetItemViewInfo: TcxGridCustomLayoutItemViewInfo;{$IFDEF DELPHI9} inline; {$ENDIF}
    function GetGridView: TcxCustomGridTableView;{$IFDEF DELPHI9} inline; {$ENDIF}
  protected
    function GetDefaultValueHeight: Integer; virtual;
    function GetMinValueWidth: Integer; virtual;
    function GetOriginalControlSize: TSize; override;
    function GetValueHeight: Integer;
    function HasBorder: Boolean; override;

    property DataViewInfo: TcxGridTableDataCellViewInfo read GetDataViewInfo;
    property GridView: TcxCustomGridTableView read GetGridView;
    property ItemViewInfo: TcxGridCustomLayoutItemViewInfo read GetItemViewInfo;
    property Item: TcxGridCustomLayoutItem read GetItem;
  public
    procedure CalculateInternalTabOrder(var AAvailTabOrder: Integer); override;
    function CalculateMinHeight: Integer; override;
    function CalculateMinWidth: Integer; override;
  end;

  { TcxGridCustomLayoutItemViewInfo }

  TcxGridCustomLayoutItemViewInfo = class(TdxLayoutControlItemViewInfo)
  private
    FGridItemViewInfo: TcxGridTableDataCellViewInfo;

    function GetControlViewInfo: TcxGridCustomLayoutItemEditViewInfo;{$IFDEF DELPHI9} inline; {$ENDIF}
    function GetItem: TcxGridCustomLayoutItem;{$IFDEF DELPHI9} inline; {$ENDIF}
    function GetGridItemViewInfo: TcxGridTableDataCellViewInfo;{$IFDEF DELPHI9} inline; {$ENDIF}
  protected
    function GetCaptionViewInfoClass: TdxCustomLayoutItemCaptionViewInfoClass; override;
    function GetControlViewInfoClass: TdxLayoutControlItemControlViewInfoClass; override;
    function GetCurrentGridItemViewInfo: TcxGridTableDataCellViewInfo; virtual;
    function GetPainterClass: TdxCustomLayoutItemPainterClass; override;

    property Item: TcxGridCustomLayoutItem read GetItem;
  public
    property ControlViewInfo: TcxGridCustomLayoutItemEditViewInfo read GetControlViewInfo;
    property GridItemViewInfo: TcxGridTableDataCellViewInfo read GetGridItemViewInfo;
  end;

  { TcxGridCustomLayoutItemCaptionOptions }

  TcxGridCustomLayoutItemCaptionOptions = class(TdxLayoutLabeledItemCustomCaptionOptions)
  private
    FGridItemCaption: string;
    function GetItem: TcxGridCustomLayoutItem;{$IFDEF DELPHI9} inline; {$ENDIF}
  protected
    function GetText: string; override;
    function IsTextStored: Boolean; override;
    procedure SetText(const Value: string); override;

    property GridItemCaption: string read FGridItemCaption write FGridItemCaption;
  public
    property Item: TcxGridCustomLayoutItem read GetItem;
  end;

  { TcxGridLayoutCustomItem }

  TcxGridCustomLayoutItem = class(TdxLayoutControlItem)
  private
    FGridViewItem: TcxCustomGridTableItem;
    FLoadedGridViewItemName: string;

    function GetCaptionOptions: TcxGridCustomLayoutItemCaptionOptions; {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetViewInfo: TcxGridCustomLayoutItemViewInfo;
    procedure SetCaptionOptions(Value: TcxGridCustomLayoutItemCaptionOptions); {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure SetGridViewItem(const AValue: TcxCustomGridTableItem);
  protected
    // IcxStoredObject
    function GetStoredProperties(AProperties: TStrings): Boolean; override;
    procedure GetStoredPropertyValue(const AName: string; var AValue: Variant); override;
    procedure SetStoredPropertyValue(const AName: string; const AValue: Variant); override;

    function CanDelete: Boolean; override;
    procedure CustomizationChanged; override;
    function GetBaseName: string; override;
    class function GetCaptionOptionsClass: TdxCustomLayoutItemCaptionOptionsClass; override;
    function GetInplaceRenameCaption: string; override;
    function GetObjectForSelect: TcxGridCustomLayoutItem; virtual;
    function GetViewInfoClass: TdxCustomLayoutItemViewInfoClass; override;
    function HasControl: Boolean; override;
    function IsVisibleForCustomization: Boolean; override;
    procedure SetInplaceRenameCaption(const ACaption: string); override;

    function CaptionToDisplayCaption(AValue: string): string; virtual;
    function DisplayCaptionToCaption(AValue: string): string; virtual;

    property LoadedGridViewItemName: string read FLoadedGridViewItemName;
  public
    procedure Assign(Source: TPersistent); override;

    property CaptionOptions: TcxGridCustomLayoutItemCaptionOptions read GetCaptionOptions write SetCaptionOptions;
    property GridViewItem: TcxCustomGridTableItem read FGridViewItem write SetGridViewItem;
    property ViewInfo: TcxGridCustomLayoutItemViewInfo read GetViewInfo;
  end;

  { TcxGridViewLayoutItemDataCellPainter }

  TcxGridViewLayoutItemDataCellPainter = class(TcxGridTableDataCellPainter)
  private
    function GetViewInfo: TcxGridViewLayoutItemDataCellViewInfo;
  protected
    function CanDrawEditBorder: Boolean; virtual;
    procedure DrawEditBorder;
    procedure DrawBackground; override;
    procedure DrawContent; override;
    procedure DrawText; override;
    function GetFocusRect: TRect; override;

    property ViewInfo: TcxGridViewLayoutItemDataCellViewInfo read GetViewInfo;
  end;

  { TcxGridViewLayoutItemDataCellViewInfo }

  TcxGridViewLayoutItemDataCellViewInfo = class(TcxGridTableDataCellViewInfo)
  private
    procedure CheckReleaseMouseCapture;
    function GetLayoutItemViewInfo: TcxGridCustomLayoutItemViewInfo;
  protected
    procedure CalculateEditViewInfo(AEditViewInfo: TcxCustomEditViewInfo; const AMousePos: TPoint); override;
    function CaptureMouseOnPress: Boolean; override;
    function CanFocus: Boolean; virtual;
    procedure DoCalculateParams; override;
    procedure GetCaptionParams(var AParams: TcxViewParams); virtual;
    function GetLayoutItemViewInfoInstance: TcxGridCustomLayoutItemViewInfo; virtual; abstract;
    function GetPainterClass: TcxCustomGridCellPainterClass; override;
    function GetPaintState: TcxButtonState; virtual;
    function GetTransparent: Boolean; override;
    procedure GetViewParams(var AParams: TcxViewParams); override;
    function GetVisible: Boolean; override;
    function HasFocusRect: Boolean; override;
    function IsValueTransparent: Boolean; virtual;
    procedure StateChanged(APrevState: TcxGridCellState); override;

    property LayoutItemViewInfo: TcxGridCustomLayoutItemViewInfo read GetLayoutItemViewInfo;
  public
    CaptionParams: TcxViewParams;
    procedure Calculate(const ABounds: TRect); override;
    procedure Calculate(ALeftBound, ATopBound: Integer; AWidth: Integer = -1;
      AHeight: Integer = -1); override;
  end;

  { TcxGridLayoutCustomizationFormHelper }

  TcxGridLayoutCustomizationFormHelper = class
  private
    FController: TcxGridCustomLayoutController;
  public
    constructor Create(AController: TcxGridCustomLayoutController);

    procedure CheckCustomizationFormBounds(var R: TRect);
    procedure CustomizationChanged;
    function GetCustomizationFormBounds: TRect;
    procedure InitializeCustomizationForm;
    function ShowModalCustomizationForm: Integer;

    property Controller: TcxGridCustomLayoutController read FController;
  end;

  { TcxGridCustomLayoutController }

  TcxGridCustomLayoutController = class(TcxCustomGridTableController);

  { TcxGridLayoutContainerViewInfo }

  TcxGridLayoutContainerViewInfo = class(TdxLayoutContainerViewInfo)
  private
    procedure PopulateGroup(AValue: TdxCustomLayoutItemViewInfo; AList: TList);
    procedure PopulateItem(AValue: TdxCustomLayoutItemViewInfo; AList: TList);
  protected
    function CanPopulateItem(AValue: TdxCustomLayoutItemViewInfo): Boolean; virtual;
    function CanUseCachedInfo: Boolean; override;
    procedure DoPopulateTabOrderList(AItem: TdxCustomLayoutItemViewInfo; AList: TList); virtual;
    function GetGridViewItem(AItemViewInfo: TdxCustomLayoutItemViewInfo): TcxCustomGridTableItem; virtual;
    function IsItemGroup(AValue: TdxCustomLayoutItemViewInfo): Boolean; virtual;
  public
    function GetGridItemViewInfo(AViewInfo: TcxGridCustomLayoutItemViewInfo): TcxGridTableDataCellViewInfo; virtual;
    procedure PopulateTabOrderList(AList: TList);
  end;

  { TcxGridRecordLayoutContainerViewInfo }

  TcxGridRecordLayoutContainerViewInfo = class(TcxGridLayoutContainerViewInfo)
  private
    FRecordViewInfo: TcxCustomGridRecordViewInfo;

    function GetRecordViewInfo: TcxCustomGridRecordViewInfo;
  protected
    function GetRecordViewInfoInstance: TcxCustomGridRecordViewInfo; virtual;
    function FindGridItemViewInfo(AViewInfo: TcxGridCustomLayoutItemViewInfo): TcxGridTableDataCellViewInfo; virtual; abstract;
    function GetLayoutItem(AViewInfo: TcxGridCustomLayoutItemViewInfo): TdxLayoutControlItem; virtual;
    function GetItemOptions(AViewInfo: TdxCustomLayoutItemViewInfo): TdxCustomLayoutLookAndFeelOptions; override;
    function GetItemLayoutLookAndFeel(AViewInfo: TdxCustomLayoutItemViewInfo): TdxCustomLayoutLookAndFeel; override;
    function IsClone: Boolean; override;
  public
    constructor Create(AContainer: TdxLayoutContainer; ARecordViewInfo: TcxCustomGridRecordViewInfo); reintroduce; virtual;

    function GetGridItemViewInfo(AViewInfo: TcxGridCustomLayoutItemViewInfo): TcxGridTableDataCellViewInfo; override;

    property RecordViewInfo: TcxCustomGridRecordViewInfo read GetRecordViewInfo;
  end;

  { TcxGridCustomLayoutContainer }

  TcxGridCustomLayoutContainer = class(TdxLayoutContainer)
  private
    function GetGridView: TcxCustomGridTableView;
  protected
    procedure CalculateRootAlignment; virtual;
    procedure DoInitialize; override;
    function GetCanvas: TcxCanvas; override;
    function GetImages: TCustomImageList; override;
    function GetItemsOwner: TComponent; override;
    function GetItemsParent: TcxControl; override;
    function GetItemsParentComponent: TComponent; override;
    function NeedStretchRecordHeight: Boolean; virtual;
    function NeedStretchRecordWidth: Boolean; virtual;

    property GridView: TcxCustomGridTableView read GetGridView;
  end;

implementation

uses
  Types, cxGeometry, cxContainer, cxTextEdit, cxGridViewLayoutCustomizationForm;

type
  TdxCustomLayoutItemViewInfoAccess = class(TdxCustomLayoutItemViewInfo);
  TdxCustomLayoutItemAccess = class(TdxCustomLayoutItem);
  TcxCustomGridTableItemAccess = class(TcxCustomGridTableItem);
  TdxLayoutControlItemAccess = class(TdxLayoutControlItem);
  TcxGridTableDataCellViewInfoAccess = class(TcxGridTableDataCellViewInfo);

{ TcxLayoutViewLookAndFeelItemPadding }

function TcxLayoutViewLookAndFeelItemPadding.GetDefaultValue(
  Index: Integer): Integer;
begin
  Result := 2;
end;

{ TcxLayoutLayoutViewLookAndFeelItemOptions }

function TcxLayoutLayoutViewLookAndFeelItemOptions.GetPaddingClass: TdxLayoutLookAndFeelPaddingClass;
begin
  Result := TcxLayoutViewLookAndFeelItemPadding;
end;

{ TcxGridCustomLayoutLookAndFeel }

procedure TcxGridCustomLayoutLookAndFeel.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TcxGridCustomLayoutLookAndFeel.Changed;
begin
  if FLockCount = 0 then
    inherited Changed;
end;

constructor TcxGridCustomLayoutLookAndFeel.Create(
  AGridView: TcxCustomGridTableView);
begin
  inherited Create(nil);
  FGridView := AGridView;
end;

procedure TcxGridCustomLayoutLookAndFeel.EndUpdate;
begin
  Dec(FLockCount);
  Changed;
end;

function TcxGridCustomLayoutLookAndFeel.GetItemOptionsClass: TdxLayoutLookAndFeelItemOptionsClass;
begin
  Result := TcxLayoutLayoutViewLookAndFeelItemOptions;
end;

{ TcxGridCustomLayoutItemCaptionPainter }

procedure TcxGridCustomLayoutItemCaptionPainter.DrawBackground;
begin
//do nothing
end;

{ TcxGridLayoutItemEditPainter }

procedure TcxGridCustomLayoutItemEditPainter.Paint;
begin
  inherited Paint;
  DrawEdit;
end;

procedure TcxGridCustomLayoutItemEditPainter.DrawEdit;
var
  AGridViewItem: TcxCustomGridTableItem;
  AProperties: TcxCustomEditProperties;
  AEditViewData: TcxCustomEditViewData;
  AEditViewInfo: TcxCustomEditViewInfo;
  AEditStyle: TcxEditStyle;
  AEditDrawBounds: TRect;
  AAlphaBitmap, AOpaqueBitmap: TcxBitmap32;
begin
  AGridViewItem := GetGridViewItem;
  AProperties := AGridViewItem.GetProperties;
  AEditStyle := TcxEditStyle.Create(nil, False);
  try
    AEditStyle.LookAndFeel := GetLookAndFeel;
    AEditStyle.ButtonTransparency := ebtNone;
    AEditStyle.BorderStyle := ebsNone;
    AEditViewData := AProperties.CreateViewData(AEditStyle, True, True);
    try
      AEditViewInfo := AProperties.GetViewInfoClass.Create as TcxCustomEditViewInfo;
      try
        AEditViewData.EditValueToDrawValue(Null, AEditViewInfo);
        AEditDrawBounds := ViewInfo.Bounds;
        InflateRect(AEditDrawBounds, 1, 1);
        AAlphaBitmap := TcxBitmap32.CreateSize(AEditDrawBounds);
        AOpaqueBitmap := TcxBitmap32.CreateSize(AEditDrawBounds);
        try
          AEditViewData.Calculate(Canvas, AEditDrawBounds, cxNullPoint, cxmbNone, [], AEditViewInfo, False);
          AOpaqueBitmap.cxCanvas.WindowOrg := AEditDrawBounds.TopLeft;
          AEditViewInfo.Paint(AOpaqueBitmap.cxCanvas);
          AOpaqueBitmap.MakeOpaque;
          AAlphaBitmap.cxCanvas.FillRect(AAlphaBitmap.ClientRect, $100005);
          AAlphaBitmap.SetAlphaChannel(32);
          AOpaqueBitmap.cxCanvas.WindowOrg := cxNullPoint;
          cxAlphaBlend(AOpaqueBitmap.Canvas.Handle, AAlphaBitmap, AOpaqueBitmap.ClientRect, AAlphaBitmap.ClientRect);
          cxBitBlt(Canvas.Handle, AOpaqueBitmap.Canvas.Handle, ViewInfo.Bounds, cxPoint(1, 1), SRCCOPY);
        finally
          AAlphaBitmap.Free;
          AOpaqueBitmap.Free;
        end;
      finally
        AEditViewInfo.Free;
      end;
    finally
      AEditViewData.Free;
    end;
  finally
    AEditStyle.Free;
  end;
end;

function TcxGridCustomLayoutItemEditPainter.GetGridViewItem: TcxCustomGridTableItem;
begin
  Result := ViewInfo.Item.GridViewItem;
end;

function TcxGridCustomLayoutItemEditPainter.GetLookAndFeel: TcxLookAndFeel;
begin
  Result := ViewInfo.GridView.LookAndFeel;
end;

function TcxGridCustomLayoutItemEditPainter.GetViewInfo: TcxGridCustomLayoutItemEditViewInfo;
begin
  Result := TcxGridCustomLayoutItemEditViewInfo(inherited ViewInfo);
end;

{ TcxGridLayoutItemPainter }

function TcxGridCustomLayoutItemPainter.CanPaint: Boolean;
begin
  Result := ViewInfo.Item.Container.Customization;
end;

function TcxGridCustomLayoutItemPainter.GetCaptionPainterClass: TdxCustomLayoutItemCaptionPainterClass;
begin
  Result := TcxGridCustomLayoutItemCaptionPainter;
end;

function TcxGridCustomLayoutItemPainter.GetControlPainterClass: TdxLayoutControlItemControlPainterClass;
begin
  Result := TcxGridCustomLayoutItemEditPainter;
end;

function TcxGridCustomLayoutItemPainter.GetViewInfo: TcxGridCustomLayoutItemViewInfo;
begin
  Result := TcxGridCustomLayoutItemViewInfo(inherited ViewInfo);
end;

{ TcxGridCustomLayoutItemCaptionViewInfo }

function TcxGridCustomLayoutItemCaptionViewInfo.CalculateHeight: Integer;
begin
  Result := inherited CalculateHeight;
  dxAdjustToTouchableSize(Result);
end;

function TcxGridCustomLayoutItemCaptionViewInfo.CalculatePadding: TRect;
begin
  Result := inherited CalculatePadding;
  InflateRect(Result, -1, -1);
end;

function TcxGridCustomLayoutItemCaptionViewInfo.GetItem: TdxLayoutControlItem;
begin
  Result := TdxLayoutControlItem(inherited Item);
end;

function TcxGridCustomLayoutItemCaptionViewInfo.GetItemViewInfo: TcxGridCustomLayoutItemViewInfo;
begin
  Result := TcxGridCustomLayoutItemViewInfo(inherited ItemViewInfo);
end;

function TcxGridCustomLayoutItemCaptionViewInfo.GetTextColor: TColor;
var
  AGridItemViewInfo: TcxGridViewLayoutItemDataCellViewInfo;
begin
  AGridItemViewInfo := TcxGridViewLayoutItemDataCellViewInfo(ItemViewInfo.GridItemViewInfo);
  if AGridItemViewInfo <> nil then
    Result := AGridItemViewInfo.CaptionParams.TextColor
  else
    Result := inherited GetTextColor;
end;

{ TcxGridCustomLayoutItemEditViewInfo }

procedure TcxGridCustomLayoutItemEditViewInfo.CalculateInternalTabOrder(var AAvailTabOrder: Integer);
begin
  ItemViewInfo.TabOrderIndex := AAvailTabOrder;
  Inc(AAvailTabOrder);
end;

function TcxGridCustomLayoutItemEditViewInfo.CalculateMinHeight: Integer;
begin
  if TdxLayoutControlItemAccess(Item).ControlOptions.IsHeightFixed then
    Result := CalculateHeight
  else
    if Visible then
      Result := GetDefaultValueHeight
    else
      Result := 0;
end;

function TcxGridCustomLayoutItemEditViewInfo.CalculateMinWidth: Integer;
begin
  if TdxLayoutControlItemAccess(Item).ControlOptions.IsWidthFixed then
    Result := CalculateWidth
  else
    if Visible then
      Result := GetControlAreaWidth(GetMinValueWidth)
    else
      Result := 0;
end;

function TcxGridCustomLayoutItemEditViewInfo.HasBorder: Boolean;
begin
  Result := False;
end;

function TcxGridCustomLayoutItemEditViewInfo.GetMinValueWidth: Integer;
begin
  Result := 0;
end;

function TcxGridCustomLayoutItemEditViewInfo.GetOriginalControlSize: TSize;
begin
  Result := cxSize(GetMinValueWidth, GetValueHeight);
end;

function TcxGridCustomLayoutItemEditViewInfo.GetValueHeight: Integer;
begin
  if FDataHeight = 0 then
  begin
    if DataViewInfo <> nil then
      FDataHeight := TcxGridTableDataCellViewInfoAccess(DataViewInfo).CalculateHeight
    else
      FDataHeight := GetDefaultValueHeight;
  end;
  dxAdjustToTouchableSize(FDataHeight);
  Result := FDataHeight;
end;

function TcxGridCustomLayoutItemEditViewInfo.GetDefaultValueHeight: Integer;
begin
  Result := 0;
end;

function TcxGridCustomLayoutItemEditViewInfo.GetDataViewInfo: TcxGridTableDataCellViewInfo;
begin
  Result := ItemViewInfo.GridItemViewInfo;
end;

function TcxGridCustomLayoutItemEditViewInfo.GetItem: TcxGridCustomLayoutItem;
begin
  Result := TcxGridCustomLayoutItem(inherited Item);
end;

function TcxGridCustomLayoutItemEditViewInfo.GetItemViewInfo: TcxGridCustomLayoutItemViewInfo;
begin
  Result := TcxGridCustomLayoutItemViewInfo(inherited ItemViewInfo);
end;

function TcxGridCustomLayoutItemEditViewInfo.GetGridView: TcxCustomGridTableView;
begin
  Result := Item.GridViewItem.GridView;
end;

{ TcxGridLayoutItemViewInfo }

function TcxGridCustomLayoutItemViewInfo.GetCaptionViewInfoClass: TdxCustomLayoutItemCaptionViewInfoClass;
begin
  Result := TcxGridCustomLayoutItemCaptionViewInfo;
end;

function TcxGridCustomLayoutItemViewInfo.GetControlViewInfoClass: TdxLayoutControlItemControlViewInfoClass;
begin
  Result := TcxGridCustomLayoutItemEditViewInfo;
end;

function TcxGridCustomLayoutItemViewInfo.GetCurrentGridItemViewInfo: TcxGridTableDataCellViewInfo;
begin
  Result := nil;
end;

function TcxGridCustomLayoutItemViewInfo.GetPainterClass: TdxCustomLayoutItemPainterClass;
begin
  Result := TcxGridCustomLayoutItemPainter;
end;

function TcxGridCustomLayoutItemViewInfo.GetControlViewInfo: TcxGridCustomLayoutItemEditViewInfo;
begin
  Result := TcxGridCustomLayoutItemEditViewInfo(inherited ControlViewInfo);
end;

function TcxGridCustomLayoutItemViewInfo.GetGridItemViewInfo: TcxGridTableDataCellViewInfo;
begin
  if FGridItemViewInfo = nil then
    FGridItemViewInfo := GetCurrentGridItemViewInfo;
  Result := FGridItemViewInfo;
end;

function TcxGridCustomLayoutItemViewInfo.GetItem: TcxGridCustomLayoutItem;
begin
  Result := TcxGridCustomLayoutItem(inherited Item);
end;

{ TcxGridCustomLayoutItemCaptionOptions }

function TcxGridCustomLayoutItemCaptionOptions.GetText: string;
begin
  if Item.Customization then
    Result := inherited GetText
  else
  begin
    if Item.GridViewItem <> nil then
      Result := Item.GridViewItem.VisibleCaption
    else
      Result := '';
  end;
end;

function TcxGridCustomLayoutItemCaptionOptions.IsTextStored: Boolean;
begin
  Result := False;
end;

procedure TcxGridCustomLayoutItemCaptionOptions.SetText(const Value: string);
begin
  inherited SetText(Value);
  if IsRestoring and (Item.GridViewItem <> nil) then
    GridItemCaption := Item.DisplayCaptionToCaption(Value);
end;

function TcxGridCustomLayoutItemCaptionOptions.GetItem: TcxGridCustomLayoutItem;
begin
  Result := TcxGridCustomLayoutItem(inherited Item);
end;


{ TcxGridLayoutCustomItem }

procedure TcxGridCustomLayoutItem.Assign(Source: TPersistent);
var
  ASource: TcxGridCustomLayoutItem;
begin
  inherited Assign(Source);
  if (Source is TcxGridCustomLayoutItem) then
  begin
    ASource := TcxGridCustomLayoutItem(Source);
    case Container.AssignLayoutMode of
      almFromParent:
        GridViewItem := ASource.GridViewItem;
      almToParent:
        if GridViewItem = nil then
          GridViewItem := ASource.GridViewItem
        else
          GridViewItem.Caption := ASource.CaptionOptions.GridItemCaption;
    end;
  end;
end;

function TcxGridCustomLayoutItem.GetStoredProperties(AProperties: TStrings): Boolean;
begin
  Result := inherited GetStoredProperties(AProperties);
  AProperties.Add('GridViewItem');
end;

procedure TcxGridCustomLayoutItem.GetStoredPropertyValue(const AName: string; var AValue: Variant);
begin
  if AName = 'GridViewItem' then
    AValue := TcxCustomGridTableItemAccess(GridViewItem).GetObjectName
  else
    inherited GetStoredPropertyValue(AName, AValue);
end;

procedure TcxGridCustomLayoutItem.SetStoredPropertyValue(const AName: string; const AValue: Variant);
begin
  if AName = 'GridViewItem' then
    FLoadedGridViewItemName := AValue
  else
    inherited SetStoredPropertyValue(AName, AValue);
end;

function TcxGridCustomLayoutItem.CanDelete: Boolean;
begin
  Result := False;
end;

procedure TcxGridCustomLayoutItem.CustomizationChanged;
begin
  inherited CustomizationChanged;
  if Customization and not IsDestroying and (GridViewItem <> nil) then
  begin
    CaptionOptions.GridItemCaption := GridViewItem.Caption;
    Caption := GridViewItem.VisibleCaption;
  end;
end;

function TcxGridCustomLayoutItem.GetBaseName: string;
begin
  Result := inherited GetBaseName + 'LayoutItem';
end;

class function TcxGridCustomLayoutItem.GetCaptionOptionsClass: TdxCustomLayoutItemCaptionOptionsClass;
begin
  Result := TcxGridCustomLayoutItemCaptionOptions;
end;

function TcxGridCustomLayoutItem.GetInplaceRenameCaption: string;
begin
  Result := CaptionOptions.GridItemCaption;
end;

function TcxGridCustomLayoutItem.GetObjectForSelect: TcxGridCustomLayoutItem;
begin
  if Data <> nil then
    Result := TcxGridCustomLayoutItem(Data).GetObjectForSelect
  else
    Result := Self;
end;

function TcxGridCustomLayoutItem.GetViewInfoClass: TdxCustomLayoutItemViewInfoClass;
begin
  Result := TcxGridCustomLayoutItemViewInfo;
end;

function TcxGridCustomLayoutItem.HasControl: Boolean;
begin
  Result := GridViewItem <> nil;
end;

function TcxGridCustomLayoutItem.IsVisibleForCustomization: Boolean;
begin
  Result := inherited IsVisibleForCustomization and (GridViewItem <> nil);
end;

procedure TcxGridCustomLayoutItem.SetInplaceRenameCaption(const ACaption: string);
begin
  CaptionOptions.GridItemCaption := ACaption;
  inherited SetInplaceRenameCaption(CaptionToDisplayCaption(ACaption));
end;

function TcxGridCustomLayoutItem.CaptionToDisplayCaption(AValue: string): string;
begin
  Result := AValue;
end;

function TcxGridCustomLayoutItem.DisplayCaptionToCaption(AValue: string): string;
begin
  Result := AValue;
end;

function TcxGridCustomLayoutItem.GetCaptionOptions: TcxGridCustomLayoutItemCaptionOptions;
begin
  Result := TcxGridCustomLayoutItemCaptionOptions(inherited CaptionOptions);
end;

function TcxGridCustomLayoutItem.GetViewInfo: TcxGridCustomLayoutItemViewInfo;
begin
  Result := TcxGridCustomLayoutItemViewInfo(inherited ViewInfo);
end;

procedure TcxGridCustomLayoutItem.SetCaptionOptions(Value: TcxGridCustomLayoutItemCaptionOptions);
begin
  inherited CaptionOptions := Value;
end;

procedure TcxGridCustomLayoutItem.SetGridViewItem(const AValue: TcxCustomGridTableItem);
begin
  if GridViewItem <> AValue then
  begin
    FGridViewItem := AValue;
    Changed;
  end;
end;

{ TcxGridViewLayoutItemDataCellPainter }

function TcxGridViewLayoutItemDataCellPainter.CanDrawEditBorder: Boolean;
begin
  Result := not ViewInfo.InvalidateOnStateChange;
end;

procedure TcxGridViewLayoutItemDataCellPainter.DrawEditBorder;
var
  ABounds: TRect;
begin
  ABounds := cxRectInflate(ViewInfo.EditViewInfo.Bounds, 1, 1);
  ViewInfo.LookAndFeelPainter.DrawBorder(Canvas, ABounds);
end;

procedure TcxGridViewLayoutItemDataCellPainter.DrawBackground;
var
  AState: TcxButtonState;
begin
  if ViewInfo.InvalidateOnStateChange then
  begin
    AState := ViewInfo.GetPaintState;
    if AState <> cxbsNormal then
      ViewInfo.LookAndFeelPainter.LayoutViewDrawItem(Canvas, ViewInfo.Bounds, AState);
  end;
end;

procedure TcxGridViewLayoutItemDataCellPainter.DrawContent;
begin
  //if ViewInfo.Transparent and (ViewInfo.BackgroundBitmap <> nil) then
    DrawBackground;
  DrawText;
  ViewInfo.EditViewInfo.Paint(Canvas);
  if CanDrawEditBorder then
    DrawEditBorder;
end;

procedure TcxGridViewLayoutItemDataCellPainter.DrawText;
begin
  with TdxCustomLayoutItemCaptionPainter.Create(Canvas, ViewInfo.LayoutItemViewInfo.CaptionViewInfo) do
  try
    Paint;
  finally
    Free;
  end;
end;

function TcxGridViewLayoutItemDataCellPainter.GetFocusRect: TRect;
begin
  if ViewInfo.InvalidateOnStateChange then
    Result := cxRectInflate(ViewInfo.ContentBounds, -2, -2)
  else
    Result := cxRectInflate(ViewInfo.EditViewInfo.Bounds, -1, -1);
end;

function TcxGridViewLayoutItemDataCellPainter.GetViewInfo: TcxGridViewLayoutItemDataCellViewInfo;
begin
  Result := TcxGridViewLayoutItemDataCellViewInfo(inherited ViewInfo);
end;

{ TcxGridViewLayoutItemDataCellViewInfo }

procedure TcxGridViewLayoutItemDataCellViewInfo.Calculate(ALeftBound, ATopBound,
  AWidth, AHeight: Integer);
begin
  if LayoutItemViewInfo = nil then Exit;
  inherited Calculate(ALeftBound, ATopBound, AWidth, AHeight);
end;

procedure TcxGridViewLayoutItemDataCellViewInfo.Calculate(const ABounds: TRect);
begin
  if LayoutItemViewInfo = nil then Exit;
  inherited Calculate(LayoutItemViewInfo.OriginalBounds);
end;

procedure TcxGridViewLayoutItemDataCellViewInfo.CalculateEditViewInfo(
  AEditViewInfo: TcxCustomEditViewInfo; const AMousePos: TPoint);
begin
  inherited CalculateEditViewInfo(AEditViewInfo, AMousePos);
  EditViewInfo.Transparent := IsValueTransparent;
  if EditViewInfo.Transparent then
    EditViewInfo.TextColor := CaptionParams.TextColor;
end;

function TcxGridViewLayoutItemDataCellViewInfo.CaptureMouseOnPress: Boolean;
begin
  Result := True;
end;

function TcxGridViewLayoutItemDataCellViewInfo.CanFocus: Boolean;
begin
  Result := Visible and LayoutItemViewInfo.Item.CanFocusControl;
end;

procedure TcxGridViewLayoutItemDataCellViewInfo.DoCalculateParams;
begin
  inherited DoCalculateParams;
  GetCaptionParams(CaptionParams);
end;

procedure TcxGridViewLayoutItemDataCellViewInfo.GetCaptionParams(
  var AParams: TcxViewParams);
begin
//do nothing
end;

function TcxGridViewLayoutItemDataCellViewInfo.GetPainterClass: TcxCustomGridCellPainterClass;
begin
  Result := TcxGridViewLayoutItemDataCellPainter;
end;

function TcxGridViewLayoutItemDataCellViewInfo.GetPaintState: TcxButtonState;
var
  AIsHotTrack: Boolean;
begin
  AIsHotTrack := GetHotTrack;
  if Focused then
  begin
    if State = gcsPressed then
      Result := cxbsPressed
    else
      if (State = gcsSelected) and AIsHotTrack then
        Result := cxbsHot
      else
        if GridView.IsControlFocused then
          Result := cxbsPressed
        else
          Result := cxbsDisabled
  end
  else
    if AIsHotTrack then
      Result := ButtonState
    else
      Result := cxbsNormal;
end;

function TcxGridViewLayoutItemDataCellViewInfo.GetTransparent: Boolean;
begin
  Result := False; //inherited GetTransparent or (Params.Color = clDefault);
end;

procedure TcxGridViewLayoutItemDataCellViewInfo.GetViewParams(
  var AParams: TcxViewParams);
begin
  GridView.Styles.GetDataCellParams(GridRecord, Item, Params, True, Self, True);
end;

function TcxGridViewLayoutItemDataCellViewInfo.GetVisible: Boolean;
begin
  Result := inherited GetVisible and (LayoutItemViewInfo <> nil) and
    LayoutItemViewInfo.ActuallyVisible;
end;

function TcxGridViewLayoutItemDataCellViewInfo.HasFocusRect: Boolean;
begin
  Result := True;
end;

function TcxGridViewLayoutItemDataCellViewInfo.IsValueTransparent: Boolean;
begin
  Result := InvalidateOnStateChange and (State = gcsSelected) and
    not (Focused and GridView.Controller.IsEditing);
end;

procedure TcxGridViewLayoutItemDataCellViewInfo.StateChanged(APrevState: TcxGridCellState);
begin
  CalculateParamsNeeded;
  inherited StateChanged(APrevState);
  CheckReleaseMouseCapture;
end;

procedure TcxGridViewLayoutItemDataCellViewInfo.CheckReleaseMouseCapture;
begin
  if GridView.OptionsBehavior.AlwaysShowEditor and (State = gcsPressed) then
    MouseCapture := False;
end;

function TcxGridViewLayoutItemDataCellViewInfo.GetLayoutItemViewInfo: TcxGridCustomLayoutItemViewInfo;
begin
  Result := GetLayoutItemViewInfoInstance;
end;

{ TcxGridLayoutCustomizationFormHelper }

constructor TcxGridLayoutCustomizationFormHelper.Create(
  AController: TcxGridCustomLayoutController);
begin
  inherited Create;
  FController := AController;
end;

procedure TcxGridLayoutCustomizationFormHelper.CheckCustomizationFormBounds(
  var R: TRect);
begin
  R := cxRectCenter(Screen.WorkAreaRect, R.Right - R.Left, R.Bottom - R.Top);
end;

procedure TcxGridLayoutCustomizationFormHelper.CustomizationChanged;
begin
  if Controller.Customization then
  begin
    Controller.DoCreateCustomizationForm;
    Controller.DoCustomization;
    if ShowModalCustomizationForm = mrOk then
      (Controller.CustomizationForm as TcxGridViewLayoutCustomizationForm).ApplyChanges
    else
      (Controller.CustomizationForm as TcxGridViewLayoutCustomizationForm).CancelChanges;
    Controller.Customization := False;
  end
  else
  begin
    Controller.HideCustomizationForm;
    Controller.DoCustomization;
  end;
end;

function TcxGridLayoutCustomizationFormHelper.GetCustomizationFormBounds: TRect;
begin
  if IsRectEmpty(Controller.CustomizationFormBounds) then
  begin
    Result := cxRect(0, 0, Controller.GetCustomizationFormDefaultWidth, Controller.GetCustomizationFormDefaultHeight);
    Controller.CheckCustomizationFormBounds(Result);
  end
  else
    Result := Controller.CustomizationFormBounds;
end;

procedure TcxGridLayoutCustomizationFormHelper.InitializeCustomizationForm;
var
  AGridCustomizationForm: IcxGridCustomizationForm;
begin
  AGridCustomizationForm := Controller.ICustomizationForm;
  if AGridCustomizationForm <> nil then
    AGridCustomizationForm.Initialize(Controller);
end;

function TcxGridLayoutCustomizationFormHelper.ShowModalCustomizationForm: Integer;
begin
  Result := Controller.CustomizationForm.ShowModal;
end;

{ TcxGridCustomLayoutContainerViewInfo }

function TcxGridLayoutContainerViewInfo.GetGridItemViewInfo(
  AViewInfo: TcxGridCustomLayoutItemViewInfo): TcxGridTableDataCellViewInfo;
begin
  Result := nil;
end;

procedure TcxGridLayoutContainerViewInfo.PopulateTabOrderList(AList: TList);
begin
  DoPopulateTabOrderList(ItemsViewInfo, AList);
end;

function TcxGridLayoutContainerViewInfo.CanPopulateItem(AValue: TdxCustomLayoutItemViewInfo): Boolean;
var
  AItemViewInfo: TcxGridCustomLayoutItemViewInfo;
begin
  Result := AValue is TcxGridCustomLayoutItemViewInfo;
  if Result then
  begin
    AItemViewInfo := TcxGridCustomLayoutItemViewInfo(AValue);
    Result := AItemViewInfo.ActuallyVisible and AItemViewInfo.Enabled;
  end;
end;

function TcxGridLayoutContainerViewInfo.CanUseCachedInfo: Boolean;
begin
  Result := False;
end;

procedure TcxGridLayoutContainerViewInfo.DoPopulateTabOrderList(AItem: TdxCustomLayoutItemViewInfo; AList: TList);
begin
  if IsItemGroup(AItem) then
    PopulateGroup(AItem, AList)
  else
    if CanPopulateItem(AItem) then
      PopulateItem(AItem, AList);
end;

function TcxGridLayoutContainerViewInfo.GetGridViewItem(
  AItemViewInfo: TdxCustomLayoutItemViewInfo): TcxCustomGridTableItem;
begin
  Result := nil;
  if CanPopulateItem(AItemViewInfo) then
    Result := TcxGridCustomLayoutItemViewInfo(AItemViewInfo).Item.GridViewItem
end;

function TcxGridLayoutContainerViewInfo.IsItemGroup(
  AValue: TdxCustomLayoutItemViewInfo): Boolean;
begin
  Result := AValue is TdxLayoutGroupViewInfo;
end;

procedure TcxGridLayoutContainerViewInfo.PopulateGroup(AValue: TdxCustomLayoutItemViewInfo; AList: TList);
var
  AGroupViewInfo: TdxLayoutGroupViewInfo;
  I: Integer;
begin
  AGroupViewInfo := TdxLayoutGroupViewInfo(AValue);
  for I := 0 to AGroupViewInfo.ItemViewInfoCount - 1 do
    DoPopulateTabOrderList(AGroupViewInfo.ItemViewInfos[I], AList);
end;

procedure TcxGridLayoutContainerViewInfo.PopulateItem(AValue: TdxCustomLayoutItemViewInfo; AList: TList);
begin
  AList.Add(GetGridViewItem(AValue));
end;

{ TcxGridRecordLayoutContainerViewInfo }

constructor TcxGridRecordLayoutContainerViewInfo.Create(
  AContainer: TdxLayoutContainer; ARecordViewInfo: TcxCustomGridRecordViewInfo);
begin
  inherited Create(AContainer);
  FRecordViewInfo := ARecordViewInfo;
end;

function TcxGridRecordLayoutContainerViewInfo.GetGridItemViewInfo(AViewInfo: TcxGridCustomLayoutItemViewInfo): TcxGridTableDataCellViewInfo;
begin
  if RecordViewInfo = nil then
    Result := inherited GetGridItemViewInfo(AViewInfo)
  else
  begin
    Result := FindGridItemViewInfo(AViewInfo);
  end;
end;

function TcxGridRecordLayoutContainerViewInfo.GetRecordViewInfoInstance: TcxCustomGridRecordViewInfo;
begin
  Result := FRecordViewInfo;
end;

function TcxGridRecordLayoutContainerViewInfo.GetLayoutItem(
  AViewInfo: TcxGridCustomLayoutItemViewInfo): TdxLayoutControlItem;
begin
  Result := AViewInfo.Item;
end;

function TcxGridRecordLayoutContainerViewInfo.GetRecordViewInfo: TcxCustomGridRecordViewInfo;
begin
  Result := GetRecordViewInfoInstance;
end;

function TcxGridRecordLayoutContainerViewInfo.GetItemOptions(AViewInfo: TdxCustomLayoutItemViewInfo): TdxCustomLayoutLookAndFeelOptions;
begin
  if TdxCustomLayoutItemAccess(TdxCustomLayoutItemViewInfoAccess(AViewInfo).Item).IsGroup then
    Result := GetLayoutLookAndFeel.GroupOptions
  else
    Result := GetLayoutLookAndFeel.ItemOptions;
end;

function TcxGridRecordLayoutContainerViewInfo.GetItemLayoutLookAndFeel(
  AViewInfo: TdxCustomLayoutItemViewInfo): TdxCustomLayoutLookAndFeel;
begin
  Result := GetLayoutLookAndFeel;
end;

function TcxGridRecordLayoutContainerViewInfo.IsClone: Boolean;
begin
  Result := True;
end;

{ TcxGridCustomLayoutContainer }

procedure TcxGridCustomLayoutContainer.CalculateRootAlignment;
const
  AlignHorzMap: array[Boolean] of TdxLayoutAlignHorz = (ahLeft, ahClient);
  AlignVertMap: array[Boolean] of TdxLayoutAlignVert = (avTop, avClient);
begin
  BeginUpdate;
  try
    Root.AlignHorz := AlignHorzMap[NeedStretchRecordWidth];
    Root.AlignVert := AlignVertMap[NeedStretchRecordHeight];
  finally
    EndUpdate(False);
  end;
end;

procedure TcxGridCustomLayoutContainer.DoInitialize;
begin
  inherited DoInitialize;
  HighlightRoot := False;
end;

function TcxGridCustomLayoutContainer.GetCanvas: TcxCanvas;
begin
  Result := GridView.ViewInfo.Canvas;
end;

function TcxGridCustomLayoutContainer.GetGridView: TcxCustomGridTableView;
begin
  Result := TcxCustomGridTableView(Owner);
end;

function TcxGridCustomLayoutContainer.GetImages: TCustomImageList;
begin
  Result := GridView.GetImages;
end;

function TcxGridCustomLayoutContainer.GetItemsOwner: TComponent;
begin
  Result := GridView.Owner;
end;

function TcxGridCustomLayoutContainer.GetItemsParent: TcxControl;
begin
  Result := GridView.Site
end;

function TcxGridCustomLayoutContainer.GetItemsParentComponent: TComponent;
begin
  Result := GridView;
end;

function TcxGridCustomLayoutContainer.NeedStretchRecordHeight: Boolean;
begin
  Result := False;
end;

function TcxGridCustomLayoutContainer.NeedStretchRecordWidth: Boolean;
begin
  Result := False;
end;

initialization
  RegisterClasses([TcxGridCustomLayoutItem]);

end.
