{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressFlowChart                                         }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSFLOWCHART AND ALL ACCOMPANYING }
{   VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.              }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE end USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxFcStrs;

interface

uses
  dxflchrt;

{$I cxVer.inc}

resourcestring
   sdxFlowChartArrowStyleNone = '无';
  sdxFlowChartArrowStyleArrow = '箭头';
  sdxFlowChartArrowStyleEllipseArrow = '椭圆箭头';
  sdxFlowChartArrowStyleRectArrow = '矩形箭头';

  sdxFlowChartConnectionEditorArrowColor = '箭头颜色';
  sdxFlowChartConnectionEditorArrowSize = '箭头大小';
  sdxFlowChartConnectionEditorArrowStyle = '箭头样式';
  sdxFlowChartConnectionEditorCaption = '编辑链接';
  sdxFlowChartConnectionEditorColor = '颜色';
  sdxFlowChartConnectionEditorDestination = '目标';
  sdxFlowChartConnectionEditorLinkedPoint = '链接点';
  sdxFlowChartConnectionEditorSource = '来源';
  sdxFlowChartConnectionEditorText = '文本';
  sdxFlowChartConnectionEditorTextFontHint = '文本字体';

  sdxFlowChartDialogButtonOk = '确定(&O)';
  sdxFlowChartDialogButtonCancel = '取消(&C)';

  sdxFlowChartBorderStyleAdjust = '调整';
  sdxFlowChartBorderStyleBottom = '下';
  sdxFlowChartBorderStyleDiagonal = '对角线';
  sdxFlowChartBorderStyleFlat = '平面';
  sdxFlowChartBorderStyleLeft = '左';
  sdxFlowChartBorderStyleMiddle = '居中';
  sdxFlowChartBorderStyleMono = '单一';
  sdxFlowChartBorderStyleRight = '右';
  sdxFlowChartBorderStyleSoft = '软';
  sdxFlowChartBorderStyleTop = '上';

  sdxFlowChartEdgeStyleRaisedIn = '凸进';
  sdxFlowChartEdgeStyleRaisedOut = '凸出';
  sdxFlowChartEdgeStyleSunkenIn = '凹进';
  sdxFlowChartEdgeStyleSunkenOut = '凹出';

  sdxFlowChartEditorChildItem = '%s 的子项目';
  sdxFlowChartEditorConnection = '连接';
  sdxFlowChartEditorConnectionArrowDestinationHint = '目标箭头';
  sdxFlowChartEditorConnectionArrowDestinationSizeHint = '目标箭头大小';
  sdxFlowChartEditorConnectionArrowSourceHint = '源箭头';
  sdxFlowChartEditorConnectionArrowSourceSizeHint = '源箭头大小';
  sdxFlowChartEditorConnectionLinkedPointDestinationHint = '链接的目标点的对象';
  sdxFlowChartEditorConnectionLinkedPointSourceHint = '链接的点源对象';
  sdxFlowChartEditorConnectionStyleHint = '线条样式';
  sdxFlowChartEditorConnectionTextFontHint = '文本字体';
  sdxFlowChartEditorCreate = '创建';
  sdxFlowChartEditorCreateConnectionHint = '链接';
  sdxFlowChartEditorCreateObjectHint = '对象';
  sdxFlowChartEditorEdit = '编辑(&E)';
  sdxFlowChartEditorEditBringToFront = '置前(&F)';
  sdxFlowChartEditorEditClearSelection = '取消选择(&E)';
  sdxFlowChartEditorEditCopy = '复制(&C)';
  sdxFlowChartEditorEditCut = '剪切(&T)';
  sdxFlowChartEditorEditDelete = '删除(&D)';
  sdxFlowChartEditorEditPaste = '粘贴(&P)';
  sdxFlowChartEditorEditSelectAll = '全选(&L)';
  sdxFlowChartEditorEditSendToBack = '置后(&B)';
  sdxFlowChartEditorEditUndo = '撤销(&U)';
  sdxFlowChartEditorFile = '文件(&F)';
  sdxFlowChartEditorFileOpen = '打开(&O)';
  sdxFlowChartEditorFileSave = '另存为(&A)...';
  sdxFlowChartEditorFitHint = '合适';
  sdxFlowChartEditorHelp = '帮助(&H)';
  sdxFlowChartEditorHelpContents = '内容(&C)';
  sdxFlowChartEditorMainItemOfUnion = '%d 联合的主要项目';
  sdxFlowChartEditorObject = '对象';
  sdxFlowChartEditorObjectImagePositionHint = '图像位置';
  sdxFlowChartEditorObjectLineWidthHint = '线宽';
  sdxFlowChartEditorObjectShapeStyleHint = '形状样式';
  sdxFlowChartEditorObjectTextFontHint = '文本字体';
  sdxFlowChartEditorObjectTextPositionHint = '文本位置';
  sdxFlowChartEditorOptions = '选项(&O)';
  sdxFlowChartEditorOptionsDynamicMoving = '动态移动(&M)';
  sdxFlowChartEditorOptionsDynamicSizing = '动态调整大小(&S)';
  sdxFlowChartEditorPixels = '%d 像素。';
  sdxFlowChartEditorPoint = '%d 点';
  sdxFlowChartEditorProperties = '属性(&P)';
  sdxFlowChartEditorUnions = '联合(&U)';
  sdxFlowChartEditorUnionsAdd = '添加到联合';
  sdxFlowChartEditorUnionsClear = '清除联合';
  sdxFlowChartEditorUnionsClearAll = '清除所有联合';
  sdxFlowChartEditorUnionsNew = '新建联合';
  sdxFlowChartEditorUnionsRemove = '从联合中删除';
  sdxFlowChartEditorView = '视图(&V)';
  sdxFlowChartEditorViewActualSize = '实际尺寸(&A)';
  sdxFlowChartEditorViewAntialiasing = '消除锯齿(&A)';
  sdxFlowChartEditorViewFit = '合适(&F)';
  sdxFlowChartEditorViewZoomIn = '放大(&I)';
  sdxFlowChartEditorViewZoomOut = '缩小(&O)';
  sdxFlowChartEditorZoomHint = '缩放';

  sdxFlowChartObjectEditorBackgroundColor = '背景颜色';
  sdxFlowChartObjectEditorBorderStyle = '边框样式';
  sdxFlowChartObjectEditorCaption = '编辑对象';
  sdxFlowChartObjectEditorEdgeStyle = '边缘样式';
  sdxFlowChartObjectEditorFrameTab = '框架';
  sdxFlowChartObjectEditorGeneralTab = '常规';
  sdxFlowChartObjectEditorHeight = '高';
  sdxFlowChartObjectEditorImageClear = '清除图像';
  sdxFlowChartObjectEditorImageLayout = '图像布局';
  sdxFlowChartObjectEditorImageTab = '图像';
  sdxFlowChartObjectEditorLineWidth = '线宽';
  sdxFlowChartObjectEditorShapeColor = '形状颜色';
  sdxFlowChartObjectEditorShapeType = '形状类型';
  sdxFlowChartObjectEditorText = '文本';
  sdxFlowChartObjectEditorTextLayout = '文本布局';
  sdxFlowChartObjectEditorTransparent = '透明';
  sdxFlowChartObjectEditorWidth = '宽';

  sdxFlowChartLayoutTopLeft = '左上角';
  sdxFlowChartLayoutTop = '上';
  sdxFlowChartLayoutTopRight = '右上角';
  sdxFlowChartLayoutLeft = '左';
  sdxFlowChartLayoutCenter = '居中';
  sdxFlowChartLayoutRight = '右';
  sdxFlowChartLayoutBottomLeft = '左下角';
  sdxFlowChartLayoutBottom = '下';
  sdxFlowChartLayoutBottomRight = '右下角';

  sdxFlowChartShapeTypeNone = '无';
  sdxFlowChartShapeTypeRect = '矩形';
  sdxFlowChartShapeTypeEllipse = '椭圆';
  sdxFlowChartShapeTypeRoundRect = '圆角矩形';
  sdxFlowChartShapeTypeDiamond = '菱形';
  sdxFlowChartShapeTypeNorthTriangle = '向北三角形';
  sdxFlowChartShapeTypeSouthTriangle = '南三角';
  sdxFlowChartShapeTypeEastTriangle = '向东三角形';
  sdxFlowChartShapeTypeWestTriangle = '西三角';
  sdxFlowChartShapeTypeHexagon = '六边形';

  sdxFlowChartUnion = '联合';
  sdxFlowChartUnions = '联合';
  sdxFlowChartUnionEditorCaption = '选择联合';

  sdxFlowChartConnectionStyleStraight = '直';
  sdxFlowChartConnectionStyleCurved = '曲线';
  sdxFlowChartConnectionStyleRectHorizontal = '矩形水平';
  sdxFlowChartConnectionStyleRectVertical = '矩形垂直';
const
  dxFlowChartArrowStyleNamesMap: array [TdxFcaType] of Pointer = (
    @sdxFlowChartArrowStyleNone, @sdxFlowChartArrowStyleArrow,
    @sdxFlowChartArrowStyleEllipseArrow, @sdxFlowChartArrowStyleRectArrow);

  dxFlowChartConnectionStyleNamesMap: array[TdxFclStyle] of Pointer = (
    @sdxFlowChartConnectionStyleStraight, @sdxFlowChartConnectionStyleCurved,
    @sdxFlowChartConnectionStyleRectHorizontal, @sdxFlowChartConnectionStyleRectVertical);

  dxFlowChartShapeNamesMap: array[TdxFcShapeType] of Pointer = (
    @sdxFlowChartShapeTypeNone, @sdxFlowChartShapeTypeRect,
    @sdxFlowChartShapeTypeEllipse, @sdxFlowChartShapeTypeRoundRect,
    @sdxFlowChartShapeTypeDiamond, @sdxFlowChartShapeTypeNorthTriangle,
    @sdxFlowChartShapeTypeSouthTriangle, @sdxFlowChartShapeTypeEastTriangle,
    @sdxFlowChartShapeTypeWestTriangle, @sdxFlowChartShapeTypeHexagon, nil);

  dxFlowChartLayoutNamesMap: array[0..8] of Pointer = (
    @sdxFlowChartLayoutTopLeft, @sdxFlowChartLayoutTop, @sdxFlowChartLayoutTopRight,
    @sdxFlowChartLayoutLeft, @sdxFlowChartLayoutCenter, @sdxFlowChartLayoutRight,
    @sdxFlowChartLayoutBottomLeft, @sdxFlowChartLayoutBottom, @sdxFlowChartLayoutBottomRight);

implementation

uses
  dxCore;

procedure AddFlowChartResourceStringNames(AProduct: TdxProductResourceStrings);
begin
  AProduct.Add('sdxFlowChartArrowStyleNone', @sdxFlowChartArrowStyleNone);
  AProduct.Add('sdxFlowChartArrowStyleArrow', @sdxFlowChartArrowStyleArrow);
  AProduct.Add('sdxFlowChartArrowStyleEllipseArrow', @sdxFlowChartArrowStyleEllipseArrow);
  AProduct.Add('sdxFlowChartArrowStyleRectArrow', @sdxFlowChartArrowStyleRectArrow);

  AProduct.Add('sdxFlowChartConnectionEditorArrowColor', @sdxFlowChartConnectionEditorArrowColor);
  AProduct.Add('sdxFlowChartConnectionEditorArrowSize', @sdxFlowChartConnectionEditorArrowSize);
  AProduct.Add('sdxFlowChartConnectionEditorArrowStyle', @sdxFlowChartConnectionEditorArrowStyle);
  AProduct.Add('sdxFlowChartConnectionEditorCaption', @sdxFlowChartConnectionEditorCaption);
  AProduct.Add('sdxFlowChartConnectionEditorColor', @sdxFlowChartConnectionEditorColor);
  AProduct.Add('sdxFlowChartConnectionEditorDestination', @sdxFlowChartConnectionEditorDestination);
  AProduct.Add('sdxFlowChartConnectionEditorLinkedPoint', @sdxFlowChartConnectionEditorLinkedPoint);
  AProduct.Add('sdxFlowChartConnectionEditorSource', @sdxFlowChartConnectionEditorSource);
  AProduct.Add('sdxFlowChartConnectionEditorText', @sdxFlowChartConnectionEditorText);
  AProduct.Add('sdxFlowChartConnectionEditorTextFontHint', @sdxFlowChartConnectionEditorTextFontHint);

  AProduct.Add('sdxFlowChartDialogButtonOk', @sdxFlowChartDialogButtonOk);
  AProduct.Add('sdxFlowChartDialogButtonCancel', @sdxFlowChartDialogButtonCancel);

  AProduct.Add('sdxFlowChartBorderStyleAdjust', @sdxFlowChartBorderStyleAdjust);
  AProduct.Add('sdxFlowChartBorderStyleBottom', @sdxFlowChartBorderStyleBottom);
  AProduct.Add('sdxFlowChartBorderStyleDiagonal', @sdxFlowChartBorderStyleDiagonal);
  AProduct.Add('sdxFlowChartBorderStyleFlat', @sdxFlowChartBorderStyleFlat);
  AProduct.Add('sdxFlowChartBorderStyleLeft', @sdxFlowChartBorderStyleLeft);
  AProduct.Add('sdxFlowChartBorderStyleMiddle', @sdxFlowChartBorderStyleMiddle);
  AProduct.Add('sdxFlowChartBorderStyleMono', @sdxFlowChartBorderStyleMono);
  AProduct.Add('sdxFlowChartBorderStyleRight', @sdxFlowChartBorderStyleRight);
  AProduct.Add('sdxFlowChartBorderStyleSoft', @sdxFlowChartBorderStyleSoft);
  AProduct.Add('sdxFlowChartBorderStyleTop', @sdxFlowChartBorderStyleTop);

  AProduct.Add('sdxFlowChartEdgeStyleRaisedIn', @sdxFlowChartEdgeStyleRaisedIn);
  AProduct.Add('sdxFlowChartEdgeStyleRaisedOut', @sdxFlowChartEdgeStyleRaisedOut);
  AProduct.Add('sdxFlowChartEdgeStyleSunkenIn', @sdxFlowChartEdgeStyleSunkenIn);
  AProduct.Add('sdxFlowChartEdgeStyleSunkenOut', @sdxFlowChartEdgeStyleSunkenOut);

  AProduct.Add('sdxFlowChartEditorChildItem', @sdxFlowChartEditorChildItem);
  AProduct.Add('sdxFlowChartEditorConnection', @sdxFlowChartEditorConnection);
  AProduct.Add('sdxFlowChartEditorConnectionArrowDestinationHint', @sdxFlowChartEditorConnectionArrowDestinationHint);
  AProduct.Add('sdxFlowChartEditorConnectionArrowDestinationSizeHint', @sdxFlowChartEditorConnectionArrowDestinationSizeHint);
  AProduct.Add('sdxFlowChartEditorConnectionArrowSourceHint', @sdxFlowChartEditorConnectionArrowSourceHint);
  AProduct.Add('sdxFlowChartEditorConnectionArrowSourceSizeHint', @sdxFlowChartEditorConnectionArrowSourceSizeHint);
  AProduct.Add('sdxFlowChartEditorConnectionLinkedPointDestinationHint', @sdxFlowChartEditorConnectionLinkedPointDestinationHint);
  AProduct.Add('sdxFlowChartEditorConnectionLinkedPointSourceHint', @sdxFlowChartEditorConnectionLinkedPointSourceHint);
  AProduct.Add('sdxFlowChartEditorConnectionStyleHint', @sdxFlowChartEditorConnectionStyleHint);
  AProduct.Add('sdxFlowChartEditorConnectionTextFontHint', @sdxFlowChartEditorConnectionTextFontHint);
  AProduct.Add('sdxFlowChartEditorCreate', @sdxFlowChartEditorCreate);
  AProduct.Add('sdxFlowChartEditorCreateConnectionHint', @sdxFlowChartEditorCreateConnectionHint);
  AProduct.Add('sdxFlowChartEditorCreateObjectHint', @sdxFlowChartEditorCreateObjectHint);
  AProduct.Add('sdxFlowChartEditorEdit', @sdxFlowChartEditorEdit);
  AProduct.Add('sdxFlowChartEditorEditBringToFront', @sdxFlowChartEditorEditBringToFront);
  AProduct.Add('sdxFlowChartEditorEditClearSelection', @sdxFlowChartEditorEditClearSelection);
  AProduct.Add('sdxFlowChartEditorEditCopy', @sdxFlowChartEditorEditCopy);
  AProduct.Add('sdxFlowChartEditorEditCut', @sdxFlowChartEditorEditCut);
  AProduct.Add('sdxFlowChartEditorEditDelete', @sdxFlowChartEditorEditDelete);
  AProduct.Add('sdxFlowChartEditorEditPaste', @sdxFlowChartEditorEditPaste);
  AProduct.Add('sdxFlowChartEditorEditSelectAll', @sdxFlowChartEditorEditSelectAll);
  AProduct.Add('sdxFlowChartEditorEditSendToBack', @sdxFlowChartEditorEditSendToBack);
  AProduct.Add('sdxFlowChartEditorEditUndo', @sdxFlowChartEditorEditUndo);
  AProduct.Add('sdxFlowChartEditorFile', @sdxFlowChartEditorFile);
  AProduct.Add('sdxFlowChartEditorFileOpen', @sdxFlowChartEditorFileOpen);
  AProduct.Add('sdxFlowChartEditorFileSave', @sdxFlowChartEditorFileSave);
  AProduct.Add('sdxFlowChartEditorFitHint', @sdxFlowChartEditorFitHint);
  AProduct.Add('sdxFlowChartEditorHelp', @sdxFlowChartEditorHelp);
  AProduct.Add('sdxFlowChartEditorHelpContents', @sdxFlowChartEditorHelpContents);
  AProduct.Add('sdxFlowChartEditorMainItemOfUnion', @sdxFlowChartEditorMainItemOfUnion);
  AProduct.Add('sdxFlowChartEditorObject', @sdxFlowChartEditorObject);
  AProduct.Add('sdxFlowChartEditorObjectImagePositionHint', @sdxFlowChartEditorObjectImagePositionHint);
  AProduct.Add('sdxFlowChartEditorObjectLineWidthHint', @sdxFlowChartEditorObjectLineWidthHint);
  AProduct.Add('sdxFlowChartEditorObjectShapeStyleHint', @sdxFlowChartEditorObjectShapeStyleHint);
  AProduct.Add('sdxFlowChartEditorObjectTextFontHint', @sdxFlowChartEditorObjectTextFontHint);
  AProduct.Add('sdxFlowChartEditorObjectTextPositionHint', @sdxFlowChartEditorObjectTextPositionHint);
  AProduct.Add('sdxFlowChartEditorOptions', @sdxFlowChartEditorOptions);
  AProduct.Add('sdxFlowChartEditorOptionsDynamicMoving', @sdxFlowChartEditorOptionsDynamicMoving);
  AProduct.Add('sdxFlowChartEditorOptionsDynamicSizing', @sdxFlowChartEditorOptionsDynamicSizing);
  AProduct.Add('sdxFlowChartEditorPixels', @sdxFlowChartEditorPixels);
  AProduct.Add('sdxFlowChartEditorPoint', @sdxFlowChartEditorPoint);
  AProduct.Add('sdxFlowChartEditorProperties', @sdxFlowChartEditorProperties);
  AProduct.Add('sdxFlowChartEditorUnions', @sdxFlowChartEditorUnions);
  AProduct.Add('sdxFlowChartEditorUnionsAdd', @sdxFlowChartEditorUnionsAdd);
  AProduct.Add('sdxFlowChartEditorUnionsClear', @sdxFlowChartEditorUnionsClear);
  AProduct.Add('sdxFlowChartEditorUnionsClearAll', @sdxFlowChartEditorUnionsClearAll);
  AProduct.Add('sdxFlowChartEditorUnionsNew', @sdxFlowChartEditorUnionsNew);
  AProduct.Add('sdxFlowChartEditorUnionsRemove', @sdxFlowChartEditorUnionsRemove);
  AProduct.Add('sdxFlowChartEditorView', @sdxFlowChartEditorView);
  AProduct.Add('sdxFlowChartEditorViewAntialiasing', @sdxFlowChartEditorViewAntialiasing);
  AProduct.Add('sdxFlowChartEditorViewActualSize', @sdxFlowChartEditorViewActualSize);
  AProduct.Add('sdxFlowChartEditorViewFit', @sdxFlowChartEditorViewFit);
  AProduct.Add('sdxFlowChartEditorViewZoomIn', @sdxFlowChartEditorViewZoomIn);
  AProduct.Add('sdxFlowChartEditorViewZoomOut', @sdxFlowChartEditorViewZoomOut);
  AProduct.Add('sdxFlowChartEditorZoomHint', @sdxFlowChartEditorZoomHint);

  AProduct.Add('sdxFlowChartObjectEditorBackgroundColor', @sdxFlowChartObjectEditorBackgroundColor);
  AProduct.Add('sdxFlowChartObjectEditorBorderStyle', @sdxFlowChartObjectEditorBorderStyle);
  AProduct.Add('sdxFlowChartObjectEditorCaption', @sdxFlowChartObjectEditorCaption);
  AProduct.Add('sdxFlowChartObjectEditorEdgeStyle', @sdxFlowChartObjectEditorEdgeStyle);
  AProduct.Add('sdxFlowChartObjectEditorFrameTab', @sdxFlowChartObjectEditorFrameTab);
  AProduct.Add('sdxFlowChartObjectEditorGeneralTab', @sdxFlowChartObjectEditorGeneralTab);
  AProduct.Add('sdxFlowChartObjectEditorHeight', @sdxFlowChartObjectEditorHeight);
  AProduct.Add('sdxFlowChartObjectEditorImageClear', @sdxFlowChartObjectEditorImageClear);
  AProduct.Add('sdxFlowChartObjectEditorImageLayout', @sdxFlowChartObjectEditorImageLayout);
  AProduct.Add('sdxFlowChartObjectEditorImageTab', @sdxFlowChartObjectEditorImageTab);
  AProduct.Add('sdxFlowChartObjectEditorLineWidth', @sdxFlowChartObjectEditorLineWidth);
  AProduct.Add('sdxFlowChartObjectEditorShapeColor', @sdxFlowChartObjectEditorShapeColor);
  AProduct.Add('sdxFlowChartObjectEditorShapeType', @sdxFlowChartObjectEditorShapeType);
  AProduct.Add('sdxFlowChartObjectEditorText', @sdxFlowChartObjectEditorText);
  AProduct.Add('sdxFlowChartObjectEditorTextLayout', @sdxFlowChartObjectEditorTextLayout);
  AProduct.Add('sdxFlowChartObjectEditorTransparent', @sdxFlowChartObjectEditorTransparent);
  AProduct.Add('sdxFlowChartObjectEditorWidth', @sdxFlowChartObjectEditorWidth);

  AProduct.Add('sdxFlowChartLayoutTopLeft', @sdxFlowChartLayoutTopLeft);
  AProduct.Add('sdxFlowChartLayoutTop', @sdxFlowChartLayoutTop);
  AProduct.Add('sdxFlowChartLayoutTopRight', @sdxFlowChartLayoutTopRight);
  AProduct.Add('sdxFlowChartLayoutLeft', @sdxFlowChartLayoutLeft);
  AProduct.Add('sdxFlowChartLayoutCenter', @sdxFlowChartLayoutCenter);
  AProduct.Add('sdxFlowChartLayoutRight', @sdxFlowChartLayoutRight);
  AProduct.Add('sdxFlowChartLayoutBottomLeft', @sdxFlowChartLayoutBottomLeft);
  AProduct.Add('sdxFlowChartLayoutBottom', @sdxFlowChartLayoutBottom);
  AProduct.Add('sdxFlowChartLayoutBottomRight', @sdxFlowChartLayoutBottomRight);

  AProduct.Add('sdxFlowChartShapeTypeNone', @sdxFlowChartShapeTypeNone);
  AProduct.Add('sdxFlowChartShapeTypeRect', @sdxFlowChartShapeTypeRect);
  AProduct.Add('sdxFlowChartShapeTypeEllipse', @sdxFlowChartShapeTypeEllipse);
  AProduct.Add('sdxFlowChartShapeTypeRoundRect', @sdxFlowChartShapeTypeRoundRect);
  AProduct.Add('sdxFlowChartShapeTypeDiamond', @sdxFlowChartShapeTypeDiamond);
  AProduct.Add('sdxFlowChartShapeTypeNorthTriangle', @sdxFlowChartShapeTypeNorthTriangle);
  AProduct.Add('sdxFlowChartShapeTypeSouthTriangle', @sdxFlowChartShapeTypeSouthTriangle);
  AProduct.Add('sdxFlowChartShapeTypeEastTriangle', @sdxFlowChartShapeTypeEastTriangle);
  AProduct.Add('sdxFlowChartShapeTypeWestTriangle', @sdxFlowChartShapeTypeWestTriangle);
  AProduct.Add('sdxFlowChartShapeTypeHexagon', @sdxFlowChartShapeTypeHexagon);

  AProduct.Add('sdxFlowChartUnion', @sdxFlowChartUnion);
  AProduct.Add('sdxFlowChartUnions', @sdxFlowChartUnions);
  AProduct.Add('sdxFlowChartUnionEditorCaption', @sdxFlowChartUnionEditorCaption);

  AProduct.Add('sdxFlowChartConnectionStyleStraight', @sdxFlowChartConnectionStyleStraight);
  AProduct.Add('sdxFlowChartConnectionStyleCurved', @sdxFlowChartConnectionStyleCurved);
  AProduct.Add('sdxFlowChartConnectionStyleRectHorizontal', @sdxFlowChartConnectionStyleRectHorizontal);
  AProduct.Add('sdxFlowChartConnectionStyleRectVertical', @sdxFlowChartConnectionStyleRectVertical);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressFlowChart', @AddFlowChartResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressFlowChart');
end.
