{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctionsLookup;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, SysUtils, Classes, StrUtils, DateUtils, Variants, Math, cxVariants, dxCore, dxSpreadSheetTypes,
  dxSpreadSheetCore, dxSpreadSheetUtils, dxSpreadSheetStrs, dxSpreadSheetFormulas;

procedure fnAddress(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnColumn(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnColumns(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFormulaText(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnHLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMatch(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRow(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRows(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTranspose(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnVLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

{
procedure fnChoose(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
}
implementation

uses
  dxSpreadSheetFunctionsText;

type
  TdxSpreadSheetFormulaExtractVectorFromRange = function(const ARange: TdxSpreadSheetFormulaToken;
    AVectorIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetVector of object;

  TdxSpreadSheetXLookupComparer = function(const AValue1: Variant; AValue2: TdxSpreadSheetVectorValue): Integer;

  TFormulaAccess = class(TdxSpreadSheetFormula);
  TReferenceAccess = class(TdxSpreadSheetFormulaReference);
  TTokenAccess = class(TdxSpreadSheetFormulaToken);

procedure fnAddress(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
const
  anAbsolute        = 1;
  anAbsRowRelColumn = 2;
  anRelRowAbsColumn = 3;
  anRelative        = 4;

  function CheckParams(var ARow, AColumn, AAbsNum: Variant): Boolean;
  begin
    ARow := Trunc(ARow);
    AColumn := Trunc(AColumn);
    AAbsNum := Trunc(AAbsNum);
    Result := InRange(ARow, 1, 65536) and InRange(AColumn, 1, 256) and
      (Trunc(AAbsNum) in [anAbsolute, anAbsRowRelColumn, anRelRowAbsColumn, anRelative]);
    if not Result then
      Sender.SetError(ecValue);
  end;

  function GetSheetAddressName(const ASheet: TdxUnicodeString): TdxUnicodeString;
  begin
    Result := ASheet;
    if Result = '' then
      Exit;
    if Pos(' ', Result) > 0 then
      Result := '''' + Result + '''';
    Result := Result + '!';
  end;

var
  ARow, AColumn, AAbsNum, AA1RefStyle, ASheet: Variant;
  ARowReference, AColumnReference: TdxSpreadSheetReference;
begin
  if not (Sender.ExtractNumericParameter(ARow, AParams) and Sender.ExtractNumericParameter(AColumn, AParams, 1) and
          Sender.ExtractNumericParameterDef(AAbsNum, 1, 1, AParams, 2) and
          Sender.ExtractNumericParameterDef(AA1RefStyle, 1, 1, AParams, 3) and
          CheckParams(ARow, AColumn, AAbsNum) and Sender.ExtractParameterDef(ASheet, '', '', AParams, 4)) then Exit;
  ARowReference.Offset := ARow - 1;
  AColumnReference.Offset := AColumn - 1;
  ARowReference.IsAbsolute := (AAbsNum = anAbsolute) or (AAbsNum = anAbsRowRelColumn);
  AColumnReference.IsAbsolute := (AAbsNum = anAbsolute) or (AAbsNum = anRelRowAbsColumn);
  if AA1RefStyle = 0 then
  begin
    if not AColumnReference.IsAbsolute then
      AColumnReference.Offset := AColumnReference.Offset + 1;
    if not ARowReference.IsAbsolute then
      ARowReference.Offset := ARowReference.Offset + 1;
  end;
  if AA1RefStyle <> 0 then
    Sender.AddValue(GetSheetAddressName(VarToStr(ASheet)) + dxReferenceToString(0, 0, ARowReference, AColumnReference))
  else
    Sender.AddValue(GetSheetAddressName(VarToStr(ASheet)) + dxR1C1ReferenceToString(0, 0, ARowReference, AColumnReference));
end;

procedure fnColumn(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.AddValue(Sender.Owner.Cell.ColumnIndex + 1)
  else
  begin
    AParam := AParams.FirstChild;
    if not (AParam is TdxSpreadSheetFormulaReference) then
      Sender.SetError(ecValue)
    else
      Sender.AddValue(TdxSpreadSheetFormulaReference(AParam).ActualColumn + 1);
  end;
end;

procedure fnColumns(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
  ADimension: TdxSpreadSheetFormulaTokenDimension;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.SetError(ecValue)
  else
  begin
    AParam := AParams.FirstChild;
    if (AParam is TdxSpreadSheetFormulaReference) or (AParam is TdxSpreadSheetFormulaAreaReference) or
      (AParam is TdxSpreadSheetFormulaArrayToken) then
      begin
        ADimension := AParam.GetDimension(AErrorCode);
        if AErrorCode = ecNone then
          Sender.AddValue(ADimension.ColumnCount)
        else
          Sender.SetError(AErrorCode);
      end
    else
      if (AParam is TdxSpreadSheetFormulaIntegerValueToken) or (AParam is TdxSpreadSheetFormulaFloatValueToken) then
        Sender.AddValue(1)
      else
        Sender.SetError(ecValue);
  end;
end;

procedure fnFormulaText(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
  ACell: TdxSpreadSheetCell;
  AReference: TReferenceAccess;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.SetError(ecNA)
  else
  begin
    AParam := AParams.FirstChild;
    if not(AParam is TdxSpreadSheetFormulaReference) then
      Sender.SetError(ecNA)
    else
    begin
      AReference := TReferenceAccess(TTokenAccess(AParam));
      ACell := AReference.GetSheet.Cells[TdxSpreadSheetFormulaReference(AParam).ActualRow, TdxSpreadSheetFormulaReference(AParam).ActualColumn];
      if ACell.IsFormula then
        Sender.AddValue(ACell.AsFormula.AsText)
      else
        Sender.SetError(ecNA);
    end;
  end;
end;

function dxBooleanLookupComparer(const ABoolValue: Variant; AValue2: TdxSpreadSheetVectorValue): Integer; inline;
begin
  if AValue2.IsError or not dxIsLogical(AValue2.Value) then
    Result := -2
  else
    if (ABoolValue = False) and (AValue2.Value = True) then
      Result := -1
    else
      if (ABoolValue = True) and (AValue2.Value = False) then
        Result := 1
      else
        Result := 0;
end;

function dxNumericLookupComparer(const ANumericValue: Variant; AValue2: TdxSpreadSheetVectorValue): Integer; inline;
begin
  if AValue2.IsError or not dxIsNumberOrDateTime(AValue2.Value) then
    Result := -2
  else
    Result := Sign(Double(ANumericValue - AValue2.Value));
end;

function dxStringLookupComparer(const AStrValue: Variant; AValue2: TdxSpreadSheetVectorValue): Integer; inline;
const
  AResult: array[Boolean] of Integer = (-1,1);
begin
  if AValue2.IsError or not VarIsStr(AValue2.Value) then
    Result := -2
  else
    if VarEquals(AStrValue, AValue2.Value) then
      Result := 0
    else
      Result := AResult[AStrValue > AValue2.Value];
end;

function dxCheckLookupComparer(const ALookupValue: Variant): TdxSpreadSheetXLookupComparer;
begin
  @Result := nil;
  if dxIsLogical(ALookupValue) then
    Result := @dxBooleanLookupComparer
  else
    if dxIsNumberOrDateTime(ALookupValue) then
      Result := @dxNumericLookupComparer
    else
      if VarIsStr(ALookupValue) then
        Result := @dxStringLookupComparer;
end;

function dxGetApproximateXLookupIndexForAscending(const AValue: Variant; const AVector: TdxSpreadSheetVector;
  AComparer: TdxSpreadSheetXLookupComparer): Integer;

  function GetNextSameTypeValueIndex(AComparer: TdxSpreadSheetXLookupComparer;
    const ACheckedValue: Variant; const AEmptyValueIndex, AEndIndex: Integer): Integer;
  var
    AIndex: Integer;
  begin
    Result := AEmptyValueIndex;
    AIndex := Result + 1;
    while AIndex <= AEndIndex do
      if AComparer(ACheckedValue, AVector[AIndex]) = -2 then
        Inc(AIndex)
      else
      begin
        Result := AIndex;
        Break;
      end;
  end;

  function ExecuteBinaryForwardSearch(AComparer: TdxSpreadSheetXLookupComparer; AStartIndex, AEndIndex: Integer): Integer;
  var
    AMedianIndex, ANextIndex: Integer;
  begin
    Result := -1;
    if AStartIndex > AEndIndex then
      Exit;
    AMedianIndex := (AStartIndex + AEndIndex) div 2;
    case AComparer(AValue, AVector[AMedianIndex]) of
      -1: Result := ExecuteBinaryForwardSearch(AComparer, AStartIndex, AMedianIndex - 1);
       0: Result := AMedianIndex;
       1: Result := Max(AMedianIndex, ExecuteBinaryForwardSearch(AComparer, AMedianIndex + 1, AEndIndex));
    else
      begin
        ANextIndex := GetNextSameTypeValueIndex(AComparer, AValue, AMedianIndex, AEndIndex);
        if ANextIndex > AMedianIndex then
          case AComparer(AValue, AVector[ANextIndex]) of
           -1: Result := ExecuteBinaryForwardSearch(AComparer, AStartIndex, AMedianIndex - 1);
            0: Result := ANextIndex;
            1: Result := Max(ANextIndex, ExecuteBinaryForwardSearch(AComparer, ANextIndex + 1, AEndIndex));
          end
        else
          Result := ExecuteBinaryForwardSearch(AComparer, AStartIndex, AMedianIndex - 1);
      end;
    end;
  end;

var
  AHighIndex: Integer;
begin
  AHighIndex := AVector.Count - 1;
  Result := ExecuteBinaryForwardSearch(AComparer, 0, AHighIndex);
  while (Result > -1) and (Result < AHighIndex) and (AComparer(AValue, AVector[Result + 1]) = 0) and
    (AComparer(AVector[Result].Value, AVector[Result + 1]) >= 0) do
    Inc(Result);
end;

function dxGetApproximateXLookupIndexForDescending(const AValue: Variant; const AVector: TdxSpreadSheetVector;
  AComparer: TdxSpreadSheetXLookupComparer): Integer;

  function GetPriorSameTypeValueIndex(AComparer: TdxSpreadSheetXLookupComparer;
    const ACheckedValue: Variant; const AEmptyValueIndex, AEndIndex: Integer): Integer;
  var
    AIndex: Integer;
  begin
    Result := AEmptyValueIndex;
    AIndex := Result - 1;
    while AIndex > 0 do
      if AComparer(ACheckedValue, AVector[AIndex]) = -2 then
        Dec(AIndex)
      else
      begin
        Result := AIndex;
        Break;
      end;
  end;

  function ExecuteBinaryBackwardSearch(AComparer: TdxSpreadSheetXLookupComparer; AStartIndex, AEndIndex: Integer): Integer;
  var
    AMedianIndex, APriorIndex: Integer;
  begin
    Result := -1;
    if AStartIndex > AEndIndex then
      Exit;
    if AComparer(AValue, AVector[AStartIndex]) = 0 then
    begin
      Result := AStartIndex;
      Exit;
    end;
    AMedianIndex := (AStartIndex + AEndIndex) div 2;
    case AComparer(AValue, AVector[AMedianIndex]) of
      -1: Result := Max(AMedianIndex, ExecuteBinaryBackwardSearch(AComparer, AMedianIndex + 1, AEndIndex));
       0: Result := AMedianIndex;
       1: Result := ExecuteBinaryBackwardSearch(AComparer, AStartIndex, AMedianIndex - 1);
    else
      begin
        APriorIndex := GetPriorSameTypeValueIndex(AComparer, AValue, AMedianIndex, AEndIndex);
        if APriorIndex < AMedianIndex then
          case AComparer(AValue, AVector[APriorIndex]) of
           -1: Result := Max(AMedianIndex, ExecuteBinaryBackwardSearch(AComparer, AMedianIndex + 1, AEndIndex));
            0: Result := APriorIndex;
            1: Result := ExecuteBinaryBackwardSearch(AComparer, AStartIndex, APriorIndex - 1);
          end
        else
          Result := ExecuteBinaryBackwardSearch(AComparer, AMedianIndex + 1, AEndIndex);
      end;
    end;
  end;

begin
  Result := ExecuteBinaryBackwardSearch(AComparer, 0, AVector.Count - 1);
  while (Result > -1) and (Result > 0) and (AComparer(AValue, AVector[Result - 1]) = 0) and
    (AComparer(AVector[Result].Value, AVector[Result - 1]) <= 0) do
    Dec(Result);
end;

function dxGetApproximateXLookupIndex(const AValue: Variant; const AVector: TdxSpreadSheetVector; const AAscending: Boolean = True): Integer;
var
  AComparer: TdxSpreadSheetXLookupComparer;
begin
  Result := -1;
  AComparer := dxCheckLookupComparer(AValue);
  if @AComparer <> nil then
    if AAscending then
      Result := dxGetApproximateXLookupIndexForAscending(AValue, AVector, AComparer)
    else
      Result := dxGetApproximateXLookupIndexForDescending(AValue, AVector, AComparer);
end;

function dxGetExactXLookupIndex(const AValue: Variant; const AVector: TdxSpreadSheetVector): Integer;

  function HasMaskTokens(S: TdxUnicodeString): Boolean;

    function HasMaskToken(AToken: TdxUnicodeString; var AStartPos: Integer): Boolean;
    begin
      AStartPos := PosEx(AToken, S, AStartPos);
      Result := (AStartPos = 1) or ((AStartPos > 1) and (S[AStartPos - 1] <> '~'));
      if not Result and (AStartPos > 1) then
        Inc(AStartPos);
    end;

  var
    APosQ, APosA: Integer;
  begin
    APosQ := 1;
    APosA := 1;
    Result := False;
    while (APosQ > 0) or (APosA > 0) do
    begin
      Result := HasMaskToken('?', APosQ) or HasMaskToken('*', APosA);
      if Result then
        Break;
    end;
  end;

var
  I: Integer;
  ANeedMaskSearch, ASuccess: Boolean;
begin
  Result := -1;
  ANeedMaskSearch := VarIsStr(AValue) and HasMaskTokens(VarToStr(AValue));
  for I := 0 to AVector.Count - 1 do
  begin
    if ANeedMaskSearch then
      ASuccess := dxMaskSearch(AValue, AVector[I].Value, 1) = 1
    else
      ASuccess := VarEquals(AValue, AVector[I].Value);
    if ASuccess then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure dxSetSenderResultAsVectorValue(Sender: TdxSpreadSheetFormulaResult;
  const AVector: TdxSpreadSheetVector; AValueIndex: Integer);
var
  AResult: Variant;
begin
  if AVector[AValueIndex].IsError then
    Sender.SetError(AVector[AValueIndex].ErrorCode)
  else
  begin
    AResult := AVector[AValueIndex].Value;
    if dxSpreadSheetIsEmptyValue(AResult) then
      AResult := 0;
    Sender.AddValue(AResult);
  end;
end;

procedure DoXLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
  AExtractVectorMethod: TdxSpreadSheetFormulaExtractVectorFromRange);

  function CheckDimension(ADimension: TdxSpreadSheetFormulaTokenDimension; ACheckedIndex: Integer): Boolean;
  var
    ASenderExtractColumnMethod: TdxSpreadSheetFormulaExtractVectorFromRange;
  begin
    ASenderExtractColumnMethod := Sender.ExtractColumnFromRange;
    if dxSameMethods(ASenderExtractColumnMethod, AExtractVectorMethod) then
      Result := ACheckedIndex < ADimension.ColumnCount
    else
      Result := ACheckedIndex < ADimension.RowCount;
  end;

var
  ALookupValue, AResultVectorIndex, AApproximateMatch: Variant;
  ARangeParam: TdxSpreadSheetFormulaToken;
  ARangeDimension: TdxSpreadSheetFormulaTokenDimension;
  AFirstVector, AResultVector: TdxSpreadSheetVector;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AFoundIndex: Integer;
begin
  if not (Sender.ExtractParameterDef(ALookupValue, 0, AParams) and
          Sender.ExtractNumericParameterDef(AResultVectorIndex, 0, AParams, 2) and
          Sender.ExtractNumericParameterDef(AApproximateMatch, 1, 0, AParams, 3)) then
    Exit;
  AResultVectorIndex := Trunc(AResultVectorIndex) - 1;

  ARangeParam := AParams.Next.FirstChild;
  ARangeDimension := ARangeParam.GetDimension(AErrorCode);
  if AErrorCode = ecNone then
  begin
    AFirstVector := AExtractVectorMethod(ARangeParam, 0, AErrorCode);
    try
      if AErrorCode = ecNone then
      begin
        if AApproximateMatch <> 0 then
          AFoundIndex := dxGetApproximateXLookupIndex(ALookupValue, AFirstVector)
        else
          AFoundIndex := dxGetExactXLookupIndex(ALookupValue, AFirstVector);
        if AFoundIndex = -1 then
          Sender.SetError(ecNA)
        else
          if AResultVectorIndex < 0 then
            Sender.SetError(ecValue)
          else
          begin
            AResultVector := AExtractVectorMethod(ARangeParam, AResultVectorIndex, AErrorCode);
            try
              if AErrorCode = ecNone then
                if not CheckDimension(ARangeDimension, AResultVectorIndex) then
                  Sender.SetError(ecRefErr)
                else
                  dxSetSenderResultAsVectorValue(Sender, AResultVector, AFoundIndex);
            finally
              FreeAndNil(AResultVector);
            end;
          end;
      end
    finally
      FreeAndNil(AFirstVector);
    end;
  end;
  if Sender.Validate and (AErrorCode <> ecNone) then
    Sender.SetError(AErrorCode);
end;

procedure fnHLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  DoXLookup(Sender, AParams, Sender.ExtractRowFromRange);
end;

procedure fnVLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  DoXLookup(Sender, AParams, Sender.ExtractColumnFromRange);
end;

procedure fnLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

    function ExtractResultVector(ALookupRange: TdxSpreadSheetFormulaToken; const ALookupRangeDimension: TdxSpreadSheetFormulaTokenDimension;
      AExtractLookupVectorMethod: TdxSpreadSheetFormulaExtractVectorFromRange; const ALookupVectorLength: Integer): TdxSpreadSheetVector;
    var
      AResultRange: TdxSpreadSheetFormulaToken;
      AResultRangeDimension: TdxSpreadSheetFormulaTokenDimension;
      AErrorCode: TdxSpreadSheetFormulaErrorCode;
    begin
      Result := nil;
      if Sender.ParameterExists(AParams, 2) then
      begin
        AResultRange := AParams.Next.Next.FirstChild;
        AResultRangeDimension := AResultRange.GetDimension(AErrorCode);
        if (AErrorCode <> ecNone) or ((AResultRangeDimension.ColumnCount > 1) and (AResultRangeDimension.RowCount > 1)) then
        begin
          if AErrorCode <> ecNone then
            Sender.SetError(AErrorCode)
          else
            Sender.SetError(ecNA);
        end
        else
          if AResultRangeDimension.RowCount > 1 then
          begin
            if AResultRange is TdxSpreadSheetFormulaReference then
             Result := AResultRange.Sheet.ExtractVerticalVector(
               TdxSpreadSheetFormulaReference(AResultRange).ActualRow, TdxSpreadSheetFormulaReference(AResultRange).ActualColumn, ALookupVectorLength)
            else
              Result := Sender.ExtractColumnFromRange(AResultRange, 0, AErrorCode)
          end
          else
          begin
            if AResultRange is TdxSpreadSheetFormulaReference then
             Result := AResultRange.Sheet.ExtractHorizontalVector(
               TdxSpreadSheetFormulaReference(AResultRange).ActualRow, TdxSpreadSheetFormulaReference(AResultRange).ActualColumn, ALookupVectorLength)
            else
              Result := Sender.ExtractRowFromRange(AResultRange, 0, AErrorCode);
          end;
      end
      else
        Result := AExtractLookupVectorMethod(ALookupRange, Min(ALookupRangeDimension.ColumnCount - 1,
          ALookupRangeDimension.RowCount - 1), AErrorCode);
    end;

var
  ALookupValue: Variant;
  ALookupRange: TdxSpreadSheetFormulaToken;
  ALookupRangeDimension: TdxSpreadSheetFormulaTokenDimension;
  AExtractLookupVectorMethod: TdxSpreadSheetFormulaExtractVectorFromRange;
  ALookupVector, AResultVector: TdxSpreadSheetVector;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AFoundIndex: Integer;
begin
  if not Sender.ExtractParameterDef(ALookupValue, 0, AParams) then
    Exit;

  ALookupRange := AParams.Next.FirstChild;
  ALookupRangeDimension := ALookupRange.GetDimension(AErrorCode);
  if ALookupRangeDimension.ColumnCount > ALookupRangeDimension.RowCount then
    AExtractLookupVectorMethod := Sender.ExtractRowFromRange
  else
    AExtractLookupVectorMethod := Sender.ExtractColumnFromRange;
  ALookupVector := AExtractLookupVectorMethod(ALookupRange, 0, AErrorCode);
  if AErrorCode <> ecNone then
  begin
    FreeAndNil(ALookupVector);
    Exit;
  end;

  try
    AResultVector := ExtractResultVector(ALookupRange, ALookupRangeDimension, AExtractLookupVectorMethod, ALookupVector.Count);
    try
      if Sender.Validate then
      begin
        if Sender.Validate then
        begin
          AFoundIndex := dxGetApproximateXLookupIndex(ALookupValue, ALookupVector);
          if (AFoundIndex = -1) or (AFoundIndex > AResultVector.Count - 1) then
            Sender.SetError(ecNA)
          else
            dxSetSenderResultAsVectorValue(Sender, AResultVector, AFoundIndex);
        end;
      end;
    finally
      FreeAndNil(AResultVector);
    end;
  finally
    FreeAndNil(ALookupVector);
  end;
end;

procedure fnMatch(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

  function ExtractLookupVector(var ALookupVector: TdxSpreadSheetVector): Boolean;
  var
    ADimension: TdxSpreadSheetFormulaTokenDimension;
    AErrorCode: TdxSpreadSheetFormulaErrorCode;
    ALookupToken: TdxSpreadSheetFormulaToken;
  begin
    Result := (AParams.Next <> nil) and (AParams.Next.FirstChild <> nil);
    if not Result then
    begin
      Sender.SetError(ecNA);
      Exit;
    end;

    ALookupToken := AParams.Next.FirstChild;
    ADimension := ALookupToken.GetDimension(AErrorCode);
    if (AErrorCode <> ecNone) or ((ADimension.ColumnCount > 1) and (ADimension.RowCount > 1)) then
    begin
      Result := False;
      if AErrorCode <> ecNone then
        Sender.SetError(AErrorCode)
      else
        Sender.SetError(ecNA);
      Exit;
    end;
    if ADimension.ColumnCount > ADimension.RowCount then
      ALookupVector := Sender.ExtractRowFromRange(ALookupToken, 0, AErrorCode)
    else
      ALookupVector := Sender.ExtractColumnFromRange(ALookupToken, 0, AErrorCode);
    Result := AErrorCode = ecNone;
    if not Result then
      Sender.SetError(AErrorCode);
  end;

var
  ALookupValue, AMatchType: Variant;
  ALookupVector: TdxSpreadSheetVector;
  AFoundIndex: Integer;
begin
  if not (Sender.ExtractParameterDef(ALookupValue, 0, AParams) and
          Sender.ExtractNumericParameterDef(AMatchType, 1, 0, AParams, 2) and
          ExtractLookupVector(ALookupVector)) then
  begin
    FreeAndNil(ALookupVector);
    Exit;
  end;

  try
    if AMatchType = 0 then
      AFoundIndex := dxGetExactXLookupIndex(ALookupValue, ALookupVector)
    else
      AFoundIndex := dxGetApproximateXLookupIndex(ALookupValue, ALookupVector, AMatchType > 0);
  finally
    FreeAndNil(ALookupVector);
  end;
  if AFoundIndex = -1 then
    Sender.SetError(ecNA)
  else
    Sender.AddValue(AFoundIndex + 1);
end;

procedure fnRow(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.AddValue(Sender.Owner.Cell.RowIndex + 1)
  else
  begin
    AParam := AParams.FirstChild;
    if not (AParam is TdxSpreadSheetFormulaReference) then
      Sender.SetError(ecValue)
    else
      Sender.AddValue(TdxSpreadSheetFormulaReference(AParam).ActualRow + 1);
  end;
end;

procedure fnRows(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
  ADimension: TdxSpreadSheetFormulaTokenDimension;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.SetError(ecValue)
  else
  begin
    AParam := AParams.FirstChild;
    if (AParam is TdxSpreadSheetFormulaReference) or (AParam is TdxSpreadSheetFormulaAreaReference) or
      (AParam is TdxSpreadSheetFormulaArrayToken) then
      begin
        ADimension := AParam.GetDimension(AErrorCode);
        if AErrorCode = ecNone then
          Sender.AddValue(ADimension.RowCount)
        else
          Sender.SetError(AErrorCode);
      end
    else
      if (AParam is TdxSpreadSheetFormulaIntegerValueToken) or (AParam is TdxSpreadSheetFormulaFloatValueToken) then
        Sender.AddValue(1)
      else
        Sender.SetError(ecValue);
  end;
end;

procedure fnTranspose(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AOwner: TdxSpreadSheetFormula;
  ASource, AParentToken: TdxSpreadSheetFormulaToken;
  ADimension: TdxSpreadSheetFormulaTokenDimension;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AResult: TdxSpreadSheetFormulaArrayToken;
  ARow, AColumn: Integer;
  AValue: Variant;
begin
  AOwner := Sender.Owner;
  if not AOwner.IsArrayFormula then
    Sender.SetError(ecValue)
  else
  begin
    ASource := AParams.FirstChild;
    ADimension := ASource.GetDimension(AErrorCode);
    if AErrorCode = ecNone then
    begin
      AResult := TdxSpreadSheetFormulaArrayToken.Create;
      for ARow := 0 to ADimension.ColumnCount - 1 do
        for AColumn := 0 to ADimension.RowCount - 1 do
        begin
          if (AColumn = 0) and (ARow > 0) then
            TFormulaAccess(AOwner).AddChild(AResult, TdxSpreadSheetFormulaArrayRowSeparator.Create());

          TTokenAccess(ASource).GetValueAsArrayItem(AColumn, ARow, AValue, AErrorCode);
          if AValue = Unassigned then
            AValue := 0;
          AParentToken := TdxSpreadSheetFormulaToken.Create;
          if AErrorCode <> ecNone then
            TFormulaAccess(AOwner).AddChild(AParentToken, TdxSpreadSheetFormulaErrorValueToken.Create(AErrorCode))
          else
            TFormulaAccess(AOwner).AddChild(AParentToken, TdxSpreadSheetFormulaVariantToken.Create(AValue));
          TFormulaAccess(AOwner).AddChild(AResult, AParentToken);
          AErrorCode := ecNone;
        end;
      Sender.Add(AResult);
    end
    else
      Sender.SetError(AErrorCode);
  end;
end;

(*
procedure fnChoose(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AList: TList;
  AIndex: Variant;
  AIntIndex: Integer;
begin
  if not Sender.ExtractNumericParameter(AIndex, AParams) then
    Exit;
  AIntIndex := Trunc(AIndex);
  if not (AIntIndex in [1..254]) then
  begin
    Sender.SetError(ecValue);
    Exit;
  end;
  AList := TList.Create;
  try
    Sender.ForEach(AParams.Next, @cbPopulateList, AList);
    if AIntIndex <= AList.Count then
      Sender.AddValue(Variant(AList[AIntIndex - 1]^))
    else
      Sender.SetError(ecValue);
  finally
    AList.Free;
  end;
end;
*)
end.
