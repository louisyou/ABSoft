{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLSXReader;

{$I cxVer.Inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Windows, SysUtils, Classes, Graphics, dxCore, dxCoreClasses, cxClasses, dxCustomTree, dxXMLDoc, dxZIPUtils,
  dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetClasses, dxSpreadSheetStrs, dxSpreadSheetPackedFileFormatCore,
  dxSpreadSheetUtils, dxGDIPlusClasses, Generics.Collections, dxCoreGraphics, cxGeometry, dxHashUtils,
  dxSpreadSheetPrinting;

{$R dxSpreadSheetFormatXLSXReader.res}

type
  TdxSpreadSheetXLSXReader = class;

  { TdxSpreadSheetXLSXHashTableItemList }

  TdxSpreadSheetXLSXHashTableItemList = class(TList<TdxHashTableItem>)
  protected
    procedure Notify(const Item: TdxHashTableItem; Action: TCollectionNotification); override;
  end;

  { TdxSpreadSheetXLSXFormulaAsTextInfoList }

  TdxSpreadSheetXLSXFormulaAsTextInfoList = class(TdxSpreadSheetFormulaAsTextInfoList)
  public
    procedure Add(ACell: TdxSpreadSheetCell; AFunctionNode: TdxXMLNode); overload;
  end;

  { TdxSpreadSheetXLSXReaderContentIndex }

  TdxSpreadSheetXLSXReaderContentIndex = class
  strict private
    FData: TStringList;
    function GetContentType(const AFileName: AnsiString): AnsiString;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Load(AReader: TdxSpreadSheetXLSXReader; const AFileName: AnsiString);
    //
    property ContentType[const AFileName: AnsiString]: AnsiString read GetContentType;
  end;

  { TdxSpreadSheetXLSXReaderRelsItem }

  TdxSpreadSheetXLSXReaderRelsItem = class
  public
    ID, FileName, FileType: AnsiString;
    procedure SetValue(const AFileName, AFileType: AnsiString);
  end;

  { TdxSpreadSheetXLSXReaderRels }

  TdxSpreadSheetXLSXReaderRels = class
  strict private
    FList: TcxObjectList;
    function GetItem(const ID: AnsiString): TdxSpreadSheetXLSXReaderRelsItem;
    function GetCount: Integer;
    function GetItemByIndex(Index: Integer): TdxSpreadSheetXLSXReaderRelsItem;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Clear;
    function Find(const ID: AnsiString): TdxSpreadSheetXLSXReaderRelsItem;
    function FindByType(const AType: AnsiString; out AItem: TdxSpreadSheetXLSXReaderRelsItem): Boolean;
    procedure Load(AReader: TdxSpreadSheetXLSXReader; const AOwnerFileName: AnsiString);

    property Count: Integer read GetCount;
    property Items[const ID: AnsiString]: TdxSpreadSheetXLSXReaderRelsItem read GetItem;
    property ItemsByIndex[Index: Integer]: TdxSpreadSheetXLSXReaderRelsItem read GetItemByIndex;
  end;

  { TdxSpreadSheetXLSXReaderCustomParser }

  TdxSpreadSheetXLSXReaderCustomParser = class(TdxSpreadSheetCustomPackedReaderParser)
  strict private
    procedure ExtractIndexedColor(ANode: TdxXMLNode; AColorIndex: Integer; var AColor: TColor);
    procedure ExtractThemedColor(ANode: TdxXMLNode; AThemeIndex: Integer; var AColor: TColor);
    function GetOwner: TdxSpreadSheetXLSXReader; inline;
  protected
    function CheckListIndex(AIndex: Integer; AList: TList;
      const AMessage: string; AMessageType: TdxSpreadSheetMessageType): Boolean; overload;
    function CheckListIndex(AIndex: Integer; AList: TdxSpreadSheetXLSXHashTableItemList;
      const AMessage: string; AMessageType: TdxSpreadSheetMessageType): Boolean; overload;
    function DecodeColor(ANode: TdxXMLNode): TColor; overload;
    function DecodeColor(const HexCode: string): TColor; overload;
  public
    property Owner: TdxSpreadSheetXLSXReader read GetOwner;
  end;

  { TdxSpreadSheetXLSXReaderCustomNodeParser }

  TdxSpreadSheetXLSXReaderCustomNodeParser = class(TdxSpreadSheetXLSXReaderCustomParser)
  protected
    FNode: TdxXMLNode;
  public
    constructor Create(ANode: TdxXMLNode; AOwner: TdxSpreadSheetXLSXReader);
    //
    property Node: TdxXMLNode read FNode;
  end;

  { TdxSpreadSheetXLSXReaderCustomDocumentParser }

  TdxSpreadSheetXLSXReaderCustomDocumentParser = class(TdxSpreadSheetXLSXReaderCustomNodeParser)
  strict private
    FDocument: TdxXMLDocument;
    FDocumentFileName: AnsiString;
    FRels: TdxSpreadSheetXLSXReaderRels;
  public
    constructor Create(const AFileName: AnsiString; AOwner: TdxSpreadSheetXLSXReader);
    destructor Destroy; override;
    //
    property Document: TdxXMLDocument read FDocument;
    property DocumentFileName: AnsiString read FDocumentFileName;
    property Rels: TdxSpreadSheetXLSXReaderRels read FRels;
  end;

  { TdxSpreadSheetXLSXReaderCustomDocumentSubParser }

  TdxSpreadSheetXLSXReaderCustomDocumentSubParser = class(TdxSpreadSheetXLSXReaderCustomNodeParser)
  strict private
    FOwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser;
  protected
    function ReadImage(ANode: TdxXMLNode): TStream;
  public
    constructor Create(ANode: TdxXMLNode; AOwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser);
    //
    property OwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser read FOwnerParser;
  end;

  { TdxSpreadSheetXLSXReader }

  TdxSpreadSheetXLSXReader = class(TdxSpreadSheetCustomPackedReader)
  strict private
    FBorders: TdxSpreadSheetXLSXHashTableItemList;
    FFills: TdxSpreadSheetXLSXHashTableItemList;
    FFonts: TdxSpreadSheetXLSXHashTableItemList;
    FFormats: TdxSpreadSheetXLSXHashTableItemList;
    FFormulasRefs: TdxSpreadSheetXLSXFormulaAsTextInfoList;
    FIndexedColors: TList<TColor>;
    FSharedStrings: TdxSpreadSheetXLSXHashTableItemList;
    FStyles: TdxSpreadSheetXLSXHashTableItemList;
    FThemedBrushes: TObjectList<TdxGPBrush>;
    FThemedColors: TList;
    FThemedPens: TObjectList<TdxGPPen>;

    FColorMap: TDictionary<TdxXMLString, TColor>;
    FColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper;
    FContentIndex: TdxSpreadSheetXLSXReaderContentIndex;

    function GetColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper;
    function GetContentIndex: TdxSpreadSheetXLSXReaderContentIndex;
  protected
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; override;
    procedure ForEachNodeChild(ANode: TdxXMLNode; AProc: TdxXMLNodeForEachProc; AUserData: Pointer);

    property Borders: TdxSpreadSheetXLSXHashTableItemList read FBorders;
    property Fills: TdxSpreadSheetXLSXHashTableItemList read FFills;
    property Fonts: TdxSpreadSheetXLSXHashTableItemList read FFonts;
    property Formats: TdxSpreadSheetXLSXHashTableItemList read FFormats;
    property FormulasRefs: TdxSpreadSheetXLSXFormulaAsTextInfoList read FFormulasRefs;
    property IndexedColors: TList<TColor> read FIndexedColors;
    property SharedStrings: TdxSpreadSheetXLSXHashTableItemList read FSharedStrings;
    property Styles: TdxSpreadSheetXLSXHashTableItemList read FStyles;
    property ThemedBrushes: TObjectList<TdxGPBrush> read FThemedBrushes;
    property ThemedColors: TList read FThemedColors;
    property ThemedPens: TObjectList<TdxGPPen> read FThemedPens;

    property ColorMap: TDictionary<TdxXMLString, TColor> read FColorMap;
    property ColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper read GetColumnWidthHelper;
    property ContentIndex: TdxSpreadSheetXLSXReaderContentIndex read GetContentIndex;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    function GetWorkbookFileName(out AFileName: AnsiString): Boolean; virtual;
    procedure ReadData; override;
  end;

  { TdxSpreadSheetXLSXReaderDrawingParser }

  TdxSpreadSheetXLSXReaderDrawingParser = class(TdxSpreadSheetXLSXReaderCustomDocumentParser)
  strict private
    FView: TdxSpreadSheetTableView;
  protected
    procedure ProcessContainer(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessContainerPicture(ANode: TdxXMLNode); virtual;
    procedure ProcessContainerShape(ANode: TdxXMLNode); virtual;
  public
    constructor Create(const AFileName: AnsiString; AOwner: TdxSpreadSheetXLSXReader; AView: TdxSpreadSheetTableView);
    procedure Execute; override;
    //
    property View: TdxSpreadSheetTableView read FView;
  end;

  { TdxSpreadSheetXLSXReaderDrawingBrushParser }

  TdxSpreadSheetXLSXReaderDrawingBrushParser = class(TdxSpreadSheetXLSXReaderCustomDocumentSubParser)
  strict private
    FBrush: TdxGPBrush;
  protected
    procedure ProcessGradientBrushPoints(ANode: TdxXMLNode; AUserData: Pointer); virtual;

    function ReadColor(AParentNode: TdxXMLNode): TdxAlphaColor;
    procedure ReadGradientBrushParameters; virtual;
    procedure ReadPatternBrushParameters; virtual;
    procedure ReadSolidBrushParameters; virtual;
    procedure ReadTexturedBrushParameters; virtual;
  public
    constructor Create(ANode: TdxXMLNode; ABrush: TdxGPBrush; AOwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser);
    procedure Execute; override;
    //
    property Brush: TdxGPBrush read FBrush;
  end;

  { TdxSpreadSheetXLSXReaderDrawingPenParser }

  TdxSpreadSheetXLSXReaderDrawingPenParser = class(TdxSpreadSheetXLSXReaderCustomDocumentSubParser)
  strict private
    FPen: TdxGPPen;
  public
    constructor Create(ANode: TdxXMLNode; APen: TdxGPPen; AOwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser);
    procedure Execute; override;
    //
    property Pen: TdxGPPen read FPen;
  end;

  { TdxSpreadSheetXLSXReaderContainerParser }

  TdxSpreadSheetXLSXReaderContainerParser = class(TdxSpreadSheetXLSXReaderCustomDocumentSubParser)
  strict private
    function GetOwnerParser: TdxSpreadSheetXLSXReaderDrawingParser; inline;
    procedure ReadAnchorMarker(ANode: TdxXMLNode; AAnchor: TdxSpreadSheetContainerAnchorPoint);
  protected
    procedure ReadDescription(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer); virtual;
    procedure ReadEditAsMode(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer); virtual;
    procedure ReadPlacement(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer); virtual;
    function ReadPoint(ANode: TdxXMLNode; const XName, YName: TdxXMLString): TPoint;
    //
    procedure ReadAbsoluteAnchor(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
    procedure ReadOneCellAnchor(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
    procedure ReadTwoCellAnchor(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
  public
    property OwnerParser: TdxSpreadSheetXLSXReaderDrawingParser read GetOwnerParser;
  end;

  { TdxSpreadSheetXLSXReaderDrawingShapeContainerParser }

  TdxSpreadSheetXLSXReaderDrawingShapeContainerParser = class(TdxSpreadSheetXLSXReaderContainerParser)
  protected
    procedure ReadContainerRestrictions(ANode: TdxXMLNode; AContainer: TdxSpreadSheetShapeContainer); virtual;
    procedure ReadShapeDefaultStyles(ANode: TdxXMLNode; AShape: TdxSpreadSheetShape); virtual;
    procedure ReadShapeFillParameters(ANode: TdxXMLNode; AShape: TdxSpreadSheetShape); virtual;
    procedure ReadShapeGeometry(ANode: TdxXMLNode; AShape: TdxSpreadSheetShape); virtual;
    procedure ReadShapeLineParameters(ANode: TdxXMLNode; APen: TdxGPPen); virtual;
    procedure ReadShapeProperties(ANode: TdxXMLNode; AContainer: TdxSpreadSheetShapeContainer); virtual;
    procedure ReadTransformProperties(ANode: TdxXMLNode; AContainer: TdxSpreadSheetShapeContainer); virtual;
  public
    procedure Execute; override;
  end;

  { TdxSpreadSheetXLSXReaderDrawingPictureContainerParser }

  TdxSpreadSheetXLSXReaderDrawingPictureContainerParser = class(TdxSpreadSheetXLSXReaderDrawingShapeContainerParser)
  protected
    procedure ReadPicture(ANode: TdxXMLNode; APicture: TdxSpreadSheetPicture); virtual;
    procedure ReadPictureContainerDescription(ANode: TdxXMLNode; AContainer: TdxSpreadSheetPictureContainer); virtual;
    procedure ReadPictureContainerProperties(ANode: TdxXMLNode; AContainer: TdxSpreadSheetPictureContainer); virtual;
    procedure ReadPictureResource(ANode: TdxXMLNode; APicture: TdxSpreadSheetPicture); virtual;
  public
    procedure Execute; override;
  end;

  { TdxSpreadSheetXLSXReaderExternalLinkParser }

  TdxSpreadSheetXLSXReaderExternalLinkParser = class(TdxSpreadSheetXLSXReaderCustomDocumentParser)
  protected
    function DecodePath(const APath: AnsiString): TdxUnicodeString;
  public
    procedure Execute; override;
  end;

  { TdxSpreadSheetXLSXReaderFontParser }

  TdxSpreadSheetXLSXReaderFontParser = class(TdxSpreadSheetXLSXReaderCustomParser)
  strict private
    FFont: TdxSpreadSheetFontHandle;
    FNode: TdxXMLNode;
  protected
    procedure ProcessParam(ANode: TdxXMLNode; AUserData: Pointer);
  public
    constructor Create(AFont: TdxSpreadSheetFontHandle; ANode: TdxXMLNode; AOwner: TdxSpreadSheetXLSXReader);
    procedure Execute; override;
    //
    property Font: TdxSpreadSheetFontHandle read FFont;
  end;

  { TdxSpreadSheetXLSXReaderFormulasParser }

  TdxSpreadSheetXLSXReaderFormulasParser = class(TdxSpreadSheetXLSXReaderCustomParser)
  public
    procedure Execute; override;
  end;

  { TdxSpreadSheetXLSXReaderHeaderFooterParser }

  TdxSpreadSheetXLSXReaderHeaderFooterParser = class(TdxSpreadSheetXLSXReaderCustomNodeParser)
  strict private
    FText: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText;
  public
    constructor Create(ANode: TdxXMLNode; AOwner: TdxSpreadSheetXLSXReader;
      AText: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
    procedure Execute; override;
    //
    property Text: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText read FText;
  end;

  { TdxSpreadSheetXLSXReaderRichTextHelper }

  TdxSpreadSheetXLSXReaderRichTextHelper = class
  public
    Owner: TdxSpreadSheetXLSXReader;
    Runs: TdxSpreadSheetFormattedSharedStringRuns;
    Text: TdxUnicodeString;

    constructor Create(AOwner: TdxSpreadSheetXLSXReader); virtual;
    destructor Destroy; override;
  end;

  { TdxSpreadSheetXLSXReaderSharedStringParser }

  TdxSpreadSheetXLSXReaderSharedStringParser = class(TdxSpreadSheetXLSXReaderCustomDocumentParser)
  protected
    procedure ProcessSharedString(ANode: TdxXMLNode; AUserData: Pointer);
  public
    procedure Execute; override;
  end;

  { TdxSpreadSheetXLSXReaderSharedStringParserHelper }

  TdxSpreadSheetXLSXReaderSharedStringParserHelper = class
  protected
    class procedure ProcessRichTextRun(ANode: TdxXMLNode; AUserData: Pointer);
  public
    class function Parse(AOwner: TdxSpreadSheetXLSXReader; ANode: TdxXMLNode): TdxSpreadSheetSharedString;
  end;

  { TdxSpreadSheetXLSXReaderStyleParser }

  TdxSpreadSheetXLSXReaderStyleParser = class(TdxSpreadSheetXLSXReaderCustomDocumentParser)
  protected
    procedure ProcessBorder(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessCellXfs(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessFill(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessFont(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessIndexedColors(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessNumberFormat(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessStyleCollection(ANode: TdxXMLNode; AUserData: Pointer); virtual;
  public
    procedure Execute; override;
  end;

  { TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper }

  TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper = class
  strict private
    FNodeCellStyleXf: TdxXMLNode;
    FNodeCellStyleXfAlignment: TdxXMLNode;
    FNodeCellStyleXfProtection: TdxXMLNode;
    FNodeCellXf: TdxXMLNode;
    FNodeCellXfAlignment: TdxXMLNode;
    FNodeCellXfProtection: TdxXMLNode;

    function FindAttr(const AAttrName: TdxXMLString; ANode, ANodeDefault: TdxXMLNode): TdxXMLNodeAttribute;
    function GetAlignmentNode(ANode: TdxXMLNode): TdxXMLNode;
    function GetCellXfsNode(ANode: TdxXMLNode; const ANodeName: TdxXMLString): TdxXMLNode;
    function GetProtectionNode(ANode: TdxXMLNode): TdxXMLNode;
  public
    constructor Create(ANode: TdxXMLNode);
    function GetAlignmentAttrValue(const AAttrName: TdxXMLString; const ADefaultValue: TdxXMLString = ''): TdxXMLString;
    function GetAlignmentAttrValueAsBoolean(const AAttrName: TdxXMLString; const ADefaultValue: Boolean = False): Boolean;
    function GetAlignmentAttrValueAsInteger(const AAttrName: TdxXMLString; const ADefaultValue: Integer = 0): Integer;
    function GetAttrValue(const AAttrName: TdxXMLString; ADefaultValue: Integer = 0): Integer;
    function GetNumberFormatHandle(AReader: TdxSpreadSheetXLSXReader): TObject;
    function GetProtectionAttrValueAsBoolean(const AAttrName: TdxXMLString; const ADefaultValue: Boolean = False): Boolean;
    function GetResourceHandle(const AResName, AOptionName: TdxXMLString; AResourceCollection: TdxSpreadSheetXLSXHashTableItemList): TObject;
  end;

  { TdxSpreadSheetXLSXReaderThemeParser }

  TdxSpreadSheetXLSXReaderThemeParser = class(TdxSpreadSheetXLSXReaderCustomDocumentParser)
  strict private
    function GetColorMap: TDictionary<TdxXMLString, TColor>; inline;
    function GetThemedBrushes: TObjectList<TdxGPBrush>; inline;
    function GetThemedColors: TList; inline;
    function GetThemedPens: TObjectList<TdxGPPen>; inline;
  protected
    function ProcessColor(ANode: TdxXMLNode): TColor;
    procedure ProcessFillStyle(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessLineStyle(ANode: TdxXMLNode; AUserData: Pointer); virtual;

    procedure ReadColorSchema(ANode: TdxXMLNode); virtual;
  public
    procedure Execute; override;
    //
    property ColorMap: TDictionary<TdxXMLString, TColor> read GetColorMap;
    property ThemedBrushes: TObjectList<TdxGPBrush> read GetThemedBrushes;
    property ThemedColors: TList read GetThemedColors;
    property ThemedPens: TObjectList<TdxGPPen> read GetThemedPens;
  end;

  { TdxSpreadSheetXLSXReaderWorkbookParser }

  TdxSpreadSheetXLSXReaderWorkbookParser = class(TdxSpreadSheetXLSXReaderCustomParser)
  strict private
    FFileName: AnsiString;
    FLocalSheetIdMap: TDictionary<string, TdxSpreadSheetCustomView>;
    FRels: TdxSpreadSheetXLSXReaderRels;
  protected
    procedure ParseWorkbook; virtual;
    procedure ParseWorkbookDependencies; virtual;
    procedure ParseWorkbookProperties(AWorkbook: TdxXMLDocument); virtual;
    //
    procedure ProcessDefinedName(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessExternalReference(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessWorksheet(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    //
    procedure ReadSharedStrings(const AFileName: AnsiString); virtual;
    procedure ReadStyles(const AFileName: AnsiString); virtual;
    procedure ReadTheme(const AFileName: AnsiString); virtual;
  public
    constructor Create(const AFileName: AnsiString; AOwner: TdxSpreadSheetXLSXReader);
    destructor Destroy; override;
    procedure Execute; override;
    //
    property FileName: AnsiString read FFileName;
    property Rels: TdxSpreadSheetXLSXReaderRels read FRels;
  end;

  { TdxSpreadSheetXLSXReaderWorksheetParser }

  TdxSpreadSheetXLSXReaderWorksheetParser = class(TdxSpreadSheetXLSXReaderCustomDocumentParser)
  strict private
    FView: TdxSpreadSheetTableView;

    function GetCellStyles: TdxSpreadSheetXLSXHashTableItemList;
    function GetChild(const AName: TdxXMLString; out ANode: TdxXMLNode): Boolean;
    function GetSharedStrings: TdxSpreadSheetXLSXHashTableItemList;
  protected
    function ConvertRowHeight(const AValue: Double): Integer; virtual;
    function ExtractColumnIndex(const S: TdxUnicodeString): Integer;

    procedure ProcessBreaks(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessColumn(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessFixedPain(ANode: TdxXMLNode); virtual;
    procedure ProcessHeaderFooter(ANode: TdxXMLNode); virtual;
    procedure ProcessMergedCells(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessPageMargins(ANode: TdxXMLNode); virtual;
    procedure ProcessPageSetup(ANode: TdxXMLNode); virtual;
    procedure ProcessPageSetupExProperties(ANode: TdxXMLNode); virtual;
    procedure ProcessPrintOptions(ANode: TdxXMLNode); virtual;
    procedure ProcessProperties(ANode: TdxXMLNode); virtual;
    procedure ProcessRow(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessRowCell(ANode: TdxXMLNode; AUserData: Pointer); virtual;
    procedure ProcessSelection(ANode: TdxXMLNode; ASelection: TdxSpreadSheetTableViewSelection); virtual;
    procedure ProcessViewProperties(ANode: TdxXMLNode); virtual;

    procedure ReadDrawing(const AFileName: AnsiString); virtual;
    procedure ReadDrawings; virtual;
  public
    constructor Create(const AFileName: AnsiString; AView: TdxSpreadSheetTableView; AOwner: TdxSpreadSheetXLSXReader);
    procedure Execute; override;
    //
    property CellStyles: TdxSpreadSheetXLSXHashTableItemList read GetCellStyles;
    property SharedStrings: TdxSpreadSheetXLSXHashTableItemList read GetSharedStrings;
    property View: TdxSpreadSheetTableView read FView;
  end;

implementation

uses
  AnsiStrings, Math, dxColorPicker, cxGraphics, dxSpreadSheetFormulas,
  dxSpreadSheetFormatXLSX, dxSpreadSheetFormatXLSXTags, dxSpreadSheetGraphics, dxSpreadSheetFormatUtils;

type
  TdxSpreadSheetPictureAccess = class(TdxSpreadSheetPicture);
  TdxSpreadSheetTableColumnAccess = class(TdxSpreadSheetTableColumn);
  TdxSpreadSheetTableRowAccess = class(TdxSpreadSheetTableRow);
  TdxSpreadSheetTableViewAccess = class(TdxSpreadSheetTableView);

{ TdxSpreadSheetXLSXFormulaAsTextInfoList }

procedure TdxSpreadSheetXLSXFormulaAsTextInfoList.Add(ACell: TdxSpreadSheetCell; AFunctionNode: TdxXMLNode);
var
  S: AnsiString;
begin
  S := AFunctionNode.Attributes.GetValue(sdxXLSXAttrCellType);
  if AFunctionNode.Text <> '' then
  begin
    Add(ACell, dxDefaultOperations[opEQ] + AFunctionNode.TextAsUnicodeString,
      SameText(S, sdxXLSXValueArray), SameText(S, sdxXLSXValueShared),
      TdxSpreadSheetXLSXCellRef.DecodeRange(AFunctionNode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrRef)));
  end;
end;

{ TdxSpreadSheetXLSXReaderContentIndex }

constructor TdxSpreadSheetXLSXReaderContentIndex.Create;
begin
  inherited Create;
  FData := TStringList.Create;
  FData.Delimiter := '|';
end;

destructor TdxSpreadSheetXLSXReaderContentIndex.Destroy;
begin
  FreeAndNil(FData);
  inherited Destroy;
end;

procedure TdxSpreadSheetXLSXReaderContentIndex.Load(AReader: TdxSpreadSheetXLSXReader; const AFileName: AnsiString);
var
  ADocument: TdxXMLDocument;
  ANode: TdxXMLNode;
begin
  FData.Clear;
  ADocument := AReader.ReadXML(AFileName);
  try
    if ADocument.Root.First <> nil then
    begin
      ANode := ADocument.Root.First.First;
      while ANode <> nil do
      begin
        if SameText(ANode.Name, sdxXLSXNodeOverride) then
          FData.Values[ANode.Attributes.GetValueAsString(sdxXLSXAttrPartName)] := ANode.Attributes.GetValueAsString(sdxXLSXAttrContentType);
        ANode := ANode.Next;
      end;
    end;
  finally
    ADocument.Free;
  end;
end;

function TdxSpreadSheetXLSXReaderContentIndex.GetContentType(const AFileName: AnsiString): AnsiString;
begin
  Result := dxStringToAnsiString(FData.Values[dxAnsiStringToString(AFileName)]);
end;

{ TdxSpreadSheetXLSXReaderRelsItem }

procedure TdxSpreadSheetXLSXReaderRelsItem.SetValue(const AFileName, AFileType: AnsiString);
begin
  FileName := AFileName;
  FileType := AFileType;
end;

{ TdxSpreadSheetXLSXReaderRels }

constructor TdxSpreadSheetXLSXReaderRels.Create;
begin
  inherited Create;
  FList := TcxObjectList.Create;
end;

destructor TdxSpreadSheetXLSXReaderRels.Destroy;
begin
  FreeAndNil(FList);
  inherited Destroy;
end;

procedure TdxSpreadSheetXLSXReaderRels.Clear;
begin
  FList.Clear;
end;

function TdxSpreadSheetXLSXReaderRels.Find(const ID: AnsiString): TdxSpreadSheetXLSXReaderRelsItem;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FList.Count - 1 do
    if SameText(ID, TdxSpreadSheetXLSXReaderRelsItem(FList.List[I]).ID) then
    begin
      Result := TdxSpreadSheetXLSXReaderRelsItem(FList.List[I]);
      Break;
    end;
end;

function TdxSpreadSheetXLSXReaderRels.FindByType(
  const AType: AnsiString; out AItem: TdxSpreadSheetXLSXReaderRelsItem): Boolean;
var
  I: Integer;
begin
  AItem := nil;
  for I := 0 to FList.Count - 1 do
    if SameText(AType, TdxSpreadSheetXLSXReaderRelsItem(FList.List[I]).FileType) then
    begin
      AItem := TdxSpreadSheetXLSXReaderRelsItem(FList.List[I]);
      Break;
    end;

  Result := AItem <> nil;
end;

procedure TdxSpreadSheetXLSXReaderRels.Load(AReader: TdxSpreadSheetXLSXReader; const AOwnerFileName: AnsiString);
var
  ADocument: TdxXMLDocument;
  AFileName: AnsiString;
  ANode: TdxXMLNode;
  ARootPath: AnsiString;
begin
  Clear;
  AFileName := TdxSpreadSheetXLSXUtils.GetRelsFileNameForFile(AOwnerFileName);
  if AReader.IsFileExists(AFileName) then
  begin
    ADocument := AReader.ReadXML(AFileName);
    try
      if ADocument.Root.First <> nil then
      begin
        ANode := ADocument.Root.First.First;
        ARootPath := Copy(AOwnerFileName, 1, LastDelimiter(dxUnixPathDelim, AOwnerFileName));
        while ANode <> nil do
        begin
          AFileName := ANode.Attributes.GetValue(sdxXLSXAttrTarget);
          AFileName := TdxZIPPathHelper.EncodePath(AFileName); 
          if not SameText(ANode.Attributes.GetValue(sdxXLSXAttrTargetMode), sdxXLSXValueTargetModeExternal) then
            AFileName := TdxZIPPathHelper.AbsoluteFileName(ARootPath, AFileName);
          Items[ANode.Attributes.GetValue(sdxXLSXAttrId)].SetValue(
            TdxZIPPathHelper.ExpandFileName(AFileName), ANode.Attributes.GetValue(sdxXLSXAttrType));
          ANode := ANode.Next;
        end;
      end;
    finally
      ADocument.Free;
    end;
  end;
end;

function TdxSpreadSheetXLSXReaderRels.GetCount: Integer;
begin
  Result := FList.Count;
end;

function TdxSpreadSheetXLSXReaderRels.GetItem(const ID: AnsiString): TdxSpreadSheetXLSXReaderRelsItem;
begin
  Result := Find(ID);
  if Result = nil then
  begin
    Result := TdxSpreadSheetXLSXReaderRelsItem.Create;
    Result.ID := ID;
    FList.Add(Result);
  end;
end;

function TdxSpreadSheetXLSXReaderRels.GetItemByIndex(Index: Integer): TdxSpreadSheetXLSXReaderRelsItem;
begin
  Result := TdxSpreadSheetXLSXReaderRelsItem(FList.Items[Index]);
end;

{ TdxSpreadSheetXLSXReaderCustomParser }

function TdxSpreadSheetXLSXReaderCustomParser.CheckListIndex(AIndex: Integer;
  AList: TList; const AMessage: string; AMessageType: TdxSpreadSheetMessageType): Boolean;
begin
  Result := (AIndex >= 0) and (AIndex < AList.Count);
  if not Result then
    DoError(AMessage, [AIndex], AMessageType);
end;

function TdxSpreadSheetXLSXReaderCustomParser.CheckListIndex(AIndex: Integer;
  AList: TdxSpreadSheetXLSXHashTableItemList; const AMessage: string; AMessageType: TdxSpreadSheetMessageType): Boolean;
begin
  Result := (AIndex >= 0) and (AIndex < AList.Count);
  if not Result then
    DoError(AMessage, [AIndex], AMessageType);
end;

function TdxSpreadSheetXLSXReaderCustomParser.DecodeColor(ANode: TdxXMLNode): TColor;
var
  AAttr: TdxXMLNodeAttribute;
begin
  Result := clDefault;
  if ANode.Attributes.Find(sdxXLSXAttrRGB, AAttr) then
    Result := DecodeColor(AAttr.ValueAsString)
  else
    if ANode.Attributes.Find(sdxXLSXAttrTheme, AAttr) then
      ExtractThemedColor(ANode, AAttr.ValueAsInteger, Result)
    else
      if ANode.Attributes.Find(sdxXLSXAttrIndexed, AAttr) then
        ExtractIndexedColor(ANode, AAttr.ValueAsInteger, Result)
      else
        Result := clNone;
end;

function TdxSpreadSheetXLSXReaderCustomParser.DecodeColor(const HexCode: string): TColor;
begin
  Result := dxAlphaColorToColor(TdxColorPickerHexCodeHelper.HexCodeToAlphaColor(HexCode, cphnHTML));
end;

procedure TdxSpreadSheetXLSXReaderCustomParser.ExtractIndexedColor(ANode: TdxXMLNode; AColorIndex: Integer; var AColor: TColor);
begin
  if Owner.IndexedColors.Count > 0 then
  begin
    if AColorIndex >= Owner.IndexedColors.Count then
      AColor := clDefault
    else
      AColor := Owner.IndexedColors[AColorIndex];
  end
  else
  begin
    if AColorIndex >= 8 then
      Dec(AColorIndex, 8);

    if (AColorIndex >= Low(dxExcelStandardColors)) and (AColorIndex <= High(dxExcelStandardColors)) then
      AColor := dxExcelStandardColors[AColorIndex]
    else
      case AColorIndex of
        56, 57:
          AColor := clDefault;
      else
        DoError(sdxErrorInvalidColorIndex, [AColorIndex], ssmtWarning);
      end;
  end;
end;

procedure TdxSpreadSheetXLSXReaderCustomParser.ExtractThemedColor(ANode: TdxXMLNode; AThemeIndex: Integer; var AColor: TColor);
var
  AAttr: TdxXMLNodeAttribute;
  AColorHSL: TdxHSL;
  ATintValue: Double;
begin
  if CheckListIndex(AThemeIndex, Owner.ThemedColors, sdxErrorInvalidColorIndex, ssmtWarning) then
  begin
    AColor := TColor(Owner.ThemedColors[AThemeIndex]);
    if ANode.Attributes.Find(sdxXLSXAttrThemeTint, AAttr) then
    begin
      ATintValue := AAttr.ValueAsFloat;
      if not IsZero(ATintValue) then
      begin
        AColorHSL := TdxColorSpaceConverter.ColorToHSL(AColor);
        if ATintValue < 0 then
          AColorHSL.L := AColorHSL.L * (1 + ATintValue)
        else
          AColorHSL.L := AColorHSL.L * (1 - ATintValue) + ATintValue;
        AColor := TdxColorSpaceConverter.HSLToColor(AColorHSL);
      end;
    end;
  end;
end;

function TdxSpreadSheetXLSXReaderCustomParser.GetOwner: TdxSpreadSheetXLSXReader;
begin
  Result := TdxSpreadSheetXLSXReader(inherited Owner);
end;

{ TdxSpreadSheetXLSXReaderCustomNodeParser }

constructor TdxSpreadSheetXLSXReaderCustomNodeParser.Create(ANode: TdxXMLNode; AOwner: TdxSpreadSheetXLSXReader);
begin
  inherited Create(AOwner);
  FNode := ANode;
end;

{ TdxSpreadSheetXLSXReaderCustomDocumentParser }

constructor TdxSpreadSheetXLSXReaderCustomDocumentParser.Create(const AFileName: AnsiString; AOwner: TdxSpreadSheetXLSXReader);
begin
  inherited Create(nil, AOwner);
  FDocumentFileName := AFileName;
  FDocument := ReadXML(AFileName);
  FNode := Document.Root.First;
  FRels := TdxSpreadSheetXLSXReaderRels.Create;
  FRels.Load(Owner, AFileName);
end;

destructor TdxSpreadSheetXLSXReaderCustomDocumentParser.Destroy;
begin
  FreeAndNil(FDocument);
  FreeAndNil(FRels);
  inherited Destroy;
end;

{ TdxSpreadSheetXLSXReaderCustomDocumentSubParser }

constructor TdxSpreadSheetXLSXReaderCustomDocumentSubParser.Create(
  ANode: TdxXMLNode; AOwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser);
begin
  inherited Create(ANode, AOwnerParser.Owner);
  FOwnerParser := AOwnerParser;
end;

function TdxSpreadSheetXLSXReaderCustomDocumentSubParser.ReadImage(ANode: TdxXMLNode): TStream;

  procedure DoReadEmbeded(const AValue: TdxXMLString; var AStream: TStream);
  var
    AItem: TdxSpreadSheetXLSXReaderRelsItem;
  begin
    AItem := OwnerParser.Rels.Find(AValue);
    if AItem = nil then
      DoError(sdxErrorPictureCannotBeFound, [OwnerParser.DocumentFileName + ':' + AValue], ssmtWarning)
    else
      if Owner.IsFileExists(AItem.FileName) then
        AStream := Owner.ReadFile(AItem.FileName)
      else
        DoError(sdxErrorPictureCannotBeFound, [AItem.FileName], ssmtWarning);
  end;

var
  AAttr: TdxXMLNodeAttribute;
begin
  Result := nil;
  if ANode.Attributes.Find(sdxXLSXAttrDrawingResourceEmbed, AAttr) then
    DoReadEmbeded(AAttr.Value, Result)
  else
      DoError(sdxErrorPictureCannotBeFound, [OwnerParser.DocumentFileName], ssmtWarning);
end;

{ TdxSpreadSheetXLSXHashTableItemList }

procedure TdxSpreadSheetXLSXHashTableItemList.Notify(const Item: TdxHashTableItem; Action: TCollectionNotification);
begin
  inherited Notify(Item, Action);
  case Action of
    cnAdded:
      Item.AddRef;
    cnRemoved:
      Item.Release;
  end;
end;

{ TdxSpreadSheetXLSXReader }

constructor TdxSpreadSheetXLSXReader.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner, AStream);
  FIndexedColors := TList<TColor>.Create;
  FFormulasRefs := TdxSpreadSheetXLSXFormulaAsTextInfoList.Create(SpreadSheet);
  FSharedStrings := TdxSpreadSheetXLSXHashTableItemList.Create;
  FThemedColors := TList.Create;
  FStyles := TdxSpreadSheetXLSXHashTableItemList.Create;
  FFormats := TdxSpreadSheetXLSXHashTableItemList.Create;
  FBorders := TdxSpreadSheetXLSXHashTableItemList.Create;
  FFills := TdxSpreadSheetXLSXHashTableItemList.Create;
  FFonts := TdxSpreadSheetXLSXHashTableItemList.Create;
  FThemedBrushes := TObjectList<TdxGPBrush>.Create;
  FThemedPens := TObjectList<TdxGPPen>.Create;
  FColorMap := TDictionary<TdxXMLString, TColor>.Create;
end;

destructor TdxSpreadSheetXLSXReader.Destroy;
begin
  FreeAndNil(FColorMap);
  FreeAndNil(FIndexedColors);
  FreeAndNil(FThemedBrushes);
  FreeAndNil(FThemedPens);
  FreeAndNil(FContentIndex);
  FreeAndNil(FColumnWidthHelper);
  FreeAndNil(FFormats);
  FreeAndNil(FBorders);
  FreeAndNil(FStyles);
  FreeAndNil(FFills);
  FreeAndNil(FFonts);
  FreeAndNil(FSharedStrings);
  FreeAndNil(FFormulasRefs);
  FreeAndNil(FThemedColors);
  inherited Destroy;
end;

function TdxSpreadSheetXLSXReader.GetWorkbookFileName(out AFileName: AnsiString): Boolean;
const
  WorkbookContentType = AnsiString('spreadsheetml.sheet.main');
var
  AContentType: AnsiString;
  AItem: TdxSpreadSheetXLSXReaderRelsItem;
  ARels: TdxSpreadSheetXLSXReaderRels;
begin
  Result := False;
  ARels := TdxSpreadSheetXLSXReaderRels.Create;
  try
    ARels.Load(Self, '');
    if ARels.FindByType(sdxXLSXWorkbookRelationship, AItem) then
    begin
      AContentType := LowerCase(ContentIndex.ContentType[dxUnixPathDelim + AItem.FileName]);
      Result := (AContentType = '') or (Pos(WorkbookContentType, AContentType) > 0);
      if Result then
        AFileName := AItem.FileName;
    end;
  finally
    ARels.Free;
  end;
end;

procedure TdxSpreadSheetXLSXReader.ReadData;
var
  AFileName: AnsiString;
begin
  if GetWorkbookFileName(AFileName) then
  begin
    ExecuteSubTask(TdxSpreadSheetXLSXReaderWorkbookParser.Create(AFileName, Self));
    ExecuteSubTask(TdxSpreadSheetXLSXReaderFormulasParser.Create(Self));
  end
  else
    DoError(sdxErrorInvalidDocumentType, ssmtError);
end;

function TdxSpreadSheetXLSXReader.CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
begin
  Result := TdxSpreadSheetCustomFilerProgressHelper.Create(Self, 5);
end;

procedure TdxSpreadSheetXLSXReader.ForEachNodeChild(ANode: TdxXMLNode; AProc: TdxXMLNodeForEachProc; AUserData: Pointer);
var
  AChildNode: TdxXMLNode;
begin
  if ANode <> nil then
  begin
    ProgressHelper.BeginStage(ANode.Count);
    try
      AChildNode := ANode.First;
      while AChildNode <> nil do
      begin
        AProc(AChildNode, AUserData);
        ProgressHelper.NextTask;
        AChildNode := AChildNode.Next;
      end;
    finally
      ProgressHelper.EndStage;
    end;
  end
  else
    ProgressHelper.SkipStage;
end;

function TdxSpreadSheetXLSXReader.GetColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper;
begin
  if FColumnWidthHelper = nil then
  begin
    FColumnWidthHelper := TdxSpreadSheetExcelColumnWidthHelper.Create;
    CellStyles.Fonts.DefaultFont.AssignToFont(FColumnWidthHelper.Font);
  end;
  Result := FColumnWidthHelper;
end;

function TdxSpreadSheetXLSXReader.GetContentIndex: TdxSpreadSheetXLSXReaderContentIndex;
begin
  if FContentIndex = nil then
  begin
    FContentIndex := TdxSpreadSheetXLSXReaderContentIndex.Create;
    FContentIndex.Load(Self, sdxXLSXContentTypeFileName);
  end;
  Result := FContentIndex;
end;

{ TdxSpreadSheetXLSXReaderDrawingParser }

constructor TdxSpreadSheetXLSXReaderDrawingParser.Create(
  const AFileName: AnsiString; AOwner: TdxSpreadSheetXLSXReader; AView: TdxSpreadSheetTableView);
begin
  inherited Create(AFileName, AOwner);
  FView := AView;
end;

procedure TdxSpreadSheetXLSXReaderDrawingParser.Execute;
begin
  if Node <> nil then
    Node.ForEach(ProcessContainer);
end;

procedure TdxSpreadSheetXLSXReaderDrawingParser.ProcessContainer(ANode: TdxXMLNode; AUserData: Pointer);
begin
  if ANode.FindChild(sdxXLSXNodeDrawingPictureContainer) <> nil then
    ProcessContainerPicture(ANode)
  else
    if ANode.FindChild(sdxXLSXNodeDrawingShapeContainer) <> nil then
      ProcessContainerShape(ANode);
end;

procedure TdxSpreadSheetXLSXReaderDrawingParser.ProcessContainerPicture(ANode: TdxXMLNode);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingPictureContainerParser.Create(ANode, Self));
end;

procedure TdxSpreadSheetXLSXReaderDrawingParser.ProcessContainerShape(ANode: TdxXMLNode);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.Create(ANode, Self));
end;

{ TdxSpreadSheetXLSXReaderDrawingBrushParser }

constructor TdxSpreadSheetXLSXReaderDrawingBrushParser.Create(ANode: TdxXMLNode;
  ABrush: TdxGPBrush; AOwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser);
begin
  inherited Create(ANode, AOwnerParser);
  FBrush := ABrush;
end;

procedure TdxSpreadSheetXLSXReaderDrawingBrushParser.Execute;
begin
  if SameText(Node.Name, sdxXLSXNodeSolidFill) then
    ReadSolidBrushParameters
  else

  if SameText(Node.Name, sdxXLSXNodeTexturedFill) then
    ReadTexturedBrushParameters
  else

  if SameText(Node.Name, sdxXLSXNodeGradientFill) then
    ReadGradientBrushParameters
  else

  if SameText(Node.Name, sdxXLSXNodePatternFill) then
    ReadPatternBrushParameters;
end;

procedure TdxSpreadSheetXLSXReaderDrawingBrushParser.ProcessGradientBrushPoints(ANode: TdxXMLNode; AUserData: Pointer);
var
  APosition: Double;
begin
  APosition := TdxSpreadSheetXLSXUtils.DecodePercents(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrGradientPointPos)) / 100;
  TdxGPBrush(AUserData).GradientPoints.Add(Min(Max(APosition, 0), 1), ReadColor(ANode));
end;

function TdxSpreadSheetXLSXReaderDrawingBrushParser.ReadColor(AParentNode: TdxXMLNode): TdxAlphaColor;

  function ReadAlpha(AColorNode: TdxXMLNode): Byte;
  var
    AAlphaNode: TdxXMLNode;
  begin
    if AColorNode.FindChild(sdxXLSXNodeColorAlpha, AAlphaNode) then
      Result := TdxSpreadSheetXLSXUtils.DecodeColorAlpha(AAlphaNode.Attributes.GetValueAsInteger(sdxXLSXAttrVal))
    else
      Result := MaxByte;
  end;

  function ReadSchemeColor(AColorNode: TdxXMLNode): TColor;
  var
    AColorHSL: TdxHSL;
    ANode: TdxXMLNode;
  begin
    if Owner.ColorMap.TryGetValue(AColorNode.Attributes.GetValue(sdxXLSXAttrVal), Result) then
    begin
      AColorHSL := TdxColorSpaceConverter.ColorToHSL(Result);
      if AColorNode.FindChild(sdxXLSXNodeLumMod, ANode) then
        AColorHSL.L := AColorHSL.L * TdxSpreadSheetXLSXUtils.DecodePercents(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrVal)) / 100;
      if AColorNode.FindChild(sdxXLSXNodeLumOff, ANode) then
        AColorHSL.L := AColorHSL.L + TdxSpreadSheetXLSXUtils.DecodePercents(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrVal)) / 100;
      AColorHSL.L := Min(Max(AColorHSL.L, 0), 1);
      Result := TdxColorSpaceConverter.HSLToColor(AColorHSL);
    end
    else
      Result := clNone;
  end;

var
  AChildNode: TdxXMLNode;
begin
  if AParentNode.FindChild(sdxXLSXNodeThemesCustomColor, AChildNode) then
    Result := dxColorToAlphaColor(DecodeColor(AChildNode.Attributes.GetValueAsString(sdxXLSXAttrVal)), ReadAlpha(AChildNode))
  else
    if AParentNode.FindChild(sdxXLSXNodeSchemeColor, AChildNode) then
      Result := dxColorToAlphaColor(ReadSchemeColor(AChildNode), ReadAlpha(AChildNode))
    else
      Result := clNone;
end;

procedure TdxSpreadSheetXLSXReaderDrawingBrushParser.ReadGradientBrushParameters;
var
  AChildNode: TdxXMLNode;
  AInverseOrder: Boolean;
begin
  Brush.Style := gpbsGradient;
  if Node.FindChild(sdxXLSXNodeGradientPoints, AChildNode) then
  begin
    Brush.GradientPoints.Clear;
    AChildNode.ForEach(ProcessGradientBrushPoints, Brush);
  end;

  if Node.FindChild(sdxXLSXNodeLinearGradientFill, AChildNode) then
  begin
    Brush.GradientMode := dxSpreadSheetGetGradientMode(TdxSpreadSheetXLSXUtils.DecodePositiveFixedAngle(
      AChildNode.Attributes.GetValueAsInteger(sdxXLSXAttrAngle)), AInverseOrder);
    if AInverseOrder then
      Brush.GradientPoints.InvertOrder;
  end;
end;

procedure TdxSpreadSheetXLSXReaderDrawingBrushParser.ReadPatternBrushParameters;
var
  ABackgroundColor: TdxAlphaColor;
  AChildNode: TdxXMLNode;
  AForegroundColor: TdxAlphaColor;
begin
  if Node.FindChild(sdxXLSXNodeDrawingPatternBackgroundColor, AChildNode) then
    ABackgroundColor := ReadColor(AChildNode)
  else
    ABackgroundColor := 0;

  if Node.FindChild(sdxXLSXNodeDrawingPatternForegroundColor, AChildNode) then
    AForegroundColor := ReadColor(AChildNode)
  else
    AForegroundColor := 0;

  dxSpreadSheetLoadBrushPattern(Brush, HInstance, 'XLSX_BRUSHPATTERN_' +
    UpperCase(Node.Attributes.GetValueAsString(sdxXLSXAttrPreset)), AForegroundColor, ABackgroundColor);
end;

procedure TdxSpreadSheetXLSXReaderDrawingBrushParser.ReadSolidBrushParameters;
begin
  Brush.Style := gpbsSolid;
  Brush.Color := ReadColor(Node);
end;

procedure TdxSpreadSheetXLSXReaderDrawingBrushParser.ReadTexturedBrushParameters;
var
  AChildNode: TdxXMLNode;
  AStream: TStream;
begin
  Brush.Style := gpbsClear;
  if Node.FindChild(sdxXLSXNodeDrawingBlip, AChildNode) then
  begin
    AStream := ReadImage(AChildNode);
    if AStream <> nil then
    try
      Brush.Texture.LoadFromStream(AStream);
      Brush.Style := gpbsTexture;
    finally
      AStream.Free;
    end;
  end;
end;

{ TdxSpreadSheetXLSXReaderDrawingPenParser }

constructor TdxSpreadSheetXLSXReaderDrawingPenParser.Create(
  ANode: TdxXMLNode; APen: TdxGPPen; AOwnerParser: TdxSpreadSheetXLSXReaderCustomDocumentParser);
begin
  inherited Create(ANode, AOwnerParser);
  FPen := APen;
end;

procedure TdxSpreadSheetXLSXReaderDrawingPenParser.Execute;
var
  AAttr: TdxXMLNodeAttribute;
  AChildNode: TdxXMLNode;
begin
  if Node.Attributes.Find(sdxXLSXAttrLineWidth, AAttr) then
    Pen.Width := dxEMUToPixelsF(AAttr.ValueAsInt64);
  if Node.FindChild(sdxXLSXNodeLineDash, AChildNode) then
    Pen.Style := TdxSpreadSheetXLSXHelper.StringToPenStyle(AChildNode.Attributes.GetValue(sdxXLSXAttrVal));
  if Node.Count > 0 then
    ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingBrushParser.Create(Node.First, Pen.Brush, OwnerParser));
end;

{ TdxSpreadSheetXLSXReaderContainerParser }

procedure TdxSpreadSheetXLSXReaderContainerParser.ReadDescription(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
begin
  AContainer.Name := ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrName);
  AContainer.Description := ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrAlternateText);
  AContainer.Title := ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrTitle);
end;

procedure TdxSpreadSheetXLSXReaderContainerParser.ReadEditAsMode(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
var
  AEditAs: TdxXMLString;
begin
  AEditAs := ANode.Attributes.GetValue(sdxXLSXAttrEditAs, sdxXLSXValueEditAsTwoCell);
  AContainer.AnchorPoint1.FixedToCell := SameText(AEditAs, sdxXLSXValueEditAsOneCell) or SameText(AEditAs, sdxXLSXValueEditAsTwoCell);
  AContainer.AnchorPoint2.FixedToCell := SameText(AEditAs, sdxXLSXValueEditAsTwoCell);
end;

procedure TdxSpreadSheetXLSXReaderContainerParser.ReadPlacement(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
begin
  AContainer.BeginUpdate;
  try
    if SameText(ANode.Name, sdxXLSXNodeAnchorTwoCell) then
      ReadTwoCellAnchor(ANode, AContainer)
    else

    if SameText(ANode.Name, sdxXLSXNodeAnchorOneCell) then
      ReadOneCellAnchor(ANode, AContainer)
    else

    if SameText(ANode.Name, sdxXLSXNodeAnchorAbsolute) then
      ReadAbsoluteAnchor(ANode, AContainer)
    else
      DoError(sdxErrorInvalidAnchorDefinition, ssmtWarning);
  finally
    AContainer.EndUpdate;
  end;
end;

function TdxSpreadSheetXLSXReaderContainerParser.ReadPoint(ANode: TdxXMLNode; const XName, YName: TdxXMLString): TPoint;
begin
  try
    Result.X := dxEMUToPixels(StrToInt64(ANode.Attributes.GetValueAsString(XName)));
    Result.Y := dxEMUToPixels(StrToInt64(ANode.Attributes.GetValueAsString(YName)));
  except
    DoError(sdxErrorInvalidAnchorDefinition, ssmtWarning);
    Result := cxNullPoint;
  end;
end;

procedure TdxSpreadSheetXLSXReaderContainerParser.ReadAbsoluteAnchor(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
var
  AChildNode: TdxXMLNode;
begin
  AContainer.AnchorType := catAbsolute;
  if ANode.FindChild(sdxXLSXNodePos, AChildNode) then
    AContainer.AnchorPoint1.Offset := ReadPoint(AChildNode, sdxXLSXAttrCoordX, sdxXLSXAttrCoordY);
  if ANode.FindChild(sdxXLSXNodeExt, AChildNode) then
    AContainer.AnchorPoint2.Offset := ReadPoint(AChildNode, sdxXLSXAttrCoordExtX, sdxXLSXAttrCoordExtY);
end;

procedure TdxSpreadSheetXLSXReaderContainerParser.ReadOneCellAnchor(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
var
  AChildNode: TdxXMLNode;
begin
  AContainer.AnchorType := catOneCell;
  if ANode.FindChild(sdxXLSXNodeAnchorFrom, AChildNode) then
    ReadAnchorMarker(AChildNode, AContainer.AnchorPoint1);
  if ANode.FindChild(sdxXLSXNodeExt, AChildNode) then
    AContainer.AnchorPoint2.Offset := ReadPoint(AChildNode, sdxXLSXAttrCoordExtX, sdxXLSXAttrCoordExtY);
  ReadEditAsMode(ANode, AContainer);
end;

procedure TdxSpreadSheetXLSXReaderContainerParser.ReadTwoCellAnchor(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer);
var
  AChildNode: TdxXMLNode;
begin
  AContainer.AnchorType := catTwoCell;
  if ANode.FindChild(sdxXLSXNodeAnchorFrom, AChildNode) then
    ReadAnchorMarker(AChildNode, AContainer.AnchorPoint1);
  if ANode.FindChild(sdxXLSXNodeAnchorTo, AChildNode) then
    ReadAnchorMarker(AChildNode, AContainer.AnchorPoint2);
  ReadEditAsMode(ANode, AContainer);
end;

function TdxSpreadSheetXLSXReaderContainerParser.GetOwnerParser: TdxSpreadSheetXLSXReaderDrawingParser;
begin
  Result := inherited OwnerParser as TdxSpreadSheetXLSXReaderDrawingParser;
end;

procedure TdxSpreadSheetXLSXReaderContainerParser.ReadAnchorMarker(ANode: TdxXMLNode; AAnchor: TdxSpreadSheetContainerAnchorPoint);
var
  AColumnNode: TdxXMLNode;
  AOffset: TPoint;
  ARowNode: TdxXMLNode;
begin
  try
    if ANode.FindChild(sdxXLSXNodeDrawingPosMarkerRow, ARowNode) and ANode.FindChild(sdxXLSXNodeDrawingPosMarkerColumn, AColumnNode) then
      AAnchor.Cell := OwnerParser.View.CreateCell(StrToInt(ARowNode.TextAsString), StrToInt(AColumnNode.TextAsString))
    else
      AAnchor.Cell := nil;

    AOffset := cxNullPoint;
    if ANode.FindChild(sdxXLSXNodeDrawingPosMarkerColumnOffset, AColumnNode) then
      AOffset.X := dxEMUToPixels(StrToInt64(AColumnNode.TextAsString));
    if ANode.FindChild(sdxXLSXNodeDrawingPosMarkerRowOffset, ARowNode) then
      AOffset.Y := dxEMUToPixels(StrToInt64(ARowNode.TextAsString));
    AAnchor.Offset := AOffset;
  except
    DoError(sdxErrorInvalidAnchorDefinition, ssmtWarning);
  end;
end;

{ TdxSpreadSheetXLSXReaderDrawingShapeContainerParser }

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.Execute;
var
  AChildNode: TdxXMLNode;
  AContainer: TdxSpreadSheetShapeContainer;
begin
  AContainer := OwnerParser.View.Containers.AddShapeContainer;
  AContainer.BeginUpdate;
  try
    ReadPlacement(Node, AContainer);
    if Node.FindChild([sdxXLSXNodeDrawingShapeContainer, sdxXLSXNodeDrawingStyle], AChildNode) then
      ReadShapeDefaultStyles(AChildNode, AContainer.Shape);
    if Node.FindChild([sdxXLSXNodeDrawingShapeContainer, sdxXLSXNodeDrawingShapeDesription, sdxXLSXNodeDrawingDescription], AChildNode) then
      ReadDescription(AChildNode, AContainer);
    if Node.FindChild([sdxXLSXNodeDrawingShapeContainer, sdxXLSXNodeDrawingShapeProperties], AChildNode) then
      ReadShapeProperties(AChildNode, AContainer);
    if Node.FindChild([sdxXLSXNodeDrawingShapeContainer, sdxXLSXNodeDrawingShapeDesription,
      sdxXLSXNodeDrawingShapeAttributesEx, sdxXLSXNodeDrawingShapeLocks], AChildNode)
    then
      ReadContainerRestrictions(AChildNode, AContainer);
  finally
    AContainer.EndUpdate;
  end;
end;

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.ReadShapeFillParameters(
  ANode: TdxXMLNode; AShape: TdxSpreadSheetShape);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingBrushParser.Create(ANode, AShape.Brush, OwnerParser));
end;

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.ReadContainerRestrictions(
  ANode: TdxXMLNode; AContainer: TdxSpreadSheetShapeContainer);
var
  AIndex: TdxSpreadSheetContainerRestriction;
begin
  for AIndex := Low(TdxSpreadSheetContainerRestriction) to High(TdxSpreadSheetContainerRestriction) do
  begin
    if ANode.Attributes.GetValueAsBoolean(dxXLSXRestrictionNames[AIndex]) then
      AContainer.Restrictions := AContainer.Restrictions + [AIndex]
    else
      AContainer.Restrictions := AContainer.Restrictions - [AIndex];
  end;
end;

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.ReadShapeDefaultStyles(
  ANode: TdxXMLNode; AShape: TdxSpreadSheetShape);
var
  AChildNode: TdxXMLNode;
  AIndex: Integer;
begin
  if ANode.FindChild(sdxXLSXNodeLineRef, AChildNode) then
  begin
    AIndex := AChildNode.Attributes.GetValueAsInteger(sdxXLSXAttrRefIndex);
    if (AIndex >= 0) and (AIndex < Owner.ThemedPens.Count) then
      AShape.Pen.Assign(Owner.ThemedPens[AIndex]);
  end;

  if ANode.FindChild(sdxXLSXNodeFillRef, AChildNode) then
  begin
    AIndex := AChildNode.Attributes.GetValueAsInteger(sdxXLSXAttrRefIndex);
    if (AIndex >= 0) and (AIndex < Owner.ThemedBrushes.Count) then
      AShape.Brush.Assign(Owner.ThemedBrushes[AIndex]);
  end;
end;

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.ReadShapeGeometry(
  ANode: TdxXMLNode; AShape: TdxSpreadSheetShape);
begin
  AShape.ShapeType := TdxSpreadSheetXLSXHelper.StringToShapeType(ANode.Attributes.GetValue(sdxXLSXAttrPreset));
end;

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.ReadShapeLineParameters(ANode: TdxXMLNode; APen: TdxGPPen);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingPenParser.Create(ANode, APen, OwnerParser));
end;

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.ReadShapeProperties(
  ANode: TdxXMLNode; AContainer: TdxSpreadSheetShapeContainer);
var
  AChildNode: TdxXMLNode;
begin
  if ANode.FindChild(sdxXLSXNodeDrawingXForm, AChildNode) then
    ReadTransformProperties(AChildNode, AContainer);

  if ANode.FindChild(sdxXLSXNodeDrawingShapeGeometry, AChildNode) then
    ReadShapeGeometry(AChildNode, AContainer.Shape);

  if ANode.FindChild(sdxXLSXNodeLine, AChildNode) then
    ReadShapeLineParameters(AChildNode, AContainer.Shape.Pen);

  if ANode.FindChild(sdxXLSXNodeSolidFill, AChildNode) or
     ANode.FindChild(sdxXLSXNodeTexturedFill, AChildNode) or
     ANode.FindChild(sdxXLSXNodeGradientFill, AChildNode) or
     ANode.FindChild(sdxXLSXNodePatternFill, AChildNode)
  then
    ReadShapeFillParameters(AChildNode, AContainer.Shape);
end;

procedure TdxSpreadSheetXLSXReaderDrawingShapeContainerParser.ReadTransformProperties(
  ANode: TdxXMLNode; AContainer: TdxSpreadSheetShapeContainer);
begin
  AContainer.Transform.RotationAngle := TdxSpreadSheetXLSXUtils.DecodePositiveFixedAngle(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrRot));
  AContainer.Transform.FlipHorizontally := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrFlipH);
  AContainer.Transform.FlipVertically := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrFlipV);
end;

{ TdxSpreadSheetXLSXReaderDrawingPictureContainerParser }

procedure TdxSpreadSheetXLSXReaderDrawingPictureContainerParser.Execute;
var
  AContainer: TdxSpreadSheetPictureContainer;
  AChildNode: TdxXMLNode;
begin
  AContainer := OwnerParser.View.Containers.AddPictureContainer;
  AContainer.BeginUpdate;
  try
    ReadPlacement(Node, AContainer);
    if Node.FindChild([sdxXLSXNodeDrawingPictureContainer, sdxXLSXNodeDrawingPictureDescription], AChildNode) then
      ReadPictureContainerDescription(AChildNode, AContainer);
    if Node.FindChild([sdxXLSXNodeDrawingPictureContainer, sdxXLSXNodeDrawingBlipFill], AChildNode) then
      ReadPicture(AChildNode, AContainer.Picture);
    if Node.FindChild([sdxXLSXNodeDrawingPictureContainer, sdxXLSXNodeDrawingShapeProperties], AChildNode) then
      ReadShapeProperties(AChildNode, AContainer);
  finally
    AContainer.EndUpdate;
  end;
end;

procedure TdxSpreadSheetXLSXReaderDrawingPictureContainerParser.ReadPicture(
  ANode: TdxXMLNode; APicture: TdxSpreadSheetPicture);
var
  AChildNode: TdxXMLNode;
begin
  if ANode.FindChild(sdxXLSXNodeDrawingBlip, AChildNode) then
    ReadPictureResource(AChildNode, APicture);
  if ANode.FindChild(sdxXLSXNodeDrawingAttributeSourceRect, AChildNode) and (AChildNode.Attributes.Count > 0) then
  begin
    APicture.CropMargins := TdxSpreadSheetXLSXUtils.DecodeSourceRect(cxRect(
      AChildNode.Attributes.GetValueAsInteger('l'), AChildNode.Attributes.GetValueAsInteger('t'),
      AChildNode.Attributes.GetValueAsInteger('r'), AChildNode.Attributes.GetValueAsInteger('b')));
  end;
end;

procedure TdxSpreadSheetXLSXReaderDrawingPictureContainerParser.ReadPictureContainerProperties(ANode: TdxXMLNode;
  AContainer: TdxSpreadSheetPictureContainer);
var
  AAttr: TdxXMLNodeAttribute;
begin
  if ANode.Attributes.Find(sdxXLSXAttrPreferRelativeResize, AAttr) then
    AContainer.RelativeResize := AAttr.ValueAsBoolean;
end;

procedure TdxSpreadSheetXLSXReaderDrawingPictureContainerParser.ReadPictureContainerDescription(
  ANode: TdxXMLNode; AContainer: TdxSpreadSheetPictureContainer);
var
  AChildNode: TdxXMLNode;
begin
  if ANode.FindChild(sdxXLSXNodeDrawingDescription, AChildNode) then
    ReadDescription(AChildNode, AContainer);
  if ANode.FindChild(sdxXLSXNodeDrawingPictureAttributes, AChildNode) then
    ReadPictureContainerProperties(AChildNode, AContainer);
  if ANode.FindChild([sdxXLSXNodeDrawingPictureAttributes, sdxXLSXNodeDrawingPictureLocks], AChildNode) then
    ReadContainerRestrictions(AChildNode, AContainer);
end;

procedure TdxSpreadSheetXLSXReaderDrawingPictureContainerParser.ReadPictureResource(
  ANode: TdxXMLNode; APicture: TdxSpreadSheetPicture);
var
  AStream: TStream;
begin
  AStream := ReadImage(ANode);
  if AStream <> nil then
  try
    TdxSpreadSheetPictureAccess(APicture).ImageHandle := Owner.AddImage(AStream);
  finally
    AStream.Free;
  end;
end;

{ TdxSpreadSheetXLSXReaderExternalLinkParser }

procedure TdxSpreadSheetXLSXReaderExternalLinkParser.Execute;
var
  AChildNode: TdxXMLNode;
  ARelsItem: TdxSpreadSheetXLSXReaderRelsItem;
  ARID: AnsiString;
begin
  if (Node <> nil) and Node.FindChild(sdxXLSXNodeExternalBook, AChildNode) then
    ARID := AChildNode.Attributes.GetValue(sdxXLSXAttrRId)
  else
    ARID := '';

  ARelsItem := Rels.Find(ARID);
  if ARelsItem <> nil then
    SpreadSheet.ExternalLinks.Add(DecodePath(ARelsItem.FileName))
  else
    DoError(sdxErrorInvalidRelationshipId, [ARID], ssmtError);
end;

function TdxSpreadSheetXLSXReaderExternalLinkParser.DecodePath(const APath: AnsiString): TdxUnicodeString;
begin
  Result := dxXMLStringToUnicodeString(APath);
end;

{ TdxSpreadSheetXLSXReaderFontParser }

constructor TdxSpreadSheetXLSXReaderFontParser.Create(
  AFont: TdxSpreadSheetFontHandle; ANode: TdxXMLNode; AOwner: TdxSpreadSheetXLSXReader);
begin
  inherited Create(AOwner);
  FFont := AFont;
  FNode := ANode;
end;

procedure TdxSpreadSheetXLSXReaderFontParser.Execute;
begin
  FNode.ForEach(ProcessParam);
end;

procedure TdxSpreadSheetXLSXReaderFontParser.ProcessParam(ANode: TdxXMLNode; AUserData: Pointer);
var
  AStyle: TFontStyle;
begin
  if SameText(ANode.Name, sdxXLSXNodeSZ) then
    Font.Size := Round(ANode.Attributes.GetValueAsFloat(sdxXLSXAttrVal))
  else

  if SameText(ANode.Name, sdxXLSXNodeName) or SameText(ANode.Name, sdxXLSXNodeFontName) then
    Font.Name := ANode.Attributes.GetValueAsString(sdxXLSXAttrVal)
  else

  if SameText(ANode.Name, sdxXLSXNodeCharset) then
    Font.Charset := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrVal)
  else

  if SameText(ANode.Name, sdxXLSXNodeColor) then
    Font.Color := DecodeColor(ANode)
  else
    for AStyle := Low(AStyle) to High(AStyle) do
      if SameText(dxXLSXFontStyles[AStyle], ANode.Name) then
      begin
        Font.Style := Font.Style + [AStyle];
        Break;
      end;
end;

{ TdxSpreadSheetXLSXReaderFormulasParser }

procedure TdxSpreadSheetXLSXReaderFormulasParser.Execute;
begin
  try
    Owner.FormulasRefs.ResolveReferences;
  except
    on E: Exception do
      DoError(E.Message, ssmtError);
  end;
end;

{ TdxSpreadSheetXLSXReaderHeaderFooterParser }

constructor TdxSpreadSheetXLSXReaderHeaderFooterParser.Create(ANode: TdxXMLNode;
  AOwner: TdxSpreadSheetXLSXReader; AText: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText);
begin
  inherited Create(ANode, AOwner);
  FText := AText;
end;

procedure TdxSpreadSheetXLSXReaderHeaderFooterParser.Execute;
begin
  if Node <> nil then
    TdxSpreadSheetHeaderFooterHelper.Parse(Text, Node.TextAsUnicodeString);
end;

{ TdxSpreadSheetXLSXReaderRichTextHelper }

constructor TdxSpreadSheetXLSXReaderRichTextHelper.Create(AOwner: TdxSpreadSheetXLSXReader);
begin
  inherited Create;
  Owner := AOwner;
  Runs := TdxSpreadSheetFormattedSharedStringRuns.Create;
end;

destructor TdxSpreadSheetXLSXReaderRichTextHelper.Destroy;
begin
  FreeAndNil(Runs);
  inherited Destroy;
end;

{ TdxSpreadSheetXLSXReaderSharedStringParser }

procedure TdxSpreadSheetXLSXReaderSharedStringParser.Execute;
begin
  Owner.ForEachNodeChild(Node, ProcessSharedString, nil)
end;

procedure TdxSpreadSheetXLSXReaderSharedStringParser.ProcessSharedString(ANode: TdxXMLNode; AUserData: Pointer);
begin
  Owner.SharedStrings.Add(TdxSpreadSheetXLSXReaderSharedStringParserHelper.Parse(Owner, ANode));
end;

{ TdxSpreadSheetXLSXReaderSharedStringParserHelper }

class function TdxSpreadSheetXLSXReaderSharedStringParserHelper.Parse(
  AOwner: TdxSpreadSheetXLSXReader; ANode: TdxXMLNode): TdxSpreadSheetSharedString;
var
  AFormattedString: TdxSpreadSheetFormattedSharedString;
  AHelper: TdxSpreadSheetXLSXReaderRichTextHelper;
  ATextNode: TdxXMLNode;
begin
  if ANode.FindChild(sdxXLSXNodeText, ATextNode) then
    Result := AOwner.AddSharedString(dxXMLStringToUnicodeString(ATextNode.Text))
  else
  begin
    AHelper := TdxSpreadSheetXLSXReaderRichTextHelper.Create(AOwner);
    try
      ANode.ForEach(ProcessRichTextRun, AHelper);
      AFormattedString := AOwner.CreateTempFormattedSharedString(AHelper.Text);
      AFormattedString.Runs.Assign(AHelper.Runs);
      Result := AOwner.AddFormattedSharedString(AFormattedString);
    finally
      AHelper.Free;
    end;
  end;
end;

class procedure TdxSpreadSheetXLSXReaderSharedStringParserHelper.ProcessRichTextRun(ANode: TdxXMLNode; AUserData: Pointer);
var
  AFont: TdxSpreadSheetFontHandle;
  AHelper: TdxSpreadSheetXLSXReaderRichTextHelper;
  AParagraphNode: TdxXMLNode;
  ATextNode: TdxXMLNode;
begin
  if SameText(ANode.Name, sdxXLSXNodeRichTextRun) then
  begin
    AHelper := TdxSpreadSheetXLSXReaderRichTextHelper(AUserData);
    if ANode.FindChild(sdxXLSXNodeText, ATextNode) and (Length(ATextNode.Text) > 0) then
    begin
      AFont := AHelper.Owner.CreateTempFontHandle;
      if ANode.FindChild(sdxXLSXNodeRichTextRunParagraph, AParagraphNode) then
        AHelper.Owner.ExecuteSubTask(TdxSpreadSheetXLSXReaderFontParser.Create(AFont, AParagraphNode, AHelper.Owner));
      AHelper.Runs.Add(Length(AHelper.Text) + 1, AHelper.Owner.AddFont(AFont));
      AHelper.Text := AHelper.Text + ATextNode.TextAsUnicodeString;
    end;
  end;
end;

{ TdxSpreadSheetXLSXReaderStyleParser }

procedure TdxSpreadSheetXLSXReaderStyleParser.Execute;
var
  AChildNode: TdxXMLNode;
begin
  if Node.FindChild(['colors', 'indexedColors'], AChildNode) then
    AChildNode.ForEach(ProcessIndexedColors);
  Owner.ForEachNodeChild(Node, ProcessStyleCollection, nil);
end;

procedure TdxSpreadSheetXLSXReaderStyleParser.ProcessBorder(ANode: TdxXMLNode; AUserData: Pointer);
var
  ABorder: TcxBorder;
  AChildNode: TdxXMLNode;
  AHandle: TdxSpreadSheetBordersHandle;
begin
  AHandle := Owner.CreateTempBordersHandle;
  for ABorder := Low(ABorder) to High(ABorder) do
  begin
    if ANode.FindChild(dxXLSXBorderNames[ABorder], AChildNode) then
    begin
      AHandle.BorderStyle[ABorder] := TdxSpreadSheetXLSXHelper.StringToBorderStyle(AChildNode.Attributes.GetValue(sdxXLSXAttrStyle));
      if AChildNode.FindChild(sdxXLSXNodeColor, AChildNode) then
        AHandle.BorderColor[ABorder] := DecodeColor(AChildNode);
    end;
  end;
  Owner.Borders.Add(Owner.AddBorders(AHandle));
end;

procedure TdxSpreadSheetXLSXReaderStyleParser.ProcessCellXfs(ANode: TdxXMLNode; AUserData: Pointer);
var
  AHandle: TdxSpreadSheetCellStyleHandle;
  AHelper: TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper;
begin
  AHelper := TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.Create(ANode);
  try
    AHandle := Owner.CreateTempCellStyle(
      TdxSpreadSheetFontHandle(AHelper.GetResourceHandle(sdxXLSXAttrFontId, sdxXLSXAttrApplyFont, Owner.Fonts)),
      TdxSpreadSheetFormatHandle(AHelper.GetNumberFormatHandle(Owner)),
      TdxSpreadSheetBrushHandle(AHelper.GetResourceHandle(sdxXLSXAttrFillId, sdxXLSXAttrApplyFill, Owner.Fills)),
      TdxSpreadSheetBordersHandle(AHelper.GetResourceHandle(sdxXLSXAttrBorderId, sdxXLSXAttrApplyBorder, Owner.Borders)));

    AHandle.AlignHorz := TdxSpreadSheetXLSXHelper.StringToAlignHorz(AHelper.GetAlignmentAttrValue(sdxXLSXAttrHorizontal));
    AHandle.AlignHorzIndent := AHelper.GetAlignmentAttrValueAsInteger(sdxXLSXAttrIndent);
    if AHandle.AlignHorzIndent <> 0 then
      AHandle.AlignHorzIndent := Owner.ColumnWidthHelper.SpacesNumberToPixels(AHandle.AlignHorzIndent);
    AHandle.AlignVert := TdxSpreadSheetXLSXHelper.StringToAlignVert(AHelper.GetAlignmentAttrValue(sdxXLSXAttrVertical));
    AHandle.Rotation := AHelper.GetAlignmentAttrValueAsInteger(sdxXLSXAttrTextRotation);
    AHandle.States := [];
    if AHelper.GetAlignmentAttrValueAsBoolean(sdxXLSXAttrShrinkToFit) then
      AHandle.States := AHandle.States + [csShrinkToFit];
    if AHelper.GetAlignmentAttrValueAsBoolean(sdxXLSXAttrWrapText) then
      AHandle.States := AHandle.States + [csWordWrap];
    if AHelper.GetProtectionAttrValueAsBoolean(sdxXLSXAttrLocked, True) then
      AHandle.States := AHandle.States + [csLocked];
    if AHelper.GetProtectionAttrValueAsBoolean(sdxXLSXAttrHidden) then
      AHandle.States := AHandle.States + [csHidden];

    if Owner.Styles.Add(Owner.AddCellStyle(AHandle)) = 0 then
      SpreadSheet.DefaultCellStyle.Handle := TdxSpreadSheetCellStyleHandle(Owner.Styles.First);
  finally
    AHelper.Free;
  end;
end;

procedure TdxSpreadSheetXLSXReaderStyleParser.ProcessFill(ANode: TdxXMLNode; AUserData: Pointer);
var
  ABrush: TdxSpreadSheetBrushHandle;
  AChildNode: TdxXMLNode;
begin
  ABrush := Owner.CreateTempBrushHandle;
  if ANode.FindChild(sdxXLSXNodeCellStylePatternFill, ANode) then
  begin
    ABrush.Style := TdxSpreadSheetXLSXHelper.StringToFillStyle(ANode.Attributes.GetValue(sdxXLSXAttrPatternType));
    if ANode.FindChild(sdxXLSXNodeBackgroundColor, AChildNode) then
      ABrush.BackgroundColor := DecodeColor(AChildNode);
    if ANode.FindChild(sdxXLSXNodeForegroundColor, AChildNode) then
      ABrush.ForegroundColor := DecodeColor(AChildNode);
    if ABrush.Style = sscfsSolid then
      ABrush.BackgroundColor := ABrush.ForegroundColor;
  end;
  Owner.Fills.Add(Owner.AddBrush(ABrush));
end;

procedure TdxSpreadSheetXLSXReaderStyleParser.ProcessFont(ANode: TdxXMLNode; AUserData: Pointer);
var
  AFont: TdxSpreadSheetFontHandle;
begin
  AFont := Owner.CreateTempFontHandle;
  ExecuteSubTask(TdxSpreadSheetXLSXReaderFontParser.Create(AFont, ANode, Owner));
  if Owner.Fonts.Count = 0 then
    Owner.CellStyles.Fonts.DefaultFont.Assign(AFont);
  Owner.Fonts.Add(Owner.AddFont(AFont));
end;

procedure TdxSpreadSheetXLSXReaderStyleParser.ProcessIndexedColors(ANode: TdxXMLNode; AUserData: Pointer);
begin
  Owner.IndexedColors.Add(DecodeColor(ANode.Attributes.GetValueAsString(sdxXLSXAttrRGB)));
end;

procedure TdxSpreadSheetXLSXReaderStyleParser.ProcessNumberFormat(ANode: TdxXMLNode; AUserData: Pointer);
begin
  Owner.Formats.Add(Owner.AddNumberFormat(
    ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrFormatCode),
    ANode.Attributes.GetValueAsInteger(sdxXLSXAttrNumFmtId, -1)));
end;

procedure TdxSpreadSheetXLSXReaderStyleParser.ProcessStyleCollection(ANode: TdxXMLNode; AUserData: Pointer);
begin
  if SameText(ANode.Name, sdxXLSXNodeStyleNumberFormats) then
    ANode.ForEach(ProcessNumberFormat)
  else

  if SameText(ANode.Name, sdxXLSXNodeStyleFonts) then
    ANode.ForEach(ProcessFont)
  else

  if SameText(ANode.Name, sdxXLSXNodeStyleFills) then
    ANode.ForEach(ProcessFill)
  else

  if SameText(ANode.Name, sdxXLSXNodeStyleBorders) then
    ANode.ForEach(ProcessBorder)
  else

  if SameText(ANode.Name, sdxXLSXNodeStyleCellXfs) then
    ANode.ForEach(ProcessCellXfs);
end;

{ TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper }

constructor TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.Create(ANode: TdxXMLNode);
begin
  inherited Create;
  FNodeCellXf := ANode;
  FNodeCellXfAlignment := GetAlignmentNode(FNodeCellXf);
  FNodeCellXfProtection := GetProtectionNode(FNodeCellXf);

  FNodeCellStyleXf := GetCellXfsNode(ANode, sdxXLSXNodeStyleCellStyleXfs);
  FNodeCellStyleXfAlignment := GetAlignmentNode(FNodeCellStyleXf);
  FNodeCellStyleXfProtection := GetProtectionNode(FNodeCellStyleXf);
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.FindAttr(
  const AAttrName: TdxXMLString; ANode, ANodeDefault: TdxXMLNode): TdxXMLNodeAttribute;
begin
  if (ANode = nil) or not ANode.Attributes.Find(AAttrName, Result) then
  begin
    if (ANodeDefault = nil) or not ANodeDefault.Attributes.Find(AAttrName, Result) then
      Result := nil;
  end;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetAlignmentAttrValue(
  const AAttrName, ADefaultValue: TdxXMLString): TdxXMLString;
var
  AAttr: TdxXMLNodeAttribute;
begin
  AAttr := FindAttr(AAttrName, FNodeCellXfAlignment, FNodeCellStyleXfAlignment);
  if AAttr <> nil then
    Result := AAttr.Value
  else
    Result := ADefaultValue;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetAlignmentAttrValueAsBoolean(
  const AAttrName: TdxXMLString; const ADefaultValue: Boolean = False): Boolean;
var
  AAttr: TdxXMLNodeAttribute;
begin
  AAttr := FindAttr(AAttrName, FNodeCellXfAlignment, FNodeCellStyleXfAlignment);
  if AAttr <> nil then
    Result := AAttr.ValueAsBoolean
  else
    Result := ADefaultValue;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetAlignmentAttrValueAsInteger(
  const AAttrName: TdxXMLString; const ADefaultValue: Integer = 0): Integer;
begin
  Result := StrToIntDef(dxAnsiStringToString(GetAlignmentAttrValue(AAttrName)), ADefaultValue);
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetAttrValue(
  const AAttrName: TdxXMLString; ADefaultValue: Integer = 0): Integer;
var
  AAttr: TdxXMLNodeAttribute;
begin
  Result := ADefaultValue;
  AAttr := FindAttr(AAttrName, FNodeCellXf, FNodeCellStyleXf);
  if AAttr <> nil then
  try
    Result := AAttr.ValueAsInteger;
  except
    Result := ADefaultValue;
  end;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetAlignmentNode(ANode: TdxXMLNode): TdxXMLNode;
begin
  Result := nil;
  if ANode <> nil then
    if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrApplyAlignment) then
    begin
      if not ANode.FindChild(sdxXLSXNodeAlignment, Result) then
        Result := nil;
    end;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetCellXfsNode(ANode: TdxXMLNode; const ANodeName: TdxXMLString): TdxXMLNode;
var
  AChildNode: TdxXMLNode;
  AValue: Integer;
begin
  Result := nil;
  if ANode.Parent.Parent.FindChild(ANodeName, AChildNode) then
  begin
    AValue := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrXFId, -1);
    if (AValue >= 0) and (AValue < AChildNode.Count) then
      Result := AChildNode.Items[AValue];
  end;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetNumberFormatHandle(AReader: TdxSpreadSheetXLSXReader): TObject;
var
  AResIndex: Integer;
  I: Integer;
begin
  Result := nil;
  if GetAttrValue(sdxXLSXAttrApplyNumberFormat) <> 0 then
  begin
    AResIndex := GetAttrValue(sdxXLSXAttrNumFmtId, -1);
    Result := AReader.SpreadSheet.CellStyles.Formats.PredefinedFormats.GetFormatHandleByID(AResIndex);
    if Result = nil then
      for I := 0 to AReader.Formats.Count - 1 do
        if TdxSpreadSheetFormatHandle(AReader.Formats[I]).FormatCodeID = AResIndex then
        begin
          Result := TdxSpreadSheetFormatHandle(AReader.Formats[I]);
          Break;
        end;
  end;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetProtectionAttrValueAsBoolean(
  const AAttrName: TdxXMLString; const ADefaultValue: Boolean = False): Boolean;
var
  AAttr: TdxXMLNodeAttribute;
begin
  AAttr := FindAttr(AAttrName, FNodeCellXfProtection, FNodeCellStyleXfProtection);
  if AAttr <> nil then
    Result := AAttr.ValueAsBoolean
  else
    Result := ADefaultValue;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetProtectionNode(ANode: TdxXMLNode): TdxXMLNode;
begin
  Result := nil;
  if ANode <> nil then
    if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrApplyProtection) then
    begin
      if not ANode.FindChild(sdxXLSXNodeProtection, Result) then
        Result := nil;
    end;
end;

function TdxSpreadSheetXLSXReaderStyleParserCellStyleHelper.GetResourceHandle(
  const AResName, AOptionName: TdxXMLString; AResourceCollection: TdxSpreadSheetXLSXHashTableItemList): TObject;
var
  AResIndex: Integer;
begin
  Result := nil;
  if GetAttrValue(AOptionName, 1) <> 0 then
  begin
    AResIndex := GetAttrValue(AResName, -1);
    if (AResIndex >= 0) and (AResIndex < AResourceCollection.Count) then
      Result := TObject(AResourceCollection[AResIndex])
  end;
end;

{ TdxSpreadSheetXLSXReaderThemeParser }

procedure TdxSpreadSheetXLSXReaderThemeParser.Execute;
var
  AChildNode: TdxXMLNode;
begin
  if Node.FindChild([sdxXLSXNodeThemesElements, sdxXLSXNodeThemesColorScheme], AChildNode) then
    ReadColorSchema(AChildNode);
  if Node.FindChild([sdxXLSXNodeThemesElements, sdxXLSXNodeThemesFormatScheme, sdxXLSXNodeThemesFormatSchemeFillStyleList], AChildNode) then
    AChildNode.ForEach(ProcessFillStyle);
  if Node.FindChild([sdxXLSXNodeThemesElements, sdxXLSXNodeThemesFormatScheme, sdxXLSXNodeThemesFormatSchemeLineStyleList], AChildNode) then
    AChildNode.ForEach(ProcessLineStyle);
end;

procedure TdxSpreadSheetXLSXReaderThemeParser.ReadColorSchema(ANode: TdxXMLNode);
const
  Names: array[0..11] of AnsiString = (
    'lt1', 'dk1', 'lt2', 'dk2', 'accent1', 'accent2', 'accent3',
    'accent4', 'accent5', 'accent6', 'hlink', 'folHlink'
  );
var
  AChildNode: TdxXMLNode;
  AColor: TColor;
  I: Integer;
begin
  for I := Low(Names) to High(Names) do
  begin
    if ANode.FindChild('a:' + Names[I], AChildNode) and (AChildNode.First <> nil) then
      AColor := ProcessColor(AChildNode.First)
    else
    begin
      AColor := clDefault;
      DoError(sdxErrorColorValueIsNotSpecified, ssmtWarning);
    end;

    ColorMap.Add(Names[I], AColor);
    ThemedColors.Add(Pointer(AColor));
  end;

  ColorMap.AddOrSetValue('tx1', ColorMap.Items['dk2']);
  ColorMap.AddOrSetValue('tx2', ColorMap.Items['dk1']);
  ColorMap.AddOrSetValue('bg1', ColorMap.Items['lt1']);
  ColorMap.AddOrSetValue('bg2', ColorMap.Items['lt2']);
  ColorMap.AddOrSetValue('phClr', ColorMap.Items['accent1']);
end;

function TdxSpreadSheetXLSXReaderThemeParser.ProcessColor(ANode: TdxXMLNode): TColor;
begin
  Result := clDefault;
  if SameText(ANode.Name, sdxXLSXNodeThemesSystemColor) then
  begin
    if not IdentToColor('cl' + ANode.Attributes.GetValueAsString(sdxXLSXAttrVal), Integer(Result)) then
      DoError(sdxErrorInvalidColor, [ANode.Attributes.GetValue(sdxXLSXAttrVal)], ssmtWarning);
  end
  else
    if SameText(ANode.Name, sdxXLSXNodeThemesCustomColor) then
    try
      Result := DecodeColor(ANode.Attributes.GetValueAsString(sdxXLSXAttrVal));
    except
      DoError(sdxErrorInvalidColor, [ANode.Attributes.GetValue(sdxXLSXAttrVal)], ssmtWarning);
    end
    else
      DoError(sdxErrorColorValueIsNotSpecified, ssmtWarning);
end;

procedure TdxSpreadSheetXLSXReaderThemeParser.ProcessFillStyle(ANode: TdxXMLNode; AUserData: Pointer);
var
  ABrush: TdxGPBrush;
begin
  ABrush := TdxGPBrush.Create;
  ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingBrushParser.Create(ANode, ABrush, Self));
  ThemedBrushes.Add(ABrush);
end;

procedure TdxSpreadSheetXLSXReaderThemeParser.ProcessLineStyle(ANode: TdxXMLNode; AUserData: Pointer);
var
  APen: TdxGPPen;
begin
  APen := TdxGPPen.Create;
  ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingPenParser.Create(ANode, APen, Self));
  ThemedPens.Add(APen);
end;

function TdxSpreadSheetXLSXReaderThemeParser.GetColorMap: TDictionary<TdxXMLString, TColor>;
begin
  Result := Owner.ColorMap;
end;

function TdxSpreadSheetXLSXReaderThemeParser.GetThemedBrushes: TObjectList<TdxGPBrush>;
begin
  Result := Owner.ThemedBrushes;
end;

function TdxSpreadSheetXLSXReaderThemeParser.GetThemedColors: TList;
begin
  Result := Owner.ThemedColors;
end;

function TdxSpreadSheetXLSXReaderThemeParser.GetThemedPens: TObjectList<TdxGPPen>;
begin
  Result := Owner.ThemedPens;
end;

{ TdxSpreadSheetXLSXReaderWorkbookParser }

constructor TdxSpreadSheetXLSXReaderWorkbookParser.Create(const AFileName: AnsiString; AOwner: TdxSpreadSheetXLSXReader);
begin
  inherited Create(AOwner);
  FFileName := AFileName;
  FLocalSheetIdMap := TDictionary<string, TdxSpreadSheetCustomView>.Create;
  FRels := TdxSpreadSheetXLSXReaderRels.Create;
  FRels.Load(Owner, AFileName);
end;

destructor TdxSpreadSheetXLSXReaderWorkbookParser.Destroy;
begin
  FreeAndNil(FLocalSheetIdMap);
  FreeAndNil(FRels);
  inherited Destroy;
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.Execute;
begin
  ParseWorkbookDependencies; 
  ParseWorkbook;
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ParseWorkbook;
var
  AWorkbook: TdxXMLDocument;
begin
  AWorkbook := ReadXML(FileName);
  try
    ParseWorkbookProperties(AWorkbook);
    Owner.ForEachNodeChild(AWorkbook.FindChild([sdxXLSXNodeWorkbook, sdxXLSXNodeSheets]), ProcessWorksheet, nil);
    Owner.ForEachNodeChild(AWorkbook.FindChild([sdxXLSXNodeWorkbook, sdxXLSXNodeExternalReferences]), ProcessExternalReference, nil);
    Owner.ForEachNodeChild(AWorkbook.FindChild([sdxXLSXNodeWorkbook, sdxXLSXNodeDefinedNames]), ProcessDefinedName, nil);
  finally
    AWorkbook.Free;
  end;
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ParseWorkbookDependencies;
var
  AItem: TdxSpreadSheetXLSXReaderRelsItem;
begin
  if Rels.FindByType(sdxXLSXThemeRelationship, AItem) then
    ReadTheme(AItem.FileName);
  if Rels.FindByType(sdxXLSXStyleRelationship, AItem) then
    ReadStyles(AItem.FileName);
  if Rels.FindByType(sdxXLSXSharedStringRelationship, AItem) then
    ReadSharedStrings(AItem.FileName);
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ParseWorkbookProperties(AWorkbook: TdxXMLDocument);
const
  Map: array[Boolean] of TdxSpreadSheetDateTimeSystem = (dts1900, dts1904);
var
  AIs1904DateSystem: Boolean;
  AIsdxXLSXR1C1ReferenceMode: Boolean;
  AIterate: Boolean;
  AIterateCount: Integer;
  ANode: TdxXMLNode;
  AProtected: Boolean;
begin
  if AWorkbook.FindChild([sdxXLSXNodeWorkbook, sdxXLSXNodeCalcPr], ANode) then
  begin
    AIsdxXLSXR1C1ReferenceMode := SameText(ANode.Attributes.GetValue(sdxXLSXAttrRefMode), sdxXLSXValueR1C1);
    AIterateCount := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrIterateCount, 100);
    AIterate := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrIterate);
  end
  else
  begin
    AIsdxXLSXR1C1ReferenceMode := False;
    AIterateCount := 100;
    AIterate := False;
  end;

  if AWorkbook.FindChild([sdxXLSXNodeWorkbook, sdxXLSXNodeWorkbookPr], ANode) then
    AIs1904DateSystem := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrDate1904)
  else
    AIs1904DateSystem := False;

  if AWorkbook.FindChild([sdxXLSXNodeWorkbook, sdxXLSXNodeWorkbookProtection], ANode) then
    AProtected := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrLockStructure)
  else
    AProtected := False;

  SpreadSheet.OptionsBehavior.Protected := AProtected;
  SpreadSheet.OptionsBehavior.IterativeCalculation := AIterate;
  SpreadSheet.OptionsBehavior.IterativeCalculationMaxCount := AIterateCount;
  SpreadSheet.OptionsView.R1C1Reference := AIsdxXLSXR1C1ReferenceMode;
  SpreadSheet.OptionsView.DateTimeSystem := Map[AIs1904DateSystem];
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ProcessDefinedName(ANode: TdxXMLNode; AUserData: Pointer);
var
  AAttr: TdxXMLNodeAttribute;
  ADefinedName: TdxSpreadSheetDefinedName;
  AScope: TdxSpreadSheetCustomView;
begin
  AScope := nil;
  if ANode.Attributes.Find(sdxXLSXAttrLocalSheetId, AAttr) then
  begin
    if not FLocalSheetIdMap.TryGetValue(AAttr.ValueAsString, AScope) then
      DoError(sdxErrorInvalidSheetId, [AAttr.ValueAsString], ssmtWarning);
  end;

  ADefinedName := SpreadSheet.DefinedNames.AddOrSet(
    ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrName),
    ANode.TextAsUnicodeString, AScope);
  if ADefinedName.Caption = sdxXLSXPrintAreaDefinedName then
    TdxSpreadSheetPrintAreaHelper.AssignFromDefinedName(AScope, ADefinedName);
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ProcessExternalReference(ANode: TdxXMLNode; AUserData: Pointer);
var
  ARelsItem: TdxSpreadSheetXLSXReaderRelsItem;
begin
  ARelsItem := Rels.Find(ANode.Attributes.GetValue(sdxXLSXAttrRId));
  if ARelsItem <> nil then
    ExecuteSubTask(TdxSpreadSheetXLSXReaderExternalLinkParser.Create(ARelsItem.FileName, Owner))
  else
    DoError(sdxErrorInvalidRelationshipId, [ANode.Attributes.GetValue(sdxXLSXAttrRId)], ssmtError);
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ProcessWorksheet(ANode: TdxXMLNode; AUserData: Pointer);
var
  AItem: TdxSpreadSheetXLSXReaderRelsItem;
  AView: TdxSpreadSheetTableView;
begin
  AItem := Rels.Find(ANode.Attributes.GetValue(sdxXLSXAttrRId));
  if AItem = nil then
    DoError(sdxErrorInvalidRelationshipId, [ANode.Attributes.GetValue(sdxXLSXAttrRId)], ssmtError)
  else
    if SameText(AItem.FileType, sdxXLSXWorksheetRelationship) then
    begin
      AView := Owner.AddTableView(ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrName));
      FLocalSheetIdMap.Add(IntToStr(ANode.Index), AView);
      ExecuteSubTask(TdxSpreadSheetXLSXReaderWorksheetParser.Create(AItem.FileName, AView, Owner));
    end;
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ReadSharedStrings(const AFileName: AnsiString);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderSharedStringParser.Create(AFileName, Owner));
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ReadStyles(const AFileName: AnsiString);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderStyleParser.Create(AFileName, Owner));
end;

procedure TdxSpreadSheetXLSXReaderWorkbookParser.ReadTheme(const AFileName: AnsiString);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderThemeParser.Create(AFileName, Owner));
end;

{ TdxSpreadSheetXLSXReaderWorksheetParser }

constructor TdxSpreadSheetXLSXReaderWorksheetParser.Create(
  const AFileName: AnsiString; AView: TdxSpreadSheetTableView; AOwner: TdxSpreadSheetXLSXReader);
begin
  inherited Create(AFileName, AOwner);
  FView := AView;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.Execute;
var
  ANode: TdxXMLNode;
begin
  if GetChild(sdxXLSXNodeSheetProtection, ANode) then
    View.Options.Protected := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrSheet)
  else
    View.Options.Protected := False;

  if GetChild(sdxXLSXNodeColumns, ANode) then
    ANode.ForEach(ProcessColumn);
  if GetChild(sdxXLSXNodeSheetData, ANode) then
    ANode.ForEach(ProcessRow);
  if GetChild(sdxXLSXNodeMergeCells, ANode) then
    ANode.ForEach(ProcessMergedCells);
  if GetChild(sdxXLSXNodeSheetFormatPr, ANode) then
    ProcessProperties(ANode);
  if GetChild(sdxXLSXNodeHeaderFooter, ANode) then
    ProcessHeaderFooter(ANode);
  if GetChild(sdxXLSXNodePageMargins, ANode) then
    ProcessPageMargins(ANode);
  if GetChild(sdxXLSXNodePageSetup, ANode) then
    ProcessPageSetup(ANode);
  if GetChild(sdxXLSXNodeRowBreaks, ANode) then
    ANode.ForEach(ProcessBreaks, View.OptionsPrint.Pagination.RowPageBreaks);
  if GetChild(sdxXLSXNodeColBreaks, ANode) then
    ANode.ForEach(ProcessBreaks, View.OptionsPrint.Pagination.ColumnPageBreaks);
  if GetChild(sdxXLSXNodeSheetPr, ANode) and ANode.FindChild(sdxXLSXNodePageSetUpPr, ANode) then
    ProcessPageSetupExProperties(ANode);
  if GetChild(sdxXLSXNodePrintOptions, ANode) then
    ProcessPrintOptions(ANode);
  if GetChild(sdxXLSXNodeSheetsView, ANode) and ANode.FindChild(sdxXLSXNodeSheetView, ANode) then
    ProcessViewProperties(ANode);
  ReadDrawings;
end;

function TdxSpreadSheetXLSXReaderWorksheetParser.ConvertRowHeight(const AValue: Double): Integer;
begin
  Result := cxGetValueCurrentDPI(Round(100 / 75 * AValue)) + 1;
end;

function TdxSpreadSheetXLSXReaderWorksheetParser.ExtractColumnIndex(const S: TdxUnicodeString): Integer;
var
  X: Integer;
begin
  TdxSpreadSheetXLSXCellRef.Decode(S, Result, X);
  if Result < 0 then
  begin
    DoError(sdxErrorInvalidColumnIndex, [S], ssmtError);
    Result := 0;
  end;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessBreaks(ANode: TdxXMLNode; AUserData: Pointer);
begin
  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrBreakManual) then
    TList<Cardinal>(AUserData).Add(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrBreakID));
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessColumn(ANode: TdxXMLNode; AUserData: Pointer);
var
  AAttr: TdxXMLNodeAttribute;
  AColumn: TdxSpreadSheetTableColumnAccess;
  AHidden: Boolean;
  AIsCustomWidth: Boolean;
  AMax: Integer;
  AMin: Integer;
  AStyleHandle: TdxSpreadSheetCellStyleHandle;
  AStyleIndex: Integer;
  AWidth: Integer;
  I: Integer;
begin

  AMin := Min(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrMin) - 1, MAXWORD);
  AMax := Min(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrMax) - 1, MAXWORD);

  AIsCustomWidth := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrCustomWidth);
  if ANode.Attributes.Find(sdxXLSXAttrWidth, AAttr) then
    AWidth := Owner.ColumnWidthHelper.WidthToPixels(AAttr.ValueAsFloat)
  else
    AWidth := -1;

  AStyleIndex := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrStyle, -1);
  if (AStyleIndex >= 0) and CheckListIndex(AStyleIndex, CellStyles, sdxErrorInvalidStyleIndex, ssmtError) then
    AStyleHandle := TdxSpreadSheetCellStyleHandle(CellStyles[AStyleIndex])
  else
    AStyleHandle := nil;

  AHidden := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrHidden);

  if AMax >= 0 then
  begin
    if AMax - AMin = dxSpreadSheetMaxColumnCount - 1 then
    begin
      View.Columns.DefaultSize := AWidth;
      if AStyleHandle <> nil then
        SpreadSheet.DefaultCellStyle.Handle := AStyleHandle;
    end
    else
      for I := AMin to AMax do
      begin
        AColumn := TdxSpreadSheetTableColumnAccess(View.Columns.CreateItem(I));
        if AStyleHandle <> nil then
          AColumn.Style.Handle := AStyleHandle;
        if AWidth >= 0 then
        begin
          AColumn.Size := AWidth;
          AColumn.IsCustomSize := AIsCustomWidth;
        end;
        AColumn.Visible := not AHidden;
      end;
  end;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessFixedPain(ANode: TdxXMLNode);
var
  AColumnIndex: Integer;
  ARowIndex: Integer;
begin
  View.FrozenColumn := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrSplitX) - 1;
  View.FrozenRow := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrSplitY) - 1;
  TdxSpreadSheetXLSXCellRef.Decode(ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrTopLeftCell), AColumnIndex, ARowIndex);
  if (AColumnIndex >= 0) and (ARowIndex >= 0) then
  begin
    TdxSpreadSheetTableViewAccess(View).ViewInfo.FirstScrollableColumn := AColumnIndex;
    TdxSpreadSheetTableViewAccess(View).ViewInfo.FirstScrollableRow := ARowIndex;
  end;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessHeaderFooter(ANode: TdxXMLNode);
var
  AHeaderFooter: TdxSpreadSheetTableViewOptionsPrintHeaderFooter;
begin
  AHeaderFooter := View.OptionsPrint.HeaderFooter;
  AHeaderFooter.AlignWithMargins := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrHeaderFooterAlignWithMargins);
  AHeaderFooter.ScaleWithDocument := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrHeaderFooterScaleWithDocument);

  ExecuteSubTask(TdxSpreadSheetXLSXReaderHeaderFooterParser.Create(ANode.FindChild(sdxXLSXNodeOddFooter), Owner, AHeaderFooter.CommonFooter));
  ExecuteSubTask(TdxSpreadSheetXLSXReaderHeaderFooterParser.Create(ANode.FindChild(sdxXLSXNodeOddHeader), Owner, AHeaderFooter.CommonHeader));

  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrHeaderFooterDifferentFirst) then
  begin
    ExecuteSubTask(TdxSpreadSheetXLSXReaderHeaderFooterParser.Create(ANode.FindChild(sdxXLSXNodeFirstFooter), Owner, AHeaderFooter.FirstPageFooter));
    ExecuteSubTask(TdxSpreadSheetXLSXReaderHeaderFooterParser.Create(ANode.FindChild(sdxXLSXNodeFirstHeader), Owner, AHeaderFooter.FirstPageHeader));
  end;

  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrHeaderFooterDifferentOddEven) then
  begin
    ExecuteSubTask(TdxSpreadSheetXLSXReaderHeaderFooterParser.Create(ANode.FindChild(sdxXLSXNodeEvenFooter), Owner, AHeaderFooter.EvenPagesFooter));
    ExecuteSubTask(TdxSpreadSheetXLSXReaderHeaderFooterParser.Create(ANode.FindChild(sdxXLSXNodeEvenHeader), Owner, AHeaderFooter.EvenPagesHeader));
  end;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessMergedCells(ANode: TdxXMLNode; AUserData: Pointer);
var
  ARect: TRect;
begin
  ARect := TdxSpreadSheetXLSXCellRef.DecodeRange(ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrRef));
  if (ARect.Left >= 0) and (ARect.Top >= 0) and (ARect.Right >= 0) and (ARect.Bottom >= 0) then
    View.MergedCells.Add(ARect);
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessPageMargins(ANode: TdxXMLNode);
var
  AMargins: TdxSpreadSheetTableViewOptionsPrintPageMargins;
begin
  AMargins := View.OptionsPrint.Page.Margins;
  AMargins.Left := ANode.Attributes.GetValueAsFloat(sdxXLSXAttrPageMarginsLeft);
  AMargins.Top := ANode.Attributes.GetValueAsFloat(sdxXLSXAttrPageMarginsTop);
  AMargins.Right := ANode.Attributes.GetValueAsFloat(sdxXLSXAttrPageMarginsRight);
  AMargins.Bottom := ANode.Attributes.GetValueAsFloat(sdxXLSXAttrPageMarginsBottom);
  AMargins.Header := ANode.Attributes.GetValueAsFloat(sdxXLSXAttrPageMarginsHeader);
  AMargins.Footer := ANode.Attributes.GetValueAsFloat(sdxXLSXAttrPageMarginsFooter);
  AMargins.Assigned := True;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessPageSetup(ANode: TdxXMLNode);
var
  APage: TdxSpreadSheetTableViewOptionsPrintPage;
  APaper: TdxSpreadSheetTableViewOptionsPrintPagePaper;
  APrinting: TdxSpreadSheetTableViewOptionsPrintPrinting;
begin
  APage := View.OptionsPrint.Page;
  APage.FitToHeight := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrPageSetupFitToHeight);
  APage.FitToWidth := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrPageSetupFitToWidth);
  APage.Orientation := TdxSpreadSheetXLSXHelper.StringToPrintPageOrientation(ANode.Attributes.GetValue(sdxXLSXAttrPageSetupOrientation));
  APage.Scale := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrPageSetupScale, 100);
  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrPageSetupUseFirstPageNumber) then
    APage.FirstPageNumber := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrPageSetupFirstPageNumber);

  APaper := View.OptionsPrint.Page.Paper;
  APaper.CustomSize.X := TdxValueUnitsHelper.ValueToInchesF(ANode.Attributes.GetValueAsString(sdxXLSXAttrPageSetupPaperWidth));
  APaper.CustomSize.Y := TdxValueUnitsHelper.ValueToInchesF(ANode.Attributes.GetValueAsString(sdxXLSXAttrPageSetupPaperHeight));
  APaper.SizeID := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrPageSetupPaperSize);
  APaper.Assigned := (APaper.SizeID <> 0) or (APaper.CustomSize.X > 0) and (APaper.CustomSize.Y > 0);

  APrinting := View.OptionsPrint.Printing;
  APrinting.Draft := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrPageSetupDraft);
  APrinting.BlackAndWhite := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrPageSetupBlackAndWhite);
  APrinting.Copies := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrPageSetupCopies);
  APrinting.PageOrder := TdxSpreadSheetXLSXHelper.StringToPrintPageOrder(ANode.Attributes.GetValue(sdxXLSXAttrPageSetupPageOrder));

  View.OptionsPrint.Source.ErrorIndication := TdxSpreadSheetXLSXHelper.StringToPrintErrorIndication(ANode.Attributes.GetValue(sdxXLSXAttrPageSetupErrors));
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessPageSetupExProperties(ANode: TdxXMLNode);
begin
  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrFitToPage) then
    View.OptionsPrint.Page.ScaleMode := oppsmFitToPage
  else
    View.OptionsPrint.Page.ScaleMode := oppsmAdjustToScale;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessPrintOptions(ANode: TdxXMLNode);
var
  ASource: TdxSpreadSheetTableViewOptionsPrintSource;
begin
  View.OptionsPrint.Printing.HorizontalCentered := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrPrintOptionsHorzCenter);
  View.OptionsPrint.Printing.VerticalCentered := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrPrintOptionsVertCenter);

  ASource := View.OptionsPrint.Source;
  ASource.Headers := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrPrintOptionsHeadings);
  ASource.GridLines := ANode.Attributes.GetValueAsDefaultBoolean(sdxXLSXAttrPrintOptionsGridLines);
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessProperties(ANode: TdxXMLNode);
var
  AAttr: TdxXMLNodeAttribute;
begin
  if ANode.Attributes.Find(sdxXLSXAttrDefaultColumnWidth, AAttr) then
    View.Options.DefaultColumnWidth := Owner.ColumnWidthHelper.WidthToPixels(AAttr.ValueAsFloat)
  else
    View.Options.DefaultColumnWidth := 3 + Owner.ColumnWidthHelper.CharsNumberToPixels(
      ANode.Attributes.GetValueAsInteger(sdxXLSXAttrBaseColumnWidth, 8));

  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrCustomHeight, True) then
    View.Options.DefaultRowHeight := ConvertRowHeight(ANode.Attributes.GetValueAsFloat(sdxXLSXAttrDefaultRowHeight));
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessRow(ANode: TdxXMLNode; AUserData: Pointer);
var
  AAttr: TdxXMLNodeAttribute;
  AIndex: Integer;
  ARow: TdxSpreadSheetTableRowAccess;
begin
  ARow := TdxSpreadSheetTableRowAccess(View.Rows.CreateItem(ANode.Attributes.GetValueAsInteger(sdxXLSXAttrRowIndex) - 1));

  if ANode.Attributes.Find(sdxXLSXAttrRowHeight, AAttr) then
  begin
    ARow.Size := ConvertRowHeight(AAttr.ValueAsFloat);
    ARow.IsCustomSize := ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrCustomHeight);
  end;

  ARow.Visible := not ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrHidden);

  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrCustomFormat) then
  begin
    AIndex := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrStyleIndex, 0);
    if CheckListIndex(AIndex, CellStyles, sdxErrorInvalidStyleIndex, ssmtError) then
      ARow.Style.Handle := TdxSpreadSheetCellStyleHandle(CellStyles[AIndex]);
  end;

  ANode.ForEach(ProcessRowCell, ARow);
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessRowCell(ANode: TdxXMLNode; AUserData: Pointer);
var
  AAttr: TdxXMLNodeAttribute;
  ACell: TdxSpreadSheetCell;
  ACellValue: TdxXMLNode;
  AChildNode: TdxXMLNode;
  AIndex: Integer;
  ATempFloat: Double;
begin
  ACell := TdxSpreadSheetTableRow(AUserData).CreateCell(ExtractColumnIndex(ANode.Attributes.GetValueAsUnicodeString(sdxXLSXAttrCellColumn)));

  if ANode.Attributes.Find(sdxXLSXAttrStyleIndex, AAttr) then
  begin
    AIndex := AAttr.ValueAsInteger;
    if CheckListIndex(AIndex, CellStyles, sdxErrorInvalidStyleIndex, ssmtError) then
      ACell.Style.Handle := TdxSpreadSheetCellStyleHandle(CellStyles[AIndex]);
  end;

  if ANode.FindChild(sdxXLSXNodeCellFunction, AChildNode) then
    Owner.FormulasRefs.Add(ACell, AChildNode)
  else
    if ANode.FindChild(sdxXLSXNodeCellValue, ACellValue) then
    begin
      case TdxSpreadSheetXLSXHelper.StringToCellType(ANode.Attributes.GetValue(sdxXLSXAttrCellType)) of
        sxctBoolean:
          ACell.AsBoolean := TdxXMLHelper.DecodeBoolean(ACellValue.TextAsUnicodeString);
        sxctError:
          ACell.AsString := ACellValue.TextAsUnicodeString;
        sxctFloat:
          ACell.AsFloat := StrToFloat(dxXMLStringToString(ACellValue.Text), Owner.InvariantFormatSettings);

        sxctRichText:
          if ACellValue.FindChild(sdxXLSXNodeCellRichText, AChildNode) then
            ACell.AsSharedString := TdxSpreadSheetXLSXReaderSharedStringParserHelper.Parse(Owner, AChildNode);

        sxctString:
          if TryStrToFloat(dxXMLStringToString(ACellValue.Text), ATempFloat, Owner.InvariantFormatSettings) then
            ACell.AsFloat := ATempFloat
          else
            ACell.AsString := ACellValue.TextAsUnicodeString;

        sxctSharedString:
          begin
            AIndex := StrToIntDef(dxXMLStringToString(ACellValue.Text), -1);
            if CheckListIndex(AIndex, SharedStrings, sdxErrorInvalidSharedStringIndex, ssmtError) then
              ACell.AsSharedString := TdxSpreadSheetSharedString(SharedStrings[AIndex]);
          end;
      end;
    end;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessSelection(
  ANode: TdxXMLNode; ASelection: TdxSpreadSheetTableViewSelection);
var
  AAttr: TdxXMLNodeAttribute;
  AParts: TStringList;
  ARect: TRect;
  I: Integer;
begin
  ASelection.Clear;

  if ANode.Attributes.Find(sdxXLSXAttrActiveCell, AAttr) then
  begin
    ARect := TdxSpreadSheetXLSXCellRef.DecodeRange(AAttr.ValueAsString);
    ASelection.Add(ARect, [ssCtrl], ARect.Left, ARect.Top);
  end;

  AParts := TStringList.Create;
  try
    AParts.Text := StringReplace(ANode.Attributes.GetValueAsString(sdxXLSXAttrSqRef), ' ', #13#10, [rfReplaceAll]);
    for I := 0 to AParts.Count - 1 do
    begin
      ARect := TdxSpreadSheetXLSXCellRef.DecodeRange(AParts[I]);
      if (ARect.Left = 0) and (ARect.Right = dxXLSXMaxColumnIndex) then
        ARect.Right := MaxInt;
      if (ARect.Top = 0) and (ARect.Bottom = dxXLSXMaxRowIndex) then
        ARect.Bottom := MaxInt;
      if not ASelection.HasArea(ARect) then
        ASelection.Add(ARect, [ssCtrl]);
    end;
  finally
    AParts.Free;
  end;
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ProcessViewProperties(ANode: TdxXMLNode);
var
  AChildNode: TdxXMLNode;
  AAttr: TdxXMLNodeAttribute;
begin
  View.Options.ZoomFactor := ANode.Attributes.GetValueAsInteger(sdxXLSXAttrZoomScaleNormal, 100);
  if ANode.Attributes.GetValueAsBoolean(sdxXLSXAttrTabSelected) then
    View.Active := True;
  if ANode.Attributes.Find(sdxXLSXAttrZeroValues, AAttr) then
    View.Options.ZeroValues := TdxDefaultBoolean(AAttr.ValueAsBoolean);
  if ANode.Attributes.Find(sdxXLSXAttrShowFormulas, AAttr) then
    View.Options.ShowFormulas := TdxDefaultBoolean(AAttr.ValueAsBoolean);
  if ANode.Attributes.Find(sdxXLSXAttrGridLines, AAttr) then
    View.Options.GridLines := TdxDefaultBoolean(AAttr.ValueAsBoolean);
  if ANode.FindChild(sdxXLSXNodePane, AChildNode) then
    ProcessFixedPain(AChildNode);
  if ANode.FindChild(sdxXLSXNodeSelection, AChildNode) then
    ProcessSelection(AChildNode, View.Selection);
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ReadDrawing(const AFileName: AnsiString);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXReaderDrawingParser.Create(AFileName, Owner, View));
end;

procedure TdxSpreadSheetXLSXReaderWorksheetParser.ReadDrawings;
var
  I: Integer;
begin
  for I := 0 to Rels.Count - 1 do
  begin
    if Rels.ItemsByIndex[I].FileType = sdxXLSXDrawingRelationship then
      ReadDrawing(Rels.ItemsByIndex[I].FileName);
  end;
end;

function TdxSpreadSheetXLSXReaderWorksheetParser.GetCellStyles: TdxSpreadSheetXLSXHashTableItemList;
begin
  Result := Owner.Styles;
end;

function TdxSpreadSheetXLSXReaderWorksheetParser.GetChild(const AName: TdxXMLString; out ANode: TdxXMLNode): Boolean;
begin
  Result := (Document.Root.First <> nil) and Document.Root.First.FindChild(AName, ANode);
end;

function TdxSpreadSheetXLSXReaderWorksheetParser.GetSharedStrings: TdxSpreadSheetXLSXHashTableItemList;
begin
  Result := Owner.SharedStrings;
end;

end.
