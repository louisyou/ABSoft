{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatUtils;

{$I cxVer.inc}

interface

uses
  Types, Windows, Graphics, dxGDIPlusClasses, dxCore, dxCoreGraphics, dxSpreadSheetCore, dxSpreadSheetPrinting;

type

  { TdxValueUnitsHelper }

  TdxValueUnitsHelper = class
  public
    class function InchesToPixels(const AValue: Integer): Integer;
    class function InchesToPixelsF(const AValue: Double): Integer;
    class function PixelsToInches(const AValue: Integer): Integer;
    class function PixelsToInchesF(const AValue: Double): Double;
    class function PixelsToPoints(const AValue: Integer): Double;
    class function PointsToPixels(const AValue: Double): Integer;
    class function ValueToInches(const S: string): Integer;
    class function ValueToInchesF(const S: string): Double;
    class function ValueToPixels(const S: string): Integer;
    class function ValueToPixelsF(const S: string): Double;
  end;

  { TdxSpreadSheetHeaderFooterHelper }

  TdxSpreadSheetHeaderFooterHelper = class
  public
    class function Build(AText: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText): TdxUnicodeString;
    class procedure Parse(ATarget: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText; const ASource: TdxUnicodeString);
  end;

  { TdxSpreadSheetPrintAreaHelper }

  TdxSpreadSheetPrintAreaHelper = class
  public
    class procedure AssignFromDefinedName(ATargetView: TdxSpreadSheetCustomView; ADefinedName: TdxSpreadSheetDefinedName);
  end;

procedure dxSpreadSheetDecodeCellRef(const ARef: TdxUnicodeString; out AColumnIndex, ARowIndex: Integer);
function dxSpreadSheetGetGradientMode(const AAngle: Double; out AInverseOrder: Boolean): TdxGPBrushGradientMode;
procedure dxSpreadSheetLoadBrushPattern(const ABrush: TdxGPBrush; const AInstance: THandle;
  const AResName: UnicodeString; const AForegroundColor, ABackgroundColor: TdxAlphaColor);
implementation

uses
  Math, SysUtils, dxSpreadSheetUtils, cxGraphics, cxGeometry;

const
  sdxHeaderFooterCenterSectionMacro = '&C';
  sdxHeaderFooterLeftSectionMacro = '&L';
  sdxHeaderFooterRightSectionMacro = '&R';

procedure dxSpreadSheetDecodeCellRef(const ARef: TdxUnicodeString; out AColumnIndex, ARowIndex: Integer);

  function FindFirstNumber(const S: TdxUnicodeString): Integer;
  var
    I: Integer;
  begin
    Result := 0;
    for I := 1 to Length(S) do
      if dxCharInSet(Char(S[I]), ['0'..'9']) then
      begin;
        Result := I;
        Break;
      end;
  end;

var
  APos: Integer;
begin
  APos := FindFirstNumber(ARef);
  if APos > 0 then
    AColumnIndex := TdxSpreadSheetColumnHelper.IndexByName(Copy(ARef, 1, APos - 1))
  else
    AColumnIndex := -1;

  ARowIndex := StrToIntDef(dxUnicodeStringToString(Copy(ARef, APos, MaxInt)), 0) - 1;
end;

function dxSpreadSheetGetGradientMode(const AAngle: Double; out AInverseOrder: Boolean): TdxGPBrushGradientMode;
var
  AIndex: Integer;
begin
  AIndex := Round(AAngle / 45);
  case AIndex of
    1, 5:
      Result := gpbgmForwardDiagonal;
    3, 7:
      Result := gpbgmBackwardDiagonal;
    2, 6:
      Result := gpbgmVertical;
  else
    Result := gpbgmHorizontal;
  end;
  AInverseOrder := InRange(AIndex, 4, 7);
end;

procedure dxSpreadSheetLoadBrushPattern(const ABrush: TdxGPBrush; const AInstance: THandle;
  const AResName: UnicodeString; const AForegroundColor, ABackgroundColor: TdxAlphaColor);
var
  ABackgroundColorQ: TRGBQuad;
  ABitmap: TBitmap;
  AColors: TRGBColors;
  AForegroundColorQ: TRGBQuad;
  I: Integer;
begin
  if FindResource(AInstance, PChar(AResName), RT_BITMAP) = 0 then
  begin
    ABrush.Style := gpbsClear;
    Exit;
  end;

  ABackgroundColorQ := dxAlphaColorToRGBQuad(ABackgroundColor);
  AForegroundColorQ := dxAlphaColorToRGBQuad(AForegroundColor);

  ABitmap := TBitmap.Create;
  try
    ABitmap.LoadFromResourceName(HInstance, AResName);
    GetBitmapBits(ABitmap, AColors, True);

    for I := 0 to Length(AColors) - 1 do
    begin
      if AColors[I].rgbBlue > 0 then
        AColors[I] := ABackgroundColorQ
      else
        AColors[I] := AForegroundColorQ;
    end;

    ABrush.Style := gpbsTexture;
    ABrush.Texture.LoadFromBits(ABitmap.Width, ABitmap.Height, AColors, True);
  finally
    ABitmap.Free;
  end;
end;

{ TdxValueUnitsHelper }

class function TdxValueUnitsHelper.InchesToPixels(const AValue: Integer): Integer;
begin
  Result := InchesToPixelsF(AValue);
end;

class function TdxValueUnitsHelper.InchesToPixelsF(const AValue: Double): Integer;
begin
  Result := Round(AValue * cxGetCurrentDPI);
end;

class function TdxValueUnitsHelper.PixelsToInches(const AValue: Integer): Integer;
begin
  Result := Round(PixelsToInchesF(AValue));
end;

class function TdxValueUnitsHelper.PixelsToInchesF(const AValue: Double): Double;
begin
  Result := AValue / cxGetCurrentDPI
end;

class function TdxValueUnitsHelper.PixelsToPoints(const AValue: Integer): Double;
begin
  Result := PixelsToInchesF(AValue) * 75;
end;

class function TdxValueUnitsHelper.PointsToPixels(const AValue: Double): Integer;
begin
  Result := InchesToPixelsF(AValue * 1 / 75);
end;

class function TdxValueUnitsHelper.ValueToInches(const S: string): Integer;
begin
  Result := PixelsToInches(ValueToPixels(S));
end;

class function TdxValueUnitsHelper.ValueToInchesF(const S: string): Double;
begin
  Result := PixelsToInchesF(ValueToPixelsF(S));
end;

class function TdxValueUnitsHelper.ValueToPixels(const S: string): Integer;
begin
  Result := Round(ValueToPixelsF(S));
end;

class function TdxValueUnitsHelper.ValueToPixelsF(const S: string): Double;
var
  ADelimPos: Integer;
  AValue: Double;
  AValueType: string;
begin
  Result := 0;
  ADelimPos := LastDelimiter('0123456789.', S);
  if ADelimPos > 0 then
  begin
    AValue := dxStrToFloat(Copy(S, 1, ADelimPos));
    AValueType := Copy(S, ADelimPos + 1, MaxInt);
    if AValueType = 'mm' then
      Result := AValue / 25.4 * cxGetCurrentDPI
    else

    if AValueType = 'cm' then
      Result := AValue / 2.54 * cxGetCurrentDPI
    else

    if AValueType = 'pt' then
      Result := AValue / 72 * cxGetCurrentDPI
    else

    if AValueType = 'in' then
      Result := AValue * cxGetCurrentDPI
  end;
end;

{ TdxSpreadSheetHeaderFooterHelper }

class function TdxSpreadSheetHeaderFooterHelper.Build(
  AText: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText): TdxUnicodeString;
begin
  Result := '';
  if AText.LeftSection <> '' then
    Result := Result + sdxHeaderFooterLeftSectionMacro + AText.LeftSection;
  if AText.CenterSection <> '' then
    Result := Result + sdxHeaderFooterCenterSectionMacro + AText.CenterSection;
  if AText.RightSection <> '' then
    Result := Result + sdxHeaderFooterRightSectionMacro + AText.RightSection;
end;

class procedure TdxSpreadSheetHeaderFooterHelper.Parse(
  ATarget: TdxSpreadSheetTableViewOptionsPrintHeaderFooterText; const ASource: TdxUnicodeString);
var
  APosL, APosR, APosC, APos: Integer;
begin
  APosL := Pos(sdxHeaderFooterLeftSectionMacro, ASource);
  APosC := Pos(sdxHeaderFooterCenterSectionMacro, ASource);
  APosR := Pos(sdxHeaderFooterRightSectionMacro, ASource);

  if APosL > 0 then
  begin
    if APosC > 0 then
      APos := APosC
    else
      if APosR > 0 then
        APos := APosR
      else
        APos := Length(ASource) + 1;

    ATarget.LeftSection := Copy(ASource,
      APosL + Length(sdxHeaderFooterLeftSectionMacro),
      APos - (APosL + Length(sdxHeaderFooterLeftSectionMacro)));
  end;

  if APosC > 0 then
  begin
    if APosR > 0 then
      APos := APosR
    else
      APos := Length(ASource) + 1;

    ATarget.CenterSection := Copy(ASource,
      APosC + Length(sdxHeaderFooterCenterSectionMacro),
      APos - (APosC + Length(sdxHeaderFooterCenterSectionMacro)));
  end;

  if APosR > 0 then
    ATarget.RightSection := Copy(ASource, APosR + Length(sdxHeaderFooterRightSectionMacro), MaxInt);
end;

{ TdxSpreadSheetPrintAreaHelper }

class procedure TdxSpreadSheetPrintAreaHelper.AssignFromDefinedName(
  ATargetView: TdxSpreadSheetCustomView; ADefinedName: TdxSpreadSheetDefinedName);
var
  ARange: TRect;
  ARangeIsValid: Boolean;
begin
  try
    ARange := cxNullRect;
    ARangeIsValid := False;

    ADefinedName.EnumReferences(
      procedure (const AArea: TRect; AView: TdxSpreadSheetTableView)
      begin
        if ATargetView = nil then
          ATargetView := AView;
        if AView = ATargetView then
        begin
          if ARangeIsValid then
            ARange := cxRectUnion(ARange, AArea)
          else
          begin
            ARange := AArea;
            ARangeIsValid := True;
          end;
        end;
      end);

    if ARangeIsValid then
    begin
      if ATargetView is TdxSpreadSheetTableView then
        TdxSpreadSheetTableView(ATargetView).OptionsPrint.Source.Area.Rect := ARange;
    end;
  finally
    ADefinedName.Owner.Remove(ADefinedName);
  end;
end;

end.
