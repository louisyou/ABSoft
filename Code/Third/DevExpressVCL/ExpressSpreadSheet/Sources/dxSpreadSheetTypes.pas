{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetTypes;

{$I cxVer.Inc}

interface

uses
  Windows, Types, SysUtils, Graphics, cxClasses, dxCore, Math;

var
  IsBreak: Boolean = False;

type
  PFloat = PDouble;
  PObject = ^TObject;

  TdxSpreadSheetMessageType = (ssmtError, ssmtWarning, ssmtHint);
  TdxSpreadSheetMessageTypes = set of TdxSpreadSheetMessageType;

  TdxSpreadSheetCellDataType = (cdtBlank, cdtBoolean, cdtError, cdtCurrency, cdtFloat, cdtDateTime, cdtInteger, cdtString, cdtFormula);

  TdxSpreadSheetDateTimeSystem = (dts1900, dts1904, dtsDefault);

  TdxSpreadSheetCellState = (csLocked, csHidden, csShrinkToFit, csWordWrap);
  TdxSpreadSheetCellStates = set of TdxSpreadSheetCellState;

  TdxSpreadSheetCellsModification = (cmShiftCellsHorizontally, cmShiftCellsVertically, cmShiftColumns, cmShiftRows);
  TdxSpreadSheetCellsModificationMode = (cmmClear, cmmDelete, cmmReplace);

  TdxSpreadSheetNumberFormatCategory = (nfcGeneral, nfcNumber, nfcCurrency, nfcAccounting,
    nfcDate, nfcTime, nfcPercentage, nfcFraction, nfcScientific, nfcText, nfcCustom);

  TdxSpreadSheetSizingMarker = (smLeft, smTopLeft, smTop, smTopRight, smRight, smBottomRight, smBottom, smBottomLeft);
  TdxSpreadSheetSizingMarkers = set of TdxSpreadSheetSizingMarker;

  // formulas
  TdxSpreadSheetFormulaOperation = (opAdd, opSub, opMul, opDiv, opPower, opConcat, opLT, opLE,
    opEQ, opGE, opGT, opNE, opIsect, opUnion, opRange, opUplus, opUminus, opPercent, opParen);
  TdxSpreadSheetOperationStrings = array[TdxSpreadSheetFormulaOperation] of TdxUnicodeString;

  TdxSpreadSheetParserResult = (prOk, prName, prRef, prError);

  TdxSpreadSheetFormulaErrorCode = (ecNone, ecNull, ecDivByZero, ecValue, ecRefErr, ecName, ecNUM, ecNA);

  TdxSpreadSheetChange = (sscData, sscLayout, sscZoom);
  TdxSpreadSheetChanges = set of TdxSpreadSheetChange;

  { TdxSpreadSheetCellData }

  TdxSpreadSheetCellData = class
  public
    Column: Integer;
    Row: Integer;
    Value: Variant;
    constructor Create(ARow, AColumn: Integer; const AValue: Variant);
  end;

  { TdxSpreadSheetVectorValue }

  TdxSpreadSheetVectorValue = class
  private
    FErrorCode: TdxSpreadSheetFormulaErrorCode;
    FValue: Variant;
  public
    constructor Create(const AValue: Variant; AErrorCode: TdxSpreadSheetFormulaErrorCode);
    function IsError: Boolean;

    property ErrorCode: TdxSpreadSheetFormulaErrorCode read FErrorCode;
    property Value: Variant read FValue;
  end;

  { TdxSpreadSheetVector }

  TdxSpreadSheetVector = class(TcxObjectList)
  strict private
    function GetItem(Index: Integer): TdxSpreadSheetVectorValue;
  public
    property Items[Index: Integer]: TdxSpreadSheetVectorValue read GetItem; default;
  end;

  { TdxSpreadSheetFormulaTokenDimension }

  TdxSpreadSheetFormulaTokenDimension = record
    ColumnCount: Integer;
    RowCount: Integer;
    class operator Equal(const ADimension1, ADimension2: TdxSpreadSheetFormulaTokenDimension): Boolean;
    class operator NotEqual(const ADimension1, ADimension2: TdxSpreadSheetFormulaTokenDimension): Boolean;
  end;

 { TdxSpreadSheetReference }

  TdxSpreadSheetReference = record
    Value: Int64;
    function GetFlags: Integer; inline;
    function GetIsAbsolute: Boolean; inline;
    function GetIsAllItems: Boolean; inline;
    function GetIsError: Boolean; inline;
    function GetOffset: Integer; inline;
    procedure SetFlags(AValue: Integer); inline;
    procedure SetIsAbsolute(AValue: Boolean); inline;
    procedure SetIsAllItems(AValue: Boolean); inline;
    procedure SetIsError(AValue: Boolean); inline;
    procedure SetOffset(AValue: Integer); inline;
  public
    function ActualValue(const AOrigin: Integer): Integer;
    procedure SetActualValue(const AOrigin, AValue: Integer);
    class operator Equal(const V1, V2: TdxSpreadSheetReference): Boolean;
    class operator NotEqual(const V1, V2: TdxSpreadSheetReference): Boolean;
    procedure Move(ADelta: Integer); inline;
    procedure Reset;
    function UpdateAreaReference(const AOrigin, AIndex1, AIndex2: Integer; var APoint2: TdxSpreadSheetReference; AIsDeletion: Boolean): Boolean; inline;
    function UpdateReference(const AOrigin, AIndex1, AIndex2: Integer; AIsDeletion: Boolean): Boolean; inline;

    property Flags: Integer read GetFlags write SetFlags;
    property IsAbsolute: Boolean read GetIsAbsolute write SetIsAbsolute;
    property IsAllItems: Boolean read GetIsAllItems write SetIsAllItems;
    property IsError: Boolean read GetIsError write SetIsError;
    property Offset: Integer read GetOffset write SetOffset;
  end;

  { TdxSpreadSheetReferencePath }

  TdxSpreadSheetReferencePath = class
  private
    FColumn: Integer;
    FNext: TdxSpreadSheetReferencePath;
    FRow: Integer;
    FSheet: TObject;
  public
    constructor Create(ARow, AColumn: Integer; ASheet: TObject);
    destructor Destroy; override;
    procedure Add(ARow, AColumn: Integer; ASheet: TObject);
    procedure Remove(ARow, AColumn: Integer; ASheet: TObject);

    property Column: Integer read FColumn;
    property Next: TdxSpreadSheetReferencePath read FNext;
    property Row: Integer read FRow;
    property Sheet: TObject read FSheet;
  end;


const
  dxSpreadSheetDefaultRowHeight = 20;
  dxSpreadSheetDefaultColumnWidth = 85;
  //
  dxSpreadSheetDefaultZoomFactor = 100;
  dxSpreadSheetMinimumZoomFactor = 45;
  dxSpreadSheetMaximumZoomFactor = 400;
  //
  dxSpreadSheetMaxColumnCount = 1 shl 14;
  dxSpreadSheetMaxRowCount = 1 shl 20;
  dxSpreadSheetMaxColumnIndex = dxSpreadSheetMaxColumnCount - 1;
  dxSpreadSheetMaxRowIndex = dxSpreadSheetMaxRowCount - 1;

  dxSpreadSheetMaxSeparatorWidth  = 10;

  dxSpreadSheetSelectionThickness = 3;

  dxSpreadSheetScrollAreaWidth = 15;

  dxExcelStandardColors: array[0..55] of TColor = (
    $000000, $FFFFFF, $0000FF, $00FF00, $FF0000, $00FFFF, $FF00FF, $FFFF00,
    $000080, $008000, $800000, $008080, $800080, $808000, $C0C0C0, $808080,
    $FF9999, $663399, $CCFFFF, $FFFFCC, $660066, $8080FF, $CC6600, $FFCCCC,
    $800000, $FF00FF, $00FFFF, $FFFF00, $800080, $000080, $808000, $FF0000,
    $FFCC00, $FFFFCC, $CCFFCC, $99FFFF, $FFCC99, $CC99FF, $FF99CC, $99CCFF,
    $FF6633, $CCCC33, $00CC99, $00CCFF, $0099FF, $0066FF, $996666, $969696,
    $663300, $669933, $003300, $003333, $003399, $663399, $993333, $333333
  );

  // table view hittests bits
  hcBackground   = 1;
  hcColumnHeader = 2;
  hcRowHeader    = 4;
  hcResizeArea   = 8;
  hcCell         = 16;
  hcCellHint     = 32;
  hcFrozenPaneSeparator = 64;
  hcContainer = 128;
  hcContainerSelection = 256;

  // pagecontrol hittest
  hcButton      = 2;
  hcSplitter    = 4;
  hcPageTab     = 8;

  // styles
  s_Background = 0;
  s_Content    = 1;
  s_Header     = 2;
  s_PageControl= 3;
  s_Selection  = 4;

  dxSpreadSheetResizeDelta = 5;

function dxSpreadSheetGetStandardColorIndex(const AColor: TColor): Integer;

implementation

uses dxSpreadSheetUtils;

type
  TInt64 = record
    Hi, Low: Integer;
  end;

const
  dxRefAbsolute = 1;
  dxRefAllItems = 2;
  dxRefError    = 4;

function dxSpreadSheetGetStandardColorIndex(const AColor: TColor): Integer;
var
  I: Integer;
begin
  for I := Low(dxExcelStandardColors) to High(dxExcelStandardColors) do
  begin
    if dxExcelStandardColors[I] = AColor then
      Exit(I);
  end;
  Result := -1;
end;

{  TdxSpreadSheetCellData }

constructor TdxSpreadSheetCellData.Create(ARow, AColumn: Integer; const AValue: Variant);
begin
  Column := AColumn;
  Row := ARow;
  Value := AValue;
end;

{ TdxSpreadSheetVectorValue }

constructor TdxSpreadSheetVectorValue.Create(const AValue: Variant; AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  FValue := AValue;
  FErrorCode := AErrorCode;
end;

function TdxSpreadSheetVectorValue.IsError: Boolean;
begin
  Result := ErrorCode <> ecNone;
end;

{ TdxSpreadSheetVector }

function TdxSpreadSheetVector.GetItem(Index: Integer): TdxSpreadSheetVectorValue;
begin
  Result := TdxSpreadSheetVectorValue(inherited Items[Index]);
end;

{ TdxSpreadSheetFormulaTokenDimension }

class operator TdxSpreadSheetFormulaTokenDimension.Equal(const ADimension1, ADimension2: TdxSpreadSheetFormulaTokenDimension): Boolean;
begin
  Result := (ADimension1.ColumnCount = ADimension2.ColumnCount) and (ADimension1.RowCount = ADimension2.RowCount);
end;

class operator TdxSpreadSheetFormulaTokenDimension.NotEqual(const ADimension1, ADimension2: TdxSpreadSheetFormulaTokenDimension): Boolean;
begin
  Result := not (ADimension1 = ADimension2);
end;

{ TdxSpreadSheetReference }

function TdxSpreadSheetReference.ActualValue(const AOrigin: Integer): Integer;
begin
  if IsError then
    Result := -1
  else
    if IsAllItems then
      Result := MaxInt
    else
      if IsAbsolute then
        Result := Offset
      else
        Result := Offset + AOrigin;
end;

procedure TdxSpreadSheetReference.SetActualValue(const AOrigin, AValue: Integer);
begin
  if IsAbsolute then
    Offset := AValue
  else
    Offset := AValue - AOrigin;
  IsError := IsError or (ActualValue(AOrigin) < 0);
end;

class operator TdxSpreadSheetReference.Equal(const V1, V2: TdxSpreadSheetReference): Boolean;
begin
  Result := V1.Value = V2.Value;
end;

class operator TdxSpreadSheetReference.NotEqual(const V1, V2: TdxSpreadSheetReference): Boolean;
begin
  Result := not (V1 = V2);
end;

procedure TdxSpreadSheetReference.Move(ADelta: Integer);
begin
  if IsAbsolute or IsError or IsAllItems then Exit;
  Offset := Offset + ADelta;
end;

procedure TdxSpreadSheetReference.Reset;
begin
  Value := 0;
end;

function TdxSpreadSheetReference.UpdateAreaReference(const AOrigin, AIndex1, AIndex2: Integer;
  var APoint2: TdxSpreadSheetReference; AIsDeletion: Boolean): Boolean;
var
  ADestOrigin, ARef1, ARef2, ADelta, AOffset1, AOffset2: Integer;
begin
  Result := False;
  if IsError or IsAllItems then Exit;
  ADelta := AIndex2 - AIndex1 + 1;
  AOffset1 := Offset;
  AOffset2 := APoint2.Offset;
  ADestOrigin := AOrigin;
  if AIndex1 <= AOrigin then
    ADestOrigin := ADestOrigin - ADelta * (Byte(AIsDeletion) * 2 - 1);
  ARef1 := ActualValue(AOrigin);
  ARef2 := APoint2.ActualValue(AOrigin);
  if AIsDeletion then
  begin
    IsError := (AIndex1 <= ARef1) and (AIndex2 >= ARef2);
    //
    if ARef1 > AIndex2 then
      Dec(ARef1, ADelta)
    else
      if ARef1 >= AIndex1 then
        ARef1 := AIndex1;
    //
    if ARef2 >= AIndex2 then
      Dec(ARef2, ADelta)
    else
      if AIndex1 < ARef2 then
        ARef2 := AIndex1;
    SetActualValue(ADestOrigin, ARef1);
    APoint2.SetActualValue(ADestOrigin, ARef2);
  end
  else
  begin
    UpdateReference(AOrigin, AIndex1, AIndex2, False);
    APoint2.UpdateReference(AOrigin, AIndex1, AIndex2, False);
  end;
  IsError := IsError or APoint2.IsError or (APoint2.ActualValue(AOrigin) < ActualValue(AOrigin));
  APoint2.IsError := IsError;
  Result := IsError or (AOffset1 <> Offset) or (AOffset2 <> APoint2.Offset);
end;

function TdxSpreadSheetReference.UpdateReference(const AOrigin, AIndex1, AIndex2: Integer; AIsDeletion: Boolean): Boolean;
var
  ADestOrigin, ARef, ADelta, AOffset: Integer;
begin
  Result := False;
  if IsError or IsAllItems then Exit;
  ADelta := AIndex2 - AIndex1 + 1;
  ADestOrigin := AOrigin;
  if AIndex1 <= AOrigin then
    ADestOrigin := ADestOrigin - ADelta * (Byte(AIsDeletion) * 2 - 1);
  ARef := ActualValue(AOrigin);
  AOffset := Offset;
  if AIsDeletion then
  begin
    IsError := InRange(ARef, AIndex1, AIndex2);
    if ARef >= AIndex2 then
      Dec(ARef, ADelta)
    else
      if ARef >= AIndex1 then
        ARef := AIndex1;
  end
  else
  begin
    if ARef >= AIndex1 then
      Inc(ARef, ADelta);
  end;
  SetActualValue(ADestOrigin, ARef);
  Result := IsError or (AOffset <> Offset);
end;

function TdxSpreadSheetReference.GetFlags: Integer;
begin
  Result := TInt64(Value).Hi;
end;

function TdxSpreadSheetReference.GetIsAbsolute: Boolean;
begin
  Result := Flags and dxRefAbsolute = dxRefAbsolute;
end;

function TdxSpreadSheetReference.GetIsAllItems: Boolean;
begin
  Result := Flags and dxRefAllItems = dxRefAllItems;
end;

function TdxSpreadSheetReference.GetIsError: Boolean;
begin
  Result := Flags and dxRefError = dxRefError;
end;

function TdxSpreadSheetReference.GetOffset: Integer;
begin
  Result := TInt64(Value).Low;
end;

procedure TdxSpreadSheetReference.SetFlags(AValue: Integer);
begin
  TInt64(Value).Hi := AValue;
end;

procedure TdxSpreadSheetReference.SetIsAbsolute(AValue: Boolean);
begin
  if AValue then
    Flags := Flags or dxRefAbsolute
  else
    Flags := Flags and not dxRefAbsolute;
end;

procedure TdxSpreadSheetReference.SetIsAllItems(AValue: Boolean);
begin
  if AValue then
    Flags := Flags or dxRefAllItems
  else
    Flags := Flags and not dxRefAllItems;
end;

procedure TdxSpreadSheetReference.SetIsError(AValue: Boolean);
begin
  if AValue then
    Flags := Flags or dxRefError
  else
    Flags := Flags and not dxRefError;
end;

procedure TdxSpreadSheetReference.SetOffset(AValue: Integer);
begin
  TInt64(Value).Low := AValue;
  IsAllItems := AValue = MaxInt;
end;

{ TdxSpreadSheetReferencePath }

constructor TdxSpreadSheetReferencePath.Create(ARow, AColumn: Integer; ASheet: TObject);
begin
  FColumn := AColumn;
  FRow := ARow;
  FSheet := ASheet;
end;

destructor TdxSpreadSheetReferencePath.Destroy;
begin
  FreeAndNil(FNext);
  inherited Destroy;
end;

procedure TdxSpreadSheetReferencePath.Add(ARow, AColumn: Integer; ASheet: TObject);
var
  ALast, AItem: TdxSpreadSheetReferencePath;
begin
  AItem := TdxSpreadSheetReferencePath.Create(ARow, AColumn, ASheet);
  ALast := Self;
  while ALast.Next <> nil do
    ALast := ALast.Next;
  ALast.FNext := AItem;
end;

procedure TdxSpreadSheetReferencePath.Remove(ARow, AColumn: Integer; ASheet: TObject);
var
  ABeforeLast, ALast: TdxSpreadSheetReferencePath;
begin
  ALast := Self;
  ABeforeLast := nil;
  while ALast.Next <> nil do
  begin
    ABeforeLast := ALast;
    ALast := ALast.Next;
  end;
  if (ALast.Row = ARow) and (ALast.Column = AColumn) and (ALast.Sheet = ASheet) then
  begin
    if ABeforeLast <> nil then
      ABeforeLast.FNext := nil;
    ALast.Free;
  end;
end;

end.
