﻿{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatCSV;

{$I cxVer.Inc}

interface

uses
  Variants, SysUtils, Classes, dxSpreadSheetCore, dxSpreadSheetClasses, dxCore;

type
  TdxSpreadSheetCSVFormatExportSettings = class;

  { TdxSpreadSheetCSVFormat }

  TdxSpreadSheetCSVFormat = class(TdxSpreadSheetCustomFormat)
  public
    class function CanCheckByContent: Boolean; override;
    class function CreateFormatSettings: TdxSpreadSheetFormatSettings; override;
    class function GetDescription: string; override;
    class function GetExt: string; override;
    class function GetReader: TdxSpreadSheetCustomReaderClass; override;
    class function GetWriter: TdxSpreadSheetCustomWriterClass; override;
  end;

  { TdxSpreadSheetCSVFormatSettings }

  TdxSpreadSheetCSVFormatSettings = class(TPersistent)
  strict private
    FEncoding: TEncoding;
    FExport: TdxSpreadSheetCSVFormatExportSettings;
    FQuote: WideChar;
    FValueSeparator: WideChar;

    procedure SetExport(AValue: TdxSpreadSheetCSVFormatExportSettings);
  public
    constructor Create; 
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset;
    //
    property Encoding: TEncoding read FEncoding write FEncoding;
    property Export: TdxSpreadSheetCSVFormatExportSettings read FExport write SetExport;
    property Quote: WideChar read FQuote write FQuote;
    property ValueSeparator: WideChar read FValueSeparator write FValueSeparator;
  end;

  { TdxSpreadSheetCSVFormatExportSettings }

  TdxSpreadSheetCSVFormatExportSettings = class(TPersistent)
  strict private
    FQuoteStringValues: Boolean;
    FUseDisplayValues: Boolean;
  public
    constructor Create; 
    procedure Assign(Source: TPersistent); override;
    procedure Reset;
    //
    property QuoteStringValues: Boolean read FQuoteStringValues write FQuoteStringValues;
    property UseDisplayValues: Boolean read FUseDisplayValues write FUseDisplayValues;
  end;

  { TdxSpreadSheetCSVCustomParser }

  TdxSpreadSheetCSVCustomParser = class(TObject)
  strict private
    FChars: PWideChar;
    FCount: Integer;
    FSettings: TdxSpreadSheetCSVFormatSettings;
    FValueContainsQuote: Boolean;
    FValueCursor: PWideChar;

    procedure GoToNext; inline;
    function LookInNext: WideChar; inline;
    procedure ProcessQuotedValue;
  protected
    procedure DoRowBegin; virtual;
    procedure DoRowEnd; virtual;
    procedure DoValue(const AValue: TdxUnicodeString; AIsQuotedValue: Boolean); virtual;
    procedure DoValueBegin; inline;
    procedure DoValueEnd; inline;
  public
    constructor Create(AChars: PWideChar; ACount: Integer);
    destructor Destroy; override;
    procedure Parse;
    //
    property Chars: PWideChar read FChars;
    property Count: Integer read FCount;
    property Settings: TdxSpreadSheetCSVFormatSettings read FSettings;
  end;

  { TdxSpreadSheetCSVReader }

  TdxSpreadSheetCSVReader = class(TdxSpreadSheetCustomReader)
  protected
    function CreateParser(const AData: WideString): TdxSpreadSheetCSVCustomParser; virtual;
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; override;
    procedure Parse(const AData: WideString);
  public
    procedure ReadData; override;
  end;

  { TdxSpreadSheetCSVReaderParser }

  TdxSpreadSheetCSVReaderParser = class(TdxSpreadSheetCSVCustomParser)
  strict private
    FCharsCount: Integer;
    FColumnIndex: Integer;
    FFormatSettings: TdxSpreadSheetFormatSettings;
    FProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
    FRow: TdxSpreadSheetTableRow;
    FRowIndex: Integer;
    FView: TdxSpreadSheetTableView;
  protected
    procedure DoRowBegin; override;
    procedure DoRowEnd; override;
    procedure DoValue(const AValue: TdxUnicodeString; AIsQuotedValue: Boolean); override;
  public
    constructor Create(AProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
      AView: TdxSpreadSheetTableView; AChars: PWideChar; ACount: Integer);
    destructor Destroy; override;
    //
    property View: TdxSpreadSheetTableView read FView;
  end;

  { TdxSpreadSheetCSVWriter }

  TdxSpreadSheetCSVWriter = class(TdxSpreadSheetCustomWriter)
  strict private
    FSettings: TdxSpreadSheetCSVFormatSettings;
  protected
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; override;
    function EncodeValue(ACell: TdxSpreadSheetCell): string; virtual;
    procedure Write(AWriter: TStreamWriter; AView: TdxSpreadSheetTableView); virtual;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    procedure WriteData; override;
    //
    property Settings: TdxSpreadSheetCSVFormatSettings read FSettings;
  end;

function dxSpreadSheetCSVFormatSettings: TdxSpreadSheetCSVFormatSettings;
implementation

uses
  dxHashUtils, Math, dxSpreadSheetTypes, dxSpreadSheetUtils, cxClasses, dxSpreadSheetStrs, cxDateUtils;

type
  TdxCustomSpreadSheetAccess = class(TdxCustomSpreadSheet);
  TdxSpreadSheetCellAccess = class(TdxSpreadSheetCell);
  TdxSpreadSheetTableRowAccess = class(TdxSpreadSheetTableRow);
  TdxSpreadSheetTableRowCellsAccess = class(TdxSpreadSheetTableRowCells);
  TdxSpreadSheetTableRowsAccess = class(TdxSpreadSheetTableRows);

var
  FFormatSettings: TdxSpreadSheetCSVFormatSettings;

function dxSpreadSheetCSVFormatSettings: TdxSpreadSheetCSVFormatSettings;
begin
  if FFormatSettings = nil then
    FFormatSettings := TdxSpreadSheetCSVFormatSettings.Create;
  Result := FFormatSettings;
end;

{ TdxSpreadSheetCSVFormat }

class function TdxSpreadSheetCSVFormat.CanCheckByContent: Boolean;
begin
  Result := False; 
end;

class function TdxSpreadSheetCSVFormat.CreateFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := TdxSpreadSheetFormatSettings.Create;
end;

class function TdxSpreadSheetCSVFormat.GetDescription: string;
begin
  Result := 'Comma delimited';
end;

class function TdxSpreadSheetCSVFormat.GetExt: string;
begin
  Result := '.csv';
end;

class function TdxSpreadSheetCSVFormat.GetReader: TdxSpreadSheetCustomReaderClass;
begin
  Result := TdxSpreadSheetCSVReader;
end;

class function TdxSpreadSheetCSVFormat.GetWriter: TdxSpreadSheetCustomWriterClass;
begin
  Result := TdxSpreadSheetCSVWriter;
end;

{ TdxSpreadSheetCSVFormatSettings }

constructor TdxSpreadSheetCSVFormatSettings.Create;
begin
  inherited Create;
  FExport := TdxSpreadSheetCSVFormatExportSettings.Create;
  Reset;
end;

destructor TdxSpreadSheetCSVFormatSettings.Destroy;
begin
  FreeAndNil(FExport);
  inherited Destroy;
end;

procedure TdxSpreadSheetCSVFormatSettings.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetCSVFormatSettings then
  begin
    Encoding := TdxSpreadSheetCSVFormatSettings(Source).Encoding;
    Export := TdxSpreadSheetCSVFormatSettings(Source).Export;
    Quote := TdxSpreadSheetCSVFormatSettings(Source).Quote;
    ValueSeparator := TdxSpreadSheetCSVFormatSettings(Source).ValueSeparator;
  end;
end;

procedure TdxSpreadSheetCSVFormatSettings.Reset;
begin
  Export.Reset;
  FEncoding := TEncoding.Default;
  FValueSeparator := ',';
  FQuote := '"';
end;

procedure TdxSpreadSheetCSVFormatSettings.SetExport(AValue: TdxSpreadSheetCSVFormatExportSettings);
begin
  FExport.Assign(AValue);
end;

{ TdxSpreadSheetCSVFormatExportSettings }

constructor TdxSpreadSheetCSVFormatExportSettings.Create;
begin
  inherited Create;
  Reset;
end;

procedure TdxSpreadSheetCSVFormatExportSettings.Assign(Source: TPersistent);
begin
  if Source is TdxSpreadSheetCSVFormatExportSettings then
  begin
    QuoteStringValues := TdxSpreadSheetCSVFormatExportSettings(Source).QuoteStringValues;
    UseDisplayValues := TdxSpreadSheetCSVFormatExportSettings(Source).UseDisplayValues;
  end;
end;

procedure TdxSpreadSheetCSVFormatExportSettings.Reset;
begin
  QuoteStringValues := True;
  UseDisplayValues := True;
end;

{ TdxSpreadSheetCSVCustomParser }

constructor TdxSpreadSheetCSVCustomParser.Create(AChars: PWideChar; ACount: Integer);
begin
  inherited Create;
  FChars := AChars;
  FCount := ACount;
  FSettings := TdxSpreadSheetCSVFormatSettings.Create;
  FSettings.Assign(dxSpreadSheetCSVFormatSettings);
end;

destructor TdxSpreadSheetCSVCustomParser.Destroy;
begin
  FreeAndNil(FSettings); 
  inherited Destroy;
end;

procedure TdxSpreadSheetCSVCustomParser.Parse;
var
  AChar: WideChar;
begin
  if FCount = 0 then
    Exit;

  DoRowBegin;
  while FCount > 0 do
  begin
    AChar := FChars^;
    if AChar = Settings.Quote then
      ProcessQuotedValue
    else

    if AChar = Settings.ValueSeparator then
    begin
      DoValueEnd;
      GoToNext;
      while (FCount > 0) and CharInSet(FChars^, [#9, ' ']) do
        GoToNext;
      DoValueBegin;
    end
    else

    if AChar = #10 then
    begin
      DoRowEnd;
      GoToNext;
      DoRowBegin;
    end
    else

    if AChar = #13 then
    begin
      DoRowEnd;
      if LookInNext = #10 then
        GoToNext;
      GoToNext;
      DoRowBegin;
    end
    else
      GoToNext;
  end;
  DoRowEnd;
end;

procedure TdxSpreadSheetCSVCustomParser.DoRowBegin;
begin
  DoValueBegin;
end;

procedure TdxSpreadSheetCSVCustomParser.DoRowEnd;
begin
  DoValueEnd;
end;

procedure TdxSpreadSheetCSVCustomParser.DoValue(const AValue: TdxUnicodeString; AIsQuotedValue: Boolean);
begin
  // do nothing
end;

procedure TdxSpreadSheetCSVCustomParser.DoValueBegin;
begin
  FValueCursor := FChars;
  FValueContainsQuote := False;
end;

procedure TdxSpreadSheetCSVCustomParser.DoValueEnd;
var
  ACount: Integer;
  AIsQuotedValue: Boolean;
  AValue: TdxUnicodeString;
begin
  ACount := (TdxNativeUInt(FChars) - TdxNativeUInt(FValueCursor)) div SizeOf(WideChar);
  if ACount > 0 then
  begin
    AIsQuotedValue := (FValueCursor^ = Settings.Quote) and ((FValueCursor + ACount - 1)^ = Settings.Quote);
    if AIsQuotedValue then
    begin
      Inc(FValueCursor);
      Dec(ACount, 2);
    end
    else
      if (FCount > 0) and (FChars^ = Settings.ValueSeparator) then
      begin
        while (ACount > 0) and CharInSet((FValueCursor + ACount - 1)^, [' ', #9]) do
          Dec(ACount);
      end;

    SetString(AValue, FValueCursor, ACount);
    if FValueContainsQuote then
      AValue := StringReplace(AValue, Settings.Quote + Settings.Quote, Settings.Quote, [rfReplaceAll]);
    DoValue(AValue, AIsQuotedValue);
  end
  else
    DoValue('', False);
end;

procedure TdxSpreadSheetCSVCustomParser.GoToNext;
begin
  Inc(FChars);
  Dec(FCount);
end;

function TdxSpreadSheetCSVCustomParser.LookInNext: WideChar;
begin
  if FCount > 1 then
    Result := (FChars + 1)^
  else
    Result := #0;
end;

procedure TdxSpreadSheetCSVCustomParser.ProcessQuotedValue;
begin
  GoToNext;
  if (FCount > 0) and (FChars^ = Settings.Quote) then
  begin
    FValueContainsQuote := True;
    GoToNext;
    Exit;
  end;

  while FCount > 0 do
  begin
    if FChars^ = Settings.Quote then
    begin
      if LookInNext <> Settings.Quote then
      begin
        GoToNext;
        Break;
      end;
      FValueContainsQuote := True;
      GoToNext;
    end;
    GoToNext;
  end;
end;

{ TdxSpreadSheetCSVReader }

procedure TdxSpreadSheetCSVReader.ReadData;
var
  ABuffer: TBytes;
  AEncoding: TEncoding;
  ASize: Integer;
begin
  ASize := Stream.Size - Stream.Position;
  if ASize > 0 then
  begin
    SetLength(ABuffer, ASize);
    Stream.Read(ABuffer[0], ASize);

    AEncoding := nil;
    ASize := TEncoding.GetBufferEncoding(ABuffer, AEncoding);
    Parse(AEncoding.GetString(ABuffer, ASize, Length(ABuffer) - ASize));
  end;
end;

function TdxSpreadSheetCSVReader.CreateParser(const AData: WideString): TdxSpreadSheetCSVCustomParser;
begin
  Result := TdxSpreadSheetCSVReaderParser.Create(ProgressHelper, AddTableView(''), PWideChar(AData), Length(AData));
end;

function TdxSpreadSheetCSVReader.CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
begin
  Result := TdxSpreadSheetCustomFilerProgressHelper.Create(Self, 1);
end;

procedure TdxSpreadSheetCSVReader.Parse(const AData: WideString);
begin
  with CreateParser(AData) do
  try
    Parse;
  finally
    Free;
  end;
end;

{ TdxSpreadSheetCSVReaderParser }

constructor TdxSpreadSheetCSVReaderParser.Create(AProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
  AView: TdxSpreadSheetTableView; AChars: PWideChar; ACount: Integer);
begin
  inherited Create(AChars, ACount);
  FView := AView;
  FFormatSettings := TdxCustomSpreadSheetAccess(View.SpreadSheet).FormatSettings;
  FProgressHelper := AProgressHelper;
  FProgressHelper.BeginStage(ACount);
end;

destructor TdxSpreadSheetCSVReaderParser.Destroy;
begin
  FProgressHelper.EndStage;
  inherited Destroy;
end;

procedure TdxSpreadSheetCSVReaderParser.DoRowBegin;
begin
  inherited DoRowBegin;
  FCharsCount := Count;
end;

procedure TdxSpreadSheetCSVReaderParser.DoRowEnd;
begin
  inherited DoRowEnd;
  FProgressHelper.NextTask(FCharsCount - Count);
  Inc(FRowIndex);
  FColumnIndex := 0;
  FRow := nil;
end;

procedure TdxSpreadSheetCSVReaderParser.DoValue(const AValue: TdxUnicodeString; AIsQuotedValue: Boolean);
var
  ACell: TdxSpreadSheetCell;
  AOrdinalValue: Variant;
begin
  if AValue <> '' then
  begin
    if FRow = nil then
      FRow := View.Rows.CreateItem(FRowIndex);

    ACell := FRow.CreateCell(FColumnIndex);
    if AIsQuotedValue then
      ACell.AsString := AValue
    else
      if dxTryStrToOrdinal(AValue, AOrdinalValue, FFormatSettings) then
        ACell.AsVariant := AOrdinalValue
      else
        ACell.AsString := AValue;
  end;
  Inc(FColumnIndex);
end;

{ TdxSpreadSheetCSVWriter }

constructor TdxSpreadSheetCSVWriter.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); 
begin
  inherited Create(AOwner, AStream);
  FSettings := TdxSpreadSheetCSVFormatSettings.Create;
  FSettings.Assign(dxSpreadSheetCSVFormatSettings);
end;

destructor TdxSpreadSheetCSVWriter.Destroy;
begin
  FreeAndNil(FSettings); 
  inherited Destroy;
end;

procedure TdxSpreadSheetCSVWriter.WriteData;
var
  AStreamWriter: TStreamWriter;
begin
  AStreamWriter := TStreamWriter.Create(Stream, Settings.Encoding);
  try
    Write(AStreamWriter, SpreadSheet.ActiveSheetAsTable);
  finally
    AStreamWriter.Free;
  end;
end;

function TdxSpreadSheetCSVWriter.CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
begin
  Result := TdxSpreadSheetCustomFilerProgressHelper.Create(Self, 1);
end;

function TdxSpreadSheetCSVWriter.EncodeValue(ACell: TdxSpreadSheetCell): string;

  function GetCellValue(ACell: TdxSpreadSheetCell): string;
  begin
    if Settings.Export.UseDisplayValues then
      Result := ACell.DisplayText
    else
      Result := ACell.AsString;
  end;

  function HasProhibitedSymbols(const S: string): Boolean;
  begin
    Result := LastDelimiter(#13#10 + Settings.ValueSeparator, S) > 0;
  end;

  function IsStringValue(ACell: TdxSpreadSheetCell): Boolean;
  begin
    if Settings.Export.UseDisplayValues then
      Result := TdxSpreadSheetCellAccess(ACell).ActualDataType = cdtString
    else
      Result := ACell.DataType = cdtString;  
  end;

begin
  Result := StringReplace(GetCellValue(ACell), Settings.Quote, Settings.Quote + Settings.Quote, [rfReplaceAll]);
  if Settings.Export.QuoteStringValues and IsStringValue(ACell) or HasProhibitedSymbols(Result) then
    Result := Settings.Quote + Result + Settings.Quote;
end;

procedure TdxSpreadSheetCSVWriter.Write(AWriter: TStreamWriter; AView: TdxSpreadSheetTableView);
var
  AColumnIndex: Integer;
  ARowIndex: Integer;
begin
  ProgressHelper.BeginStage(AView.Rows.Count); 
  try
    ARowIndex := -1;
    TdxSpreadSheetTableRowsAccess(AView.Rows).ForEach(
      procedure (AItem: TdxDynamicListItem; AData: Pointer)
      var
        ARow: TdxSpreadSheetTableRowAccess;
        I: Integer; 
      begin
        ARow := TdxSpreadSheetTableRowAccess(AItem);
        for I := 1 to ARow.Index - Max(ARowIndex, 0) do 
          AWriter.WriteLine(Settings.ValueSeparator);

        ProgressHelper.NextTask(ARow.Index - ARowIndex);
        ARowIndex := ARow.Index;

        AColumnIndex := 0;
        TdxSpreadSheetTableRowCellsAccess(ARow.RowCells).ForEach(
          procedure (AItem: TdxDynamicListItem; AData: Pointer)
          var
            ACell: TdxSpreadSheetCell;
            I: Integer;
          begin
            ACell := TdxSpreadSheetCell(AItem);
            for I := 1 to ACell.ColumnIndex - AColumnIndex do
              AWriter.Write(Settings.ValueSeparator);
            AWriter.Write(EncodeValue(ACell));
            AColumnIndex := ACell.ColumnIndex;
          end);
      end);
  finally
    ProgressHelper.EndStage;
  end;
end;

initialization
  TdxSpreadSheetCSVFormat.Register;

finalization
  TdxSpreadSheetCSVFormat.Unregister;
  FreeAndNil(FFormatSettings); 
end.
