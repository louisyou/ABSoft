{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLSDrawing;

{$I cxVer.inc}

interface

uses
  Windows, Types, Classes, SysUtils, Dialogs, Math, dxCore, cxClasses, Generics.Collections, dxSpreadSheetFormatXLS, Graphics,
  dxSpreadSheetFormatXLSTypes, dxSpreadSheetCore, dxSpreadSheetFormulas, dxSpreadSheetTypes, dxSpreadSheetFunctions,
  cxVariants, dxSpreadSheetFormatXLSFormulas, dxGDIPlusClasses, dxSpreadSheetFormatUtils, dxSpreadSheetUtils, dxCoreGraphics,
  ZLib;



const
  { Office art record types }
   oaDrawingContainer        = $F000;
   oaBlipStoreContainer      = $F001;
   oaDrawingObjectsContainer = $F002;
   oaShapeGroupContainer     = $F003;
   oaShapeContainer          = $F004;
   oaFileDrawingGroupRecord  = $F006;
   oaFileBlipStoreEntry      = $F007;
   oaFileDrawingRecord       = $F008;
   oaShapeGroupCoordinates   = $F009;
   oaFileShape               = $F00A;
   oaPropertiesTable         = $F00B;
   oaClientTextbox           = $F00D;
   oaChildAnchor             = $F00F;
   oaClientAnchor            = $F010;
   oaClientData              = $F011;
   oaSplitMenuColorContainer = $F11E;
   oaTertiaryPropertiesTable = $F122;
   //
   oaBLIPFirst               = $F018;
   oaBLIPLast                = $F117;
   //
   oaBlipEMF      = $03D4; // 03D5
   oaBlipWMF      = $0216; // 0217
   oaBlipJPG_RGB  = $046A; // 046B
   oaBlipJPG_CMYK = $06E2; // 06E3
   oaBlipPNG      = $06E0; // 06E1
   oaBlipDIB      = $07A8; // 07A9
   oaBlipTIFF     = $06E4; // 06E5

   // shape flags

   fsGroup         = $001;
   fsChild         = $002;
   fsPatriarch     = $004;
   fsDeleted       = $008;
   fsOleShape      = $010;
   fsHaveMaster    = $020;
   fsFlipH         = $040;
   fsFlipV         = $080;
   fsConnector     = $100;
   fsHaveAnchor    = $200;
   fsBackground    = $400;
   fsHaveShapeType = $800;

type
  ToaBlipTypes = (
    btError = 0,
    btUnknown = 1,
    btEmf = 2,
    btWmf = 3,
    btMacPict = 4,
    btJpeg = 5,
    btPng = 6,
    btDib = 7,
    btTiff = $11,
    btCMYKJpeg = $12);

  TdxMSOHeader = record
    recVer: Byte;
    recInstance: Word;
    recType: Word;
    recSize: LongWord;
  end;

  PdxMSOAnchor = ^TdxMSOAnchor;
  TdxMSOAnchor = packed record
    Options,
    Col1, Col1Offset,
    Row1, Row1Offset,
    Col2, Col2Offset,
    Row2, Row2Offset: Word;
  end;

  TdxMSODrawingGroupRecord = packed record
    spidMax: Integer;
    sidcl: Integer;
    cspSaved: Integer;
    cdgSaved: Integer;
  end;

  TdxMSOBlipStoreEntry = packed record
    btWinType: Byte;
    btMacType: Byte;
    rgbUid: array[0..15] of Byte;
    tag: Word;
    size: LongWord;
    cRef: LongWord;
    foDelay: LongWord;
    unused1: Byte;
    cbName: Byte;
    unused2: Byte;
    unused3: Byte;
  end;


type
  TdxXLSFixedPoint = packed record
    Integral: Word;
    Fractional: SmallInt;
  end;

  TdxXLSFunction = function: Boolean of object;
  TdxXLSDrawingProperty = procedure(const AValue: Integer; const AComplexData: array of Byte) of object;

  { TdxSpreadSheetMSOCustomReader }

  TdxSpreadSheetMSOCustomReader = class
  private
    FAnchor:  TdxMSOAnchor;
    FAnchorAssigned: Boolean;
    FBlipCount: Integer;
    FBlipID: Integer;
    FBrushColor: TdxAlphaColor;
    FBrushGradientMode: TdxGPBrushGradientMode;
    FBrushGradientModeInverseOrder: Boolean;
    FBrushStyle: TdxGPBrushStyle;
    FFillBlipID: Integer;
    FFlipHorizontally: Boolean;
    FFlipVertically: Boolean;
    FKeepOnMove: Boolean;
    FKeepOnResize: Boolean;
    FOwner: TdxSpreadSheetXLSReader;
    FPenStyle: TdxGPPenStyle;
    FPenWidth: Single;
    FPenColor: TdxAlphaColor;
    FPropertyReaders: array[0..$FFFF] of TdxXLSDrawingProperty;
    FReader: TcxReader;
    FRecordHeader: TdxMSOHeader;
    FRecordReaders: array[$F000..$FFFF] of TdxXLSFunction;
    FRotationAngle: Double;
    FShapeID: Integer;
    FShapeFlags: Integer;
    FShapeType: Integer;
    function GetStream: TStream; inline;
    //
    procedure Property_BooleanProtection(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingCropFromTop(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingCropFromBottom(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingCropFromLeft(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingCropFromRight(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingBlipName(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingBlipFlags(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingBlipBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillStyleBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineWidth(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineMiterLimit(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineCompoundType(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineDashing(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineJoinStyle(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineCapStyle(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineStyleBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingBlackWhiteMode(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingShapeBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingBlipIdentifier(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingTextIdentifier(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingShapeName(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingShapeDescription(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingShapeHyperlink(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingShapeTooltip(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShapeBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DiagramBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShapePosH(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShapePosRelH(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShapePosV(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShapePosRelV(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShape2PctHoriz(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShape2PctVert(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShape2PctHorizPos(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShape2PctVertPos(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShape2SizeRelH(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingGroupShape2SizeRelV(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingRotation(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingLineColor(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingTextLeft(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingTextTop(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingTextRight(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingTextBottom(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingWrapLeftDistance(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingWrapTopDistance(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingWrapRightDistance(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingWrapBottomDistance(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBlipIdentifier(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingShadowColor(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingShadowStyleBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingConnectionPointsType(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingTextBooleanProperties(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingTextDirection(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_OfficeDrawingFillType(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillColor(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillOpacity(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBackColor(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBackOpacity(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBWColor(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBlip(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBlipName(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBlipFlags(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillWidth(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillHeight(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillAngle(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillFocus(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillToLeft(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillToTop(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillToRight(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillToBottom(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillRectLeft(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillRectTop(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillRectRight(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillRectBottom(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillDzType(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillShadePreset(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillShadeColors(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillOriginX(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillOriginY(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillShapeOriginX(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillShapeOriginY(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillShadeType(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillColorExt(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillReserved415(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillTintShade(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillReserved417(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBackColorExt(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillReserved419(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillBackTintShade(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillReserved421(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillReserved422(const AValue: Integer; const AComplexData: array of Byte);
    procedure Property_DrawingFillReserved423(const AValue: Integer; const AComplexData: array of Byte);
    //
    procedure ReadComplexData(ASize: Integer; var AData: array of Byte);
  protected
    procedure ApplyPropertyValues;
    procedure ClearPropertyValues;
    function GetGraphic(AIndex: Integer): TdxSmartImage;
    function HasBlip: Boolean;
    function HasShape: Boolean;
    procedure Initialize; virtual;
    function LoadMetaFile(AStream: TMemoryStream; AIsEMF: Boolean): TdxSmartImage;
    function ReadBlip: Boolean;
    function ReadBlipStoreContainer: Boolean;
    function ReadBlipStoryEntry: Boolean;
    function ReadClientAnchor: Boolean;
    function ReadDrawingContainer: Boolean;
    function ReadDrawingGroupRecord: Boolean;
    function ReadDrawingObjectsContainer: Boolean;
    function ReadDrawingRecord: Boolean;
    function ReadFileShape: Boolean;
    function ReadPropertiesTable: Boolean;
    function ReadShapeContainer: Boolean;
    function ReadShapeGroupCoordinates: Boolean;
    function ReadShapeGroupContainer: Boolean;
    function ReadSplitMenuColorContainer: Boolean;
    procedure RegisterPropertyReader(AKey: Word; AProc: TdxXLSDrawingProperty);
  public
    constructor Create(AOwner: TdxSpreadSheetXLSReader); virtual;
    destructor Destroy; override;
    procedure Read;
    //
    function HOffsetToInt(AColumnIndex, AOffset: Integer): Integer;
    function VOffsetToInt(ARowIndex, AOffset: Integer): Integer;

    property BlipCount: Integer read FBlipCount;
    property Owner: TdxSpreadSheetXLSReader read FOwner;
    property Reader: TcxReader read FReader;
    property RecordHeader: TdxMSOHeader read FRecordHeader;
    property Stream: TStream read GetStream;
  end;

  { TdxSpreadSheetMSODrawingReader }

  TdxSpreadSheetMSODrawingReader = class(TdxSpreadSheetMSOCustomReader)
  end;

  { TdxSpreadSheetMSODrawingGroupReader }

  TdxSpreadSheetMSODrawingGroupReader = class(TdxSpreadSheetMSOCustomReader)
  end;

implementation

{ TdxSpreadSheetMSOCustomReader }

constructor TdxSpreadSheetMSOCustomReader.Create(AOwner: TdxSpreadSheetXLSReader);
begin
  FOwner := AOwner;
  FReader  := TcxReader.Create(Stream);
  Initialize;
end;

destructor TdxSpreadSheetMSOCustomReader.Destroy;
begin
  FreeAndNil(FReader);
  inherited Destroy;
end;

procedure TdxSpreadSheetMSOCustomReader.Read;
var
  ASavePos: Integer;
  ARecordReader: TdxXLSFunction;
begin
  while Stream.Position < Stream.Size - 8 do
  begin
    FRecordHeader.recInstance :=  Reader.ReadWord;
    FRecordHeader.recVer := FRecordHeader.recInstance and $0F;
    FRecordHeader.recInstance := FRecordHeader.recInstance shr 4 and $0FFF;
    FRecordHeader.recType :=  Reader.ReadWord;
    FRecordHeader.recSize :=  LongWord(Reader.ReadInteger);
    if FRecordHeader.recType < $F000 then
    begin
      FRecordHeader.recType := $F000;
      Break;
    end;
    ARecordReader := FRecordReaders[FRecordHeader.recType];
    ASavePos := Stream.Position;
    if Assigned(ARecordReader) {or not ARecordReader} then
    begin
      if not ARecordReader then
        Stream.Position := ASavePos + Integer(FRecordHeader.recSize);
    end
    else
      Stream.Position := ASavePos + Integer(FRecordHeader.recSize);
  end;
end;

function TdxSpreadSheetMSOCustomReader.HOffsetToInt(AColumnIndex, AOffset: Integer): Integer;
begin
  if TdxSpreadSheetTableView(Owner.CurrentSheet).Columns[AColumnIndex] = nil then
    Result := TdxSpreadSheetTableView(Owner.CurrentSheet).Columns.DefaultSize
  else
    Result := TdxSpreadSheetTableView(Owner.CurrentSheet).Columns[AColumnIndex].Size;
  Result := Round(Result * AOffset / 1024);
end;

function TdxSpreadSheetMSOCustomReader.VOffsetToInt(ARowIndex, AOffset: Integer): Integer;
begin
  if TdxSpreadSheetTableView(Owner.CurrentSheet).Rows[ARowIndex] = nil then
    Result := TdxSpreadSheetTableView(Owner.CurrentSheet).Rows.DefaultSize
  else
    Result := TdxSpreadSheetTableView(Owner.CurrentSheet).Rows[ARowIndex].Size;
  Result := Round(Result * AOffset / 256);
end;



procedure TdxSpreadSheetMSOCustomReader.Initialize;
var
  I: Integer;
begin
  FRecordReaders[oaBlipStoreContainer] := ReadBlipStoreContainer;
  FRecordReaders[oaDrawingObjectsContainer] := ReadDrawingObjectsContainer;
  FRecordReaders[oaDrawingContainer] := ReadDrawingContainer;
  FRecordReaders[oaFileDrawingGroupRecord] := ReadDrawingGroupRecord;
  FRecordReaders[oaFileDrawingRecord] := ReadDrawingRecord;
  FRecordReaders[oaFileBlipStoreEntry] := ReadBlipStoryEntry;
  FRecordReaders[oaFileShape] := ReadFileShape;
  FRecordReaders[oaPropertiesTable] := ReadPropertiesTable;
  FRecordReaders[oaClientAnchor] := ReadClientAnchor;
  FRecordReaders[oaSplitMenuColorContainer] := ReadSplitMenuColorContainer;
  FRecordReaders[oaShapeContainer] := ReadShapeContainer;
  FRecordReaders[oaShapeGroupContainer] := ReadShapeGroupContainer;
  FRecordReaders[oaShapeGroupCoordinates] := ReadShapeGroupCoordinates;
  for I := oaBlipFirst to oaBlipLast do
    FRecordReaders[I] := ReadBlip;
  //
  RegisterPropertyReader($007f, Property_BooleanProtection);
  RegisterPropertyReader($0100, Property_DrawingCropFromTop);
  RegisterPropertyReader($0101, Property_DrawingCropFromBottom);
  RegisterPropertyReader($0102, Property_DrawingCropFromLeft);
  RegisterPropertyReader($0103, Property_DrawingCropFromRight);
  RegisterPropertyReader($c105, Property_DrawingBlipName);
  RegisterPropertyReader($0106, Property_DrawingBlipFlags);
  RegisterPropertyReader($013f, Property_DrawingBlipBooleanProperties);
  RegisterPropertyReader($01bf, Property_DrawingFillStyleBooleanProperties);
  RegisterPropertyReader($01cb, Property_DrawingLineWidth);
  RegisterPropertyReader($01cc, Property_DrawingLineMiterLimit);
  RegisterPropertyReader($01cd, Property_DrawingLineCompoundType);
  RegisterPropertyReader($01ce, Property_DrawingLineDashing);
  RegisterPropertyReader($01d6, Property_DrawingLineJoinStyle);
  RegisterPropertyReader($01d7, Property_DrawingLineCapStyle);
  RegisterPropertyReader($01ff, Property_DrawingLineStyleBooleanProperties);
  RegisterPropertyReader($0304, Property_DrawingBlackWhiteMode);
  RegisterPropertyReader($033f, Property_DrawingShapeBooleanProperties);
  RegisterPropertyReader($4104, Property_DrawingBlipIdentifier);
  RegisterPropertyReader($0080, Property_DrawingTextIdentifier);
  RegisterPropertyReader($c380, Property_DrawingShapeName);
  RegisterPropertyReader($c381, Property_DrawingShapeDescription);
  RegisterPropertyReader($c382, Property_DrawingShapeHyperlink);
  RegisterPropertyReader($c38d, Property_DrawingShapeTooltip);
  RegisterPropertyReader($03bf, Property_DrawingGroupShapeBooleanProperties);
  RegisterPropertyReader($053f, Property_DiagramBooleanProperties);
  RegisterPropertyReader($038F, Property_DrawingGroupShapePosH);
  RegisterPropertyReader($0390, Property_DrawingGroupShapePosRelH);
  RegisterPropertyReader($0391, Property_DrawingGroupShapePosV);
  RegisterPropertyReader($0392, Property_DrawingGroupShapePosRelV);
  RegisterPropertyReader($07c0, Property_DrawingGroupShape2PctHoriz);
  RegisterPropertyReader($07c1, Property_DrawingGroupShape2PctVert);
  RegisterPropertyReader($07c2, Property_DrawingGroupShape2PctHorizPos);
  RegisterPropertyReader($07c3, Property_DrawingGroupShape2PctVertPos);
  RegisterPropertyReader($07c4, Property_DrawingGroupShape2SizeRelH);
  RegisterPropertyReader($07c5, Property_DrawingGroupShape2SizeRelV);
  RegisterPropertyReader($0004, Property_DrawingRotation);
  RegisterPropertyReader($01c0, Property_DrawingLineColor);
  RegisterPropertyReader($0081, Property_DrawingTextLeft);
  RegisterPropertyReader($0082, Property_DrawingTextTop);
  RegisterPropertyReader($0083, Property_DrawingTextRight);
  RegisterPropertyReader($0084, Property_DrawingTextBottom);
  RegisterPropertyReader($0384, Property_DrawingWrapLeftDistance);
  RegisterPropertyReader($0385, Property_DrawingWrapTopDistance);
  RegisterPropertyReader($0386, Property_DrawingWrapRightDistance);
  RegisterPropertyReader($0387, Property_DrawingWrapBottomDistance);
  RegisterPropertyReader($4186, Property_DrawingFillBlipIdentifier);
  RegisterPropertyReader($0201, Property_DrawingShadowColor);
  RegisterPropertyReader($023f, Property_DrawingShadowStyleBooleanProperties);
  RegisterPropertyReader($0158, Property_DrawingConnectionPointsType);
  RegisterPropertyReader($00bf, Property_DrawingTextBooleanProperties);
  RegisterPropertyReader($008b, Property_DrawingTextDirection);
  RegisterPropertyReader($0180, Property_OfficeDrawingFillType);
  RegisterPropertyReader($0181, Property_DrawingFillColor);
  RegisterPropertyReader($0182, Property_DrawingFillOpacity);
  RegisterPropertyReader($0183, Property_DrawingFillBackColor);
  RegisterPropertyReader($0184, Property_DrawingFillBackOpacity);
  RegisterPropertyReader($0185, Property_DrawingFillBWColor);
  RegisterPropertyReader($0186, Property_DrawingFillBlip);
  RegisterPropertyReader($0187, Property_DrawingFillBlipName);
  RegisterPropertyReader($0188, Property_DrawingFillBlipFlags);
  RegisterPropertyReader($0189, Property_DrawingFillWidth);
  RegisterPropertyReader($018a, Property_DrawingFillHeight);
  RegisterPropertyReader($018b, Property_DrawingFillAngle);
  RegisterPropertyReader($018c, Property_DrawingFillFocus);
  RegisterPropertyReader($018d, Property_DrawingFillToLeft);
  RegisterPropertyReader($018e, Property_DrawingFillToTop);
  RegisterPropertyReader($018f, Property_DrawingFillToRight);
  RegisterPropertyReader($0190, Property_DrawingFillToBottom);
  RegisterPropertyReader($0191, Property_DrawingFillRectLeft);
  RegisterPropertyReader($0192, Property_DrawingFillRectTop);
  RegisterPropertyReader($0193, Property_DrawingFillRectRight);
  RegisterPropertyReader($0194, Property_DrawingFillRectBottom);
  RegisterPropertyReader($0195, Property_DrawingFillDzType);
  RegisterPropertyReader($0196, Property_DrawingFillShadePreset);
  RegisterPropertyReader($0197, Property_DrawingFillShadeColors);
  RegisterPropertyReader($0198, Property_DrawingFillOriginX);
  RegisterPropertyReader($0199, Property_DrawingFillOriginY);
  RegisterPropertyReader($019a, Property_DrawingFillShapeOriginX);
  RegisterPropertyReader($019b, Property_DrawingFillShapeOriginY);
  RegisterPropertyReader($019c, Property_DrawingFillShadeType);
  RegisterPropertyReader($019e, Property_DrawingFillColorExt);
  RegisterPropertyReader($019f, Property_DrawingFillReserved415);
  RegisterPropertyReader($01a0, Property_DrawingFillTintShade);
  RegisterPropertyReader($01a1, Property_DrawingFillReserved417);
  RegisterPropertyReader($01a2, Property_DrawingFillBackColorExt);
  RegisterPropertyReader($01a3, Property_DrawingFillReserved419);
  RegisterPropertyReader($01a4, Property_DrawingFillBackTintShade);
  RegisterPropertyReader($01a5, Property_DrawingFillReserved421);
  RegisterPropertyReader($01a6, Property_DrawingFillReserved422);
  RegisterPropertyReader($01a7, Property_DrawingFillReserved423);
end;

function TdxSpreadSheetMSOCustomReader.LoadMetaFile(AStream: TMemoryStream; AIsEMF: Boolean): TdxSmartImage;
var
  ASize, ASave: Integer;
  ACanvasSize: TSize;
  ABounds: TRect;
  AFilter: Byte;
  ACompression: Byte;
  AMetaFile: TMetaFile;
  ABitmap: TBitmap;
  ASourceBits, ADestBits: Pointer;
begin
  Result := TdxSmartImage.Create;
  Result.ImageDataFormat := dxImageBitmap;
  AStream.ReadBuffer(ASize, SizeOf(ASize));
  AStream.ReadBuffer(ABounds, SizeOf(ABounds));
  AStream.ReadBuffer(ACanvasSize, SizeOf(ACanvasSize));
  ACanvasSize.cx := dxEMUToPixels(ACanvasSize.cx);
  ACanvasSize.cy := dxEMUToPixels(ACanvasSize.cy);
  AStream.ReadBuffer(ASave, SizeOf(ASave));
  AStream.ReadBuffer(ACompression, SizeOf(ACompression));
  AStream.ReadBuffer(AFilter, SizeOf(AFilter));
  ASourceBits := @PByteArray(AStream.Memory)^[AStream.Position];
  AMetaFile := TMetafile.Create;
  try
    if ACompression = 0 then
    begin
      GetMem(ADestBits, ASize);
      ZDecompress(ASourceBits, ASave, ADestBits, ASize, ASize);
    end
    else
      ADestBits := ASourceBits;
    AMetaFile.SetSize(ACanvasSize.cx, ACanvasSize.cy);
    AMetaFile.Handle := SetEnhMetafileBits(ASize, ADestBits);
    ABitmap := TBitmap.Create;
    try
      ABitmap.SetSize(ACanvasSize.cx, ACanvasSize.cy);
      ABitmap.Canvas.Draw(0, 0, AMetaFile);
      Result.SetBitmap(ABitmap);
    finally
      ABitmap.Free;
    end;
  finally
    if ACompression = 0 then
      FreeMem(ADestBits);
    AMetaFile.Free;
  end;
end;

procedure TdxSpreadSheetMSOCustomReader.ApplyPropertyValues;
var
  AContaner: TdxSpreadSheetPictureContainer;
begin
  if (Owner.CurrentSheet = nil) or not FAnchorAssigned then Exit;
  if not (HasShape or HasBlip) then Exit;
//  if ((FBlipID < 0) and (AShapeID < 0)) or (FBlipID >= Owner.Images.Count) or (Owner.Images[FBlipID] = nil) or (Owner.CurrentSheet = nil) or not FAnchorAssigned then Exit;
  AContaner := Owner.CurrentSheet.Containers.Add(TdxSpreadSheetPictureContainer) as TdxSpreadSheetPictureContainer;
// AContaner.Transform.FlipHorizontally := FFlipHorizontally;
// AContaner.Transform.FlipVertically := FFlipVertically;
  AContaner.Picture.Image := GetGraphic(FBlipID);
  AContaner.Transform.RotationAngle := FRotationAngle;
  //
  AContaner.AnchorType := catTwoCell;
  AContaner.AnchorPoint1.Cell := Owner.CurrentSheet.CreateCell(FAnchor.Row1, FAnchor.Col1);
  AContaner.AnchorPoint1.Offset := Point(FAnchor.Col1Offset, FAnchor.Row1Offset);
  AContaner.AnchorPoint2.Cell := Owner.CurrentSheet.CreateCell(FAnchor.Row2, FAnchor.Col2);
  AContaner.AnchorPoint2.Offset := Point(FAnchor.Col2Offset, FAnchor.Row2Offset);
  //
  if FShapeType >= 0 then
    AContaner.Shape.ShapeType := TdxSpreadSheetShapeType(FShapeType);
  AContaner.Shape.Pen.Style := FPenStyle;
  AContaner.Shape.Pen.Width := FPenWidth;
  AContaner.Shape.Pen.Brush.Style := gpbsSolid;
  AContaner.Shape.Pen.Brush.Color := FPenColor;
  if FBrushColor <> 0 then
  begin
    AContaner.Shape.Brush.Color := FBrushColor;
    AContaner.Shape.Brush.Style := gpbsSolid;
  end
  else
    AContaner.Shape.Brush.Style := gpbsSolid;
  AContaner.Shape.Brush.GradientMode := FBrushGradientMode;
  AContaner.Shape.Brush.Texture := GetGraphic(FFillBlipID);
  if GetGraphic(FFillBlipID) <> nil then
    AContaner.Shape.Brush.Style := gpbsTexture{ FBrushStyle};
  if FBrushGradientModeInverseOrder then
    AContaner.Shape.Brush.GradientPoints.InvertOrder;
//  AContaner.Restrictions

{  AContaner.Transform.
  FKeepOnMove := False;
  FKeepOnResize := False;}

{  AContaner.Transform.FlipHorizontally := FFlipHorizontally;
  AContaner.Transform.FlipVertically := FFlipVertically;}
//  AContaner.Shape.Brush.GradientPoints := FBrushGradient;

  ClearPropertyValues;
end;

procedure TdxSpreadSheetMSOCustomReader.ClearPropertyValues;
begin
  FFillBlipID := -1;
  FBlipID := -1;
  FillChar(FAnchor, SizeOf(FAnchor), 0);
  FAnchorAssigned := False;
  FBrushStyle := gpbsClear;
  FBrushGradientMode := gpbgmVertical;
  FBrushGradientModeInverseOrder := False;
  FRotationAngle := 0;
  FPenStyle := gppsSolid;
  FPenColor := 0;
  FKeepOnMove := False;
  FKeepOnResize := False;
end;

function TdxSpreadSheetMSOCustomReader.GetGraphic(AIndex: Integer): TdxSmartImage;
begin
  if (AIndex >= 0) and (AIndex < Owner.Images.Count) then
    Result := Owner.Images[AIndex]
  else
    Result := nil;
end;

function TdxSpreadSheetMSOCustomReader.HasBlip: Boolean;
begin
  Result := (FBlipID >=0) and (FBlipID < Owner.Images.Count) and (Owner.Images[FBlipID] <> nil);
end;

function TdxSpreadSheetMSOCustomReader.HasShape: Boolean;
begin
  Result := (FShapeType >= 0) and (FShapeType < 3);
end;

function TdxSpreadSheetMSOCustomReader.ReadBlip: Boolean;
var
  AStream: TMemoryStream;
  AImage: TdxSmartImage;
  AFormat: TdxImageDataFormat;
  AOffset: Integer;
begin
  Owner.Images.Count := BlipCount;
  AImage := nil;
  AFormat := dxImageUnknown;
  AOffset := 17;
  case RecordHeader.recInstance of
    oaBlipEMF, oaBlipWMF:
    begin
      Dec(AOffset);
      AFormat := dxImageBitmap;
    end;
    oaBlipJPG_RGB, oaBlipJPG_CMYK:
      AFormat := dxImageJpeg;
    oaBlipPNG:
      AFormat := dxImagePng;
    oaBlipDIB:
      AFormat := dxImageBitmap;
    oaBlipTIFF:
      AFormat := dxImageTiff;
  end;
  Stream.Position := Stream.Position + AOffset;
  if AFormat <> dxImageUnknown then
  begin
    AStream := TMemoryStream.Create;
    try
      AStream.CopyFrom(Stream, Integer(RecordHeader.recSize) - AOffset);
      AStream.Position := 0;
      if (RecordHeader.recInstance = oaBlipEMF)or (RecordHeader.recInstance = oaBlipWMF)then
        AImage := LoadMetaFile(AStream, RecordHeader.recInstance = oaBlipEMF)
      else
      begin
        AImage := TdxSmartImage.Create;
        AImage.LoadFromStream(AStream);
      end;
    finally
      AStream.Free;
    end;
  end;
  Owner.Images[BlipCount - 1] := AImage;
  Result := AImage <> nil;
end;

function TdxSpreadSheetMSOCustomReader.ReadBlipStoreContainer: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetMSOCustomReader.ReadBlipStoryEntry: Boolean;
begin
  Stream.Position := Stream.Position + SizeOf(TdxMSOBlipStoreEntry);
  Inc(FBlipCount);
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadClientAnchor: Boolean;
begin
  FAnchorAssigned := True;
  Stream.ReadBuffer(FAnchor, SizeOf(TdxMSOAnchor));
  FKeepOnMove :=  FAnchor.Options and 1 = 1;
  FKeepOnResize := FAnchor.Options and 2 = 2;
  FAnchor.Row1Offset := VOffsetToInt(FAnchor.Row1, FAnchor.Row1Offset);
  FAnchor.Col1Offset := HOffsetToInt(FAnchor.Col1, FAnchor.Col1Offset);
  FAnchor.Row2Offset := VOffsetToInt(FAnchor.Row2, FAnchor.Row2Offset);
  FAnchor.Col2Offset := HOffsetToInt(FAnchor.Col2, FAnchor.Col2Offset);
  ApplyPropertyValues;
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadDrawingContainer: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadDrawingGroupRecord: Boolean;
var
  AGroup: TdxMSODrawingGroupRecord;
begin
  Result := True;
  Stream.ReadBuffer(AGroup, SizeOf(AGroup));
  Stream.Position := Stream.Position + AGroup.sidcl * 8;
end;

function TdxSpreadSheetMSOCustomReader.ReadDrawingObjectsContainer: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadDrawingRecord: Boolean;
begin
  Result := False
end;

function TdxSpreadSheetMSOCustomReader.ReadFileShape: Boolean;
{var
  ID, Flags: Integer;}
begin
// recInstance must be following
// 1 - rect
// 2 - round rect
// 3 - ellipse
// 4 - diamond shape
// 5 - trinagle shape
// 6 -  right triangle
// 7 - parallelogram
// 8 - trapezoid
// 9 - heaxagon
// A - octagon
// B - Plus
// C - Star
// D - Arrow
// F - irregular pentagon
//10 - cube
//11 - speech ballon
//12 - seal
//13 - curved arc
//14 - line
//15 - plaque
//16 - cylinder
//17 - donut
  FShapeType := RecordHeader.recInstance;
  FShapeFlags := Reader.ReadInteger;
  FShapeID := Reader.ReadInteger;
  //
  FFlipHorizontally := RecordHeader.recInstance and fsFlipH = fsFlipH;
  FFlipVertically := RecordHeader.recInstance and fsFlipV = fsFlipV;
  //
  if FShapeType > 0 then
    FShapeType := FShapeType - 1;
  if (RecordHeader.recInstance and fsHaveShapeType = fsHaveShapeType) or (FShapeType >= 3) then
    FShapeType := -1;
  //
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadPropertiesTable: Boolean;
var
  AKey: Word;
  ASavePos, APropIndex, AValue: Integer;
  AComplexProperties: TList<TdxXLSDrawingProperty>;
  AComplexPropertySize: TList<Integer>;
  AComplexData: array of Byte;
begin
  ClearPropertyValues;
  ASavePos := Stream.Position;
  AComplexProperties := TList<TdxXLSDrawingProperty>.Create;
  AComplexPropertySize := TList<Integer>.Create;
  try
    for APropIndex := 0 to RecordHeader.recInstance - 1 do
    begin
      AKey := Reader.ReadWord;
      AValue := Reader.ReadInteger;
      // 180 - FillType  // Property_DrawingFillBlipIdentifier
      // 1BF - fillshapeboolprop
      // 1C0 - line color
      // 1DF
      // 1FF -
      if Assigned(FPropertyReaders[AKey]) then
      begin
        if AKey and $8000 <> 0 then
        begin
          AComplexProperties.Add(FPropertyReaders[AKey]);
          AComplexPropertySize.Add(AValue);
        end
        else
          FPropertyReaders[AKey](AValue, []);
      end;

      // $85 WrapText MSOWRAPMODE
      // $87 anchorText MSOANCHOR

    end;
    for APropIndex := 0 to AComplexProperties.Count - 1 do
    begin
      SetLength(AComplexData, AComplexPropertySize[APropIndex]);
      ReadComplexData(AComplexPropertySize[APropIndex], AComplexData);
      AComplexProperties[APropIndex](AComplexPropertySize[APropIndex], AComplexData);
    end;
  finally
    AComplexProperties.Free;
    AComplexPropertySize.Free;
    Stream.Position := ASavePos + Integer(RecordHeader.recSize);
  end;
  Result := True;
  ApplyPropertyValues;
end;

function TdxSpreadSheetMSOCustomReader.ReadShapeGroupContainer: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadShapeContainer: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadShapeGroupCoordinates: Boolean;
var
  R: TRect;
begin
  R := Reader.ReadRect;
  Result := True;
end;

function TdxSpreadSheetMSOCustomReader.ReadSplitMenuColorContainer: Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetMSOCustomReader.RegisterPropertyReader(AKey: Word; AProc: TdxXLSDrawingProperty);
begin
  FPropertyReaders[AKey] := AProc;
end;

function TdxSpreadSheetMSOCustomReader.GetStream: TStream;
begin
  Result := Owner.RecordReader;
end;

procedure TdxSpreadSheetMSOCustomReader.Property_BooleanProtection(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DiagramBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingBlackWhiteMode(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingBlipBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingBlipFlags(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingBlipIdentifier(
  const AValue: Integer; const AComplexData: array of Byte);
begin
  FBlipID := AValue - 1;
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingBlipName(
  const AValue: Integer; const AComplexData: array of Byte);
begin
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingConnectionPointsType(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingCropFromBottom(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingCropFromLeft(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingCropFromRight(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingCropFromTop(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillAngle(
  const AValue: Integer; const AComplexData: array of Byte);
var
  P: TdxXLSFixedPoint;
begin
  Move(Pointer(@AValue)^, Pointer(@P)^, SizeOf(P));
  FBrushGradientMode := dxSpreadSheetGetGradientMode(P.Fractional, FBrushGradientModeInverseOrder);
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBackColor(
  const AValue: Integer; const AComplexData: array of Byte);
begin
   // BKColor := AValue;
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBackColorExt(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBackOpacity(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBackTintShade(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBlip(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBlipFlags(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBlipIdentifier(
  const AValue: Integer; const AComplexData: array of Byte);
begin
  FFillBlipID := AValue - 1;
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBlipName(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillBWColor(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillColor(
  const AValue: Integer; const AComplexData: array of Byte);
begin
  with TRGBQuad(AValue) do
    FBrushColor := dxMakeAlphaColor(255 - rgbReserved, rgbBlue, rgbGreen, rgbRed);
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillColorExt(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillDzType(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillFocus(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillHeight(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillOpacity(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillOriginX(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillOriginY(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillRectBottom(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillRectLeft(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillRectRight(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillRectTop(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillReserved415(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillReserved417(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillReserved419(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillReserved421(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillReserved422(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillReserved423(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillShadeColors(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillShadePreset(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillShadeType(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillShapeOriginX(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillShapeOriginY(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillStyleBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillTintShade(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillToBottom(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillToLeft(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillToRight(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillToTop(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingFillWidth(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShape2PctHoriz(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShape2PctHorizPos(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShape2PctVert(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShape2PctVertPos(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShape2SizeRelH(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShape2SizeRelV(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShapeBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShapePosH(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShapePosRelH(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShapePosRelV(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingGroupShapePosV(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineCapStyle(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineColor(
  const AValue: Integer; const AComplexData: array of Byte);
begin
  with TRGBQuad(AValue) do
    FPenColor := dxMakeAlphaColor(255 - rgbReserved, rgbBlue, rgbGreen, rgbRed);
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineCompoundType(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineDashing(
  const AValue: Integer; const AComplexData: array of Byte);
const
  AStyle2GPPenStyle: array[0..$A] of TdxGPPenStyle =
    (gppsSolid, gppsSolid, gppsDot, gppsDashDot, gppsDashDotDot, gppsDot, gppsDash, gppsDashDot, gppsDashDot, gppsDashDot, gppsDashDot);
begin
  FPenStyle := AStyle2GPPenStyle[AValue and $F];
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineJoinStyle(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineMiterLimit(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineStyleBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingLineWidth(
  const AValue: Integer; const AComplexData: array of Byte);
begin
  FPenWidth := dxEMUToPixels(AValue);
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingRotation(const AValue: Integer; const AComplexData: array of Byte);
begin
  FRotationAngle := AValue / 65536;
  if FRotationAngle > 0 then
    while FRotationAngle > 360 do
      FRotationAngle := FRotationAngle - 360
  else
    while Abs(FRotationAngle) > 360 do
      FRotationAngle := FRotationAngle + 360;
end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingShadowColor(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingShadowStyleBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingShapeBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingShapeDescription(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingShapeHyperlink(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingShapeName(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingShapeTooltip(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingTextBooleanProperties(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingTextBottom(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingTextDirection(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingTextIdentifier(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingTextLeft(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingTextRight(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingTextTop(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingWrapBottomDistance(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingWrapLeftDistance(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingWrapRightDistance(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_DrawingWrapTopDistance(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.Property_OfficeDrawingFillType(
  const AValue: Integer; const AComplexData: array of Byte);
begin

end;

procedure TdxSpreadSheetMSOCustomReader.ReadComplexData(ASize: Integer; var AData: array of Byte); 
begin
  Stream.ReadBuffer(AData[0], Length(AData));
end;


end.


