{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetUtils;

{$I cxVer.Inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Windows, Classes, SysUtils, Variants, Math, Graphics, Generics.Collections, cxGraphics, dxCore, cxGeometry,
  cxDateUtils, cxClasses, cxVariants, dxSpreadSheetTypes, dxSpreadSheetStrs, dxTypeHelpers, dxSpreadSheetClasses;

const
  ValueIncr: array[Boolean] of Integer = (-1, 1);

type

  { TdxSpreadSheetColumnHelper }

  TdxSpreadSheetColumnHelper = class
  public
    class function IndexByName(const AName: TdxUnicodeString; AIsRCReferenceStyle: Boolean = False): Integer; inline;
    class function NameByIndex(AIndex: Integer; AIsRCReferenceStyle: Boolean = False): TdxUnicodeString; inline;
  end;

  { TdxSpreadSheetExcelColumnWidthHelper }

  TdxSpreadSheetExcelColumnWidthHelper = class
  strict private
    FFont: TFont;
    FMaxDigitWidth: Integer;
    FTwoSpaceWidth: Integer;

    procedure FontChangeHandler(Sender: TObject);
    function GetMaxDigitWidth: Integer; inline;
    function GetTwoSpaceWidth: Integer; inline;
    procedure SetFont(const Value: TFont);
  protected
    procedure CalculateMaxDigitWidth;
    procedure CalculateTwoSpaceWidth;
  public
    constructor Create;
    destructor Destroy; override;
    function CharsNumberToPixels(const ACharsNumber: Double): Integer;
    function CharsNumberToWidth(const ACharsNumber: Double): Double;
    function PixelsToCharsNumber(const APixels: Integer): Integer;
    function PixelsToSpacesNumber(const APixels: Integer): Integer;
    function PixelsToWidth(const APixels: Integer): Double;
    function SpacesNumberToPixels(const ASpaceNumber: Integer): Integer;
    function WidthToCharsNumber(const AWidth: Double): Double;
    function WidthToPixels(const AWidth: Double): Integer;
    //
    property Font: TFont read FFont write SetFont;
    property MaxDigitWidth: Integer read GetMaxDigitWidth;
    property TwoSpaceWidth: Integer read GetTwoSpaceWidth;
  end;

function dxIsCurrency(const AValue: Double): Boolean; inline;
function dxIsDateTime(const AValue: Variant): Boolean; inline;
function dxIsLogical(const AValue: Variant): Boolean; inline;
function dxIsNumberOrDateTime(const AValue: Variant): Boolean; inline;
function dxIsNumericOrDateTime(const AValue: Variant): Boolean; inline;
function dxIsText(const AValue: Variant): Boolean; inline;
function dxTryStrToBool(const AText: TdxUnicodeString; var AValue: Boolean): Boolean; inline;
function dxTryStrToDateTime(const AText: TdxUnicodeString; var AValue: TDateTime; const AFormatSettings: TFormatSettings): Boolean;
function dxTryStrToOrdinal(const AText: TdxUnicodeString; var AValue: Variant; const AFormatSettings: TdxSpreadSheetFormatSettings): Boolean;

function dxConvertToXLSDate(const AValue: Variant; out ADate: TDateTime): Boolean; inline;
function dxConvertXLSDateToNumeric(const AValue: Variant; out AResult: Double): Boolean; inline;

function dxReferenceToString(const AAbsoluteArea: TRect; R1C1Reference: Boolean): TdxUnicodeString; overload;
function dxReferenceToString(const AAbsoluteArea: TRect; R1C1Reference: Boolean; const ARowOrigin, AColumnOrigin: Integer): TdxUnicodeString; overload;
function dxReferenceToString(const ARowOrigin, AColumnOrigin: Integer; const ARow, AColumn: TdxSpreadSheetReference): TdxUnicodeString; overload;
function dxR1C1ReferenceAreaPartToString(const AOrigin: Integer; const ARef1, ARef2, ARef3: TdxSpreadSheetReference;
  const ASheet, ASheet2, APrefix: TdxUnicodeString; var AValue: TdxUnicodeString): Boolean; inline;
function dxR1C1ReferenceAreaToString(const ARowOrigin, AColumnOrigin: Integer; const ASheet: TdxUnicodeString;
  const ARow, AColumn: TdxSpreadSheetReference; const ASheet2: TdxUnicodeString; const ARow2, AColumn2: TdxSpreadSheetReference): TdxUnicodeString; inline;
function dxR1C1ReferencePartToStr(const APrefix: TdxUnicodeString; AOrigin: Integer; const AReference: TdxSpreadSheetReference; var APart: TdxUnicodeString): Boolean; inline;
function dxR1C1ReferenceToString(const ARowOrigin, AColumnOrigin: Integer; const ARow, AColumn: TdxSpreadSheetReference): TdxUnicodeString; inline;

function dxSpreadSheetCompareText(const S1, S2: Pointer; L1, L2: Integer): Integer; overload; inline;
function dxSpreadSheetCompareText(const S1: TdxUnicodeString; const S2: Pointer; L2: Integer = -1): Integer; overload; inline;
function dxSpreadSheetCompareText(const S1, S2: TdxUnicodeString): Integer; overload; inline;
function dxSpreadSheetLowerCase(const S: TdxUnicodeString): TdxUnicodeString; inline;
function dxSpreadSheetUpperCase(const S: TdxUnicodeString): TdxUnicodeString; inline;

function dxSpreadSheetCharIsEqual(const AChar1, AChar2: TdxUnicodeChar): Boolean; inline;
function dxSpreadSheetTextIsEqual(const AText1, AText2: TdxUnicodeString): Boolean; inline;

function dxIntPower(const Base, Exp: Integer): Integer;

function dxEMUToPixels(const Value: Int64): Integer;
function dxEMUToPixelsF(const Value: Int64): Single;
function dxPixelsToEMU(const Value: Integer): Int64;
function dxPixelsToEMUF(const Value: Single): Int64;

function dxDateTimeToRealDateTime(const AValue: TDateTime; ADataTimeSystem: TdxSpreadSheetDateTimeSystem): TDateTime;
function dxRealDateTimeToDateTime(const AValue: TDateTime; ADataTimeSystem: TdxSpreadSheetDateTimeSystem): TDateTime;

function dxSpreadSheetArea(const ATopLeft: TPoint; const ASize: TSize): TRect; inline;
function dxSpreadSheetAreaHeight(const AArea: TRect): Integer; inline;
function dxSpreadSheetAreaWidth(const AArea: TRect): Integer; inline;
function dxSpreadSheetAreaSize(const AArea: TRect): TSize; inline;
function dxSpreadSheetCellsUnion(const AArea1, AArea2: TRect): TRect; inline;
function dxSpreadSheetContains(const AArea, ATestArea: TRect): Boolean; overload; inline;
function dxSpreadSheetContains(const AArea: TRect; ARow, AColumn: Integer): Boolean; overload; inline;
function dxSpreadSheetGetRealArea(const AArea: TRect): TRect;
function dxSpreadSheetIntersects(const AArea1, AArea2: TRect): Boolean; overload; inline;
function dxSpreadSheetIntersects(const AArea1, AArea2: TRect; var AArea3: TRect): Boolean; overload; inline;
function dxSpreadSheetIsEmptyValue(const AValue: Variant): Boolean; inline;
function dxSpreadSheetIsEntireColumn(const AArea: TRect): Boolean; inline;
function dxSpreadSheetIsEntireRow(const AArea: TRect): Boolean; inline;
function dxSpreadSheetIsEntireRowOrColumn(const AArea: TRect): Boolean; inline;
function dxSpreadSheetIsErrorString(const S: TdxUnicodeString; var ACode: TdxSpreadSheetFormulaErrorCode): Boolean; inline;
function dxSpreadSheetIsSingleCellArea(const R: TRect): Boolean; inline;
function dxSpreadSheetIsValidArea(const AArea: TRect): Boolean; inline;

procedure dxSpreadSheetValidate(var AValue: Integer; const AMinValue, AMaxValue: Integer); inline;

function dxSpreadSheetErrorCodeToString(ACode: TdxSpreadSheetFormulaErrorCode): TdxUnicodeString;

function dxSpreadSheetExtractOperation(ACondition: TdxUnicodeString;
  var AOperation: TdxSpreadSheetFormulaOperation; const AFormatSettings: TdxSpreadSheetFormatSettings): Variant;

implementation

uses
  StrUtils;

const
  dxExcelColumnWidthPadding = 2 * 2 {Edge margins} + 1 {GridLine width};

function dxIsCurrency(const AValue: Double): Boolean;
begin
  Result := Ceil(100000000 * Abs(AValue)) mod 10000 = 0;
end;

function dxIsDateTime(const AValue: Variant): Boolean;
begin
  Result := VarIsType(AValue, varDate);
end;

function dxIsLogical(const AValue: Variant): Boolean;
begin
  Result := VarIsType(AValue, varBoolean);
end;

function dxIsNumberOrDateTime(const AValue: Variant): Boolean;
begin
 Result := dxIsNumericOrDateTime(AValue) and not dxIsLogical(AValue);
end;

function dxIsNumericOrDateTime(const AValue: Variant): Boolean;
begin
 Result := VarIsNumeric(AValue) or dxIsDateTime(AValue);
end;

function dxIsText(const AValue: Variant): Boolean;
begin
  Result := VarIsStr(AValue);
end;

function dxTryStrToBool(const AText: TdxUnicodeString; var AValue: Boolean): Boolean;
begin
  Result := True;
  if dxSpreadSheetTextIsEqual(AText, cxGetResourceString(@sdxTrue)) then
    AValue := True
  else
    if dxSpreadSheetTextIsEqual(AText, cxGetResourceString(@sdxFalse)) then
      AValue := False
    else
      Result := False;
end;

function dxTryStrToDateTime(const AText: TdxUnicodeString; var AValue: TDateTime; const AFormatSettings: TFormatSettings): Boolean;
begin
  Result := False;
  if TryStrToDateTime(AText, AValue, AFormatSettings) then 
  try
    AValue := cxGetLocalCalendar.ToDateTime(cxStrToDate(AText, AFormatSettings));
    Result := True;
  except
  end;
end;

function dxTryStrToOrdinal(const AText: TdxUnicodeString; var AValue: Variant; const AFormatSettings: TdxSpreadSheetFormatSettings): Boolean;
var
  ABoolValue: Boolean;
  ADateTimeValue: TDateTime;
  AFloatValue: Double;
begin
  Result := (Pos('=', AText) <= 0) and (Pos('"', AText) <= 0);
  if not Result then
    Exit;

  if AText = '' then
    AValue := Null
  else
    if TryStrToFloat(AText, AFloatValue, AFormatSettings.Data) then
    begin
      if IsZero(Frac(AFloatValue)) then 
        AValue := Int(AFloatValue)
      else
        if dxIsCurrency(AFloatValue) then
          AValue := FloatToCurr(AFloatValue)
        else
          AValue := AFloatValue;
    end
    else
      if dxTryStrToBool(AText, ABoolValue) then
        AValue := ABoolValue
      else
         if dxTryStrToDateTime(AText, ADateTimeValue, AFormatSettings.Data) then
          AValue := dxRealDateTimeToDateTime(ADateTimeValue, AFormatSettings.DateTimeSystem)
        else
          Result := False;
end;


function dxConvertToXLSDate(const AValue: Variant; out ADate: TDateTime): Boolean; inline;
begin
  Result := False;
  if VarIsStr(AValue) then
  begin
    Result := TextToDateEx(AValue, ADate);
    if Result and ((ADate >= 1)) and (ADate <= 60) then
      ADate := ADate - 1;
    Result := Result and (ADate > 0);
  end;
end;

function dxConvertXLSDateToNumeric(const AValue: Variant; out AResult: Double): Boolean; inline;
begin
  Result := dxIsDateTime(AValue);
  if Result then
  begin
    AResult := AValue;
    if (AValue >= 0) and (AValue <=60) then
      AResult := AResult - 1;
  end;
end;

function dxEMUToPixelsFactor: Double; inline;
begin
  Result := cxGetCurrentDPI / 914400;
end;

function dxEMUToPixels(const Value: Int64): Integer;
begin
  Result := Round(Value * dxEMUToPixelsFactor);
end;

function dxEMUToPixelsF(const Value: Int64): Single;
begin
  Result := Value * dxEMUToPixelsFactor;
end;

function dxPixelsToEMU(const Value: Integer): Int64;
begin
  Result := Round(Value / dxEMUToPixelsFactor);
end;

function dxPixelsToEMUF(const Value: Single): Int64;
begin
  Result := Round(Value / dxEMUToPixelsFactor);
end;

function dxDateTimeToRealDateTime(const AValue: TDateTime; ADataTimeSystem: TdxSpreadSheetDateTimeSystem): TDateTime;
begin
  if ADataTimeSystem = dts1904 then
    Result := AValue + EncodeDate(1904, 1, 1)
  else
    Result := AValue;
end;

function dxRealDateTimeToDateTime(const AValue: TDateTime; ADataTimeSystem: TdxSpreadSheetDateTimeSystem): TDateTime;
begin
  if ADataTimeSystem = dts1904 then
    Result := AValue - EncodeDate(1904, 1, 1)
  else
    Result := AValue;
end;

function dxIntPower(const Base, Exp: Integer): Integer;
var
  I: Integer;
begin
  Result := 1;
  for I := 1 to Exp do
    Result := Result * Base;
end;

function dxReferenceToString(const AAbsoluteArea: TRect; R1C1Reference: Boolean): TdxUnicodeString;
begin
  Result := dxReferenceToString(AAbsoluteArea, R1C1Reference, -1, -1);
end;

function dxReferenceToString(const AAbsoluteArea: TRect; R1C1Reference: Boolean; const ARowOrigin, AColumnOrigin: Integer): TdxUnicodeString;

  procedure SetupReference(var AReference: TdxSpreadSheetReference; APosition, AOrigin: Integer);
  begin
    AReference.Reset;
    AReference.IsAbsolute := AOrigin < 0;
    if AReference.IsAbsolute then
      AReference.Offset := APosition
    else
      AReference.Offset := APosition - AOrigin;
  end;

var
  AColumn1: TdxSpreadSheetReference;
  AColumn2: TdxSpreadSheetReference;
  ARow1: TdxSpreadSheetReference;
  ARow2: TdxSpreadSheetReference;
begin
  if R1C1Reference then
  begin
    SetupReference(ARow1, AAbsoluteArea.Top, ARowOrigin);
    SetupReference(AColumn1, AAbsoluteArea.Left, AColumnOrigin);
    if (AAbsoluteArea.Left <> AAbsoluteArea.Right) or (AAbsoluteArea.Top <> AAbsoluteArea.Bottom) then
    begin
      SetupReference(ARow2, AAbsoluteArea.Bottom, ARowOrigin);
      SetupReference(AColumn2, AAbsoluteArea.Right, AColumnOrigin);
      Result := dxR1C1ReferenceAreaToString(ARowOrigin, AColumnOrigin, '', ARow1, AColumn1, '', ARow2, AColumn2);
    end
    else
      Result := dxR1C1ReferenceToString(ARowOrigin, AColumnOrigin, ARow1, AColumn1);
  end
  else
  begin
    Result := TdxSpreadSheetColumnHelper.NameByIndex(AAbsoluteArea.Left, False) + IntToStr(AAbsoluteArea.Top + 1);
    if (AAbsoluteArea.Left <> AAbsoluteArea.Right) or (AAbsoluteArea.Top <> AAbsoluteArea.Bottom) then
      Result := Result + ':' + TdxSpreadSheetColumnHelper.NameByIndex(AAbsoluteArea.Right, False) + IntToStr(AAbsoluteArea.Bottom + 1);
  end;
end;

function dxReferenceToString(const ARowOrigin, AColumnOrigin: Integer; const ARow, AColumn: TdxSpreadSheetReference): TdxUnicodeString;
begin
  if (ARow.ActualValue(ARowOrigin) < 0) or (AColumn.ActualValue(AColumnOrigin) < 0) then
  begin
    Result := serRefError;
    Exit;
  end;

  if not AColumn.IsAllItems then
  begin
    Result := TdxSpreadSheetColumnHelper.NameByIndex(AColumn.ActualValue(AColumnOrigin), False);
    if AColumn.IsAbsolute then
      Result := dxAbsoluteReferenceChar + Result;
  end
  else
    Result := '';

  if not ARow.IsAllItems then
  begin
    if ARow.IsAbsolute then
      Result := Result + dxAbsoluteReferenceChar;
    Result := Result + IntToStr(ARow.ActualValue(ARowOrigin) + 1);
  end;
end;

function dxR1C1ReferencePartToStr(const APrefix: TdxUnicodeString;
  AOrigin: Integer; const AReference: TdxSpreadSheetReference; var APart: TdxUnicodeString):Boolean;
begin
  Result := AReference.ActualValue(AOrigin) >= 0;
  if Result then
  begin
    APart := APart + APrefix;
    if AReference.IsAbsolute then
    begin
      if not AReference.IsAllItems then
        APart := APart + IntToStr(AReference.Offset + 1)
    end
    else
      if AReference.Offset <> 0 then
         APart := APart + dxReferenceLeftParenthesis + IntToStr(AReference.Offset) + dxReferenceRightParenthesis;
  end
  else
    APart := serRefError;
end;

function dxR1C1ReferenceAreaPartToString(const AOrigin: Integer; const ARef1, ARef2, ARef3: TdxSpreadSheetReference;
  const ASheet, ASheet2, APrefix: TdxUnicodeString; var AValue: TdxUnicodeString): Boolean;
begin
  Result := ARef3.IsAllItems;
  if Result then
  begin
    AValue := ASheet;
    dxR1C1ReferencePartToStr(APrefix, AOrigin, ARef1, AValue);
  end;
  if (ARef2 <> ARef1) or (ASheet2 <> '')  then
  begin
    AValue := AValue + dxAreaSeparator + ASheet2;
    dxR1C1ReferencePartToStr(APrefix, AOrigin, ARef2, AValue);
  end;
end;

function dxR1C1ReferenceAreaToString(const ARowOrigin, AColumnOrigin: Integer;
  const ASheet: TdxUnicodeString; const ARow, AColumn: TdxSpreadSheetReference;
  const ASheet2: TdxUnicodeString; const ARow2, AColumn2: TdxSpreadSheetReference): TdxUnicodeString;
begin
  if not dxR1C1ReferenceAreaPartToString(ARowOrigin, ARow, ARow2, AColumn2, ASheet, ASheet2, dxRCRowReferenceChar, Result) then
    if not dxR1C1ReferenceAreaPartToString(AColumnOrigin, AColumn, AColumn2, ARow2, ASheet, ASheet2, dxRCColumnReferenceChar, Result) then
    begin
      Result := ASheet + dxR1C1ReferenceToString(ARowOrigin, AColumnOrigin, ARow, AColumn) + dxAreaSeparator +
        ASheet2 + dxR1C1ReferenceToString(ARowOrigin, AColumnOrigin, ARow2, AColumn2);
    end;
end;

function dxR1C1ReferenceToString(const ARowOrigin, AColumnOrigin: Integer; const ARow, AColumn: TdxSpreadSheetReference): TdxUnicodeString;
begin
  Result := '';
  if not dxR1C1ReferencePartToStr(dxRCRowReferenceChar, ARowOrigin, ARow, Result) or
    not dxR1C1ReferencePartToStr(dxRCColumnReferenceChar, AColumnOrigin, AColumn, Result) then
    Result := serRefError;
end;

function dxSpreadSheetCompareText(const S1, S2: Pointer; L1, L2: Integer): Integer;
begin
  Result := CompareStringW(LOCALE_USER_DEFAULT, NORM_IGNORECASE, S1, L1, S2, L2) - 2;
end;

function dxSpreadSheetCompareText(const S1: TdxUnicodeString; const S2: Pointer; L2: Integer): Integer;
begin
  if L2 = -1 then
    L2 := Length(S1);
  Result := dxSpreadSheetCompareText(PWideChar(S1), S2, Length(S1), L2);
end;

function dxSpreadSheetCompareText(const S1, S2: TdxUnicodeString): Integer; 
begin
  Result := dxSpreadSheetCompareText(PWideChar(S1), PWideChar(S2), Length(S1), Length(S2));
end;

function dxSpreadSheetLowerCase(const S: TdxUnicodeString): TdxUnicodeString;
var
  Len: Integer;
begin
  Len := Length(S);
  SetString(Result, PWideChar(S), Len);
  if Len > 0 then CharLowerBuffW(Pointer(Result), Len);
end;

function dxSpreadSheetUpperCase(const S: TdxUnicodeString): TdxUnicodeString;
var
  Len: Integer;
begin
  Len := Length(S);
  SetString(Result, PWideChar(S), Len);
  if Len > 0 then CharUpperBuffW(Pointer(Result), Len);
end;

function dxSpreadSheetCharIsEqual(const AChar1, AChar2: TdxUnicodeChar): Boolean;
begin
  Result := Integer(AChar1) = Integer(AChar2);
end;

function dxSpreadSheetTextIsEqual(const AText1, AText2: TdxUnicodeString): Boolean;
begin
  Result := dxSpreadSheetCompareText(AText1, AText2) = 0;
end;

function dxSpreadSheetArea(const ATopLeft: TPoint; const ASize: TSize): TRect;
begin
  Result.TopLeft := ATopLeft;
  Result.BottomRight := cxPointOffset(Result.TopLeft, ASize.cx - 1, ASize.cy - 1);
end;

function dxSpreadSheetAreaHeight(const AArea: TRect): Integer;
begin
  Result := AArea.Bottom - AArea.Top + 1;
end;

function dxSpreadSheetAreaWidth(const AArea: TRect): Integer;
begin
  Result := AArea.Right - AArea.Left + 1;
end;

function dxSpreadSheetAreaSize(const AArea: TRect): TSize;
begin
  Result := cxSize(dxSpreadSheetAreaWidth(AArea), dxSpreadSheetAreaHeight(AArea));
end;

function dxSpreadSheetCellsUnion(const AArea1, AArea2: TRect): TRect;
begin
  Result.Left := Min(AArea1.Left, AArea2.Left);
  Result.Top := Min(AArea1.Top, AArea2.Top);
  Result.Right := Max(AArea1.Right, AArea2.Right);
  Result.Bottom := Max(AArea1.Bottom, AArea2.Bottom);
end;

function dxSpreadSheetContains(const AArea, ATestArea: TRect): Boolean;
begin
  Result :=
    dxSpreadSheetContains(AArea, ATestArea.Top, ATestArea.Left) and
    dxSpreadSheetContains(AArea, ATestArea.Bottom, ATestArea.Right);
end;

function dxSpreadSheetContains(const AArea: TRect; ARow, AColumn: Integer): Boolean;
begin
  Result := (AColumn >= AArea.Left) and (AColumn <= AArea.Right) and (ARow >= AArea.Top) and (ARow <= AArea.Bottom);
end;

function dxSpreadSheetGetRealArea(const AArea: TRect): TRect;
begin
  Result := cxRectAdjust(AArea);
  Result.Right := Min(Result.Right, dxSpreadSheetMaxColumnIndex);
  Result.Bottom := Min(Result.Bottom, dxSpreadSheetMaxRowIndex);
end;

function dxSpreadSheetIntersects(const AArea1, AArea2: TRect): Boolean;
var
  R: TRect;
begin
  Result := dxSpreadSheetIntersects(AArea1, AArea2, R);
end;

function dxSpreadSheetIntersects(const AArea1, AArea2: TRect; var AArea3: TRect): Boolean;
begin
  AArea3.Left := Max(AArea2.Left, AArea1.Left);
  AArea3.Top := Max(AArea2.Top, AArea1.Top);
  AArea3.Right := Min(AArea2.Right, AArea1.Right);
  AArea3.Bottom := Min(AArea2.Bottom, AArea1.Bottom);
  Result := (AArea3.Left <= AArea3.Right) and (AArea3.Top <= AArea3.Bottom);
  if not Result then
    AArea3 := cxNullRect;
end;

function dxSpreadSheetIsEmptyValue(const AValue: Variant): Boolean; inline;
begin
  Result := VarIsEmpty(AValue) or VarIsNull(AValue);
end;

function dxSpreadSheetIsSingleCellArea(const R: TRect): Boolean;
begin
  Result := (R.Width = 0) and (R.Height = 0);
end;

function dxSpreadSheetIsValidArea(const AArea: TRect): Boolean;
begin
  Result := (AArea.Left >= 0) and (AArea.Top >= 0) and (AArea.Right >= AArea.Left) and (AArea.Bottom >= AArea.Top);
end;

function dxSpreadSheetIsEntireColumn(const AArea: TRect): Boolean;
begin
  Result := (AArea.Top = 0) and (AArea.Bottom >= dxSpreadSheetMaxRowIndex);
end;

function dxSpreadSheetIsEntireRow(const AArea: TRect): Boolean;
begin
  Result := (AArea.Left = 0) and (AArea.Right >= dxSpreadSheetMaxColumnIndex);
end;

function dxSpreadSheetIsEntireRowOrColumn(const AArea: TRect): Boolean;
begin
  Result := dxSpreadSheetIsEntireRow(AArea) or dxSpreadSheetIsEntireColumn(AArea);
end;

function dxSpreadSheetIsErrorString(const S: TdxUnicodeString; var ACode: TdxSpreadSheetFormulaErrorCode): Boolean; inline;
var
  I: TdxSpreadSheetFormulaErrorCode;
begin
  for I := Low(TdxSpreadSheetFormulaErrorCode) to High(TdxSpreadSheetFormulaErrorCode) do
  begin
    Result := dxSpreadSheetCompareText(S, dxSpreadSheetErrorCodeToString(I)) = 0;
    if not Result then
      Continue;
    ACode := I;
    Break;
  end;
end;

procedure dxSpreadSheetValidate(var AValue: Integer; const AMinValue, AMaxValue: Integer);
begin
  if AValue < AMinValue then
    AValue := AMinValue
  else
    if AValue > AMaxValue then
      AValue := AMaxValue
end;

{ TdxSpreadSheetColumnHelper }

class function TdxSpreadSheetColumnHelper.IndexByName(
  const AName: TdxUnicodeString; AIsRCReferenceStyle: Boolean = False): Integer;
var
  I: Integer;
begin
  if AIsRCReferenceStyle then
    Result := StrToInt(AName)
  else
  begin
    Result := 0;
    for I := Length(AName) downto 1 do
      Result := Result + dxIntPower(26, Length(AName) - I) * (Byte(AName[I]) - Byte('A') + 1);
    Dec(Result);
  end;
end;

class function TdxSpreadSheetColumnHelper.NameByIndex(
  AIndex: Integer; AIsRCReferenceStyle: Boolean = False): TdxUnicodeString;
begin
  if AIsRCReferenceStyle then
    Result := IntToStr(AIndex + 1)
  else
  begin
    Result := '';
    if AIndex >= 26 then
      Result := Result + NameByIndex(AIndex div 26 - 1, AIsRCReferenceStyle);
    Result := Result + TdxUnicodeChar(Byte('A') + AIndex mod 26);
  end;
end;

{ TdxSpreadSheetExcelColumnWidthHelper }

constructor TdxSpreadSheetExcelColumnWidthHelper.Create;
begin
  inherited Create;
  FFont := TFont.Create;
  FFont.OnChange := FontChangeHandler;
end;

destructor TdxSpreadSheetExcelColumnWidthHelper.Destroy;
begin
  FreeAndNil(FFont);
  inherited Destroy;
end;

function TdxSpreadSheetExcelColumnWidthHelper.CharsNumberToPixels(const ACharsNumber: Double): Integer;
begin
  Result := WidthToPixels(CharsNumberToWidth(ACharsNumber));
end;

function TdxSpreadSheetExcelColumnWidthHelper.CharsNumberToWidth(const ACharsNumber: Double): Double;
begin
  Result := Trunc((ACharsNumber * MaxDigitWidth + dxExcelColumnWidthPadding) / MaxDigitWidth * 256) / 256;
end;

function TdxSpreadSheetExcelColumnWidthHelper.PixelsToCharsNumber(const APixels: Integer): Integer;
begin
  Result := Trunc(WidthToCharsNumber(PixelsToWidth(APixels)));
end;

function TdxSpreadSheetExcelColumnWidthHelper.PixelsToSpacesNumber(const APixels: Integer): Integer;
begin
  Result := MulDiv(APixels, 2, TwoSpaceWidth);
end;

function TdxSpreadSheetExcelColumnWidthHelper.PixelsToWidth(const APixels: Integer): Double;
begin
  Result := CharsNumberToWidth((APixels - dxExcelColumnWidthPadding) / MaxDigitWidth);
end;

function TdxSpreadSheetExcelColumnWidthHelper.WidthToCharsNumber(const AWidth: Double): Double;
begin
  Result := Trunc((WidthToPixels(AWidth) - dxExcelColumnWidthPadding) / MaxDigitWidth * 100 + 0.5) / 100;
end;

function TdxSpreadSheetExcelColumnWidthHelper.WidthToPixels(const AWidth: Double): Integer;
begin
  Result := Trunc((256 * AWidth + Trunc(128 / MaxDigitWidth)) / 256 * MaxDigitWidth);
end;

procedure TdxSpreadSheetExcelColumnWidthHelper.CalculateMaxDigitWidth;
var
  C: Char;
begin
  FMaxDigitWidth := 0;
  cxScreenCanvas.Font := Font;
  for C := '0' to '9' do
    FMaxDigitWidth := Max(FMaxDigitWidth, cxScreenCanvas.TextWidth(C));
  cxScreenCanvas.Dormant;
end;

procedure TdxSpreadSheetExcelColumnWidthHelper.CalculateTwoSpaceWidth;
var
  AMetrics: TTextMetric;
begin
  cxScreenCanvas.Font := Font;
  GetTextMetrics(cxScreenCanvas.Handle, AMetrics);
  cxScreenCanvas.Dormant;
  FTwoSpaceWidth := AMetrics.tmAscent + AMetrics.tmDescent;
end;

procedure TdxSpreadSheetExcelColumnWidthHelper.FontChangeHandler(Sender: TObject);
begin
  FTwoSpaceWidth := 0;
  FMaxDigitWidth := 0;
end;

function TdxSpreadSheetExcelColumnWidthHelper.GetMaxDigitWidth: Integer;
begin
  if FMaxDigitWidth = 0 then
    CalculateMaxDigitWidth;
  Result := FMaxDigitWidth;
end;

function TdxSpreadSheetExcelColumnWidthHelper.GetTwoSpaceWidth: Integer;
begin
  if FTwoSpaceWidth = 0 then
    CalculateTwoSpaceWidth;
  Result := FTwoSpaceWidth;
end;

procedure TdxSpreadSheetExcelColumnWidthHelper.SetFont(const Value: TFont);
begin
  FFont.Assign(Value);
end;

function TdxSpreadSheetExcelColumnWidthHelper.SpacesNumberToPixels(const ASpaceNumber: Integer): Integer;
begin
  Result := MulDiv(ASpaceNumber, TwoSpaceWidth, 2);
end;

function dxSpreadSheetErrorCodeToString(ACode: TdxSpreadSheetFormulaErrorCode): TdxUnicodeString;
begin
  Result := '';
  case ACode of
    ecNull:
      Result := cxGetResourceString(@serNullError);
    ecDivByZero:
      Result := cxGetResourceString(@serDivZeroError);
    ecValue:
      Result := cxGetResourceString(@serValueError);
    ecRefErr:
      Result := cxGetResourceString(@serRefError);
    ecNUM:
      Result := cxGetResourceString(@serNumError);
    ecName:
      Result := cxGetResourceString(@serNameError);
    ecNA:
      Result := cxGetResourceString(@serNAError);
  end;
end;

function dxSpreadSheetExtractOperation(ACondition: TdxUnicodeString;
  var AOperation: TdxSpreadSheetFormulaOperation; const AFormatSettings: TdxSpreadSheetFormatSettings): Variant;
var
  I, L, L1: Integer;
const
  ACandidate: array[0..5] of TdxSpreadSheetFormulaOperation = (opLE, opGE, opNE, opLT, opGT, opEQ);
begin
  AOperation := opEQ;
  for I := 0 to High(ACandidate) do
  begin
    L := Length(dxDefaultOperations[ACandidate[I]]);
    L1 := Min(L, Length(ACondition));
    if dxSpreadSheetCompareText(PWideChar(ACondition), PWideChar(dxDefaultOperations[ACandidate[I]]), L1, L) = 0 then
    begin
      AOperation := ACandidate[I];
      Delete(ACondition, 1, L);
      Break;
    end;
  end;
  if not dxTryStrToOrdinal(ACondition, Result, AFormatSettings) then
    Result := dxSpreadSheetUpperCase(ACondition);
end;

end.
