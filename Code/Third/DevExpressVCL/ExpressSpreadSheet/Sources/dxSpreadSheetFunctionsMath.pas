{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctionsMath;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, SysUtils, DateUtils, Variants, Math, dxCore, dxSpreadSheetTypes, dxSpreadSheetCore, dxSpreadSheetCoreHelpers,
  dxSpreadSheetUtils, dxSpreadSheetFormulas, dxSpreadSheetFunctionsStatistical;

procedure fnABS(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnACOS(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnACOSH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnACOT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnACOTH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnASIN(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnASINH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnATAN(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnATAN2(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnATANH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnBase(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCeiling(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCOS(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCOSH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCOT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCOTH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCSC(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCSCH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCeiling_Math(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCeiling_Precise(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCombin(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnCombinA(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnDecimal(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnDegrees(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnExp(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnEven(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFact(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFactDouble(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFloor(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFloor_Math(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFloor_Precise(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnINT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnIso_Ceiling(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLN(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLOG(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLOG10(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMMult(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMOD(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMRound(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnOdd(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnPI(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnPower(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnProduct(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnQuotient(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRadians(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRand(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRandBetween(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRound(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRoundDown(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRoundUp(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSec(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSech(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSign(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSin(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSinH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSQRT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSQRTPI(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSum(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSumIF(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSumSQ(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTan(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTanH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTrunc(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

function dxSpreadSheetRound(Sender: TdxSpreadSheetFormulaResult; var ANumber: Extended; const ADigits: Integer): Boolean; overload;
procedure dxSpreadSheetRound(var ANumber: Extended; const APrecision: Extended); overload;

implementation

type
  TFormulaAccess = class(TdxSpreadSheetFormula);
  TTokenAccess = class(TdxSpreadSheetFormulaToken);

const
  Resolution = 1E-15;
  AllowedChars: array[0..35] of Char =
    ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
     'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

function dxGetCharIndex(AChar: Char): Integer; inline;
var
  I: Byte;
begin
  Result := -1;
  for I := 0 to 35 do
    if AChar = AllowedChars[I] then
    begin
      Result := I;
      Break;
    end;
end;

function dxGetDoubleFactorial(AValue: Integer): Extended;
begin
  Result := 1;
  while AValue > 1 do
  begin
    Result := Int(Result * AValue);
    AValue := AValue - 2;
  end;
end;

function dxGetCombinA(N, K: Integer): Extended;
var
  I: Integer;
begin
  Result := 1;
  for I := N to N + K - 1 do
    Result := Int(Result * I);
  Result := Result / dxGetFactorial(K);
end;

function dxGetRoundPrecision(Sender: TdxSpreadSheetFormulaResult; const ADigits: Integer; var APrecision: Extended): Boolean;
begin
  Result := False;
  try
    APrecision := IntPower(10, ADigits);
    Result := True;
  except
    on EOverflow do
      Sender.SetError(ecNUM)
  end;
end;

function dxExecRoundDown(Sender: TdxSpreadSheetFormulaResult; var ANumber: Extended; const ADigits: Integer): Boolean;
var
  APrecision, AProduct: Extended;
  AMultiplier: Int64;
begin
  Result := dxGetRoundPrecision(Sender, ADigits, APrecision);
  if Result then
  begin
    AProduct := Abs(ANumber * APrecision);
    AMultiplier := Trunc(AProduct);
    if Abs(Abs(AProduct - AMultiplier) - 1) <= 1E-10 then
      Inc(AMultiplier);
    ANumber := Sign(ANumber) * AMultiplier / APrecision;
  end;
end;

function dxSpreadSheetRound(Sender: TdxSpreadSheetFormulaResult; var ANumber: Extended; const ADigits: Integer): Boolean;
var
  APrecision: Extended;
begin
  Result := dxGetRoundPrecision(Sender, ADigits, APrecision);
  if Result then
    dxSpreadSheetRound(ANumber, APrecision);
end;

procedure dxSpreadSheetRound(var ANumber: Extended; const APrecision: Extended);
const
  LimitFracValue = 0.5;
var
  A, B: Extended;
  AEpsilon: Extended;
  AIntProduct: Extended;
  AProduct: Extended;
begin
  AProduct := ANumber * APrecision;
  AIntProduct := Int(AProduct);
  A := Abs(AProduct);
  B := Abs(AIntProduct + Sign(ANumber) * LimitFracValue);
  AEpsilon := Max(Min(A, B) * Resolution, Resolution);
  if CompareValue(A, B, AEpsilon) <> -1 then
    AIntProduct := AIntProduct + Sign(ANumber);
  ANumber := AIntProduct / APrecision;
end;

procedure fnABS(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Abs(AParameter));
end;

procedure fnACOS(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if Abs(AParameter) > 1 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(ArcCos(AParameter));
end;

procedure fnACOSH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if Abs(AParameter) < 1 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(ArcCosH(AParameter));
end;

procedure fnACOT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(ArcCot(AParameter));
end;

procedure fnACOTH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if Abs(AParameter) <= 1 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(ArcCotH(AParameter));
end;

procedure fnASIN(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if Abs(AParameter) > 1 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(ArcSin(AParameter));
end;

procedure fnASINH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(ArcSinH(AParameter));
end;

procedure fnATAN(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(ArcTan(AParameter));
end;

procedure fnATAN2(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameterX, AParameterY: Variant;
begin
  if Sender.ExtractNumericParameter(AParameterX, AParams) and Sender.ExtractNumericParameter(AParameterY, AParams.Next) then
    if (AParameterX = 0) and (AParameterY = 0) then
      Sender.SetError(ecDivByZero)
    else
      Sender.AddValue(ArcTan2(AParameterY, AParameterX));
end;

procedure fnATANH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if Abs(AParameter) >= 1 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(ArcTanH(AParameter));
end;

procedure fnBase(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

  function GetBase(ANumber: Int64; const ARadix, AMinLength: Int64): string;
  var
    ARatio: Int64;
    I: Integer;
  begin
    Result := '';
    while ANumber >= ARadix do
    begin
      ARatio := ANumber div ARadix;
      Result := AllowedChars[ANumber - ARatio * ARadix] + Result;
      ANumber := ARatio;
    end;
    Result := AllowedChars[ANumber] + Result;
    for I := Length(Result) + 1 to AMinLength do
      Result := '0' + Result;
  end;

var
  ANumber, ARadix, AMinLength: Variant;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(ARadix, AParams, 1) and
    Sender.ExtractNumericParameterDef(AMinLength, 0, 0, AParams, 2) then
  begin
    ARadix := Trunc(ARadix);
    if (ANumber < 0) or (ARadix < 2) or (ARadix > 36) or (AMinLength < 0) then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(GetBase(Trunc(ANumber), ARadix, Trunc(AMinLength)));
  end;
end;

procedure fnCeiling(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken); 
var
  ANumber, ASignificance: Variant;
  AMultiplier: Int64;
  ARatio: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(ASignificance, AParams.Next) then
    if (ANumber > 0) and (ASignificance < 0) then
      Sender.SetError(ecNUM)
    else
      if (ANumber = 0) or (ASignificance = 0) then
        Sender.AddValue(0)
      else
      begin
        ARatio := Abs(ANumber / ASignificance);
        AMultiplier := Trunc(ARatio);
        if Frac(ARatio) > Resolution then
          Inc(AMultiplier, Integer(ANumber * ASignificance > 0));
        Sender.AddValue(Sign(Double(ANumber)) * Abs(ASignificance) * AMultiplier);
      end;
end;

procedure fnCOS(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Cos(AParameter));
end;

procedure fnCOSH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(CosH(AParameter));
end;

procedure fnCOT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter = 0 then
      Sender.SetError(ecDivByZero)
    else
      Sender.AddValue(Cot(AParameter));
end;

procedure fnCOTH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter = 0 then
      Sender.SetError(ecDivByZero)
    else
      Sender.AddValue(CotH(AParameter));
end;

procedure fnCSC(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter = 0 then
      Sender.SetError(ecDivByZero)
    else
      Sender.AddValue(Cosecant(AParameter));
end;

procedure fnCSCH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter = 0 then
      Sender.SetError(ecDivByZero)
    else
      Sender.AddValue(CscH(AParameter));
end;

procedure fnCeiling_Math(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken); 
var
  ANumber, ASignificance, AMode: Variant;
  AMultiplier: Int64;
  ARatio: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameterDef(ASignificance, 1, 1, AParams, 1) and
     Sender.ExtractNumericParameterDef(AMode, 0, 0, AParams, 2) then
    if (ANumber = 0) or (ASignificance = 0) then
      Sender.AddValue(0)
    else
    begin
      ARatio := Abs(ANumber / ASignificance);
      AMultiplier := Trunc(ARatio);
      if ANumber > 0 then
        Inc(AMultiplier, Integer(Frac(ARatio) <> 0))
      else
        Inc(AMultiplier, Integer((Frac(ARatio) <> 0) and (AMode <> 0)));
      Sender.AddValue(Sign(Double(ANumber)) * Abs(ASignificance) * AMultiplier);
    end;
end;

procedure fnCeiling_Precise(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken); 
var
  ANumber, ASignificance: Variant;
  AMultiplier: Int64;
  ARatio: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameterDef(ASignificance, 1, 1, AParams, 1) then
    if (ANumber = 0) or (ASignificance = 0) then
      Sender.AddValue(0)
    else
    begin
      ARatio := Abs(ANumber / ASignificance);
      AMultiplier := Trunc(ARatio);
      Inc(AMultiplier, Integer((ANumber > 0) and (Frac(ARatio) <> 0)));
      Sender.AddValue(Sign(Double(ANumber)) * Abs(ASignificance) * AMultiplier);
    end;
end;

procedure fnCombin(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, AChosen: Variant;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(AChosen, AParams.Next) then
  begin
    ANumber := Trunc(ANumber);
    AChosen := Trunc(AChosen);
    if (ANumber < 0) or (AChosen < 0) or (ANumber < AChosen) then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(dxGetCombin(ANumber, AChosen));
  end;
end;

procedure fnCombinA(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, AChosen: Variant;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(AChosen, AParams.Next) then
  begin
    ANumber := Trunc(ANumber);
    AChosen := Trunc(AChosen);
    if (ANumber < 0) or (AChosen < 0) or ((ANumber = 0) and (ANumber < AChosen)) then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(dxGetCombinA(ANumber, AChosen));
  end;
end;

procedure fnDecimal(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

  function GetDecimal(const AText: string; const ARadix: Int64): Int64;
  var
    I, P: Integer;
  begin
    Result := 0;
    for I := Length(AText) downto 1 do
    begin
      P := Length(AText) - I;
      Result := Result + dxGetCharIndex(AText[I]) * Trunc(IntPower(ARadix, P));
    end;
  end;

  function CheckArguments(const AText: string; const ARadix: Int64): Boolean;
  var
    I, AIndex: Integer;
  begin
    Result := ARadix in [2..36];
    if Result then
      for I := 1 to Length(AText) do
      begin
        AIndex := dxGetCharIndex(AText[I]);
        if (AIndex = -1) or (AIndex > ARadix - 1) then
        begin
          Result := False;
          Break;
        end;
      end;
  end;

var
  AText, ARadix: Variant;
begin
  AText := dxSpreadSheetUpperCase(Sender.ExtractStringParameter(AParams));
  if Sender.ExtractNumericParameter(ARadix, AParams.Next) then
  begin
    ARadix := Trunc(ARadix);
    if CheckArguments(AText, ARadix) then
      Sender.AddValue(GetDecimal(AText, ARadix))
    else
      Sender.SetError(ecNUM);
  end;
end;

procedure fnDegrees(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(RadToDeg(AParameter));
end;

procedure fnExp(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Exp(AParameter));
end;

procedure fnEven(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
  ANumber: Int64;
begin
  if not Sender.ExtractNumericParameter(AParameter, AParams) then
    Exit;
  ANumber:= Abs(Trunc(AParameter));
  if Odd(ANumber) then
    Inc(ANumber)
  else
    if Frac(AParameter) <> 0 then
      Inc(ANumber, 2);
  Sender.AddValue(Sign(Double(AParameter)) * ANumber);
end;

procedure fnFact(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if not Sender.ExtractNumericParameter(AParameter, AParams) then
    Exit
  else
    AParameter := Trunc(AParameter);
  if AParameter >= 0 then
    Sender.AddValue(dxGetFactorial(AParameter))
  else
    Sender.SetError(ecNUM);
end;

procedure fnFactDouble(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter < -1 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(dxGetDoubleFactorial(Trunc(AParameter)));
end;

procedure fnFloor(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);  
var
  ANumber, ASignificance: Variant;
  AMultiplier: Int64;
  ARatio: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(ASignificance, AParams.Next) then
    if (ANumber > 0) and (ASignificance < 0) then
      Sender.SetError(ecNUM)
    else
      if ANumber = 0 then
        Sender.AddValue(0)
      else
        if ASignificance = 0 then
          Sender.SetError(ecDivByZero)
        else
        begin
          ARatio := Abs(ANumber / ASignificance);
          AMultiplier := Trunc(ARatio);
          if Frac(ARatio) > Resolution then
            Inc(AMultiplier, Integer((ANumber < 0) and (ASignificance > 0)));
          Sender.AddValue(Sign(Double(ANumber)) * Abs(ASignificance) * AMultiplier);
        end;
end;

procedure fnFloor_Math(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken); 
var
  ANumber, ASignificance, AMode: Variant;
  AMultiplier: Int64;
  ARatio: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameterDef(ASignificance, 1, 1, AParams, 1) and
     Sender.ExtractNumericParameterDef(AMode, 0, 0, AParams, 2) then
    if (ANumber = 0) or (ASignificance = 0) then
      Sender.AddValue(0)
    else
    begin
      ARatio := Abs(ANumber / ASignificance);
      AMultiplier := Trunc(ARatio);
      if ANumber < 0 then
        Inc(AMultiplier, Integer((Frac(ARatio) <> 0) and (AMode = 0)));
      Sender.AddValue(Sign(Double(ANumber)) * Abs(ASignificance) * AMultiplier);
    end;
end;

procedure fnFloor_Precise(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);  
var
  ANumber, ASignificance: Variant;
  AMultiplier: Int64;
  ARatio: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameterDef(ASignificance, 1, 1, AParams, 1) then
    if (ANumber = 0) or (ASignificance = 0) then
      Sender.AddValue(0)
    else
    begin
      ARatio := Abs(ANumber / ASignificance);
      AMultiplier := Trunc(ARatio);
      Inc(AMultiplier, Integer((ANumber < 0) and (Frac(ARatio) <> 0)));
      Sender.AddValue(Sign(Double(ANumber)) * Abs(ASignificance) * AMultiplier);
    end;
end;

procedure fnINT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Floor(AParameter));
end;

procedure fnIso_Ceiling(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);  
begin
  fnCeiling_Precise(Sender, AParams);
end;

procedure fnLN(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter <= 0 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(Ln(AParameter));
end;

procedure fnLOG(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);  
var
  ANumber, ABase: Variant;
begin
  if Sender.ExtractNumericParameterDef(ANumber, 0, AParams) and Sender.ExtractNumericParameterDef(ABase, 10, 0, AParams, 1) then
    if ABase = 1 then
      Sender.SetError(ecDivByZero)
    else
      if (ANumber <= 0) or (ABase <= 0) then
        Sender.SetError(ecNUM)
      else
        Sender.AddValue(LogN(ABase, ANumber));
end;

procedure fnLOG10(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter <= 0 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(Log10(AParameter));
end;

function CheckMMultDimensions(AMatrixToken1, AMatrixToken2: TdxSpreadSheetFormulaToken;
  var ADimension1, ADimension2: TdxSpreadSheetFormulaTokenDimension): TdxSpreadSheetFormulaErrorCode;
begin
  Result := ecNone;
  ADimension1 := AMatrixToken1.GetDimension(Result);
  if Result = ecNone then
    ADimension2 := AMatrixToken2.GetDimension(Result);
  if (Result = ecNone) and (ADimension1.ColumnCount <> ADimension2.RowCount) then
    Result := ecValue;
end;

function GetMMultMatrix(AMatrixToken: TdxSpreadSheetFormulaToken; const ADimension: TdxSpreadSheetFormulaTokenDimension;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode): Variant;
var
  ARow, AColumn, ARowCount, AColumnCount: Integer;
  AValue: Variant;
begin
  ARowCount := ADimension.RowCount;
  AColumnCount := ADimension.ColumnCount;
  Result := VarArrayCreate([0, ARowCount - 1, 0, AColumnCount - 1], varVariant);
  AErrorCode := ecNone;
  for ARow := 0 to ARowCount - 1 do
    for AColumn := 0 to AColumnCount - 1 do
    begin
      TTokenAccess(AMatrixToken).GetValueAsArrayItem(ARow, AColumn, AValue, AErrorCode);
      if (AErrorCode = ecNone) and not dxIsNumberOrDateTime(AValue) then
        AErrorCode := ecValue
      else
        Result[ARow, AColumn] := AValue;
      if AErrorCode <> ecNone then
        Break;
    end;
  if AErrorCode <> ecNone then
    Result := Unassigned;
end;

function ExtractSourcesMatrix(const AParams: TdxSpreadSheetFormulaToken; var AMatrix1, AMatrix2: Variant): TdxSpreadSheetFormulaErrorCode;
var
  AMatrixToken1, AMatrixToken2: TdxSpreadSheetFormulaToken;
  ADimension1, ADimension2: TdxSpreadSheetFormulaTokenDimension;
begin
  Result := ecNone;
  AMatrixToken2 := nil;
  AMatrixToken1 := AParams.FirstChild;
  if (AMatrixToken1 <> nil) and (AParams.Next <> nil) then
    AMatrixToken2 := AParams.Next.FirstChild;
  if (AMatrixToken1 = nil) or (AMatrixToken2 = nil) then
    Result := ecValue;

  if Result = ecNone then
    Result := CheckMMultDimensions(AMatrixToken1, AMatrixToken2, ADimension1, ADimension2);

  if Result = ecNone then
  begin
    AMatrix1 := GetMMultMatrix(AMatrixToken1, ADimension1, Result);
    if Result = ecNone then
      AMatrix2 := GetMMultMatrix(AMatrixToken2, ADimension2, Result);
  end;
  if Result <> ecNone then
  begin
    AMatrix1 := Unassigned;
    AMatrix2 := Unassigned;
  end;
end;

procedure fnMMult(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AOwner: TdxSpreadSheetFormula;
  AParentToken: TdxSpreadSheetFormulaToken;
  AMatrix1, AMatrix2: Variant;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AResult: TdxSpreadSheetFormulaArrayToken;
  ARow, AColumn, AIndex, AMatrix1RowCount, AMatrix1ColumnCount, AMatrix2ColumnCount: Integer;
  AValue: Variant;
begin
  AErrorCode := ExtractSourcesMatrix(AParams, AMatrix1, AMatrix2);
  if AErrorCode <> ecNone then
    Sender.SetError(AErrorCode)
  else
  begin
    AOwner := Sender.Owner;
    AResult := TdxSpreadSheetFormulaArrayToken.Create;
    AMatrix1RowCount := VarArrayHighBound(AMatrix1, 1) + 1;
    AMatrix1ColumnCount := VarArrayHighBound(AMatrix1, 2) + 1;
    AMatrix2ColumnCount := VarArrayHighBound(AMatrix2, 2) + 1;
    for ARow := 0 to AMatrix1RowCount - 1 do
      for AColumn := 0 to AMatrix2ColumnCount - 1 do
      begin
        if (AColumn = 0) and (ARow > 0) then
          TFormulaAccess(AOwner).AddChild(AResult, TdxSpreadSheetFormulaArrayRowSeparator.Create());

        AValue := 0;
        for AIndex := 0 to AMatrix1ColumnCount - 1 do
          AValue := AValue + AMatrix1[ARow, AIndex] * AMatrix2[AIndex, AColumn];

        AParentToken := TdxSpreadSheetFormulaToken.Create;
        TFormulaAccess(AOwner).AddChild(AParentToken, TdxSpreadSheetFormulaVariantToken.Create(AValue));
        TFormulaAccess(AOwner).AddChild(AResult, AParentToken);
      end;
    Sender.Add(AResult);
  end;
end;

procedure fnMOD(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken); 
var
  ANumber, ADivisor, AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(ADivisor, AParams.Next) then
    if ADivisor = 0 then
      Sender.SetError(ecDivByZero)
    else
    begin
      AParameter := ANumber - ADivisor * Floor(ANumber / ADivisor);
      if ADivisor * AParameter < 0 then
        AParameter := -AParameter;       
      Sender.AddValue(AParameter);
    end;
end;

procedure fnMRound(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, AMultiple: Variant;
  ARatio, AResult: Extended;
begin
  if dxSpreadSheetExtractedArgumentsAreNotNull(Sender, AParams, ANumber, AMultiple) then
    if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(AMultiple, AParams.Next) then
      if ANumber * AMultiple < 0 then
        Sender.SetError(ecNUM)
      else
        if (ANumber = 0) or (AMultiple = 0) then
          Sender.AddValue(0)
        else
        begin
          ARatio := ANumber / AMultiple;
          AResult := AMultiple * Trunc(ARatio);
          if Frac(ARatio) >= 0.5 then
            AResult := AMultiple * (Trunc(ARatio) + 1);
          Sender.AddValue(AResult);
        end;
end;

procedure fnOdd(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
  begin
    if (Abs(AParameter) - Abs(Trunc(AParameter))) <> 0 then
      AParameter := Trunc(AParameter + ValueIncr[AParameter >= 0]);
    if not Odd(Trunc(AParameter)) then
      AParameter := AParameter + ValueIncr[AParameter >= 0];
    Sender.AddValue(AParameter);
  end;
end;

procedure fnPI(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(Pi);
end;

procedure fnPower(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, APower: Variant;
begin
  if Sender.ExtractNumericParameterDef(ANumber, 0, AParams) and Sender.ExtractNumericParameterDef(APower, 0, 0, AParams.Next) then
  begin
    if (ANumber = 0) and (APower = 0) then
      Sender.SetError(ecNUM)
    else
      if (ANumber = 0) and (APower < 0) then
        Sender.SetError(ecDivByZero)
      else
        if (ANumber < 0) and (APower < 0) and (APower <> Trunc(APower)) then
          Sender.SetError(ecNUM)
        else
          Sender.AddValue(Power(ANumber, APower));
  end;
end;

procedure fnProduct(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  dxSpreadSheetFunctionsStatistical.fnProduct(Sender, AParams);
end;

procedure fnQuotient(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, ADivisor: Variant;
begin
  if dxSpreadSheetExtractedArgumentsAreNotNull(Sender, AParams, ANumber, ADivisor) then
    if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameterWithoutBoolean(ADivisor, AParams.Next) then
      if ADivisor = 0 then
        Sender.SetError(ecDivByZero)
      else
        Sender.AddValue(Trunc(ANumber / ADivisor));
end;

procedure fnRadians(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(DegToRad(AParameter));
end;

procedure fnRand(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(Random);
end;

procedure fnRandBetween(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ABottom, ATop: Variant;
  AResult: Variant;
begin
  if dxSpreadSheetExtractedArgumentsAreNotNull(Sender, AParams, ABottom, ATop) and
    Sender.ExtractNumericParameterWithoutBoolean(ABottom, AParams) and
    Sender.ExtractNumericParameterWithoutBoolean(ATop, AParams.Next) then
      if ABottom > ATop then
        Sender.SetError(ecNUM)
      else
      begin
        AResult := Round(ABottom + Random(101) / 100 * (ATop - ABottom));
        if AResult < ABottom then
          AResult := Trunc(ABottom) + 1;
        if ATop - ABottom < 1 then
        begin
          if AResult > ATop then
            AResult := Min(AResult, Trunc(ATop) + 1);
        end
        else
          AResult := Min(AResult, Trunc(ATop));
        Sender.AddValue(AResult);
      end;
end;

procedure fnRound(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, ADigits: Variant;
  AResult: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(ADigits, AParams.Next) then
  begin
    AResult := Extended(ANumber);
    if dxSpreadSheetRound(Sender, AResult, Trunc(ADigits)) then
      Sender.AddValue(AResult);
  end;
end;

procedure fnRoundDown(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, ADigits: Variant;
  AResult: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(ADigits, AParams.Next) then
  begin
    AResult := Extended(ANumber);
    if dxExecRoundDown(Sender, AResult, Trunc(ADigits)) then
      Sender.AddValue(AResult);
  end;
end;

procedure fnRoundUp(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, ADigits: Variant;
  AMultiplier: Int64;
  APrecision, AProduct: Extended;
begin
  if Sender.ExtractNumericParameter(ANumber, AParams) and Sender.ExtractNumericParameter(ADigits, AParams.Next) then
    if dxGetRoundPrecision(Sender, ADigits, APrecision) then
    begin
      ADigits := Trunc(ADigits);
      AProduct := Abs(ANumber * APrecision);
      AMultiplier := Trunc(AProduct);
      Inc(AMultiplier, Integer(Frac(AProduct) > Resolution));
      Sender.AddValue(Sign(Double(ANumber)) * AMultiplier / APrecision);
    end;
end;

procedure fnSec(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Secant(AParameter));
end;

procedure fnSech(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(SecH(AParameter));
end;

procedure fnSign(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Sign(Double(AParameter)));
end;

procedure fnSin(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Sin(AParameter));
end;

procedure fnSinH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(SinH(AParameter));
end;

procedure fnSQRT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    if AParameter < 0 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(SQRT(AParameter));
end;

procedure fnSQRTPI(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameterWithoutBoolean(AParameter, AParams) then
    if AParameter < 0 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(SQRT(AParameter * Pi));
end;

procedure fnSum(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  dxSpreadSheetFunctionsStatistical.fnSum(Sender, AParams);
end;

procedure fnSumIF(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  dxSpreadSheetFunctionsStatistical.fnSumIF(Sender, AParams);
end;

procedure fnSumSQ(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  dxSpreadSheetFunctionsStatistical.fnSumSQ(Sender, AParams);
end;

procedure fnTan(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(Tan(AParameter));
end;

procedure fnTanH(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractNumericParameter(AParameter, AParams) then
    Sender.AddValue(TanH(AParameter));
end;

procedure fnTrunc(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ANumber, ADigits: Variant;
  AResult: Extended;
begin
  if Sender.ExtractNumericParameterDef(ANumber, 0, AParams) and Sender.ExtractNumericParameterDef(ADigits, 0, 0, AParams, 1) then
  begin
    AResult := Extended(ANumber);
    if dxExecRoundDown(Sender, AResult, Trunc(ADigits)) then
      Sender.AddValue(AResult);
  end;
end;

//
{
var
  Criteria: string;

function IfCompare(const Value: string): Boolean;
begin
  if (Length(Criteria) > 0) and dxCharInSet(Criteria[1], ['>', '<']) then
  begin
    if Criteria[1] = '>' then
    begin
      if (Length(Criteria) > 1) and (Criteria[2] = '=') then
        Result := AnsiCompareText(Value, Copy(Criteria, 3, Length(Criteria) - 2)) >= 0
      else
        Result := AnsiCompareText(Value, Copy(Criteria, 2, Length(Criteria) - 1)) > 0;
    end
    else
    begin
      if (Length(Criteria) > 1) and (Criteria[2] = '=') then
        Result := AnsiCompareText(Value, Copy(Criteria, 3, Length(Criteria) - 2)) <= 0
      else
        Result := AnsiCompareText(Value, Copy(Criteria, 2, Length(Criteria) - 1)) < 0;
    end;
  end
  else
    Result := AnsiCompareText(Value, Criteria) = 0;
end;
}

end.
