{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressExport                                            }
{                                                                    }
{           Copyright (c) 2009-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSEXPORT AND ALL                 }
{   ACCOMPANYING VCL AND CLX CONTROLS AS PART OF AN EXECUTABLE       }
{   PROGRAM ONLY.                                                    }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxXLSXExport;

{$I cxVer.inc}

interface

uses
  Windows, cxExport, Types, SysUtils, Classes, Graphics, dxCore, cxClasses, dxCustomTree,
  RTLConsts, Variants, {$IFNDEF NONDB} FMTBcd, SqlTimSt, {$ENDIF} ZLib, dxZIPUtils,
  cxExportStrs, dxXMLDoc, dxGdiPlusClasses {$IFDEF DELPHI12}, AnsiStrings {$ENDIF};

type
  TdxXLSXIntenalDocument = class;

  { TdxSharedObjectsList }

  TdxSharedObjectClass = class of TdxSharedObject;
  TdxSharedObject = class
  public
    Hash: Cardinal;
    Index: Integer;
    Next: TdxSharedObject;
    Prev: TdxSharedObject;
    RefCount: Integer;
    function IsEqual(const AObject: TdxSharedObject): Boolean; virtual;
  end;

  { TdxSharedObjectsList }

  TdxSharedObjectsList = class
  private
    FCount: Integer;
    FFirst: TdxSharedObject;
    FLast: TdxSharedObject;
    FUniqueCount: Integer;
  protected
    function InsertItem(AItem, ANext: TdxSharedObject): TdxSharedObject;
  public
    destructor Destroy; override;
    function Add(ACandidate: TdxSharedObject): TdxSharedObject;
    procedure Clear;
    procedure Release(AItem: TdxSharedObject);

    property Count: Integer read FCount;
    property First: TdxSharedObject read FFirst;
    property Last: TdxSharedObject read FLast;
    property UniqueCount: Integer read FUniqueCount;
  end;

  { TdxSharedFont }

  TdxSharedFont = class(TdxSharedObject)
  public
    Style: PcxCacheCellStyle;
    constructor Create(const AStyle: PcxCacheCellStyle);
    function DataSize: Integer;
    function IsEqual(const AObject: TdxSharedObject): Boolean; override;
  end;

  { TdxSharedBrush }

  TdxSharedBrush = class(TdxSharedObject)
  public
    Style: TcxBrushStyle;
    constructor Create(const AStyle: PcxCacheCellStyle);
    function IsEqual(const AObject: TdxSharedObject): Boolean; override;
    
    property Color: Cardinal read Hash write Hash;
  end;

  { TdxSharedCellBorders }

  TdxSharedCellBorders = class(TdxSharedObject)
  public
    Style: PcxCacheCellStyle;
    constructor Create(const AStyle: PcxCacheCellStyle);
    function IsEmpty: Boolean;
    function IsEqual(const AObject: TdxSharedObject): Boolean; override;
  end;

  { TdxSharedStyle }

  TdxSharedStyle = class(TdxSharedObject)
  public
    Font: TdxSharedObject;
    Borders: TdxSharedObject;
    Brush: TdxSharedObject;
    Format: Integer;
    Style: PcxCacheCellStyle;
    constructor Create( AStyle: PcxCacheCellStyle; AFont, ABrush, ABorders: TdxSharedObject);
    function IsEqual(const AObject: TdxSharedObject): Boolean; override;
  end;

  { TdxSharedStringTable }

  TdxSharedAnsiString = class(TdxSharedObject)
  public
    Text: AnsiString;
    constructor Create(const AText: AnsiString);
    function IsEqual(const AObject: TdxSharedObject): Boolean; override;
  end;

  { TdxSharedUnicodeString }

  TdxSharedUnicodeString = class(TdxSharedObject)
  public
    Text: WideString;
    constructor Create(const AText: WideString);
    function IsEqual(const AObject: TdxSharedObject): Boolean; override;
  end;

  { TdxSharedStringTable }

  TdxSharedStringTable = class(TdxSharedObjectsList)
  public
    function AddAnsiString(const S: AnsiString): TdxSharedObject;
    function AddUnicodeString(const S: WideString): TdxSharedObject;
  end;

  { TdxXLSXInternalXMLNode }

  TdxXLSXInternalXMLNode = class(TdxXMLNode)
  protected
    procedure CheckTextEncoding; override;
  end;

  { TdxXLSXInternalXMLDocument }

  TdxXLSXInternalXMLDocument = class(TdxXMLDocument)
  public
    function GetNodeClass(ARelativeNode: TdxTreeCustomNode): TdxTreeCustomNodeClass; override;
  end;

  { TdxXLSXIntenalDocument }

  TdxXLSXIntenalDocument = class
  protected
    constructor CreateInstance(const ADocName, ARelation, ANamespace, ARootName: AnsiString);
  public
    Document: TdxXLSXInternalXMLDocument;
    DocumentName: AnsiString;
    Namespace: AnsiString;
    Root: TdxXMLNode;
    destructor Destroy; override;
    function AddNode(AParent: TdxXMLNode; const AName: AnsiString;
      const AAttributes, AAttributesValues: array of AnsiString): TdxXMLNode; overload;
    function AddNode(const AName: AnsiString;
      const AAttributes, AAttributesValues: array of AnsiString): TdxXMLNode; overload;
    procedure SaveToStream(AStream: TStream); virtual;
  end;

  { TdxXLSXRels }

  TdxXLSXRels = class(TdxXLSXIntenalDocument)
  private
    FCount: Integer;
  public
    constructor Create(ADocName: AnsiString);
    procedure Add(const AType, ATarget: AnsiString; AIdPrefix: AnsiString = 'rId');
  end;

  { TdxXLSXContentTypes }

  TdxXLSXContentTypes = class(TdxXLSXIntenalDocument)
  public
    constructor Create;
  end;

  { TdxXLSXWorkbook }

  TdxXLSXWorkbook = class(TdxXLSXIntenalDocument)
  private
    FCount: Integer;
    FSheets: TdxXMLNode;
  public
    constructor Create;
    procedure AddSheet(AName: Widestring);
  end;

  { TdxXLSXSharedStrings }

  TdxXLSXSharedStrings = class(TdxXLSXIntenalDocument)
  public
    constructor Create(const ACount, AUniqueCount: Integer);
    procedure Add(const AValue: Widestring);
  end;

  { TdxXLSXStyles }

  TdxAddSharedObjectProc = function(const AObject: TdxSharedObject): Boolean of object;

  TdxXLSXStyles = class(TdxXLSXIntenalDocument)
  private
    FBorders: TdxXMLNode;
    FFonts: TdxXMLNode;
    FFormats: TdxXMLNode;
    FNumFormats: TdxXMLNode;
    FNumFormatsCount: Integer;
    FPatterns: TdxXMLNode;
  protected
    function AddCellBorders(const AObject: TdxSharedObject): Boolean;
    function AddFont(const AObject: TdxSharedObject): Boolean;
    function AddNumFormat(const AFormat: AnsiString; const FormatID: Byte = 0): Integer;
    function AddPattern(const AObject: TdxSharedObject): Boolean;
    procedure AddSharedObjects(ANode: TdxXMLNode; AList: TdxSharedObjectsList;
      AddProc: TdxAddSharedObjectProc; ACount: Integer = 0);
    function AddStyle(const AObject: TdxSharedObject): Boolean;
    procedure Initialize;
  public
    constructor Create;
    procedure AddBorders(ABorders: TdxSharedObjectsList);
    procedure AddBrushes(ABrushes: TdxSharedObjectsList);
    procedure AddFonts(AFonts: TdxSharedObjectsList);
    procedure AddStyles(AStyles: TdxSharedObjectsList);
    procedure SaveToStream(AStream: TStream); override;
  end;

  { TdxXLSXDrawing }

  TdxXLSXDrawing = class(TdxXLSXIntenalDocument)
  private
    FIndex: Integer;
    FRels: TdxXLSXRels;
  public
    constructor Create(ARels: TdxXLSXRels);
    procedure AddImage(ACol, ARow: Integer; AImage: TdxPNGImage);
    property Index: Integer read FIndex;
  end;

  { TdxXLSXSheet }

  TdxXLSXSheet = class(TdxXLSXIntenalDocument)
  private
    FColCount: Integer;
    FColumns: TdxXMLNode;
    FData: TdxXMLNode;
    FRowCount: Integer;
  public
    constructor Create(const ARowCount, AColCount: Integer; AGridlines: Boolean);
    function AddCell(AParentRow: TdxXMLNode; const ARow, ACol, AStyleIndex: Integer): TdxXMLNode;
    procedure SetDataAsBoolean(ACell: TdxXMLNode; const AValue: Boolean);
    procedure SetDataAsCurrency(ACell: TdxXMLNode; const AValue: Currency);
    procedure SetDataAsFloat(ACell: TdxXMLNode; const AValue: Double);
    procedure SetDataAsInteger(ACell: TdxXMLNode; const AValue: Integer);
    procedure SetDataAsString(ACell: TdxXMLNode; const ASSTIndex: Integer);
    procedure AddMergeCells(const ARefs: TStringList);

    function AddRowHeight(const ARow, AHeight: Integer): TdxXMLNode;
    procedure SetColumnWidth(const AColumn: Integer; AWidth: Integer);
  end;

  { TdxXLSXCell }

  TdxXLSXCellDataType = (cdtNone, cdtBoolean, cdtInteger, cdtCurrency, cdtFloat, cdtDateTime, cdtString, cdtImage);

  TdxXLSXCell = class
  private
    FDataType: TdxXLSXCellDataType;
    function GetDataTypeSize(ADataType: TdxXLSXCellDataType): Integer;
    function NeedAllocate(ADataType: TdxXLSXCellDataType): Boolean; {$IFDEF DELPHI9} inline;{$ENDIF}
    procedure SetDataType(ADataType: TdxXLSXCellDataType); {$IFDEF DELPHI9} inline;{$ENDIF}
  public
    Col, Row: Integer;
    Style: Integer;
    Data: Pointer;
    destructor Destroy; override;
    procedure Clear;

    property DataType: TdxXLSXCellDataType read FDataType write SetDataType;
  end;

  { TdxXLSXPackedStreamWriter }

  TdxXLSXPackedStreamWriter = class(TdxZIPStreamWriter)
  public
    procedure AddImage(const APath: AnsiString; AImage: TdxPNGImage; AIndex: Integer);
    procedure AddIntenalDocument(ADocument: TdxXLSXIntenalDocument);
  end;

  { TdxXLSXExportProvider }

  TdxXLSXExportProvider = class(TcxCustomExportProvider, IcxExportProvider)
  private
    FCells: TcxObjectList;
    FColCount: Integer;
    FCols: TList;
    FContentTypes: TdxXLSXContentTypes;
    FDrawing: TdxXLSXDrawing;
    FImagesCount: Integer;
    FMergedCells: TStringList;
    FName: WideString;
    FRels: TdxXLSXRels;
    FRowCount: Integer;
    FRows: TList;
    FSharedStrings: TdxXLSXSharedStrings;
    FSheet: TdxXLSXSheet;
    FSST: TdxSharedStringTable;
    FStream: TStream;
    FStyleCache: TcxExportStyleManager;
    FStyles: TdxXLSXStyles;
    FWorkbook: TdxXLSXWorkbook;
    FWorkbookRels: TdxXLSXRels;
    FWriter: TdxXLSXPackedStreamWriter;
    FXMLItems: TcxObjectList; 
    function GetCell(ACol, ARow: Integer): TdxXLSXCell;
  protected
    procedure AddStructureItem(AItem: TdxXLSXIntenalDocument; var Instance);
    procedure Commit;
    function CheckPos(const ACol, ARow: Integer): Boolean;
    procedure CreateXMLStructure;
    function GetCellStyle(const ACol, ARow: Integer): PcxCacheCellStyle;
    function GetStyle(AStyleIndex: Integer): PcxCacheCellStyle;
    function InitCellData(const ACol, ARow: Integer; ADataType: TdxXLSXCellDataType): TdxXLSXCell;  
    procedure InitializeProvider;
    function RegisterStyle(const AStyle: TcxCacheCellStyle): Integer;
    procedure SetCellDataBoolean(const ACol, ARow: Integer; const AValue: Boolean);
    procedure SetCellDataCurrency(const ACol, ARow: Integer; const AValue: Currency);
    procedure SetCellDataDateTime(const ACol, ARow: Integer; const AValue: TDateTime);
    procedure SetCellDataDouble(const ACol, ARow: Integer; const AValue: Double);
    procedure SetCellDataInteger(const ACol, ARow: Integer; const AValue: Integer);
    procedure SetCellDataString(const ACol, ARow: Integer; const AText: string);
    procedure SetCellDataWideString(const ACol, ARow: Integer; const AText: Widestring);
    procedure SetCellStyle(const ACol, ARow, AStyleIndex: Integer); overload;
    procedure SetCellStyle(const ACol, ARow, AExampleCol, AExampleRow: Integer); overload;
    procedure SetCellStyle(const ACol, ARow: Integer; const AStyle: TcxCacheCellStyle); overload;
    procedure SetCellStyleEx(const ACol, ARow, H, W: Integer; const AStyleIndex: Integer);
    procedure SetCellUnion(const ACol, ARow: Integer; H, W: Integer);
    procedure SetCellValue(const ACol, ARow: Integer; const AValue: Variant);
    procedure SetColumnWidth(const ACol, AWidth: Integer);
    procedure SetDefaultStyle(const AStyle: TcxCacheCellStyle);
    procedure SetRange(const AColCount, ARowCount: Integer; IsVisible: Boolean = True);
    procedure SetRowHeight(const ARow, AHeight: Integer);
    procedure ValidateNativeStyle(var AStyleIndex: Integer; const ADataType: TdxXLSXCellDataType; const Data: Pointer);
    //
    procedure WriteData;
    procedure WriteSharedStrings;
    procedure WriteSheetLayout;
    procedure WriteStyles;
    // export graphic extension
    procedure SetCellDataGraphic(const ACol, ARow: Integer; var AGraphic: TGraphic);
    procedure SetCellPNGImage(const ACol, ARow: Integer; APNGImage: TObject);
    function SupportGraphic: Boolean;

    property Cells[ACol, ARow: Integer]: TdxXLSXCell read GetCell;
    property ColCount: Integer read FColCount;
    property Cols: TList read FCols;
    property ContentTypes: TdxXLSXContentTypes read FContentTypes;
    property Drawing: TdxXLSXDrawing read FDrawing;
    property ImagesCount: Integer read FImagesCount;
    property MergedCells: TStringList read FMergedCells;
    property Name: WideString read FName write FName;
    property Rels: TdxXLSXRels read FRels;
    property RowCount: Integer read FRowCount;
    property Rows: TList read FRows;
    property SharedStrings: TdxXLSXSharedStrings read FSharedStrings;
    property Sheet: TdxXLSXSheet read FSheet;
    property SST: TdxSharedStringTable read FSST;
    property StyleCache: TcxExportStyleManager read FStyleCache;
    property Styles: TdxXLSXStyles read FStyles;
    property Workbook: TdxXLSXWorkbook read FWorkbook;
    property WorkbookRels: TdxXLSXRels read FWorkbookRels;
    property Writer: TdxXLSXPackedStreamWriter read FWriter;
    property XMLItems: TcxObjectList read FXMLItems; 
  public
    constructor Create(const AFileName: string); override;
    destructor Destroy; override;
    class function ExportType: Integer; override;
    class function ExportName: string; override;
  end;

function dxColumnToString(ACol: Integer): AnsiString;
function dxRefToString(const ACol, ARow: Integer): AnsiString;
implementation

uses
  cxXLSExport, StrUtils, dxHashUtils;

const
  // xml strings
  sAlignment = 'alignment';
  sApplyAlign = 'applyAlignment';
  sApplyFont = 'applyFont';
  sApplyFill  = 'applyFill';
  sBGColor = 'bgColor';
  sBorder = 'border';
  sBorderID = 'borderId';
  sBorders = 'borders';
  sBottom = 'bottom';
  sCellStyleXfs = 'cellStyleXfs';
  sCellXfs = 'cellXfs';
  sCenter = 'center';
  sCharset = 'charset';
  sCol = 'col';
  sColor = 'color';
  sColumns = 'cols';
  sContentType = 'ContentType';
  sCount = 'count';
  sCustomHeight = 'customHeight';
  sCustomWidth = 'customWidth';
  sDefault = 'Default';
  sDiagonal = 'diagonal';
  sDimension = 'dimension';
  sExtension = 'Extension';
  sFGColor = 'fgColor';
  sFill = 'fill';
  sFillID = 'fillId';
  sFills = 'fills';
  sFont = 'font';
  sFontID = 'fontId';
  sFonts = 'fonts';
  sFormatCode = 'formatCode';
  sHorizontal = 'horizontal';
  sHt = 'ht';
  sID = 'Id';
  sIndexed = 'indexed';
  sLeft = 'left';
  sMax = 'max';
  sMedium = 'medium';
  sMergeCell = 'mergeCell';
  sMergeCells = 'mergeCells';
  sMin = 'min';
  sName = 'name';
  sNamespace = 'xmlns';
  sNone = 'none';
  sNumFmt = 'numFmt';
  sNumFmtID = 'numFmtId';
  sNumFmts = 'numFmts';
  sOverride = 'Override';
  sPartName = 'PartName';
  sPatternFill = 'patternFill';
  sPatternType = 'patternType';
  sR = 'r';
  sRef = 'ref';
  sRelationship = 'Relationship';
  sRelationships = 'Relationships';
  sRGB = 'rgb';
  sRID = 'r:id';
  sRight = 'right';
  sRow = 'row';
  sSheet = 'sheet';
  sSheetData = 'sheetData';
  sSheetID = 'sheetId';
  sSheets = 'sheets';
  sSheetView  = 'sheetView';
  sSheetViews = 'sheetViews';
  sShowGridLines = 'showGridLines';
  sSize = 'sz';
  sSolid = 'solid';
  sSpans = 'spans';
  sSST = 'sst';
  sStyle = 'style';
  sStyleSheet = 'styleSheet';
  sTarget = 'Target';
  sThick = 'thick';
  sThin = 'thin';
  sTop = 'top';
  sType = 'Type';
  sTypes = 'Types';
  sUniqueCount = 'uniqueCount';
  sVal = 'val';
  sVertical = 'vertical';
  sWidth = 'width';
  sWorkbook = 'workbook';
  sWorkbookViewID = 'workbookViewId';
  sWorkSheet = 'worksheet';
  sWrapText = 'wrapText';
  sXDR = 'xdr'; 
  sXF = 'xf';
  sXFID = 'xfId';

  cxExcelDefaultColumnWidth = 64;
  cxExcelDefaultRowHeight = 20;

  // other
  sOfficeDocument = 'application/vnd.openxmlformats-officedocument.';
  sPackage        = 'application/vnd.openxmlformats-package.';
  sBaseURL = 'http://schemas.openxmlformats.org/';

  ContentTypesTemplate: array[0..6, 0..4] of AnsiString =
  ((sDefault, sExtension, sContentType, 'png', 'image/png'),
   (sDefault, sExtension, sContentType, 'xml', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml'),
   (sDefault, sExtension, sContentType, 'rels', 'application/vnd.openxmlformats-package.relationships+xml'),
   (sOverride, sPartName, sContentType, '/xl/drawings/drawing0.xml', 'application/vnd.openxmlformats-officedocument.drawing+xml'),
   (sOverride, sPartName, sContentType, '/xl/worksheets/sheet0.xml', 'application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml'),
   (sOverride, sPartName, sContentType, '/xl/sharedStrings.xml', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml'),
   (sOverride, sPartName, sContentType, '/xl/styles.xml', 'application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml'));

function dxFloatToStr(const V: Extended): AnsiString;
var
  F: TFormatSettings;
begin
  FillChar(F, SizeOf(F), 0);
  F.DecimalSeparator := '.';
  Result := dxStringToAnsiString(FloatToStr(V, F));
end;

function dxIntToStr(const I: Integer): AnsiString;
begin
  Result := dxStringToAnsiString(IntToStr(I));
end;

function dxColorToText(AColor: Integer): AnsiString;
begin
  with TRGBQuad(ColorToRgb(AColor)) do
    AColor := Rgb(rgbRed, rgbGreen, rgbBlue);
  Result := dxStringToAnsiString(IntToHex(AColor or Integer($FF000000), 8));
end;

function dxColumnToString(ACol: Integer): AnsiString;
begin
  Result := '';
  if ACol >= 26 then
    Result := Result + dxColumnToString(ACol div 26 - 1);
  Result := Result + AnsiChar(Byte('A') + ACol mod 26);
end;

function dxRefToString(const ACol, ARow: Integer): AnsiString;
begin
  Result := dxColumnToString(ACol) + dxIntToStr(ARow + 1);
end;

function dxAreaRefToString(const ALeft, ATop, ARight, ABottom: Integer): AnsiString;
begin
  Result := dxRefToString(ALeft, ATop) +  ':' + dxRefToString(ARight, ABottom);
end;

{ TdxSharedObject }

function TdxSharedObject.IsEqual(const AObject: TdxSharedObject): Boolean;
begin
  Result := Hash = AObject.Hash; 
end;

{ TdxSharedObjectList }

destructor TdxSharedObjectsList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

function TdxSharedObjectsList.Add(ACandidate: TdxSharedObject): TdxSharedObject;
var
  AIsObjectUnique: Boolean;
begin
  Result := FLast;
  AIsObjectUnique := True;
{  if (FFirst <> nil) and (FLast.Hash >= ACandidate.Hash) then
  begin
    if FLast.Hash = ACandidate.Hash then
    begin
      Result := FLast;
      while AIsObjectUnique and (Result <> nil) and (Result.Hash = ACandidate.Hash) do
      begin
        AIsObjectUnique := not ((Result.Hash = ACandidate.Hash) and
          (Result.ClassType = ACandidate.ClassType) and Result.IsEqual(ACandidate));
        if AIsObjectUnique then
          Result := Result.Prev;
      end
    end
    else
    begin
      Result := FFirst;
      while AIsObjectUnique and (Result <> nil) and (Result.Hash <= ACandidate.Hash) do
      begin
        AIsObjectUnique := not ((Result.Hash = ACandidate.Hash) and
          (Result.ClassType = ACandidate.ClassType) and Result.IsEqual(ACandidate));
        if AIsObjectUnique then
          Result := Result.Next;
      end;
    end;
  end;}
  if AIsObjectUnique then
  begin
    Result := InsertItem(ACandidate, Result);
    Inc(FUniqueCount);
  end
  else
    ACandidate.Free;
  Inc(Result.RefCount);
  Inc(FCount);
end;

procedure TdxSharedObjectsList.Clear;
var
  AItem: TdxSharedObject;
begin
  while FFirst <> nil do
  begin
    AItem := FFirst;
    FFirst := FFirst.Next;
    AItem.Free;
  end;
  FLast := nil;
  FCount := 0;
  FUniqueCount := 0;
end;

procedure TdxSharedObjectsList.Release(AItem: TdxSharedObject);
begin
  Dec(FCount);
  Dec(AItem.RefCount);
  if AItem.RefCount = 0 then
  begin
    if AItem.Prev <> nil then
      AItem.Prev.Next := AItem.Next;
    if AItem.Next <> nil then
      AItem.Next.Prev := AItem.Prev;
    if AItem = FFirst then
      FFirst := AItem.Next;
    if AItem = FLast then
      FLast := AItem.Prev;
    Dec(FUniqueCount);
    AItem.Free;
  end;
end;

function TdxSharedObjectsList.InsertItem(AItem, ANext: TdxSharedObject): TdxSharedObject;
begin
  AItem.Next := ANext;
  if ANext <> nil then
  begin
    AItem.Prev := ANext.Prev;
    ANext.Prev := AItem;
  end
  else
  begin
    AItem.Prev := FLast;
    FLast := AItem;
  end;
  if AItem.Prev <> nil then
    AItem.Prev.Next := AItem
  else
    FFirst := AItem;
  Result := AItem;
end;

{ TdxSharedFont }

constructor TdxSharedFont.Create(const AStyle: PcxCacheCellStyle);
begin
  Style := AStyle;
  Hash := dxCRC32(@AStyle^.FontName, SizeOf(DataSize));
end;

function TdxSharedFont.DataSize: Integer;
begin
  Result := 64 + SizeOf(TcxFontStyles) + SizeOf(Integer) * 3;
end;

function TdxSharedFont.IsEqual(const AObject: TdxSharedObject): Boolean;
begin
  Result := CompareMem(@Style^.FontName,
    @TdxSharedFont(AObject).Style^.FontName, DataSize);
end;

{TdxSharedBrush}

constructor TdxSharedBrush.Create(
  const AStyle: PcxCacheCellStyle);
begin
  Color := AStyle^.BrushBkColor;
  Style := AStyle^.BrushStyle;
end;

function TdxSharedBrush.IsEqual(const AObject: TdxSharedObject): Boolean;
begin
  Result := Style = TdxSharedBrush(AObject).Style;
end;

{ TdxSharedCellBorders }

constructor TdxSharedCellBorders.Create(const AStyle: PcxCacheCellStyle);
var
  I: Integer;
begin
  Style := AStyle;
  for I := 0 to 3 do
    with Style^.Borders[I] do 
    begin
      if IsDefault then
        Width := 0;
      IsDefault := Width = 0;
    end;
  Hash := dxCRC32(@Style^.Borders, SizeOf(Style^.Borders));
end;

function TdxSharedCellBorders.IsEmpty: Boolean;
begin
  with Style^ do  
    Result := Borders[0].IsDefault and Borders[1].IsDefault and 
      Borders[2].IsDefault and Borders[3].IsDefault;
end;

function TdxSharedCellBorders.IsEqual(const AObject: TdxSharedObject): Boolean;
begin
  Result := CompareMem(@Style^.Borders,
    @TdxSharedCellBorders(AObject).Style^.Borders, SizeOf(Style^.Borders));
end;

constructor TdxSharedStyle.Create(AStyle: PcxCacheCellStyle;
  AFont, ABrush, ABorders: TdxSharedObject);
begin
  Font := AFont;
  Borders := ABorders;
  Brush := ABrush;
  Style := AStyle;
  Format := AStyle.Format;
  Hash := dxCRC32(@Font, SizeOf(Integer) * 3);
end;

function TdxSharedStyle.IsEqual(const AObject: TdxSharedObject): Boolean;
begin
  with TdxSharedStyle(AObject) do
  begin
    Result := (Self.Font = Font) and
              (Self.Borders = Borders) and
              (Self.Brush = Brush) and
              (Self.Style^.Format = Style^.Format) and
              (Self.Style^.SingleLine = Style^.SingleLine);
  end;
end;

{ TdxSharedAnsiString }

constructor TdxSharedAnsiString.Create(const AText: AnsiString);
begin
  Text := AText;
  if Length(Text) > 0 then
    Hash := dxCRC32(@Text[1], Length(Text));
end;

function TdxSharedAnsiString.IsEqual(const AObject: TdxSharedObject): Boolean;
begin
  Result := Text = TdxSharedAnsiString(AObject).Text;
end;

{ TdxSharedUnicodeString }

constructor TdxSharedUnicodeString.Create(const AText: WideString);
begin
  Text := AText;
  if Length(Text) > 0 then
    Hash := dxCRC32(@Text[1], Length(Text));
end;

function TdxSharedUnicodeString.IsEqual(const AObject: TdxSharedObject): Boolean;
begin
  Result := Text = TdxSharedUnicodeString(AObject).Text;
end;

{ TdxSharedStringTable }

function TdxSharedStringTable.AddAnsiString(
  const S: AnsiString): TdxSharedObject;
begin
  Result := Add(TdxSharedAnsiString.Create(S));
end;

function TdxSharedStringTable.AddUnicodeString(
  const S: WideString): TdxSharedObject;
begin
  Result := Add(TdxSharedUnicodeString.Create(S));
end;

{ TdxXLSXInternalXMLNode }

procedure TdxXLSXInternalXMLNode.CheckTextEncoding;
begin
  if Length(Text) > MAXSHORT then
  begin
    Text := Copy(Text, 1, MAXSHORT);
    Text := dxUnicodeStringToXMLString(dxXMLStringToUnicodeString(Text)); 
  end;
  inherited CheckTextEncoding;
end;

{ TdxXLSXInternalXMLDocument }

function TdxXLSXInternalXMLDocument.GetNodeClass(ARelativeNode: TdxTreeCustomNode): TdxTreeCustomNodeClass;
begin
  Result := TdxXLSXInternalXMLNode;
end;

{ TdxXLSXIntenalDocument }

constructor TdxXLSXIntenalDocument.CreateInstance(const ADocName, ARelation, ANamespace, ARootName: AnsiString);
begin
  Document := TdxXLSXInternalXMLDocument.Create(nil);
  DocumentName := ADocName;
  Document.Encoding := 'UTF-8';
  Document.Version := '1.0';
  Document.StandAlone := 'yes';
  if ANameSpace <> '' then
    Namespace := sBaseURL + ANamespace;
  Root := Document.AddChild(ARootName);
  if Length(ARelation) > 0 then
    Root.SetAttribute('xmlns:r', sBaseURL + ARelation);
  if Length(ANamespace) > 0 then
    Root.SetAttribute(sNamespace, NameSpace);
end;

destructor TdxXLSXIntenalDocument.Destroy;
begin
  Document.Free; 
  inherited Destroy;
end;

function TdxXLSXIntenalDocument.AddNode(AParent: TdxXMLNode; const AName: AnsiString;
  const AAttributes, AAttributesValues: array of AnsiString): TdxXMLNode;
var
  I: Integer;
begin
  Result := AParent.AddChild(AName);
  for I := Low(AAttributes) to High(AAttributes) do
    Result.SetAttribute(AAttributes[I], AAttributesValues[I]);
end;

function TdxXLSXIntenalDocument.AddNode(const AName: AnsiString;
  const AAttributes, AAttributesValues: array of AnsiString): TdxXMLNode;
begin
  Result := AddNode(Root, AName, AAttributes, AAttributesValues);
end;

procedure TdxXLSXIntenalDocument.SaveToStream(AStream: TStream);
begin
  Document.SaveToStream(AStream);
end;

{ TdxXLSXRels }

constructor TdxXLSXRels.Create(ADocName: AnsiString);
begin
  inherited CreateInstance(ADocName, '', 'package/2006/relationships', sRelationships);
end;

procedure TdxXLSXRels.Add(const AType, ATarget: AnsiString; AIdPrefix: AnsiString = 'rId');
begin
  if AIdPrefix = 'rId' then
  begin
    AIdPrefix := AIdPrefix + dxIntToStr(FCount);
    Inc(FCount);
  end;
  AddNode(sRelationship, [sID, sType, sTarget], [AIdPrefix, AType, ATarget]);
end;

{ TdxXLSXContentTypes }

constructor TdxXLSXContentTypes.Create;
var
  I: Integer;
begin
  inherited CreateInstance('[Content_Types].xml', '', 'package/2006/content-types', sTypes);
  for I := Low(ContentTypesTemplate) to High(ContentTypesTemplate) do
    AddNode(ContentTypesTemplate[I, 0], [ContentTypesTemplate[I, 1], ContentTypesTemplate[I, 2]],
      [ContentTypesTemplate[I, 3], ContentTypesTemplate[I, 4]]);
end;

{ TdxXLSXWorkbook }

constructor TdxXLSXWorkbook.Create;
begin
  inherited CreateInstance('xl\workbook.xml', 'officeDocument/2006/relationships', 'spreadsheetml/2006/main', sWorkbook);
  AddNode('workbookPr', ['date1904'], ['0']);
  FSheets := AddNode(sSheets, [], []);
end;

procedure TdxXLSXWorkbook.AddSheet(AName: Widestring);
var
  AnsiName: AnsiString;
  ANode: TdxXMLNode;
begin
  Inc(FCount);
  AnsiName := dxWideStringToXMLString(AName);
  if Length(AnsiName) > 31 then
    SetLength(AnsiName, 31);
  ANode := AddNode(FSheets, sSheet, [sRID, sSheetID], ['rId' + dxIntToStr(FCount - 1), dxIntToStr(FCount)]);
  ANode.Attributes.Add(sName, AnsiName);
end;

{ TdxXLSXSharedStrings }

constructor TdxXLSXSharedStrings.Create(const ACount, AUniqueCount: Integer);
begin
  inherited CreateInstance('xl\sharedStrings.xml', '', 'spreadsheetml/2006/main', sSST);
  Root.SetAttribute(sCount, dxIntToStr(ACount));
  Root.SetAttribute(sUniqueCount, dxIntToStr(AUniqueCount));
end;

procedure TdxXLSXSharedStrings.Add(const AValue: Widestring);
var
  ANode: TdxXMLNode;
begin
  ANode := Root.AddChild('si');
  ANode.AddChild('t').Text := dxWideStringToXMLString(AValue);
end;

{ TdxXLSXStyles }

constructor TdxXLSXStyles.Create;
begin
  inherited CreateInstance('xl\styles.xml', '', 'spreadsheetml/2006/main', sStyleSheet);
  FNumFormats := Root.AddChild(sNumFmts);
  FFonts := Root.AddChild(sFonts);
  FPatterns := Root.AddChild(sFills);
  FBorders := Root.AddChild(sBorders);
  AddNode(AddNode(Root, sCellStyleXfs, [sCount], ['1']), sXF,
    [sNumFmtID, sFontID, sFillID, sBorderID], ['0', '0', '0', '0']); 
  FFormats := Root.AddChild(sCellXfs);
  Initialize;
end;

procedure TdxXLSXStyles.AddSharedObjects(ANode: TdxXMLNode;
  AList: TdxSharedObjectsList; AddProc: TdxAddSharedObjectProc; ACount: Integer = 0);
var
  AObject: TdxSharedObject;
begin
  AObject := AList.FLast;
  if AObject = nil then Exit;
  repeat
    AObject.Index := ACount;
    if AddProc(AObject) then 
      Inc(ACount);
    AObject := AObject.Prev;
  until AObject = nil;
  ANode.SetAttribute(sCount, dxIntToStr(ACount));
end;

function TdxXLSXStyles.AddStyle(const AObject: TdxSharedObject): Boolean;
var
  AStyle: TdxSharedStyle;
  ANode: TdxXMLNode;
const
  AAlignToString: array[TcxAlignText] of AnsiString = (sLeft, sCenter, sRight);
begin
  Result := True;
  AStyle := AObject as TdxSharedStyle;
  ANode := AddNode(FFormats, sXF, [sNumFmtID, sFontID, sFillID, sBorderID, sXFID, sApplyAlign],
    [dxIntToStr(AStyle.Format), dxIntToStr(AStyle.Font.Index), dxIntToStr(AStyle.Brush.Index), dxIntToStr(AStyle.Borders.Index), '0', '1']);
  AddNode(ANode, sAlignment, [sHorizontal, sVertical, sWrapText], [AAlignToString[AStyle.Style^.AlignText], sTop, dxIntToStr(Byte(not AStyle.Style^.SingleLine))]);
end;

procedure TdxXLSXStyles.AddBorders(ABorders: TdxSharedObjectsList);
begin
  AddSharedObjects(FBorders, ABorders, AddCellBorders, 1);
end;

procedure TdxXLSXStyles.AddBrushes(ABrushes: TdxSharedObjectsList);
begin
  AddSharedObjects(FPatterns, ABrushes, AddPattern, 2);
end;

procedure TdxXLSXStyles.AddFonts(AFonts: TdxSharedObjectsList);
begin
  AddSharedObjects(FFonts, AFonts, AddFont, 1);
end;

procedure TdxXLSXStyles.AddStyles(AStyles: TdxSharedObjectsList);
begin
  AddSharedObjects(FFormats, AStyles, AddStyle, 1);
end;

procedure TdxXLSXStyles.SaveToStream(AStream: TStream);
begin
  FNumFormats.SetAttribute(sCount, dxIntToStr(FNumFormatsCount));
{  FFonts.SetAttribute(sCount, dxIntToStr(FFontCount));
  FPatterns.SetAttribute(sCount, dxIntToStr(FPatternCount));
  FBorders.SetAttribute(sCount, dxIntToStr(FBordersCount));
  FFormats.SetAttribute(sCount, dxIntToStr(FFormatCount));}
  inherited SaveToStream(AStream);
end;

function TdxXLSXStyles.AddCellBorders(const AObject: TdxSharedObject): Boolean;
var
  I: Integer;
  AValue: Widestring;
  ANode, ASubNode: TdxXMLNode;
  ABorder: ^TcxCellBorders;
  ACellBorders: TdxSharedCellBorders;
const
  AIndexes: array[0..3] of Integer = (0, 2, 1, 3);
  ABorders: array[0..3] of AnsiString = (sLeft, sRight, sTop, sBottom);
begin
  ACellBorders := AObject as TdxSharedCellBorders;
  Result := not ACellBorders.IsEmpty;
  if not Result then
  begin
    ACellBorders.Index := 0;
    Exit;
  end;
  ANode := FBorders.AddChild(sBorder);
  for I := 0 to 3 do
  begin
    ASubNode := ANode.AddChild(ABorders[I]);
    ABorder := @ACellBorders.Style^.Borders[AIndexes[I]];
    if not ABorder^.IsDefault and (ABorder^.Width > 0) then
    begin
      case ABorder^.Width of
        0:
          AValue := '';
        1:
          AValue := sThin;
        2:
          AValue := sMedium;
      else
        AValue := sThick;
      end;
      if AValue <> '' then
      begin
        ASubNode.SetAttribute(sStyle, AValue);
        AddNode(ASubNode, sColor, [sRGB], [dxColorToText(ABorder^.Color)]);
      end;
    end;
  end;
  ANode.AddChild(sDiagonal);
end;

function TdxXLSXStyles.AddFont(const AObject: TdxSharedObject): Boolean;
var
  AName: string;
  AFontNode: TdxXMLNode;
  AFont: TdxSharedFont;
  AFontStyle: TcxFontStyle;
const
  ExcelFontStyles: array[TcxFontStyle] of AnsiString = ('b', 'i', 'u', 'strike');
begin
  Result := True;
  AFont := AObject as TdxSharedFont;
  AName := PChar(@AFont.Style^.FontName);
  AFontNode := FFonts.AddChild(sFont);
  for AFontStyle := cfsBold to cfsStrikeOut do
    if AFontStyle in AFont.Style^.FontStyle then
      AFontNode.AddChild(ExcelFontStyles[AFontStyle]);
  AFontNode.AddChild(sSize).SetAttribute(sVal, dxIntToStr(AFont.Style^.FontSize));
  AFontNode.AddChild(sColor).SetAttribute(sRGB, dxColorToText(AFont.Style^.FontColor));
  AFontNode.AddChild(sName).SetAttribute(sVal, AName);
  if AFont.Style^.FontCharset <> 0 then
    AFontNode.AddChild(sCharset).SetAttribute(sVal, dxIntToStr(AFont.Style^.FontCharset));
end;

function TdxXLSXStyles.AddNumFormat(
  const AFormat: AnsiString; const FormatID: Byte = 0): Integer;
var
  I: Integer;
  S: AnsiString;
const
  Reserved: array[0..3] of AnsiString = (' ', '-', '(', ')');
begin
  Result := FormatID;
  if FormatID = 0 then
  begin
    Result := $A4 + FNumFormatsCount;
    S := AFormat;
    for I := 0 to High(Reserved) do
      S := StringReplace(S, Reserved[I], '\' + Reserved[I], [rfReplaceAll]);
  end;
  AddNode(FNumFormats, sNumFmt, [sNumFmtId, sFormatCode], [dxIntToStr(Result), AFormat]);
  Inc(FNumFormatsCount);
end;

procedure TdxXLSXStyles.Initialize;
var
  ANode: TdxXMLNode; 
begin
// todo: Q298046 Ignore currency type for national locale settings
//  AddNumFormat('"$"#,##0.00_);\("$"#,##0.00\)', $07);
  ANode := FFonts.AddChild(sFont);
  ANode.AddChild(sSize).SetAttribute(sVal, '11');
  ANode.AddChild(sName).SetAttribute(sVal, 'Calibri');
  FPatterns.AddChild(sFill).AddChild(sPatternFill).SetAttribute(sPatternType, sNone);
  FPatterns.AddChild(sFill).AddChild(sPatternFill).SetAttribute(sPatternType, 'gray125');
  ANode := FBorders.AddChild(sBorder);
  ANode.AddChild(sLeft);
  ANode.AddChild(sRight);
  ANode.AddChild(sTop);
  ANode.AddChild(sBottom);
  AddNode(FFormats, sXF, [sNumFmtID, sFontID, sFillID, sBorderID, sXFID], ['0', '0', '0', '0', '0']);
end;

function TdxXLSXStyles.AddPattern(const AObject: TdxSharedObject): Boolean;
var
  AStyle: TdxSharedBrush;
  APattern, AFill: TdxXMLNode;
begin
  Result := False;
  AStyle := AObject as TdxSharedBrush;
  if AStyle.Style <> cbsClear then
  begin
    APattern := FPatterns.AddChild(sFill);
    AFill := APattern.AddChild(sPatternFill);
    AFill.SetAttribute(sPatternType, sSolid);
    AFill.AddChild(sFGColor).SetAttribute(sRGB, dxColorToText(AStyle.Color));
    Result := True;
  end
  else
    AObject.Index := 0;
end;

{ TdxXLSXDrawing }

constructor TdxXLSXDrawing.Create(ARels: TdxXLSXRels);
begin
 inherited CreateInstance('xl\drawings\drawing0.xml', '', '', 'xdr:wsDr');
 FRels := ARels;
 Inc(FRels.FCount);
 Root.SetAttribute('xmlns:xdr', sBaseURL + 'drawingml/2006/spreadsheetDrawing');
 Root.SetAttribute('xmlns:a', sBaseURL + 'drawingml/2006/main');
end;

procedure TdxXLSXDrawing.AddImage(ACol, ARow: Integer; AImage: TdxPNGImage);
var
  AAnchor, APicNode, ANode: TdxXMLNode;
begin
  Inc(FIndex);
  FRels.Add(sBaseURL + 'officeDocument/2006/relationships/image', '/xl/media/image' + dxIntToStr(Index) + '.png');
  AAnchor := Root.AddChild('xdr:oneCellAnchor');
  ANode := AAnchor.AddChild('xdr:from');
  ANode.AddChild('xdr:col').Text := dxIntToStr(ACol);
  ANode.AddChild('xdr:colOff').Text := '0';
  ANode.AddChild('xdr:row').Text := dxIntToStr(ARow);
  ANode.AddChild('xdr:rowOff').Text := '0';
  ANode := AAnchor.AddChild('xdr:ext');
  ANode.SetAttribute('cx', AImage.Width * 9525);
  ANode.SetAttribute('cy', AImage.Height * 9525);
  APicNode := AAnchor.AddChild('xdr:pic');

  ANode := APicNode.AddChild('xdr:nvPicPr');
  with ANode.AddChild('xdr:cNvPr') do
  begin
    SetAttribute('id', FIndex);
    SetAttribute('name', Format('Picture %d', [FIndex]));
  end;
  ANode.AddChild('xdr:cNvPicPr');

  ANode := APicNode.AddChild('xdr:blipFill');
  with ANode.AddChild('a:blip') do
  begin
    SetAttribute('xmlns:r', sBaseURL + 'officeDocument/2006/relationships');
    SetAttribute('r:embed', Format('rId%d', [FIndex]));
  end;
  ANode.AddChild('a:srcRect');
  ANode.AddChild('a:stretch').AddChild('a:fillRect');

  ANode := APicNode.AddChild('xdr:spPr');
  ANode.AddChild('a:prstGeom').SetAttribute('prst', 'rect');
  AAnchor.AddChild('xdr:clientData');
end;

{ TdxXLSXSheet }

constructor TdxXLSXSheet.Create(
  const ARowCount, AColCount: Integer; AGridlines: Boolean);
begin
  inherited CreateInstance('xl\worksheets\sheet0.xml',
    'officeDocument/2006/relationships', 'spreadsheetml/2006/main', sWorkSheet);
  AddNode(sDimension, [sRef], ['A1:' + dxRefToString(AColCount - 1, ARowCount - 1)]);
  AddNode(AddNode(sSheetViews, [], []), sSheetView,
    [sShowGridLines, sWorkbookViewID], [dxIntToStr(Byte(AGridLines)), '0']);
  FColumns := Root.AddChild(sColumns);
  FData := Root.AddChild(sSheetData);
  FColCount := AColCount;
  FRowCount := ARowCount;  
end;

function TdxXLSXSheet.AddCell(
  AParentRow: TdxXMLNode; const ARow, ACol, AStyleIndex: Integer): TdxXMLNode;
begin
  Result := AddNode(AParentRow, 'c', ['r', 's'],
    [dxRefToString(ACol, ARow), dxIntToStr(AStyleIndex)]);
end;

procedure TdxXLSXSheet.SetDataAsBoolean(ACell: TdxXMLNode; const AValue: Boolean);
begin
  ACell.SetAttribute('t', 'b');
  SetDataAsInteger(ACell, Byte(AValue));
end;

procedure TdxXLSXSheet.SetDataAsCurrency(ACell: TdxXMLNode; const AValue: Currency);
begin
  ACell.AddChild('v').Text := dxFloatToStr(AValue);
end;

procedure TdxXLSXSheet.SetDataAsFloat(ACell: TdxXMLNode; const AValue: Double);
begin
  ACell.AddChild('v').Text := dxFloatToStr(AValue);
end;

procedure TdxXLSXSheet.SetDataAsInteger(ACell: TdxXMLNode; const AValue: Integer);
begin
  ACell.AddChild('v').Text := dxIntToStr(AValue);
end;

procedure TdxXLSXSheet.SetDataAsString(ACell: TdxXMLNode; const ASSTIndex: Integer);
begin
  ACell.SetAttribute('t', 's');
  ACell.AddChild('v').Text := dxIntToStr(ASSTIndex);
end;

procedure TdxXLSXSheet.AddMergeCells(const ARefs: TStringList);
var
  I: Integer;
  AMergeCells: TdxXMLNode;
begin
  if ARefs.Count = 0 then Exit; 
  AMergeCells := AddNode(Root, sMergeCells, [sCount], [dxIntToStr(ARefs.Count)]);
  for I := 0 to ARefs.Count - 1 do
    AddNode(AMergeCells, sMergeCell, [sRef], [dxStringToAnsiString(ARefs[I])]);
end;

function TdxXLSXSheet.AddRowHeight(const ARow, AHeight: Integer): TdxXMLNode;
begin
  Result := AddNode(FData, sRow, [sR, sSpans, sHt, sCustomHeight],
    [dxIntToStr(ARow), '1:' + dxIntToStr(FColCount), dxFloatToStr(AHeight * 75 / 100), '1']);
end;

procedure TdxXLSXSheet.SetColumnWidth(const AColumn: Integer; AWidth: Integer);
begin
  AddNode(FColumns, sCol, [sMin, sMax, sWidth, sCustomWidth],
   [dxIntToStr(AColumn), dxIntToStr(AColumn), dxFloatToStr(AWidth / 7.1), '1']);
end;

{ TdxXLSXCell }

destructor TdxXLSXCell.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TdxXLSXCell.Clear;
begin
  if NeedAllocate(DataType) then
    FreeMem(Data)
  else
    if DataType = cdtImage then
      TObject(Data).Free;
  FDataType := cdtNone;
end;

function TdxXLSXCell.GetDataTypeSize(ADataType: TdxXLSXCellDataType): Integer;
const
  DataTypeSizes: array[TdxXLSXCellDataType] of Byte = (0, SizeOf(Boolean), SizeOf(Integer),
    SizeOf(Currency), SizeOf(Double), SizeOf(TDateTime), SizeOf(Integer), SizeOf(Pointer));
begin
  Result := DataTypeSizes[ADataType];
end;

function TdxXLSXCell.NeedAllocate(ADataType: TdxXLSXCellDataType): Boolean;
begin
  Result := GetDataTypeSize(ADataType) > 4;
end;

procedure TdxXLSXCell.SetDataType(ADataType: TdxXLSXCellDataType);
begin
  if NeedAllocate(ADataType) then
    GetMem(Data, GetDataTypeSize(ADataType));
  FDataType := ADataType;
end;

{ TdxXLSXPackedStreamWriter }

procedure TdxXLSXPackedStreamWriter.AddImage(const APath: AnsiString; AImage: TdxPNGImage; AIndex: Integer);
var
  AStream: TMemoryStream;
begin
  AStream := TMemoryStream.Create();
  try
    AImage.SaveToStream(AStream);
    AStream.Position := 0;
    AddFile(APath + 'image' + dxIntToStr(AIndex) + '.png', '', AStream);
  finally
    AStream.Free;
  end;
end;

procedure TdxXLSXPackedStreamWriter.AddIntenalDocument(ADocument: TdxXLSXIntenalDocument);
var
  AStream: TStream;
begin
  AStream := TMemoryStream.Create;
  try
    ADocument.SaveToStream(AStream);
    AStream.Position := 0;
    AddFile(ADocument.DocumentName, '', AStream);
  finally
    AStream.Free;
  end;
end;

{ TdxXLSXExportProvider }

constructor TdxXLSXExportProvider.Create(const AFileName: string);
begin
  inherited Create(AFileName);
  FCells := TcxObjectList.Create;
  FName := cxExtractFileNameEx(ChangeFileExt(AFileName, ''));
  FStream := cxFileStreamClass.Create(cxUnicodeToStr(FileName), fmCreate);
  FSST := TdxSharedStringTable.Create;
  FCols := TList.Create;
  FRows := TList.Create;
  FStyleCache := TcxExportStyleManager.GetInstance(AFileName);
  FMergedCells := TStringList.Create;
  InitializeProvider;
end;

destructor TdxXLSXExportProvider.Destroy;
begin
  FCells.Free;
  FSST.Free;
  FStream.Free;
  FCols.Free;
  FRows.Free;
  if Assigned(FStyleCache) then
    FStyleCache.Clear;
  FMergedCells.Free;
  inherited Destroy;
end;

class function TdxXLSXExportProvider.ExportType: Integer;
begin
  Result := cxExportToXLSX;
end;

class function TdxXLSXExportProvider.ExportName: string;
begin
  Result := cxGetResString(@scxExportToXlsx);
end;

procedure TdxXLSXExportProvider.AddStructureItem(AItem: TdxXLSXIntenalDocument; var Instance);
begin
  TdxXLSXIntenalDocument(Instance) := AItem;
  FXMLItems.Add(AItem); 
end;

procedure TdxXLSXExportProvider.Commit;
var
  I: Integer;
begin
  FXMLItems := TcxObjectList.Create;
  try
    FWriter := TdxXLSXPackedStreamWriter.Create(FStream);
    try
      CreateXMLStructure;
      for I := 0 to FXMLItems.Count - 1 do
        FWriter.AddIntenalDocument(FXMLItems[I] as TdxXLSXIntenalDocument);
    finally
      FreeAndNil(FWriter);
    end;
    FreeAndNil(FStream);
  finally
    FXMLItems.Free;
  end;
end;

function TdxXLSXExportProvider.CheckPos(const ACol, ARow: Integer): Boolean;
begin
  Result := (ACol >= 0) and (ACol < ColCount) and
    (ARow >= 0) and (ARow < RowCount);
end;

procedure TdxXLSXExportProvider.CreateXMLStructure;
var
  ADrawingRels: TdxXLSXRels;
begin
  AddStructureItem(TdxXLSXRels.Create('_rels\.rels'), FRels);
  AddStructureItem(TdxXLSXContentTypes.Create(), FContentTypes);
  AddStructureItem(TdxXLSXWorkbook.Create, FWorkbook);
  AddStructureItem(TdxXLSXSharedStrings.Create(SST.Count, SST.UniqueCount), FSharedStrings);
  AddStructureItem(TdxXLSXStyles.Create, FStyles);
  AddStructureItem(TdxXLSXSheet.Create(RowCount, ColCount, False), FSheet);
  AddStructureItem(TdxXLSXRels.Create('xl\_rels\workbook.xml.rels'), FWorkbookRels);
  // initialize documents
  Rels.Add(sBaseURL + 'officeDocument/2006/relationships/officeDocument', 'xl/workbook.xml');
  Workbook.AddSheet(cxExtractFileNameEx(ChangeFileExt(FileName, '')));
  WorkbookRels.Add(sBaseURL + 'officeDocument/2006/relationships/styles', '/xl/styles.xml', 'rIdSR');
  WorkbookRels.Add(sBaseURL + 'officeDocument/2006/relationships/worksheet', '/xl/worksheets/sheet0.xml');
  WorkbookRels.Add(sBaseURL + 'officeDocument/2006/relationships/sharedStrings', '/xl/sharedStrings.xml', 'rIdSS');
  if ImagesCount > 0 then
  begin
    AddStructureItem(TdxXLSXRels.Create('xl\worksheets\_rels\sheet0.xml.rels'), ADrawingRels);
    ADrawingRels.Add(sBaseURL + 'officeDocument/2006/relationships/drawing', '/xl/drawings/drawing0.xml');
    AddStructureItem(TdxXLSXRels.Create('xl\drawings\_rels\drawing0.xml.rels'), ADrawingRels);
    AddStructureItem(TdxXLSXDrawing.Create(ADrawingRels), FDrawing);
  end;
  WriteStyles;
  WriteSharedStrings;
  WriteSheetLayout;
  WriteData;
end;

function TdxXLSXExportProvider.GetCellStyle(
  const ACol, ARow: Integer): PcxCacheCellStyle;
begin
  Result := StyleCache.GetStyle(Cells[ACol, ARow].Style);
end;

function TdxXLSXExportProvider.GetStyle(
  AStyleIndex: Integer): PcxCacheCellStyle;
begin
  if AStyleIndex >= FStyleCache.Count then
    Result := @DefaultCellStyle
  else
    Result := FStyleCache.GetStyle(AStyleIndex);
end;

function TdxXLSXExportProvider.InitCellData(
  const ACol, ARow: Integer; ADataType: TdxXLSXCellDataType): TdxXLSXCell;
begin
  Result := Cells[ACol, ARow];
  if Result.DataType = cdtString then
    FSST.Release(Result.Data);
  Result.Clear;
  Result.DataType := ADataType;
end;

procedure TdxXLSXExportProvider.InitializeProvider;
var
  I: Integer; 
  AStyle: TcxCacheCellStyle;
begin
  AStyle := DefaultCellStyle;
  AStyle.AlignText := catLeft;
  AStyle.BrushStyle := cbsClear;
  for I := 0 to 3 do
    AStyle.Borders[I].IsDefault := True;
  dxStringToBytes('Calibri', AStyle.FontName);
  AStyle.FontSize := 11;
  FStyleCache.RegisterStyle(AStyle);
  FStyleCache.RegisterStyle(DefaultCellStyle);
end;

function TdxXLSXExportProvider.RegisterStyle(
  const AStyle: TcxCacheCellStyle): Integer;
begin
  Result := StyleCache.RegisterStyle(AStyle);
end;

procedure TdxXLSXExportProvider.SetCellDataBoolean(
  const ACol, ARow: Integer; const AValue: Boolean);
begin
  InitCellData(ACol, ARow, cdtBoolean).Data := Pointer(AValue);
end;

procedure TdxXLSXExportProvider.SetCellDataCurrency(
  const ACol, ARow: Integer; const AValue: Currency);
begin
  with InitCellData(ACol, ARow, cdtCurrency) do
  begin
    PCurrency(Data)^ := AValue;
    ValidateNativeStyle(Style, cdtCurrency, Data);
  end;
end;

procedure TdxXLSXExportProvider.SetCellDataDateTime(
  const ACol, ARow: Integer; const AValue: TDateTime);
begin
  with InitCellData(ACol, ARow, cdtDateTime) do
  begin
    PDateTime(Data)^ := AValue;
    ValidateNativeStyle(Style, cdtDateTime, Data);
  end;
end;

procedure TdxXLSXExportProvider.SetCellDataDouble(
  const ACol, ARow: Integer; const AValue: Double);
begin
  PDouble(InitCellData(ACol, ARow, cdtFloat).Data)^ := AValue;
end;

procedure TdxXLSXExportProvider.SetCellDataInteger(
  const ACol, ARow: Integer; const AValue: Integer);
begin
  InitCellData(ACol, ARow, cdtInteger).Data := Pointer(AValue);
end;

procedure TdxXLSXExportProvider.SetCellDataString(
  const ACol, ARow: Integer; const AText: string);
begin
{$IFDEF DELPHI12}
  InitCellData(ACol, ARow, cdtString).Data := FSST.AddUnicodeString(AText);
{$ELSE}
  InitCellData(ACol, ARow, cdtString).Data := FSST.AddAnsiString(AText);
{$ENDIF}
end;

procedure TdxXLSXExportProvider.SetCellDataWideString(
  const ACol, ARow: Integer; const AText: Widestring);
begin
  InitCellData(ACol, ARow, cdtString).Data := FSST.AddUnicodeString(AText);
end;

procedure TdxXLSXExportProvider.SetCellStyle(
  const ACol, ARow, AStyleIndex: Integer);
begin
  if not CheckPos(ACol, ARow) then Exit;
  GetCell(ACol, ARow).Style := AStyleIndex;
end;

procedure TdxXLSXExportProvider.SetCellStyle(
  const ACol, ARow, AExampleCol, AExampleRow: Integer);
begin
  if not CheckPos(ACol, ARow) then Exit;
  SetCellStyle(ACol, ARow, GetCellStyle(AExampleCol, AExampleRow)^);
end;

procedure TdxXLSXExportProvider.SetCellStyle(
  const ACol, ARow: Integer; const AStyle: TcxCacheCellStyle);
begin
  if not CheckPos(ACol, ARow) then Exit;
  SetCellStyle(ACol, ARow, RegisterStyle(AStyle));
end;

procedure TdxXLSXExportProvider.SetCellStyleEx(
  const ACol, ARow, H, W: Integer; const AStyleIndex: Integer);
var
  I, J: Integer;
  AStyle: TcxCacheCellStyle;
begin
  for I := ACol to ACol + W - 1 do
    for J := ARow to ARow + H - 1 do
      with GetStyle(Cells[I, J].Style)^ do
      begin
        if Format = 0 then
          Cells[I, J].Style := AStyleIndex
        else
        begin
          AStyle := GetStyle(AStyleIndex)^;
          AStyle.SingleLine := SingleLine;
          AStyle.Format := Format;
          Cells[I, J].Style := RegisterStyle(AStyle);
        end;
      end;
  if (H > 1) or (W > 1) then
    SetCellUnion(ACol, ARow, H, W);
end;

procedure TdxXLSXExportProvider.SetCellUnion(
  const ACol, ARow: Integer; H, W: Integer);
begin
  MergedCells.Add(dxAnsiStringToString(
    dxAreaRefToString(ACol, ARow, ACol + W - 1, ARow + H - 1)));
end;

procedure TdxXLSXExportProvider.SetCellValue(
  const ACol, ARow: Integer; const AValue: Variant);
begin
  cxSetCellNativeValue(ACol, ARow, AValue, Self);
end;

procedure TdxXLSXExportProvider.SetColumnWidth(
  const ACol, AWidth: Integer);
begin
  Cols[ACol] := Pointer(AWidth);
end;

procedure TdxXLSXExportProvider.SetDefaultStyle(
  const AStyle: TcxCacheCellStyle);
begin
end;

procedure TdxXLSXExportProvider.SetRange(
  const AColCount, ARowCount: Integer; IsVisible: Boolean = True);
var
  I: Integer;
begin
  FColCount := AColCount;
  FRowCount := ARowCount;
  FCells.Count := AColCount * ARowCount;
  FCols.Count := AColCount;
  FRows.Count := ARowCount;
  for I := 0 to AColCount - 1 do
    FCols.List[I] := Pointer(cxExcelDefaultColumnWidth);
  for I := 0 to ARowCount - 1 do
    FRows.List[I] := Pointer(cxExcelDefaultRowHeight);
  for I := 0 to AColCount * ARowCount - 1 do
    FCells.List[I] := TdxXLSXCell.Create;
end;

procedure TdxXLSXExportProvider.SetRowHeight(const ARow, AHeight: Integer);
begin
  Rows[ARow] := Pointer(AHeight);
end;

procedure TdxXLSXExportProvider.ValidateNativeStyle(var AStyleIndex: Integer;
  const ADataType: TdxXLSXCellDataType; const Data: Pointer);
var
  AStyle: TcxCacheCellStyle;
begin
  AStyle := GetStyle(AStyleIndex)^;
  if (AStyle.Format <> 0) or not (ADataType in [cdtCurrency, cdtDateTime]) then Exit;
  if ADataType = cdtCurrency then
    AStyle.Format := NativeFormats[3]
  else
    AStyle.Format := NativeFormats[cxXlsGetDateTimeFormat(PDouble(Data)^) xor $1000];
  AStyleIndex := RegisterStyle(AStyle);
end;

procedure TdxXLSXExportProvider.WriteStyles;
var
  I: Integer;
  AStyle: PcxCacheCellStyle;
  ASharedStyle: TdxSharedStyle;
  AStyles, AFonts, ABrushes, ABorders: TdxSharedObjectsList;
begin
  AStyles := TdxSharedObjectsList.Create;
  AFonts := TdxSharedObjectsList.Create;
  ABrushes := TdxSharedObjectsList.Create;
  ABorders := TdxSharedObjectsList.Create;
  try
    for I := 0 to StyleCache.Count - 1 do
    begin
      AStyle := StyleCache.GetStyle(I);
      ASharedStyle := TdxSharedStyle.Create(AStyle, AFonts.Add(TdxSharedFont.Create(AStyle)),
        ABrushes.Add(TdxSharedBrush.Create(AStyle)), ABorders.Add(TdxSharedCellBorders.Create(AStyle)));
      AStyle.Format := TdxNativeInt(AStyles.Add(ASharedStyle));
    end;
    Styles.AddFonts(AFonts);
    Styles.AddBrushes(ABrushes);
    Styles.AddBorders(ABorders);
    Styles.AddStyles(AStyles);
    for I := 0 to StyleCache.Count - 1 do
      with StyleCache.GetStyle(I)^ do
        Format := TdxSharedObject(Format).Index;
  finally
    ABorders.Free;
    ABrushes.Free;
    AFonts.Free;
    AStyles.Free;
  end;
end;

procedure TdxXLSXExportProvider.WriteData;
var
  AXMLCell: TdxXMLNode; 
  ACell: TdxXLSXCell;
  ARowNode: TdxXMLNode;
  ACol, ARow: Integer;
begin
  for ARow := 0 to RowCount - 1 do
  begin
    ARowNode := Sheet.AddRowHeight(ARow + 1, Integer(Rows[ARow]));
    for ACol := 0 to ColCount - 1 do
    begin
      ACell := Cells[ACol, ARow];
      AXMLCell := Sheet.AddCell(ARowNode, ARow, ACol, StyleCache.GetStyle(ACell.Style).Format);
      case ACell.DataType of
        cdtBoolean:
          Sheet.SetDataAsBoolean(AXMLCell, Boolean(ACell.Data));
        cdtInteger:
          Sheet.SetDataAsInteger(AXMLCell, Integer(ACell.Data));
        cdtCurrency:
          Sheet.SetDataAsCurrency(AXMLCell, PCurrency(ACell.Data)^);
        cdtFloat:
          Sheet.SetDataAsFloat(AXMLCell, PDouble(ACell.Data)^);
        cdtDateTime:
          Sheet.SetDataAsFloat(AXMLCell, PDateTime(ACell.Data)^);
        cdtString:
          Sheet.SetDataAsString(AXMLCell, TdxSharedObject(ACell.Data).Index);
        cdtImage:
        begin
          Drawing.AddImage(ACol, ARow, TdxPNGImage(ACell.Data));
          Writer.AddImage('xl\media\', TdxPNGImage(ACell.Data), Drawing.Index);
        end;
      end;
    end;
  end;
end;

procedure TdxXLSXExportProvider.WriteSharedStrings;
var
  AIndex: Integer;
  AText: TdxSharedObject;
begin
  AIndex := 0;
  AText := SST.First;
  while AText <> nil do
  begin
    AText.Index := AIndex;
    if AText is TdxSharedUnicodeString then
      SharedStrings.Add(TdxSharedUnicodeString(AText).Text)
    else
      SharedStrings.Add(dxAnsiStringToWideString(TdxSharedAnsiString(AText).Text));
    AText := AText.Next;
    Inc(AIndex);
  end;
end;

procedure TdxXLSXExportProvider.WriteSheetLayout;
var
  I: Integer;
begin
  for I := 0 to ColCount - 1 do
    Sheet.SetColumnWidth(I + 1, Integer(Cols[I]));
  Sheet.AddMergeCells(MergedCells);
  if ImagesCount > 0 then
    Sheet.AddNode('drawing', ['r:id'], ['rId0']);
end;

// export graphic extension
procedure TdxXLSXExportProvider.SetCellDataGraphic(
  const ACol, ARow: Integer; var AGraphic: TGraphic);
begin
//  SetCellPNGImage();
end;

procedure TdxXLSXExportProvider.SetCellPNGImage(const ACol, ARow: Integer;
  APNGImage: TObject);
begin
  InitCellData(ACol, ARow, cdtImage).Data := APNGImage;
  Inc(FImagesCount);
end;

function TdxXLSXExportProvider.SupportGraphic: Boolean;
begin
  Result := True;  
end;

function TdxXLSXExportProvider.GetCell(ACol, ARow: Integer): TdxXLSXCell;
begin
  Result := TdxXLSXCell(FCells[FColCount * ARow + ACol]);
end;

initialization
  TcxExport.RegisterProviderClass(TdxXLSXExportProvider);
end.
