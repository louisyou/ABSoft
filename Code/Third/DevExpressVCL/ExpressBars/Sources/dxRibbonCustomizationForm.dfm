object dxRibbonCustomizationForm: TdxRibbonCustomizationForm
  Left = 246
  Top = 141
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Ribbon Customization'
  ClientHeight = 582
  ClientWidth = 739
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 618
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lcMain: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 739
    Height = 582
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = dxLayoutCxLookAndFeel
    CustomizeFormTabbedView = True
    object cbCommands: TcxComboBox
      Left = 10
      Top = 28
      Properties.DropDownListStyle = lsFixedList
      Properties.OnChange = cbCommandsPropertiesChange
      Style.HotTrack = False
      TabOrder = 0
      Width = 326
    end
    object btnMoveUp: TcxButton
      Left = 704
      Top = 241
      Width = 25
      Height = 25
      Action = acMoveUp
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        00000000000000000000000000020000000A0000000F00000010000000100000
        000F0000000A0000000200000000000000000000000000000000000000000000
        00000000000000000000000000097E5237C1A8663FFFA7633DFFA6623BFFA560
        3AFF754429C10000000900000000000000000000000000000000000000000000
        000000000000000000000000000CB57852FFE6C69AFFE2BE8EFFE2BE8EFFE2BE
        8DFFA6613CFF0000000D00000000000000000000000000000000000000000000
        000000000000000000000000000CB67B55FFE6C79DFFDAAD73FFD9AD73FFE3C0
        8FFFA8653EFF0000000D00000000000000000000000000000000000000000000
        0002000000080000000B00000013B87F59FFE7C79DFFDBAF77FFDBAE77FFE4C1
        91FFA96842FF000000150000000C0000000A0000000300000000000000000000
        0007B77D56FFB67B54FFB47953FFB37751FFE5C495FFDCB279FFDCB17AFFE4C2
        95FFAB6B46FFAA6A44FFAA6842FFA86640FF0000000800000000000000000000
        00057E573DB5E7D3C4FFEACEA8FFE6C69AFFE5C598FFDDB37DFFDDB37DFFE5C4
        97FFE5C597FFE7C79CFFDDC5B7FF673923B60000000600000000000000000000
        0002140E0A24B78869F2F2E5D2FFE6C799FFE7C997FFEACF9FFFEACF9EFFE7C9
        97FFE3C18FFFEFDFCBFF9D6549F3100906260000000200000000000000000000
        0000000000034E372871D7B7A0FFF5E9CDFFEEDAABFFEDD7A8FFEDD7A8FFEED7
        A8FFF4E5C4FFC7A08CFF41261872000000040000000000000000000000000000
        0000000000010000000590684BC7EEE0D3FFF3E5BFFFF1DFB2FFF1DFB2FFF2E2
        B7FFE8D8CBFF7B4C30C700000006000000010000000000000000000000000000
        000000000000000000011D15102DC4997AFAFBF4E4FFF5E8C0FFF4E6BBFFF9F2
        E0FFB07D5FFA19100A2F00000001000000000000000000000000000000000000
        00000000000000000000000000025F453383DFC5B1FFFBF2D7FFFAF2D4FFD3B6
        A2FF533523830000000300000000000000000000000000000000000000000000
        000000000000000000000000000005030309A37C5CD8F4EBDFFFF1E6DAFF9362
        45D80403020A0000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000012E231A3FCFAB8EFDC09679FD291C
        1340000000010000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000174584298674731950000
        0001000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000001000000010000
        0000000000000000000000000000000000000000000000000000}
      PaintStyle = bpsGlyph
      TabOrder = 11
    end
    object btnMoveDown: TcxButton
      Left = 704
      Top = 272
      Width = 25
      Height = 25
      Action = acMoveDown
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000003000000090000000A0000
        0003000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000010000000D552C19A64E2916A40000
        000D000000010000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000623130B54A77053FD9E6443FD2011
        0A54000000060000000000000000000000000000000000000000000000000000
        000000000000000000000000000204020117814A2EDEDFBF9BFFD4AC7EFF7942
        26DF030201180000000200000000000000000000000000000000000000000000
        00000000000000000001000000094D2D1C90C69D7FFFDFB987FFDFB785FFBA87
        5FFF472617910000000A00000001000000000000000000000000000000000000
        00000000000000000004180F0A3AA87252FBE5C89FFFDCB27AFFDCB17BFFE0BC
        8EFF9C6140FB160C073B00000004000000000000000000000000000000000000
        0000000000010000000C7E4E32CCDDBFA0FFDFB783FFDDB37DFFDDB37DFFDFB6
        82FFD2AB80FF714027CD0000000D000000020000000000000000000000000000
        000000000006452C1E78C69D7FFFE4C394FFDEB680FFDFB680FFDEB67FFFDEB5
        7FFFE3BF8FFFB8855FFF3E241679000000070000000000000000000000000000
        0002120C0828AA7758F3E9CEAAFFE4C290FFE5C794FFE8CA98FFE8CA99FFE6C5
        93FFE3C18DFFE2C193FF996141F31009062A0000000300000000000000000000
        0006795339B6E0C7ADFFF9F1E0FFFAF2E2FFFAF2E2FFEBD3A3FFECD2A3FFF1DD
        B7FFF0DDB7FFF9F1E0FFDBBEA6FF6B4028B80000000700000000000000000000
        0006C1906AFFC18E68FFC08C66FFBE8A64FFFBF5E5FFEFDAACFFEFD9ACFFF3E2
        BEFFB9805AFFB77E58FFB77C55FFB57A54FF0000000800000000000000000000
        000200000006000000070000000DC08D68FFFBF7E9FFF2E1B4FFF2E1B4FFF5E9
        C5FFB27753FF0000001000000009000000070000000200000000000000000000
        0000000000000000000000000007C3926CFFFCF8EBFFF5E8BCFFF5E7BCFFF8EE
        CCFFB47C56FF0000000800000000000000000000000000000000000000000000
        0000000000000000000000000005C59570FFFDFAEDFFFDFAEDFFFDFAEDFFFDFA
        EDFFB67F5AFF0000000600000000000000000000000000000000000000000000
        000000000000000000000000000393755CBACDA37EFFCBA17DFFCAA07BFFC99D
        79FF8C6A50BB0000000400000000000000000000000000000000000000000000
        0000000000000000000000000001000000020000000400000004000000040000
        0004000000030000000100000000000000000000000000000000}
      PaintStyle = bpsGlyph
      TabOrder = 12
    end
    object btnRemove: TcxButton
      Left = 342
      Top = 272
      Width = 25
      Height = 25
      Action = acRemove
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0002000000080000001200000012000000050000000000000000000000000000
        0000000000000000000000000000000000000000000000000002000000060000
        001332180C6E813C1CE0AC5A2DFF000000130000000000000000000000000000
        000000000000000000000000000000000001000000050000000F231209527E40
        20D4B67042FFDAA66DFFAE5E30FF000000170000000000000000000000000000
        00000000000000000001000000030000000C180E083C733E24BFB66F42FFD6A4
        6DFFE4B77BFFE4B77BFFB06034FF000000160000000000000000000000000000
        000100000002000000090F090529673E25A8B57548FDD7A674FFE7BF89FFE6BB
        80FFE5B87CFFE6B97FFFB26537FF000000150000000000000000000000010000
        00070906041C5B3B2590B4774EF7D8A87CFFECCA9AFFEAC38EFFE9C089FFE8BD
        84FFE7BA81FFE7BE86FFB4693AFF000000140000000000000000000000065139
        2978B17B54ECD7AA82FFEFD1A9FFEFCFA0FFEDC996FFECC793FFEBC490FFE9C1
        8AFFE7BE86FFE9C38FFFB56B3FFF00000013000000000000000000000009CB97
        6FFFF1DCC1FFF3DCB8FFF3D8AEFFF0D2A4FFF0CFA0FFEFD0A1FFEFCF9FFFEDCC
        9BFFEDC996FFEECDA0FFB87043FF000000120000000000000000000000044433
        2661AF815BE1DDB693FFF5E0C3FFF8E3C3FFF6DFB9FFF4DAAFFFF2D5AAFFF0D2
        A4FFEECE9DFFF1D5ABFFBA7648FF000000110000000000000000000000010000
        000405040311533D2B78BF8D67F1E3C2A2FFF8EACEFFF8E6C5FFF5DDB6FFF2D6
        ABFFF0D2A4FFF3DBB5FFBD784CFF000000100000000000000000000000000000
        000000000001000000050C09061B6B4E3894C79772F8E6C9AAFFFAEBCEFFF6E2
        BEFFF2D7ACFFF5E0BCFFBF7C52FF0000000F0000000000000000000000000000
        00000000000000000000000000010000000618120C2B7C583FA7D2A17BFFEBD2
        B4FFF8E6C8FFF8E7C9FFC18456FF0000000E0000000000000000000000000000
        0000000000000000000000000000000000000000000200000007261C133E9167
        4AC0D5A881FFEFDABDFFC3875BFF0000000E0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000020000
        000936281C54A37453D5C68C63FF0000000A0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0001000000030000000800000008000000020000000000000000}
      PaintStyle = bpsGlyph
      TabOrder = 3
    end
    object btnAdd: TcxButton
      Left = 342
      Top = 241
      Width = 25
      Height = 25
      Action = acAdd
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00040000000F0000000F00000007000000010000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000EB0683FFF834324E131190D6A000000110000000600000001000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0011B37045FFDEAF77FFB47247FF773C20D2201009500000000F000000050000
        0001000000000000000000000000000000000000000000000000000000000000
        0010B6764BFFECC38FFFE8BA7DFFD8A66FFFAD693FFF6A371CBE170C06400000
        000D000000040000000100000000000000000000000000000000000000000000
        000FBB7C51FFEFCD9CFFE9BB7FFFE8BC80FFEABE83FFD39D6CFFA7623BFD5B30
        1AA90D07032C0000000B00000003000000010000000000000000000000000000
        000EBE8257FFF1D5ACFFEBC087FFE9BF85FFE8BD81FFE9BF82FFE9C087FFCD97
        65FF9F5933F74E29169408040221000000090000000200000000000000000000
        000DC0855EFFF5DEBBFFEDC68FFFEDC58DFFEBC289FFEAC084FFE8BC80FFE9BD
        82FFE8BB87FFC78E60FF965430EE4223137E0000000900000000000000000000
        000CC59067FFF9E9CFFFF3D4A3FFF2D09FFFF0CC98FFEEC890FFEBC088FFE9BE
        83FFEAC38AFFECC693FFE3B889FFA65B33FF0000000D00000000000000000000
        000BC9956EFFFAEFDAFFF4DAAEFFF5D8AAFFF2D5A6FFF1D1A1FFF1D0A2FFF1D2
        A8FFEDCDA1FFCC956BFF9A5D39E33A2314680000000700000000000000000000
        000ACC9B73FFFCF4E3FFF8E0B7FFF6DDB4FFF6DEB4FFF7E2C0FFF6E1C1FFD9B2
        8CFFB0754EF14D301F7E04030216000000060000000100000000000000000000
        0009CDA077FFFEF7E9FFFBE7C2FFFBEBCCFFFBEFD6FFE3C5A7FFC08B65F96544
        2E960B07051E0000000600000002000000000000000000000000000000000000
        0008D0A47CFFFEFBEFFFFDF4E0FFEEDAC1FFD1A07DFF78553CA717100B2C0000
        0007000000020000000000000000000000000000000000000000000000000000
        0007D2A77FFFF5EADBFFD8B291FF916D50BE261C143C00000007000000020000
        0000000000000000000000000000000000000000000000000000000000000000
        0005D5AA83FFA98462D3372B204F000000070000000200000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0001000000040000000500000002000000010000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      PaintStyle = bpsGlyph
      TabOrder = 2
    end
    object btnNewElement: TcxButton
      Left = 373
      Top = 504
      Width = 75
      Height = 25
      Caption = '&Add'
      DropDownMenu = bpmNewElement
      Kind = cxbkOfficeDropDown
      OptionsImage.Layout = blGlyphTop
      TabOrder = 8
    end
    object btnRename: TcxButton
      Left = 454
      Top = 504
      Width = 75
      Height = 25
      Action = acRename
      TabOrder = 9
    end
    object btnImportExport: TcxButton
      Left = 603
      Top = 504
      Width = 95
      Height = 25
      Caption = 'Im&port/Export'
      DropDownMenu = bpmImportExport
      Kind = cxbkOfficeDropDown
      OptionsImage.Layout = blGlyphTop
      TabOrder = 10
    end
    object btnReset: TcxButton
      Left = 10
      Top = 547
      Width = 75
      Height = 25
      Caption = 'R&eset'
      DropDownMenu = bpmReset
      Kind = cxbkOfficeDropDown
      OptionsImage.Layout = blGlyphTop
      TabOrder = 13
    end
    object btnOK: TcxButton
      Left = 573
      Top = 547
      Width = 75
      Height = 25
      Caption = '&OK'
      ModalResult = 1
      TabOrder = 14
    end
    object btnCancel: TcxButton
      Left = 654
      Top = 547
      Width = 75
      Height = 25
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 15
    end
    object tlCommands: TcxTreeList
      Left = 10
      Top = 55
      Width = 326
      Height = 474
      Bands = <
        item
        end>
      DragMode = dmAutomatic
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.IncSearchItem = tlCommandsMainColumn
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsSelection.HideFocusRect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Headers = False
      OptionsView.ShowRoot = False
      OptionsView.TreeLineStyle = tllsNone
      TabOrder = 1
      OnBeginDragNode = tlCommandsBeginDragNode
      OnClick = acUpdateActionsStateExecute
      OnCollapsing = tlCommandsCollapsing
      OnCustomDrawDataCell = tlCommandsCustomDrawDataCell
      OnDeletion = tlCommandsDeletion
      OnDragOver = tlCommandsDragOver
      OnEndDrag = tlCommandsEndDrag
      OnFocusedNodeChanged = tlCommandsFocusedNodeChanged
      object tlCommandsMainColumn: TcxTreeListColumn
        DataBinding.ValueType = 'String'
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object tlRibbon: TcxTreeList
      Left = 373
      Top = 101
      Width = 325
      Height = 397
      Bands = <
        item
        end>
      DragMode = dmAutomatic
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.DragFocusing = True
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsSelection.HideFocusRect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.CheckGroups = True
      OptionsView.Headers = False
      OptionsView.ShowRoot = False
      OptionsView.TreeLineStyle = tllsNone
      PopupMenu = bpmRibbon
      Styles.OnGetContentStyle = tlRibbonStylesGetContentStyle
      TabOrder = 7
      OnBeginDragNode = tlCommandsBeginDragNode
      OnClick = acUpdateActionsStateExecute
      OnCollapsing = tlCommandsCollapsing
      OnCustomDrawDataCell = tlCommandsCustomDrawDataCell
      OnDeletion = tlCommandsDeletion
      OnDragOver = tlRibbonDragOver
      OnEndDrag = tlCommandsEndDrag
      OnFocusedNodeChanged = tlCommandsFocusedNodeChanged
      OnMoveTo = tlRibbonMoveTo
      object tlRibbonMainColumn: TcxTreeListColumn
        DataBinding.ValueType = 'String'
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object cbShowQATBelowRibbon: TcxCheckBox
      Left = 373
      Top = 78
      AutoSize = False
      Caption = 'Show Quick Access Toolbar below the Ribbon'
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
      Transparent = True
      Height = 17
      Width = 325
    end
    object lbRibbonQAT: TcxLabel
      Left = 373
      Top = 55
      AutoSize = False
      Caption = 'Customize &Quick Access Toolbar:'
      Style.HotTrack = False
      Transparent = True
      Height = 17
      Width = 325
    end
    object cbRibbon: TcxComboBox
      Left = 373
      Top = 28
      Properties.DropDownListStyle = lsFixedList
      Properties.OnChange = cbRibbonPropertiesChange
      Style.HotTrack = False
      TabOrder = 4
      Width = 325
    end
    object lcMainGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object lciCommandsSource: TdxLayoutItem
      Parent = lcgCommands
      AlignVert = avTop
      CaptionOptions.Text = 'C&hoose commands from:'
      CaptionOptions.Layout = clTop
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = cbCommands
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lcuMoveUp: TdxLayoutItem
      Parent = lcgReordering
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnMoveUp
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciMoveDown: TdxLayoutItem
      Parent = lcgReordering
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnMoveDown
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciRemove: TdxLayoutItem
      Parent = lcgActions
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnRemove
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciAdd: TdxLayoutItem
      Parent = lcgActions
      CaptionOptions.Text = 'cxButton4'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnAdd
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciNewElement: TdxLayoutItem
      Parent = lcgRibbonActions
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxButton5'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnNewElement
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciRename: TdxLayoutItem
      Parent = lcgRibbonActions
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxButton6'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnRename
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciImportExport: TdxLayoutItem
      Parent = lcgRibbonActions
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton7'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Visible = False
      Control = btnImportExport
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciReset: TdxLayoutItem
      Parent = lcgControlling
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxButton8'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnReset
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciOK: TdxLayoutItem
      Parent = lcgControlling
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton9'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnOK
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciCancel: TdxLayoutItem
      Parent = lcgControlling
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton10'
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = btnCancel
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lcgCommands: TdxLayoutGroup
      Parent = lcgEditing
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = 0
    end
    object lcgActions: TdxLayoutGroup
      Parent = lcgEditing
      AlignHorz = ahLeft
      AlignVert = avCenter
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = 1
    end
    object lcgControlling: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignHorz = ahClient
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object lcgEditing: TdxLayoutGroup
      Parent = lcMainGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object lcgRibbon: TdxLayoutGroup
      Parent = lcgEditing
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = 2
    end
    object lcgRibbonActions: TdxLayoutGroup
      Parent = lcgRibbon
      AlignHorz = ahClient
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 4
    end
    object lciSeparator: TdxLayoutSeparatorItem
      Parent = lcMainGroup_Root
      CaptionOptions.Text = 'Separator'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      Index = 1
    end
    object lcgReordering: TdxLayoutGroup
      Parent = lcgEditing
      AlignHorz = ahRight
      AlignVert = avCenter
      CaptionOptions.Text = 'New Group'
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = 3
    end
    object lciCommands: TdxLayoutItem
      Parent = lcgCommands
      AlignVert = avClient
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = tlCommands
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciRibbon: TdxLayoutItem
      Parent = lcgRibbon
      AlignVert = avClient
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Control = tlRibbon
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object lciShowQATBelowRibbon: TdxLayoutItem
      Parent = lcgRibbon
      CaptionOptions.Text = 'lciShowQATBelowRibbon'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = cbShowQATBelowRibbon
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object lciRibbonQAT: TdxLayoutItem
      Parent = lcgRibbon
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lbRibbonQAT
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lciRibbonSource: TdxLayoutItem
      Parent = lcgRibbon
      AlignVert = avTop
      CaptionOptions.Text = 'Customize the Ri&bbon:'
      CaptionOptions.Layout = clTop
      LayoutLookAndFeel = dxLayoutCxLookAndFeel
      Visible = False
      Control = cbRibbon
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
  object alActions: TActionList
    Left = 208
    object acAddNewContext: TAction
      Caption = 'Add New &Context'
      OnExecute = acAddNewContextExecute
    end
    object acAddNewTab: TAction
      Caption = 'Add New &Tab'
      OnExecute = acAddNewTabExecute
    end
    object acAddNewGroup: TAction
      Caption = 'Add New &Group'
      OnExecute = acAddNewGroupExecute
    end
    object acAdd: TAction
      Caption = '&Add'
      OnExecute = acAddExecute
    end
    object acRemove: TAction
      Caption = '&Remove'
      ShortCut = 46
      OnExecute = acRemoveExecute
    end
    object acRename: TAction
      Caption = 'Rena&me...'
      ShortCut = 113
      OnExecute = acRenameExecute
    end
    object acShowTab: TAction
      AutoCheck = True
      Caption = '&Show Tab'
      OnExecute = acShowTabExecute
    end
    object acResetAllCustomizations: TAction
      Caption = 'Reset a&ll customizations'
      OnExecute = acResetAllCustomizationsExecute
    end
    object acResetSelectedTab: TAction
      Caption = 'Reset Ta&b'
      OnExecute = acResetSelectedTabExecute
    end
    object acMoveUp: TAction
      Caption = 'Move &Up'
      OnExecute = acMoveUpExecute
    end
    object acMoveDown: TAction
      Caption = 'Move &Down'
      OnExecute = acMoveDownExecute
    end
    object acUpdateActionsState: TAction
      Caption = 'UpdateActionsState'
      OnExecute = acUpdateActionsStateExecute
    end
  end
  object dxLayoutLookAndFeelList: TdxLayoutLookAndFeelList
    Left = 240
    object dxLayoutCxLookAndFeel: TdxLayoutCxLookAndFeel
    end
  end
  object bmMain: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 272
    DockControlHeights = (
      0
      0
      0
      0)
    object bbtnNewContext: TdxBarButton
      Action = acAddNewContext
      Category = 0
    end
    object bbtnNewTab: TdxBarButton
      Action = acAddNewTab
      Category = 0
    end
    object bbtnNewGroup: TdxBarButton
      Action = acAddNewGroup
      Category = 0
    end
    object bsUpperSeparator: TdxBarSeparator
      Caption = 'Upper separator'
      Category = 0
      Hint = 'Upper separator'
      Visible = ivAlways
      ShowCaption = False
    end
    object bbtnRemove: TdxBarButton
      Action = acRemove
      Category = 0
    end
    object bbtnRename: TdxBarButton
      Action = acRename
      Category = 0
    end
    object bbtnResetTab: TdxBarButton
      Action = acResetSelectedTab
      Category = 0
    end
    object bbtnShowTab: TdxBarButton
      Action = acShowTab
      Category = 0
      ButtonStyle = bsChecked
    end
    object bsLowerSeparator: TdxBarSeparator
      Caption = 'Lower separator'
      Category = 0
      Hint = 'Lower separator'
      Visible = ivAlways
      ShowCaption = False
    end
    object bbtnMoveUp: TdxBarButton
      Action = acMoveUp
      Category = 0
    end
    object bbtnMoveDown: TdxBarButton
      Action = acMoveDown
      Category = 0
    end
    object bbtnResetOnlySelectedTab: TdxBarButton
      Action = acResetSelectedTab
      Caption = 'Reset only &selected tab'
      Category = 0
    end
    object bbtnResetAllCustomizations: TdxBarButton
      Action = acResetAllCustomizations
      Category = 0
    end
    object bbtnImportCustomizationFile: TdxBarButton
      Caption = 'Import customization file'
      Category = 0
      Hint = 'Import customization file'
      Visible = ivAlways
    end
    object bbtnExportAllCustomizations: TdxBarButton
      Caption = 'Export all customizations'
      Category = 0
      Hint = 'Export all customizations'
      Visible = ivAlways
    end
  end
  object bpmRibbon: TdxBarPopupMenu
    BarManager = bmMain
    ItemLinks = <
      item
        Visible = True
        ItemName = 'bbtnNewTab'
      end
      item
        Visible = True
        ItemName = 'bbtnNewGroup'
      end
      item
        Visible = True
        ItemName = 'bsUpperSeparator'
      end
      item
        Visible = True
        ItemName = 'bbtnRename'
      end
      item
        Visible = True
        ItemName = 'bbtnShowTab'
      end
      item
        Visible = True
        ItemName = 'bbtnResetTab'
      end
      item
        Visible = True
        ItemName = 'bbtnRemove'
      end
      item
        Visible = True
        ItemName = 'bsLowerSeparator'
      end
      item
        Visible = True
        ItemName = 'bbtnMoveUp'
      end
      item
        Visible = True
        ItemName = 'bbtnMoveDown'
      end>
    UseOwnFont = False
    OnPopup = acUpdateActionsStateExecute
    Left = 384
    Top = 112
  end
  object bpmReset: TdxBarPopupMenu
    BarManager = bmMain
    ItemLinks = <
      item
        Visible = True
        ItemName = 'bbtnResetOnlySelectedTab'
      end
      item
        Visible = True
        ItemName = 'bbtnResetAllCustomizations'
      end>
    UseOwnFont = False
    OnPopup = acUpdateActionsStateExecute
    Left = 84
    Top = 547
  end
  object bpmNewElement: TdxBarPopupMenu
    BarManager = bmMain
    ItemLinks = <
      item
        Visible = True
        ItemName = 'bbtnNewTab'
      end
      item
        Visible = True
        ItemName = 'bbtnNewGroup'
      end>
    UseOwnFont = False
    OnPopup = acUpdateActionsStateExecute
    Left = 420
    Top = 528
  end
  object bpmImportExport: TdxBarPopupMenu
    BarManager = bmMain
    ItemLinks = <
      item
        Visible = True
        ItemName = 'bbtnImportCustomizationFile'
      end
      item
        Visible = True
        ItemName = 'bbtnExportAllCustomizations'
      end>
    UseOwnFont = False
    OnPopup = acUpdateActionsStateExecute
    Left = 670
    Top = 528
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 304
    PixelsPerInch = 96
    object stNodeDisabled: TcxStyle
    end
  end
end
