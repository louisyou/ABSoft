{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressDataController                                    }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSDATACONTROLLER AND ALL         }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit cxDataConsts;

{$I cxVer.inc}

interface

resourcestring
  // Data
  cxSDataReadError = '输入流错误';
  cxSDataWriteError = '输出流错误';
  cxSDataItemExistError = '项目已经存在';
  cxSDataRecordIndexError = '记录索引超出范围';
  cxSDataItemIndexError = '项目索引超出范围';
  cxSDataProviderModeError = '数据提供者不提供该操作';
  cxSDataInvalidStreamFormat = '错误的流格式';
  cxSDataRowIndexError = '行索引超出范围';
//  cxSDataRelationItemExistError = '关联项目不存在'; 
//  cxSDataRelationCircularReference = '细节数据控制器循环引用';
//  cxSDataRelationMultiReferenceError = '引用细节数据控制器已经存在';
  cxSDataCustomDataSourceInvalidCompare = 'GetInfoForCompare 没有实现';
  // DB
//  cxSDBDataSetNil = '数据集为空';
  cxSDBDetailFilterControllerNotFound = '细节数据控制器没有发现';
  cxSDBNotInGridMode = '数据控制器不在表格(Grid)模式';
  cxSDBKeyFieldNotFound = 'Key Field not found';

implementation                               

uses
  dxCore;

procedure AddExpressDataControllerResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAdress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAdress);
  end;

begin
  InternalAdd('cxSDataReadError', @cxSDataReadError);
  InternalAdd('cxSDataWriteError', @cxSDataWriteError);
  InternalAdd('cxSDataItemExistError', @cxSDataItemExistError);
  InternalAdd('cxSDataRecordIndexError', @cxSDataRecordIndexError);
  InternalAdd('cxSDataItemIndexError', @cxSDataItemIndexError);
  InternalAdd('cxSDataProviderModeError', @cxSDataProviderModeError);
  InternalAdd('cxSDataInvalidStreamFormat', @cxSDataInvalidStreamFormat);
  InternalAdd('cxSDataRowIndexError', @cxSDataRowIndexError);
  InternalAdd('cxSDataCustomDataSourceInvalidCompare', @cxSDataCustomDataSourceInvalidCompare);
  InternalAdd('cxSDBDetailFilterControllerNotFound', @cxSDBDetailFilterControllerNotFound);
  InternalAdd('cxSDBNotInGridMode', @cxSDBNotInGridMode);
  InternalAdd('cxSDBKeyFieldNotFound', @cxSDBKeyFieldNotFound);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressDataController', @AddExpressDataControllerResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressDataController');

end.
