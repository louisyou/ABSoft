{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressLayoutControl common routines                     }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSLAYOUTCONTROL AND ALL          }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxLayoutStrs;

{$I cxVer.inc}

interface

resourcestring

  sdxLayoutControlContainerCannotBeControl = '容器不能是其项目的控件。';
  sdxLayoutControlControlIsUsed = '%s 项目已经使用 %s 控件。';

  sdxLayoutControlNewGroupCaption = '新建组';
  sdxLayoutControlRoot = '根';
  sdxLayoutControlNewItemCaption = '新项';
  sdxLayoutControlNewEmptySpaceItemCaption = '空白项目';
  sdxLayoutControlNewSeparatorItemCaption = '分隔符';
  sdxLayoutControlNewLabeledItemCaption = '标签';
  sdxLayoutControlNewImageItemCaption = '图像';  
   sdxLayoutControlNewSplitterItemCaption = '分隔栏';
  sdxLayoutControlNewHiddenGroup = '隐藏分组';
  sdxLayoutControlNewAutoCreatedGroup = '自动创建组';

   sdxLayoutControlCustomizeFormCaption = '自定义';

  sdxLayoutControlCustomizeFormAddAuxiliaryItem = '添加辅助项目';
  sdxLayoutControlCustomizeFormAddGroup = '添加分组';
  sdxLayoutControlCustomizeFormAddItem = '添加项目';
  sdxLayoutControlCustomizeFormAddEmptySpaceItem = '添加空白项目';
  sdxLayoutControlCustomizeFormAddSeparatorItem = '添加分隔条';
  sdxLayoutControlCustomizeFormAddSplitterItem = '添加拆分条项';
  sdxLayoutControlCustomizeFormAddLabeledItem = '添加文本标签项';
  sdxLayoutControlCustomizeFormAddImageItem = '条件图片项';

 sdxLayoutControlCustomizeFormDelete = '删除';
  sdxLayoutControlCustomizeFormDeleteHint = '删除 (Del)';
  sdxLayoutControlCustomizeFormAlignBy = '对齐通过';
  sdxLayoutControlCustomizeFormClose = '关闭(&C)';
  sdxLayoutControlCustomizeFormExpandAll = '全部展开';
  sdxLayoutControlCustomizeFormCollapseAll = '全部折叠';
  sdxLayoutControlCustomizeFormAlignLeftSide = '左边';
  sdxLayoutControlCustomizeFormAlignRightSide = '右边';
  sdxLayoutControlCustomizeFormAlignTopSide = '上面';
  sdxLayoutControlCustomizeFormAlignBottomSide = '下面';
  sdxLayoutControlCustomizeFormAlignNone = '无';
  sdxLayoutControlCustomizeFormTreeViewGroup = '树状视图布局(&L)';
  sdxLayoutControlCustomizeFormListViewGroup = '可用项目(&A)';
  sdxLayoutControlCustomizeFormTabbedView = '标签页式视图(&T)';
  sdxLayoutControlCustomizeFormTreeView = '树状/平面列表浏览';
  sdxLayoutControlCustomizeFormStore = '存储布局';
  sdxLayoutControlCustomizeFormRestore = '恢复布局';
  sdxLayoutControlCustomizeFormRename = '重命名';
  sdxLayoutControlCustomizeFormUndo = '撤销';
  sdxLayoutControlCustomizeFormRedo = '重做';

  sdxLayoutControlCustomizeFormHAlign = '水平对齐';
  sdxLayoutControlCustomizeFormHAlignLeft = '左';
  sdxLayoutControlCustomizeFormHAlignCenter = '居中';
  sdxLayoutControlCustomizeFormHAlignRight = '右';
  sdxLayoutControlCustomizeFormHAlignClient = '客户区';
  sdxLayoutControlCustomizeFormHAlignParent = '父组件管理';

  sdxLayoutControlCustomizeFormVAlign = '垂直对齐';
  sdxLayoutControlCustomizeFormVAlignTop = '上';
  sdxLayoutControlCustomizeFormVAlignCenter = '居中';
  sdxLayoutControlCustomizeFormVAlignBottom = '下';
  sdxLayoutControlCustomizeFormVAlignClient = '客户区';
  sdxLayoutControlCustomizeFormVAlignParent = '父组件管理';

  sdxLayoutControlCustomizeFormDirection = '布局方向';
  sdxLayoutControlCustomizeFormDirectionHorizontal = '水平';
  sdxLayoutControlCustomizeFormDirectionVertical = '垂直';
  sdxLayoutControlCustomizeFormDirectionTabbed = '标签页式';

  sdxLayoutControlCustomizeFormTextPosition = '标题位置';
  sdxLayoutControlCustomizeFormTextPositionLeft = '左';
  sdxLayoutControlCustomizeFormTextPositionTop = '上';
  sdxLayoutControlCustomizeFormTextPositionRight = '右';
  sdxLayoutControlCustomizeFormTextPositionBottom = '下';

  sdxLayoutControlCustomizeFormCaptionAlignHorz = '标题水平对齐';
  sdxLayoutControlCustomizeFormCaptionAlignHorzLeft = '左';
  sdxLayoutControlCustomizeFormCaptionAlignHorzCenter = '居中';
  sdxLayoutControlCustomizeFormCaptionAlignHorzRight = '右';

  sdxLayoutControlCustomizeFormCaptionAlignVert = '标题垂直对齐';
  sdxLayoutControlCustomizeFormCaptionAlignVertTop = '上';
  sdxLayoutControlCustomizeFormCaptionAlignVertCenter = '居中';
  sdxLayoutControlCustomizeFormCaptionAlignVertBottom = '下';

  sdxLayoutControlCustomizeFormItemCaption = '标题';
  sdxLayoutControlCustomizeFormGroupBorder = '边框';
  sdxLayoutControlCustomizeFormGroupExpandButton = '展开按钮';

  sdxLayoutControlCustomizeFormGroup = '分组';
  sdxLayoutControlCustomizeFormUngroup = '取消分组';

  sdxLayoutControlEditFormOK = '确定';
  sdxLayoutControlEditFormCancel = '取消';

  sdxLayoutControlCollapseButtonHint = 'Click to expand';
  sdxLayoutControlExpandButtonHint = 'Click to collapse';
  sdxLayoutControlHomeButtonHint = 'Stop Float';

  // Dialogs
  sdxLayoutControlDesignerCaptionFormat = '%s - 设计器';

  // Old version
  sdxLayoutControlCustomizeFormShowBorder = '显示边框';
implementation

uses
  dxCore;

procedure AddLayoutControlResourceStringNames(AProduct: TdxProductResourceStrings);

  procedure InternalAdd(const AResourceStringName: string; AAddress: Pointer);
  begin
    AProduct.Add(AResourceStringName, AAddress);
  end;

begin
  InternalAdd('sdxLayoutControlContainerCannotBeControl', @sdxLayoutControlContainerCannotBeControl);
  InternalAdd('sdxLayoutControlControlIsUsed', @sdxLayoutControlControlIsUsed);
  InternalAdd('sdxLayoutControlNewGroupCaption', @sdxLayoutControlNewGroupCaption);
  InternalAdd('sdxLayoutControlRoot', @sdxLayoutControlRoot);
  InternalAdd('sdxLayoutControlNewItemCaption', @sdxLayoutControlNewItemCaption);
  InternalAdd('sdxLayoutControlNewEmptySpaceItemCaption', @sdxLayoutControlNewEmptySpaceItemCaption);
  InternalAdd('sdxLayoutControlNewSeparatorItemCaption', @sdxLayoutControlNewSeparatorItemCaption);
  InternalAdd('sdxLayoutControlNewLabeledItemCaption', @sdxLayoutControlNewLabeledItemCaption);
  InternalAdd('sdxLayoutControlNewImageItemCaption', @sdxLayoutControlNewImageItemCaption);
  InternalAdd('sdxLayoutControlNewSplitterItemCaption', @sdxLayoutControlNewSplitterItemCaption);
  InternalAdd('sdxLayoutControlNewHiddenGroup', @sdxLayoutControlNewHiddenGroup);
  InternalAdd('sdxLayoutControlNewAutoCreatedGroup', @sdxLayoutControlNewAutoCreatedGroup);
  InternalAdd('sdxLayoutControlCustomizeFormCaption', @sdxLayoutControlCustomizeFormCaption);
  InternalAdd('sdxLayoutControlCustomizeFormAddAuxiliaryItem', @sdxLayoutControlCustomizeFormAddAuxiliaryItem);
  InternalAdd('sdxLayoutControlCustomizeFormAddGroup', @sdxLayoutControlCustomizeFormAddGroup);
  InternalAdd('sdxLayoutControlCustomizeFormAddItem', @sdxLayoutControlCustomizeFormAddItem);
  InternalAdd('sdxLayoutControlCustomizeFormAddEmptySpaceItem', @sdxLayoutControlCustomizeFormAddEmptySpaceItem);
  InternalAdd('sdxLayoutControlCustomizeFormAddSeparatorItem', @sdxLayoutControlCustomizeFormAddSeparatorItem);
  InternalAdd('sdxLayoutControlCustomizeFormAddSplitterItem', @sdxLayoutControlCustomizeFormAddSplitterItem);
  InternalAdd('sdxLayoutControlCustomizeFormAddLabeledItem', @sdxLayoutControlCustomizeFormAddLabeledItem);
  InternalAdd('sdxLayoutControlCustomizeFormAddImageItem', @sdxLayoutControlCustomizeFormAddImageItem);
  InternalAdd('sdxLayoutControlCustomizeFormDelete', @sdxLayoutControlCustomizeFormDelete);
  InternalAdd('sdxLayoutControlCustomizeFormDeleteHint', @sdxLayoutControlCustomizeFormDeleteHint);
  InternalAdd('sdxLayoutControlCustomizeFormAlignBy', @sdxLayoutControlCustomizeFormAlignBy);
  InternalAdd('sdxLayoutControlCustomizeFormClose', @sdxLayoutControlCustomizeFormClose);
  InternalAdd('sdxLayoutControlCustomizeFormExpandAll', @sdxLayoutControlCustomizeFormExpandAll);
  InternalAdd('sdxLayoutControlCustomizeFormCollapseAll', @sdxLayoutControlCustomizeFormCollapseAll);
  InternalAdd('sdxLayoutControlCustomizeFormAlignLeftSide', @sdxLayoutControlCustomizeFormAlignLeftSide);
  InternalAdd('sdxLayoutControlCustomizeFormAlignRightSide', @sdxLayoutControlCustomizeFormAlignRightSide);
  InternalAdd('sdxLayoutControlCustomizeFormAlignTopSide', @sdxLayoutControlCustomizeFormAlignTopSide);
  InternalAdd('sdxLayoutControlCustomizeFormAlignBottomSide', @sdxLayoutControlCustomizeFormAlignBottomSide);
  InternalAdd('sdxLayoutControlCustomizeFormAlignNone', @sdxLayoutControlCustomizeFormAlignNone);
  InternalAdd('sdxLayoutControlCustomizeFormTreeViewGroup', @sdxLayoutControlCustomizeFormTreeViewGroup);
  InternalAdd('sdxLayoutControlCustomizeFormListViewGroup', @sdxLayoutControlCustomizeFormListViewGroup);
  InternalAdd('sdxLayoutControlCustomizeFormTabbedView', @sdxLayoutControlCustomizeFormTabbedView);
  InternalAdd('sdxLayoutControlCustomizeFormTreeView', @sdxLayoutControlCustomizeFormTreeView);
  InternalAdd('sdxLayoutControlCustomizeFormStore', @sdxLayoutControlCustomizeFormStore);
  InternalAdd('sdxLayoutControlCustomizeFormRestore', @sdxLayoutControlCustomizeFormRestore);
  InternalAdd('sdxLayoutControlCustomizeFormRename', @sdxLayoutControlCustomizeFormRename);
  InternalAdd('sdxLayoutControlCustomizeFormUndo', @sdxLayoutControlCustomizeFormUndo);
  InternalAdd('sdxLayoutControlCustomizeFormRedo', @sdxLayoutControlCustomizeFormRedo);
  InternalAdd('sdxLayoutControlCustomizeFormHAlign', @sdxLayoutControlCustomizeFormHAlign);
  InternalAdd('sdxLayoutControlCustomizeFormHAlignLeft', @sdxLayoutControlCustomizeFormHAlignLeft);
  InternalAdd('sdxLayoutControlCustomizeFormHAlignCenter', @sdxLayoutControlCustomizeFormHAlignCenter);
  InternalAdd('sdxLayoutControlCustomizeFormHAlignRight', @sdxLayoutControlCustomizeFormHAlignRight);
  InternalAdd('sdxLayoutControlCustomizeFormHAlignClient', @sdxLayoutControlCustomizeFormHAlignClient);
  InternalAdd('sdxLayoutControlCustomizeFormHAlignParent', @sdxLayoutControlCustomizeFormHAlignParent);
  InternalAdd('sdxLayoutControlCustomizeFormVAlign', @sdxLayoutControlCustomizeFormVAlign);
  InternalAdd('sdxLayoutControlCustomizeFormVAlignTop', @sdxLayoutControlCustomizeFormVAlignTop);
  InternalAdd('sdxLayoutControlCustomizeFormVAlignCenter', @sdxLayoutControlCustomizeFormVAlignCenter);
  InternalAdd('sdxLayoutControlCustomizeFormVAlignBottom', @sdxLayoutControlCustomizeFormVAlignBottom);
  InternalAdd('sdxLayoutControlCustomizeFormVAlignClient', @sdxLayoutControlCustomizeFormVAlignClient);
  InternalAdd('sdxLayoutControlCustomizeFormVAlignParent', @sdxLayoutControlCustomizeFormVAlignParent);
  InternalAdd('sdxLayoutControlCustomizeFormDirection', @sdxLayoutControlCustomizeFormDirection);
  InternalAdd('sdxLayoutControlCustomizeFormDirectionHorizontal', @sdxLayoutControlCustomizeFormDirectionHorizontal);
  InternalAdd('sdxLayoutControlCustomizeFormDirectionVertical', @sdxLayoutControlCustomizeFormDirectionVertical);
  InternalAdd('sdxLayoutControlCustomizeFormDirectionTabbed', @sdxLayoutControlCustomizeFormDirectionTabbed);
  InternalAdd('sdxLayoutControlCustomizeFormGroupBorder', @sdxLayoutControlCustomizeFormGroupBorder);
  InternalAdd('sdxLayoutControlCustomizeFormGroupExpandButton', @sdxLayoutControlCustomizeFormGroupExpandButton);
  InternalAdd('sdxLayoutControlEditFormOK', @sdxLayoutControlEditFormOK);
  InternalAdd('sdxLayoutControlEditFormCancel', @sdxLayoutControlEditFormCancel);
  InternalAdd('sdxLayoutControlDesignerCaptionFormat', @sdxLayoutControlDesignerCaptionFormat);
  InternalAdd('sdxLayoutControlCustomizeFormTextPosition', @sdxLayoutControlCustomizeFormTextPosition);
  InternalAdd('sdxLayoutControlCustomizeFormTextPositionLeft', @sdxLayoutControlCustomizeFormTextPositionLeft);
  InternalAdd('sdxLayoutControlCustomizeFormTextPositionTop', @sdxLayoutControlCustomizeFormTextPositionTop);
  InternalAdd('sdxLayoutControlCustomizeFormTextPositionRight', @sdxLayoutControlCustomizeFormTextPositionRight);
  InternalAdd('sdxLayoutControlCustomizeFormTextPositionBottom', @sdxLayoutControlCustomizeFormTextPositionBottom);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignHorz', @sdxLayoutControlCustomizeFormCaptionAlignHorz);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignHorzLeft', @sdxLayoutControlCustomizeFormCaptionAlignHorzLeft);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignHorzCenter', @sdxLayoutControlCustomizeFormCaptionAlignHorzCenter);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignHorzRight', @sdxLayoutControlCustomizeFormCaptionAlignHorzRight);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignVert', @sdxLayoutControlCustomizeFormCaptionAlignVert);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignVertTop', @sdxLayoutControlCustomizeFormCaptionAlignVertTop);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignVertCenter', @sdxLayoutControlCustomizeFormCaptionAlignVertCenter);
  InternalAdd('sdxLayoutControlCustomizeFormCaptionAlignVertBottom', @sdxLayoutControlCustomizeFormCaptionAlignVertBottom);
  InternalAdd('sdxLayoutControlCustomizeFormItemCaption', @sdxLayoutControlCustomizeFormItemCaption);
  InternalAdd('sdxLayoutControlCustomizeFormGroup', @sdxLayoutControlCustomizeFormGroup);
  InternalAdd('sdxLayoutControlCustomizeFormUngroup', @sdxLayoutControlCustomizeFormUngroup);
  InternalAdd('sdxLayoutControlCollapseButtonHint', @sdxLayoutControlCollapseButtonHint);
  InternalAdd('sdxLayoutControlExpandButtonHint', @sdxLayoutControlExpandButtonHint);
  InternalAdd('sdxLayoutControlHomeButtonHint', @sdxLayoutControlHomeButtonHint);

  // Old version
  InternalAdd('sdxLayoutControlCustomizeFormShowBorder', @sdxLayoutControlCustomizeFormShowBorder);
end;

initialization
  dxResourceStringsRepository.RegisterProduct('ExpressLayoutControl', @AddLayoutControlResourceStringNames);

finalization
  dxResourceStringsRepository.UnRegisterProduct('ExpressLayoutControl');

end.
