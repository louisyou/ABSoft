{
连接单元
两层或三层兼容相关
}
unit ABThirdConnU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubConstU,
  ABPubMessageU,
  ABPubFuncU,
  ABPubVarU,
  ABPubUserU,
  ABPubDBU,
  ABPubLogU,
  ABPubLocalParamsU,
  ABPubSQLLogU,

  ABThirdDBU,
  ABThirdConnDatabaseU,
  ABThirdConnServerU,

  ADODB,
  typinfo,
  DB,DBClient,
  FireDAC.Comp.Client,
  Dialogs,
  Variants, DateUtils,SysUtils,Classes;


//设置数据连接,生成连接列表
function ABCreateConns(aShow:boolean=false):boolean;

//手工增加连接到连接列表中(当非框架方式调用包中的函数时可使用这种方式)
function ABAddToConnList(aConnName:string;aConn:TCustomConnection;aDatabaseType:TABDatabaseType=dtSQLServer):boolean;
//根据连接名称从连接列表中取得连接结构
function ABGetConnInfoByConnName(aConnName:string;aNoConnShow:boolean=true):TABConninfo;

//根据连接名称设置数据集的连接
function ABSetDatasetConnByConnName(aDataset:TDataset;aConnName:String):boolean;

//开启事务
procedure ABBegTransaction(aConnName:string);
//撤消事务
procedure ABRollbackTransaction(aConnName:string);
//提交事务
procedure ABCommitTransaction(aConnName:string);
//设置连接控件的超时
procedure ABSetConnTimeout(aConnName: string;aTimeout:LongInt);

//检测连接名称是否连接
function ABIsConn(aConnName:string):Boolean; overload;
function ABIsConn:Boolean; overload;

//刷新连接
function ABRefreshConn:boolean;
function ABRefreshConnByConnName(aConnName:String):boolean;

type
  TABConnNotifyEvent = procedure();
var
  ABAfterConnProc: TABConnNotifyEvent;
  ABAfterRefreshConnProc: TABConnNotifyEvent;


implementation

function ABCreateConns(aShow:boolean=false):boolean;
var
  tempDo:boolean;
begin
  tempDo:=false;
  result:=true;
  if (aShow) or
     (not ABIsConn)  then
  begin
    if ABLocalParams.LoginType=ltCS_Two then
    begin
      tempDo:=true;
      Result:=ABThirdConnDatabaseU.ABCreateConns_inside(aShow);
    end
    else if ABLocalParams.LoginType=ltCS_Three then
    begin
      tempDo:=true;
      Result:=ABThirdConnServerU.ABCreateConns_inside(aShow);
    end;
  end;

  if (tempDo) then
  begin
    if Result then
    begin
      if Assigned(ABAfterConnProc) then
        ABAfterConnProc;
    end;
  end;
end;

function ABAddToConnList(aConnName:string;aConn:TCustomConnection;aDatabaseType:TABDatabaseType):boolean;
var
  tempFind:Boolean;
  i:longint;
  tempConnParams:TABConnParams;
begin
  Result:=false;
  tempFind:=false;;
  for I := low(ABConns) to High(ABConns) do
  begin
    if (AnsiCompareText(aConnName,ABConns[i].Conninfo.ConnName)=0) then
    begin
      tempFind:=true;;
      Break;
    end;
  end;

  if not tempFind then
  begin
    SetLength(ABConns,high(ABConns)+1+1);
    i:=high(ABConns);
    tempConnParams:=ABGetConnParams(aConn);

    ABConns[i].Conninfo.DatabaseType   :=aDatabaseType;
    ABConns[i].Conninfo.Manual   :=True;
    ABConns[i].Conninfo.ConnName :=aConnName;
    ABConns[i].Conninfo.Host     :=tempConnParams.Host;
    ABConns[i].Conninfo.Database :=tempConnParams.Database;
    ABConns[i].Conninfo.IsMain   :=tempConnParams.IsMain;
    ABConns[i].Conninfo.UserName :=tempConnParams.UserName;
    ABConns[i].Conninfo.PassWord :=tempConnParams.PassWord;
    ABConns[i].Conn              :=aConn;
    Result:=True;
  end;
end;

function ABGetConnInfoByConnName(aConnName:string;aNoConnShow:boolean):TABConninfo;
var
  i:LongInt;
  tempOK:Boolean;
begin
  tempOK:=false;
  if (aConnName=EmptyStr) then
    aConnName:='Main';

  for I := low(ABConns) to High(ABConns) do
  begin
    if ((aConnName=EmptyStr)) or
       (AnsiCompareText(aConnName,ABConns[i].Conninfo.ConnName)=0) then
    begin
     tempOK:=True;
      Result:=ABConns[i];
      Break;
    end;
  end;

  if (not tempOK) and
     (aNoConnShow) then
  begin
    abshow('数据库连接[%s]暂停使用或未在数据库连接列表中',[aConnName]);
    abort;
  end;
end;

procedure ABBegTransaction(aConnName:string);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        TFireDACConnection(tempConn).StartTransaction;
      end
      else if tempConn is TADOConnection then
      begin
        TADOConnection(tempConn).BeginTrans;
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.StartTransaction(aConnName);
  end;
end;

procedure ABCommitTransaction(aConnName:string);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        TFireDACConnection(tempConn).Commit;
      end
      else if tempConn is TADOConnection then
      begin
        TADOConnection(tempConn).CommitTrans;
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.Commit(aConnName);
  end;
end;

procedure ABRollbackTransaction(aConnName:string);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        TFireDACConnection(tempConn).Rollback;
      end
      else if tempConn is TADOConnection then
      begin
        TADOConnection(tempConn).RollbackTrans;
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.Rollback(aConnName);
  end;
end;

procedure ABSetConnTimeout(aConnName: string;aTimeout:LongInt);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        ABWriteItemInStrings('LoginTimeout',inttostr(aTimeout),TFireDACConnection(tempConn).Params);
      end
      else if tempConn is TADOConnection then
      begin
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.SetTimeout(aConnName,aTimeout);
  end;
end;

function ABIsConn:Boolean;
begin
  Result:=(high(ABConns)-low(ABConns))>=0;
end;

function ABIsConn(aConnName:string):Boolean;
var
  i:LongInt;
begin
  Result:=false;
  for I := low(ABConns) to High(ABConns) do
  begin
    if ((aConnName=EmptyStr)) or
       (AnsiCompareText(aConnName,ABConns[i].Conninfo.ConnName)=0) then
    begin
      Result:=true;
      Break;
    end;
  end;
end;

function ABRefreshConnByConnName(aConnName:String):boolean;
var
  tempConn:TCustomConnection;
begin
  result:=false;
  if ABIsConn then
  begin
    try
      tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
      if Assigned(tempConn) then
      begin
        tempConn.Close;
        if (ABLocalParams.LoginType=ltCS_Two) then
          tempConn.open;
        result:=True;
      end;
    except
    end;
  end;
end;

function ABRefreshConn:boolean;
var
  i:LongInt;
begin
  result:=True;
  for I := low(ABConns) to High(ABConns) do
  begin
    if not ABRefreshConnByConnName(ABConns[i].Conninfo.ConnName) then
    begin
      result:=false;
    end;
  end;

  if Assigned(ABAfterRefreshConnProc) then
    ABAfterRefreshConnProc;
end;

function ABSetDatasetConnByConnName(aDataset:TDataset;aConnName:String):boolean;
begin
  result:=false;
  if ABCreateConns then
  begin
    ABSetDatasetConn(aDataset,ABGetConnInfoByConnName(aConnName,false).Conn);
    result:=True;
  end;
end;




end.
