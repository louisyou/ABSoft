{
注册组件单元
}
unit ABThirdRegU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubFuncU,
  ABPubVarU,

  ABThirdConndatabaseU,
  ABThirdCustomQueryU,
  ABThirdQueryU,
  ABThirdDBU,
  ABThirdConnU,
  ABThird_DevExtPopupMenuU,

  DesignIntf,Classes,DesignEditors;

type
  //SQL编辑器
  TABSQLBuildProperty = class(TStringProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  //连接名称下拉列框的属性编辑器
  TABConnNameComboxProperty = class(TStringProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;



procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ABThird',
    [
//      TABSQLBuild,
      TABcxGridPopupMenu,
      TABcxPivotGridPopupMenu
      ]);


  RegisterComponents('ABThirdDB',
    [
      TFireDACConnection,
      TFireDACMemTable,

      TFireDACQuery,
      TABThirdQuery

    ]);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'ConnName',TABConnNameComboxProperty);
{

  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'BeforeInsertCheckFieldValues',TABFieldSelectsProperty);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'BeforeDeleteCheckFieldValues',TABFieldSelectsProperty);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'BeforeEditCheckFieldValues',TABFieldSelectsProperty);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'BeforePostCheckFieldValues',TABFieldSelectsProperty);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'FieldDefaultValues',TABFieldSelectsProperty);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'FieldCaptions',TABFieldSelectsProperty);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'FocusFieldNameOnInsert',TABFieldComboxProperty);
  RegisterPropertyEditor(TypeInfo(string),TABThirdReadDataQuery , 'FocusFieldNameOnEdit',TABFieldComboxProperty);
}
end;

{ TABSQLBuildProperty }

function TABSQLBuildProperty.GetAttributes: TPropertyAttributes;
begin
  result:=[paDialog,paMultiselect];
end;

procedure TABSQLBuildProperty.GetValues(Proc: TGetStrProc);
var
  tempStr1:string;
begin
//  ABSQLBuild(tempStr1,ABGetDataSetProperty(GetComponent(0)));
  Proc(tempStr1);
end;

{ TABConnNameComboxProperty }

function TABConnNameComboxProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList];
end;

procedure TABConnNameComboxProperty.GetValues(Proc: TGetStrProc);
var
  i:LongInt;
begin
  for I := low(ABConns) to High(ABConns) do
  begin
    Proc(ABConns[i].Conninfo.ConnName);
  end;
end;

end.



