object ABDBPanelForm: TABDBPanelForm
  Left = 0
  Top = 0
  Caption = #25968#25454#32534#36753
  ClientHeight = 500
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ABcxPageControl1: TABcxPageControl
    Tag = 91000
    Left = 0
    Top = 0
    Width = 700
    Height = 461
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    ActivePageIndex = 0
    ClientRectBottom = 460
    ClientRectLeft = 1
    ClientRectRight = 699
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = 'cxTabSheet1'
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page1_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page1_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page1_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page1_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page1_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page1_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page1_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page1_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page1_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'cxTabSheet2'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page2_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page2_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page2_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page2_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page2_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page2_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page2_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page2_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page2_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'cxTabSheet3'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page3_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page3_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page3_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page3_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page3_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page3_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page3_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page3_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page3_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'cxTabSheet4'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page4_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        BevelInner = bvRaised
        Caption = #39
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page4_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page4_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page4_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page4_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page4_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page4_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page4_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page4_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = 'cxTabSheet5'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page5_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page5_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page5_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page5_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page5_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page5_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page5_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page5_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page5_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet6: TcxTabSheet
      Caption = 'cxTabSheet6'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page6_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page6_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page6_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page6_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page6_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page6_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page6_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page6_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page6_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet7: TcxTabSheet
      Caption = 'cxTabSheet7'
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page7_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page7_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page7_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page7_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page7_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page7_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page7_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page7_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page7_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet8: TcxTabSheet
      Caption = 'cxTabSheet8'
      ImageIndex = 7
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page8_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page8_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page8_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page8_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page8_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page8_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page8_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page8_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page8_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet9: TcxTabSheet
      Caption = 'cxTabSheet9'
      ImageIndex = 8
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page9_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page9_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page9_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page9_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page9_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page9_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page9_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page9_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page9_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
    object cxTabSheet10: TcxTabSheet
      Caption = 'cxTabSheet10'
      ImageIndex = 9
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Page10_ABDBPanel: TABDBPanel
        Left = 0
        Top = 0
        Width = 698
        Height = 439
        Align = alClient
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        AddAnchors_akRight = False
        AddAnchors_akBottom = False
        AutoHeight = False
        AutoWidth = False
        object Page10_Bevel1: TBevel
          Tag = 91000
          Left = 0
          Top = 8
          Width = 420
          Height = 2
        end
        object Page10_Bevel2: TBevel
          Tag = 91000
          Left = 8
          Top = 16
          Width = 420
          Height = 2
        end
        object Page10_Bevel3: TBevel
          Tag = 91000
          Left = 16
          Top = 24
          Width = 420
          Height = 2
        end
        object Page10_Bevel4: TBevel
          Tag = 91000
          Left = 24
          Top = 32
          Width = 420
          Height = 2
        end
        object Page10_Bevel5: TBevel
          Tag = 91000
          Left = 32
          Top = 40
          Width = 420
          Height = 2
        end
        object Page10_Bevel6: TBevel
          Tag = 91000
          Left = 40
          Top = 48
          Width = 420
          Height = 2
        end
        object Page10_Bevel7: TBevel
          Tag = 91000
          Left = 48
          Top = 56
          Width = 420
          Height = 2
        end
        object Page10_Bevel8: TBevel
          Tag = 91000
          Left = 56
          Top = 64
          Width = 420
          Height = 2
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 461
    Width = 700
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      700
      39)
    object Button2: TABcxButton
      Left = 636
      Top = 8
      Width = 58
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36820#22238
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = Button2Click
      ShowProgressBar = False
    end
    object SpeedButton1: TABcxButton
      Left = 7
      Top = 8
      Width = 50
      Height = 25
      Caption = #25193#23637'>>'
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      OnClick = SpeedButton1Click
      ShowProgressBar = False
    end
    object SpeedButton2: TABcxButton
      Left = 548
      Top = 8
      Width = 82
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #35774#35745'/'#20445#23384
      DropDownMenu = PopupMenu1
      Kind = cxbkOfficeDropDown
      LookAndFeel.Kind = lfFlat
      TabOrder = 3
      ShowProgressBar = False
    end
    object Panel2: TPanel
      Left = 74
      Top = 2
      Width = 459
      Height = 35
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 0
      object LabeledEdit2: TLabeledEdit
        Left = 144
        Top = 9
        Width = 313
        Height = 21
        TabStop = False
        EditLabel.Width = 48
        EditLabel.Height = 13
        EditLabel.Caption = #39029#38754#26631#31614
        LabelPosition = lpLeft
        TabOrder = 1
      end
      object LabeledEdit1: TLabeledEdit
        Left = 57
        Top = 9
        Width = 33
        Height = 21
        TabStop = False
        EditLabel.Width = 48
        EditLabel.Height = 13
        EditLabel.Caption = #25193#23637#39640#24230
        LabelPosition = lpLeft
        TabOrder = 0
        Text = '0'
      end
    end
  end
  object DataSource1: TDataSource
    AutoEdit = False
    Left = 264
    Top = 96
  end
  object PopupMenu1: TPopupMenu
    Left = 553
    Top = 317
    object N1: TMenuItem
      Caption = #35774#35745
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #32467#26463#35774#35745
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #20445#23384
      OnClick = N3Click
    end
  end
end
