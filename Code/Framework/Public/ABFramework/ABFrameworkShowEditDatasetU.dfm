object ABFrameworkShowEditDatasetForm: TABFrameworkShowEditDatasetForm
  Left = 0
  Top = 0
  Caption = #26174#31034#25110#32534#36753#25968#25454#38598#25968#25454
  ClientHeight = 500
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBNavigator1: TABDBNavigator
    Left = 0
    Top = 0
    Width = 700
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Caption = 'ABDBNavigator1'
    ShowCaption = False
    TabOrder = 0
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtMain
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbCustom1
    ApprovedCommitButton = nbCustom1
    ButtonNativeStyle = False
    BtnCaptions.Strings = (
      'BtnFirstRecord='
      'BtnPreviousRecord='
      'BtnNextRecord='
      'BtnLastRecord='
      'BtnInsert='
      'btnCopy='
      'BtnDelete='
      'BtnEdit='
      'BtnPost='
      'BtnCancel='
      'btnQuery='
      'BtnReport='
      'BtnCustom1='
      'BtnCustom2='
      'BtnCustom3='
      'BtnCustom4='
      'BtnCustom5='
      'BtnCustom6='
      'BtnCustom7='
      'BtnCustom8='
      'BtnCustom9='
      'BtnCustom10='
      'BtnExit=')
  end
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 481
    Width = 700
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
  end
  object ABcxGrid1: TABcxGrid
    Left = 0
    Top = 25
    Width = 700
    Height = 415
    Align = alClient
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
      PopupMenu.AutoHotkeys = maManual
      PopupMenu.CloseFootStr = False
      PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
      PopupMenu.AutoApplyBestFit = True
      PopupMenu.AutoCreateAllItem = True
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ABDatasource1
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Filter.AutoDataSetFilter = True
      DataController.Filter.TranslateBetween = True
      DataController.Filter.TranslateIn = True
      DataController.Filter.TranslateLike = True
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.DataRowSizing = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.MultiSelect = True
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      ExtPopupMenu.AutoHotkeys = maManual
      ExtPopupMenu.CloseFootStr = False
      ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
      ExtPopupMenu.AutoApplyBestFit = True
      ExtPopupMenu.AutoCreateAllItem = True
    end
    object cxGridLevel1: TcxGridLevel
      GridView = ABcxGridDBBandedTableView1
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 440
    Width = 700
    Height = 41
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      700
      41)
    object btn1: TButton
      Left = 534
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      Default = True
      TabOrder = 0
      OnClick = btn1Click
    end
    object btn2: TButton
      Left = 615
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 1
      OnClick = btn2Click
    end
  end
  object ABDatasource1: TABDatasource
    Left = 560
    Top = 336
  end
end
