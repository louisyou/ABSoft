{
设计面板查询单元
通过数据字典中指定的查询字段生成查询面板来查询数据集数据
}
unit ABFrameworkQuerySelectFieldPanelU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubPanelU,
  ABPubFuncU,
  ABPubMessageU,

  ABThirdCustomQueryU,
  ABThirdQueryU,

  ABFrameworkControlU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkQuerySelectFieldPanelDesignU,

  cxDropDownEdit,
  cxLabel,cxSplitter,
  Menus,SysUtils,Classes,Controls,DB,ExtCtrls,cxButtons,StdCtrls;

type
  TABQuerySelectFieldPanel = class(TABCustomPanel)
  private
    //是否创建了导入数据的菜单
    FCreateInputPopupMenu:boolean;
    //内部控件变量
    FQueryControlPanel:TABCustomPanel;
    FQueryButtonPanel:TABCustomPanel;
    FQueryButton,
    FInputQueryButton:TABcxButton;
    //导入数据的菜单
    FInputPopupMenu: TPopupMenu;
    //初始化的菜单
    FInitPopupMenu: TPopupMenu;
  private
    FWhereRemark: string;
    FOnQueryButtonClick: TNotifyEvent;
    FOnInputQueryButtonClick: TNotifyEvent;
    FDFMControl: string;
    FAutoButtonPosition: boolean;
    FDataSource: TDataSource;
    procedure CreateConstControls;
    procedure FreeConstControls;
    procedure SetDFMControl(const Value: string);
    procedure SetQueryControlPanelAutoPosition;
    procedure SelectMenuItemOfGroup(Sender: TObject);
    function GetQueryButtonPanelWidth: longint;
    procedure SetQueryButtonPanelWidth(const Value: longint);
    function GetHideInputQueryButton: boolean;
    procedure SetHideInputQueryButton(const Value: boolean);
    function GetInputQueryButtonLeft: longint;
    function GetInputQueryButtonTop: longint;
    function GetQueryButtonLeft: longint;
    function GetQueryButtonTop: longint;
    procedure SetInputQueryButtonLeft(const Value: longint);
    procedure SetInputQueryButtonTop(const Value: longint);
    procedure SetQueryButtonLeft(const Value: longint);
    procedure SetQueryButtonTop(const Value: longint);
  protected
    procedure InputQueryButtonClick(Sender: TObject);
    procedure QueryButtonClick(Sender: TObject);

    procedure Resize; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure Loaded; override;
  public
    procedure ReFreshControl;
    function GetWidth:LongInt;
    procedure PopupMenu1Popup(Sender: TObject);

    //根据DFM形式的控件信息创建查询的输入控件
    procedure RefreshQueryControls;
    //弹出设计输入控件的界面
    procedure DesignInputControls;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    //查询条件描述
    property WhereRemark : string read FWhereRemark;
    //关联的数据源
    property DataSource : TDataSource read FDataSource write FDataSource;
    //序列化到DFM形式的控件信息
    property DFMControl : string read FDFMControl write SetDFMControl;

    property AutoButtonPosition : boolean read FAutoButtonPosition write FAutoButtonPosition;
    property QueryButtonPanelWidth : longint read GetQueryButtonPanelWidth write SetQueryButtonPanelWidth;

    property QueryButtonLeft : longint read GetQueryButtonLeft write SetQueryButtonLeft;
    property QueryButtonTop : longint read GetQueryButtonTop write SetQueryButtonTop;

    property HideInputQueryButton : boolean read GetHideInputQueryButton write SetHideInputQueryButton;
    property InputQueryButtonLeft : longint read GetInputQueryButtonLeft write SetInputQueryButtonLeft;
    property InputQueryButtonTop : longint read GetInputQueryButtonTop write SetInputQueryButtonTop;

    //点击查询按钮触发事件
    property OnQueryButtonClick :TNotifyEvent read FOnQueryButtonClick write FOnQueryButtonClick;
    //点击导入按钮触发事件
    property OnInputQueryButtonClick :TNotifyEvent read FOnInputQueryButtonClick write FOnInputQueryButtonClick;
  end;

implementation

{ TABQuerySelectFieldPanel }

constructor TABQuerySelectFieldPanel.Create(AOwner: TComponent);
begin
  inherited;
  FAutoButtonPosition:=true;
  CreateConstControls;
end;

destructor TABQuerySelectFieldPanel.Destroy;
begin
  FreeConstControls;
  inherited;
end;

procedure TABQuerySelectFieldPanel.CreateConstControls;
begin
  FreeConstControls;

  FInputPopupMenu:=TPopupMenu.Create(self);
  FInputPopupMenu.OnPopup:=PopupMenu1Popup;

  FQueryControlPanel:=TABCustomPanel.Create(Self);
  FQueryControlPanel.Name:='InnerQueryControlPanel';
  FQueryControlPanel.Parent:=self;
  FQueryControlPanel.Align:=alClient;
  FQueryControlPanel.BevelOuter:=bvNone;

  FQueryButtonPanel:=TABCustomPanel.Create(Self);
  FQueryButtonPanel.Name:='InnerQueryButtonPanel';
  FQueryButtonPanel.Parent:=self;
  FQueryButtonPanel.Align:=alRight;
  FQueryButtonPanel.BevelOuter:=bvNone;
  FQueryButtonPanel.Width:=165;

  FQueryButton:=TABcxButton.Create(Self);
  FQueryButton.Caption:=('查询');
  FQueryButton.Name:='InnerQueryButton';
  FQueryButton.Parent:=FQueryButtonPanel;
  FQueryButton.Height:=25;
  FQueryButton.Width:=75;
  FQueryButton.top:=5;
  FQueryButton.Left:=5;
  FQueryButton.OnClick:=QueryButtonClick;

  FInputQueryButton:=TABcxButton.Create(Self);
  FInputQueryButton.DropDownMenu:=FInputPopupMenu;
  FInputQueryButton.Caption:=('导入查询');
  FInputQueryButton.Name:='InnerInputQueryButton';
  FInputQueryButton.Parent:=FQueryButtonPanel;
  FInputQueryButton.Height:=25;
  FInputQueryButton.Width:=75;
  FInputQueryButton.top:=5;
  FInputQueryButton.Left:=FQueryButton.Left+FQueryButton.Width+5;
  FInputQueryButton.Kind:=cxbkDropDownButton;
  FInputQueryButton.OnClick:=InputQueryButtonClick;
end;

procedure TABQuerySelectFieldPanel.FreeConstControls;
var
  i:LongInt;
begin
  if Assigned(FInputPopupMenu) then
    FInputPopupMenu.Free;
  if Assigned(FInitPopupMenu) then
    FInitPopupMenu.Free;

  if Assigned(FQueryControlPanel) then
  begin
    for I := FQueryControlPanel.ControlCount - 1  downto 0 do
    begin
      FQueryControlPanel.Controls[i].Free;
    end;
    FQueryControlPanel.Free;
  end;

  if Assigned(FQueryButton) then
    FQueryButton.Free;
  if Assigned(FInputQueryButton) then
    FInputQueryButton.Free;
  if Assigned(FQueryButtonPanel) then
    FQueryButtonPanel.Free;
end;

function TABQuerySelectFieldPanel.GetHideInputQueryButton: boolean;
begin
  result:= false;
  if (Assigned(FInputQueryButton))  then
  begin
    result:= not FInputQueryButton.Visible;    
  end;
end;

function TABQuerySelectFieldPanel.GetInputQueryButtonLeft: longint;
begin
  result:= 0;
  if (Assigned(FInputQueryButton))  then
  begin
    result:= FInputQueryButton.Left;
  end;
end;

function TABQuerySelectFieldPanel.GetInputQueryButtonTop: longint;
begin
  result:= 0;
  if (Assigned(FInputQueryButton))  then
  begin
    result:= FInputQueryButton.Top;
  end;
end;

function TABQuerySelectFieldPanel.GetQueryButtonLeft: longint;
begin
  result:= 0;
  if (Assigned(FQueryButton))  then
  begin
    result:= FQueryButton.Left;
  end;
end;

function TABQuerySelectFieldPanel.GetQueryButtonPanelWidth: longint;
begin
  result:=  FQueryButtonPanel.Width;
end;

function TABQuerySelectFieldPanel.GetQueryButtonTop: longint;
begin
  result:= 0;
  if (Assigned(FQueryButton))  then
  begin
    result:= FQueryButton.Top;
  end;
end;

function TABQuerySelectFieldPanel.GetWidth: LongInt;
begin
  result:=FQueryControlPanel.Width+FQueryButtonPanel.Width+2;
end;

procedure TABQuerySelectFieldPanel.InputQueryButtonClick(Sender: TObject);
begin
  if (Assigned(FDataSource )) and
     (Assigned(FDataSource.DataSet))   then
  begin
    if (Assigned(FQueryControlPanel))  then
    begin
      PopupMenu1Popup(FInputPopupMenu);

      ABInputMultiLevelQueryControlValue(FQueryControlPanel,TABDictionaryQuery(FDataSource.DataSet),FInputPopupMenu.Items);

      if (Assigned(Owner)) and
         (Owner is TcxPopupEdit) then
      begin
        TcxPopupEdit(Owner).DroppedDown := true;
      end;

      if (Assigned(FOnInputQueryButtonClick))  then
        FOnInputQueryButtonClick(Sender);
    end;
  end;
end;

procedure TABQuerySelectFieldPanel.Loaded;
begin
  inherited;
end;

procedure TABQuerySelectFieldPanel.QueryButtonClick(Sender: TObject);
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet))   then
  begin
    if (Assigned(FQueryControlPanel))  then
      TABThirdReadDataQuery(FDataSource.DataSet).RefreshQuery(
        ABGetMultiLevelQueryWhere(FQueryControlPanel,
                                  TABDictionaryQuery(FDataSource.DataSet),
                                  TABDictionaryQuery(FDataSource.DataSet).OnMakeFieldInputWhereEvent,
                                  FWhereRemark),
                                                             []);

    if (Assigned(FOnQueryButtonClick))  then
      FOnQueryButtonClick(Sender);
  end;
end;

procedure TABQuerySelectFieldPanel.PopupMenu1Popup(Sender: TObject);
begin
  if not FCreateInputPopupMenu  then
  begin
    ABCreateMultiLevelQueryInputMenuItem(FQueryControlPanel,TABDictionaryQuery(FDataSource.DataSet),FInputPopupMenu.Items,SelectMenuItemOfGroup);
    FCreateInputPopupMenu:=true;
  end;
end;

procedure TABQuerySelectFieldPanel.SetQueryButtonLeft(const Value: longint);
begin
  if (Assigned(FQueryButton))  then
  begin
    FQueryButton.Left:=Value;
  end;
end;

procedure TABQuerySelectFieldPanel.SetQueryButtonPanelWidth(const Value: longint);
begin
  FQueryButtonPanel.Width:=Value;
end;

procedure TABQuerySelectFieldPanel.SetQueryButtonTop(const Value: longint);
begin
  if (Assigned(FQueryButton))  then
  begin
    FQueryButton.Top:=Value;
  end;
end;

procedure TABQuerySelectFieldPanel.SetQueryControlPanelAutoPosition;
var
  tempSpace:longint;
begin
  if (Assigned(FQueryButtonPanel)) and
     (Assigned(FQueryButton))  and
     (Assigned(FInputQueryButton))  then
  begin
    if FAutoButtonPosition then
    begin
      if FQueryButtonPanel.Height>=65 then
      begin
        if FQueryButton.Left<>5 then
          FQueryButton.Left:=5;

        if FInputQueryButton.Left<>5 then
          FInputQueryButton.Left:=5;

        if HideInputQueryButton then
        begin
          if FQueryButton.top<>ABTrunc((FQueryButtonPanel.Height-FQueryButton.Height)/2) then
            FQueryButton.top:=ABTrunc((FQueryButtonPanel.Height-FQueryButton.Height)/2);
        end
        else
        begin
          tempSpace:=ABTrunc((FQueryButtonPanel.Height-FQueryButton.Height-FInputQueryButton.Height-5)/2)+2;
          if FQueryButton.top<>tempSpace then
            FQueryButton.top:=tempSpace;

          if FInputQueryButton.top<>FQueryButton.Height+tempSpace+5 then
            FInputQueryButton.top:=FQueryButton.Height+tempSpace+5;
        end;
      end
      else
      begin
        if FQueryButton.Left<>5 then
          FQueryButton.Left:=5;
        if FQueryButton.top<>ABTrunc((FQueryButtonPanel.Height-FQueryButton.Height)/2)+2 then
          FQueryButton.top:=ABTrunc((FQueryButtonPanel.Height-FQueryButton.Height)/2)+2;

        if FInputQueryButton.Left<>FQueryButton.Left+FQueryButton.Width+5 then
          FInputQueryButton.Left:=FQueryButton.Left+FQueryButton.Width+5;
        if FInputQueryButton.top<>FQueryButton.top then
          FInputQueryButton.top:=FQueryButton.top;
      end;
    end
    else
    begin
      if FQueryButton.top<>QueryButtontop then
        FQueryButton.top:=QueryButtontop;
      if FQueryButton.Left<>QueryButtonLeft then
        FQueryButton.Left:=QueryButtonLeft;

      if FInputQueryButton.top<>InputQueryButtontop then
        FInputQueryButton.top:=InputQueryButtontop;
      if FInputQueryButton.Left<>InputQueryButtonLeft then
        FInputQueryButton.Left:=InputQueryButtonLeft;
    end;
  end;
end;

procedure TABQuerySelectFieldPanel.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if  (FDataSource =AComponent)   then
      FDataSource:=nil;
  end;
end;

procedure TABQuerySelectFieldPanel.Resize;
begin
  inherited;
  SetQueryControlPanelAutoPosition;
end;

procedure TABQuerySelectFieldPanel.SelectMenuItemOfGroup(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
end;

procedure TABQuerySelectFieldPanel.SetDFMControl(const Value: string);
begin
  FDFMControl := Value;
  RefreshQueryControls;
end;

procedure TABQuerySelectFieldPanel.SetHideInputQueryButton(
  const Value: boolean);
begin
  if (Assigned(FInputQueryButton))  then
  begin
    FInputQueryButton.Visible:=not Value;
    FInputQueryButton.Width:=ABIIF(FInputQueryButton.Visible,75,0);
    SetQueryControlPanelAutoPosition;
  end;
end;

procedure TABQuerySelectFieldPanel.SetInputQueryButtonLeft(
  const Value: longint);
begin
  if (Assigned(FInputQueryButton))  then
  begin
    FInputQueryButton.Left:=Value;
  end;
end;

procedure TABQuerySelectFieldPanel.SetInputQueryButtonTop(const Value: longint);
begin
  if (Assigned(FInputQueryButton))  then
  begin
    FInputQueryButton.Top:=Value;
  end;
end;

procedure TABQuerySelectFieldPanel.DesignInputControls;
begin
  if ABQuerySelectFieldPanelDesign(FDataSource,FDFMControl) then
  begin
    RefreshQueryControls;
  end;
end;

procedure TABQuerySelectFieldPanel.RefreshQueryControls;
var
  i:LongInt;
  tempStrings:TStrings;
  tempDFMControl:string;
begin
  for I := FQueryControlPanel.ControlCount - 1  downto 0 do
  begin
    FQueryControlPanel.Controls[i].Free;
  end;

  if (FDFMControl<>EmptyStr) then
  begin
    tempStrings:=TStringList.Create;
    try
      tempStrings.Text:= Trim(FDFMControl);
      tempDFMControl:=ABDFMControlCopy(tempStrings,'  ','object InnerQueryControlPanel: TABCustomPanel');
      if (tempDFMControl<>EmptyStr) then
      begin
        ABDFMPropertyToComponent(tempDFMControl,FQueryControlPanel);
        SetQueryControlPanelAutoPosition;
      end;
    finally
      tempStrings.Free;
    end;
  end;
end;

procedure TABQuerySelectFieldPanel.ReFreshControl;
begin
  if not (csDesigning in ComponentState)  then
  begin
    if (Assigned(DataSource)) and
       (Assigned(DataSource.DataSet)) and
       (DataSource.DataSet.Active) then
    begin
      ABCreateMultiLevelQueryControls(FQueryControlPanel,
                                      self,
                                      TABDictionaryQuery(FDataSource.DataSet),
                                      True,
                                      True,
                                      0,
                                      True,

                                      0,
                                      True,

                                      true,
                                      False,
                                      False
                                       );
    end;
  end;
end;


end.


