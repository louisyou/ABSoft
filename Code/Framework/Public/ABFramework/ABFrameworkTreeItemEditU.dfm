object ABDownItemForm: TABDownItemForm
  Left = 0
  Top = 0
  Caption = #22686#21152#26032#30340#19979#25289#39033#30446
  ClientHeight = 470
  ClientWidth = 420
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = ABcxGridPopupMenu1
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 429
    Width = 420
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      420
      41)
    object btn2: TcxButton
      Left = 332
      Top = 5
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = btn2Click
    end
    object ABcxLabel1: TcxLabel
      Left = 8
      Top = 4
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Properties.Alignment.Horz = taLeftJustify
      Properties.Alignment.Vert = taVCenter
      Properties.WordWrap = True
      Transparent = True
      Height = 34
      Width = 318
      AnchorY = 21
    end
  end
  object Sheet1_PageControl2: TcxPageControl
    Left = 0
    Top = 25
    Width = 420
    Height = 404
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = Sheet1_Detail_Sheet1
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    ClientRectBottom = 403
    ClientRectLeft = 1
    ClientRectRight = 419
    ClientRectTop = 21
    object Sheet1_Detail_Sheet1: TcxTabSheet
      Caption = 'Grid '#21015#34920
      ImageIndex = 0
      object Sheet1_Grid1: TcxGrid
        Left = 0
        Top = 0
        Width = 418
        Height = 382
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        object Sheet1_TableView1: TcxGridDBBandedTableView
          PopupMenu = ABcxGridPopupMenu1
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = DataSource1
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
        end
        object Sheet1_Level1: TcxGridLevel
          GridView = Sheet1_TableView1
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = #26641#21015#21015#34920
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dxDBTreeView1: TdxDBTreeView
        Left = 0
        Top = 0
        Width = 418
        Height = 382
        ShowNodeHint = True
        DataSource = DataSource1
        KeyField = 'Ti_Guid'
        ListField = 'Ti_Name'
        ParentField = 'Ti_ParentGuid'
        RootValue = Null
        SeparatedSt = ' - '
        RaiseOnError = True
        ReadOnly = True
        DragMode = dmAutomatic
        Indent = 19
        Align = alClient
        ParentColor = False
        Options = [trDBCanDelete, trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
        SelectedIndex = -1
        TabOrder = 0
        PopupMenu = ABDBMainDetailPopupMenu1
      end
    end
  end
  object ABMultilingualDBNavigator1: TABMultilingualDBNavigator
    Left = 0
    Top = 0
    Width = 420
    Height = 25
    DataSource = DataSource1
    VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
    Align = alTop
    TabOrder = 2
  end
  object DataSource1: TDataSource
    AutoEdit = False
    Left = 210
    Top = 56
  end
  object EditRepository: TcxEditRepository
    Left = 244
    Top = 280
  end
  object ABDBMainDetailPopupMenu1: TABDBMainDetailPopupMenu
    ParentField = 'Ti_ParentGuid'
    KeyField = 'Ti_Guid'
    ListField = 'Ti_Name'
    DefaultParentValue = '0'
    Left = 288
    Top = 112
  end
  object ABcxGridPopupMenu1: TABcxGridPopupMenu
    AutoHotkeys = maManual
    CloseFootStr = False
    LinkTableView = Sheet1_TableView1
    AutoApplyBestFit = True
    AutoCreateAllItem = True
    Left = 56
    Top = 64
  end
end
