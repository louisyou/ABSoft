{
框架功能窗体单元
关闭时默认释放,增加了指定操作员角色时设置控件的属性
}
unit ABFrameworkFuncFormU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubRegisterFileU,
  ABPubUserU,
  ABPubFuncU,
  ABPubDesignU,
  ABPubDBU,
  ABPubDesignAlignU,
  ABPubLocalParamsU,
  ABPubLogU,
  ABPubFormU,

  ABThirdFormU,
  ABThirdDBU,
  ABThirdConnU,
  ABThirdCustomQueryU,

  ABFrameworkObjectInspectorU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkFuncU,
  ABFrameworkUserU,

  DB,Controls,Forms,SysUtils,Classes,Windows,Messages;
  
type
  TABFuncForm = class(TABThirdForm)
  private
    FLoadControlProperty:Boolean;
    procedure LoadControlProperty;
    { Private declarations }
  protected
    procedure Loaded; override;
     { Private declarations }
  public
    procedure ChangeDesignControlMsg(var Msg: Tmessage); message ABChangeDesignControl_Msg;
    procedure   WMMOVE(var   Msg:   TMessage);   message   WM_MOVE;
    { Public declarations }
  published
  end;
                         


implementation

{$R *.dfm}

{ TABPubForm }

procedure TABFuncForm.Loaded;
begin
  inherited;
  LoadControlProperty;
end;

procedure TABFuncForm.WMMOVE(var Msg: TMessage);
begin
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form))  then
  begin
    ABRefreshDesignAlignPosition;
    ABRefreshObjectInspectorPosition(TForm(ABPubDesignerHook.Form));
  end;
end;

procedure TABFuncForm.ChangeDesignControlMsg(var Msg: Tmessage);
begin
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form))  then
  begin
    Sleep(10);

    if (ABPubDesignerHook.ControlCount>0) and
       (Assigned(ABPubDesignerHook.Controls[0])) then
    begin
      ABSetObjectInspectorObject(ABPubDesignerHook.Controls[0]);
      ABRefreshObjectInspectorProperty;
    end
    else
      ABSetObjectInspectorObject(nil);
  end;
end;


procedure TABFuncForm.LoadControlProperty;
var
  tempDataset:TDataSet;
  tempComponent:TComponent;

  tempPropertyName,
  tempPropertyValue,
  tempLoadKeyOrOperator:string;                        
  tempDo:Boolean;
begin
  if ABCloseFormEvent then
    exit;

  if FLoadControlProperty then
    Exit;

  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+Name+'.LoadControlProperty');

  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncControlProperty',
               [
                ABGetFuncFieldValueByFuncFileName(ABGetBPLFileName(self),'Fu_Guid'),
                Name
                ],TABInsideQuery);
  if (Assigned(tempDataset)) and
     (not ABDatasetIsEmpty(tempDataset)) then
  begin
    ABSetDatasetFilter(tempDataset,ABGetBooleanFilter(tempDataset,'FP_AutoLoad',True));
    try
      if (not ABDatasetIsEmpty(tempDataset)) then
      begin
        tempDataset.First;
        while not tempDataset.Eof do
        begin
          tempLoadKeyOrOperator:=tempDataset.FindField('FP_LoadKey').AsString;
          tempDo:=false;

          if (tempLoadKeyOrOperator=EmptyStr)  then
            tempDo:=true
          else
          begin
            tempLoadKeyOrOperator:=','+tempLoadKeyOrOperator+',';
            if (ABPos(','+ABPubUser.Code+',',tempLoadKeyOrOperator)>0) then
            begin
              tempDo:=true;
            end
            else  if (Assigned(ABUser.OperatorKeyDataSet)) and
                     (not ABUser.OperatorKeyDataSet.isempty) then
            begin
              ABUser.OperatorKeyDataSet.First;
              while not ABUser.OperatorKeyDataSet.Eof do
              begin
                if (ABPos(','+ABUser.OperatorKeyDataSet.FindField('Ke_Guid').AsString+',',tempLoadKeyOrOperator)>0) then
                begin
                  tempDo:=true;
                  Break;
                end;

                ABUser.OperatorKeyDataSet.Next;
              end;
            end;
          end;

          if tempDo then
          begin
            if AnsiCompareText(tempDataset.FindField('FP_ControlName').AsString,Name)=0 then
              tempComponent:=Self
            else
              tempComponent:=FindComponent(tempDataset.FindField('FP_ControlName').AsString);

            if Assigned(tempComponent)  then
            begin
              tempPropertyName:=tempDataset.FindField('FP_PropertyName').AsString;
              if ABIsPublishProp(tempComponent,tempPropertyName) then
              begin
                tempPropertyValue:= tempDataset.FindField('FP_PropertyValue').AsString;
                if ABIsSelectSQL(tempPropertyValue) then
                begin
                  tempPropertyValue:=ABGetSQLValue('Main',tempPropertyValue,[],'');
                end;

                ABSetPropValue(tempComponent,tempPropertyName,tempPropertyValue);
              end;
            end;
          end;
          tempDataset.Next;
        end;
      end;
    finally
      ABSetDatasetFilter(tempDataset,emptystr);
      FLoadControlProperty:=true;
    end;
  end;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+Name+'.LoadControlProperty');
end;


end.
