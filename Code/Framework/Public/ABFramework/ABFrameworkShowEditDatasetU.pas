unit ABFrameworkShowEditDatasetU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkQuerySelectFieldPanelU,
  ABFrameworkcxGridU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  cxDateUtils,
  cxLookAndFeels,
  cxTextEdit,
  cxContainer,
  cxLabel,
  cxCalendar,
  cxDropDownEdit,
  cxMaskEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxCore,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,Menus,
  ABFrameworkQueryAllFieldComboxU, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxSplitter, dxBarBuiltInMenu ;

type
  TABFrameworkShowEditDatasetForm = class(TABFuncForm)
    ABDBNavigator1: TABDBNavigator;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABDatasource1: TABDatasource;
    pnl1: TPanel;
    btn1: TButton;
    btn2: TButton;
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//显示或编辑数据集数据
//aAppend=是否能增加
//aDelete=是否能删除
//aEdit=是否能修改
//aCaption=显示框标题
Function ABShowEditDataset(aDataSet:TABDictionaryQuery;
                            aAppend:Boolean=true;aDelete:Boolean=true;aEdit:Boolean=true;
                            aReturnFieldNames:string='';
                            aCaption:string='';
                            aOKCaption:string='确认';
                            aCancelCaption:string=''
                            ):String;

implementation

{$R *.dfm}

Function ABShowEditDataset(aDataSet:TABDictionaryQuery;
                            aAppend,aDelete,aEdit:Boolean;
                            aReturnFieldNames:string;
                            aCaption:string;
                            aOKCaption:string;
                            aCancelCaption:string
                             ):String;
var
  tempForm: TABFrameworkShowEditDataSetForm;
begin
  result:=emptystr;
  if not Assigned(aDataSet) then
    exit;

  tempForm := TABFrameworkShowEditDataSetForm.Create(nil);
  try
    if aOKCaption<>emptystr then
    begin
      tempForm.btn1.Caption :=aOKCaption;
      tempForm.btn1.Visible :=True;
    end
    else
    begin
      tempForm.btn1.Visible :=False;
    end;
    if aCancelCaption<>emptystr then
    begin
      tempForm.btn2.Caption :=aCancelCaption;
      tempForm.btn2.Visible :=True;
    end
    else
    begin
      tempForm.btn2.Visible :=False;
      tempForm.btn1.Left :=tempForm.btn2.Left;
    end;

    if (aAppend) then
    begin
      tempForm.ABDBNavigator1.VisibleButtons :=tempForm.ABDBNavigator1.VisibleButtons+ [nbInsertSpacer,nbInsert,nbCopy];
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Appending :=true;
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Inserting :=true;
    end
    else
    begin
      tempForm.ABDBNavigator1.VisibleButtons :=tempForm.ABDBNavigator1.VisibleButtons- [nbInsertSpacer,nbInsert,nbCopy];
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Appending :=false;
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Inserting :=false;
    end;
    if (aEdit) then
    begin
      tempForm.ABDBNavigator1.VisibleButtons :=tempForm.ABDBNavigator1.VisibleButtons+ [nbEdit];
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Editing :=true;
    end
    else
    begin
      tempForm.ABDBNavigator1.VisibleButtons :=tempForm.ABDBNavigator1.VisibleButtons- [nbEdit];
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Editing :=false;
    end;
    if (aDelete) then
    begin
      tempForm.ABDBNavigator1.VisibleButtons :=tempForm.ABDBNavigator1.VisibleButtons+ [nbDelete];
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Deleting :=true;
    end
    else
    begin
      tempForm.ABDBNavigator1.VisibleButtons :=tempForm.ABDBNavigator1.VisibleButtons- [nbDelete];
      tempForm.ABcxGridDBBandedTableView1.OptionsData.Deleting :=false;
    end;
    if ( aAppend) or ( aEdit)  then
      tempForm.ABDBNavigator1.VisibleButtons:=tempForm.ABDBNavigator1.VisibleButtons+[nbPostSpacer,nbPost,nbCancel]
    else
      tempForm.ABDBNavigator1.VisibleButtons:=tempForm.ABDBNavigator1.VisibleButtons-[nbPostSpacer,nbPost,nbCancel];

    tempForm.ABDBNavigator1.Visible :=not (tempForm.ABDBNavigator1.VisibleButtons=[]);

    if aCaption<>EmptyStr then
    begin
      tempForm.Caption:= aCaption;
      tempForm.Name:='ABFrameworkShowEditDatasetForm_'+ABStrToName(aCaption) ;
    end;

    tempForm.ABDatasource1.DataSet:=aDataSet;
    tempForm.ABDBNavigator1.RefreshSubItem;
    tempForm.ABcxGridDBBandedTableView1.ExtPopupMenu.SetupFileNameSuffix:=tempForm.Caption;
    if tempForm.ShowModal=mrOk then
    begin
      if aReturnFieldNames<>EmptyStr then
        result:=ABGetFieldValue(aDataSet,aReturnFieldNames,[]);
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABFrameworkShowEditDatasetForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABFrameworkShowEditDatasetForm.btn2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABFrameworkShowEditDatasetForm.FormShow(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],TABDictionaryQuery(ABDatasource1.DataSet),true,false,false);

  ABSetTableViewSelect(ABcxGridDBBandedTableView1);
end;

end.
