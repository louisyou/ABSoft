//
// Created by the DataSnap proxy generator.
// 2012-8-13 下午 03:21:11
// 

unit ClientClassesUnit1;

interface

uses


  DBXCommon,DBXJSON,DSProxy,Dialogs,Classes,DB,SqlExpr,DbxCompressionFilter;

type
  TABServerClient = class(TDSAdminClient)
  private
    //管理服务器上公布的方法调用
    FCurDBXCommand:TDBXCommand;
    FDBXCommandLists:TStrings;
    function GetDBXCommand(aServerMethodName: string): TDBXCommand;
    function DoExecuteUpdate(aDBXCommand: TDBXCommand): boolean;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;

    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;

    procedure GetConnInfo(out aStream: TStream);
    procedure GetDBXReader(aConnName: string; aSQL: string; out aDBXReader: TDBXReader);
    procedure GetSQLDataset(aConnName: string; aSQL: string; out aDataset: TDataSet);
    procedure GetVariant(aConnName: string; aSQL: string; out aData: OleVariant;
                         out aFieldCount:longint;out aRecoredCount:longint);
    procedure LongTimeRunFunc(aCallback: TDBXCallback);
  end;

implementation

constructor TABServerClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;

constructor TABServerClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
  FDBXCommandLists:=TStringList.Create;
end;

destructor TABServerClient.Destroy;
var
  i:LongInt;
begin
  for I := FDBXCommandLists.Count-1 downto 0 do
  begin
    TDBXCommand(FDBXCommandLists.Objects[i]).Free;
  end;
  FDBXCommandLists.Free;
  inherited;
end;

function TABServerClient.DoExecuteUpdate(aDBXCommand: TDBXCommand):boolean;
begin
  try
    aDBXCommand.ExecuteUpdate;
    Result:=true;
  except
    ShowMessage('请确认服务器已启动,请重启后再试.');
    raise;
  end;
end;

function TABServerClient.GetDBXCommand(aServerMethodName: string):TDBXCommand;
var
  i:LongInt;
begin
  i:=FDBXCommandLists.IndexOf(aServerMethodName);
  if i<0 then
  begin
    Result := FDBXConnection.CreateCommand;
    Result.CommandType := TDBXCommandTypes.DSServerMethod;
    Result.Text := aServerMethodName;
    Result.Prepare;
    FDBXCommandLists.AddObject(aServerMethodName,Result);
  end
  else
  begin
    Result:= TDBXCommand(FDBXCommandLists.Objects[i]);
  end;
end;

function TABServerClient.EchoString(Value: string): string;
begin
  FCurDBXCommand:=GetDBXCommand('TABServer.EchoString');
  FCurDBXCommand.Parameters[0].Value.SetWideString(Value);
  if DoExecuteUpdate(FCurDBXCommand) then
    Result := FCurDBXCommand.Parameters[1].Value.GetWideString;
end;

function TABServerClient.ReverseString(Value: string): string;
begin
  FCurDBXCommand:=GetDBXCommand('TABServer.ReverseString');
  FCurDBXCommand.Parameters[0].Value.SetWideString(Value);
  if DoExecuteUpdate(FCurDBXCommand) then
    Result := FCurDBXCommand.Parameters[1].Value.GetWideString;
end;

procedure TABServerClient.GetConnInfo(out aStream: TStream);
begin
  FCurDBXCommand:=GetDBXCommand('TABServer.GetConnInfo');
  if DoExecuteUpdate(FCurDBXCommand) then
    aStream := FCurDBXCommand.Parameters[0].Value.GetStream(FInstanceOwner);
end;

procedure TABServerClient.GetDBXReader(aConnName: string; aSQL: string; out aDBXReader: TDBXReader);
begin
  FCurDBXCommand:=GetDBXCommand('TABServer.GetDBXReader');
  FCurDBXCommand.Parameters[0].Value.SetWideString(aConnName);
  FCurDBXCommand.Parameters[1].Value.SetWideString(aSQL);
  if DoExecuteUpdate(FCurDBXCommand) then
    aDBXReader := FCurDBXCommand.Parameters[2].Value.GetDBXReader(FInstanceOwner);
end;

procedure TABServerClient.GetSQLDataset(aConnName: string; aSQL: string; out aDataset: TDataSet);
begin
  FCurDBXCommand:=GetDBXCommand('TABServer.GetSQLDataset');
  FCurDBXCommand.Parameters[0].Value.SetWideString(aConnName);
  FCurDBXCommand.Parameters[1].Value.SetWideString(aSQL);
  if DoExecuteUpdate(FCurDBXCommand) then
  begin
    aDataset := TCustomSQLDataSet.Create(nil, FCurDBXCommand.Parameters[2].Value.GetDBXReader(False), True);
    aDataset.Open;
    if FInstanceOwner then
      FCurDBXCommand.FreeOnExecute(aDataset);
  end;
end;

procedure TABServerClient.GetVariant(aConnName: string; aSQL: string; out aData: OleVariant;
                                     out aFieldCount:longint;out aRecoredCount:longint);
begin
  FCurDBXCommand:=GetDBXCommand('TABServer.GetVariant');
  FCurDBXCommand.Parameters[0].Value.SetWideString(aConnName);
  FCurDBXCommand.Parameters[1].Value.SetWideString(aSQL);
  FCurDBXCommand.Parameters[3].Value.SetInt32(aFieldCount);
  FCurDBXCommand.Parameters[4].Value.SetInt32(aRecoredCount);
  if DoExecuteUpdate(FCurDBXCommand) then
    aData := FCurDBXCommand.Parameters[2].Value.AsVariant;
end;

procedure TABServerClient.LongTimeRunFunc(aCallback: TDBXCallback);
begin
  FCurDBXCommand:=GetDBXCommand('TABServer.LongTimeRunFunc');
  FCurDBXCommand.Parameters[0].Value.SetCallbackValue(aCallback);
  DoExecuteUpdate(FCurDBXCommand);
end;

end.


