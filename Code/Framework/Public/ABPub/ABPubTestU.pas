unit ABPubTestU;

interface

uses

{
  ABPubCheckTreeViewU                      ,
  ABPubFrameU                              ,
  ABPubRegU                                ,

  ABPubThreadDemoU                         ,
  ABPubThreadU                             ,

  ABPubSockDemoU                           ,
  ABPubSockFrameU                          ,
  ABPubSockU                               ,
  }


  ABPubConstU                              ,
  ABPubVarU                                ,

  ABPubFileRunOneU                         ,
  ABPubDBU                                 ,

  ABPubInPutPassWordU                      ,
  ABPubEditPassWordU                       ,
  ABPubInPutStrU                           ,
  ABPubLoginU                              ,
  ABPubMessageU                            ,
  ABPubOrderStrU                           ,
  ABPubShowEditDatasetU                    ,
  ABPubShowEditFieldValueU                 ,
  ABPubStartFormU                          ,
  ABPubSelectCheckComboBoxU                ,
  ABPubSelectCheckTreeViewU                ,
  ABPubSelectComboBoxU                     ,
  ABPubSelectStrU                          ,
  ABPubSelectTreeViewU                     ,
  ABPubSelectNameAndValuesU                                ,
  ABPubDesignU                             ,
  ABPubDefaultValueU                       ,
  ABPubFormU                                                             ,

  ABPubCharacterCodingU                                                 ,
  ABPubIndyU                                                               ,
  ABPubPassU                                                                                      ,
  ABPubScriptU                             ,
  ABPubServiceU                            ,
  ABPubSQLLogU                             ,
  ABPubFuncU                               ,

  ABPubImageFieldU                         ,
  ABPubImageU                              ,
  ABPubItemListFrameU                      ,
  ABPubItemListsFrameU                     ,
  ABPubMemoFieldU                          ,
  ABPubMemoU                               ,
  ABPubLanguageEditU                       ,
  ABPubLanguageU                           ,
  ABPubLocalParamsEditU                    ,
  ABPubLocalParamsU                        ,
  ABPubAutoProgressBar_ThreadU             ,
  ABPubManualProgressBar_ThreadU           ,
  ABPubManualProgressBarU                  ,
  ABPubSockDemoU                           ,
  ABPubSockFrameU                          ,
  ABPubSockU                               ,
  ABPubThreadDemoU                         ,
  ABPubThreadU                             ,

  ABPubDBLabelsU                           ,
  ABPubMultilingualDBNavigatorU                        ,
  ABPubDBMainDetailPopupMenuU              ,
  ABPubCheckedComboBoxU                    ,
  ABPubCheckTreeViewU                      ,
  ABPubDownButtonU                         ,
  ABPubStringGridU                         ,
  ABPubIPEditU                             ,
  ABPubPakandZipU                          ,
  ABPubRegisterFileU                       ,
  ABPubScrollBoxU                          ,
  ABPubSQLParseU                           ,
  ABPubUserU                               ,
  ABPubLedU                                ,
  ABPubLogU                                ,

  TypInfo,Types,
  DBClient, ShlObj,
  Forms, Windows, Messages, SysUtils, Variants, Classes, Controls, ExtCtrls,
  DBGrids, DB, StdCtrls,
  ComCtrls, shellapi, StdConvs, DBCtrls, ImgList, Graphics, jpeg, Menus,
  Dialogs, OleServer, ADODB,
  CheckLst, Grids, IdHTTP, IdFTP, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, ABPubPanelU, System.ImageList;

type
  TABTestForm = class(TABPubForm)
    FDataSource: TDataSource;
    IdFTP1: TIdFTP;
    IdHTTP1: TIdHTTP;
    MainMenu1: TMainMenu;
    aa1: TMenuItem;
    SaveDialog1: TSaveDialog;
    ImageList1: TImageList;
    DataSource_Main: TDataSource;
    DataSource_Detail: TDataSource;
    DataSource_MainDetail: TDataSource;
    bb1: TMenuItem;
    cc1: TMenuItem;
    PopupMenu1: TPopupMenu;
    aa2: TMenuItem;
    bb2: TMenuItem;
    cc2: TMenuItem;
    bb11: TMenuItem;
    PageControl3: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Button3: TButton;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox3: TGroupBox;
    Button4: TButton;
    Button7: TButton;
    Button11: TButton;
    Button16: TButton;
    Button48: TButton;
    Label3: TLabel;
    Label4: TLabel;
    Button12: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Button13: TButton;
    btn1: TButton;
    Button1: TButton;
    Button2: TButton;
    Button14: TButton;
    Panel4: TPanel;
    GroupBox8: TGroupBox;
    Panel5: TPanel;
    GroupBox9: TGroupBox;
    Panel6: TPanel;
    Button46: TButton;
    Button47: TButton;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    Button43: TButton;
    Button10: TButton;
    Button18: TButton;
    GroupBox14: TGroupBox;
    GroupBox15: TGroupBox;
    GroupBox16: TGroupBox;
    Button19: TButton;
    Button21: TButton;
    Button20: TButton;
    GroupBox17: TGroupBox;
    GroupBox18: TGroupBox;
    Button30: TButton;
    Button31: TButton;
    GroupBox20: TGroupBox;
    GroupBox21: TGroupBox;
    GroupBox22: TGroupBox;
    GroupBox23: TGroupBox;
    GroupBox30: TGroupBox;
    GroupBox32: TGroupBox;
    Button41: TButton;
    Button33: TButton;
    Button42: TButton;
    Button44: TButton;
    Button39: TButton;
    GroupBox36: TGroupBox;
    Button17: TButton;
    Button8: TButton;
    Panel9: TPanel;
    GroupBox31: TGroupBox;
    GroupBox39: TGroupBox;
    GroupBox40: TGroupBox;
    GroupBox41: TGroupBox;
    Button45: TButton;
    Button32: TButton;
    GroupBox13: TGroupBox;
    Button36: TButton;
    GroupBox27: TGroupBox;
    Button35: TButton;
    GroupBox26: TGroupBox;
    Button37: TButton;
    GroupBox25: TGroupBox;
    Button40: TButton;
    GroupBox24: TGroupBox;
    Button38: TButton;
    GroupBox10: TGroupBox;
    Button9: TButton;
    Button49: TButton;
    GroupBox35: TGroupBox;
    Button50: TButton;
    GroupBox28: TGroupBox;
    Button51: TButton;
    GroupBox42: TGroupBox;
    ABDownButton1: TABDownButton;
    GroupBox47: TGroupBox;
    Button29: TButton;
    GroupBox46: TGroupBox;
    GroupBox48: TGroupBox;
    GroupBox49: TGroupBox;
    GroupBox50: TGroupBox;
    GroupBox51: TGroupBox;
    Button25: TButton;
    Button26: TButton;
    Button27: TButton;
    Button28: TButton;
    Button53: TButton;
    Panel8: TPanel;
    Memo4: TMemo;
    Memo3: TMemo;
    GroupBox19: TGroupBox;
    Button34: TButton;
    ProgressBar2: TProgressBar;
    Button23: TButton;
    Button24: TButton;
    Button54: TButton;
    Button55: TButton;
    TabSheet5: TTabSheet;
    Panel28: TPanel;
    GroupBox52: TGroupBox;
    GroupBox45: TGroupBox;
    DBGrid1: TDBGrid;
    Panel29: TPanel;
    GroupBox54: TGroupBox;
    Panel12: TPanel;
    TrackBar1: TTrackBar;
    Panel16: TPanel;
    Panel15: TPanel;
    Label11: TLabel;
    Image_begin: TImage;
    Panel17: TPanel;
    Label12: TLabel;
    Image_Max: TImage;
    Panel18: TPanel;
    Label13: TLabel;
    Image_Avg: TImage;
    Panel19: TPanel;
    Label14: TLabel;
    Image_WeightedAvg: TImage;
    RadioGroup2: TRadioGroup;
    Panel20: TPanel;
    Panel21: TPanel;
    Label15: TLabel;
    Image_bmp: TImage;
    Panel22: TPanel;
    Label16: TLabel;
    Image_ico: TImage;
    Panel23: TPanel;
    Label17: TLabel;
    Image_jpg: TImage;
    Panel24: TPanel;
    Label18: TLabel;
    Image_null: TImage;
    GroupBox55: TGroupBox;
    DBGrid6: TDBGrid;
    Panel14: TPanel;
    GroupBox56: TGroupBox;
    TreeView1: TTreeView;
    TreeView2: TTreeView;
    GroupBox57: TGroupBox;
    ListBox1: TListBox;
    ListBox2: TListBox;
    CheckListBox: TGroupBox;
    CheckListBox1: TCheckListBox;
    CheckListBox2: TCheckListBox;
    CheckListBox3: TCheckListBox;
    HotKey1: THotKey;
    Memo5: TMemo;
    Panel10: TPanel;
    Button52: TButton;
    Button6: TButton;
    RadioGroup1: TRadioGroup;
    DBGrid3: TDBGrid;
    Button22: TButton;
    GroupBox37: TGroupBox;
    Button56: TButton;
    GroupBox53: TGroupBox;
    Button58: TButton;
    GroupBox61: TGroupBox;
    Button59: TButton;
    Edit3: TEdit;
    Memo1: TMemo;
    ABDBMainDetailPopupMenu1: TABDBMainDetailPopupMenu;
    ABSocket1: TABSocket;
    TabSheet2: TTabSheet;
    GroupBox73: TGroupBox;
    ABSockUI1: TABSockUI;
    ABCheckedComboBox1: TABCheckedComboBox;
    Panel7: TPanel;
    GroupBox33: TGroupBox;
    ABScrollBox1: TABScrollBox;
    Panel25: TPanel;
    GroupBox43: TGroupBox;
    ABLed1: TABLed;
    ABLed2: TABLed;
    ABLed3: TABLed;
    ABLed4: TABLed;
    ABLed5: TABLed;
    ABLed6: TABLed;
    Button5: TButton;
    Panel26: TPanel;
    GroupBox3aaaaa: TGroupBox;
    ABMultilingualDBNavigator1: TABMultilingualDBNavigator;
    Panel2: TPanel;
    Image1: TImage;
    Image2: TImage;
    DBMemo1: TDBMemo;
    DBGrid2: TDBGrid;
    DBImage1: TDBImage;
    Panel1: TPanel;
    Label6: TLabel;
    ABDBLabels1: TABDBLabels;
    GroupBox34: TGroupBox;
    ABIPEdit1: TABIPEdit;
    GroupBox29: TGroupBox;
    ABStringGrid1: TABStringGrid;
    ABDownButton2: TABDownButton;
    GroupBox58: TGroupBox;
    ABCheckTreeView1: TABCheckTreeView;
    GroupBox72: TGroupBox;
    Button70: TButton;
    GroupBox59: TGroupBox;
    Button63: TButton;
    GroupBox62: TGroupBox;
    ABItemList1: TABItemList;
    GroupBox63: TGroupBox;
    ABItemLists1: TABItemLists;
    Button61: TButton;
    Button60: TButton;
    Button57: TButton;
    procedure Button10Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure Button19Click(Sender: TObject);
    procedure Button21Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure Button25Click(Sender: TObject);
    procedure Button26Click(Sender: TObject);
    procedure Button27Click(Sender: TObject);
    procedure Button28Click(Sender: TObject);
    procedure Button29Click(Sender: TObject);
    procedure Button30Click(Sender: TObject);
    procedure Button31Click(Sender: TObject);
    procedure Button33Click(Sender: TObject);
    procedure Button34Click(Sender: TObject);
    procedure Button35Click(Sender: TObject);
    procedure Button36Click(Sender: TObject);
    procedure Button37Click(Sender: TObject);
    procedure Button40Click(Sender: TObject);
    procedure Button39Click(Sender: TObject);
    procedure Button38Click(Sender: TObject);
    procedure Button41Click(Sender: TObject);
    procedure Button42Click(Sender: TObject);
    procedure Button43Click(Sender: TObject);
    procedure Button44Click(Sender: TObject);
    procedure ABScrollBox1HScroll(var Message: TWMScroll);
    procedure ABScrollBox1VScroll(var Message: TWMScroll);
    procedure Button32Click(Sender: TObject);
    procedure Button45Click(Sender: TObject);
    procedure Button47Click(Sender: TObject);
    procedure Button48Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button49Click(Sender: TObject);
    procedure Button50Click(Sender: TObject);
    procedure Button51Click(Sender: TObject);
    procedure Button53Click(Sender: TObject);
    procedure Memo3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TrackBar1Change(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button23Click(Sender: TObject);
    procedure Button24Click(Sender: TObject);
    procedure Button54Click(Sender: TObject);
    procedure Button52Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button55Click(Sender: TObject);
    procedure Button22Click(Sender: TObject);
    procedure Button56Click(Sender: TObject);
    procedure ABSQLLogExecSQL(aType:string;aSQL: string);
    procedure Button58Click(Sender: TObject);
    procedure Button60Click(Sender: TObject);
    procedure Button61Click(Sender: TObject);
    procedure Button63Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ABCheckedComboBox1Change(Sender: TObject);
    procedure GroupBox73Click(Sender: TObject);
    procedure Button57Click(Sender: TObject);
  private
    temStrings: TStrings;
    temStrings_1: TStrings;
    tempList: TList;
    FMemoryStream: TMemoryStream;
    FMemoryStream_1: TMemoryStream;
    FpriDatetime:TDateTime;
    FNeedFilePath:string;
    FDataSet: TClientDataset;

    FtestMainDataset:TClientDataSet;
    FtestDetailDataset:TClientDataSet;
    FtestMainDetailDataset:TClientDataSet;
    FShowHideHotKey_ID:integer;

    procedure FtestNewDatasetBeforeScroll(DataSet: TDataSet);
    procedure FtestNewDatasetAfterScroll(DataSet: TDataSet);
    procedure FtestNewDatasetaaChange(Sender: TField);
    procedure FtestNewDatasetaaGetText(Sender: TField; var Text: string;DisplayText: Boolean);
    procedure FtestNewDatasetaaSetText(Sender: TField; const Text: string);
    procedure FtestNewDatasetaaValidate(Sender: TField);
    procedure Addlog(aMemo:TMemo;atext: string);
    { Private declarations }
  public
    FADOConnection: TADOConnection;
    FADOQuery1,FADOQuery2: TADOQuery;

    procedure WMHotKey(var Msg : TWMHotKey); message WM_HOTKEY;
    { Public declarations }
  end;

var
  ABTestForm: TABTestForm;

implementation
{$R *.dfm}

procedure TABTestForm.FormCreate(Sender: TObject);
var
  i:LongInt;
begin
  temStrings_1:= TStringList.Create;
  temStrings:= TStringList.Create;
  tempList:= TList.Create;
  //初始化变量
  FMemoryStream:=TMemoryStream.Create;
  FMemoryStream_1:=TMemoryStream.Create;
  FNeedFilePath:=ABAppPath+'测试必备文件\';
  FpriDatetime:=Now;
  ABInDelphi := False;

  //创建共用的数据集
  FDataSet := ABCreateClientDataSet(['ParentCode', 'Code', 'Name', 'Remark',
    'Order', 'photo_bmp','photo_jpg'], ['父编号', '编号', '名称', '备注', '序号', 'bmp照片', 'jpg照片'],
    [ftString, ftString, ftString, ftMemo, ftInteger, ftBlob, ftBlob], [100, 100, 100]);
  FDataSet.Name:='TABTestForm_FDataSet';
  FDataSet.DisableControls;
  try
    for I := 1 to 2000 do
    begin
      FDataSet.Append;
      if i<=2 then
        FDataSet.FieldByName('ParentCode').AsString := '0'
      else if i<=15 then
        FDataSet.FieldByName('ParentCode').AsString := inttostr(i div 10)
      else
        FDataSet.FieldByName('ParentCode').AsString := '';

      FDataSet.FieldByName('Code').AsString := inttostr(i);
      FDataSet.FieldByName('Name').AsString := inttostr(i);
      FDataSet.FieldByName('Remark').AsString := '张三家有只小狗';
      if i<=10 then
        FDataSet.FieldByName('Order').AsInteger := i
      else
        FDataSet.FieldByName('Order').AsInteger := 0;

      if (i=1) then
      begin
        Image1.Picture.SaveToFile(FNeedFilePath+'~tempFile1');
        ABJpgFileToBmpFile(FNeedFilePath+'~tempFile1',FNeedFilePath+'~tempFile2');
        TBlobField(FDataSet.FieldByName('photo_bmp')).LoadFromFile(FNeedFilePath+'~tempFile2');
        TBlobField(FDataSet.FieldByName('photo_jpg')).LoadFromFile(FNeedFilePath+'~tempFile1');
      end;
      FDataSet.post;
    end;
  finally
    FDataSet.EnableControls;
    ABDeleteFile(FNeedFilePath+'~tempFile1');
    ABDeleteFile(FNeedFilePath+'~tempFile2');
  end;
  FDataSource.DataSet := FDataSet;
  ABDBMainDetailPopupMenu1.DataSource := FDataSource;

  //共用数据集数据导出到树中
  ABDataSetsToTree(TreeView1.Items,
                   FDataSet,
                   'ParentCode',
                   'Code',
                   'Name'
                   );

  //初始化测试用的两个数据集
  FADOConnection:= TADOConnection.Create(nil);
  FADOConnection.LoginPrompt:=false;
  FADOQuery1:= TADOQuery.Create(nil);
  FADOQuery1.Connection:=FADOConnection;
  FADOQuery2:= TADOQuery.Create(nil);
  FADOQuery2.Connection:=FADOConnection;

  FADOConnection.Close;
  FADOConnection.ConnectionString:= 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+FNeedFilePath+'Data.mdb;Persist Security Info=False';
  FADOConnection.open;
  FADOQuery1.Close;
  FADOQuery1.SQL.Text:='select * from ABSys_Org_TreeItem';
  FADOQuery1.Open;
  FADOQuery2.Close;
  FADOQuery2.SQL.Text:='select Fu_Ti_Guid,Fu_FileName from ABSys_Org_Function';
  FADOQuery2.Open;
end;

procedure TABTestForm.FormDestroy(Sender: TObject);
begin
  temStrings.Free;
  temStrings_1.Free;
  tempList.Free;
  FMemoryStream.Free;
  FMemoryStream_1.Free;
end;

procedure TABTestForm.Button25Click(Sender: TObject);
var
  tempStr: string;
begin
  tempStr := '11-1';
  if ABSelectTreeView(FADOQuery1, 'Ti_ParentGuid','Ti_Guid', 'Ti_Name',false,nil, '', '',tempStr, '(已选11-1)') then
  begin
    Addlog(Memo3,'ABPubSelectTreeViewU ABSelectTreeView[选择数据集中的树形结点 单数据集]'+tempStr);
  end;
  tempStr := '22aa';
  if ABSelectTreeView(FADOQuery1, 'Ti_ParentGuid','Ti_Guid', 'Ti_Name',false,FADOQuery2, 'Fu_Ti_Guid', 'Fu_FileName',tempStr, '(已选22aa)') then
  begin
    Addlog(Memo3,'ABPubSelectTreeViewU ABSelectTreeView[选择数据集中的树形结点 主从数据集]'+tempStr);
  end;
end;

procedure TABTestForm.Button26Click(Sender: TObject);
var
  tempStrings: Tstrings;
begin
  tempStrings := Tstringlist.Create;
  try
    tempStrings.Clear;
    tempStrings.Add('22');
    tempStrings.Add('11-1');
    if ABSelectCheckTreeView(FADOQuery1,'Ti_ParentGuid','Ti_Guid', 'Ti_Name',false,nil, '', '', tempStrings, 'Ti_Name', nil, '(已选11-1,22)') then
    begin
      Addlog(Memo3,'ABPubSelectCheckTreeViewU ABSelectCheckTreeView[选择数据集中的树形结点 单数据集]'+tempStrings.Text);
    end;
    tempStrings.Clear;
    tempStrings.Add('22aa');
    tempStrings.Add('11-1-1aa');
    if ABSelectCheckTreeView(FADOQuery1, 'Ti_ParentGuid','Ti_Guid', 'Ti_Name',false,FADOQuery2, 'Fu_Ti_Guid','Fu_FileName',tempStrings, 'Fu_FileName', nil, '(已选22aa,11-1-1aa)') then
    begin
      Addlog(Memo3,'ABPubSelectCheckTreeViewU ABSelectCheckTreeView[选择数据集中的树形结点 主从数据集]'+tempStrings.Text);
    end;
  finally
    tempStrings.Free;
  end;
end;

//字段事件
procedure TABTestForm.FtestNewDatasetaaChange(Sender: TField);
begin
  Addlog(Memo3,Sender.DataSet.Name+'_'+Sender.FieldName+'_'+'Change');
end;

procedure TABTestForm.FtestNewDatasetaaGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Addlog(Memo3,Sender.DataSet.Name+'_'+Sender.FieldName+'_'+'GetText');
end;

procedure TABTestForm.FtestNewDatasetaaSetText(Sender: TField; const Text: string);
begin
  Addlog(Memo3,Sender.DataSet.Name+'_'+Sender.FieldName+'_'+'SetText');
end;

procedure TABTestForm.FtestNewDatasetaaValidate(Sender: TField);
begin
  Addlog(Memo3,Sender.DataSet.Name+'_'+Sender.FieldName+'_'+'Validate');
end;

//记录事件
procedure TABTestForm.FtestNewDatasetAfterScroll(DataSet: TDataSet);
begin
  Addlog(Memo3,DataSet.Name+'_'+'AfterScroll');
end;

procedure TABTestForm.FtestNewDatasetBeforeScroll(DataSet: TDataSet);
begin
  Addlog(Memo3,DataSet.Name+'_'+'BeforeScroll');
end;

procedure TABTestForm.GroupBox73Click(Sender: TObject);
begin
  Caption:= ABCheckedComboBox1.DownText;
  Caption:= ABCheckedComboBox1.SaveText;

end;

procedure TABTestForm.Button23Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubConstU ABDelphiVersion'+ABEnumValueToName(TypeInfo(TABDelphiVersion),ord(ABDelphiVersion)));
end;

procedure TABTestForm.Button24Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubVarU ABAppPath           '+ABAppPath           );
  Addlog(Memo3,'ABPubVarU ABAppDir            '+ABAppDir            );
  Addlog(Memo3,'ABPubVarU ABAppName           '+ABAppName           );
  Addlog(Memo3,'ABPubVarU ABAppFullName       '+ABAppFullName       );
  Addlog(Memo3,'ABPubVarU ABPublicSetPath     '+ABPublicSetPath     );
  Addlog(Memo3,'ABPubVarU ABSoftSetPath       '+ABSoftSetPath       );
  Addlog(Memo3,'ABPubVarU ABDefaultUserPath   '+ABDefaultUserPath   );
  Addlog(Memo3,'ABPubVarU ABConfigPath        '+ABConfigPath        );
  Addlog(Memo3,'ABPubVarU ABDefaultValuePath  '+ABDefaultValuePath  );
  Addlog(Memo3,'ABPubVarU ABLocalTablePath    '+ABLocalTablePath    );
  Addlog(Memo3,'ABPubVarU ABCurUserPath       '+ABCurUserPath       );
  Addlog(Memo3,'ABPubVarU ABExceptLogFile     '+ABExceptLogFile     );
  Addlog(Memo3,'ABPubVarU ABLogFile           '+ABLogFile           );
  Addlog(Memo3,'ABPubVarU ABBackPath          '+ABBackPath          );
  Addlog(Memo3,'ABPubVarU ABLanguagesPath     '+ABLanguagesPath     );
  Addlog(Memo3,'ABPubVarU ABOtherPath         '+ABOtherPath         );
  Addlog(Memo3,'ABPubVarU ABTempPath          '+ABTempPath          );
  Addlog(Memo3,'ABPubVarU ABUserFile          '+ABUserFile          );
  Addlog(Memo3,'ABPubVarU ABLocalVersionsFile '+ABLocalVersionsFile );
  Addlog(Memo3,'ABPubVarU ABStrDataType       '+ABSetToStr(TypeInfo(TFieldTypes),ABStrDataType));
  Addlog(Memo3,'ABPubVarU ABMemoDataType      '+ABSetToStr(TypeInfo(TFieldTypes),ABMemoDataType));
  Addlog(Memo3,'ABPubVarU ABIntAndDecimalDataType  '+ABSetToStr(TypeInfo(TFieldTypes),ABIntAndDecimalDataType));
  Addlog(Memo3,'ABPubVarU ABBlobDataType      '+ABSetToStr(TypeInfo(TFieldTypes),ABBlobDataType));
  Addlog(Memo3,'ABPubVarU ABStrAndMemoDataType'+ABSetToStr(TypeInfo(TFieldTypes),ABStrAndMemoDataType));
  Addlog(Memo3,'ABPubVarU ABInDelphi'+ABBoolToStr(ABInDelphi));
  Addlog(Memo3,'ABPubVarU ABPubEnvironments'+ABGetEnvironmentList.text);
  if Assigned(ABPubParamDataset) then
    Addlog(Memo3,'ABPubVarU ABPubParamDataset'+ABPubParamDataset.ToString);
  Addlog(Memo3,'ABPubVarU ABPubParamNameFieldName'+ABPubParamNameFieldName);
  Addlog(Memo3,'ABPubVarU ABPubParamValueFieldName'+ABPubParamValueFieldName);
  if Assigned(ABPubReplaceSQLDataset) then
    Addlog(Memo3,'ABPubVarU ABPubReplaceSQLDataset'+ABPubReplaceSQLDataset.ToString);
  Addlog(Memo3,'ABPubVarU ABPubReplaceSQLNameFieldName'+ABPubReplaceSQLNameFieldName);
  Addlog(Memo3,'ABPubVarU ABPubReplaceSQLValueFieldName'+ABPubReplaceSQLValueFieldName);
end;

procedure TABTestForm.Button27Click(Sender: TObject);
var
  tempStr: string;
begin
  tempStr := '2';
  if ABSelectComboBox(tempStr, FDataSet, 'Code', 'Name','选择数据集字段值(已选择2)') then
  begin
    Addlog(Memo3,'ABPubSelectComboBoxU ABSelectComboBox[选择数据集中的字段值]'+tempStr);
  end;
  tempStr := 'Name';
  if ABSelectComboBox(tempStr, FDataSet, '选择数据集字段名(已选择Name)') then
  begin
    Addlog(Memo3,'ABPubSelectComboBoxU ABSelectComboBox[选择数据集字段名]'+tempStr);
  end;
end;

procedure TABTestForm.Button53Click(Sender: TObject);
var
  tempValue:string;
begin
  tempValue:='aa=11';
  temStrings.Clear;
  temStrings_1.Clear;
  temStrings.Add('aa');
  temStrings.Add('bb');
  temStrings.Add('cc');
  temStrings_1.Add('11');
  temStrings_1.Add('22');
  temStrings_1.Add('33');

  if ABSelectNameAndValues(temStrings,
                         temStrings_1,
                         tempValue) then
  begin
    Addlog(Memo3,'ABPubSelectNameAndValuesU ABSelectNameAndValues[在来源与目标列表中选择对应项]'+tempValue);
  end;
end;

procedure TABTestForm.Button28Click(Sender: TObject);
var
  tempStr: string;
begin
  tempStr := '2';
  if ABSelectCheckComboBox(tempStr, FDataSet, 'Code','Code',  'Name','选择数据集字段值(已选择2)') then
  begin
    Addlog(Memo3,'ABPubSelectCheckComboBoxU ABSelectCheckComboBox[选择数据集中的字段值]'+tempStr);
  end;
  tempStr := 'Name';
  if ABSelectCheckComboBox(tempStr, FDataSet, '选择数据集字段名(已选择Name)') then
  begin
    Addlog(Memo3,'ABPubSelectCheckComboBoxU ABSelectCheckComboBox[选择数据集字段名]'+tempStr);
  end;
end;

procedure TABTestForm.Button29Click(Sender: TObject);
var
  tempStr, tempStr1: string;
  tempStrings: Tstrings;
begin
  tempStrings := Tstringlist.Create;
  try
    tempStr := 'Name';
    if ABSelectFieldNames(tempStr, FDataSet,'', ',', '选择数据集字段名(已选择Name)') then
    begin
      Addlog(Memo3,'ABPubSelectStrU ABSelectFieldNames[选择数据集字段名]'+tempStr);
    end;

    if ABSelectFieldNamesToFieldValue(FDataSet.FieldByName('Name'),0,0,'', ',', '选择字段名到字段的值中') then
    begin
      Addlog(Memo3,'ABPubSelectStrU ABSelectFieldNamesToFieldValue[选择字段名到字段的值中]'+FDataSet.FieldByName('Name').AsString);
    end;

    tempStr := '张三';
    if ABSelectFieldValue(tempStr, FDataSet.FieldByName('Name'), ',', '选择数据集字段的字段值(已选择张三)') then
    begin
      Addlog(Memo3,'ABPubSelectStrU ABSelectFieldValue[选择数据集字段的字段值]'+tempStr);
    end;
    tempStrings.Add('张三');
    if ABSelectFieldValue(tempStrings,FDataSet.FieldByName('Name'), ',', '选择数据集字段的字段值(已选择张三)') then
    begin
      Addlog(Memo3,'ABPubSelectStrU ABSelectFieldValue[选择数据集字段的字段值]'+tempStrings.Text);
    end;

    tempStr := '1,2,3,4,5';
    tempStr1 := '6,7,8,9,10';
    if ABSelectStr(tempStr, tempStr1, ',', '选择字串，左边1~5，右边6~10') then
    begin
      Addlog(Memo3,'ABPubSelectStrU ABSelectStr[选择字串]'+tempStr1);
    end;
  finally
    tempStrings.Free;
  end;
end;

procedure TABTestForm.Button32Click(Sender: TObject);
begin
  ABShowSockDemoForm;
  Addlog(Memo3,'ABPubSockDemoU ABShowSockDemoForm');
end;

procedure TABTestForm.Button45Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubIndyU ABSetIDFtpConn[根据FTP的地址，连接TIdFTP ftp://uploads:@uploads.2ccc.com/]'+ABBoolToStr(ABSetIDFtpConn('ftp://uploads:@uploads.2ccc.com/', IdFTP1)));
  Addlog(Memo3,'ABPubIndyU ABGetWANIP[到外网上跑一下，返回外网的IP http://ipseeker.cn/]'+ABGetWANIP('http://ipseeker.cn/', IdHTTP1));
  Addlog(Memo3,'ABPubIndyU ABUpToFtp[通过FTP更新本地文件到FTP服务器器当前目录下 ABPubTestG.dpr,aaaa.dpr]'+ABBoolToStr(ABUpToFtp(FNeedFilePath + 'ABPubTestG.dpr', 'aaaa.dpr', IdFTP1)));
  Addlog(Memo3,'ABPubIndyU ABDownFromFTP[通过FTP下载FTP服务器器当前目录下文件到本地 aaaa.dpr,1.1.1.111, '', aaaa.dpr]'+ABBoolToStr(ABDownFromFTP('aaaa.dpr', '1.1.1.111', '', 'aaaa.dpr', IdFTP1)));
end;

procedure TABTestForm.ABCheckedComboBox1Change(Sender: TObject);
begin
//
end;

procedure TABTestForm.ABScrollBox1HScroll(var Message: TWMScroll);
begin
  Addlog(Memo3,'ABPubScrollBoxU HScroll');
end;

procedure TABTestForm.ABScrollBox1VScroll(var Message: TWMScroll);
begin
  Addlog(Memo3,'ABPubScrollBoxU VScroll');
end;

procedure TABTestForm.Button51Click(Sender: TObject);
var
  tempServiceInfo: TABServiceInfo;
begin
  Addlog(Memo3,'ABPubServiceU ABServiceIsInstall[判断服务是否安装]'+ABBoolToStr(ABServiceIsInstall('ABSoftIPRegister')));
  Addlog(Memo3,'ABPubServiceU ABServiceIsRunning[判断服务是否运行中]'+ABBoolToStr(ABServiceIsRunning('ABSoftIPRegister')));
  Addlog(Memo3,'ABPubServiceU ABGetServiceStatus[取得服务当前的状态]'+GetEnumName(TypeInfo(TABServiceState), Ord(ABGetServiceStatus('ABSoftIPRegister'))));

  tempServiceInfo.aServiceName    := ABSoftName+'IPRegister';
  tempServiceInfo.aDisplayName    := ABSoftName+' IP Register';
  tempServiceInfo.DesireAccess    := 0;
  tempServiceInfo.ServiceType     := 0;
  tempServiceInfo.StartType       := 0;
  tempServiceInfo.ErrorControl    := 0;
  tempServiceInfo.BinaryPathName  := PChar(FNeedFilePath + 'ServiceSample\ServiceSample.exe');
  tempServiceInfo.LoadOrderGroup  := nil;
  tempServiceInfo.TagId           := nil;
  tempServiceInfo.Dependencies    := nil;
  tempServiceInfo.ServerStartName := nil;
  tempServiceInfo.Password        := nil;
  Addlog(Memo3,'ABPubServiceU ABInstallService[API方式安装服务]'+ABBoolToStr(ABInstallService(tempServiceInfo)));
  Addlog(Memo3,'ABPubServiceU ABStopService[API方式停止服务]'+ABBoolToStr(ABStopService('ABSoftIPRegister')));
  Addlog(Memo3,'ABPubServiceU ABStartService[API方式启动服务]'+ABBoolToStr(ABStartService('ABSoftIPRegister')));

  ABSetServiceDescription('ABSoftIPRegister','注册三层服务程序IP地址');
  Addlog(Memo3,'ABPubServiceU ABSetServiceDescription[增加服务描述]');

  Addlog(Memo3,'ABPubServiceU ABServiceIsInstall[判断服务是否安装]'+ABBoolToStr(ABServiceIsInstall('ABSoftIPRegister')));
  Addlog(Memo3,'ABPubServiceU ABServiceIsRunning[判断服务是否运行中]'+ABBoolToStr(ABServiceIsRunning('ABSoftIPRegister')));
  Addlog(Memo3,'ABPubServiceU ABGetServiceStatus[取得服务当前的状态]'+GetEnumName(TypeInfo(TABServiceState), Ord(ABGetServiceStatus('ABSoftIPRegister'))));

  ABGetSqlServerList(temStrings);
  Addlog(Memo3,'ABPubServiceU ABGetSqlServerList[得到局域网中的SQL SERVER列表]'+temStrings.Text);

  Addlog(Memo3,'ABPubServiceU ABUninstallService[API方式删除服务]'+ABBoolToStr(ABUninstallService('ABSoftIPRegister')));
  Addlog(Memo3,'ABPubServiceU ABInstallService_Dos[DOS方式安装服务]'+ABBoolToStr(ABInstallService_Dos('ABSoftIPRegister',FNeedFilePath + 'ServiceSample\ServiceSample.exe',true)));
  Addlog(Memo3,'ABPubServiceU ABUninstallService[API方式删除服务]'+ABBoolToStr(ABUninstallService('ABSoftIPRegister')));
end;

procedure TABTestForm.Button50Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubRegisterFileU LastRunFileBeginTime'+ABDateTimeToStr(ABRegisterFile.LastRunFileBeginTime));
  Addlog(Memo3,'ABPubRegisterFileU LastRunFileEndTime'+ABDateTimeToStr(ABRegisterFile.LastRunFileEndTime));
  
  Addlog(Memo3,'ABPubRegisterFileU GetVersion[C:\WINDOWS\system32\calc.exe]'+ABRegisterFile.GetVersion('C:\WINDOWS\system32\calc.exe'));
  Addlog(Memo3,'ABPubRegisterFileU IsDown[C:\WINDOWS\system32\calc.exe]'+ABBoolToStr(ABRegisterFile.IsDown('C:\WINDOWS\system32\calc.exe','','')));

  Addlog(Memo3,'ABPubRegisterFileU IsUp[C:\WINDOWS\system32\calc.exe]'+ABBoolToStr(ABRegisterFile.IsUp('C:\WINDOWS\system32\calc.exe','')));
  Addlog(Memo3,'ABPubRegisterFileU LoadRun[C:\WINDOWS\system32\calc.exe]'+ABBoolToStr(ABRegisterFile.LoadRun('C:\WINDOWS\system32\calc.exe')));

  Addlog(Memo3,'ABPubRegisterFileU GetFormNameByFileName[aa]'+ABRegisterFile.GetFormNameByFileName('aa'));
  Addlog(Memo3,'ABPubRegisterFileU GetGuidByFileName[aa]'+ABRegisterFile.GetGuidByFileName('aa'));
end;

procedure TABTestForm.Button8Click(Sender: TObject);
begin
  ABLanguage.ReFreshMenuItems(MainMenu1.Items[0]);
  Addlog(Memo3,'ABPubLanguageU ReFreshMenuItems 主菜单中创建多语言菜单');

  ABLanguage.SetComponentsText(self);
  ABLanguage.SetComponentText(self.Name,Button8.Name,Button8);
  Addlog(Memo3,'ABPubLanguageU SetComponentsText 根据当前语言设置窗体与其中控件的字符串');

  ABLanguage.SetCollectionText(self.Name,FDataSet.Params);
  Addlog(Memo3,'ABPubLanguageU SetCollectionText 设置集合的字项翻译');

  Addlog(Memo3,'ABPubLanguageU [GetPropertyStr 根据当前语言取得字符串用于DFM文件]'+ABLanguage.GetPropertyStr(self.Name,Button8.Name,'Caption','中国人'));

  Addlog(Memo3,'ABPubLanguageU [GetCodeStr 根据当前语言取得字符串用于PAS文件]'+ABLanguage.GetCodeStr('中国人'));
  Addlog(Memo3,'ABPubLanguageU [GetCodeStr 根据当前语言取得字符串用于PAS文件]'+ABLanguage.GetCodeStr('中国人[%s]',['中文']));

  Addlog(Memo3,'ABPubLanguageU [FormsConrtosPropertys 用于保存DFM窗体中的组件属性值]'+ABLanguage.FormsConrtosPropertys.text);
  Addlog(Memo3,'ABPubLanguageU [Items 语言内容的INI资源文件]'+ABLanguage.Items.text);
  Addlog(Memo3,'ABPubLanguageU [Language 当前语言名]'+ABLanguage.Language);
  Addlog(Memo3,'ABPubLanguageU [LanguageFileName 当前保存语所字串的INI文件名]'+ABLanguage.LanguageFileName);

  Addlog(Memo3,'ABPubLanguageU [ExtTransitionPropertyNames 扩展翻译属性列表]'+ABLanguage.ExtTransitionPropertyNames.text);
  Addlog(Memo3,'ABPubLanguageU [NotTransitionFormNames 不需要翻译的窗体名]'+ABLanguage.NotTransitionFormNames.text);
  Addlog(Memo3,'ABPubLanguageU [NotTransitionFormComponentNames 不需要翻译的控件名]'+ABLanguage.NotTransitionFormComponentNames.text);
  Addlog(Memo3,'ABPubLanguageU [NotTransitionFormComponentPropertyNames 不需要翻译的控件属性名]'+ABLanguage.NotTransitionFormComponentPropertyNames.text);
end;

procedure TABTestForm.Button17Click(Sender: TObject);
begin
  ABLanguageEdit;
  Addlog(Memo3,'ABPubLanguageEditU ABLanguageEdit 多语言管理');
end;

procedure TABTestForm.Button49Click(Sender: TObject);
begin
  ABSetLocalParamsValue('Debug', 'False');
  Addlog(Memo3,'ABPubLocalParamsU ABSetLocalParamsValue[Debug,False] 设置地参数值');

  Addlog(Memo3,'ABPubLocalParamsU [ParamsFile             ]'+            ABLocalParamsFile             );
  Addlog(Memo3,'ABPubLocalParamsU [HaveFileBeforeRun      ]'+ABBoolToStr(ABLocalParams.HaveFileBeforeRun      ));
  Addlog(Memo3,'ABPubLocalParamsU [LoginType              ]'+GetEnumName(TypeInfo(TABLoginType),ord(ABLocalParams.LoginType)));
  Addlog(Memo3,'ABPubLocalParamsU [ShowStartForm          ]'+ABBoolToStr(ABLocalParams.ShowStartForm          ));
  Addlog(Memo3,'ABPubLocalParamsU [Debug                  ]'+ABBoolToStr(ABLocalParams.Debug                  ));
  Addlog(Memo3,'ABPubLocalParamsU [FuncUpdateCount        ]'+inttostr(   ABLocalParams.FuncUpdateCount        ));
  Addlog(Memo3,'ABPubLocalParamsU [SockLocalPort          ]'+inttostr(   ABLocalParams.SockLocalPort          ));
  Addlog(Memo3,'ABPubLocalParamsU [SockToPort             ]'+inttostr(   ABLocalParams.SockToPort             ));
  Addlog(Memo3,'ABPubLocalParamsU [Language               ]'+            ABLocalParams.Language               );
  Addlog(Memo3,'ABPubLocalParamsU [LanguageEnabled        ]'+ABBoolToStr(ABLocalParams.LanguageEnabled        ));
  Addlog(Memo3,'ABPubLocalParamsU [CurSkinName            ]'+            ABLocalParams.CurSkinName            );
  Addlog(Memo3,'ABPubLocalParamsU [BackType               ]'+GetEnumName(TypeInfo(TABBackType)   ,ord(ABLocalParams.BackType               )));
  Addlog(Memo3,'ABPubLocalParamsU [SourceCodeChineseType  ]'+GetEnumName(TypeInfo(TABChineseType),ord(ABLocalParams.SourceCodeChineseType  )));
  Addlog(Memo3,'ABPubLocalParamsU [PriLanguageChineseType ]'+GetEnumName(TypeInfo(TABChineseType),ord(ABLocalParams.PriLanguageChineseType )));
  Addlog(Memo3,'ABPubLocalParamsU [LanguageChineseType    ]'+GetEnumName(TypeInfo(TABChineseType),ord(ABLocalParams.LanguageChineseType    )));

  Addlog(Memo3,'ABPubLocalParamsU [ParamsFile             ]'+            ABLocalParamsFile             );
  Addlog(Memo3,'ABPubLocalParamsU [HaveFileBeforeRun      ]'+ABBoolToStr(ABLocalParams.HaveFileBeforeRun));
  Addlog(Memo3,'ABPubLocalParamsU [LoginType              ]'+GetEnumName(TypeInfo(TABLoginType),ord(ABLocalParams.LoginType)));
  Addlog(Memo3,'ABPubLocalParamsU [ShowStartForm          ]'+ABBoolToStr(ABLocalParams.ShowStartForm          ));
  Addlog(Memo3,'ABPubLocalParamsU [Debug                  ]'+ABBoolToStr(ABLocalParams.Debug                  ));
  Addlog(Memo3,'ABPubLocalParamsU [FuncUpdateCount        ]'+inttostr(   ABLocalParams.FuncUpdateCount        ));
  Addlog(Memo3,'ABPubLocalParamsU [SockLocalPort          ]'+inttostr(   ABLocalParams.SockLocalPort          ));
  Addlog(Memo3,'ABPubLocalParamsU [SockToPort             ]'+inttostr(   ABLocalParams.SockToPort             ));
  Addlog(Memo3,'ABPubLocalParamsU [Language               ]'+            ABLocalParams.Language               );
  Addlog(Memo3,'ABPubLocalParamsU [LanguageEnabled        ]'+ABBoolToStr(ABLocalParams.LanguageEnabled        ));
  Addlog(Memo3,'ABPubLocalParamsU [CurSkinName            ]'+            ABLocalParams.CurSkinName            );
  Addlog(Memo3,'ABPubLocalParamsU [BackType               ]'+GetEnumName(TypeInfo(TABBackType)   ,ord(ABLocalParams.BackType               )));
  Addlog(Memo3,'ABPubLocalParamsU [SourceCodeChineseType  ]'+GetEnumName(TypeInfo(TABChineseType),ord(ABLocalParams.SourceCodeChineseType  )));
  Addlog(Memo3,'ABPubLocalParamsU [PriLanguageChineseType ]'+GetEnumName(TypeInfo(TABChineseType),ord(ABLocalParams.PriLanguageChineseType )));
  Addlog(Memo3,'ABPubLocalParamsU [LanguageChineseType    ]'+GetEnumName(TypeInfo(TABChineseType),ord(ABLocalParams.LanguageChineseType    )));
end;

procedure TABTestForm.Button9Click(Sender: TObject);
begin
  ABLocalParamsEdit;
  Addlog(Memo3,'ABPubLocalParamsEditU ABLocalParamsEdit 多语言管理');
end;

procedure TABTestForm.Button39Click(Sender: TObject);
var
  i: longint;
begin
  Addlog(Memo3,'ABPubThreadU ABGetMutex 根据名称到到已创建的互斥量[001001]'+inttostr(ABGetMutex('001001')));
  ABCreateMutex('001001');
  Addlog(Memo3,'ABPubThreadU ABCreateMutex 根据名称创建互斥量');
  Addlog(Memo3,'ABPubThreadU ABGetMutex 根据名称到到已创建的互斥量[001001]'+inttostr(ABGetMutex('001001')));
  ABCloseMutex('001001');
  Addlog(Memo3,'ABPubThreadU ABCloseMutex 根据名称关闭互斥量');
  Addlog(Memo3,'ABPubThreadU ABGetMutex 根据名称到到已创建的互斥量[001001]'+inttostr(ABGetMutex('001001')));

  ABEnterCriticalSection;
  Addlog(Memo3,'ABPubThreadU ABEnterCriticalSection 进入临界区');
  ABLeaveCriticalSection;
  Addlog(Memo3,'ABPubThreadU ABLeaveCriticalSection 离开临界区');

  //抢用ABPublicSetPath+'ConnDatabaseSetup.ini'测试
  for i := 1 to 5 do
  begin
    ABDeleteFile(FNeedFilePath+ABAppName+'_'+inttostr(i)+ '.exe');
  end;

  for i := 1 to 5 do
  begin
    if not ABCheckFileExists(FNeedFilePath+ABAppName+'_'+inttostr(i)+ '.exe') then
    begin
      ABCopyFile(ABAppFullName, FNeedFilePath+ABAppName+'_'+inttostr(i)+ '.exe');
    end;
  end;

  for i := 1 to 5 do
  begin
    if ABCheckFileExists(FNeedFilePath+ABAppName+'_'+inttostr(i)+ '.exe') then
    begin
      ShellExecute(Application.Handle, nil,PChar(FNeedFilePath+ABAppName+'_'+inttostr(i)+ '.exe'), nil, nil,SW_SHOW);
    end;
  end;
end;

procedure TABTestForm.Button44Click(Sender: TObject);
begin
  ABShowThreadDemoForm;
  Addlog(Memo3,'ABPubThreadDemoU ABShowThreadDemoForm');
end;

procedure TABTestForm.Button42Click(Sender: TObject);
begin
  ABShowEditDataset(FDataSet);
  Addlog(Memo3,'ABPubShowEditDatasetU ABShowEditDataset');
end;

procedure TABTestForm.Button33Click(Sender: TObject);
begin
  ABShowEditFieldValue(FDataSet);
  Addlog(Memo3,'ABPubShowEditFieldValueU ABShowEditFieldValue');
end;

procedure TABTestForm.Button41Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubUserU ABPubUser.Guid                :'+ABPubUser.Guid                );
  Addlog(Memo3,'ABPubUserU ABPubUser.Code                :'+ABPubUser.Code                );
  Addlog(Memo3,'ABPubUserU ABPubUser.Name                :'+ABPubUser.Name                );
  Addlog(Memo3,'ABPubUserU ABPubUser.PassWord            :'+ABPubUser.PassWord            );
  Addlog(Memo3,'ABPubUserU ABPubUser.EncryptPassWord     :'+ABPubUser.EncryptPassWord     );
  Addlog(Memo3,'ABPubUserU ABPubUser.LoginDateTime       :'+ABDateTimeToStr(ABPubUser.LoginDateTime       ));
  Addlog(Memo3,'ABPubUserU ABPubUser.LoginOutDateTime    :'+ABDateTimeToStr(ABPubUser.LoginOutDateTime    ));
  Addlog(Memo3,'ABPubUserU ABPubUser.HostName            :'+ABPubUser.HostName            );
  Addlog(Memo3,'ABPubUserU ABPubUser.HostIP              :'+ABPubUser.HostIP              );
  Addlog(Memo3,'ABPubUserU ABPubUser.MacAddress          :'+ABPubUser.MacAddress          );
  Addlog(Memo3,'ABPubUserU ABPubUser.SPID                :'+ABPubUser.SPID                );
  Addlog(Memo3,'ABPubUserU ABPubUser.BroadIP             :'+ABPubUser.BroadIP             );
  Addlog(Memo3,'ABPubUserU ABPubUser.UseBeginDateTime    :'+ABDateTimeToStr(ABPubUser.UseBeginDateTime    ));
  Addlog(Memo3,'ABPubUserU ABPubUser.UseEndDateTime      :'+ABDateTimeToStr(ABPubUser.UseEndDateTime      ));
  Addlog(Memo3,'ABPubUserU ABPubUser.LicenseBeginDateTime:'+ABDateTimeToStr(ABPubUser.LicenseBeginDateTime));
  Addlog(Memo3,'ABPubUserU ABPubUser.LicenseEndDateTime  :'+ABDateTimeToStr(ABPubUser.LicenseEndDateTime  ));
  Addlog(Memo3,'ABPubUserU ABPubUser.PassWordEndDateTime :'+ABDateTimeToStr(ABPubUser.PassWordEndDateTime ));
  Addlog(Memo3,'ABPubUserU ABPubUser.UseFuncRight        :'+ABBoolToStr(ABPubUser.UseFuncRight        ));
  Addlog(Memo3,'ABPubUserU ABPubUser.FuncRight           :'+ABPubUser.FuncRight           );
  Addlog(Memo3,'ABPubUserU ABPubUser.CustomFunc          :'+ABPubUser.CustomFunc          );
  Addlog(Memo3,'ABPubUserU ABPubUser.VarGuid             :'+ABPubUser.VarGuid             );
  Addlog(Memo3,'ABPubUserU ABPubUser.VarDatetime         :'+ABDateTimeToStr(ABPubUser.VarDatetime         ));
  Addlog(Memo3,'ABPubUserU ABPubUser.IsAdmin             :'+ABBoolToStr(ABPubUser.IsAdmin             ));
  Addlog(Memo3,'ABPubUserU ABPubUser.IsSysuser           :'+ABBoolToStr(ABPubUser.IsSysuser           ));
  Addlog(Memo3,'ABPubUserU ABPubUser.IsAdminOrSysuser    :'+ABBoolToStr(ABPubUser.IsAdminOrSysuser    ));
end;

procedure TABTestForm.Button38Click(Sender: TObject);
var
  tempStr: string;
begin
  tempStr := ABReadTxt(ABLogFile);
  Addlog(Memo3,'ABPubLoginU ABLogFile'+tempStr);
  tempStr := ABReadTxt(ABExceptLogFile);
  Addlog(Memo3,'ABPubLoginU ABExceptLogFile'+tempStr);

  ABWriteLog('LogMessage1');
  ABWriteLog('LogMessage2',True,true);
  ABWriteLog('LogMessage3',True,true,ABLogFile);

  Addlog(Memo3,'ABPubLoginU ABWriteExceptLog 记录异常日志');
  Addlog(Memo3,'ABPubLoginU ABWriteLog 记录普通日志');

  tempStr := ABReadTxt(ABLogFile);
  Addlog(Memo3,'ABPubLoginU ABLogFile'+tempStr);
  tempStr := ABReadTxt(ABExceptLogFile);
  Addlog(Memo3,'ABPubLoginU ABExceptLogFile'+tempStr);
end;

procedure TABTestForm.Button40Click(Sender: TObject);
var
  tempForm: TABPubLoginForm;
begin
  tempForm := TABPubLoginForm.Create(nil);
  try
    tempForm.ShowModal;
  finally
    tempForm.Free;
  end;
  Addlog(Memo3,'ABPubLoginU 显示登录窗体');
end;

procedure TABTestForm.Button37Click(Sender: TObject);
begin
  abshow('显示字串');
  abshow('显示字串有参数+两个按钮+备注+定时 [%s]', ['bb'], ['确认', '取消'], 2,'aaaaaaaaaaaaaaaaabbbbbbbbb', '标题', 5);
  abshow('显示字串有参数+单个按钮+备注+定时 [%s]', ['bb'], 'aaaaaaaaaaaaaaaaabbbbbbbbb', '标题', 5);
  abshow('显示字串无参数+单个按钮+备注+定时','aaaaaaaaaaaaaaaaabbbbbbbbb', '标题', 5);
  ABShow_WindowsMessage('调用MessageBoxExW显示字符消息框 [%s]', ['bb']);

  Addlog(Memo3,'ABPubMessageU abshow 显示字串有参数、多按钮、有备注、可定时的消息框, 最多5个按钮');
  Addlog(Memo3,'ABPubMessageU abshow 只有一个按钮、显示字串有参数、有备注、可定时的消息框');
  Addlog(Memo3,'ABPubMessageU abshow 只有一个按钮、显示字串无参数、有备注、可定时的消息框');
  Addlog(Memo3,'ABPubMessageU abshow 调用MessageBoxExW显示字符消息框');
end;

procedure TABTestForm.Button35Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubInPutPassWordU ABInputPassWord '+ABBoolToStr(ABInputPassWord('输入测试密码')));
end;

procedure TABTestForm.Button34Click(Sender: TObject);
var
  tempStr: string;
begin
  if ABInPutStr('输入', tempStr, '一串描述', '标题') then
  begin
    Addlog(Memo3,'ABPubInPutStrU ABInPutStr '+tempStr);
  end;
  if ABInPutStr('输入', tempStr) then
  begin
    Addlog(Memo3,'ABPubInPutStrU ABInPutStr '+tempStr);
  end;
end;

procedure TABTestForm.Button31Click(Sender: TObject);
var
  tempNewPass: string;
begin
  if ABEditPassWord('', tempNewPass) then
  begin
    Addlog(Memo3,'ABPubEditPassWordU ABEditPassWord 修改密码'+tempNewPass);
  end;
end;

procedure TABTestForm.Button30Click(Sender: TObject);
var
  tempStr1: string;
begin
  tempStr1 := emptystr;
  if ABOrderFieldNames(tempStr1, FDataSet, ',', '字段名排序') then
  begin
    Addlog(Memo3,'ABPubOrderStrU ABOrderFieldNames 字段名排序'+tempStr1);
  end;
  tempStr1 := '1,2,3,4,5,6';
  if ABOrderStrs(tempStr1, ',', '字串排序') then
  begin
    Addlog(Memo3,'ABPubOrderStrU ABOrderStrs 字串排序'+tempStr1);
  end;
end;

procedure TABTestForm.Button20Click(Sender: TObject);
var
  tempStr: string;
begin
  tempStr := FDataSet.FieldByName('Remark').AsString;
  ABShowEditMemo(tempStr, True, '显示备注控件');
  if ABShowEditMemo(tempStr, False, '编辑备注控件') then
  begin
    ABSetFieldValue(tempStr, FDataSet.FieldByName('Remark'));
  end;

  Addlog(Memo3,'ABPubMemoU ABShowEditMemo 显示备注控件');
  Addlog(Memo3,'ABPubMemoU ABShowEditMemo 编辑备注控件');
end;

procedure TABTestForm.Button21Click(Sender: TObject);
begin
  ABShowEditMemoField(FDataSet, 'Remark', True, '显示备注字段');
  ABShowEditMemoField(FDataSet, 'Remark', False, '编辑备注字段');

  Addlog(Memo3,'ABPubMemoFieldU ABShowEditMemoField 显示备注字段');
  Addlog(Memo3,'ABPubMemoFieldU ABShowEditMemoField 编辑备注字段');
end;

procedure TABTestForm.Button19Click(Sender: TObject);
begin
  ABShowEditImage(Image1.Picture, True, '显示照片控件');
  ABShowEditImage(Image2.Picture, False, '编辑照片控件');

  Addlog(Memo3,'ABPubImageU ABShowEditImage 显示照片控件');
  Addlog(Memo3,'ABPubImageU ABShowEditImage 编辑照片控件');
end;

procedure TABTestForm.Button18Click(Sender: TObject);
begin
  if FDataSet.Locate('Code','1',[]) then
  begin
    ABShowEditImageField(FDataSet, 'photo_bmp', True, '显示照片字段');
    ABShowEditImageField(FDataSet, 'photo_bmp', False, '编辑照片字段');
  end;
  Addlog(Memo3,'ABPubImageFieldU ABShowEditImageField 显示照片字段');
  Addlog(Memo3,'ABPubImageFieldU ABShowEditImageField 编辑照片字段');
end;

procedure TABTestForm.Button10Click(Sender: TObject);
begin
  with TButton.Create(Panel8) do
  begin
    Parent:= Panel8;
    left:= 48;
    top:= 280;
  end;
  ABBeginDesignClientOfOwner(self,Panel8);
  Addlog(Memo3,'ABPubDesignU ABBeginDesignClientOfOwner 开始设计');
end;

procedure TABTestForm.Button22Click(Sender: TObject);
begin
  ABEndDesign(self);
  Addlog(Memo3,'ABPubDesignU ABEndDesign 结束设计');
end;

procedure TABTestForm.Button5Click(Sender: TObject);
var
  i:LongInt;
begin
  for I := 1 to 10 do
  begin
    ABLed1.Value := not ABLed1.Value;
    ABLed2.Value := not ABLed2.Value;
    ABLed3.Value := not ABLed3.Value;
    ABLed4.Value := not ABLed4.Value;
    ABLed5.Value := not ABLed5.Value;
    ABLed6.Value := not ABLed6.Value;
    ABSleep(100);
    Addlog(Memo3,'ABPubLedU ABLed1.Value := not ABLed1.Value ');
  end;
end;

procedure TABTestForm.Button60Click(Sender: TObject);
var
  I: Integer;
begin
  ABItemList1.Frame.ListBox1.Items.Clear;
  for I := 0 to 5 do
    ABItemList1.Frame.ListBox1.Items.Add('aa'+inttostr(i));
end;

procedure TABTestForm.Button61Click(Sender: TObject);
var
  I: Integer;
begin
  ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.Clear;
  for I := 0 to 5 do
    ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.Add('aa'+inttostr(i));
end;

procedure TABTestForm.Button63Click(Sender: TObject);
begin
  ABBeginDesignClientOfParent(self,Panel8);
  Addlog(Memo3,'ABPubDesignU ABBeginDesignClientOfParent 开始设计');

end;

procedure TABTestForm.Button6Click(Sender: TObject);
begin
  Memo3.Clear;
  Memo4.Clear;
end;

procedure TABTestForm.Button43Click(Sender: TObject);
var
  tempStr: string;
  tempSQLParseDef2: TABSQLParseDef;
begin
  tempStr := ' declare @tempi int ' + ABEnterWrapStr+
             ' SELECT Pe_Ta_Guid,TA_Guid' + ABEnterWrapStr+
             ' FROM dbo.testPeople INNER JOIN' +ABEnterWrapStr+
             '      dbo.ABSys_Org_Table ON' +ABEnterWrapStr+
             '      dbo.testPeople.Pe_Ta_Guid = dbo.ABSys_Org_Table.TA_Guid INNER JOIN' + ABEnterWrapStr+
             '      dbo.ABSys_Org_Field ON dbo.testPeople.Pe_Fi_Guid = dbo.ABSys_Org_Field.FI_Guid'+ABEnterWrapStr+
             ' where Pe_Ta_Guid<>'''' and Pe_name=''CFG001'' ' +ABEnterWrapStr+
             ' Group by Pe_Ta_Guid,TA_Guid ' + ABEnterWrapStr+
             ' Having count(*)>1 ' +ABEnterWrapStr+
             ' Order by Pe_Ta_Guid,TA_Guid ';
  ABPubSQLParse.Parse(tempStr);
  Addlog(Memo3,'ABPubSQLParseU ABPubSQLParse.Parse SQL解析['+tempStr+']');
  Addlog(Memo3,'Select语句的定义部分:'+ABPubSQLParse.SQLParseDef.BeforeSelectStr);
  Addlog(Memo3,'单表时的表名:'+ ABPubSQLParse.SQLParseDef.SingleTableName);
  Addlog(Memo3,'Select 内容:' + ABPubSQLParse.SQLParseDef.SelectStr);
  Addlog(Memo3,'From   内容:' + ABPubSQLParse.SQLParseDef.FromStr);
  Addlog(Memo3,'Where  内容:' + ABPubSQLParse.SQLParseDef.WhereStr);
  Addlog(Memo3,'Group  内容:' + ABPubSQLParse.SQLParseDef.GroupStr);
  Addlog(Memo3,'Having 内容:' + ABPubSQLParse.SQLParseDef.HavingStr);
  Addlog(Memo3,'Order  内容:' + ABPubSQLParse.SQLParseDef.OrderStr);
  Addlog(Memo3,'Compute内容:' + ABPubSQLParse.SQLParseDef.ComputeStr);
  Addlog(Memo3,'Union  内容:' + ABPubSQLParse.SQLParseDef.UnionStr);

  ABPubSQLParse.CopySQLParseDef(ABPubSQLParse.SQLParseDef,@tempSQLParseDef2);
  Addlog(Memo3,'复制aSQLParseDef1到aSQLParseDef2的解析结构:' + tempSQLParseDef2.SQL);

  Addlog(Memo3,'增加查询条件 在内部SQL的基础上增加条件后返回:' + ABPubSQLParse.AddWhere('1=1'));
  Addlog(Memo3,'增加查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'增加查询条件: 在内部SQL的基础上增加条件后返回(不影响原有的SQL与内部结构)' +ABPubSQLParse.AddWhereNoEditOldSQL('2=2'));
  Addlog(Memo3,'增加查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'增加查询条件 增加条件到SQL,返回结果SQL:' +ABPubSQLParse.AddWhere('2=2','select * from aaaa where a=b'));

  Addlog(Memo3,'删除查询条件: 在内部SQL的基础上删除条件后返回' + ABPubSQLParse.DelWhere('Pe_Ta_Guid<>'''''));
  Addlog(Memo3,'删除查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'删除查询条件: 在内部SQL的基础上删除条件后返回(不影响原有的SQL与内部结构)' +ABPubSQLParse.DelWhereNoEditOldSQL('Pe_Ta_Guid<>'''''));
  Addlog(Memo3,'删除查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'删除查询条件: 从SQL删除所有条件,返回结果SQL' +ABPubSQLParse.DelWhere('2=2','select * from aaaa where a=b'));
  Addlog(Memo3,'删除查询条件: 在内部SQL的基础上删除所有条件后返回' +ABPubSQLParse.DelWhere);
  Addlog(Memo3,'删除查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);

  Addlog(Memo3,'增加查询条件 在内部SQL的基础上增加排序:' + ABPubSQLParse.AddOrder('tempa,tempb'));
  Addlog(Memo3,'增加查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'增加查询条件: 在内部SQL的基础上增加排序后返回(不影响原有的SQL与内部结构)' +ABPubSQLParse.AddOrderNoEditOldSQL('tempa,tempb'));
  Addlog(Memo3,'增加查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'增加查询条件 在SQL的基础上增加排序:' +ABPubSQLParse.AddOrder('tempa,tempb','select * from aaaa where a=b'));

  Addlog(Memo3,'删除查询条件: 在内部SQL的基础上删除排序' + ABPubSQLParse.DelOrder('tempa,tempb'));
  Addlog(Memo3,'删除查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'删除查询条件: 在内部SQL的基础上删除排序后返回(不影响原有的SQL与内部结构)' +ABPubSQLParse.DelOrderNoEditOldSQL('tempa,tempb'));
  Addlog(Memo3,'删除查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);
  Addlog(Memo3,'删除查询条件: 在SQL的基础上删除排序' +ABPubSQLParse.DelOrder('tempa,tempb','select * from aaaa where a=b'));
  Addlog(Memo3,'删除查询条件: 在内部SQL的基础上删除所有排序' +ABPubSQLParse.DelOrder);
  Addlog(Memo3,'删除查询条件SQLParseDef.SQL:' + ABPubSQLParse.SQLParseDef.SQL);

  Addlog(Memo3,'组合内部解析结构到SQL.SQL:' + ABPubSQLParse.MakeSQL);
end;

procedure TABTestForm.Button36Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubScriptU ABExecScript 执行Javascript脚本[if (-1 > 0) ''bbbb'']->'+ABExecScript('if (-1 > 0) ''bbbb'' '));
  Addlog(Memo3,'ABPubScriptU ABExecScript 执行Javascript脚本[if (-1 <= 0) ''aaaa'']->'+ABExecScript('if (-1 <= 0) ''aaaa'' '));
  Addlog(Memo3,'ABPubScriptU ABExecScripts 执行批量的Javascript脚本[Begin]if (-1 > 0) ''bbbb''   [End] [Begin]if (-1 <= 0) ''aaaa''  [End]->'+ABBoolToStr(ABExecScripts('[Begin]if (-1 > 0) ''bbbb''   [End] [Begin]if (-1 <= 0) ''aaaa''  [End]')));
end;

procedure TABTestForm.Button13Click(Sender: TObject);
begin
  Edit2.Text := ABDOPassWord(Edit1.Text);
  Addlog(Memo3,'ABPubPassU ABDOPassWord 明->密' +Edit1.Text+'->' +Edit2.Text);
end;

procedure TABTestForm.Button1Click(Sender: TObject);
begin
  Edit2.Text := ABDoRegisterKey(Edit1.Text);
  Addlog(Memo3,'ABPubPassU ABDoRegisterKey 明>注册码' +Edit1.Text+'->' +Edit2.Text);
end;

procedure TABTestForm.btn1Click(Sender: TObject);
begin
  Edit1.Text := ABUnDOPassWord(Edit2.Text);
  Addlog(Memo3,'ABPubPassU ABUnDOPassWord 密>明' +Edit2.Text+'->' +Edit1.Text);
end;

procedure TABTestForm.Button12Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubPassU ABCheckLinkUsbSoftDog 检测加密狗' +ABBoolToStr(ABCheckLinkUsbSoftDog('11111111','11111111',0,5,'3core')));
end;

procedure TABTestForm.Button2Click(Sender: TObject);
var
  tempSubsequentClientCount: longint;
begin
  Addlog(Memo3,'ABPubPassU ABCheckLicense 检测授权文件' +ABBoolToStr(ABCheckLicense(ABPublicSetPath+'License.key',Edit1.Text, now(), tempSubsequentClientCount)));
end;

procedure TABTestForm.Button14Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubPassU ABCheckPassword 检测密文由明文+公钥产生' +ABBoolToStr(ABCheckPassword(Edit1.Text,Edit2.Text)));
end;

procedure TABTestForm.Button48Click(Sender: TObject);
begin
  Addlog(Memo3,'ABPubCharacterCodingU ABBig5ToGB Big5转GB[中国人民共和国]' + ABBig5ToGB('中国人民共和国'));
  Addlog(Memo3,'ABPubCharacterCodingU ABGBToBig5 GB转Big5[中国人民共和国]' + ABGBToBig5('中国人民共和国'));

  Addlog(Memo3,'ABPubCharacterCodingU ABGBChsToGBCht GB简转繁[中国人民共和国]' + ABGBChsToGBCht('中国人民共和国'));
  Addlog(Memo3,'ABPubCharacterCodingU ABGBChtToGBChs GB繁转简[中国人民共和国]' + ABGBChtToGBChs('中国人民共和国'));

  Addlog(Memo3,'ABPubCharacterCodingU ABUnicodeJanToFanD2009及以后简转繁[中国人民共和国]' + ABUnicodeJanToFan('中国人民共和国'));
  Addlog(Memo3,'ABPubCharacterCodingU ABUnicodeFanToJan D2009及以后繁转简[中国人民共和国]' + ABUnicodeFanToJan('中国人民共和国'));

  Addlog(Memo3,'ABPubCharacterCodingU ABDFMToChina DFMW文件中的汉字编码转汉字[中国人民共和国]' +ABDFMToChina('#20013#22269#20154#27665#20849#21644#22269'));
  Addlog(Memo3,'ABPubCharacterCodingU ABChinaToDFM 汉字转DFMW文件中的汉字编码[中国人民共和国]' + ABChinaToDFM('中国人民共和国'));

  Addlog(Memo3,'ABPubCharacterCodingU ABLanguageIsBig5 判断语言是否是繁体[ChinaTw]' + ABBoolToStr(ABLanguageIsBig5('ChinaTw')));
  Addlog(Memo3,'ABPubCharacterCodingU ABLanguageIsBig5 判断语言是否是繁体[China]' + ABBoolToStr(ABLanguageIsBig5('China')));

  Addlog(Memo3,'ABPubCharacterCodingU ABLanguageIsGB 判断语言是否是简体[ChinaTw]' + ABBoolToStr(ABLanguageIsGB('ChinaTw')));
  Addlog(Memo3,'ABPubCharacterCodingU ABLanguageIsGB 判断语言是否是简体[China]' + ABBoolToStr(ABLanguageIsGB('China')));

  Addlog(Memo3,'ABPubCharacterCodingU ABLanguageToChineseType 语言转化为中文字符集类型[China]' + GetEnumName(TypeInfo(TABChineseType),Ord(ABLanguageToChineseType('China'))));
  Addlog(Memo3,'ABPubCharacterCodingU ABLanguageToChineseType 语言转化为中文字符集类型[ChinaTw]' + GetEnumName(TypeInfo(TABChineseType),Ord(ABLanguageToChineseType('ChinaTw'))));

  Addlog(Memo3,'ABPubCharacterCodingU ABGetSysChineseType 取得系统环境的字符集' + GetEnumName(TypeInfo(TABChineseType),Ord(ABGetSysChineseType)));
  Addlog(Memo3,'ABPubCharacterCodingU ABChinesePosition 汉字的开始位置[abcd中国人民共和国]' + inttostr(ABChinesePosition('abcd中国人民共和国')));

  Addlog(Memo3,'ABPubCharacterCodingU ABGetPYWB_List 返回汉字的拼音[中国人民abc共和国]' + ABGetPYWB_List('中国人民abc共和国',1));
  Addlog(Memo3,'ABPubCharacterCodingU ABGetPYWB_List 返回汉字的五笔[中国人民abc共和国]' + ABGetPYWB_List('中国人民abc共和国',2));
  Addlog(Memo3,'ABPubCharacterCodingU ABGetPYWB_List 返回汉字的拼音首字母[中国人民abc共和国]' + ABGetPYWB_List('中国人民abc共和国',3));
  Addlog(Memo3,'ABPubCharacterCodingU ABGetPYWB_List 返回汉字的五笔首字母[中国人民abc共和国]' + ABGetPYWB_List('中国人民abc共和国',4));

  Addlog(Memo3,'ABPubCharacterCodingU ABGetFirstPy 返回汉字的拼音[中国人民abc共和国]' + ABGetFirstPy('中国人民abc共和国'));
  Addlog(Memo3,'ABPubCharacterCodingU ABGetAllPy   返回汉字的五笔[中国人民abc共和国]' + ABGetAllPy('中国人民abc共和国'));
  Addlog(Memo3,'ABPubCharacterCodingU ABGetFirstWB 返回汉字的拼音首字母[中国人民abc共和国]' + ABGetFirstWB('中国人民abc共和国'));
  Addlog(Memo3,'ABPubCharacterCodingU ABGetAllWB   返回汉字的五笔首字母[中国人民abc共和国]' + ABGetAllWB('中国人民abc共和国'));
end;

procedure TABTestForm.Button16Click(Sender: TObject);
begin
  // 2秒欢迎界面
  ABShowStartForm;
  try
    ABSleep(2000);
  finally
    ABCloseStartForm;
  end;
  Addlog(Memo3,'ABPubStartFormU 2秒欢迎界面');
end;

procedure TABTestForm.Button3Click(Sender: TObject);
begin
  // 5秒线程循环进度条
  ABPubAutoProgressBar_ThreadU.ABRunProgressBar('5秒线程循环进度条');
  try
    ABSleep(5000);
  finally
    ABPubAutoProgressBar_ThreadU.ABStopProgressBar;
  end;

  Addlog(Memo3,'ABPubAutoProgressBar_ThreadU 5秒线程循环进度条');
end;

procedure TABTestForm.Button4Click(Sender: TObject);
var
  tempMainMax, tempCur, i: Integer;
begin
  tempCur := 0;
  tempMainMax := 100;
  ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCur, tempMainMax,'线程可控(1~100)进度条');
  try
    for i := 1 to tempMainMax do
    begin
      ABEnterCriticalSection;
      try
        tempCur := i;
      finally
        ABLeaveCriticalSection;
      end;

      ABSleep(1);
      Application.ProcessMessages;
    end;
  finally
    ABPubManualProgressBar_ThreadU.ABStopProgressBar;
  end;
  Addlog(Memo3,'ABPubManualProgressBar_ThreadU 线程可控(1~100)进度条');
end;

procedure TABTestForm.Button7Click(Sender: TObject);
var
  tempMainMax, i: Integer;
begin
  // 主进程内可控单条进度条
  tempMainMax := 100;
  ABGetPubManualProgressBarForm.Title := '主进程内可控单条进度条';
  ABGetPubManualProgressBarForm.MainMin := 0;
  ABGetPubManualProgressBarForm.MainPosition := 0;
  ABGetPubManualProgressBarForm.MainMax := tempMainMax;
  ABGetPubManualProgressBarForm.Run(True);
  try
    for i := 0 to tempMainMax - 1 do
    begin
      ABGetPubManualProgressBarForm.MainPosition :=
        ABGetPubManualProgressBarForm.MainPosition + 1;
      Application.ProcessMessages;

      ABSleep(1);
    end;
  finally
    ABGetPubManualProgressBarForm.Stop;
  end;
  Addlog(Memo3,'ABPubManualProgressBarU 主进程内可控单条进度条');
end;

procedure TABTestForm.Button11Click(Sender: TObject);
var
  tempMainMax, tempDetailMax, i, j: Integer;
begin
  // 主进程内可控主从条进度条
  tempMainMax := 10;
  tempDetailMax := 10;
  ABGetPubManualProgressBarForm.Title := '主进程内可控主从条进度条';
  ABGetPubManualProgressBarForm.MainMin := 0;
  ABGetPubManualProgressBarForm.MainPosition := 0;
  ABGetPubManualProgressBarForm.MainMax := tempMainMax;
  ABGetPubManualProgressBarForm.DetailVisible := True;
  ABGetPubManualProgressBarForm.DetailMin := 0;
  ABGetPubManualProgressBarForm.DetailPosition := 0;
  // 传入true表示执行过程中可以关闭退出
  ABGetPubManualProgressBarForm.Run(True);
  try
    for i := 0 to tempMainMax - 1 do
    begin
      ABGetPubManualProgressBarForm.MainPosition :=
        ABGetPubManualProgressBarForm.MainPosition + 1;
      ABGetPubManualProgressBarForm.DetailMax := tempDetailMax;
      ABGetPubManualProgressBarForm.DetailPosition := 0;
      Application.ProcessMessages;
      for j := 0 to tempDetailMax - 1 do
      begin
        if ABGetPubManualProgressBarForm.ExitRun then
          exit;

        ABGetPubManualProgressBarForm.DetailPosition :=
          ABGetPubManualProgressBarForm.DetailPosition + 1;
        ABSleep(1);
      end;
    end;
  finally
    ABGetPubManualProgressBarForm.DetailVisible := False;
    ABGetPubManualProgressBarForm.Stop;
  end;
  Addlog(Memo3,'ABPubManualProgressBarU 主进程内可控主从条进度条');
end;


procedure TABTestForm.Memo3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      TMemo(Sender).SelectAll;
    end;
  end;
end;

procedure TABTestForm.Addlog(aMemo:TMemo;atext:string);
var
  tempDatetime:TDateTime;
begin
  tempDatetime:=Now;
  aMemo.Lines.Add('时间:'+ABDateTimeToStr(tempDatetime,23)+' '+
                  '距上次日志['+ABRoundStr(ABGetDateTimeSpan_Float(FpriDatetime,tempDatetime,tuMilliSeconds),2,true)+'毫秒] '+
                  atext);
  FpriDatetime:=tempDatetime;
end;

//检测按键是否是注册热键
procedure TABTestForm.WMHotKey(var Msg: TWMHotKey);
begin
  if (Msg.HotKey=FShowHideHotKey_ID) then
  begin
    ShowMessage('WMHotKey FShowHideHotKey_ID');
  end;
end;

procedure TABTestForm.TrackBar1Change(Sender: TObject);
begin
  case RadioGroup2.ItemIndex of
    0:
    begin
      ABBlackWhiteBitmap(Image_begin.Picture.Bitmap,Image_Max.Picture.Bitmap        ,gbtMax,TrackBar1.Position);
      Addlog(Memo3,'ABBlackWhiteBitmap(二值化图片 (gbtMax)):');
      ABBlackWhiteBitmap(Image_begin.Picture.Bitmap,Image_Avg.Picture.Bitmap        ,gbtAvg,TrackBar1.Position);
      Addlog(Memo3,'ABBlackWhiteBitmap(二值化图片 (gbtAvg)):');
      ABBlackWhiteBitmap(Image_begin.Picture.Bitmap,Image_WeightedAvg.Picture.Bitmap,gbtWeightedAvg,TrackBar1.Position);
      Addlog(Memo3,'ABBlackWhiteBitmap(二值化图片 (gbtWeightedAvg)):');
    end;
    1:
    begin
      ABGrayBitmap(Image_begin.Picture.Bitmap,Image_Max.Picture.Bitmap,gbtMax,TrackBar1.Position);
      Addlog(Memo3,'ABGrayBitmap(灰化图片 (gbtMax)):');
      ABGrayBitmap(Image_begin.Picture.Bitmap,Image_Avg.Picture.Bitmap,gbtAvg,TrackBar1.Position);
      Addlog(Memo3,'ABGrayBitmap(灰化图片 (gbtAvg)):');
      ABGrayBitmap(Image_begin.Picture.Bitmap,Image_WeightedAvg.Picture.Bitmap,gbtWeightedAvg,TrackBar1.Position);
      Addlog(Memo3,'ABGrayBitmap(灰化图片 (gbtWeightedAvg)):');
    end;
  end;
end;

procedure TABTestForm.RadioGroup2Click(Sender: TObject);
begin
  case RadioGroup2.ItemIndex of
    0:
    begin
      TrackBar1.Position:=128;
    end;
    1:
    begin
      TrackBar1.Position:=0;
    end;
  end;
end;

procedure TABTestForm.Button52Click(Sender: TObject);
var
  tempWMScroll:TWMScroll;
begin
  Memo3.Clear;
  Button5Click           (Button5);
  Button10Click          (Button10);
  Button22Click          (Button22);
  Button17Click          (Button17);
  Button9Click           (Button9);
  Button13Click          (Button13);
  btn1Click              (btn1);
  Button12Click          (Button12);
  Button1Click           (Button1);
  Button2Click           (Button2);
  Button3Click           (Button3);
  Button4Click           (Button4);
  Button7Click           (Button7);
  Button11Click          (Button11);
  Button16Click          (Button16);
  Button18Click          (Button18);
  Button19Click          (Button19);
  Button21Click          (Button21);
  Button20Click          (Button20);
  Button25Click          (Button25);
  Button26Click          (Button26);
  Button27Click          (Button27);
  Button28Click          (Button28);
  Button29Click          (Button29);
  Button30Click          (Button30);
  Button31Click          (Button31);
  Button33Click          (Button33);
  Button34Click          (Button34);
  Button35Click          (Button35);
  Button36Click          (Button36);
  Button37Click          (Button37);
  Button40Click          (Button40);
  Button39Click          (Button39);
  Button38Click          (Button38);
  Button41Click          (Button41);
  Button42Click          (Button42);
  Button43Click          (Button43);
  Button44Click          (Button44);
  ABScrollBox1HScroll    (tempWMScroll);
  ABScrollBox1VScroll    (tempWMScroll);
  Button32Click          (Button32);
  //Button45Click          (Button45);
  Button47Click          (Button47);
  Button48Click          (Button48);
  Button8Click           (Button8);
  Button49Click          (Button49);
  Button50Click          (Button50);
  Button51Click          (Button51);
  Button53Click          (Button53);
  Button14Click          (Button14);
  Button23Click          (Button23);
  Button24Click          (Button24);
  Button54Click          (Button54);
  Button55Click          (Button55);

  RadioGroup2.ItemIndex:=0;
  TrackBar1Change(TrackBar1);
  RadioGroup2.ItemIndex:=1;
  TrackBar1Change(TrackBar1);
end;

procedure TABTestForm.Button56Click(Sender: TObject);
begin
  ABSQLLog.ExecSQL:=ABSQLLogExecSQL;
  Addlog(Memo3,'ABPubSQLLogU ABSQLLog.ExecSQL:=ABSQLLogExecSQL:');
end;

procedure TABTestForm.Button57Click(Sender: TObject);
begin
  ABDeleteFile(FNeedFilePath + 'all.pak');

  ABWriteTxt(FNeedFilePath + 'a.txt','1234');
  ABWriteInI('setup','aa','1',FNeedFilePath + 'b.ini');
  Image1.Picture.SaveToFile(FNeedFilePath+'~tempFile1.Jpg');

  temStrings.Clear;
  temStrings.Add('D:\Other\InputOutput\OutOtherFile\OtherFileList.txt');
  temStrings.Add('D:\Other\InputOutput\OutSQLFile\DictionaryInfo.SQL');
  ABDoPak(temStrings, FNeedFilePath + 'all.pak');

  temStrings.Clear;
  ABUnDoPak(FNeedFilePath + 'all.pak', FNeedFilePath, temStrings);
end;

procedure TABTestForm.ABSQLLogExecSQL(aType, aSQL: string);
begin
  Memo3.Lines.Add(aType+' '+aSQL);
end;

procedure TABTestForm.Button58Click(Sender: TObject);
begin
  ABSaveAutoSavePropertys(self);
  Addlog(Memo3,'ABPubDefaultValueU ABSaveAutoSavePropertys(保存所有者为aOwner的所有子组件的属性):self');
  ABLoadAutoSavePropertys(self);
  Addlog(Memo3,'ABPubDefaultValueU ABLoadAutoSavePropertys(加载所有者为aOwner的所有子组件的属性):self');

  ABSaveAutoSavePropertys([Edit3,Memo1]);
  Addlog(Memo3,'ABPubDefaultValueU ABSaveAutoSavePropertys(保存aComponentArray中所有组件的属性):Edit3,Memo1');
  ABLoadAutoSavePropertys([Edit3,Memo1]);
  Addlog(Memo3,'ABPubDefaultValueU ABLoadAutoSavePropertys(加载aComponentArray中所有组件的属性):Edit3,Memo1');

  ABSaveAutoSaveProperty(Edit3);
  Addlog(Memo3,'ABPubDefaultValueU ABSaveAutoSaveProperty(保存aComponent组件的属性):Edit3');
  ABLoadAutoSaveProperty(Edit3);
  Addlog(Memo3,'ABPubDefaultValueU ABLoadAutoSaveProperty(加载aComponent组件的属性):Edit3');

  ABSaveAutoSaveProperty(Edit3,['text'],[]);
  Addlog(Memo3,'ABPubDefaultValueU ABSaveAutoSaveProperty(保存aComponent组件的扩展属性):Edit3');
  ABLoadAutoSaveProperty(Edit3,['text'],[]);
  Addlog(Memo3,'ABPubDefaultValueU ABLoadAutoSaveProperty(加载aComponent组件的扩展属性):Edit3');

  Addlog(Memo3,'ABPubDefaultValueU ABCheckOperatorInTag(ttPubAutoSave_91,91 判断Tag值是否包含指定的操作类型aType):'+ABBoolToStr(ABCheckOperatorInTag(ttPubAutoSave_91,91)));
  Addlog(Memo3,'ABPubDefaultValueU ABCheckOperatorInTag(tt_91000000,91 判断Tag值是否包含指定的操作类型aType):'+ABBoolToStr(ABCheckOperatorInTag(tt_91000000,91)));
end;

procedure TABTestForm.Button47Click(Sender: TObject);
begin
  ABDeleteFile(FNeedFilePath + 'all.pak');

  ABWriteTxt(FNeedFilePath + 'a.txt','1234');
  ABWriteInI('setup','aa','1',FNeedFilePath + 'b.ini');
  Image1.Picture.SaveToFile(FNeedFilePath+'~tempFile1.Jpg');

  temStrings.Clear;
  temStrings.Add(FNeedFilePath + 'a.txt');
  temStrings.Add(FNeedFilePath + 'a.ini');
  ABDoPak(temStrings, FNeedFilePath + 'all.pak');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 将多个文件合成一个包');

  ABAddPakItem(FNeedFilePath + 'all.pak', FNeedFilePath + '~tempFile1.Jpg');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 增加文件到包中');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 取包的文件数目[all.pak]' + inttostr(ABGetPakItemCount(FNeedFilePath +'all.pak')));

  ABDeleteFile(FNeedFilePath + 'a.txt');
  ABDeleteFile(FNeedFilePath + 'a.ini');
  ABDeleteFile(FNeedFilePath + '~tempFile1.Jpg');

  ABPakItemToFile(FNeedFilePath + 'all.pak', FNeedFilePath + '~tempFile1.Jpg');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 从包中释放单个文件');

  ABUnDoPak(FNeedFilePath + 'all.pak', FNeedFilePath, temStrings);
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 将包解到多个文件中');

  ABDeletePakItem(FNeedFilePath + 'all.pak', FNeedFilePath + '~tempFile1.Jpg');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 从包中删除文件');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 取包的文件数目[all.pak]' + inttostr(ABGetPakItemCount(FNeedFilePath +'all.pak')));
end;

procedure TABTestForm.Button54Click(Sender: TObject);
var
  i:LongInt;
  tempText:string;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempOnGetText: TFieldGetTextEvent;
  tempOnSetText: TFieldSetTextEvent;
  tempOnValidate: TFieldNotifyEvent;
  tempOnChange: TFieldNotifyEvent;
begin
  Addlog(Memo3,'ABPubDBU ABStrsToSql(将aSpaceSign分隔的字串aStrs转换成union连接的SQL语句 ):'+ABStrsToSql('1 as aa go  2  go  3 ',' go '));
  Addlog(Memo3,'ABPubDBU ABFieldNamesAndFieldValuesToWhere(多字段名与多字段值转成SQL的Where ):'+
    ABFieldNamesAndFieldValuesToWhere(['fieldname1','fieldname2','fieldname3'],
                                      ['fieldValue1','fieldValue2','fieldValue3'],
                                      ['=','<>','>'],
                                      [true,true,true],
                                      ' or '
                                       ));

  Addlog(Memo3,'ABPubDBU ABFieldNamesAndFieldValuesToWhere(多字段名与多字段值转成SQL的Where ):'+
    ABFieldNamesAndFieldValuesToWhere(['fieldname1','fieldname2','fieldname3'],
                                      ['fieldValue1','fieldValue2','fieldValue3'],
                                      ['=','<>','>'],
                                      [false,false,false],
                                      ' and '
                                       ));
  Addlog(Memo3,'ABPubDBU ABFieldNamesAndFieldValuesToWhere(多字段名与多字段值转成SQL的Where ):'+
    ABFieldNamesAndFieldValuesToWhere(['fieldname1','fieldname2','fieldname3'],
                                      ['fieldValue1','fieldValue2','fieldValue3'],
                                      [] ,
                                      [false,false,false]
                                       ));

  Addlog(Memo3,'ABPubDBU ABFieldNamesAndFieldValueToWhere(多字段名与单字段值转成SQL的Where ):'+
    ABFieldNamesAndFieldValueToWhere(['fieldname1','fieldname2','fieldname3'],
                                      'fieldValue1',
                                      ['=','<>','>'],
                                      true,
                                      ' and '
                                       ));
  Addlog(Memo3,'ABPubDBU ABFieldNamesAndFieldValueToWhere(多字段名与单字段值转成SQL的Where ):'+
    ABFieldNamesAndFieldValueToWhere(['fieldname1','fieldname2','fieldname3'],
                                      'fieldValue1',
                                      [],
                                      false,
                                      ' or '
                                       ));

  Addlog(Memo3,'ABPubDBU ABFieldNameAndFieldValuesToWhere(单字段名与多字段值转成SQL的Where ):'+
    ABFieldNameAndFieldValuesToWhere( 'fieldname1',
                                      ['fieldValue1','fieldValue2','fieldValue3'],
                                      ['=','<>','>'],
                                      true,
                                      ' and '
                                       ));
  Addlog(Memo3,'ABPubDBU ABFieldNameAndFieldValuesToWhere(单字段名与多字段值转成SQL的Where ):'+
    ABFieldNameAndFieldValuesToWhere( 'fieldname1',
                                      ['fieldValue1','fieldValue2','fieldValue3'],
                                      [],
                                      false,
                                      ' or '
                                       ));

  Addlog(Memo3,'ABPubDBU ABGetSQLFieldValue(取可应用到SQL语句中的字段值 FDataSet.Fields[0]):'+ABGetSQLFieldValue(FDataSet.Fields[0]));
  Addlog(Memo3,'ABPubDBU ABGetSQLFieldValue(取可应用到SQL语句中的字段值 FDataSet.Fields[1]):'+ABGetSQLFieldValue(FDataSet.Fields[1]));
  Addlog(Memo3,'ABPubDBU ABGetSQLFieldValue(取可应用到SQL语句中的字段值 FDataSet.Fields[2]):'+ABGetSQLFieldValue(FDataSet.Fields[2]));
  Addlog(Memo3,'ABPubDBU ABGetSQLFieldValue(取可应用到SQL语句中的字段值 FDataSet.Fields[3]):'+ABGetSQLFieldValue(FDataSet.Fields[3]));
  Addlog(Memo3,'ABPubDBU ABGetSQLFieldValue(取可应用到SQL语句中的字段值 FDataSet.Fields[4]):'+ABGetSQLFieldValue(FDataSet.Fields[4]));

  Addlog(Memo3,'ABPubDBU ABTransactQL(将aSQL中的格式为"[:字段名]"的字段名用数据集的值进行替换 ):'+
    ABTransactQL('select [:'+FDataSet.Fields[0].FieldName+']'+',[:'+FDataSet.Fields[1].FieldName+']',[FDataSet]));

  Addlog(Memo3,'ABPubDBU ABIsOkSQL(是否是正确的SQL语句):'+ABBoolToStr(ABIsOkSQL('update table1 set a=1')));
  Addlog(Memo3,'ABPubDBU ABIsOkSQL(是否是正确的SQL语句):'+ABBoolToStr(ABIsOkSQL('declare @a int select * from table1  ')));

  Addlog(Memo3,'ABPubDBU ABIsSelectSQL(是否是正确的查询SQL语句):'+ABBoolToStr(ABIsSelectSQL('update table1 set a=1')));
  Addlog(Memo3,'ABPubDBU ABIsSelectSQL(是否是正确的查询SQL语句):'+ABBoolToStr(ABIsSelectSQL('declare @a int select * from table1  ')));

  Addlog(Memo3,'ABPubDBU ABGetSQLByDataset(得到数据集记录的更新和插入的SQL语句):'+
    ABGetSQLByDataset(FDataSet,
                      'table1',
                      ['ParentCode','Code'],
                      ['Name', 'Remark','Order'],
                      ['ParentCode', 'Code', 'Name', 'Remark','Order'],

                      nil,[],

                      false)
                      );
  Addlog(Memo3,'ABPubDBU ABGetSQLByDataset(得到数据集记录的更新和插入的SQL语句):'+
    ABGetSQLByDataset(FDataSet,
                      'table1',
                      ['ParentCode','Code'],
                      ['Name', 'Remark','Order'],
                      ['ParentCode', 'Code', 'Name', 'Remark','Order'],

                      nil,[],

                      true)
                      );

  Addlog(Memo3,'ABPubDBU ABGetSQLByTreeView(得到树关联数据集记录的更新和插入的SQL语句):'+
    ABGetSQLByTreeView(TreeView1,
                       FDataSet,
                      'table1',
                      ['ParentCode','Code'],
                      ['Name', 'Remark','Order'],
                      ['ParentCode', 'Code', 'Name', 'Remark','Order'],

                      nil,[])
                      );

  if Assigned(TreeView1.Selected) then
    Addlog(Memo3,'ABPubDBU ABGetSQLByTreeView(得到树关联数据集记录的更新和插入的SQL语句):'+
      ABGetSQLByTreeNode(TreeView1.Selected,
                       FDataSet,
                      'table1',
                      ['ParentCode','Code'],
                      ['Name', 'Remark','Order'],
                      ['ParentCode', 'Code', 'Name', 'Remark','Order'],

                      nil,[],

                      false
                      )
                      );

  FtestMainDataset:=ABCreateClientDataSet(['ParentCode_Main', 'Code_Main', 'Name_Main', 'Remark_Main','Order_Main'],
                                     ['父编号', '编号', '名称', '备注', '序号'],
                                     [ftString, ftString, ftString, ftMemo, ftInteger],
                                     [100, 100, 100]
                                     );
  FtestDetailDataset:=ABCreateClientDataSet(['Parent_detail', 'Code_detail', 'Name_detail', 'Remark_detail','Order_detail'],
                                     ['父编号', '编号', '名称', '备注', '序号'],
                                     [ftString, ftString, ftString, ftMemo, ftInteger],
                                     [100, 100, 100]
                                     );
  FtestMainDetailDataset:=ABCreateClientDataSet(['ParentCode', 'Code', 'Name', 'Remark','Order','IsDetail'],
                                     ['父编号', '编号', '名称', '备注', '序号','是否从表'],
                                     [ftString, ftString, ftString, ftMemo, ftInteger,ftBoolean],
                                     [100, 100, 100]
                                     );
  DataSource_Main.DataSet:=FtestMainDataset;
  DataSource_Detail.DataSet:=FtestDetailDataset;
  FtestDetailDataset.MasterFields:='Code_Main';
  FtestDetailDataset.IndexFieldNames:='Parent_detail';
  FtestDetailDataset.MasterSource:=DataSource_Main;
  DataSource_MainDetail.DataSet:=FtestMainDetailDataset;
  ABSetDataSetValue(FDataset,
                    FtestMainDetailDataset);
  ABSetDataSetValue(FDataset,['ParentCode', 'Code', 'Name', 'Remark','Order'],
                    FtestMainDataset,['ParentCode_Main', 'Code_Main', 'Name_Main', 'Remark_Main','Order_Main']);
  for I := 1 to 9 do
  begin
    ABSetFieldValue([inttostr((i div 5)+1), inttostr(i), 'detail_'+inttostr(i),inttostr(i),i],
                      FtestDetailDataset,
                      ['Parent_detail', 'Code_detail', 'Name_detail', 'Remark_detail','Order_detail'],
                      true,true);
  end;

  Addlog(Memo3,'ABPubDBU ABCreateClientDataSet(根据传入的字段信息创建并激活ClientDataSet且返回)');
  FtestMainDetailDataset.First;

  FtestMainDetailDataset.BeforeScroll:=FtestNewDatasetBeforeScroll;
  FtestMainDetailDataset.afterScroll:=FtestNewDatasetAfterScroll;
  FtestMainDetailDataset.FieldByName('Name').OnChange:=FtestNewDatasetaaChange;
  FtestMainDetailDataset.FieldByName('Name').OnGetText:=FtestNewDatasetaaGetText;
  FtestMainDetailDataset.FieldByName('Name').OnSetText:=FtestNewDatasetaaSetText;
  FtestMainDetailDataset.FieldByName('Name').OnValidate:=FtestNewDatasetaaValidate;
  Addlog(Memo3,'ABPubDBU 关联数据集和字段事件:');

  FtestMainDetailDataset.Next;
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.Next');
  ABSetFieldValue(FtestMainDetailDataset.FieldByName('Name').AsString+'1',FtestMainDetailDataset.FieldByName('Name'),true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue Name:');

  ABBackAndStopDatasetEvent(FtestMainDetailDataset,tempOldBeforeScroll,tempOldAfterScroll);
  Addlog(Memo3,'ABPubDBU 备份数据集的事件到参数并清空:');
  ABBackAndStopFieldEvent(FtestMainDetailDataset.FieldByName('Name'),tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
  Addlog(Memo3,'ABPubDBU 备份字段的事件到参数并清空');
  try
    FtestMainDetailDataset.Next;
    Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.Next');
    ABSetFieldValue(FtestMainDetailDataset.FieldByName('Name').AsString+'1',FtestMainDetailDataset.FieldByName('Name'),true);
    Addlog(Memo3,'ABPubDBU ABSetFieldValue Name:');
  finally
    ABUnBackDatasetEvent(FtestMainDetailDataset,tempOldBeforeScroll,tempOldAfterScroll);
    Addlog(Memo3,'ABPubDBU 将备份的数据集事件恢复到数据集中:');
    ABUnBackFieldEvent(FtestMainDetailDataset.FieldByName('Name'),tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
    Addlog(Memo3,'ABPubDBU 将备份的字段事件恢复到字段中:');
  end;
  FtestMainDetailDataset.Next;
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.Next');
  ABSetFieldValue(FtestMainDetailDataset.FieldByName('Name').AsString+'1',FtestMainDetailDataset.FieldByName('Name'),true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue Name:');

  ABBackAndStopDatasetEvent(FtestMainDetailDataset,tempOldBeforeScroll,tempOldAfterScroll);
  Addlog(Memo3,'ABPubDBU 备份数据集的事件到参数并清空:');
  ABBackAndStopFieldEvent(FtestMainDetailDataset.FieldByName('Name'),tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
  Addlog(Memo3,'ABPubDBU 备份字段的事件到参数并清空');

  FtestMainDetailDataset.Next;
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.Next');
  ABSetFieldValue(FtestMainDetailDataset.FieldByName('Name').AsString+'1',FtestMainDetailDataset.FieldByName('Name'),true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue Name:');

  Addlog(Memo3,'ABPubDBU ABGetFieldNamesByDisplayLabels(数据集字段显示名到字段名的转换 ParentCode,Code,Remark):'+ABGetFieldNamesByDisplayLabels(FtestMainDetailDataset,'父编号,编号,备注'));
  Addlog(Memo3,'ABPubDBU ABGetFieldByDisplayLabel(通过数据集字段显示名取得字段对象 ParentCode):'+ABGetFieldByDisplayLabel(FtestMainDetailDataset,'父编号').fieldName);
  Addlog(Memo3,'ABPubDBU ABGetDisplayLabelsByFieldName(数据集字段字段名到显示名的转换 父编号,编号,备注):'+ABGetDisplayLabelsByFieldName(FtestMainDetailDataset,'ParentCode,Code,Remark'));

  tempList.Clear;
  ABGetDetailDatasetList(FtestMainDataset,tempList);
  Addlog(Memo3,'ABPubDBU ABGetDetailDatasetList(得到所有关联到数据集的所有层的子数据集列表 1):'+inttostr(tempList.Count));

  ABOpenDetailDataset(FtestMainDataset);
  Addlog(Memo3,'ABPubDBU ABOpenDetailDataset(打开所有关联到数据集的相关数据集):');
  tempList.Clear;
  ABGetDetailDatasetList(FtestMainDataset,tempList);
  ABOpenDetailDataset(tempList);
  Addlog(Memo3,'ABPubDBU ABOpenDetailDataset(打开列表中的数据集):');
  tempList.Clear;
  ABGetActiveDetailDataSets(FtestMainDataset,tempList);
  Addlog(Memo3,'ABPubDBU ABGetActiveDetailDataSets(得到所有关联到数据集的所有层的已打开子数据集列表):');

  tempList.Clear;
  ABGetDetailDatasetList(FtestMainDataset,tempList,true);
  Addlog(Memo3,'ABPubDBU ABGetDetailDatasetList(得到所有关联到数据集的所有层的子数据集列表 2):'+inttostr(tempList.Count));
  tempList.Clear;
  ABGetDataSourceList(self,FtestMainDetailDataset,tempList);
  Addlog(Memo3,'ABPubDBU ABGetDataSourceList(得到所有关联到数据集的DataSource列表 1):'+inttostr(tempList.Count));

  ABSetDatasetDisableControls(FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABSetDatasetDisableControls(断开所有关联到主数据集的所有层的子数据集与界面显示的关联):');
  ABSetDatasetEnableControls(FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABSetDatasetEnableControls(恢复所有关联到主数据集的所有层的子数据集与界面显示的关联):');


  ABOnlyEditFields(FtestMainDetailDataset,['Code', 'Name'],false);
  Addlog(Memo3,'ABPubDBU ABOnlyEditFields(设置数据集指定字段名的字段可编辑 Code,Name可编辑):');
  ABOnlyEditFields(FtestMainDetailDataset,['Code', 'Name'],true);
  Addlog(Memo3,'ABPubDBU ABOnlyEditFields(设置数据集指定字段名的字段可编辑 仅Code,Name可编辑):');

  ABOnlyReadFields(FtestMainDetailDataset,['Code', 'Name'],false);
  Addlog(Memo3,'ABPubDBU ABOnlyReadFields(设置数据集指定字段名的字段只读 Code,Name只读):');
  ABOnlyReadFields(FtestMainDetailDataset,['Code', 'Name'],true);
  Addlog(Memo3,'ABPubDBU ABOnlyReadFields(设置数据集指定字段名的字段只读 仅Code,Name只读):');

  ABOnlyReadFields(FtestMainDetailDataset,[],true);

  ABOnlyHintFields(FtestMainDetailDataset,['Code', 'Name'],false);
  Addlog(Memo3,'ABPubDBU ABOnlyHintFields(设置数据集指定字段名的字段隐藏 Code,Name隐藏):');
  ABOnlyHintFields(FtestMainDetailDataset,['Code', 'Name'],true);
  Addlog(Memo3,'ABPubDBU ABOnlyHintFields(设置数据集指定字段名的字段隐藏 仅Code,Name隐藏):');

  ABOnlyShowFields(FtestMainDetailDataset,['Code', 'Name'],false);
  Addlog(Memo3,'ABPubDBU ABOnlyShowFields(设置数据集指定字段名的字段显示 显示Code,Name):');
  ABOnlyShowFields(FtestMainDetailDataset,['Code', 'Name'],true);
  Addlog(Memo3,'ABPubDBU ABOnlyShowFields(设置数据集指定字段名的字段显示 仅显示Code,Name):');

  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集单个字段的值(由字段名决定返回的字段)):'+VarToStr(ABGetFieldValue(FtestMainDetailDataset,'Name','')));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集单个字段的值(由字段名决定返回的字段)):'+VarToStr(ABGetFieldValue(FtestMainDetailDataset,'aaName','aa')));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集指定Key字段值下的多个字段的值(由字段名串决定返回的多个字段)):'+VarToStr(ABGetFieldValue(FtestMainDetailDataset,['Name'],['aa'],['Code'],'')));

  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集单个字段的值(由Fields的字段序号决定返回的字段)):'+VarToStr(ABGetFieldValue(FtestMainDetailDataset,2,'')));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集单个字段的值(由Fields的字段序号决定返回的字段)):'+VarToStr(ABGetFieldValue(FtestMainDetailDataset,22,'aa')));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Code','Name','Remark'],[]));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['aa','Code','Name','Remark'],['tempaa']));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名串决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,'Code,Name,Remark',[]));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由Fields的字段序号数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,[1,2,3],[]));
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由Fields的字段序号数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,[13,1,2,3],['tempaa']));

  ABSetFieldValue('tempName',FtestMainDetailDataset.FieldByName('Name'));
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(设置字段的值 tempName):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集单个字段的值(由字段名决定返回的字段)):'+ABGetFieldValue(FtestMainDetailDataset,'Name',''));
  ABSetFieldValue('tempName',FtestMainDetailDataset.FieldByName('Name'),false,true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(设置字段的值 tempNametempName):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集单个字段的值(由字段名决定返回的字段)):'+ABGetFieldValue(FtestMainDetailDataset,'Name',''));

  ABSetFieldValue(['tempName','tempRemark'],FtestMainDetailDataset,['Name','Remark']);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将多个值设置给数据集多个字段(由字段名数组决定设置的多个字段)):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Name','Remark'],[]));
  ABSetFieldValue(['tempName','tempRemark'],FtestMainDetailDataset,['Name','Remark'],false,false,true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将多个值设置给数据集多个字段(由字段名数组决定设置的多个字段)):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Name','Remark'],[]));

  ABSetFieldValue(FtestMainDetailDataset,['Name','Remark'],FtestMainDetailDataset,['Name','Remark']);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将源数据集的多个字段值设置给目标数据集多个字段):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Name','Remark'],[]));
  ABSetFieldValue(FtestMainDetailDataset,['Name','Remark'],FtestMainDetailDataset,['Name','Remark'],false,false,true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将源数据集的多个字段值设置给目标数据集多个字段):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Name','Remark'],[]));

  ABSetFieldValue(FtestMainDetailDataset,FtestMainDetailDataset,['Code','Name','Remark']);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将源数据集与目标数据集中指定的多个共有字段复制给目标数据集字段中):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Code','Name','Remark'],[]));
  ABSetFieldValue(FtestMainDetailDataset,FtestMainDetailDataset,['Code','Name','Remark'],false,false,true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将源数据集与目标数据集中指定的多个共有字段复制给目标数据集字段中):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Code','Name','Remark'],[]));

  ABSetFieldValue(FtestMainDetailDataset,FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将源数据集与目标数据集中所有共有的字段复制给目标数据集字段中):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Code','Name','Remark'],[]));
  ABSetFieldValue(FtestMainDetailDataset,FtestMainDetailDataset,false,false,true);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(将源数据集与目标数据集中所有共有的字段复制给目标数据集字段中):');
  Addlog(Memo3,'ABPubDBU ABGetFieldValue(得到数据集多个字段的值(由字段名数组决定返回的多个字段)):'+ABGetFieldValue(FtestMainDetailDataset,['Code','Name','Remark'],[]));

  ABSetFieldValue(FDataset,['Code'],['11'],['Name','Remark'],['aa','bb']);
  Addlog(Memo3,'ABPubDBU ABSetFieldValue(设置数据集指定Key字段值下的多个字段的值):');

  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由字段名数组决定多个字段)):'+ABGetDatasetValue(FtestMainDetailDataset,['Code','Name','Remark'],[],[]));
  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由字段名数组决定多个字段)):'+ABGetDatasetValue(FtestMainDetailDataset,['Code','Name','Remark'],['code'],['1']));
  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由Fields的字段序号数组决定多个字段)):'+ABGetDatasetValue(FtestMainDetailDataset,[1,2,3],[],[]));
  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由Fields的字段序号数组决定多个字段)):'+ABGetDatasetValue(FtestMainDetailDataset,[1,2,3],['code'],['1']));

  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由字段名数组决定多个字段)):'+
    ABGetDatasetValue(FtestMainDetailDataset,['Code','Name','Remark'],[],[],'order',FtestMainDetailDataset,'code'));
  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由字段名数组决定多个字段)):'+
    ABGetDatasetValue(FtestMainDetailDataset,['Code','Name','Remark'],[],[],'order',FtestMainDetailDataset,'code','not find'));


  ABDeleteDataset(FtestMainDetailDataset);
  ABSetDataSetValue(FDataset,['Code','Name','Remark'],FtestMainDetailDataset,['Code','Name','Remark']);
  Addlog(Memo3,'ABPubDBU ABSetDataSetValue(将源数据集所有记录多个字段值拷贝到目标数据集多个记录多个字段中):');
  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由字段名数组决定多个字段)):'+ABGetDatasetValue(FtestMainDetailDataset,['Code','Name','Remark'],[],[]));

  ABDeleteDataset(FtestMainDetailDataset);
  ABSetDataSetValue(FDataset,FtestMainDetailDataset,['Code','Name','Remark']);
  Addlog(Memo3,'ABPubDBU ABSetDataSetValue(将源数据集所有记录多个共有字段拷贝到目标数据集中):');
  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由字段名数组决定多个字段)):'+ABGetDatasetValue(FtestMainDetailDataset,['Code','Name','Remark'],[],[]));

  ABDeleteDataset(FtestMainDetailDataset);
  ABSetDataSetValue(FDataset,FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABSetDataSetValue(将源数据集所有记录所有共有字段拷贝到目标数据集中):');
  Addlog(Memo3,'ABPubDBU ABGetDatasetValue(得到数据集多个字段所有记录的值(由字段名数组决定多个字段)):'+ABGetDatasetValue(FtestMainDetailDataset,['Code','Name','Remark'],[],[]));

  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.RecNo:'+inttostr(FtestMainDetailDataset.RecNo));
  ABSetDatasetRecno(FtestMainDetailDataset,10);
  Addlog(Memo3,'ABPubDBU ABSetDatasetRecno(设置数据集的记录号 10):');
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.RecNo:'+inttostr(FtestMainDetailDataset.RecNo));

  ABSetDatasetRecno(FtestMainDetailDataset,123,'','');
  Addlog(Memo3,'ABPubDBU ABSetDatasetRecno(将清除了开始与结束标记的记录号设置给数据集 10):');
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.RecNo:'+inttostr(FtestMainDetailDataset.RecNo));
  ABSetDatasetRecno(FtestMainDetailDataset,123,'1','');
  Addlog(Memo3,'ABPubDBU ABSetDatasetRecno(将清除了开始与结束标记的记录号设置给数据集 10):');
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.RecNo:'+inttostr(FtestMainDetailDataset.RecNo));
  ABSetDatasetRecno(FtestMainDetailDataset,123,'','3');
  Addlog(Memo3,'ABPubDBU ABSetDatasetRecno(将清除了开始与结束标记的记录号设置给数据集 12):');
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.RecNo:'+inttostr(FtestMainDetailDataset.RecNo));
  ABSetDatasetRecno(FtestMainDetailDataset,123,'8','9');
  Addlog(Memo3,'ABPubDBU ABSetDatasetRecno(将清除了开始与结束标记的记录号设置给数据集 12):');
  Addlog(Memo3,'ABPubDBU FtestMainDetailDataset.RecNo:'+inttostr(FtestMainDetailDataset.RecNo));

  Addlog(Memo3,'ABPubDBU ABGetDatasetRecno(返回数据集记录号):'+inttostr(ABGetDatasetRecno(FtestMainDetailDataset)));
  Addlog(Memo3,'ABPubDBU ABGetDatasetRecno(返回清除了开始与结束标记的数据集记录号 12345):'+inttostr(ABGetDatasetRecno(12345,'','')));
  Addlog(Memo3,'ABPubDBU ABGetDatasetRecno(返回清除了开始与结束标记的数据集记录号 2345):'+inttostr(ABGetDatasetRecno(12345,'1','')));
  Addlog(Memo3,'ABPubDBU ABGetDatasetRecno(返回清除了开始与结束标记的数据集记录号 1234):'+inttostr(ABGetDatasetRecno(12345,'','5')));
  Addlog(Memo3,'ABPubDBU ABGetDatasetRecno(返回清除了开始与结束标记的数据集记录号 12345):'+inttostr(ABGetDatasetRecno(12345,'6','9')));

  Addlog(Memo3,'ABPubDBU ABGetDataSetRecordCount(取得设置了过滤条件后的数据集记录数量):'+inttostr(ABGetDataSetRecordCount(FtestMainDetailDataset,[],[])));
  Addlog(Memo3,'ABPubDBU ABGetDataSetRecordCount(取得设置了过滤条件后的数据集记录数量):'+inttostr(ABGetDataSetRecordCount(FtestMainDetailDataset,['code'],['1'])));
  tempList.Clear;
  tempList.Add(FtestMainDetailDataset);
  tempList.Add(FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABGetDataSetRecordCount(取得数据集列表所有数据集的记录数量之和 FtestMainDetailDataset+FtestMainDetailDataset):'+inttostr(ABGetDataSetRecordCount(tempList)));

  Addlog(Memo3,'ABPubDBU ABComparisonValueSame(检测值两个变体变量是否相同 true):'+ABBoolToStr(ABComparisonValueSame(1,1)));
  Addlog(Memo3,'ABPubDBU ABComparisonValueSame(检测值两个变体变量是否相同 false):'+ABBoolToStr(ABComparisonValueSame(1,2)));
  Addlog(Memo3,'ABPubDBU ABComparisonValueSame(检测值两个变体变量是否相同 true):'+ABBoolToStr(ABComparisonValueSame(VarArrayOf([1,2,3]),VarArrayOf([1,2,3]))));
  Addlog(Memo3,'ABPubDBU ABComparisonValueSame(检测值两个变体变量是否相同 false):'+ABBoolToStr(ABComparisonValueSame(VarArrayOf([1,1,3]),VarArrayOf([1,2,3]))));

  Addlog(Memo3,'ABPubDBU ABComparisonFieldOldNewValueSame(检测字段的新旧值是否相同 true):'+ABBoolToStr(ABComparisonFieldOldNewValueSame(FtestMainDetailDataset.FieldByName('name'))));
  FtestMainDetailDataset.Edit;
  FtestMainDetailDataset.FieldByName('name').AsString:=FtestMainDetailDataset.FieldByName('name').AsString+'1';
  Addlog(Memo3,'ABPubDBU ABComparisonFieldOldNewValueSame(检测字段的新旧值是否相同 false):'+ABBoolToStr(ABComparisonFieldOldNewValueSame(FtestMainDetailDataset.FieldByName('name'))));
  FtestMainDetailDataset.Cancel;

  Addlog(Memo3,'ABPubDBU ABGetFieldOldValue(取字段的OldValue值):'+vartostrdef(ABGetFieldOldValue(FtestMainDetailDataset.FieldByName('name')),''));


  Addlog(Memo3,'ABPubDBU ABGetDatasetFilter(得到数据集的过滤条件 ):'+ABGetDatasetFilter(FtestMainDetailDataset));
  ABSetDatasetFilter(FtestMainDetailDataset,'code<='+QuotedStr('10'));
  Addlog(Memo3,'ABPubDBU ABSetDatasetFilter(设置数据集的过滤条件):'+'code<='+QuotedStr('10'));
  Addlog(Memo3,'ABPubDBU ABGetDatasetFilter(得到数据集的过滤条件'+'code<='+QuotedStr('10')+'):'+ABGetDatasetFilter(FtestMainDetailDataset));
  ABSetDatasetFilter(FtestMainDetailDataset,'');

  FtestMainDetailDataset.ReadOnly:=true;
  Addlog(Memo3,'ABPubDBU ABCheckDataSetCanEdit(检测数据集是否可修改 false):'+ABBoolToStr(ABCheckDataSetCanEdit(FtestMainDetailDataset)));
  FtestMainDetailDataset.ReadOnly:=False;
  Addlog(Memo3,'ABPubDBU ABCheckDataSetCanEdit(检测数据集是否可修改 true):'+ABBoolToStr(ABCheckDataSetCanEdit(FtestMainDetailDataset)));

  Addlog(Memo3,'ABPubDBU ABCheckDataSetInEdit(检测数据集是否在修改中 false):'+ABBoolToStr(ABCheckDataSetInEdit(FtestMainDetailDataset)));
  FtestMainDetailDataset.Edit;
  Addlog(Memo3,'ABPubDBU ABCheckDataSetInEdit(检测数据集是否在修改中 true):'+ABBoolToStr(ABCheckDataSetInEdit(FtestMainDetailDataset)));

  Addlog(Memo3,'ABPubDBU ABCheckDataSetInActive(检测数据集是否打开中 true):'+ABBoolToStr(ABCheckDataSetInActive(FtestMainDetailDataset)));

  ABSetDatasetEdit(FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABSetDatasetEdit(使数据集处于修改状态):');
  ABDeleteDataset(FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABDeleteDataset(删除数据集中的数据):');
  ABPostDataset(FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABPostDataset(保存数据集的修改):');
  ABDatasetIsEmpty(FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABDatasetIsEmpty(判断数据集是否为空):');

  Addlog(Memo3,'ABPubDBU ABSumDataset(返回数据集所有记录多个字段值的求和):'+ABStringArrayToStr(ABSumDataset(FtestMainDetailDataset,['Code','order'],[],[],[])));
  Addlog(Memo3,'ABPubDBU ABSumDataset(返回数据集所有记录多个字段值的求和):'+ABStringArrayToStr(ABSumDataset(FtestMainDetailDataset,['Code','order'],[stString,stString],[],[])));
  Addlog(Memo3,'ABPubDBU ABSumDataset(返回数据集所有记录多个字段值的求和):'+ABStringArrayToStr(ABSumDataset(FtestMainDetailDataset,['Code','order'],[stNumber,stNumber],[],[])));
  Addlog(Memo3,'ABPubDBU ABSumDataset(返回数据集所有记录多个字段值的求和):'+ABStringArrayToStr(ABSumDataset(FtestMainDetailDataset,['Code','order'],[stString,stAuto],[],[])));

  ABCopyFieldProviderFlags(FtestMainDetailDataset,FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABCopyFieldProviderFlags(拷贝源数据集所有共有字段的ProviderFlags属性到目标数据集中):');
  ABCopyFieldDisplayLabel(FtestMainDetailDataset,FtestMainDetailDataset);
  Addlog(Memo3,'ABPubDBU ABCopyFieldDisplayLabel(拷贝源数据集所有共有字段的显示标题到目标数据集中):');

  Addlog(Memo3,'ABPubDBU ABIsDataAware(检测对象是否是数据感知对象 True):'+ABBoolToStr(ABIsDataAware(DBMemo1)));
  Addlog(Memo3,'ABPubDBU ABIsDataAware(检测对象是否是数据感知对象 False):'+ABBoolToStr(ABIsDataAware(self)));
  Addlog(Memo3,'ABPubDBU ABSetControlFocusByField(设置与字段关联的感知控件得到焦点):'+ABBoolToStr(ABSetControlFocusByField(FtestMainDetailDataset.FieldByName('Remark'))));

  Addlog(Memo3,'ABPubDBU ABCheckDataSetValue(检测数据集的多个字段是否是指定的值,如不是则由显示提示框):'+ABBoolToStr(ABCheckDataSetValue(FtestMainDetailDataset,['code','Name'],['1','1'])));
  Addlog(Memo3,'ABPubDBU ABLocateFormDataset(通过窗体对象和数据集名称定位窗体中数据集符合条件的记录):'+ABBoolToStr(ABLocateFormDataset(self,FtestMainDetailDataset.Name,'code','1')));

  ABDeleteDataset(FtestMainDetailDataset);
  ABDataSetsToNewDataSet( FtestMainDetailDataset,
                            ['ParentCode', 'Code', 'Name', 'Remark','Order'],
                            'IsDetail',

                            FtestMainDataset,
                            'Code_Main',
                            ['ParentCode_Main', 'Code_Main', 'Name_Main', 'Remark_Main','Order_Main'],

                            FtestDetailDataset,
                            'Parent_detail',
                            ['Parent_detail', 'Code_detail', 'Name_detail', 'Remark_detail','Order_detail']
                            );
  Addlog(Memo3,'ABPubDBU ABDataSetsToNewDataSet(将主从数据集的数据合并到新的数据集中):');
  ABSetNoParentDatasetValue(FtestMainDetailDataset,'ParentCode','Code');
  Addlog(Memo3,'ABPubDBU ABSetNoParentDatasetValue(设置主从结构的数据集中没有第一层记录的父字段的值):');
  ABDeleteNoSubRecord(
                      FtestMainDetailDataset,
                      'ParentCode',
                      'Code',
                      ['IsDetail'],
                      ['False']
                      );
  Addlog(Memo3,'ABPubDBU ABDeleteNoSubRecord(删除主从结构数据集中没有子数据的记录):');
  ABDatasetToStrings(FtestMainDetailDataset,
                     ['code','Name'],
                     temStrings);
  Addlog(Memo3,'ABPubDBU ABDatasetToStrings(将数据集的值填充到列表中code Name):'+trim(temStrings.Text));
  ABFieldNameToStrings(FtestMainDetailDataset,
                       temStrings);
  Addlog(Memo3,'ABPubDBU ABFieldNameToStrings(将数据集的字段名或标签名填充到列表中):'+trim(temStrings.Text));
  ABFieldNameToStrings(FtestMainDetailDataset,
                       temStrings,
                       true);
  Addlog(Memo3,'ABPubDBU ABFieldNameToStrings(将数据集的字段名或标签名填充到列表中):'+trim(temStrings.Text));

  ABDatasetToStrings(FtestMainDetailDataset,
                        ['code','Name'],
                        temStrings
                        );
  ABDelStringsByDataset(temStrings,
                        FtestMainDetailDataset,
                        ['order','Name']
                        );
  Addlog(Memo3,'ABPubDBU ABDelStringsByDataset(删除字串列表中出现的数据集字段值的项):'+trim(temStrings.Text));

  ABDatasetToStrings(FtestMainDetailDataset,
                        ['code','Name'],
                        temStrings
                        );
  tempText:=ABCodeAndValueFieldConversion(temStrings.Text,
                    FtestMainDetailDataset,
                    'order','remark'
                    );
  Addlog(Memo3,'ABPubDBU ABCodeAndValueFieldConversion(替换字串中数据集编号字段值到数据集名称字段值并返回):'+tempText);
  tempText:=ABCodeAndValueFieldConversion(temStrings.Text,
                    FtestMainDetailDataset,
                    4,3
                    );
  Addlog(Memo3,'ABPubDBU ABCodeAndValueFieldConversion(替换字串中数据集编号字段值到数据集名称字段值并返回):'+tempText);

  ABDatasetToStrings(FtestMainDetailDataset,
                        ['code','Name'],
                        temStrings
                        );
  ABCodeAndValueFieldConversion(temStrings,
                    FtestMainDetailDataset,
                    'order','remark'
                    );
  Addlog(Memo3,'ABPubDBU ABCodeAndValueFieldConversion(替换字串中数据集编号字段值到数据集名称字段值):'+trim(temStrings.Text));
  ABDatasetToStrings(FtestMainDetailDataset,
                        ['code','Name'],
                        temStrings
                        );
  ABCodeAndValueFieldConversion(temStrings,
                    FtestMainDetailDataset,
                    4,3
                    );
  Addlog(Memo3,'ABPubDBU ABCodeAndValueFieldConversion(替换字串中数据集编号字段值到数据集名称字段值):'+trim(temStrings.Text));

  ABCreateRadioGroupItems(FtestDetailDataset.fieldbyname('Name_detail'),RadioGroup1);
  Addlog(Memo3,'ABPubDBU ABCreateRadioGroupItems(根据数据集字段的内容创建单选组RadioGroup的项):');

  if FDataSet.Locate('Code','1',[]) then
  begin
    try
      ABBlobFieldJpegToBitmap(FDataSet.fieldbyname('photo_jpg'),Image_bmp.Picture.Bitmap);
      Addlog(Memo3,'ABPubDBU ABBlobFieldJpegToBitmap(拷贝BlobField字段中的JPEG到TBitmap (Image3.Picture.Bitmap)):');
      ABBlobFieldJpegToPicture(FDataSet.fieldbyname('photo_jpg'),Image_jpg.Picture);
      Addlog(Memo3,'ABPubDBU ABBlobFieldJpegToPicture(拷贝BlobField字段中的JPEG到TPicture (Image4.Picture)):');
    except

    end;
  end;

  ABDataSetsToTree(TreeView2.Items,
                   FtestMainDataset,
                   'ParentCode_Main',
                   'Code_Main',
                   'Name_Main'
                          );
  ABDataSetsToTree(TreeView2.Items,
                   FtestMainDataset,
                   'ParentCode_Main',
                   'Code_Main',
                   'Name_Main',
                   false,

                   FtestDetailDataset,
                   'Parent_detail',
                   'Name_detail'
                          );

  ABDataSetsToTree(TreeView2.Items,
                   FtestMainDataset,
                   'ParentCode_Main',
                   'Code_Main',
                   'Name_Main',
                   false,

                   FtestDetailDataset,
                   'Parent_detail',
                   'Name_detail',

                   ABFindTree(TreeView2.Items,'cc',[])
                          );
  Addlog(Memo3,'ABPubDBU ABDataSetsToTree(将主从结构的主数据集和明细数据集的数据导出到TreeView中):');

  if Assigned(ABFindTree(TreeView2.Items,'11',[])) then
    Addlog(Memo3,'ABPubDBU ABFindTree(在TreeView中查找是指定字串的结点):'+ABFindTree(TreeView2.Items,'11',[]).text);
  if Assigned(ABFindTree(TreeView2.Items[0],'11',[])) then
    Addlog(Memo3,'ABPubDBU ABFindTree(在TreeView中查找是指定字串的结点):'+ABFindTree(TreeView2.Items[0],'11',[]).text);
  if Assigned(ABFindTree(TreeView2.Items[1],'11',[])) then
    Addlog(Memo3,'ABPubDBU ABFindTree(在TreeView中查找是指定字串的结点):'+ABFindTree(TreeView2.Items[1],'11',[]).text);

  if Assigned(ABFindTree(TreeView2.Items,Pointer(11),[])) then
    Addlog(Memo3,'ABPubDBU ABFindTree(在TreeView中查找是指定Data的结点):'+ABFindTree(TreeView2.Items,Pointer(11),[]).text);
  if Assigned(ABFindTree(TreeView2.Items[0],Pointer(11),[])) then
    Addlog(Memo3,'ABPubDBU ABFindTree(在TreeView中查找是指定Data的结点):'+ABFindTree(TreeView2.Items[0],Pointer(11),[]).text);
  if Assigned(ABFindTree(TreeView2.Items[1],Pointer(11),[])) then
    Addlog(Memo3,'ABPubDBU ABFindTree(在TreeView中查找是指定Data的结点):'+ABFindTree(TreeView2.Items[1],Pointer(11),[]).text);

  ABDataSetsToTree(TreeView1.Items,
                   FtestMainDataset,
                   'ParentCode_Main',
                   'Code_Main',
                   'Name_Main',
                   false,

                   FtestDetailDataset,
                   'Parent_detail',
                   'Name_detail',

                   ABFindTree(TreeView1.Items,'cc',[])
                          );
  ABDataSetsToTree(TreeView2.Items,
                   FtestMainDataset,
                   'ParentCode_Main',
                   'Code_Main',
                   'Name_Main',
                   false,

                   FtestDetailDataset,
                   'Parent_detail',
                   'Name_detail',

                   ABFindTree(TreeView2.Items,'cc',[])
                          );
  FtestMainDataset.Filter:='Order_Main>0';
  FtestMainDataset.Filtered:=True;
  ABDataSetsToMenu(MainMenu1,
                   FtestMainDataset,
                   'ParentCode_Main',
                   'Code_Main',
                   'Name_Main',
                   '',
                   '',
                   '',
                   nil,
                   false,

                   FtestDetailDataset,
                   'Parent_detail',
                   'Name_detail',
                   '',
                   '',
                   '',
                   nil,

                   nil,True,self);

  ABDataSetsToMenu(PopupMenu1,
                   FtestMainDataset,
                   'ParentCode_Main',
                   'Code_Main',
                   'Name_Main',
                   '',
                   '',
                   '',
                   nil,
                   false,

                   FtestDetailDataset,
                   'Parent_detail',
                   'Name_detail',
                   '',
                   '',
                   '',
                   nil,

                   bb11,True,TreeView2);

  Addlog(Memo3,'ABPubDBU ABDataSetsToMenu(将主从结构的主数据集和明细数据集的数据导出到菜单中):');
  Addlog(Memo3,'ABPubDBU ABQueryOutExcel_OLE(数据集输出到EXCEL):'+ABBoolToStr(
                  ABQueryOutExcel_OLE(FNeedFilePath+'QueryOutExcelS.xls',FNeedFilePath+'QueryOutExcelOut1.xls',
                                  FDataSet,

                                  true,
                                  6,
                                  2,
                                  'ParentCode,Code,Name,Remark,Order',
                                  false,

                                  'count(*),sum(*)',
                                  '11,110',

                                  temptext,
                                  false
                                  )
                  ));
  Addlog(Memo3,'ABPubDBU ABQueryOutExcel_OLE(数据集输出到EXCEL):'+ABBoolToStr(
                  ABQueryOutExcel_OLE(FNeedFilePath+'QueryOutExcelS.xls',FNeedFilePath+'QueryOutExcelOut2.xls',
                                  FDataSet,

                                  False,
                                  6,
                                  2,
                                  'ParentCode,Code,Name,Remark,Order',
                                  false,

                                  'count(*),sum(*)',
                                  '11,110',

                                  temptext,
                                  false
                                  )
                  ));
  Addlog(Memo3,'ABPubDBU ABQueryOutExcel_OLE(数据集输出到EXCEL):'+ABBoolToStr(
                  ABQueryOutExcel_OLE(FNeedFilePath+'QueryOutExcelS.xls',FNeedFilePath+'QueryOutExcelOut3.xls',
                                  FDataSet,

                                  true,
                                  6,
                                  2,
                                  'ParentCode,Code,Name,Remark,Order',
                                  false,

                                  '',
                                  '',

                                  temptext,
                                  true
                                  )
                  ));

  FDataSet.Filtered:=true;
  FDataSet.Filter:='Order>0';
  Addlog(Memo3,'ABPubDBU ABQueryOutWord_OLE(数据集输出到Word):'+ABBoolToStr(
                  ABQueryOutWord_OLE(FNeedFilePath+'QueryOutWordS.doc',FNeedFilePath+'QueryOutWordOut1.doc',
                                  FDataSet,

                                  true,
                                  2,
                                  1,
                                  'ParentCode,Code,Name,Remark,Order',
                                  false,

                                  'count(*),sum(*)',
                                  '11,110',

                                  temptext,
                                  false
                                  )
                  ));


  Addlog(Memo3,'ABPubDBU ABQueryOutWord_OLE(数据集输出到Word):'+ABBoolToStr(
                  ABQueryOutWord_OLE(FNeedFilePath+'QueryOutWordS.doc',FNeedFilePath+'QueryOutWordOut2.doc',
                                  FDataSet,

                                  False,
                                  2,
                                  1,
                                  'ParentCode,Code,Name,Remark,Order',
                                  false,

                                  'count(*),sum(*)',
                                  '11,110',

                                  temptext,
                                  false
                                  )
                  ));


  Addlog(Memo3,'ABPubDBU ABQueryOutWord_OLE(数据集输出到Word):'+ABBoolToStr(
                  ABQueryOutWord_OLE(FNeedFilePath+'QueryOutWordS.doc',FNeedFilePath+'QueryOutWordOut3.doc',
                                  FDataSet,

                                  true,
                                  2,
                                  1,
                                  'ParentCode,Code,Name,Remark,Order',
                                  false,

                                  '',
                                  '',

                                  temptext,
                                  true
                                  )
                  ));

  Addlog(Memo3,'ABPubDBU ABGetDatasetFieldNames(得到数据集的字段名):'+ABGetDatasetFieldNames(FDataSet,'Code'));

  Addlog(Memo3,'ABPubDBU ABCheckParentDataset(检测父集是否是数据集的父集):'+ABBoolToStr(ABCheckParentDataset(FtestMainDataset,FDataSet)));

  FDataSet.Filtered:=false;
  FDataSet.Filter:='';

  Addlog(Memo3,'ABPubDBU ABGetDataSetLevel(取得主从数据集键值的层次):'+inttostr(ABGetDataSetLevel(FDataSet,'Code','ParentCode','aaaa',temStrings_1)));
  ABSetDataSetLevel(FDataSet,'Code','ParentCode','','aaaa');
  Addlog(Memo3,'ABPubDBU ABSetDataSetLevel(设置主从数据集的层次):');

  ABCloseDataset(FDataSet);
  FDataSet.Open;

  //ABCreateFieldDefs(FDataSet.FieldDefs,FDataSet);
  //ABCreateFieldDef(FDataSet.FieldDefs,'','','','','');
  //ABCreateFields(FDataSet.FieldDefs);
end;

procedure TABTestForm.Button55Click(Sender: TObject);
var
  tempFindFindHWnd: HWnd;
  I: Integer;
  tempDateStr,temptimeStr,
  tempBeginDateTimeStr,tempEndDateTimeStr: string;
  tempBeginDateTime,tempEndDateTime: TDateTime;
  tempDate:TDate;
  temptime:TTime;

  A, B: Variant;
  AA,BB,CC,DD:LongInt;
  Rect1, Rect2: TRect;
  Size1, Size2: TSize;
  arr:array of LongInt;
  tempBoolean:Boolean;
  tempStringDynArray :TStringDynArray;
  tempAnsiCharArray  :TABAnsiCharDynArray;
  tempWideCharArray  :TABWideCharDynArray;
  tempIntegerArray   :TIntegerDynArray;
  tempByteArray      :TByteDynArray;
  tempDoubleArray    :TDoubleDynArray;
  tempBooleanArray    :TBooleanDynArray;
  temppPoint: TPoint;
  tempCreationTime, tempLastWriteTime,tempLastAccessTime: TDatetime ;
  tempFileAttr:LongInt;
  tempTextFormat: TABTxtFileType;
  tempText:string;
  tempIcon: TIcon;
  tempMethod:TMethod;
  tempQueryType:TABQueryCompareType;
  tempQueryTypes:TABQueryCompareTypes;
  tempUserName,tempPassWord,tempHost,tempPort,tempDir,tempFileName:string;

  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempOnGetText: TFieldGetTextEvent;
  tempOnSetText: TFieldSetTextEvent;
  tempOnValidate: TFieldNotifyEvent;
  tempOnChange: TFieldNotifyEvent;
  temparrayStr:array of string;
  tempBytes:TByteDynArray;
  Func1,Func2:TAB4ByteDynArray;
  procedure do1;
  begin
    temStrings.Add('*******************日期、时间操作*****************');
    tempBeginDateTime   :=now;
    tempEndDateTime     :=tempBeginDateTime+1;
    tempBeginDateTimeStr:=ABDateTimeToStr(tempBeginDateTime,23);
    tempEndDateTimeStr  :=ABDateTimeToStr(tempEndDateTime,23);

    ABDateTimeToDateAndTime(tempBeginDateTime,tempDate,temptime) ;
    tempDateStr         :=ABDateToStr(tempDate);
    temptimeStr         :=ABTimeToStr(temptime,12);

    Addlog(Memo3,'开始时间=now();结束时间=开始时间+1');
    Addlog(Memo3,'ABDateTimeToDateAndTime(日期时间分解到日期和时间):[DateTime:'+tempBeginDateTimeStr+'->Date:'+tempDateStr+' Time:'+temptimeStr+' ]');
    Addlog(Memo3,'ABDateAndTimeToDatetime(日期和时间合并到日期时间):[Date:'+tempDateStr+' Time:'+temptimeStr+'->DateTime:'+ABDateTimeToStr(ABDateAndTimeToDatetime(tempDate,temptime),23)+' ]');
    Addlog(Memo3,'ABDateTimeToStr(日期时间转成字串,返回日期+时分秒):'+ABDateTimeToStr(tempBeginDateTime));
    Addlog(Memo3,'ABDateTimeToStr(日期时间转成字串,返回日期+时分秒毫秒):'+ABDateTimeToStr(tempBeginDateTime,23));
    Addlog(Memo3,'ABDateToStr(日期转成字串):'+ABDateToStr(tempDate));
    Addlog(Memo3,'ABTimeToStr(时间转成字串,返回时分秒):'+ABTimeToStr(temptime));
    Addlog(Memo3,'ABTimeToStr(时间转成字串,返回时分秒毫秒):'+ABTimeToStr(temptime,12));
    Addlog(Memo3,'ABStrToDateTime(字串转成日期时间):'+ABDateTimeToStr(ABStrToDateTime(tempBeginDateTimeStr),23));
    Addlog(Memo3,'ABStrToDateTimeDef(字串转成日期时间,有默认值):'+ABDateTimeToStr(ABStrToDateTimeDef(tempBeginDateTimeStr,tempEndDateTime),23));
    Addlog(Memo3,'ABStrToDate(字串转成日期):'+ABDateToStr(ABStrToDate(tempDateStr)));
    Addlog(Memo3,'ABStrToTime(字串转成时间):'+ABTimeToStr(ABStrToTime(temptimeStr)));

    Addlog(Memo3,'ABGetMonthFirstDate(取得日期中月份的第一天的日期):'+ABDateToStr(ABGetMonthFirstDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetMonthEndDate(取得日期中月份的最后一天的日期):'+ABDateToStr(ABGetMonthEndDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetLastMonthFirstDate(取得日期中上月的第一天的日期):'+ABDateToStr(ABGetLastMonthFirstDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetLastMonthEndDate(取得日期中上月的最后一天的日期):'+ABDateToStr(ABGetLastMonthEndDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetSeasonFirstDate(取得日期中本季的第一天的日期):'+ABDateToStr(ABGetSeasonFirstDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetSeasonEndDate(取得日期中本季的最后一天的日期):'+ABDateToStr(ABGetSeasonEndDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetLastSeasonFirstDate(取得日期中上季的第一天的日期):'+ABDateToStr(ABGetLastSeasonFirstDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetLastSeasonEndDate(取得日期中上季的最后一天的日期):'+ABDateToStr(ABGetLastSeasonEndDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetYearFirstDate(取得日期中本年的第一天的日期):'+ABDateToStr(ABGetYearFirstDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetYearEndDate(取得日期中本年的最后一天的日期):'+ABDateToStr(ABGetYearEndDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetLastYearFirstDate(取得日期中上年的第一天的日期):'+ABDateToStr(ABGetLastYearFirstDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetLastYearEndDate(取得日期中上年的最后一天的日期):'+ABDateToStr(ABGetLastYearEndDate(tempBeginDateTime)));
    Addlog(Memo3,'ABGetLastYearSameDate(取得日期中上年同期天):'+ABDateToStr(ABGetLastYearSameDate(tempBeginDateTime)));
    Addlog(Memo3,'ABWeekOfYear(取得日期中上年同期天):'+inttostr(ABWeekOfYear(tempBeginDateTime)));
    Addlog(Memo3,'ABIsLeapYear(判断日期年是否为闰年,2012):'+ABBoolToStr(ABIsLeapYear(2012)));
    Addlog(Memo3,'ABIsLeapYear(判断日期年是否为闰年,2013):'+ABBoolToStr(ABIsLeapYear(2013)));
    Addlog(Memo3,'ABMaxDateTime(多个日期取较大的日期):'+ABDateTimeToStr(ABMaxDateTime([tempBeginDateTime,tempEndDateTime]),23));
    Addlog(Memo3,'ABMinDateTime(多个日期取较小的日期):'+ABDateTimeToStr(ABMinDateTime([tempBeginDateTime,tempEndDateTime]),23));

    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的年):'    +FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuYears       )));
    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的月):'    +FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuMonths      )));
    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的周):'    +FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuWeeks       )));
    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的天):'    +FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuDays        )));
    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的小时):'  +FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuHours       )));
    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的分):'    +FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuMinutes     )));
    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的秒):'    +FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuSeconds     )));
    Addlog(Memo3,'ABGetDateTimeSpan_Float(两个日期时间相差的毫秒):'+FloatToStr(ABGetDateTimeSpan_Float(tempBeginDateTime,tempEndDateTime,tuMilliSeconds)));

    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的年):'    +intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuYears       )));
    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的月):'    +intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuMonths      )));
    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的周):'    +intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuWeeks       )));
    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的天):'    +intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuDays        )));
    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的小时):'  +intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuHours       )));
    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的分):'    +intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuMinutes     )));
    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的秒):'    +intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuSeconds     )));
    Addlog(Memo3,'ABGetDateTimeSpan_Int(两个日期时间相差的毫秒):'+intToStr(ABGetDateTimeSpan_Int(tempBeginDateTime,tempEndDateTime,tuMilliSeconds)));

    Addlog(Memo3,'ABGetGMT(返回当前系统的GMT/UTC时间):'+ABDateTimeToStr(ABGetGMT,23));
    Addlog(Memo3,'ABLocaleToGMT(转换本地时间为GMT/UTC时间):'+ABDateTimeToStr(ABLocaleToGMT(tempBeginDateTime),23));
    Addlog(Memo3,'ABGMTToLocale(转换GMT/UTC时间为本地时间):'+ABDateTimeToStr(ABGMTToLocale(tempBeginDateTime),23));
  end;
  procedure do2;
  begin
    Addlog(Memo3,'*******************数学函数*****************');
    Addlog(Memo3,'ABBinToDec(二进制字符转十进制,11111111):'+ABBinToDec('11111111'));
    Addlog(Memo3,'ABDecTobin(十进制转化二进制,255):'+ABDecTobin(255));
    Addlog(Memo3,'ABDecTobin(十进制转化二进制,256):'+ABDecTobin(256));
    Addlog(Memo3,'ABHexToBin(十六进制转化成二进制,FF):'+ABHexToBin('FF'));
    Addlog(Memo3,'ABBinToHex(二进制转化成十六进制,11111111):'+ABBinToHex('11111111'));
    Addlog(Memo3,'ABTransChar(取得与ORD函数的差量,为数字时取与0的差量,为字符时取与A的差量)5:'+intToStr(ABTransChar('5')));
    Addlog(Memo3,'ABTransChar(取得与ORD函数的差量,为数字时取与0的差量,为字符时取与A的差量)D:'+intToStr(ABTransChar('D')));
    Addlog(Memo3,'ABTransChar(取得与ORD函数的差量,为数字时取与0的差量,为字符时取与A的差量)d:'+intToStr(ABTransChar('d')));
    Addlog(Memo3,'ABIsSpace(判断aCh是否是空白字符''''):'+ABBoolToStr(ABIsSpace('')));
    Addlog(Memo3,'ABIsSpace(判断aCh是否是空白字符A):'+ABBoolToStr(ABIsSpace('A')));
    Addlog(Memo3,'ABIsXDigit(判断aCh是否是十六进制数字字符A):'+ABBoolToStr(ABIsXDigit('A')));
    Addlog(Memo3,'ABIsXDigit(判断aCh是否是十六进制数字字符W):'+ABBoolToStr(ABIsXDigit('W')));
    Addlog(Memo3,'ABIsInteger(判断一个字符串是否是整数 124342.1231):'+ABBoolToStr(ABIsInteger('124342.1231')));
    Addlog(Memo3,'ABIsInteger(判断一个字符串是否是整数 124342):'+ABBoolToStr(ABIsInteger('124342')));
    Addlog(Memo3,'ABIsInt64(判断一个字符串是否是整数 1234567890123456789.1231):'+ABBoolToStr(ABIsInt64('1234567890123456789.1231')));
    Addlog(Memo3,'ABIsInt64(判断一个字符串是否是整数 1234567890123456789):'+ABBoolToStr(ABIsInt64('1234567890123456789')));
    Addlog(Memo3,'ABIsFloat(判断字符串是否是浮点数 124342.1231):'+ABBoolToStr(ABIsFloat('124342.1231')));
    Addlog(Memo3,'ABIsFloat(判断字符串是否是浮点数 124ab342.1231):'+ABBoolToStr(ABIsFloat('124ab342.1231')));

    Addlog(Memo3,'ABRandomStr(输出长度为5的随机字符串(26个字母)):'+ABRandomStr(5));
    Addlog(Memo3,'ABRandomStr(输出值在20,30之间的随机数字符串(26个字母)):'+ABRandomStr(20,30));
    Addlog(Memo3,'ABRandomChinese(输出长度为5的随机汉字):'+ABRandomStr(5));
    Addlog(Memo3,'ABRandomNum(输出最大值为5的随机数):'+inttostr(ABRandomNum(5)));
    Addlog(Memo3,'ABRandomNum(输出长度为20,30的随机数):'+inttostr(ABRandomNum(20,30)));

    Addlog(Memo3,'ABBoolToStr(布尔转换字符串 True):'+ABBoolToStr(True));
    Addlog(Memo3,'ABBoolToStr(布尔转换字符串 False):'+ABBoolToStr(False));
    Addlog(Memo3,'ABBoolToInt(布尔转换布尔Int值 True):'+inttostr(ABBoolToInt(True)));
    Addlog(Memo3,'ABBoolToInt(布尔转换布尔Int值 False):'+inttostr(ABBoolToInt(False)));

    Addlog(Memo3,'ABStrToBool(字符串转换布尔 True):'+ABBoolToStr(ABStrToBool('True')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 男):'+ABBoolToStr(ABStrToBool('男')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 man):'+ABBoolToStr(ABStrToBool('man')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 T):'+ABBoolToStr(ABStrToBool('T')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 1):'+ABBoolToStr(ABStrToBool('1')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 False):'+ABBoolToStr(ABStrToBool('False')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 女):'+ABBoolToStr(ABStrToBool('女')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 woman):'+ABBoolToStr(ABStrToBool('woman')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 F):'+ABBoolToStr(ABStrToBool('F')));
    Addlog(Memo3,'ABStrToBool(字符串转换布尔 0):'+ABBoolToStr(ABStrToBool('0')));

    Addlog(Memo3,'ABInBound(判断整数5是否在1和4之间):'+ABBoolToStr(ABInBound(5,1,4)));
    Addlog(Memo3,'ABInBound(判断整数5是否在1和5之间):'+ABBoolToStr(ABInBound(5,1,5)));
    Addlog(Memo3,'ABTrimInt(检测5，限制输出在1..4之间):'+inttostr(ABTrimInt(5,1,4)));
    Addlog(Memo3,'ABTrimInt(检测5，限制输出在1..5之间):'+inttostr(ABTrimInt(5,1,5)));
    A:=1.1;B:=2.2;
    ABSwap(A, B);
    Addlog(Memo3,'ABSwap(交换两个数,1.1,2.2):'+floattostr(A)+','+floattostr(B));
    ABEnRect(Rect1,1,2,3,5);
    Addlog(Memo3,'ABEnRect(合并左上角坐标ax, ay和宽度aWidth、高度aHeight为aRect):'+inttostr(Rect1.Left)+','+
                                                                                    inttostr(Rect1.Top)+','+
                                                                                    inttostr(Rect1.Right-Rect1.Left)+','+
                                                                                    inttostr(Rect1.Bottom-Rect1.Top)
                                                                                    );
    ABDeRect(Rect1,AA,BB,CC,DD);
    Addlog(Memo3,'ABDeRect(分解aRect为左上角坐标ax, ay和宽度aWidth、高度aHeight):'+inttostr(AA)+','+
                                                                                    inttostr(BB)+','+
                                                                                    inttostr(CC)+','+
                                                                                    inttostr(DD)
                                                                                    );
    ABEnRect(Rect2,1,2,3,5);
    Addlog(Memo3,'ABRectEqu(比较1,2,3,5, 1,2,3,5是否相等):'+ABBoolToStr(ABRectEqu(Rect1,Rect2)));
    ABEnRect(Rect2,1,2,3,4);
    Addlog(Memo3,'ABRectEqu(比较1,2,3,5, 1,2,3,4是否相等):'+ABBoolToStr(ABRectEqu(Rect1,Rect2)));

    ABEnSize(Size1,1,2);
    Addlog(Memo3,'ABEnSize(合并acx, acy为aSize):'+inttostr(Size1.cx)+','+
                                                     inttostr(Size1.cy)
                                                       );
    ABDeSize(Size1,AA,BB);
    Addlog(Memo3,'ABDeRect(分解aSize为acx, acy):'+inttostr(AA)+','+
                                                     inttostr(BB)
                                                          );
    ABEnSize(Size2,1,2);
    Addlog(Memo3,'ABSizeEqu(比较1,2, 1,2是否相等):'+ABBoolToStr(ABSizeEqu(Size1,Size2)));
    ABEnSize(Size2,1,3);
    Addlog(Memo3,'ABSizeEqu(比较1,2 1,3是否相等):'+ABBoolToStr(ABSizeEqu(Size1,Size2)));

    Addlog(Memo3,'ABCeil(向上取整 -123.55):'+inttostr(ABCeil(-123.55)));
    Addlog(Memo3,'ABCeil(向上取整 123.55):'+inttostr(ABCeil(123.55)));
    Addlog(Memo3,'ABFloor(向下取整 -123.55):'+inttostr(ABFloor(-123.55)));
    Addlog(Memo3,'ABFloor(向下取整 123.55):'+inttostr(ABFloor(123.55)));
    Addlog(Memo3,'ABTrunc(四舍五入后取整 -123.55):'+inttostr(ABTrunc(-123.55)));
    Addlog(Memo3,'ABTrunc(四舍五入后取整 123.45):'+inttostr(ABTrunc(123.45)));
    Addlog(Memo3,'ABTrunc(四舍五入后取整 123.55):'+inttostr(ABTrunc(123.55)));

    Addlog(Memo3,'ABRound(指定小数位数四舍五入 123.45,0):'+floattostr(ABRound(123.45,0)));
    Addlog(Memo3,'ABRound(指定小数位数四舍五入 123.45,1):'+floattostr(ABRound(123.45,1)));
    Addlog(Memo3,'ABRoundStr(指定小数位数四舍五入，返回字串，aFillZero=True时为不足的小数位置填充0 123.45,0):'+ABRoundStr(123.45,0));
    Addlog(Memo3,'ABRoundStr(指定小数位数四舍五入，返回字串，aFillZero=True时为不足的小数位置填充0 123.45,5,True):'+ABRoundStr(123.45,5,True));

    Addlog(Memo3,'ABxhl(交换高低字节 EF=239,FE=254):'+IntToStr(ABxhl(239)));
    Addlog(Memo3,'ABxhl(交换高低字节 EEFF=61183,FFEE=65518):'+IntToStr(ABxhl(61183)));
    Addlog(Memo3,'ABxhl(交换高低字节 EEEEFFFF=4008640511,FFFFEEEE=4294962926):'+IntToStr(ABxhl(4008640511)));

    Addlog(Memo3,'ABCompareDouble(比较两个浮点数1.2210000001,1.2210000002是否相等):'+ABBoolToStr(ABCompareDouble(1.2210000001,1.2210000002,8)=0));
    Addlog(Memo3,'ABCompareDouble(比较两个浮点数1.2210000001,1.2210000002是否相等):'+ABBoolToStr(ABCompareDouble(1.2210000001,1.2210000002,10)=0));
    SetLength(arr,4);
    arr[0]:=3;arr[1]:=4;arr[2]:=1;arr[3]:=2;
    ABIntArraySort(arr);
    Addlog(Memo3,'ABIntArraySort(正整数数组排序):'+ inttostr(arr[0])+','+
                                                    inttostr(arr[1])+','+
                                                    inttostr(arr[2])+','+
                                                    inttostr(arr[3])
                                                    );
    Addlog(Memo3,'ABGetGreatestCommonDivisor(求最大公约数 30,12):'+IntToStr(ABGetGreatestCommonDivisor(30,12)));
    Addlog(Memo3,'ABGetLeaseCommonMultiple(求最小公倍数 30,12):'+IntToStr(ABGetLeaseCommonMultiple(30,12)));
    Addlog(Memo3,'ABIsNumExpression(表达式是否是数字表达式 1+(2+3)*20+100/(50+50)=102):'+ABBoolToStr(ABIsNumExpression('1+(2+3)*20+100/(50+50)')));
    Addlog(Memo3,'ABIsNumExpression(表达式是否是数字表达式 aa+(2+3)*20+100/(50+50)=102):'+ABBoolToStr(ABIsNumExpression('aa+(2+3)*20+100/(50+50)')));
    Addlog(Memo3,'ABExpressionEval(四则运算表达式求值 aa+(2+3)*20+100/(50+50)=102):'+VarToStr(ABExpressionEval('1+(2+3)*20+100/(50+50)',tempBoolean)));

    Addlog(Memo3,'ABVartoInt64Def(Var转换成Int64 1234567890123456789):'+IntToStr(ABVartoInt64Def('1234567890123456789')));
    Addlog(Memo3,'ABVartoInt64Def(Var转换成Int64 123456abcd123456789):'+IntToStr(ABVartoInt64Def('123456abcd123456789')));
  end;
  procedure do3;
  begin
    Addlog(Memo3,'*******************注册表操作*****************');
    Addlog(Memo3,'ABDeleteRegPath(删除注册表路径 Software\Microsoft\Windows NT\aa\bb\cc):'+ABBoolToStr(ABDeleteRegPath('Software\Microsoft\Windows NT\aa\bb\cc')));
    Addlog(Memo3,'ABDeleteRegPath(删除注册表路径 Software\Microsoft\Windows NT\aa\bb):'+ABBoolToStr(ABDeleteRegPath('Software\Microsoft\Windows NT\aa\bb')));
    Addlog(Memo3,'ABDeleteRegPath(删除注册表路径 Software\Microsoft\Windows NT\aa):'+ABBoolToStr(ABDeleteRegPath('Software\Microsoft\Windows NT\aa')));
    Addlog(Memo3,'ABFindRegPath(检测注册表路径是否存在 Software\Microsoft\Windows NT\aa\bb\cc):'+ABBoolToStr(ABFindRegPath('Software\Microsoft\Windows NT\aa\bb\cc' )));
    Addlog(Memo3,'ABCreateRegPath(创建注册表路径 Software\Microsoft\Windows NT\aa\bb\cc):'+ABBoolToStr(ABCreateRegPath('Software\Microsoft\Windows NT\aa\bb\cc')));
    Addlog(Memo3,'ABFindRegPath(检测注册表路径是否存在 Software\Microsoft\Windows NT\aa\bb\cc):'+ABBoolToStr(ABFindRegPath('Software\Microsoft\Windows NT\aa\bb\cc' )));

    Addlog(Memo3,'ABDeleteRegKey(删除注册表键值 Software\Microsoft\Windows NT\aa\bb\cc tempName1):'+ABBoolToStr(ABDeleteRegKey('Software\Microsoft\Windows NT\aa\bb\cc','tempName1')));
    Addlog(Memo3,'ABDeleteRegKey(删除注册表键值 Software\Microsoft\Windows NT\aa\bb\cc tempName2):'+ABBoolToStr(ABDeleteRegKey('Software\Microsoft\Windows NT\aa\bb\cc','tempName2')));

    Addlog(Memo3,'ABFindRegKey(检测注册表键值是否存在 Software\Microsoft\Windows NT\aa\bb\cc rtString tempName1):'+ABBoolToStr(ABFindRegKey('Software\Microsoft\Windows NT\aa\bb\cc'       ,'tempName1')));
    Addlog(Memo3,'ABWriteRegKey(写注册表键值 Software\Microsoft\Windows NT\aa\bb\cc rtString tempName1:1):'+ABBoolToStr(ABWriteRegKey('Software\Microsoft\Windows NT\aa\bb\cc'       ,'tempName1', '1',rtString)));
    Addlog(Memo3,'ABWriteRegKey(写注册表键值 Software\Microsoft\Windows NT\aa\bb\cc rtInteger tempName2:2):'+ABBoolToStr(ABWriteRegKey('Software\Microsoft\Windows NT\aa\bb\cc'      ,'tempName2', '2',rtInteger)));
    Addlog(Memo3,'ABFindRegKey(检测注册表键值是否存在 Software\Microsoft\Windows NT\aa\bb\cc rtString tempName1):'+ABBoolToStr(ABFindRegKey('Software\Microsoft\Windows NT\aa\bb\cc'       ,'tempName1')));

    // WIN7下以兼容XP方式运行ABAppFullName
    // if AnsiCompareText(ABGetSysVersion,'windows 7')=0 then
    begin
      ABWriteRegKey('SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers',ABAppFullName, 'WINXPSP3 RUNASADMIN', rtString, HKEY_CURRENT_USER);
    end;

    Addlog(Memo3,'ABReadRegKey(读注册表键值 Software\Microsoft\Windows NT\aa\bb\cc tempName1:1):'+ABReadRegKey('Software\Microsoft\Windows NT\aa\bb\cc','tempName1'));
    Addlog(Memo3,'ABReadRegKey(读注册表键值 Software\Microsoft\Windows NT\aa\bb\cc tempName2:2):'+ABReadRegKey('Software\Microsoft\Windows NT\aa\bb\cc','tempName2'));

    ABDelAutoRun(atRunOnce,'tempatRunOnce');
    ABDelAutoRun(atRun,'tempatRun');
    Addlog(Memo3,'ABDelAutoRun(删除随系统启动的程序 atRunOnce)');
    Addlog(Memo3,'ABDelAutoRun(删除随系统启动的程序 atRun)');

    ABAddAutoRun(atRunOnce,'tempatRunOnce',Application.ExeName);
    Addlog(Memo3,'ABAddAutoRun(设置随系统启动的程序 atRunOnce,'+Application.ExeName+')');
    ABAddAutoRun(atRun,'tempatRun','C:\WINDOWS\system32\calc.exe');
    Addlog(Memo3,'ABAddAutoRun(设置随系统启动的程序 atRun,C:\WINDOWS\system32\calc.exe)');

    Addlog(Memo3,'*******************数组操作*****************');
    Addlog(Memo3,'ABStringArrayToStr(数组转成字串 [''1'',''2'',''3'']):'+ABStringArrayToStr(['1','2','3']));
    Addlog(Memo3,'ABStringArrayToStr(数组转成字串 [''1'',''2'',''3''],true,'';''):'+ABStringArrayToStr(['1','2','3'],true,';'));
    Addlog(Memo3,'ABAnsiCharArrayToStr(数组转成字串 [''1'',''2'',''3'']):'+ABAnsiCharArrayToStr(['1','2','3']));
    Addlog(Memo3,'ABAnsiCharArrayToStr(数组转成字串 [''1'',''2'',''3''],true,'';''):'+ABAnsiCharArrayToStr(['1','2','3'],true,';'));
    Addlog(Memo3,'ABWideCharArrayToStr(数组转成字串 [''1'',''2'',''3'']):'+ABWideCharArrayToStr(['1','2','3']));
    Addlog(Memo3,'ABWideCharArrayToStr(数组转成字串 [''1'',''2'',''3''],true,'';''):'+ABWideCharArrayToStr(['1','2','3'],true,';'));
    Addlog(Memo3,'ABIntegerArrayToStr(数组转成字串 [1,2,3]):'+ABIntegerArrayToStr([1,2,3]));
    Addlog(Memo3,'ABIntegerArrayToStr(数组转成字串 [1,2,3],true,'';''):'+ABIntegerArrayToStr([1,2,3],true,';'));
    Addlog(Memo3,'ABByteArrayToStr(数组转成字串 [1,2,3]):'+ABByteArrayToStr([1,2,3]));
    Addlog(Memo3,'ABByteArrayToStr(数组转成字串 [1,2,3],true,'';''):'+ABByteArrayToStr([1,2,3],true,';'));
    Addlog(Memo3,'ABDoubleArrayToStr(数组转成字串 [1.1,2.2,3.3]):'+ABDoubleArrayToStr([1.1,2.2,3.3]));
    Addlog(Memo3,'ABDoubleArrayToStr(数组转成字串 [1.1,2.2,3.3],true,'';''):'+ABDoubleArrayToStr([1.1,2.2,3.3],true,';'));
    Addlog(Memo3,'ABBooleanArrayToStr(数组转成字串 [true,false,true],true,'';''):'+ABBooleanArrayToStr([true,false,true],true,';'));
    Addlog(Memo3,'ABVariantArrayToStr(数组转成字串 [''1'',2,true],true,'';''):'+ABVariantArrayToStr(['1',2,true],true,';'));

    tempStringDynArray :=ABStrToStringArray('1,2,3');
    Addlog(Memo3,'ABStringArrayToStr(字串转成数组 ''1,2,3''):['+tempStringDynArray[0]+','+tempStringDynArray[1]+','+tempStringDynArray[2]+']');
    tempStringDynArray :=ABStrToStringArray('1;2;3',';',5,'AA');
    Addlog(Memo3,'ABStringArrayToStr(字串转成数组 ''1;2;3'','';'',5,''AA''):['+tempStringDynArray[0]+','+tempStringDynArray[1]+','+
                                                                                  tempStringDynArray[2]+','+tempStringDynArray[3]+','+
                                                                                  tempStringDynArray[4]+']');
    tempAnsiCharArray :=ABStrToAnsiCharArray('1,2,3');
    Addlog(Memo3,'ABStrToAnsiCharArray(字串转成数组 ''1,2,3''):['+widechar(tempAnsiCharArray[0])+','+widechar(tempAnsiCharArray[1])+','+widechar(tempAnsiCharArray[2])+']');
    tempAnsiCharArray :=ABStrToAnsiCharArray('1;2;3',';',5,'A');
    Addlog(Memo3,'ABStrToAnsiCharArray(字串转成数组 ''1;2;3'','';'',5,''A''):['+widechar(tempAnsiCharArray[0])+','+widechar(tempAnsiCharArray[1])+','+
                                                                                  widechar(tempAnsiCharArray[2])+','+widechar(tempAnsiCharArray[3])+','+
                                                                                  widechar(tempAnsiCharArray[4])+']');
    tempWideCharArray :=ABStrToWideCharArray('1,2,3');
    Addlog(Memo3,'ABStrToWideCharArray(字串转成数组 ''1,2,3''):['+tempWideCharArray[0]+','+tempWideCharArray[1]+','+tempWideCharArray[2]+']');
    tempWideCharArray :=ABStrToWideCharArray('1;2;3',';',5,'A');
    Addlog(Memo3,'ABStrToWideCharArray(字串转成数组 ''1;2;3'','';'',5,''A''):['+tempWideCharArray[0]+','+tempWideCharArray[1]+','+
                                                                                  tempWideCharArray[2]+','+tempWideCharArray[3]+','+
                                                                                  tempWideCharArray[4]+']');
    tempIntegerArray :=ABStrToIntegerArray('1,2,3');
    Addlog(Memo3,'ABStrToIntegerArray(字串转成数组 ''1,2,3''):['+inttostr(tempIntegerArray[0])+','+inttostr(tempIntegerArray[1])+','+inttostr(tempIntegerArray[2])+']');
    tempIntegerArray :=ABStrToIntegerArray('1;2;3',';',5,99);
    Addlog(Memo3,'ABStrToIntegerArray(字串转成数组 ''1;2;3'','';'',5,99):['+inttostr(tempIntegerArray[0])+','+inttostr(tempIntegerArray[1])+','+
                                                                                  inttostr(tempIntegerArray[2])+','+inttostr(tempIntegerArray[3])+','+
                                                                                  inttostr(tempIntegerArray[4])+']');
    tempByteArray :=ABStrToByteArray('1,2,3');
    Addlog(Memo3,'ABStrToByteArray(字串转成数组 ''1,2,3''):['+inttostr(tempByteArray[0])+','+inttostr(tempByteArray[1])+','+inttostr(tempByteArray[2])+']');
    tempByteArray :=ABStrToByteArray('1;2;3',';',5,99);
    Addlog(Memo3,'ABStrToByteArray(字串转成数组 ''1;2;3'','';'',5,99):['+inttostr(tempByteArray[0])+','+inttostr(tempByteArray[1])+','+
                                                                                  inttostr(tempByteArray[2])+','+inttostr(tempByteArray[3])+','+
                                                                                  inttostr(tempByteArray[4])+']');
    tempDoubleArray :=ABStrToDoubleArray('1,2,3');
    Addlog(Memo3,'ABStrToDoubleArray(字串转成数组 ''1,2,3''):['+FloatToStr(tempDoubleArray[0])+','+FloatToStr(tempDoubleArray[1])+','+FloatToStr(tempDoubleArray[2])+']');
    tempDoubleArray :=ABStrToDoubleArray('1;2;3',';',5,99.99);
    Addlog(Memo3,'ABStrToDoubleArray(字串转成数组 ''1;2;3'','';'',5,99.99):['+FloatToStr(tempDoubleArray[0])+','+FloatToStr(tempDoubleArray[1])+','+
                                                                                  FloatToStr(tempDoubleArray[2])+','+FloatToStr(tempDoubleArray[3])+','+
                                                                                  FloatToStr(tempDoubleArray[4])+']');
    tempBooleanArray :=ABStrToBooleanArray('1,0,1');
    Addlog(Memo3,'ABStrToBooleanArray(字串转成数组 ''1,0,1''):['+abbooltostr(tempBooleanArray[0])+','+abbooltostr(tempBooleanArray[1])+','+abbooltostr(tempBooleanArray[2])+']');
    tempBooleanArray :=ABStrToBooleanArray('1;0;1',';',5,false);
    Addlog(Memo3,'ABStrToBooleanArray(字串转成数组 ''1;0;1'','';'',5,false):['+abbooltostr(tempBooleanArray[0])+','+abbooltostr(tempBooleanArray[1])+','+
                                                                                  abbooltostr(tempBooleanArray[2])+','+abbooltostr(tempBooleanArray[3])+','+
                                                                                  abbooltostr(tempBooleanArray[4])+']');

    ABDeleteInArrayItem(['1','2','3'],['2']);
    Addlog(Memo3,'ABDeleteInArrayItem(在数组中删除另一数据的项目):');

    Addlog(Memo3,'ABVariantInArray(变量是否等于变量数组其中的某一个元素(不支持匹配选项，一般用作数值比较) 1 1,2,3):'+ABBoolToStr(ABVariantInArray(1,[1,2,3])));
    Addlog(Memo3,'ABVariantInArray(变量是否等于变量数组其中的某一个元素(不支持匹配选项，一般用作数值比较) 5 1,2,3):'+ABBoolToStr(ABVariantInArray(5,[1,2,3])));
    Addlog(Memo3,'ABStrInArray(字串是否匹配字串数组中的某一个元素 ''aAa'', [''aaa'',''bbb'',''ccc''],[]):'+inttoStr(ABStrInArray('aAa',['aaa','bbb','ccc'],[])));
    Addlog(Memo3,'ABStrInArray(字串是否匹配字串数组中的某一个元素 ''aAa'', [''aaa'',''bbb'',''ccc''],[loCaseInsensitive]):'+inttoStr(ABStrInArray('aAa',['aaa','bbb','ccc'],[loCaseInsensitive])));
    Addlog(Memo3,'ABStrInArray(字串是否匹配字串数组中的某一个元素 ''aa'', [''aaa'',''bbb'',''ccc''],[]):'+inttoStr(ABStrInArray('aa',['aaa','bbb','ccc'],[])));
    Addlog(Memo3,'ABStrInArray(字串是否匹配字串数组中的某一个元素 ''aa'', [''aaa'',''bbb'',''ccc''],[loPartialKey]):'+inttoStr(ABStrInArray('aa',['aaa','bbb','ccc'],[loPartialKey])));

    Addlog(Memo3,'ABStrsInArray(由分隔符分隔的字串所有子项是否匹配字串数组中的某一个元素 ''aAa;bBb'','';'', [''aaa'',''bbb'',''ccc''],[]):'+ABBoolToStr(ABStrsInArray('aAa;bBb',';',['aaa','bbb','ccc'],[])));
    Addlog(Memo3,'ABStrsInArray(由分隔符分隔的字串所有子项是否匹配字串数组中的某一个元素 ''aAa;bBb'','';'', [''aaa'',''bbb'',''ccc''],[loCaseInsensitive]):'+ABBoolToStr(ABStrsInArray('aAa;bBb',';',['aaa','bbb','ccc'],[loCaseInsensitive])));
    Addlog(Memo3,'ABStrsInArray(由分隔符分隔的字串所有子项是否匹配字串数组中的某一个元素 ''aa;bb'','';'', [''aaa'',''bbb'',''ccc''],[]):'+ABBoolToStr(ABStrsInArray('aa;bb',';',['aaa','bbb','ccc'],[])));
    Addlog(Memo3,'ABStrsInArray(由分隔符分隔的字串所有子项是否匹配字串数组中的某一个元素 ''aa;bb'','';'', [''aaa'',''bbb'',''ccc''],[loPartialKey]):'+ABBoolToStr(ABStrsInArray('aa;bb',';',['aaa','bbb','ccc'],[loPartialKey])));

    Addlog(Memo3,'ABArrayInStr(字串数组中的某一个元素是否匹配字串 [''aaa'',''bbb'',''ccc''],''aAa'',[]):'+ABBoolToStr(ABArrayInStr(['aaa','bbb','ccc'],'aAa',[])));
    Addlog(Memo3,'ABArrayInStr(字串数组中的某一个元素是否匹配字串 [''aaa'',''bbb'',''ccc''],''aAa'',[loCaseInsensitive]):'+ABBoolToStr(ABArrayInStr(['aaa','bbb','ccc'],'aAa',[loCaseInsensitive])));
    Addlog(Memo3,'ABArrayInStr(字串数组中的某一个元素是否匹配字串 [''aaa'',''bbb'',''ccc''],''aaaa'',[]):'+ABBoolToStr(ABArrayInStr(['aaa','bbb','ccc'],'aaaa',[])));
    Addlog(Memo3,'ABArrayInStr(字串数组中的某一个元素是否匹配字串 [''aaa'',''bbb'',''ccc''],''aaaa'',[loPartialKey]):'+ABBoolToStr(ABArrayInStr(['aaa','bbb','ccc'],'aaaa',[loPartialKey])));
  end;
  procedure do4;
  begin
    Addlog(Memo3,'*******************目录,文件操作相关*****************');
    SetLength(tempBytes,2);
    tempBytes[0]:=1;
    tempBytes[1]:=2;
    ABByteArrayWriteFile(tempBytes,2,FNeedFilePath+'aa.dat');
    Addlog(Memo3,'ABByteArrayWriteFile(将Byte数组生成文件):');

    tempBytes[0]:=1;
    tempBytes[1]:=2;
    ABByteArrayAppendFile(tempBytes,2,FNeedFilePath+'aa.dat');
    Addlog(Memo3,'ABByteArrayAppendFile(将Byte数组加到文件尾部):');

    SetLength(tempBytes,4);
    tempBytes:=ABFileToByteArray(FNeedFilePath+'aa.dat');
    ABByteArrayWriteFile(tempBytes,4,FNeedFilePath+'bb.dat');
    Addlog(Memo3,'ABFileToByteArray(将文件生成Byte数组):');

    Addlog(Memo3,'ABGetFileVersion(取文件的版本号):'+ABGetFileVersion(ABAppFullName));
    Addlog(Memo3,'ABGetDriverName(得到文件或目录的盘符):'+ABGetDriverName(ABAppFullName));

    Addlog(Memo3,'ABGetMaxVersion(取较大的版本号 1.9.0.1,1.13.0.1):'+ABGetMaxVersion('1.9.0.1','1.13.0.1'));
    Addlog(Memo3,'ABGetMinVersion(取较小的版本号 1.9.0.1,1.13.0.1):'+ABGetMinVersion('1.9.0.1','1.13.0.1'));
    Addlog(Memo3,'ABGetShortFileName(取DOS短文件名 ):'+ABGetShortFileName(ABAppFullName));
    Addlog(Memo3,'ABGetShortPath(取DOS短路径名):'+ABGetShortPath(FNeedFilePath));
    Addlog(Memo3,'ABGetFilePath(返回输入文件的路径):'+ABGetFilePath(ABAppFullName));
    Addlog(Memo3,'ABGetFileDir(返回输入文件的目录):'+ABGetFileDir(ABAppFullName));
    Addlog(Memo3,'ABGetDir(返回输入目录或路径的目录):'+ABGetDir(FNeedFilePath));
    Addlog(Memo3,'ABGetDir(返回..\..\开始的绝对目录 ..\DelphiXE2):'+ABGetDir(FNeedFilePath,'..\..\DelphiXE2'));
    Addlog(Memo3,'ABGetPath(返回输入目录或路径的路径):'+ABGetPath(FNeedFilePath));
    Addlog(Memo3,'ABGetPath(返回..\..\开始的绝对目录 ..\DelphiXE2):'+ABGetPath(FNeedFilePath,'..\..\DelphiXE2'));

    Addlog(Memo3,'ABGetParentPath(取得上级路径):'+ABGetParentPath(FNeedFilePath));
    Addlog(Memo3,'ABGetParentDir(取得上级目录):'+ABGetParentDir(FNeedFilePath));
    Addlog(Memo3,'ABGetLastDirName(取得最后一级目录名称):'+ABGetLastDirName(FNeedFilePath));

    Addlog(Memo3,'ABGetFileFullName(得到当前目录):'+ABGetCurrentDir);
    Addlog(Memo3,'ABGetFileFullName(返回包含绝对文件名的完整文件名字符串(当前目录路径+aFileName)):'+ABGetFileFullName('aa.exe'));
    ABSetCurrentDir('C:\WINDOWS');
    Addlog(Memo3,'ABSetCurrentDir(设置当前目录 C:\WINDOWS):');
    Addlog(Memo3,'ABGetFileFullName(返回包含绝对文件名的完整文件名字符串(当前目录路径+aFileName)):'+ABGetFileFullName('aa.exe'));

    Addlog(Memo3,'ABGetFilename(仅返回文件名 aIsDelExt=True):'+ABGetFilename(ABAppFullName,true));
    Addlog(Memo3,'ABGetFilename(仅返回文件名 aIsDelExt=False):'+ABGetFilename(ABAppFullName,False));
    Addlog(Memo3,'ABGetFileExt(返回文件的扩展名 aIsDelDian=True):'+ABGetFileExt(ABAppFullName,true));
    Addlog(Memo3,'ABGetFileExt(返回文件的扩展名 aIsDelDian=False):'+ABGetFileExt(ABAppFullName,False));
    Addlog(Memo3,'ABGetURLFileName(返回URL下载地址的文件名 http://www.delphifans.com/dl.asp):'+ABGetURLFileName('http://www.delphifans.com/dl.asp'));

    Addlog(Memo3,'ABSelectDirectory(选择目录对话框):'+ABSelectDirectory);
    Addlog(Memo3,'ABSelectFile(选择文件对话框):'+ABSelectFile);
    Addlog(Memo3,'ABSaveFile(保存文件对话框):'+ABSaveFile);
    SaveDialog1.Execute;
    Addlog(Memo3,'ABGetSaveDialogFileName(得到保存对话框的文件名):'+ABGetSaveDialogFileName(SaveDialog1));
    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath)));
    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+''aabb''):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aabb')));
    Addlog(Memo3,'ABCheckFileExists(检查文件是否存在 ABAppFullName):'+ABBoolToStr(ABCheckFileExists(ABAppFullName)));
    Addlog(Memo3,'ABCheckFileExists(检查文件是否存在 ABAppFullName+''aabb''):'+ABBoolToStr(ABCheckFileExists(ABAppFullName+'aabb')));

    Addlog(Memo3,'ABGetDirCount(取一目录下目录数 FNeedFilePath):'+inttostr(ABGetDirCount(FNeedFilePath,[])));
    Addlog(Memo3,'ABGetDirCount(取一目录下目录数 FNeedFilePath,[],1):'+inttostr(ABGetDirCount(FNeedFilePath,[],1)));
    Addlog(Memo3,'ABGetDirCount(取一目录下目录数 FNeedFilePath,[*_Set*]):'+inttostr(ABGetDirCount(FNeedFilePath,['*_Set*'])));
    Addlog(Memo3,'ABGetDirCount(取一目录下目录数 FNeedFilePath[*_Set*,*Public*]):'+inttostr(ABGetDirCount(FNeedFilePath,['*Public*','*_Set*'])));
    Addlog(Memo3,'ABGetFileCount(取一目录下的文件数 FNeedFilePath):'+inttostr(ABGetFileCount(FNeedFilePath,[])));
    Addlog(Memo3,'ABGetFileCount(取一目录下的文件数 FNeedFilePath,[],1):'+inttostr(ABGetFileCount(FNeedFilePath,[],1)));
    Addlog(Memo3,'ABGetFileCount(取一目录下的文件数 FNeedFilePath,[*.ini*]):'+inttostr(ABGetFileCount(FNeedFilePath,['*.ini*'])));
    Addlog(Memo3,'ABGetFileCount(取一目录下的文件数 FNeedFilePath,[*.ini*,*.exe*]):'+inttostr(ABGetFileCount(FNeedFilePath,['*.ini*','*.exe*'])));
    Addlog(Memo3,'ABGetFileCount(取一目录下的文件数 FNeedFilePath,[*.tmp*],false):'+inttostr(ABGetFileCount(FNeedFilePath,['*tmp*'],-1,false)));
    Addlog(Memo3,'ABGetFileCount(取一目录下的文件数 FNeedFilePath,[*.tmp*],True):'+inttostr(ABGetFileCount(FNeedFilePath,['*tmp*'],-1,True)));

    Memo4.Clear;
    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetDirCount(FNeedFilePath,[])***************');
    ProgressBar2.Max:=ABGetDirCount(FNeedFilePath,[]);
    ABGetDirList(temStrings,FNeedFilePath,[],-1,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetDirCount(FNeedFilePath,[],false)***************');
    ProgressBar2.Max:=ABGetDirCount(FNeedFilePath,[]);
    ABGetDirList(temStrings,FNeedFilePath,[],-1,ProgressBar2,false);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetDirCount(FNeedFilePath,[],1)***************');
    ProgressBar2.Max:=ABGetDirCount(FNeedFilePath,[],1);
    ABGetDirList(temStrings,FNeedFilePath,[],1,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetDirCount(FNeedFilePath,[*_Set*])***************');
    ProgressBar2.Max:=ABGetDirCount(FNeedFilePath,['*_Set*']);
    ABGetDirList(temStrings,FNeedFilePath,['*_Set*'],-1,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetDirCount(FNeedFilePath,[*_Set*,*Public*])***************');
    ProgressBar2.Max:=ABGetDirCount(FNeedFilePath,['*Public*','*_Set*']);
    ABGetDirList(temStrings,FNeedFilePath,['*Public*','*_Set*'],-1,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetFileList(FNeedFilePath,[])***************');
    ProgressBar2.Max:=ABGetFileCount(FNeedFilePath,[]);
    ABGetFileList(temStrings,FNeedFilePath,[],-1,false,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetFileList(FNeedFilePath,[],false)***************');
    ProgressBar2.Max:=ABGetFileCount(FNeedFilePath,[]);
    ABGetFileList(temStrings,FNeedFilePath,[],-1,false,ProgressBar2,false);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetFileList(FNeedFilePath,[],1)***************');
    ProgressBar2.Max:=ABGetFileCount(FNeedFilePath,[],1);
    ABGetFileList(temStrings,FNeedFilePath,[],1,false,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetFileList(FNeedFilePath,[*_Set*])***************');
    ProgressBar2.Max:=ABGetFileCount(FNeedFilePath,['*_Set*']);
    ABGetFileList(temStrings,FNeedFilePath,['*_Set*'],-1,false,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetFileList(FNeedFilePath,[*_Set*,*Public*])***************');
    ProgressBar2.Max:=ABGetFileCount(FNeedFilePath,['*Public*','*_Set*']);
    ABGetFileList(temStrings,FNeedFilePath,['*Public*','*_Set*'],-1,false,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetFileList(FNeedFilePath,[*tmp*],false)***************');
    ProgressBar2.Max:=ABGetFileCount(FNeedFilePath,['*tmp*']);
    ABGetFileList(temStrings,FNeedFilePath,['*tmp*'],-1,false,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);

    temStrings.Clear;
    Memo4.Lines.Add('***********ABGetFileList(FNeedFilePath,[*tmp*],True)***************');
    ProgressBar2.Max:=ABGetFileCount(FNeedFilePath,['*tmp*']);
    ABGetFileList(temStrings,FNeedFilePath,['*tmp*'],-1,True,ProgressBar2);
    Memo4.Lines.AddStrings(temStrings);
    ProgressBar2.Position:=0;


    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\bb\cc):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\bb\cc')));
    ABCreateDir(FNeedFilePath+'aa\bb\cc');
    Addlog(Memo3,'ABCreateDir(创建目录 FNeedFilePath+aa\bb\cc):');
    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\bb\cc):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\bb\cc')));
    ABCreateEmptystrDir(FNeedFilePath+'aa\bb\cc');
    Addlog(Memo3,'ABCreateEmptystrDir(创建目录并置空 FNeedFilePath+aa\bb\cc):');

    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\bb\ccNew):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\bb\ccNew')));
    ABEditDir(FNeedFilePath+'aa\bb\cc',FNeedFilePath+'aa\bb\ccNew');
    Addlog(Memo3,'ABEditDir(修改目录名称 FNeedFilePath+aa\bb\cc,FNeedFilePath+aa\bb\ccNew):');
    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\bb\ccNew):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\bb\ccNew')));

    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\bb\ccCopy):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\bb\ccCopy')));
    ABCopyDir(FNeedFilePath+'aa\bb\ccNew',FNeedFilePath+'aa\bb\ccCopy');
    Addlog(Memo3,'ABCopyDir(拷贝目录 FNeedFilePath+aa\bb\ccNew,FNeedFilePath+aa\bb\ccCopy):');
    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\bb\ccCopy):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\bb\ccCopy')));

    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\cc):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\cc')));
    ABMoveDir(FNeedFilePath+'aa\bb\ccNew',FNeedFilePath+'aa\cc');
    Addlog(Memo3,'ABMoveDir(移动目录 FNeedFilePath+aa\bb\ccNew,FNeedFilePath+aa\cc):');
    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa\cc):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa\cc')));

    ABDeltree(FNeedFilePath+'aa');
    Addlog(Memo3,'ABDeltree(删除目录 FNeedFilePath+aa):');
    Addlog(Memo3,'ABCheckDirExists(检测目录是否存在 FNeedFilePath+aa):'+ABBoolToStr(ABCheckDirExists(FNeedFilePath+'aa')));

    ABCopyFile(ABAppFullName,FNeedFilePath+'aa.exe');
    Addlog(Memo3,'ABDeleteFile(删除文件 FNeedFilePath+aa.exe):'+ABBoolToStr(ABDeleteFile(FNeedFilePath+'aa.exe')));
    ABCopyFile(ABAppFullName,FNeedFilePath+'aa.exe');
    Addlog(Memo3,'ABDeleteToRecycleBin(删除文件到回收站 FNeedFilePath+cc.exe):'+ABBoolToStr(ABDeleteToRecycleBin(FNeedFilePath+'aa.exe')));
    Addlog(Memo3,'ABCopyFile(不支持进度条方式拷贝文件 ABAppFullName,FNeedFilePath+cc.exe):'+ABBoolToStr(ABCopyFile(ABAppFullName,FNeedFilePath+'aa.exe')));
    Addlog(Memo3,'ABCopyFile(支持进度条方式拷贝文件 FNeedFilePath+cc.exe,FNeedFilePath+bb.exe):'+ABBoolToStr(ABCopyFile(FNeedFilePath+'aa.exe',FNeedFilePath+'bb.exe',ProgressBar2)));
    Addlog(Memo3,'ABMoveFile(移动文件 FNeedFilePath+cc.exe,FNeedFilePath+cc.exe.exe):'+ABBoolToStr(ABMoveFile(FNeedFilePath+'aa.exe',FNeedFilePath+'cc.exe')));
    Addlog(Memo3,'ABMoveFileToSubPath(移动文件 FNeedFilePath,bb.exe,PkgBack):'+ABBoolToStr(ABMoveFileToSubPath(FNeedFilePath,'bb.exe','PkgBack')));

    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,tempCreationTime, tempLastWriteTime,tempLastAccessTime):'+ABBoolToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',tempCreationTime, tempLastWriteTime,tempLastAccessTime)));
    Addlog(Memo3,'创建时间:'+ABDateTimeToStr(tempCreationTime,22));
    Addlog(Memo3,'更新时间:'+ABDateTimeToStr(tempLastWriteTime,22));
    Addlog(Memo3,'最后访问时间:'+ABDateTimeToStr(tempLastAccessTime,22));
    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,fttCreation):'+ABDateTimeToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',fttCreation),22));
    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,fttLastWrite):'+ABDateTimeToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',fttLastWrite),22));
    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,fttLastAccess):'+ABDateTimeToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',fttLastAccess),22));

    tempCreationTime:=tempCreationTime+1;
    tempLastWriteTime:=tempLastWriteTime+1;
    tempLastAccessTime:=tempLastAccessTime+1;
    Addlog(Memo3,'ABSetFileDateTime(设置文件时间 ABAppFullName,tempCreationTime+1, tempLastWriteTime+1,tempLastAccessTime+1):'+ABBoolToStr(ABSetFileDateTime(FNeedFilePath+'cc.exe',tempCreationTime, tempLastWriteTime,tempLastAccessTime)));
    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,fttCreation):'+ABDateTimeToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',fttCreation),22));
    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,fttLastWrite):'+ABDateTimeToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',fttLastWrite),22));
    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,fttLastAccess):'+ABDateTimeToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',fttLastAccess),22));

    tempCreationTime:=tempCreationTime+1;
    tempLastWriteTime:=tempLastWriteTime+1;
    tempLastAccessTime:=tempLastAccessTime+1;
    ABSetFileDateTime(FNeedFilePath+'cc.exe',fttCreation,tempCreationTime);
    ABSetFileDateTime(FNeedFilePath+'cc.exe',fttLastWrite,tempLastWriteTime);
    ABSetFileDateTime(FNeedFilePath+'cc.exe',fttLastAccess,tempLastAccessTime);
    Addlog(Memo3,'读取或设置文件的拍照及其它时间:'+ABReadOrEditFileAllDatetime(ABAppFullName,0));
    Addlog(Memo3,'ABSetFileDateTime(设置文件时间 ABAppFullName,tempCreationTime+1, tempLastWriteTime+1,tempLastAccessTime+1):');
    Addlog(Memo3,'ABGetFileDateTime(取文件时间 ABAppFullName,tempCreationTime, tempLastWriteTime,tempLastAccessTime):'+ABBoolToStr(ABGetFileDateTime(FNeedFilePath+'cc.exe',tempCreationTime, tempLastWriteTime,tempLastAccessTime)));
    Addlog(Memo3,'创建时间:'+ABDateTimeToStr(tempCreationTime,22));
    Addlog(Memo3,'更新时间:'+ABDateTimeToStr(tempLastWriteTime,22));
    Addlog(Memo3,'最后访问时间:'+ABDateTimeToStr(tempLastAccessTime,22));

    ABShowEditFileProperties(FNeedFilePath+'aa.mdb');
    Addlog(Memo3,'ABShowEditFileProperties(打开文件属性窗口FNeedFilePath+aa.mdb):');

    tempFileAttr:=ABGetFileAttr(FNeedFilePath+'cc.exe');
    Addlog(Memo3,'ABGetFileAttr(取得文件属性 FNeedFilePath):'+inttostr(tempFileAttr));

    ABSetFileAttr(FNeedFilePath+'cc.exe',sysutils.faReadOnly,ftInsert);
    Addlog(Memo3,'ABSetFileAttr(设置文件属性 FNeedFilePath,faReadOnly,ftInsert):');
    tempFileAttr:=ABGetFileAttr(FNeedFilePath+'cc.exe');
    Addlog(Memo3,'ABGetFileAttr(取得文件属性 FNeedFilePath):'+inttostr(tempFileAttr));

    ABSetFileAttr(FNeedFilePath+'cc.exe',sysutils.faHidden,ftInsert);
    Addlog(Memo3,'ABSetFileAttr(设置文件属性 FNeedFilePath,faHidden,ftInsert):');
    tempFileAttr:=ABGetFileAttr(FNeedFilePath+'cc.exe');
    Addlog(Memo3,'ABGetFileAttr(取得文件属性 FNeedFilePath):'+inttostr(tempFileAttr));

    ABSetFileAttr(FNeedFilePath+'cc.exe',sysutils.faHidden,ftDelete);
    Addlog(Memo3,'ABSetFileAttr(设置文件属性 FNeedFilePath,faHidden,ftDelete):');
    tempFileAttr:=ABGetFileAttr(FNeedFilePath+'cc.exe');
    Addlog(Memo3,'ABGetFileAttr(取得文件属性 FNeedFilePath):'+inttostr(tempFileAttr));

    ABSetFileAttr(FNeedFilePath+'cc.exe',sysutils.faReadOnly,ftDelete);
    Addlog(Memo3,'ABSetFileAttr(设置文件属性 FNeedFilePath,faReadOnly,ftDelete):');
    tempFileAttr:=ABGetFileAttr(FNeedFilePath+'cc.exe');
    Addlog(Memo3,'ABGetFileAttr(取得文件属性 FNeedFilePath):'+inttostr(tempFileAttr));

    ABSetFileAttr(FNeedFilePath+'cc.exe',sysutils.faReadOnly,ftEdit);
    Addlog(Memo3,'ABSetFileAttr(设置文件属性 FNeedFilePath,faReadOnly,ftEdit):');
    tempFileAttr:=ABGetFileAttr(FNeedFilePath+'cc.exe');
    Addlog(Memo3,'ABGetFileAttr(取得文件属性 FNeedFilePath):'+inttostr(tempFileAttr));

    ABSetFileAttr(FNeedFilePath+'cc.exe',sysutils.faHidden or sysutils.faReadOnly,ftEdit);
    Addlog(Memo3,'ABSetFileAttr(设置文件属性 FNeedFilePath,faHidden or faReadOnly,ftEdit):');
    tempFileAttr:=ABGetFileAttr(FNeedFilePath+'cc.exe');
    Addlog(Memo3,'ABGetFileAttr(取得文件属性 FNeedFilePath):'+inttostr(tempFileAttr));

    Addlog(Memo3,'ABFileInUse(判断文件是否正在使用 FNeedFilePath+aa.mdb):'+ABBoolToStr(ABFileInUse(FNeedFilePath+'aa.mdb')));
    Addlog(Memo3,'ABFileInUse(判断文件是否正在使用 ABAppFullName):'+ABBoolToStr(ABFileInUse(ABAppFullName)));
    Addlog(Memo3,'ABGetFileSize(取文件大小 ABAppFullName):'+inttostr(ABGetFileSize(ABAppFullName)));
    Addlog(Memo3,'ABGetFileSizeStr(取文件大小字串(**GB/MB/KB/B) ABAppFullName):'+ABGetFileSizeStr(ABGetFileSize(ABAppFullName)));

    Addlog(Memo3,'ABGetTextFileType(返回文本文件编码类型 FNeedFilePath+ansi.txt):'+GetEnumName(TypeInfo(TABTxtFileType), Ord(ABGetTextFileType(FNeedFilePath+'ansi.txt',tempText)))+' 内容:'+tempText);
    Addlog(Memo3,'ABGetTextFileType(返回文本文件编码类型 FNeedFilePath+Unicode.txt):'+GetEnumName(TypeInfo(TABTxtFileType), Ord(ABGetTextFileType(FNeedFilePath+'Unicode.txt',tempText)))+' 内容:'+tempText);
    Addlog(Memo3,'ABGetTextFileType(返回文本文件编码类型 FNeedFilePath+Unicode big endian.txt):'+GetEnumName(TypeInfo(TABTxtFileType), Ord(ABGetTextFileType(FNeedFilePath+'Unicode big endian.txt',tempText)))+' 内容:'+tempText);
    Addlog(Memo3,'ABGetTextFileType(返回文本文件编码类型 FNeedFilePath+Utf-8.txt):'+GetEnumName(TypeInfo(TABTxtFileType), Ord(ABGetTextFileType(FNeedFilePath+'Utf-8.txt',tempText)))+' 内容:'+tempText);

    Addlog(Memo3,'ABIsTextFile(文件是否是文本文件 FNeedFilePath+Utf-8.txt):'+ABBoolToStr(ABIsTextFile(FNeedFilePath+'Utf-8.txt')));
    Addlog(Memo3,'ABIsTextFile(文件是否是文本文件 FNeedFilePath+aa.mdb):'+ABBoolToStr(ABIsTextFile(FNeedFilePath+'aa.mdb')));

    Addlog(Memo3,'ABReadTxtFirstRow(兼容多种格式的文件文件读首行 FNeedFilePath+ansi.txt):'+ABReadTxtFirstRow(FNeedFilePath+'ansi.txt'));
    Addlog(Memo3,'ABReadTxtFirstRow(兼容多种格式的文件文件读首行 FNeedFilePath+Unicode.txt):'+ABReadTxtFirstRow(FNeedFilePath+'Unicode.txt'));
    Addlog(Memo3,'ABReadTxtFirstRow(兼容多种格式的文件文件读首行 FNeedFilePath+Unicode big endian.txt):'+ABReadTxtFirstRow(FNeedFilePath+'Unicode big endian.txt'));
    Addlog(Memo3,'ABReadTxtFirstRow(兼容多种格式的文件文件读首行 FNeedFilePath+Utf-8.txt):'+ABReadTxtFirstRow(FNeedFilePath+'Utf-8.txt'));

    Addlog(Memo3,'ABReadTxt(兼容多种格式的文件文件读 FNeedFilePath+ansi.txt):'+ABReadTxt(FNeedFilePath+'ansi.txt'));
    Addlog(Memo3,'ABReadTxt(兼容多种格式的文件文件读 FNeedFilePath+Unicode.txt):'+ABReadTxt(FNeedFilePath+'Unicode.txt'));
    Addlog(Memo3,'ABReadTxt(兼容多种格式的文件文件读 FNeedFilePath+Unicode big endian.txt):'+ABReadTxt(FNeedFilePath+'Unicode big endian.txt'));
    Addlog(Memo3,'ABReadTxt(兼容多种格式的文件文件读 FNeedFilePath+Utf-8.txt):'+ABReadTxt(FNeedFilePath+'Utf-8.txt'));

    ABWriteTxt(FNeedFilePath+'ansi.txt','123abc中国人',tfAnsi);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件写 FNeedFilePath+ansi.txt,123abc中国人,tfAnsi):');
    ABWriteTxt(FNeedFilePath+'Unicode.txt','123abc中国人',tfUnicode);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件写 FNeedFilePath+Unicode.txt,123abc中国人,tfUnicode):');
    ABWriteTxt(FNeedFilePath+'Unicode big endian.txt','123abc中国人',tfUnicodeBigEndian);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件写 FNeedFilePath+Unicode big endian.txt,123abc中国人,tfUnicodeBigEndian):');
    ABWriteTxt(FNeedFilePath+'Utf-8.txt','123abc中国人',tfUtf8);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件写 FNeedFilePath+Utf-8.txt,123abc中国人,tfUtf8):');

    ABWriteTxt(FNeedFilePath+'ansi.txt','123abc中国人',tfAnsi,true);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件追回写 FNeedFilePath+ansi.txt,123abc中国人,tfAnsi):');
    ABWriteTxt(FNeedFilePath+'Unicode.txt','123abc中国人',tfUnicode,true);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件追回写 FNeedFilePath+Unicode.txt,123abc中国人,tfUnicode):');
    ABWriteTxt(FNeedFilePath+'Unicode big endian.txt','123abc中国人',tfUnicodeBigEndian,true);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件追回写 FNeedFilePath+Unicode big endian.txt,123abc中国人,tfUnicodeBigEndian):');
    ABWriteTxt(FNeedFilePath+'Utf-8.txt','123abc中国人',tfUtf8,true);
    Addlog(Memo3,'ABWriteTxt(兼容多种格式的文件文件追回写 FNeedFilePath+Utf-8.txt,123abc中国人,tfUtf8):');

    Addlog(Memo3,'ABGetTxtline(返回一文本文件的行数 FNeedFilePath+ansi.txt):'+inttostr(ABGetTxtline(FNeedFilePath+'ansi.txt')));
    Addlog(Memo3,'ABGetTxtline(返回一文本文件的行数 FNeedFilePath+Unicode.txt):'+inttostr(ABGetTxtline(FNeedFilePath+'Unicode.txt')));
    Addlog(Memo3,'ABGetTxtline(返回一文本文件的行数 FNeedFilePath+Unicode big endian.txt):'+inttostr(ABGetTxtline(FNeedFilePath+'Unicode big endian.txt')));
    Addlog(Memo3,'ABGetTxtline(返回一文本文件的行数 FNeedFilePath+Utf-8.txt):'+inttostr(ABGetTxtline(FNeedFilePath+'Utf-8.txt')));

    ABDeleteIni('setup',FNeedFilePath+'ansi.txt');
    ABDeleteIni('Param',FNeedFilePath+'ansi.txt');
    ABDeleteIni('setup',FNeedFilePath+'Unicode.txt');
    ABDeleteIni('Param',FNeedFilePath+'Unicode.txt');
    temStrings.Text:='aa=66'+ABEnterWrapStr+'bb=77';
    ABWriteInI('setup',FNeedFilePath+'ansi.txt',temStrings);
    ABWriteInI('setup',FNeedFilePath+'Unicode.txt',temStrings);

    Addlog(Memo3,'ABReadInI(INI文件读 FNeedFilePath+ansi.txt aa):'+ABReadInI('setup','aa',FNeedFilePath+'ansi.txt'));
    Addlog(Memo3,'ABReadInI(INI文件读 FNeedFilePath+ansi.txt cc,11,22):'+ABReadInI('setup','cc',FNeedFilePath+'ansi.txt','11','22'));
    Addlog(Memo3,'ABReadInI(INI文件读 FNeedFilePath+Unicode.txt aa):'+ABReadInI('setup','aa',FNeedFilePath+'Unicode.txt'));
    Addlog(Memo3,'ABReadInI(INI文件读 FNeedFilePath+Unicode.txt cc,11,22):'+ABReadInI('setup','cc',FNeedFilePath+'Unicode.txt','11','22'));

    Addlog(Memo3,'ABReadInI(批量读取多组下项的方法 FNeedFilePath+ansi.txt [setup],[aa,bb,cc,dd]):'+ABReadInI(['setup'],['aa','bb','cc','dd'],FNeedFilePath+'ansi.txt'));
    Addlog(Memo3,'ABReadInI(批量读取多组下项的方法 FNeedFilePath+Unicode.txt [setup],[aa,bb,cc,dd]):'+ABReadInI(['setup'],['aa','bb','cc','dd'],FNeedFilePath+'Unicode.txt'));

    ABReadInI('setup',FNeedFilePath+'ansi.txt',temStrings);
    Addlog(Memo3,'ABReadInI(批量读取组下所有项到aStrings中 FNeedFilePath+ansi.txt setup):'+temStrings.Text);
    ABReadInI('setup',FNeedFilePath+'Unicode.txt',temStrings);
    Addlog(Memo3,'ABReadInI(批量读取组下所有项到aStrings中 FNeedFilePath+Unicode.txt setup):'+temStrings.Text);

    ABWriteInI('setup','aa','33',FNeedFilePath+'ansi.txt');
    ABWriteInI('setup','aa','33',FNeedFilePath+'Unicode.txt');
    Addlog(Memo3,'ABWriteInI(INI文件写 FNeedFilePath+ansi.txt,setup,aa=33):');
    Addlog(Memo3,'ABWriteInI(INI文件写 FNeedFilePath+Unicode.txt,setup,aa=33):');
    ABWriteInI(['setup','setup','Param'],['aa','bb','aa'],['11','22','33'],FNeedFilePath+'ansi.txt');
    ABWriteInI(['setup','setup','Param'],['aa','bb','aa'],['11','22','33'],FNeedFilePath+'Unicode.txt');
    Addlog(Memo3,'ABWriteInI(批量写多组下项的方法 FNeedFilePath+ansi.txt,[setup,setup,Param],[aa,bb,aa],[11,22,33]):');
    Addlog(Memo3,'ABWriteInI(批量写多组下项的方法 FNeedFilePath+Unicode.txt,[setup,setup,Param],[aa,bb,aa],[11,22,33]):');

    temStrings.Text:='aa=66'+ABEnterWrapStr+'bb=77'+ABEnterWrapStr+'cc=88';
    ABWriteInI('setup',FNeedFilePath+'ansi.txt',temStrings);
    ABWriteInI('setup',FNeedFilePath+'Unicode.txt',temStrings);
    Addlog(Memo3,'ABWriteInI(批量写aStrings下所有项到某一组中 FNeedFilePath+ansi.txt,setup,aa=66,bb=77,cc=88):');
    Addlog(Memo3,'ABWriteInI(批量写aStrings下所有项到某一组中 FNeedFilePath+Unicode.txt,setup,aa=66,bb=77,cc=88):');

    Addlog(Memo3,'ABCheckInI(检测INI某项是否存在 FNeedFilePath+ansi.txt,setup,aa):'+ABBoolToStr(ABCheckInI('setup','aa',FNeedFilePath+'ansi.txt')));
    Addlog(Memo3,'ABCheckInI(检测INI某项是否存在 FNeedFilePath+ansi.txt,setup,ww):'+ABBoolToStr(ABCheckInI('setup','ww',FNeedFilePath+'ansi.txt')));
    Addlog(Memo3,'ABCheckInI(检测INI某项是否存在 FNeedFilePath+Unicode.txt,setup,aa):'+ABBoolToStr(ABCheckInI('setup','aa',FNeedFilePath+'Unicode.txt')));
    Addlog(Memo3,'ABCheckInI(检测INI某项是否存在 FNeedFilePath+Unicode.txt,setup,ww):'+ABBoolToStr(ABCheckInI('setup','ww',FNeedFilePath+'Unicode.txt')));

    ABDeleteIni('setup','aa',FNeedFilePath+'ansi.txt');
    Addlog(Memo3,'ABDeleteIni(删除组下某项 FNeedFilePath+ansi.txt,setup,aa):');
    ABDeleteIni('setup','aa',FNeedFilePath+'Unicode.txt');
    Addlog(Memo3,'ABDeleteIni(删除组下某项 FNeedFilePath+Unicode.txt,setup,aa):');
    ABDeleteIni('setup',FNeedFilePath+'ansi.txt');
    Addlog(Memo3,'ABDeleteIni(删除组下所有项 FNeedFilePath+ansi.txt,setup):');
    ABDeleteIni('setup',FNeedFilePath+'Unicode.txt');
    Addlog(Memo3,'ABDeleteIni(删除组下所有项 FNeedFilePath+Unicode.txt,setup):');

    temStrings.Text:='[setup]'+ABEnterWrapStr+'aa=66'+ABEnterWrapStr+'bb=77';
    Addlog(Memo3,'ABReadIniInStrings(读TStrings中组aGroup下的aName的值 temStrings setup,aa):'+ABReadIniInStrings('setup','aa',temStrings));
    Addlog(Memo3,'ABReadIniInStrings(读TStrings中组aGroup下的aName的值 temStrings setup,cc,11,22):'+ABReadIniInStrings('setup','cc',temStrings,'11','22'));

    temStrings.Text:='[setup]'+ABEnterWrapStr+'aa=66'+ABEnterWrapStr+'bb=77';
    ABReadIniInStrings('setup',temStrings,temStrings_1);
    Addlog(Memo3,'ABReadIniInStrings(批量读取组下所有项到aOutStrings中 setup,temStrings ,temStrings_1):'+temStrings_1.Text);

    ABWriteIniInStrings('setup','aa','00',temStrings);
    Addlog(Memo3,'ABWriteIniInStrings(写TStrings中组aGroup下的aName的值 temStrings setup,aa,00):'+temStrings.Text);
    ABWriteItemInStrings('aa','00',temStrings);
    Addlog(Memo3,'ABWriteItemInStrings(写TStrings中aName的值 temStrings aa,00):'+temStrings.Text);

    temptext:= '[setup]'+ABEnterWrapStr+'aa=66'+ABEnterWrapStr+'bb=77';
    Addlog(Memo3,'ABReadIniInText(读字串中组aGroup下的aName的值 temptext setup,aa):'+ABReadIniInText('setup','aa',temptext));
    Addlog(Memo3,'ABReadIniInText(读字串中组aGroup下的aName的值 temptext setup,cc,11,22):'+ABReadIniInText('setup','cc',temptext,'11','22'));

    Addlog(Memo3,'ABReadItemInTxt(读字串中aName的值 temptext aa):'+ABReadItemInTxt('setup',temptext));
    ABWriteIniInText('setup','aa','00',temptext);
    Addlog(Memo3,'ABWriteIniInText(写字串中组aGroup下的aName的值 temptext setup,aa,00):'+temptext);

    ABDeleteFile(FNeedFilePath+'aa.mdb');
    Addlog(Memo3,'ABCheckFileExists(检查文件是否存在 FNeedFilePath+aa.mdb):'+ABBoolToStr(ABCheckFileExists(FNeedFilePath+'aa.mdb')));
    ABCreateMDB(FNeedFilePath+'aa.mdb');
    Addlog(Memo3,'ABCreateMDB(创建ACCESS文件):');
    Addlog(Memo3,'ABCheckFileExists(检查文件是否存在 FNeedFilePath+aa.mdb):'+ABBoolToStr(ABCheckFileExists(FNeedFilePath+'aa.mdb')));

    Addlog(Memo3,'ABOpenWith(打开文件 FNeedFilePath+Utf-8.txt):'+inttostr(ABOpenWith(FNeedFilePath+'Utf-8.txt')));

    Addlog(Memo3,'ABMergerExcel_OLE(将来源文件指定SHEET页合并到新EXCEL文件):'+ABBoolToStr(
                    ABMergerExcel_OLE([FNeedFilePath+'Excel1.xls',FNeedFilePath+'Excel2.xls',FNeedFilePath+'Excel3.xls'],[],
                                  FNeedFilePath+'ExcelAll1.xls',[],
                                  temptext,
                                  false,false,false
                                  )
                    ));

    Addlog(Memo3,'ABMergerExcel_OLE(将来源文件指定SHEET页合并到新EXCEL文件):'+ABBoolToStr(
                    ABMergerExcel_OLE([FNeedFilePath+'Excel1.xls',FNeedFilePath+'Excel2.xls',FNeedFilePath+'Excel3.xls'],['sheet1'],
                                  FNeedFilePath+'ExcelAll1.xls',['sheet_1_1','sheet_2_1,sheet_2_2,sheet_2_3','sheet_3_1,sheet_3_2,sheet_3_3'],
                                  temptext,
                                  true,false,false
                                  )
                    ));
    Addlog(Memo3,'ABMergerExcel_OLE(将来源文件指定SHEET页合并到新EXCEL文件):'+ABBoolToStr(
                    ABMergerExcel_OLE([FNeedFilePath+'Excel1.xls',FNeedFilePath+'Excel2.xls',FNeedFilePath+'Excel3.xls'],['sheet1'],
                                  FNeedFilePath+'ExcelAll2.xls',['sheet_1_1','sheet_2_1,sheet_2_2,sheet_2_3','sheet_3_1,sheet_3_2,sheet_3_3'],
                                  temptext,
                                  false,false,false
                                  )
                    ));

  end;
  procedure do5;
  begin

  end;
  procedure do6;
  begin

  end;
begin
  do1;
  do2;
  do3;
  do4;
  do5;
  do6;

  Addlog(Memo3,'*******************图像操作*****************');

  ABOperateBitmapBright(Image_bmp.Picture.Bitmap);
  Addlog(Memo3,'ABOperateBitmapBright(操作图片亮度):');
  ABOperateBitmapSaturation(Image_bmp.Picture.Bitmap);
  Addlog(Memo3,'ABOperateBitmapSaturation(操作图片饱和度):');
  ABOperateBitmapContrast(Image_bmp.Picture.Bitmap);
  Addlog(Memo3,'ABOperateBitmapContrast(操作图片对比度):');


  ABBlackWhiteBitmap(Image_bmp.Picture.Bitmap,Image_bmp.Picture.Bitmap);
  Addlog(Memo3,'ABBlackWhiteBitmap(二值化图片):');
  ABGrayBitmap(Image_bmp.Picture.Bitmap,Image_bmp.Picture.Bitmap);
  Addlog(Memo3,'ABGrayBitmap(灰化图片):');

  ABBitmapToString(Image_bmp.Picture.Bitmap);
  Addlog(Memo3,'ABBitmapToString(图片转化为01字串):');


  ABBlackWhiteImageList(FNeedFilePath,ImageList1,gbtWeightedAvg,TrackBar1.Position);
  Addlog(Memo3,'ABBlackWhiteImageList(二值化ImageList中所有图片到文件目录中文件 (FNeedFilePath,gbtWeightedAvg)):');
  ABGrayImageList(FNeedFilePath,ImageList1,gbtWeightedAvg,TrackBar1.Position);
  Addlog(Memo3,'ABGrayImageList(灰化ImageList中所有图片到文件目录中文件 (FNeedFilePath,gbtWeightedAvg)):');

  Image_jpg.Picture.SaveToFile(FNeedFilePath+'aa.jpg');
  ABJpgFileToBmpFile(FNeedFilePath+'aa.jpg',FNeedFilePath+'bb.bmp');
  Addlog(Memo3,'ABJpgFileToBmpFile(JPG文件与BMP文件互转 aa.jpg,bb.bmp):');
  ABBmpFileToJpgFile(FNeedFilePath+'bb.bmp',FNeedFilePath+'cc.jpg');
  Addlog(Memo3,'ABBmpFileToJpgFile(JPG文件与BMP文件互转 bb.bmp,cc.jpg):');

  Image_ico.Picture.SaveToFile(FNeedFilePath+'aa.ico');
  ABIcoFileToBmpFile(FNeedFilePath+'aa.ico',FNeedFilePath+'bb.bmp');
  Addlog(Memo3,'ABIcoFileToBmpFile(BMP文件与ico文件互转 aa.ico,bb.bmp):');
  ABBmpFileToIcoFile(FNeedFilePath+'bb.bmp',FNeedFilePath+'cc.ico');
  Addlog(Memo3,'ABBmpFileToIcoFile(BMP文件与ico文件互转 bb.bmp,cc.ico):');

  tempIcon:=Image_ico.Picture.Icon;
  Addlog(Memo3,'ABGetFileIcon(取得与文件相关的图标 FNeedFilePath+aa.mdb):'+ABBoolToStr(ABGetFileIcon(FNeedFilePath+'cc.exe',tempIcon)));
  ABDeleteFile(FNeedFilePath+'cc.exe');
  tempIcon:=Image_ico.Picture.Icon;
  Addlog(Memo3,'ABGetFileIcon(取得与文件相关的图标 FNeedFilePath+Utf-8.txt):'+ABBoolToStr(ABGetFileIcon(FNeedFilePath+'Utf-8.txt',tempIcon)));

  ABChangPictureSize(Image_bmp.Picture.Graphic,Image_jpg.Picture,100,100);
  Image_jpg.Invalidate;
  Addlog(Memo3,'ABChangPictureSize(改变图片文件的尺寸 100,100):');
  ABChangPictureSize(Image_bmp.Picture.Graphic,Image_jpg.Picture,10,10);
  Image_jpg.Invalidate;
  Addlog(Memo3,'ABChangPictureSize(改变图片文件的尺寸 10,10):');

  Addlog(Memo3,'*******************集合、枚举操作*****************');
  tempQueryTypes:=[qtNotEqual                    ,//不等于
                  qtLess                        ,//小于
                  qtLessEqual                   ,//小于或等于
                  qtGreater                     ,//大于
                  qtGreaterEqual                ,//大于或等于
                  qtLike                        ,//包含
                  qtNotLike                     ,//不包含
                  qtLeftLike                    //左包含
                     ];
  Addlog(Memo3,'ABGetSetCount(得到集合变量的元索个数 tempDatasetOperateTypes):'+inttostr(ABGetSetCount(tempQueryTypes,sizeof(TABQueryCompareTypes))));
  Addlog(Memo3,'ABGetSetCount(得到集合类型的元索个数 TABQueryCompareTypes):'+inttostr(ABGetSetCount(TypeInfo(TABQueryCompareTypes))));
  Addlog(Memo3,'ABSetToStr(集合转字集串 TABQueryCompareTypes):'+ABSetToStr(TypeInfo(TABQueryCompareTypes),tempQueryTypes));
  ABStrToSet(TypeInfo(TABQueryCompareTypes),'[qtLess,qtLessEqual]',tempQueryTypes);
  Addlog(Memo3,'ABSetToStr(集合转字集串 TABQueryCompareTypes):'+ABSetToStr(TypeInfo(TABQueryCompareTypes),tempQueryTypes));

  Addlog(Memo3,'ABGetEnumCount(得到枚举类型的元索个数 TABQueryCompareType):'+inttostr(ABGetEnumCount(TypeInfo(TABQueryCompareType))));
  Addlog(Memo3,'ABEnumNameToValue(枚举名转值 qtLess):'+inttostr(ABEnumNameToValue(TypeInfo(TABQueryCompareType),'qtLess')));
  Addlog(Memo3,'ABEnumValueToName(枚举值转名 1):'+ABEnumValueToName(TypeInfo(TABQueryCompareType),1));

  Addlog(Memo3,'*******************网络操作*****************');
  ABGetFtpInfo('ftp://rec:ooo@192.168.76.11/20050418/abcdef.vox',tempUserName,tempPassWord,tempHost,tempPort,tempDir);
  Addlog(Memo3,'ABGetFtpInfo(从FTP地址信息中取得FTP用户名、密码、主机、端口、目录):'+
                  'tempUserName:'+tempUserName+ABEnterWrapStr+
                  'tempPassWord:'+tempPassWord+ABEnterWrapStr+
                  'tempHost:'+tempHost+ABEnterWrapStr+
                  'tempPort:'+tempPort+ABEnterWrapStr+
                  'tempDir:'+tempDir
                  );

  Addlog(Memo3,'ABGetFtppath(取得FTP的路径 ftp://rec:ooo@192.168.76.11/20050418):'+ABGetFtppath('ftp://rec:ooo@192.168.76.11/20050418'));

  Memo5.Lines.SaveToFile(FNeedFilePath+'aa.html');
  Addlog(Memo3,'ABGetHtmlFileTxt(取得Html文件中的文本 FNeedFilePath+aa.html):'+ABGetHtmlFileTxt(FNeedFilePath+'aa.html'));
  Addlog(Memo3,'ABGetBroadIP(取本机的广播地址 ):'+ABGetBroadIP);
  Addlog(Memo3,'ABIsOnInternet(检查是否连上internet ):'+ABBoolToStr(ABIsOnInternet));
  Addlog(Memo3,'ABGetInternetKind(得到连网的类型 ):'+ABEnumValueToName(TypeInfo(TABIntetnetType),ord(ABGetInternetKind)));


  Addlog(Memo3,'*******************字符串操作*****************');
  temptext:=emptystr;
  ABAddstr(temptext,'aa');
  ABAddstr(temptext,'bb');
  Addlog(Memo3,'ABAddstr(将子串aSubStr增加到累计串aSumStr中 aa,bb):'+temptext);
  Addlog(Memo3,'ABAddstr_Ex(功能与ABAddstr类似，函数返回增加后的累计串 cc,aa,bb):'+ABAddstr_Ex(temptext,'cc',';',axdleft));
  Addlog(Memo3,'ABAddstr_Ex(功能与ABAddstr类似，函数返回增加后的累计串 dd,aa,bb):'+ABAddstr_Ex(temptext,'dd',';',axdleft));

  Addlog(Memo3,'ABGetSpaceStrCount(获得源串aStr中以分隔符aSpaceSign分隔的子串的个数 3):'+inttostr(ABGetSpaceStrCount(';aa;',';')));
  Addlog(Memo3,'ABGetSpaceStrCount(获得源串aStr中以分隔符aSpaceSign分隔的子串的个数 2):'+inttostr(ABGetSpaceStrCount(';aa',';')));
  Addlog(Memo3,'ABGetSpaceStrCount(获得源串aStr中以分隔符aSpaceSign分隔的子串的个数 2):'+inttostr(ABGetSpaceStrCount('aa;',';')));
  Addlog(Memo3,'ABGetSpaceStrCount(获得源串aStr中以分隔符aSpaceSign分隔的子串的个数 1):'+inttostr(ABGetSpaceStrCount('aa',';')));
  Addlog(Memo3,'ABGetSpaceStrCount(获得源串aStr中以分隔符aSpaceSign分隔的子串的个数 3):'+inttostr(ABGetSpaceStrCount('aa,bb,cc',',')));

  Addlog(Memo3,'ABGetSpaceStr(获得源串aStr中返回第aIndex-1和aIndex个分隔符aSpaceSign间的子串 ):'+ABGetSpaceStr(';aa;',1,';'));
  Addlog(Memo3,'ABGetSpaceStr(获得源串aStr中返回第aIndex-1和aIndex个分隔符aSpaceSign间的子串 aa):'+ABGetSpaceStr(';aa;',2,';'));
  Addlog(Memo3,'ABGetSpaceStr(获得源串aStr中返回第aIndex-1和aIndex个分隔符aSpaceSign间的子串 ):'+ABGetSpaceStr(';aa;',3,';'));
  Addlog(Memo3,'ABGetSpaceStr(获得源串aStr中返回第aIndex-1和aIndex个分隔符aSpaceSign间的子串 aa):'+ABGetSpaceStr('aa,bb,cc',1,','));
  Addlog(Memo3,'ABGetSpaceStr(获得源串aStr中返回第aIndex-1和aIndex个分隔符aSpaceSign间的子串 bb):'+ABGetSpaceStr('aa,bb,cc',2,','));
  Addlog(Memo3,'ABGetSpaceStr(获得源串aStr中返回第aIndex-1和aIndex个分隔符aSpaceSign间的子串 cc):'+ABGetSpaceStr('aa,bb,cc',3,','));

  Addlog(Memo3,'ABGetSpaceIndex(获得源串aStr中返回子串aSubStr是第几个分隔符aSpaceSign分隔的 1):'+inttostr(ABGetSpaceIndex(';aa;','',';')));
  Addlog(Memo3,'ABGetSpaceIndex(获得源串aStr中返回子串aSubStr是第几个分隔符aSpaceSign分隔的 2):'+inttostr(ABGetSpaceIndex(';aa;','aa',';')));
  Addlog(Memo3,'ABGetSpaceIndex(获得源串aStr中返回子串aSubStr是第几个分隔符aSpaceSign分隔的 1):'+inttostr(ABGetSpaceIndex('aa;bb;cc','aa',';')));
  Addlog(Memo3,'ABGetSpaceIndex(获得源串aStr中返回子串aSubStr是第几个分隔符aSpaceSign分隔的 2):'+inttostr(ABGetSpaceIndex('aa;bb;cc','bb',';')));
  Addlog(Memo3,'ABGetSpaceIndex(获得源串aStr中返回子串aSubStr是第几个分隔符aSpaceSign分隔的 3):'+inttostr(ABGetSpaceIndex('aa;bb;cc','cc',';')));

  Addlog(Memo3,'ABStringReplace(将源串aStr的第aBeginPosition个位置起旧串aOldStr替换成新串aNewStr ab123fgab123fg12345):'+ABStringReplace('abcdefgabcdefg12345','cde','123'));
  Addlog(Memo3,'ABStringReplace(将源串aStr的第aBeginPosition个位置起旧串aOldStr替换成新串aNewStr abcdefgab123fg12345):'+ABStringReplace('abcdefgabcdefg12345','cde','123',5));
  Addlog(Memo3,'ABStringReplace(将源串aStr的第aBeginPosition个位置起长度为aLength的部分替换成新串aNewStr abWWWfgabcdefg12345):'+ABStringReplace('abcdefgabcdefg12345',2,3,'WWW'));
  Addlog(Memo3,'ABStringReplace(将源串aStr的第aBeginPosition个位置起aOldStrs数组中包含的字串替换成新串aNewStr WWWbcdefgWWW):'+ABStringReplace('abcdefgabcdefg12345',['abcdefga','12345'],'WWW'));
  Addlog(Memo3,'ABStringReplace(将源串aStr的第aBeginPosition个位置起aOldStrs数组中包含的字串替换成新串aNewStr abcdefgabcdefgWWW):'+ABStringReplace('abcdefgabcdefg12345',['abcdefga','12345'],'WWW',5));

  Addlog(Memo3,'ABPos(不分大小写的POS 2):'+inttostr(ABPos('bcdefga','abcdefgabcdefg12345')));
  Addlog(Memo3,'ABPos(不分大小写的POS 0):'+inttostr(ABPos('bcdefga','abcdefgabcdefg12345',5)));
  Addlog(Memo3,'ABRightPos(不分大小写返回子串aSubstr在源串aStr中最后一次出现的位置 8):'+inttostr(ABRightPos('abcdefg','abcdefgabcdefg12345')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','abcd')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','*')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 False):'+ABBoolToStr(ABLike('abcd','???')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','????')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 False):'+ABBoolToStr(ABLike('abcd','?????')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','*abcd')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','*bcd')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','abcd*')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','abc*')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','*bc*')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 False):'+ABBoolToStr(ABLike('abcd','?b?')));
  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd','?bc?')));

  Addlog(Memo3,'ABLike(不分大小写且支援?與*的Like字符串 true):'+ABBoolToStr(ABLike('abcd',['cdf','?bc?'])));
  temptext:='aa=test1'+ABEnterWrapStr+'bb=test2';
  Addlog(Memo3,'ABGetItemValue(取得源串aStr中aItemName名称子串后的内容 =test1):'+ABGetItemValue(temptext,'aa','',ABEnterWrapStr));
  Addlog(Memo3,'ABGetItemValue(取得源串aStr中aItemName名称子串后的内容 test1):'+ABGetItemValue(temptext,'aa','=',ABEnterWrapStr));
  temStrings.Text:=temptext;
  Addlog(Memo3,'ABGetItemValue(取得源串aStr中aItemName名称子串后的内容 =test1):'+ABGetItemValue(temStrings,'aa','',ABEnterWrapStr));
  Addlog(Memo3,'ABGetItemValue(取得源串aStr中aItemName名称子串后的内容 test1):'+ABGetItemValue(temStrings,'aa','=',ABEnterWrapStr));

  ABDelItemValue(temptext,'aa=','',ABEnterWrapStr);
  Addlog(Memo3,'ABDelItemValue(删除源串aStr中aItemName名称子串后的内容 aa= bb=test2):'+trim(temptext));
  ABSetItemValue(temptext,'aa=','test001',ABEnterWrapStr);
  Addlog(Memo3,'ABSetItemValue(设置源串aStr中aItemName名称子串后的内容 aa=test001 bb=test2):'+temptext);

  Addlog(Memo3,'ABLeftStr(返回字符串左边的字符 abc):'+ABLeftStr('abcdef',3));
  Addlog(Memo3,'ABRightStr(返回字符串右边的字符 def):'+ABRightStr('abcdef',3));

  Addlog(Memo3,'ABGetGuid(得到GUID):'+ABGetGuid);
  Addlog(Memo3,'ABStrIsGUID(判断一个字符串是否是合法的GUID true):'+ABBoolToStr(ABStrIsGUID(ABGetGuid)));
  Addlog(Memo3,'ABStrIsGUID(判断一个字符串是否是合法的GUID false):'+ABBoolToStr(ABStrIsGUID(ABGetGuid+'WW')));

  temptext:=ABQuotedStr('a'+ABQuotedStr('b')+'c');
  Addlog(Memo3,'ABQuotedStr(转换为引号括括起来的串 '+temptext+'):'+temptext);
  Addlog(Memo3,'ABUnQuotedStr(转换为引号括括起来的串 '+ABUnQuotedStr(temptext)+'):'+ABUnQuotedStr(temptext));
  Addlog(Memo3,'ABUnQuotedFirstlastStr(仅去掉引号括括起来的串中的首尾引号 '+ABUnQuotedFirstlastStr(temptext)+'):'+ABUnQuotedFirstlastStr(temptext));

  Addlog(Memo3,'ABFillEndSign(检测字串aStr是否以aEndSign结束 abcd,):'+ABFillEndSign('abcd'));
  Addlog(Memo3,'ABFillEndSign(检测字串aStr是否以aEndSign结束 abcd;):'+ABFillEndSign('abcd',';'));

  Addlog(Memo3,'ABFillStr(将字串aStr以aAddStr填充到aSetLength长度 000000abcd):'+ABFillStr('abcd',10));
  Addlog(Memo3,'ABFillStr(将字串aStr以aAddStr填充到aSetLength长度 abcd111111):'+ABFillStr('abcd',10,'1',axdright));

  Addlog(Memo3,'ABGetLeftRightStr(取得源串aStr中指定串的左或右部分 aa):'+ABGetLeftRightStr('aa=test1'));
  Addlog(Memo3,'ABGetLeftRightStr(取得源串aStr中指定串的左或右部分 test1):'+ABGetLeftRightStr('aa=test1',axdright));

  Addlog(Memo3,'ABIsEmptyStr(检测是否空串 false):'+ABBoolToStr(ABIsEmptyStr('aa')));
  Addlog(Memo3,'ABIsEmptyStr(检测是否空串 true):'+ABBoolToStr(ABIsEmptyStr(emptystr)));
  Addlog(Memo3,'ABIsEmptyStr(检测是否空串 true):'+ABBoolToStr(ABIsEmptyStr('')));

  Addlog(Memo3,'ABGetSexFlag(取得字符串表示的性别标志 man):'+ABGetSexFlag('男',[]));
  Addlog(Memo3,'ABGetSexFlag(取得字符串表示的性别标志 woman):'+ABGetSexFlag('女',[]));
  Addlog(Memo3,'ABGetSexFlag(取得字符串表示的性别标志 NoKnow):'+ABGetSexFlag('不知道',[]));

  Addlog(Memo3,'ABGetSexFlag(取得字符串表示的性别标志 1):'+ABGetSexFlag('男',['1','0','-1']));
  Addlog(Memo3,'ABGetSexFlag(取得字符串表示的性别标志 0):'+ABGetSexFlag('女',['1','0','-1']));
  Addlog(Memo3,'ABGetSexFlag(取得字符串表示的性别标志 -1):'+ABGetSexFlag('不知道',['1','0','-1']));

  Addlog(Memo3,'ABCompareNumberOrText(比较两个字串,如全为数值则按数值方式比较 ,否则按字符方式比较 1):'+inttostr(ABCompareNumberOrText('12','9')));
  Addlog(Memo3,'ABCompareNumberOrText(比较两个字串,如全为数值则按数值方式比较 ,否则按字符方式比较 -1):'+inttostr(ABCompareNumberOrText('9','12')));
  Addlog(Memo3,'ABCompareNumberOrText(比较两个字串,如全为数值则按数值方式比较 ,否则按字符方式比较 0):'+inttostr(ABCompareNumberOrText('9','9')));
  Addlog(Memo3,'ABCompareNumberOrText(比较两个字串,如全为数值则按数值方式比较 ,否则按字符方式比较 1):'+inttostr(ABCompareNumberOrText('9','12a')));
  Addlog(Memo3,'ABCompareNumberOrText(比较两个字串,如全为数值则按数值方式比较 ,否则按字符方式比较 -1):'+inttostr(ABCompareNumberOrText('12a','9')));
  Addlog(Memo3,'ABCompareNumberOrText(比较两个字串,如全为数值则按数值方式比较 ,否则按字符方式比较 0):'+inttostr(ABCompareNumberOrText('12a','12a')));

  temptext:='Windows, Classes, SysUtils, DB, Menus, Math, Variants,Types,Controls, Forms, ComCtrls,Graphics;';
  Addlog(Memo3,'ABCompartTrimStr(将字串分隔整理，如可将一段文字按分隔符aSpaceSign,按行长度aRowLength进行整理):'+ABCompartTrimStr(temptext,',',50,'  '));
  Addlog(Memo3,'ABSpaceStr(将字串aStr指定每SpLen位以分隔符aSpaceSign进行分隔 1.2345.6789):'+ABSpaceStr('123456789',4,'.'));
  Addlog(Memo3,'ABSpaceStr(将字串aStr指定每SpLen位以分隔符aSpaceSign进行分隔 1,2345,6789):'+ABSpaceStr('123456789',4,','));
  Addlog(Memo3,'ABReverseString(返回字符串的反转 edcba):'+ABReverseString('abcde'));
  Addlog(Memo3,'ABCopyStr(复制字符串aCount次 abcabcabcabcabc):'+ABCopyStr('abc',5,''));
  Addlog(Memo3,'ABCopyStr(复制字符串aCount次 abc,abc,abc,abc,abc):'+ABCopyStr('abc',5,','));
  Addlog(Memo3,'ABGetStrBeginSpaceCount(取字串aStr开头的空格数量 5):'+inttostr(ABGetStrBeginSpaceCount('     abc')));
  Addlog(Memo3,'ABGetSubStrPosition(取得子串aSubStr在源串aStr中第aCount次出现的位置 10):'+inttostr(ABGetSubStrPosition('abc','123abc456abc789abc',2)));
  Addlog(Memo3,'ABDeleteBegin0(删除字段aStr首部的0并返回 abc00000):'+ABDeleteBegin0('000000abc00000'));

  temStrings.Text:=
    'object Panel2: TPanel      '+ABEnterWrapStr+
    '     Left = 0              '+ABEnterWrapStr+
    '     Width = 798           '+ABEnterWrapStr+
    '     Height = 41           '+ABEnterWrapStr+
    '     Align = alBottom      '+ABEnterWrapStr+
    '     TabOrder = 5          '+ABEnterWrapStr+
    '     object Label26: TLabel ';
  Addlog(Memo3,'ABGetIndexByBeginStr(在aStrings中依次查找以数组aFindStrs中字串开头的行，在数组aFindStrs中字串都找到时返回最后一个字串所在的行号 5):'+
    inttostr(ABGetIndexByBeginStr(temStrings,['object Panel2','TabOrder'])));
  Addlog(Memo3,'ABNumberStrToFlag(序号字串转换为标志串输出 011111001110):'+ABNumberStrToFlag('1,2,3,4,5,8,9,10',0,11));
  Addlog(Memo3,'ABNumberStrToFlag(序号字串转换为标志串输出 FTTTTTFFTTTF):'+ABNumberStrToFlag('1,2,3,4,5,8,9,10',0,11,'T','F'));

  temStrings.Clear;
  ABStrsToStrings('aa=1,bb=2,bb=3,cc=4','',temStrings);
  Addlog(Memo3,'ABStrsToStrings(将分隔符分隔的字串各项加入到字串列表中):'+temStrings.Text);
  ABStrsToStrings('aa=1,bb=2,bb=3,cc=4',',',temStrings,false,false);
  Addlog(Memo3,'ABStrsToStrings(将分隔符分隔的字串各项加入到字串列表中):'+temStrings.Text);

  ABNameofStrsToStrings('aa=1,bb=2,bb=3,cc=4',',',temStrings);
  Addlog(Memo3,'ABNameofStrsToStrings(将分隔符分隔的字串各项名称部分加入到字串列表中):'+temStrings.Text);
  ABValueofStrsToStrings('aa=1,bb=2,bb=3,cc=4',',',temStrings);
  Addlog(Memo3,'ABValueofStrsToStrings(将分隔符分隔的字串各项值部分加入到字串列表中):'+temStrings.Text);

  Addlog(Memo3,'ABStringsToStrs(将字串列表各项合并成分隔符分隔的字串并返回):'+ABStringsToStrs(temStrings));
  Addlog(Memo3,'ABNameOfStringsToStrs(将字串列表各项名称部分合并成分隔符分隔的字串并返回):'+ABNameOfStringsToStrs(temStrings));
  Addlog(Memo3,'ABValueOfStringsToStrs(将字串列表各项值部分合并成分隔符分隔的字串并返回):'+ABValueOfStringsToStrs(temStrings));

  ABStringsToStrings(temStrings,temStrings_1);
  Addlog(Memo3,'ABStringsToStrings(将源字串列表的项加到目标字串列表中):'+temStrings_1.Text);

  temStrings.Clear;
  ABStrsToStrings('aa=1,bb=2,bb=3,cc=4',',',temStrings);
  temStrings_1.Clear;
  ABStrsToStrings('bb=2,bb=3',',',temStrings_1);
  ABDeleteInStringsItem (temStrings,temStrings_1);
  Addlog(Memo3,'ABDeleteInStringsItem(在源字串列表中删除既存在源字串列表又存在目标字串列表中的项):'+temStrings.Text);

  Addlog(Memo3,'ABDeleteInStrs(在字串中删除另一字串的项目):'+ABDeleteInStrItem('a,b,c,d','b,c'));

  Addlog(Memo3,'ABInsertInStrs(在以整个字串形式的列表中新增一项):'+ABInsertInStrs (temStrings.Text,0,'ee=5'));
  Addlog(Memo3,'ABEditInStrs(修改以整个字串形式的列表中某一项):'+ABEditInStrs (temStrings.Text,0,'ee=55'));
  Addlog(Memo3,'ABDeleteInStrs(删除以整个字串形式的列表中某一项):'+ABDeleteInStrs (temStrings.Text,0));

  Addlog(Memo3,'*******************DELPHI操作*****************');
  Addlog(Memo3,'ABGetDelphiRegPath(取DELPHI的注册表目录串):'+ABGetDelphiRegPath(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiLibraryRegPath(取DELPHI的注册表包目录串):'+ABGetDelphiLibraryRegPath(ABDelphiVersion));

  Addlog(Memo3,'ABGetDelphiSetupPath(取DELPHI的安装目录):'+ABGetDelphiSetupPath(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiSeachPath(取DELPHI的包查找目录):'+ABGetDelphiSeachPath(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiBplPath(取DELPHI的bpl默认程序目录):'+ABGetDelphiBplPath(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiDcpPath(取DELPHI的dcp默认程序目录):'+ABGetDelphiDcpPath(ABDelphiVersion));

  Addlog(Memo3,'ABGetDelphiAppLongFileName(取DELPHI的主EXE应用程序全名):'+ABGetDelphiAppLongFileName(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiDccLongFileName(取DELPHI的dcc32.exe文件全名):'+ABGetDelphiDccLongFileName(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiCfgLongFileName(取DELPHI的dcc32.cfg文件全名):'+ABGetDelphiCfgLongFileName(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiDofExt(取DELPHI的dof扩展名):'+ABGetDelphiDofExt(ABDelphiVersion));
  Addlog(Memo3,'ABGetDelphiVersion(通过组文件取得DELPHI的版本):'+ABEnumValueToName(TypeInfo(TABDelphiVersion),ord(ABDelphiVersion)));

  ABCopyFile(ABAppPath+'ProjectGroup.bpg',FNeedFilePath+'ProjectGroupaa.bpg');
  temStrings.Clear;
  ABLoadGroupItemToStrings(FNeedFilePath+'ProjectGroupaa.bpg',temStrings);
  Addlog(Memo3,'ABLoadGroupItemToStrings(加载工程组文件中的项目文件到Strings中):'+temStrings.Text);
  temStrings.Clear;
  ABLoadGroupItemToStrings(FNeedFilePath+'ProjectGroupaa.groupproj',temStrings);
  Addlog(Memo3,'ABLoadGroupItemToStrings(加载工程组文件中的项目文件到Strings中):'+temStrings.Text);

  ABSaveToD7Group(temStrings,FNeedFilePath+'aaaa.bpg');
  Addlog(Memo3,'ABSaveToD7Group(另存Strings中项目文件为BPG组文件):'+ABReadTxt(FNeedFilePath+'aaaa.bpg'));

  ABCopyFile(ABAppPath+'ABPubG.dof',FNeedFilePath+'tempaa.dof');
  setlength(temparrayStr,4);
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstIncludeVerInfo,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildIncVersion,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstVersion,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstDcpPath,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBplPath,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstExePath,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildWithPackage,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildWithPackageList,temparrayStr);

  temparrayStr[0]:='1';
  temparrayStr[1]:='2';
  temparrayStr[2]:='3';
  temparrayStr[3]:='4';
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstIncludeVerInfo,temparrayStr);
  Addlog(Memo3,'ABSetDelphidof(设置dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildIncVersion,temparrayStr);
  Addlog(Memo3,'ABSetDelphidof(设置dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstVersion,temparrayStr);
  Addlog(Memo3,'ABSetDelphidof(设置dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstDcpPath,temparrayStr);
  Addlog(Memo3,'ABSetDelphidof(设置dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstBplPath,temparrayStr);
  Addlog(Memo3,'ABSetDelphidof(设置dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstExePath,temparrayStr);
  Addlog(Memo3,'ABSetDelphidof(设置dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildWithPackage,temparrayStr);
  Addlog(Memo3,'ABSetDelphidof(设置dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABSetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildWithPackageList,temparrayStr);

  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstIncludeVerInfo,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildIncVersion,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstVersion,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstDcpPath,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBplPath,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstExePath,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildWithPackage,temparrayStr);
  Addlog(Memo3,'ABGetDelphidof(取得dof中的信息):'+ABStringArrayToStr(temparrayStr));
  ABGetDelphidof(FNeedFilePath+'tempaa.dof',bstBuildWithPackageList,temparrayStr);

  ABCreateDelphiCfgFile(ABDelphiVersion,
                        FNeedFilePath+'tempaa.cfg',
                        'aDcpOutPath',
                        'aBPLOutPath',
                        'aExeOutPath',

                        temStrings,
                        false
                        );
  Addlog(Memo3,'ABCreateDelphiCfgFile(创建DelphiCFG文件):'+ABReadTxt(FNeedFilePath+'tempaa.cfg'));
  ABCreateDelphiCfgFile(ABDelphiVersion,
                        FNeedFilePath+'dcc32.cfg',
                        'aDcpOutPath',
                        'aBPLOutPath',
                        'aExeOutPath',

                        temStrings,
                        false
                        );
  Addlog(Memo3,'ABCreateDelphiCfgFile(创建DelphiCFG文件):'+ABReadTxt(FNeedFilePath+'dcc32.cfg'));

  temStrings.Clear;
  temStrings_1.Clear;
  temStrings_1.Add(FNeedFilePath+ABAppName+'.dpr');
  temStrings_1.Add(FNeedFilePath+'ABFrameworkTestP.dpr');
  Addlog(Memo3,'ABBuildProjects(批量编译DELPHI项目):'+
    ABBoolToStr(
                ABBuildProjects(ABDelphiVersion,
                                ABGetDelphiDccLongFileName(ABDelphiVersion),
                                temStrings_1,
                                temStrings,
                                temStrings,

                                '',
                                ABGetDelphiDcpPath(ABDelphiVersion),
                                ABGetDelphiBplPath(ABDelphiVersion),
                                ABGetDelphiBplPath(ABDelphiVersion),
                                temStrings,
                                true)));
  Addlog(Memo3,'ABBuildProject(单个编译DELPHI项目(生成新的cfg文件编译)):'+temStrings.text);

  temStrings.Clear;
  Addlog(Memo3,'ABBuildProject(单个编译DELPHI项目(生成新的cfg文件编译)):'+
    ABBoolToStr(
                ABBuildProject(ABDelphiVersion,
                                ABGetDelphiDccLongFileName(ABDelphiVersion),
                                FNeedFilePath+ABAppName+'.dpr',
                                tempText,

                                '',
                                ABGetDelphiDcpPath(ABDelphiVersion),
                                ABGetDelphiBplPath(ABDelphiVersion),
                                ABGetDelphiBplPath(ABDelphiVersion),
                                temStrings,
                                true)));
  Addlog(Memo3,'ABBuildProject(单个编译DELPHI项目(生成新的cfg文件编译)):'+tempText);
  Addlog(Memo3,'ABBuildProject(单个编译DELPHI项目(生成新的cfg文件编译)):'+
    ABBoolToStr(
                ABBuildProject(ABGetDelphiDccLongFileName(ABDelphiVersion),
                                FNeedFilePath+ABAppName+'.dpr',
                                tempText)));
  Addlog(Memo3,'ABBuildProject(单个编译DELPHI项目(生成新的cfg文件编译)):'+tempText);

  Addlog(Memo3,'*******************对象属性操作*****************');
  Addlog(Memo3,'ABPropIsType(对象的属性是否是指定类型 sender Caption,tkString):'+abbooltostr(ABPropIsType(sender,'Caption',tkString)));
  Addlog(Memo3,'ABGetPropValue(取对象的一般属性值 sender Caption):'+vartostr(ABGetPropValue(sender,'Caption')));
  Addlog(Memo3,'ABGetPropValue(取对象的一般属性值 sender Font.name):'+vartostr(ABGetPropValue(sender,'Font.name')));
  Addlog(Memo3,'ABGetObjectPropValue(取对象的对象属性值 Memo4 Lines):'+Tstrings(ABGetObjectPropValue(Memo4,'Lines')).text);

  ABSetPropValue(sender,'Caption','aaaa');
  Addlog(Memo3,'ABSetPropValue(设置对象的一般属性值 sender Caption):');
  Addlog(Memo3,'ABGetPropValue(取对象的一般属性值 sender Caption):'+vartostr(ABGetPropValue(sender,'Caption')));

  temStrings.Text:='aaaa';
  ABSetObjectPropValue(Memo4,'Lines',temStrings);
  Addlog(Memo3,'ABSetObjectPropValue(设置对象的对象属性值 Memo4 Lines):');
  Addlog(Memo3,'ABGetObjectPropValue(取对象的对象属性值 Memo4 Lines):'+Tstrings(ABGetObjectPropValue(Memo4,'Lines')).text);

  tempMethod:=ABGetMethodProp(Button53, 'OnClick');
  Addlog(Memo3,'ABGetMethodProp(取对象的方法  (Button53,OnClick)):'+inttostr(integer(tempMethod.Code)));
  tempMethod:=ABGetMethodProp(sender, 'OnClick');
  Addlog(Memo3,'ABGetMethodProp(取对象的方法  (Sender,OnClick)):'+inttostr(integer(tempMethod.Code)));

  tempMethod.Code := @TABTestForm.Button53Click;
  ABSetMethodProp(sender, 'OnClick',tempMethod);
  Addlog(Memo3,'ABSetMethodProp(设置对象的方法  (Sender,OnClick,Button53Click)):');

  ABExecNoParamPublishProp(sender, 'aaaa');
  Addlog(Memo3,'ABExecNoParamPublishProp(执行没有参数的公布过程  (Sender,aaaa)):');

  tempMethod:=ABGetMethodProp(sender, 'OnClick');
  Addlog(Memo3,'ABGetMethodProp(取对象的方法  (Sender,OnClick)):'+inttostr(integer(tempMethod.Code)));

  Addlog(Memo3,'ABIsPublishProp(判断是否Publish属性 (Sender,caption)):'+ABBoolToStr(ABIsPublishProp(Sender,'caption')));
  Addlog(Memo3,'ABIsPublishProp(判断是否Publish属性 (Sender,Checked)):'+ABBoolToStr(ABIsPublishProp(Sender,'Checked')));
  temStrings.Clear;
  ABGetClassProperties(sender,temStrings);
  Addlog(Memo3,'ABGetDataSetProperty(将对象的属性全放在AStrings中  (sender,temStrings)):'+temStrings.Text);

  Addlog(Memo3,'ABGetStringsProperty(取对象的tStrings属性值  (Memo4,Lines)):'+ABGetStringsProperty(Memo4,'Lines').text);
  Addlog(Memo3,'ABGetDataSetProperty(取对象的数据集属性值  (FDataSource,true)):'+ABGetDataSetProperty(FDataSource,true).Name);
  Addlog(Memo3,'ABGetDataSetProperty(取对象的数据集属性值  (FDataSource,False)):'+ABGetDataSetProperty(FDataSource,False).Name);

  Addlog(Memo3,'ABIsMoreLineProperty(检测是否是多行属性名 Items.Text):'+ABBoolToStr(ABIsMoreLineProperty('Items.Text')));
  Addlog(Memo3,'ABIsMoreLineProperty(检测是否是多行属性名 caption):'+ABBoolToStr(ABIsMoreLineProperty('caption')));
  Addlog(Memo3,'ABComponentToDFMProperty(组件的属性序列化到DFM形式的文本 Sender):'+ABComponentToDFMProperty(TComponent(Sender)));
  Addlog(Memo3,'ABDFMPropertyToComponent(DFM形式的组件属性反序列化到组件 (Sender)):'+ABDFMPropertyToComponent(ABComponentToDFMProperty(TComponent(Sender)),TComponent(Sender)).Name);

  Addlog(Memo3,'ABDFMControlDelete(删除DFM文件中控件):'+ABBoolToStr(ABDFMControlDelete(temStrings,'  ',' aa')));
  Addlog(Memo3,'ABDFMControlCopy(拷贝DFM文件中控件):'+ABDFMControlCopy(temStrings,'  ',' aa'));

  Addlog(Memo3,'*******************组件、控件,窗体等对象操作*****************');
  Addlog(Memo3,'ABGetOwnerLongName(取得对象所有Owner及组件名称 Sender):'+ABGetOwnerLongName(TComponent(Sender)));
  Addlog(Memo3,'ABObjectInheritFrom(检测对象是否从TButtonControl继承 Sender):'+abbooltostr(ABObjectInheritFrom(Sender,'TButtonControl')));
  Addlog(Memo3,'ABObjectInheritFrom(检测对象是否从TTabSheet继承 Sender):'+abbooltostr(ABObjectInheritFrom(Sender,'TTabSheet')));
  Addlog(Memo3,'ABGetParent(得到控件的父对象,到类型名称为TPageControl时停止 Sender):'+TControl(ABGetParent(TControl(Sender),'TPageControl')).Name);
  Addlog(Memo3,'ABGetOwner(得到控件所有者对象,到类型名称为Tform时停止 Sender):'+TComponent(ABGetOwner(TComponent(Sender),'Tform')).Name);
  Addlog(Memo3,'ABGetParentForm(得到控件所在的窗体 Sender):'+ABGetParentForm(TControl(Sender)).Name);

  if Assigned(ABGetMouseForm()) then
    Addlog(Memo3,'ABGetMouseForm(获得鼠标针指下的窗体):'+ABGetMouseForm.Name);
  if Assigned(ABGetMouseControl()) then
    Addlog(Memo3,'ABGetMouseControl(获得鼠标针指下的控件):'+ABGetMouseControl.Name);

  GetCursorPos(temppPoint);
  if Assigned(ABGetCoordinateForm(temppPoint.X,temppPoint.Y)) then
    Addlog(Memo3,'ABGetCoordinateForm(获得指定坐标下的窗体 temppPoint.X,temppPoint.Y):'+ABGetCoordinateForm(temppPoint.X,temppPoint.Y).Name);
  if Assigned(ABGetCoordinateForm(1,1)) then
    Addlog(Memo3,'ABGetCoordinateForm(获得指定坐标下的窗体 1,1):'+ABGetCoordinateForm(1,1).Name);

  SetCursorPos(1,1);
  GetCursorPos(temppPoint);
  Addlog(Memo3,'鼠标在XY:'+inttostr(temppPoint.X)+','+inttostr(temppPoint.Y));
  ABMoveMouseToControl(TControl(Sender));
  GetCursorPos(temppPoint);
  Addlog(Memo3,'ABMoveMouseToControl(移动鼠标到控件中心):'+inttostr(temppPoint.X)+','+inttostr(temppPoint.Y));
  Addlog(Memo3,'ABWinControlShowError(根据aIsError决定是否要显示aErrorStr字串的提示框并设置焦点在aSender上 Sender,false,''提示信息''):'+ABBoolToStr(ABWinControlShowError(TWinControl(Sender),false,'提示信息')));
  Addlog(Memo3,'ABWinControlShowError(根据aIsError决定是否要显示aErrorStr字串的提示框并设置焦点在aSender上 Sender,True,''提示信息''):'+ABBoolToStr(ABWinControlShowError(TWinControl(Sender),True,'提示信息')));

  ABSetControlEnabled(Panel8,false);
  Addlog(Memo3,'ABSetControlEnabled(设置控件的Enabled Panel8,false):');
  abshow('设置组件的Enabled Panel8,false');

  ABSetControlEnabled(Panel8,True);
  Addlog(Memo3,'ABSetControlEnabled(设置控件的Enabled Panel8,True):');
  abshow('设置组件的Enabled Panel8,True');

  ABSetControlEnabled([Panel8,TControl(Memo3)],false);
  Addlog(Memo3,'ABSetControlEnabled(设置控件的Enabled [Panel8,TControl(Memo3)],false):');
  abshow('设置组件的Enabled [Panel8,TControl(Memo3)],false');

  ABSetControlEnabled([Panel8,TControl(Memo3)],True);
  Addlog(Memo3,'ABSetControlEnabled(设置控件的Enabled [Panel8,TControl(Memo3)],True):');
  abshow('设置组件的Enabled [Panel8,TControl(Memo3)],True');

  ABSetControlVisible(Panel8,false);
  Addlog(Memo3,'ABSetControlVisible(设置控件的Visible Panel8,false):');
  abshow('设置组件的Visible Panel8,false');

  ABSetControlVisible(Panel8,True);
  Addlog(Memo3,'ABSetControlVisible(设置控件的Visible Panel8,True):');
  abshow('设置组件的Visible Panel8,True');

  ABSetControlVisible([Panel8,TControl(Memo3)],false);
  Addlog(Memo3,'ABSetControlVisible(设置控件的Visible [Panel8,TControl(Memo3)],false):');
  abshow('设置组件的Visible [Panel8,TControl(Memo3)],false');

  ABSetControlVisible([Panel8,TControl(Memo3)],True);
  Addlog(Memo3,'ABSetControlVisible(设置控件的Visible [Panel8,TControl(Memo3)],True):');
  abshow('设置组件的Visible [Panel8,TControl(Memo3)],True');

  Addlog(Memo3,'ABGetMemoRow(得到备注控件的行号 Memo3):'+inttostr(ABGetMemoRow(Memo3)));
  temppPoint:=ABGetMemoCursor(Memo3);
  Addlog(Memo3,'ABGetMemoCursor(得到备注控件当前光标的行和列信息 Memo3):'+inttostr(temppPoint.X)+','+inttostr(temppPoint.Y));

  Addlog(Memo3,'ABGetFirstClientControlByClassName(得到父控件下类型名为指定的类型或从指定的类型继承的第一个子控件 Panel8,Tbutton):'+
    ABGetFirstClientControlByClassName(Panel10,'Tbutton').name);
  Addlog(Memo3,'ABGetClassFileName(返回类型所在库文件全名 Tbutton):'+ABGetClassFileName(Tbutton));
  Addlog(Memo3,'ABGetClassFileName(返回类型所在库文件全名 TABPubUser):'+ABGetClassFileName(TABPubUser));

  Addlog(Memo3,'ABGetClassUnitName(返回类型所在单元名 Tbutton):'+ABGetClassUnitName(Tbutton));
  Addlog(Memo3,'ABGetClassUnitName(返回类型所在单元名 TABPubUser):'+ABGetClassUnitName(TABPubUser));

  ABGetParentClassLists(TABPubUser,temStrings,false,true,false);
  Addlog(Memo3,'ABGetParentClassLists(返回类型的遂层父亲类型及其所在单元或库文件名列表 TABThirdQuery,false,true,false):'+trim(temStrings.Text));
  ABGetParentClassLists(TABPubUser,temStrings);
  Addlog(Memo3,'ABGetParentClassLists(返回类型的遂层父亲类型及其所在单元或库文件名列表 TABThirdQuery):'+trim(temStrings.Text));

  ProgressBar2.Max:=100;
  ABSetProgressBarBegin(ProgressBar2,100);
  for I := 1 to 100 do
  begin
    ABSetProgressBarNext(ProgressBar2,1,10);
  end;
  ABSetProgressBarEnd(ProgressBar2);
  Addlog(Memo3,'ABSetProgressBarBegin(设置进度条开始):');
  Addlog(Memo3,'ABSetProgressBarNext(设置进度条下一个步进):');
  Addlog(Memo3,'ABSetProgressBarEnd(设置进度条结束):');

  ABListBoxItemMoveToListBox(ListBox1,1,'',ListBox2);
  ABListBoxItemMoveToListBox(CheckListBox1,1,'',CheckListBox2);
  Addlog(Memo3,'ABListBoxItemMoveToListBox(从源ListBox的aIndex项移动到目标ListBox ListBox1):'+trim(ListBox1.Items.Text));
  Addlog(Memo3,'ABListBoxItemMoveToListBox(从源ListBox的aIndex项移动到目标ListBox ListBox2):'+trim(ListBox2.Items.Text));
  Addlog(Memo3,'ABListBoxItemMoveToListBox(从源ListBox的aIndex项移动到目标ListBox CheckListBox1):'+trim(CheckListBox1.Items.Text));
  Addlog(Memo3,'ABListBoxItemMoveToListBox(从源ListBox的aIndex项移动到目标ListBox CheckListBox2):'+trim(CheckListBox2.Items.Text));

  ABListBoxUp(ListBox1,2,1);
  Addlog(Memo3,'ABListBoxUp(ListBox索引项上移 2,1):'+trim(ListBox1.Items.Text));
  ABListBoxUp(ListBox1,2,2);
  Addlog(Memo3,'ABListBoxUp(ListBox索引项上移 2,2):'+trim(ListBox1.Items.Text));
  ABListBoxUp(CheckListBox1,2,1);
  Addlog(Memo3,'ABListBoxUp(ListBox索引项上移 2,1):'+trim(CheckListBox1.Items.Text));
  ABListBoxUp(CheckListBox1,2,2);
  Addlog(Memo3,'ABListBoxUp(ListBox索引项上移 2,2):'+trim(CheckListBox1.Items.Text));

  ABListBoxDown(ListBox1,1,1);
  Addlog(Memo3,'ABListBoxDown(ListBox索引项下移 1,1):'+trim(ListBox1.Items.Text));
  ABListBoxDown(ListBox1,1,2);
  Addlog(Memo3,'ABListBoxDown(ListBox索引项下移 1,2):'+trim(ListBox1.Items.Text));
  ABListBoxDown(CheckListBox1,1,1);
  Addlog(Memo3,'ABListBoxDown(ListBox索引项下移 1,1):'+trim(CheckListBox1.Items.Text));
  ABListBoxDown(CheckListBox1,1,2);
  Addlog(Memo3,'ABListBoxDown(ListBox索引项下移 1,2):'+trim(CheckListBox1.Items.Text));

  ABListBoxSelectsCopyStrings(ListBox1,
                              temStrings);
  Addlog(Memo3,'ABListBoxSelectsCopyStrings(将ListBox中所有选择项复制到字串列表中):'+trim(temStrings.Text));
  ABListBoxSelectsCopyStrings(CheckListBox1,
                              temStrings);
  Addlog(Memo3,'ABListBoxSelectsCopyStrings(将ListBox中所有选择项复制到字串列表中):'+trim(temStrings.Text));

  ABListBoxCopyStrings(ListBox1,
                       1,2,
                       temStrings);
  Addlog(Memo3,'ABListBoxCopyStrings(将ListBox中某一位置到最后的所有项复制到字串列表中):'+trim(temStrings.Text));
  ABListBoxCopyStrings(CheckListBox1,
                       1,2,
                       temStrings);
  Addlog(Memo3,'ABListBoxCopyStrings(将ListBox中某一位置到最后的所有项复制到字串列表中):'+trim(temStrings.Text));

  ABSetListBoxWidth(ListBox1);
  Addlog(Memo3,'ABSetListBoxWidth(设置ListBox的宽度):');
  ABSetListBoxWidth(CheckListBox1);
  Addlog(Memo3,'ABSetListBoxWidth(设置ListBox的宽度):');

  ABSetListBoxSelect(ListBox1,0);
  Addlog(Memo3,'ABSetListBoxSelect(设置TCheckListBox的选择类型 0):');
  ABSetListBoxSelect(ListBox1,1);
  Addlog(Memo3,'ABSetListBoxSelect(设置TCheckListBox的选择类型 1):');
  ABSetListBoxSelect(ListBox1,2);
  Addlog(Memo3,'ABSetListBoxSelect(设置TCheckListBox的选择类型 2):');

  ABSetListBoxSelect(CheckListBox1,0);
  Addlog(Memo3,'ABSetListBoxSelect(设置TCheckListBox的选择类型 0):');
  ABSetListBoxSelect(CheckListBox1,1);
  Addlog(Memo3,'ABSetListBoxSelect(设置TCheckListBox的选择类型 1):');
  ABSetListBoxSelect(CheckListBox1,2);
  Addlog(Memo3,'ABSetListBoxSelect(设置TCheckListBox的选择类型 2):');

  ABSaveListBoxSelect(ListBox1);
  Addlog(Memo3,'ABSaveListBoxSelect(保存ListBox的选择状态到本地配制文件中):');
  ABSaveListBoxSelect(CheckListBox1);
  Addlog(Memo3,'ABSaveListBoxSelect(保存ListBox的选择状态到本地配制文件中):');
  ABLoadListBoxSelect(ListBox1);
  Addlog(Memo3,'ABLoadListBoxSelect(从本地配制文件中读取ListBox的选择状态):');
  ABLoadListBoxSelect(CheckListBox1);
  Addlog(Memo3,'ABLoadListBoxSelect(从本地配制文件中读取ListBox的选择状态):');

  ABGetMenuItems(PopupMenu1.Items,temStrings);
  Addlog(Memo3,'ABGetMenuItems(取得菜单的所有菜单项到列表TList中):'+inttostr(temStrings.Count));
  ABGetMenuItem(temStrings,'aa');
  Addlog(Memo3,'ABGetMenuItem(根据名称从列表TList中取得菜单项):');

  ABClareMenuItems(PopupMenu1);
  ABGetMenuItems(PopupMenu1.Items,temStrings);
  Addlog(Memo3,'ABClareMenuItems(清除菜单的所有菜单项):'+inttostr(temStrings.Count));

  ABSelectMenuItemOfGroup(aa1);
  Addlog(Memo3,'ABSelectMenuItemOfGroup(对组中的菜单项在点击时做必选处理):');

  Addlog(Memo3,'ABAutoTabOrder(根据控件的位置对你控件中的子控件进行Order重设置):'+inttostr(CheckListBox1.TabOrder)+' '+
                                                                            inttostr(CheckListBox2.TabOrder)+' '+
                                                                            inttostr(CheckListBox3.TabOrder) );
  ABAutoTabOrder(GroupBox6);
  Addlog(Memo3,'ABAutoTabOrder(根据控件的位置对你控件中的子控件进行Order重设置):'+inttostr(CheckListBox1.TabOrder)+' '+
                                                                            inttostr(CheckListBox2.TabOrder)+' '+
                                                                            inttostr(CheckListBox3.TabOrder) );


  Addlog(Memo3,'*******************操作系统操作*****************');
  ABCreateShortCut(CSIDL_DESKTOP,ABAppFullName,'中国家');
  Addlog(Memo3,'ABCreateShortCut(在指定位置创建指定文件的快捷方式 CSIDL_DESKTOP,ABAppFullName,中国家):');
  ABCreateShortCut(CSIDL_DESKTOP,ABAppFullName,'中国家','中国家文件');
  Addlog(Memo3,'ABCreateShortCut(在指定位置创建指定文件的快捷方式 CSIDL_DESKTOP,ABAppFullName,中国家,中国家文件):');

  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_COMMON_programs ):'+ABGetShortCutTypePath(CSIDL_COMMON_programs ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_DESKTOP         ):'+ABGetShortCutTypePath(CSIDL_DESKTOP         ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_DESKTOPDIRECTORY):'+ABGetShortCutTypePath(CSIDL_DESKTOPDIRECTORY));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_FONTS           ):'+ABGetShortCutTypePath(CSIDL_FONTS           ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_NETHOOD         ):'+ABGetShortCutTypePath(CSIDL_NETHOOD         ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_PERSONAL        ):'+ABGetShortCutTypePath(CSIDL_PERSONAL        ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_PROGRAMS        ):'+ABGetShortCutTypePath(CSIDL_PROGRAMS        ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_RECENT          ):'+ABGetShortCutTypePath(CSIDL_RECENT          ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_SENDTO          ):'+ABGetShortCutTypePath(CSIDL_SENDTO          ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_STARTMENU       ):'+ABGetShortCutTypePath(CSIDL_STARTMENU       ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_STARTUP         ):'+ABGetShortCutTypePath(CSIDL_STARTUP         ));
  Addlog(Memo3,'ABGetShortCutTypePath(快捷方式位置类型对应的路径 CSIDL_FAVORITES       ):'+ABGetShortCutTypePath(CSIDL_FAVORITES       ));

  Addlog(Memo3,'ABGetShortCutLLinkFileName(得到快捷方式的全文件名 ABGetShortCutTypePath(CSIDL_DESKTOP)+中国家.lnk):'+ABGetShortCutLLinkFileName(ABGetShortCutTypePath(CSIDL_DESKTOP)+'中国家.lnk'));
  Addlog(Memo3,'ABGetShortCutLLinkFileName(得到快捷方式的全文件名 ABGetShortCutTypePath(CSIDL_DESKTOP)+中国家文件\中国家.lnk):'+ABGetShortCutLLinkFileName(ABGetShortCutTypePath(CSIDL_DESKTOP)+'中国家文件\中国家.lnk'));

  ABDeleteFile(ABGetShortCutTypePath(CSIDL_DESKTOP)+'中国家.lnk');
  ABDeltree(ABGetShortCutTypePath(CSIDL_DESKTOP)+'中国家文件');
  Addlog(Memo3,'ABDeleteFile(删除快捷方式: ABGetShortCutTypePath(CSIDL_DESKTOP)+中国家.lnk)');
  Addlog(Memo3,'ABDeltree(删除目录:ABGetShortCutTypePath(CSIDL_DESKTOP)+中国家文件)');

  Addlog(Memo3,'ABGetWindowsPath(取Windows系统目录):'+ABGetWindowsPath);
  Addlog(Memo3,'ABGetWindowsTempPath(取Windows临时文件目录):'+ABGetWindowsTempPath);
  Addlog(Memo3,'ABGetWindowsTempFileName(返回Windows临时文件目录中指定文件扩展名的临时文件名):'+ABGetWindowsTempFileName('cos'));
  Addlog(Memo3,'ABGetWindowsTempFileName(返回指定文件目录中指定文件扩展名的临时文件名):'+ABGetWindowsTempFileName(FNeedFilePath,'wat'));

  Addlog(Memo3,'ABGetLastWsaErrorStr(返回最近一次WSA异步调用的错误信息字符串):'+ABGetLastWsaErrorStr);
  Addlog(Memo3,'ABGetLastErrorString(返回最近一次API调用的错误信息字符串):'+ABGetLastErrorString);

  Addlog(Memo3,'ABGetEnvironmentList(得到系统环境变量列表):'+ABGetEnvironmentList.Text);
  Addlog(Memo3,'ABReplaceEnvironment(替换字串中的系统变量为实际系统变量的值):'+ABReplaceEnvironment('$(ALLUSERSPROFILE)$(APPDATA)'));
  Addlog(Memo3,'ABReplaceEnvironment(替换字串中的系统变量为实际系统变量的值):'+ABReplaceEnvironment('ALLUSERSPROFILE;APPDATA','',''));

  Addlog(Memo3,'ABIsWin64(当前环境是否是64位Windows系统):'+ABBoolToStr(ABIsWin64));
  Addlog(Memo3,'ABIsVMwarePresent(当前环境是否是虚拟机):'+ABBoolToStr(ABIsVMwarePresent));

  Addlog(Memo3,'ABGetWindowsUserName(获取Windows当前登录名的用户名):'+ABGetWindowsUserName);
  ABGetRegistryOrgAndUserName(tempText,tempUserName);
  Addlog(Memo3,'ABGetRegistryOrgAndUserName(获取Windows注册的单位及用户名):'+tempText+','+tempUserName);

  Addlog(Memo3,'ABGetSysVersion(获取操作系统版本号):'+ABGetSysVersion);
  Addlog(Memo3,'ABGetSysLanguage(获取操作系统的语言类型):'+ABEnumValueToName(typeinfo(TABSysLanguageType),ord(ABGetSysLanguage)));
  Addlog(Memo3,'ABGetSysLanguageStr(获取操作系统的语言类型字串):'+ABGetSysLanguageStr(ABGetSysLanguage));

  Addlog(Memo3,'ABGetDiskVolumeSerialNumber(获取盘符的序列号 A):'+ABGetDiskVolumeSerialNumber('A'));
  Addlog(Memo3,'ABGetDiskVolumeSerialNumber(获取盘符的序列号 B):'+ABGetDiskVolumeSerialNumber('B'));
  Addlog(Memo3,'ABGetDiskVolumeSerialNumber(获取盘符的序列号 C):'+ABGetDiskVolumeSerialNumber('C'));
  Addlog(Memo3,'ABGetDiskVolumeSerialNumber(获取盘符的序列号 C):'+ABGetDiskVolumeSerialNumber('C'));
  Addlog(Memo3,'ABGetDiskVolumeSerialNumber(获取盘符的序列号 D):'+ABGetDiskVolumeSerialNumber('D'));
  Addlog(Memo3,'ABGetDiskVolumeSerialNumber(获取盘符的序列号 E):'+ABGetDiskVolumeSerialNumber('E'));

  Addlog(Memo3,'ABDiskVolumeIsOK(检查盘符准备是否就绪 A):'+ABBoolToStr(ABDiskVolumeIsOK('A')));
  Addlog(Memo3,'ABDiskVolumeIsOK(检查盘符准备是否就绪 B):'+ABBoolToStr(ABDiskVolumeIsOK('B')));
  Addlog(Memo3,'ABDiskVolumeIsOK(检查盘符准备是否就绪 C):'+ABBoolToStr(ABDiskVolumeIsOK('C')));
  Addlog(Memo3,'ABDiskVolumeIsOK(检查盘符准备是否就绪 D):'+ABBoolToStr(ABDiskVolumeIsOK('D')));
  Addlog(Memo3,'ABDiskVolumeIsOK(检查盘符准备是否就绪 E):'+ABBoolToStr(ABDiskVolumeIsOK('E')));

  Addlog(Memo3,'ABCPUSpeed(获取当前机器CPU的速率):'+FloatToStr(ABCPUSpeed));
  Addlog(Memo3,'ABCPUSpeed(获取当前机器CPU的速率):'+FloatToStr(ABCPUSpeed));
  Addlog(Memo3,'ABCPUSpeed(获取当前机器CPU的速率):'+FloatToStr(ABCPUSpeed));

  Addlog(Memo3,'ABGetCpuID(获取CPU的ID 1):'+ABGetCpuID(1));
  Addlog(Memo3,'ABGetCpuID(获取CPU的ID 2):'+ABGetCpuID(2));

  Addlog(Memo3,'ABGetTotalMemory(获取计算机的物理内存(KB)):'+FloatToStr(ABGetTotalMemory));
  Addlog(Memo3,'ABGetCanUseMemory(获取计算机可用的物理内存(KB)):'+FloatToStr(ABGetCanUseMemory));

  ABReplaceControlCaption(tempFindFindHWnd,'中国人民共和国','中华人民共和国',true);
  Addlog(Memo3,'ABReplaceControlCaption(查找指定标题且指类名的窗体并替换标题):');
  ABReplaceControlCaption(tempFindFindHWnd,'中国人民','中华',true,'',true) ;
  Addlog(Memo3,'ABReplaceControlCaption(查找指定标题且指类名的窗体并替换标题):');
  ABReplaceControlCaption(tempFindFindHWnd,'中国人民','中华',true,'',true,true);
  Addlog(Memo3,'ABReplaceControlCaption(查找指定标题且指类名的窗体并替换标题):');
  ABReplaceControlCaption(tempFindFindHWnd,'中国人民','中华',true,'',true,true,true);
  Addlog(Memo3,'ABReplaceControlCaption(查找指定标题且指类名的窗体并替换标题):');

  Addlog(Memo3,'ABGetControlClassName(返回指定句柄窗口的类名 sender.Handle):'+ABGetControlClassName(TButton(sender).Handle));
  Addlog(Memo3,'ABGetControlCaption(返回指定句柄窗口的标题 sender.Handle):'+ABGetControlCaption(TButton(sender).Handle));

  ABSetControlOnTop(self.Handle,true);
  Addlog(Memo3,'ABSetControlOnTop(设置指定句柄窗口最上方显示 sender.Handle):');
  ABSetControlOnTop(self.Handle,False);
  Addlog(Memo3,'ABSetControlOnTop(设置指定句柄窗口最上方显示 sender.Handle):');

  ABSetControlTaskBarVisible(self.Handle,true);
  Addlog(Memo3,'ABSetControlTaskBarVisible(设置指定句柄程序是否出现在任务栏 sender.Handle):');
  ABSetControlTaskBarVisible(self.Handle,False);
  Addlog(Memo3,'ABSetControlTaskBarVisible(设置指定句柄程序是否出现在任务栏 sender.Handle):');

  ABAnimateWindow(self.Handle);
  Addlog(Memo3,'ABAnimateWindow(设置指定句柄窗体动画效果 sender.Handle):');

  ABSetTaskBarVisible(False);
  Addlog(Memo3,'ABAnimateWindow(设置任务栏是否可见 False):');
  ABSetTaskBarVisible(True);
  Addlog(Memo3,'ABAnimateWindow(设置任务栏是否可见 True):');

  ABSetDesktopVisible(False);
  Addlog(Memo3,'ABSetDesktopVisible(设置桌面是否可见 False):');
  ABSetDesktopVisible(True);
  Addlog(Memo3,'ABSetDesktopVisible(设置桌面是否可见 True):');

  ABOpenIME('极品五笔输入法 2013');
  Addlog(Memo3,'ABSetDesktopVisible(打开输入法 True):');
  ABCloseIME;
  Addlog(Memo3,'ABSetDesktopVisible(关闭输入法):');

  case ABShow('请选择?',[],['取消','重启','关闭','注销'],1) of
    2:
    begin
      ABRebootComputer;
      exit;
    end;
    3:
    begin
      ABShutDownComputer;
      exit;
    end;
    4:
    begin
      ABLOGOFF;
      exit;
    end;
    else
    begin
    end;
  end;

  Func1:=ABHook_Func(GetMethodProp(Button52, 'OnClick').Code,GetMethodProp(Button33, 'OnClick').Code);
  Addlog(Memo3,'ABHook_Func(重置函数的地址):');
  ABUnHook_Func(GetMethodProp(Button52, 'OnClick').Code,Func1);
  Addlog(Memo3,'ABUnHook_Func(恢复对象函数原始地址):');

  FShowHideHotKey_ID:=ABRegisterHotKey(handle,'MyF9HotKey',HotKey1.HotKey);
  Addlog(Memo3,'ABRegisterHotKey(注册热键):');
  ABUnRegisterHotKey(handle,'MyF9HotKey');
  Addlog(Memo3,'ABUnRegisterHotKey(取消热键):');

  Addlog(Memo3,'ABWaitAppEnd(运行一个文件并等待其结束):'+abbooltostr(ABWaitAppEnd(ABAppFullName)));
  Addlog(Memo3,'ABWaitAppEnd(运行一个文件并等待其结束):'+abbooltostr(ABWaitAppEnd(ABLogFile)));

  Addlog(Memo3,'ABWindowsBootMode(获取Windows启动模式):'+ABWindowsBootMode);
  Addlog(Memo3,'ABGetKeyBoardStatus(返回当前键盘特殊键的状态):'+ABBooleanArrayToStr(ABGetKeyBoardStatus([VK_NUMLOCK,VK_CAPITAL,VK_INSERT])));

  ABGetHostInfo(tempText,tempHost,tempPassWord);
  Addlog(Memo3,'ABGetHostInfo(取当前机器名称,IP,MAC):'+tempText+' '+tempHost+' '+tempPassWord);
  Addlog(Memo3,'ABFindAProcess(查找指定的进程，然后返回进程ID):'+inttostr(ABFindAProcess('C:\PROGRA~1\MI6841~1\MSSQL\binn\sqlservr.exe')));
  Addlog(Memo3,'ABFindAProcess(查找指定的进程，然后返回进程ID):'+inttostr(ABFindAProcess('sqlservr.exe')));

  Addlog(Memo3,'ABGetProcessMemorySize(取得进程占用的内存(KB)):'+ABGetFileSizeStr(ABGetProcessMemorySize('sqlservr.exe'),1000));

  ABDynamicResolution(800,600);
  Addlog(Memo3,'ABDynamicResolution(动态设置分辨率,如果宽与高是系统不支持的则设置无效):');

  ABDoBusy(true);
  Addlog(Memo3,'ABDoBusy(使鼠标变忙和恢复正常 true):');
  ABDoBusy(False);
  Addlog(Memo3,'ABDoBusy(使鼠标变忙和恢复正常 False):');

  Addlog(Memo3,'ABSynLocalDatetime(取服务器机器时间同步到本机):'+abbooltostr(ABSynLocalDatetime('grj-t430')));
  Addlog(Memo3,'ABTicker(取得开机后系统流逝的毫秒数):'+inttostr(ABTicker));
  ABGetComs(temStrings);
  Addlog(Memo3,'ABGetComs(获得计算机的COM口列表):'+temStrings.Text);

  ABSleep(5000);
  Addlog(Memo3,'ABSleep(非阻塞式Sleep 5000):');
  ABOKBeep;
  Addlog(Memo3,'ABOKBeep(操作成功的提示音):');
  ABCreateOdbc(FNeedFilePath+'aa.mdb','templink');
  Addlog(Memo3,'ABCreateOdbc(根据Access文件名创建ODBC连接):');

  temStrings.Add('*******************其它操作*****************');
  FMemoryStream.LoadFromFile(FNeedFilePath+'aa.mdb');
  ABVariantToStream(ABStreamToVariant(FMemoryStream),FMemoryStream_1);
  FMemoryStream_1.SaveToFile(FNeedFilePath+'bb.mdb');
  Addlog(Memo3,'ABStreamToVariant(流到变体):');
  Addlog(Memo3,'ABVariantToStream(变体到流):');

  ABDeleteFile(FNeedFilePath + ABAppName+'_CompressFile.zib');
  ABDeleteFile(FNeedFilePath + ABAppName+'_CompressFile.exe');
  ABDeleteFile(FNeedFilePath + ABAppName+'_CompressStream.zib');
  ABDeleteFile(FNeedFilePath + ABAppName+'_CompressStream.exe');

  ABCompressFile(ABAppFullName,FNeedFilePath + ABAppName+'_CompressFile.zib');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 压缩文件');
  ABUnCompressFile(FNeedFilePath + ABAppName+'_CompressFile.zib',FNeedFilePath + ABAppName+'_CompressFile.exe');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 解压文件');

  FMemoryStream.LoadFromFile(ABAppFullName);
  ABCompressStream(FMemoryStream);
  FMemoryStream.SaveToFile(FNeedFilePath + ABAppName+'_CompressStream.zib');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 加载文件');

  FMemoryStream.LoadFromFile(FNeedFilePath + ABAppName+'_CompressStream.zib');
  ABUnCompressStream(FMemoryStream);
  FMemoryStream.SaveToFile(FNeedFilePath + ABAppName+'_CompressStream.exe');
  Addlog(Memo3,'ABPubPakandZipU ABCompressFile 加载文件');

  FMemoryStream.clear;
  Addlog(Memo3,'ABVarIsNull(判断变体是否为空):'+abbooltostr(ABVarIsNull(ABStreamToVariant(FMemoryStream))));
  FMemoryStream.LoadFromFile(FNeedFilePath+'aa.mdb');
  Addlog(Memo3,'ABVarIsNull(判断变体是否为空):'+abbooltostr(ABVarIsNull(ABStreamToVariant(FMemoryStream))));

  Addlog(Memo3,'ABVarIsNull(判断变体是否为空 1):'+abbooltostr(ABVarIsNull(1)));
  Addlog(Memo3,'ABVarIsNull(判断变体是否为空 null):'+abbooltostr(ABVarIsNull(null)));
  Addlog(Memo3,'ABIsNull(变体为空时返回传入值，否则返回自己 1):'+vartostr(ABIsNull(1,22)));
  Addlog(Memo3,'ABIsNull(变体为空时返回传入值，否则返回自己 null):'+vartostr(ABIsNull(null,22)));

  Addlog(Memo3,'ABPointerIsNull(判断指针是否为空 FMemoryStream):'+abbooltostr(ABPointerIsNull(FMemoryStream)));
  Addlog(Memo3,'ABPointerIsNull(判断指针是否为空 nil):'+abbooltostr(ABPointerIsNull(nil)));
  ABFreeAndNil(FMemoryStream);
  Addlog(Memo3,'ABFreeAndNil(释放并置空一个对象指针 FMemoryStream):');
  Addlog(Memo3,'ABPointerIsNull(判断指针是否为空 FMemoryStream):'+abbooltostr(ABPointerIsNull(FMemoryStream)));
  Addlog(Memo3,'ABPlay(播放声音文件):'+abbooltostr(ABPlay(FNeedFilePath+'aa.wav')));

  Addlog(Memo3,'ABIIF(if语句的简写):'+VarToStr(ABIIF(true,'T','F')));
  Addlog(Memo3,'ABIIF(if语句的简写):'+VarToStr(ABIIF(true,1,0)));

  Addlog(Memo3,'ABGetIPFlag(取IP转换的数字唯一值):'+inttostr(ABGetIPFlag(ABPubUser.HostIP)));
  Addlog(Memo3,'ABGetIPFlag(取IP转换的数字唯一值):'+inttostr(ABGetIPFlag(ABPubUser.HostIP,10)));

  Addlog(Memo3,'ABGetDefaultPrinterName(取得系统默认的打印机名称):'+ABGetDefaultPrinterName);
  Addlog(Memo3,'ABGetDefaultPrinterName(根据传入的打印机或功能名取得默认的打印机):'+ABGetDefaultPrinterName('aa'));

  Addlog(Memo3,'ABIsChinese[判断字符是否是汉字 中]'+ABBoolToStr(ABIsChinese('中')));
  Addlog(Memo3,'ABIsChinese[判断字符是否是汉字 A]'+ABBoolToStr(ABIsChinese('A')));

  Addlog(Memo3,'ABHaveChinese[判断字串中是否有汉字 123中国人abc]'+ABBoolToStr(ABHaveChinese('123中国人abc')));
  Addlog(Memo3,'ABHaveChinese[判断字串中是否有汉字 123abc]'+ABBoolToStr(ABHaveChinese('123abc')));

  Addlog(Memo3,'ABStrToName[将字串转换为可用的名称 ./\:123中国人abc]'+ABStrToName('./\:123中国人abc'));
  Addlog(Memo3,'ABStrToName[将字串转换为可用的名称 123中国人abc]'+ABStrToName('123中国人abc'));

  ABReportFileWordCount('',nil);
  Addlog(Memo3,'ABStrToName[统计文本文件中英文单词出现的次数]');
end;

end.




