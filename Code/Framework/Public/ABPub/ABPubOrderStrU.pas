{
�ַ��������嵥Ԫ�������ֶ������ܣ�
}
unit ABPubOrderStrU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  abpubfuncU,
  ABPubItemListFrameU,

  SysUtils,Classes,Controls,Forms,ExtCtrls,DB, StdCtrls, ABPubPanelU;

type
  TABOrderStrForm = class(TABPubForm)
    pnl1: TPanel;
    btn1: TButton;
    btn2: TButton;
    ABItemList1: TABItemList;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//�ֶ�������
//aValueΪ��ʼ����aSpaceSign������ֶ�����,�����������ֶ�����
function ABOrderFieldNames(var aValue:string;aDataSet:TDataSet;aSpaceSign:string=',';aCaption:String=''):Boolean;
//�ִ�����
//aValueΪ��ʼ����aSpaceSign������ֶ�����,�����������ֶ�����
function ABOrderStrs(var aValue:string;aSpaceSign:string=',';aCaption:String=''):Boolean;

implementation

{$R *.dfm}

function ABOrderFieldNames(var aValue:string;aDataSet:TDataSet;aSpaceSign:string;aCaption:String):Boolean;
var
  tempForm:TABOrderStrForm;
  I: Integer;
begin
  Result:=false;
  tempForm:=TABOrderStrForm.Create(nil);
  if aCaption<>emptystr then
    tempForm.Caption:=aCaption;
  try
    tempForm.ABItemList1.Frame.ListBox1.Items.Clear;
    if (Assigned(aDataSet)) then
    begin
      if (aDataSet.Fields.Count>0) then
      begin
        for I := 0 to aDataSet.Fields.Count - 1 do
          tempForm.ABItemList1.Frame.ListBox1.Items.Add(inttostr(i)+'='+aDataSet.Fields[i].DisplayLabel);
      end
      else if (aDataSet.FieldDefs.Count>0) then
      begin
        for I := 0 to aDataSet.FieldDefs.Count - 1 do
          tempForm.ABItemList1.Frame.ListBox1.Items.Add(aDataSet.FieldDefs[i].Name);
      end;
    end;

    aValue:=emptystr;
    if tempForm.ShowModal=mrOK then
    begin
      if (aDataSet.Fields.Count>0) then
      begin
        for I := 0 to tempForm.ABItemList1.Frame.ListBox1.Items.Count - 1 do
        begin
          abaddstr(aValue,aDataSet.Fields[strtoint(tempForm.ABItemList1.Frame.ListBox1.Items.Names[i])].FieldName,aSpaceSign);
        end;
      end
      else if (aDataSet.FieldDefs.Count>0) then
      begin
        aValue:=ABStringsToStrs(tempForm.ABItemList1.Frame.ListBox1.Items,aSpaceSign);
      end;
      Result:=True;
    end;
  finally
    tempForm.Free;                         
  end;
end;

function ABOrderStrs(var aValue:string;aSpaceSign:string=',';aCaption:String=''):Boolean;
var
  tempForm:TABOrderStrForm;
begin
  Result:=false;
  tempForm:=TABOrderStrForm.Create(nil);
  if aCaption<>emptystr then
    tempForm.Caption:=aCaption;
  try
    ABStrsToStrings(aValue,aSpaceSign,tempForm.ABItemList1.Frame.ListBox1.Items);

    if tempForm.ShowModal=mrOK then
    begin
      aValue:=ABStringsToStrs(tempForm.ABItemList1.Frame.ListBox1.Items,aSpaceSign);
      Result:=True;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABOrderStrForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABOrderStrForm.btn2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABOrderStrForm.FormShow(Sender: TObject);
begin
  ABSetListBoxWidth(ABItemList1.Frame.ListBox1);
end;


end.

