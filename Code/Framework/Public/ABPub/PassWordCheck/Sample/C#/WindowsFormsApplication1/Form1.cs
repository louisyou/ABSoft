﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace WindowsFormsApplication1
{

    
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        [DllImport("ABManagePassWordD.dll", EntryPoint = "CheckPassword", SetLastError = true,
          CharSet = CharSet.Ansi, ExactSpelling = true,
           CallingConvention = CallingConvention.StdCall)]
        protected static extern Int32 CheckPassword(string aWord, string aPassWord);


        [DllImport("ABManagePassWordD.dll", EntryPoint = "DOPassWord", SetLastError = true,
          CharSet = CharSet.Ansi, ExactSpelling = true,
           CallingConvention = CallingConvention.StdCall)]
        protected static extern string DOPassWord(string aPassWord);

        [DllImport("ABManagePassWordD.dll", EntryPoint = "UnDOPassWord", SetLastError = true,
          CharSet = CharSet.Ansi, ExactSpelling = true,
           CallingConvention = CallingConvention.StdCall)]
        protected static extern string UnDOPassWord(string aPassWord);


        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckPassword(textBox1.Text, textBox2.Text)==0)
                MessageBox.Show("成功！");
            else
                MessageBox.Show("失败！");
        } 

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = DOPassWord(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = UnDOPassWord(textBox2.Text);
        } 
    }
}
