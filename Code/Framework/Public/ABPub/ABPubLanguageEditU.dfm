object ABPubLanguageEditForm: TABPubLanguageEditForm
  Left = 455
  Top = 161
  Caption = #22810#35821#35328#32534#36753#22120
  ClientHeight = 500
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 459
    Width = 700
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      700
      41)
    object Button1: TButton
      Left = 216
      Top = 6
      Width = 150
      Height = 25
      Caption = #20174'INI'#25991#20214#21152#36733#35821#35328
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 103
      Top = 6
      Width = 81
      Height = 25
      Caption = #21024#38500#35821#35328
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 600
      Top = 6
      Width = 81
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button5: TButton
      Left = 16
      Top = 6
      Width = 81
      Height = 25
      Caption = #22686#21152#35821#35328
      TabOrder = 3
      OnClick = Button5Click
    end
    object Button4: TButton
      Left = 371
      Top = 6
      Width = 150
      Height = 25
      Caption = #21478#23384#35821#35328#20026'INI'#25991#20214
      TabOrder = 4
      OnClick = Button4Click
    end
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 700
    Height = 459
    Align = alClient
    ColCount = 2
    DefaultColWidth = 170
    DefaultRowHeight = 21
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSizing, goColSizing, goEditing]
    TabOrder = 1
    OnExit = StringGrid1Exit
    OnSelectCell = StringGrid1SelectCell
    OnSetEditText = StringGrid1SetEditText
  end
end
