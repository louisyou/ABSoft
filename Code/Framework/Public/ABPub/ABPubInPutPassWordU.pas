{
密码框输入单元
}
unit ABPubInPutPassWordU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubMessageU,

  abPubUserU,

  SysUtils,Variants,Classes,Controls,Forms,StdCtrls;

type
  TABInPutPassWordForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    leNewPassword: TEdit;
    Label2: TLabel;
    procedure leNewPasswordKeyPress(Sender: TObject; var Key: Char);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//检测输入的密码是否正确
function ABInputPassWord(ACaption:string='';aPassWord:string=''):boolean;

implementation                                                          

{$R *.dfm}
function ABInputPassWord(ACaption:string='';aPassWord:string=''):boolean;
var
  tempForm: TABInPutPassWordForm;
begin
  result:=false;
  //机器名是grj则不用输入参数管理密码
  if ABPubUser.IsNoCheckHost then
  begin
    result:=True;
  end
  else
  begin
    tempForm:=TABInPutPassWordForm.Create(nil);
    try
      if ACaption<>emptystr then
        tempForm.Caption:=ACaption;
      if tempForm.ShowModal=mrOk then
      begin
        result:=(tempForm.leNewPassword.Text=aPassWord) or
                (ABPubUser.IsNoCheckAdminPasss(tempForm.leNewPassword.Text));
        if not result then
        begin
          abshow('密码不对,不能操作,请检查.');
        end;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABInPutPassWordForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABInPutPassWordForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABInPutPassWordForm.FormShow(Sender: TObject);
begin
  if leNewPassword.CanFocus then
    leNewPassword.SetFocus;
end;

procedure TABInPutPassWordForm.leNewPasswordKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13)  then
    Button1Click(nil);
end;


end.
