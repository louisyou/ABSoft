{
显示与编辑备注单元
}
unit ABPubMemoU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,

  SysUtils,Classes,Controls,Forms,StdCtrls,ExtCtrls;

type
  TABMemoForm = class(TABPubForm)
    Memo1: TMemo;
    pnl1: TPanel;
    btn1: TButton;
    Button1: TButton;
    procedure Button2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//显示或编辑备注
function ABShowEditMemo(var aText: string;aReadOnly:Boolean=false;aCaption:string=''): boolean;

implementation

{$R *.dfm}

function ABShowEditMemo(var aText: string;aReadOnly:Boolean=false;aCaption:string=''): boolean;
var
  tempForm:TABMemoForm;
begin
  result:=False;
  tempForm := TABMemoForm.Create(nil);
  if aCaption<>EmptyStr then
    tempForm.Caption:=aCaption;
  tempForm.Memo1.ReadOnly:= aReadOnly;
  if aReadOnly then
  begin
    tempForm.Button1.Left:=tempForm.btn1.Left;
    tempForm.btn1.Visible:=false;
  end;
  try
    tempForm.Memo1.Text:=aText;
    if tempForm.ShowModal=mrok then
    begin
      aText:= tempForm.Memo1.Text;
      result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABMemoForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABMemoForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
