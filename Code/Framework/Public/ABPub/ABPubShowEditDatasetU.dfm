object ABShowEditDatasetForm: TABShowEditDatasetForm
  Left = 191
  Top = 198
  Caption = #26174#31034#25110#32534#36753#25968#25454#38598#25968#25454
  ClientHeight = 400
  ClientWidth = 600
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 25
    Width = 600
    Height = 334
    Align = alClient
    DataSource = DataSource1
    ImeName = #24555#20048#20116#31508
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object ABMultilingualDBNavigator1: TABMultilingualDBNavigator
    Left = 0
    Top = 0
    Width = 600
    Height = 25
    DataSource = DataSource1
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
    Align = alTop
    Flat = True
    TabOrder = 1
  end
  object pnl1: TPanel
    Left = 0
    Top = 359
    Width = 600
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      600
      41)
    object btn1: TButton
      Left = 428
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      Default = True
      TabOrder = 0
      OnClick = btn1Click
    end
    object btn2: TButton
      Left = 509
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 1
      OnClick = btn2Click
    end
  end
  object DataSource1: TDataSource
    AutoEdit = False
    Left = 288
    Top = 256
  end
end
