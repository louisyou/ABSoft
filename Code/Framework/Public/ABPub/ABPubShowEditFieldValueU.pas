{
显示与编辑数据集字段值单元(不支持图片字段)
}
unit ABPubShowEditFieldValueU;

interface                              
{$I ..\ABInclude.ini}

uses
  ABPubFormU,


  ABPubVarU,

  SysUtils,Classes,Controls,Forms,StdCtrls,ExtCtrls,DB,DBCtrls;

type
  TABShowEditFieldValueForm = class(TABPubForm)
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    DBMemo1: TDBMemo;
    DataSource1: TDataSource;
    pnl1: TPanel;
    btn1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    FEditField: string;
    FReadOnlyField: string;
    { Private declarations }
  public
    property ReadOnlyField:string read FReadOnlyField write FReadOnlyField;
    property EditField:string read FEditField write FEditField;
    { Public declarations }
  end;

//显示或编辑数据集字段数据
//aNoHaveField=不包含的字段
//aHaveField=包含的字段
//aReadOnlyField=只读的字段
//aEditField=能编辑的字段
//aCaption=显示框标题
function ABShowEditFieldValue(aDataSet:TDataSet;
                               aNoHaveField:string='';aHaveField:string='';
                               aReadOnlyField:string='';aEditField:string='';
                               aCaption:string=''
                               ):boolean;
implementation
{$R *.dfm}

function ABShowEditFieldValue(aDataSet:TDataSet;
                           aNoHaveField:string;aHaveField:string;
                           aReadOnlyField:string;aEditField:string;
                           aCaption:string):boolean;
var
  tempForm:TABShowEditFieldValueForm;
  tempResult,i:LongInt;
begin
  Result:=True;
  aNoHaveField:=UpperCase(aNoHaveField);
  aHaveField:=UpperCase(aHaveField);
  aReadOnlyField:=UpperCase(aReadOnlyField);
  aEditField:=UpperCase(aEditField);

  tempForm := TABShowEditFieldValueForm.Create(nil);
  tempForm.DataSource1.DataSet:=aDataSet;

  if aCaption<>EmptyStr then
     tempForm.Caption:=aCaption;

  tempForm.ReadOnlyField:=aReadOnlyField;
  tempForm.EditField:=aEditField;
  try
    for I := 0 to aDataSet.FieldCount - 1 do
    begin
      if (not (aDataSet.Fields[i].DataType in ABBlobDataType)) and
         ((aNoHaveField=EmptyStr)  or (Pos(','+UpperCase(aDataSet.Fields[i].FieldName)+',',','+aNoHaveField  +',')<=0)) and
         ((aHaveField=EmptyStr) or (Pos(','+UpperCase(aDataSet.Fields[i].FieldName)+',',','+aHaveField+',')> 0)) then
      begin
        if tempForm.ComboBox1.Items.IndexOf(inttostr(aDataSet.Fields[i].Index)+'='+aDataSet.Fields[i].DisplayLabel)<0 then
        begin
          tempForm.ComboBox1.Items.Add(inttostr(aDataSet.Fields[i].Index)+'='+aDataSet.Fields[i].DisplayLabel);
        end;
      end;
    end;

    tempResult:=tempForm.ShowModal;
    case tempResult of
      mrNo,mrCancel:
      begin
        Result:=False;
        if (aDataSet.State in [dsEdit,dsInsert]) then
          aDataSet.Cancel;
      end;
    end;
   finally
    tempForm.Free;    
  end;
end;

procedure TABShowEditFieldValueForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABShowEditFieldValueForm.Button2Click(Sender: TObject);
begin
  if DBMemo1.Focused then
  begin
    if ComboBox1.CanFocus then
      ComboBox1.SetFocus;
  end;

  ModalResult:=mrOk;
end;

procedure TABShowEditFieldValueForm.ComboBox1Change(Sender: TObject);
begin
  if (ComboBox1.Text<>EmptyStr) and
     (ComboBox1.ItemIndex>=0) and
     (strtoint(ComboBox1.Items.Names[ComboBox1.ItemIndex])>=0)   then
  begin
    DBMemo1.DataField:=DataSource1.DataSet.Fields[strtoint(ComboBox1.Items.Names[ComboBox1.ItemIndex])].FieldName;
    DBMemo1.ReadOnly:=not (((ReadOnlyField=EmptyStr) or (Pos(','+UpperCase(DBMemo1.DataField)+',',','+ReadOnlyField  +',')<=0)) and
                           ((EditField=EmptyStr)   or (Pos(','+UpperCase(DBMemo1.DataField)+',',','+EditField+',')> 0)));
  end;
end;


end.
