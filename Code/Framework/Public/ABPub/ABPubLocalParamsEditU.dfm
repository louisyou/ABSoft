object ABPubLocalParamsEditForm: TABPubLocalParamsEditForm
  Left = 310
  Top = 136
  Caption = #26412#22320#21442#25968#32534#36753#22120
  ClientHeight = 500
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 156
    Width = 700
    Height = 344
    Align = alClient
    Color = clMenuBar
    ReadOnly = True
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 700
    Height = 156
    Align = alTop
    Caption = #24120#29992#35774#32622
    TabOrder = 1
    object Label4: TLabel
      Left = 22
      Top = 104
      Width = 101
      Height = 13
      AutoSize = False
      Caption = 'Sock'#26412#22320#31471#21475
    end
    object Label5: TLabel
      Left = 22
      Top = 132
      Width = 101
      Height = 13
      AutoSize = False
      Caption = 'Sock'#30446#30340#22320#31471#21475
    end
    object SpeedButton1: TSpeedButton
      Left = 203
      Top = 23
      Width = 152
      Height = 22
      Caption = #35774#32622#21151#33021#40664#35748#25171#21360#26426
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 203
      Top = 44
      Width = 152
      Height = 22
      Caption = #25171#24320#26085#24535
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 203
      Top = 65
      Width = 152
      Height = 22
      Caption = #35821#35328#31649#29702
      OnClick = SpeedButton3Click
    end
    object CheckBox1: TCheckBox
      Left = 22
      Top = 49
      Width = 160
      Height = 17
      Caption = #35843#35797#27169#24335
      TabOrder = 0
      OnClick = CheckBox1Click
    end
    object CheckBox2: TCheckBox
      Left = 22
      Top = 76
      Width = 160
      Height = 17
      Caption = #21551#29992#22810#35821#35328
      TabOrder = 1
      OnClick = CheckBox2Click
    end
    object SpinEdit1: TSpinEdit
      Left = 126
      Top = 102
      Width = 60
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 8000
      OnChange = SpinEdit1Change
    end
    object SpinEdit2: TSpinEdit
      Left = 126
      Top = 129
      Width = 60
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 8001
      OnChange = SpinEdit2Change
    end
    object RadioGroup2: TRadioGroup
      Left = 422
      Top = 15
      Width = 138
      Height = 139
      Align = alRight
      Caption = #23458#25143#31471#32972#26223#31867#22411
      Items.Strings = (
        #31354#30333
        #22270#29255
        #37038#20214
        #27969#31243
        #33258#23450#20041#25968#25454#38598)
      TabOrder = 4
      OnClick = RadioGroup2Click
    end
    object CheckBox4: TCheckBox
      Left = 22
      Top = 23
      Width = 160
      Height = 17
      Caption = #26174#31034#21551#21160#31383#20307
      TabOrder = 5
      OnClick = CheckBox4Click
    end
    object RadioGroup1: TRadioGroup
      Left = 560
      Top = 15
      Width = 138
      Height = 139
      Align = alRight
      Caption = #23458#25143#31471#36830#25509#31867#22411
      ItemIndex = 0
      Items.Strings = (
        #20004#23618
        #19977#23618
        #27983#35272#22120)
      TabOrder = 6
      OnClick = RadioGroup1Click
    end
  end
end
