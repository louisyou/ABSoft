{
Socket通讯的封装组件单元

//发包后的 sleep Application.ProcessMessages; 和报错后的sleep
//如果事件数组中有某一个事件被传信了，函数会返回这个事件的索引值，但是这个索引值需要减去预定义值 WSA_WAIT_EVENT_0才是这个事件在事件数组中的位置
//WSAWaitForMultipleEvents( cEvents,lphEvents,fWaitAll,dwTimeout,fAlertable );
//cEvents：指出lphEvents所指数组中事件对象句柄的数目。事件对象句柄的最大值为WSA_MAXIMUM_WAIT_EVENTS。
//lphEvents：指向一个事件对象句柄数组的指针。
//fWaitAll：指定等待类型。若为真TRUE，则当lphEvents数组中的所有事件对象同时有信号时，函数返回。若为假FALSE，
//则当任意一个事件对象有信号时函数即返回。在后一种情况下，返回值指出是哪一个事件对象造成函数返回。
//dwTimeout：指定超时时间间隔（以毫秒计）。当超时间隔到，函数即返回，不论fWaitAll参数所指定的条件是否满足。
//如果dwTimeout为零，则函数测试指定的时间对象的状态，并立即返回。如果dwTimeout是WSA_INFINITE，则函数的超时间隔永远不会到。
//fAlertable：指定当系统将一个输入/输出完成例程放入队列以供执行时，函数是否返回。若为真TRUE，
//则函数返回且执行完成例程。若为假FALSE，函数不返回，不执行完成例程。请注意在Win16中忽略该参数。
//检测所指定的套接字上发生了哪种事件
//WSAEnumNetworkEvents(FSockArray[i], FEventArray[i], tempCurNetworkEvent);

  //端口复用
  var
    flag:LongInt;
  flag := 1;
  if SetSockopt(FLocalSocket,SOL_SOCKET,SO_REUSEADDR,pansichar(@flag),sizeof(flag)) = SOCKET_ERROR then
  begin
    SocketError(WSAGetLastError);
    closesocket(FLocalSocket);
    Exit;
  end;

 FD_READ：套接字可读通知。
 FD_WRITE：可写通知。
 FD_ACCEPT：服务器接收连接的通知。
 FD_CONNECT：有客户连接通知。
 FD_OOB：外带数据到达通知。
 FD_CLOSE：套接字关闭通知。
 FD_QOS：服务质量发生变化通知。
 FD_GROUP_QOS：组服务质量发生变化通知。
 FD_ROUTING_INTERFACE_CHANGE：与路由器接口发生变化的通知。
 FD_ADDRESS_LIST_CHANGE：本地地址列表发生变化的通知。

 事件值                             含义                                  触发条件
FD_READ          套接口有可读消息通知                      recv,recvfrom,WSARecv或WSARecvFrom
FD_WRITE         套接口有可发消息通知                      send,sendto,WSASend或WSASendTo
FD_OOB           套件口有外带数据消息通知                recv,recvfrom,WSARecv或WSARecvFrom
FD_ACCEPT        套接口有链接请求消息通知                accept或WSAAccept(错误码不能WSATRY_AGAIN)
FD_CONNECT       希望得到connect或多点join操作完成信息通知     无
FD_CLOSE(面向连接) 套接口关闭消息通知                                  无
FD_QOS             套接口QOS状态发生变化消息通知      WSAIoctl(SIO_GET_QOS)
FD_GROUP_QOS       保留                                              Reserved
FD_ROUTING_INTERFACE_CHANGE 特定方向的路由接口发生改变消息通知。                      WSAIoctl(SIO_ROUTING_INTERFACE_CHANGE)
FD_ADDRESS_LIST_CHANGE      得到本地地址列表上套接口协议族发生改变通知。         WSAIoctl(SIO_ADDRESS_LIST_CHANGE)

[1]FD_READ事件触发条件：
1.在数据到达socket后，并且从来没有触发过FD_READ(也就是最开始的阶段)
2.调用recv()后，缓冲区还有未读完的数据

注意：
    1.winsock2发出一个FD_READ后，如果程序没有用recv()，
    即使还有数据没接收FD_READ也不会再触发另一个FD_READ，要等到recv()调用后FD_READ才会发出。
    2.对一个FD_READ多次recv()的情形：如果程序对一个FD_READ多次recv()将会造成触发多个空的FD_READ，
    所以程序在第2次recv()前要关掉FD_READ(可以使用WSAAsynSelect关掉FD_READ)，然后再多次recv()。
    3.recv()返回WSAECONNABORTED,WSAECONNRESET...等消息，可以不做任何处理，可以等到FD_CLOSE事件触发时再处理

[2]FD_ACCEPT事件触发条件：
1.当有请求建立连接，并且从来没有触发过FD_ACCEPT(也就是最开始的阶段)
2.当有请求建立连接，并且前一个accept()调用后
注意：当FD_ACCEPT触发后，如果程序没有调用accept(),即使还有建立连接的请求FD_ACCEPT也不会触发，要直到accept()调用后
[3]FD_WRITE事件触发条件：
1.第一次connect()或accept()后(即连接建立后)
2.调用send（WSASend）/sendto（WSASendTo）发送失败返回WSAEWOULDBLOCK，并且当缓冲区有可用空间时，则会触发FD_WRITE事件
注意：send出去的数据其实都先存在winsock的发送缓冲区中，然后才发送出去，如果缓冲区满了，
      那么再调用send（WSASend,sendto,WSASendTo）的话，就会返回一个 WSAEWOULDBLOCK的错误码，
      接下来随着发送缓冲区中的数据被发送出去，缓冲区中出现可用空间时，一个 FD_WRITE 事件才会被触发，
      这里比较容易混淆的是 FD_WRITE 触发的前提是 缓冲区要先被充满然后随着数据的发送又出现可用空间，而不是缓冲区中有可用空间，
      问题在于建立连接后 FD_WRITE 第一次被触发， 如果send发送的数据不足以充满缓冲区，虽然缓冲区中仍有空闲空间，
      但是 FD_WRITE 不会再被触发，程序永远也等不到可以发送的网络事件。
      基于以上原因，在收到FD_WRITE事件时，程序就用循环或线程不停的send数据，直至send返回WSAEWOULDBLOCK，
      表明缓冲区已满，再退出循环或线程。当缓冲区中又有新的空闲空间时，FD_WRITE 事件又被触发，程序被通知后又可发送数据了。

[4]FD_CLOSE事件触发条件：自己或对端中断连接后
注意：closesocket()调用后FD_CLOSE不会触发
[5]FD_CONNECT事件触发条件：调用了connect()，并且连接建立后。


各种模式效率比较
模式1
缓冲1024时
UDP(发送每一传输包有ACK)
文本 10000次50字节(1行[123中华人民共和国456中国]) 约2分10秒
文本 1次 1000000字节(10000行01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567) 约2 秒
文件（13,916,157） 约30 秒


缓冲1024时
TCP(发送每一上层包有ACK)
文本 10000次50字节(1行[123中华人民共和国456中国]) 约2分45秒
文本 1次 1000000字节(10000行01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567) 约3 秒
文件（13,916,157） 36秒

模式2
缓冲1024时
UDP(发送每一传输包有ACK)
文本 10000次50字节(1行[123中华人民共和国456中国]) 约30 秒
文本 1次 1000000字节(10000行01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567) 约5 秒
文件（13,916,157） 1分


缓冲1024时
TCP(发送每一上层包有ACK)
文本 10000次50字节(1行[123中华人民共和国456中国]) 约32 秒
文本 1次 1000000字节(10000行01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567) 约3 秒
文件（13,916,157） 30秒

模式3
缓冲1024时
UDP(发送每一传输包有ACK)
文本 10000次50字节(1行[123中华人民共和国456中国]) 约2分45秒
文本 1次 1000000字节(10000行01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567) 约3 秒
文件（13,916,157） 35秒


缓冲1024时
TCP(发送每一上层包有ACK)
文本 10000次50字节(1行[123中华人民共和国456中国]) 约2分5秒
文本 1次 1000000字节(10000行01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567) 约3 秒
文件（13,916,157） 27秒


  }

unit ABPubSockU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubVarU,
  ABPubMessageU,
  ABPubFuncU,
  ABPubConstU,


  Windows,ComCtrls, WinSock2, SysUtils, Classes, Messages, Forms,Types;

const
  //异步选择模型时监听的消息
  WM_ASYNCSELECT = WM_USER + 150;
  //加入与离开一个组播
  IP_ADD_MEMBERSHIP         =  12; //* add an IP group membership */
  IP_DROP_MEMBERSHIP        =  13; //* drop an IP group membership */
  RECV_POSTED = 0;
  SEND_POSTED = 1;
  TIME_OUT = 550;

type
  //加入与离开一个组播时用到的组播结构
  ip_mreq = record
    imr_multiaddr: in_addr;  (* IP multicast address of group *)
    imr_interface: in_addr;  (* local IP address of interface *)
  end;
  TIpMReq = ip_mreq;
  PIpMReq = ^ip_mreq;

  TSocketObject=(soNull,soText,soFile,soStream,soByteArray,
                        soTextACK,soFileACK,soStreamACK,soByteArrayACK);
  //Socket通讯中信息的头部结构,在传送数据的前N个字节中
  TSocketPackage = packed record
    _Type: TSocketObject;         //包类型:1=短信，2=文件,3=数据流,4=字节流,11=短信ACK,21=文件ACK,31=流ACK ,41=流ACK
    _Number: LongInt;             //包序号(发送一个包后将自动加一)
    _DataLength: LongInt;         //包长度

    _SmallPkgCount: LongInt;      //分小包发送时分包的次数
    _SmallPkgNumber: LongInt;     //分小包发送时小包的流水号
    _SmallPkgDataLength: LongInt; //分小包发送时小包的长度
  end;

  TOnEvent = procedure(Sender: TObject; aSocket: TSocket;aEvent:LongInt;aEventDesc:string) of object;
  TOnError = procedure(Sender: TObject; Error: integer; Msg: string) of object;
  TOnData = procedure(Sender: TObject; aSocket: TSocket;aFromIP,aFromPort:string;aSocketPackage:TSocketPackage;aBuff:PByte;aFileName:string) of object;
  TOnAccept = procedure(Sender: TObject; aSocket: TSocket) of object;
  TOnConnect = procedure(Sender: TObject; aSocket: TSocket) of object;
  TOnClose = procedure(Sender: TObject; aSocket: TSocket) of object;

  //TCP服务器下的客户端
  TABClientList = class(TObject)
  private
    FSockets: TList;
  protected
    function GetSockets(Index: integer): TSocket;
    function GetCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    function Add(aSocket: TSocket): boolean;
    procedure Delete(aSocket: TSocket);
    procedure Clear;
    function IndexOf(aSocket: TSocket): integer;
    property Sockets[Index: integer]: TSocket read GetSockets; default;
    property Count: integer read GetCount;
  end;

  TABSocket = class(TComponent)
  private
    FSocketType: TABSocketType;
    FSocketModel: TABSocketModel;

    FSocket: TSocket;
    FSocketAddrIn: TSockAddrIn;
    FSocketState: TABSocketState;

    FOnError: TOnError;
    FOnData: TOnData;
    FOnClose: TOnClose;
    FOnAccept: TOnAccept;
    FOnConnect: TOnConnect;
    FOnEvent: TOnEvent;

    FLocalPort: Word;
    FToHost: string;
    FToPort: Word;

    FBuff: TByteDynArray;
    FBuffAdd1: TByteDynArray;
    FBuffSize: LongInt;

    FLastReadPackage: TSocketPackage;
    FLastWritePackage: TSocketPackage;

    FLastReadACKPackage: TSocketPackage;
    FLastWriteACKPackage: TSocketPackage;

    FACKList: TStrings;
    FACKOnData: Boolean;

    FClientLists: TABClientList;
    FMaxError: WORD;

    //UDP方式下多播IP
    FGroupBroadcastIP: string;
    //UDP方式下全255IP广播的标志
    FAllBroadcastFlag:LongInt;
    //接收大文时的数据流，直接存盘中，不用占用内存
    FReadBigPkgFile:TFileStream;
    FModelObject: TObject;

    //设置Buff缓冲区大小
    procedure SetBufSize(const Value: LongInt);
    //根据Buff缓冲区大小设置Socket发送和接收缓冲区
    procedure RefreshRCVAndSNDBUF;
    //根据Socket包对象得到ACK对象
    function GetSocketObjectACK(aSockecObject: TSocketObject): TSocketObject;
    procedure SetACKList(const Value: TStrings);
  protected
    //Socket事件处理函数
    function DoRead(aSocket: TSocket;aReadLen:LongInt=0):LongInt; virtual;
    procedure DoClose(aSocket: TSocket); virtual;
    function DoAccept(var aSocket: TSocket): Boolean; virtual;
  public
    //收到数据后ACK
    function DoACK(aSocket: TSocket;aSockAddrIn: TSockAddrIn): Boolean;

    //Socket错误时调用的过程,触发FOnError事件
    procedure SocketError(aError: integer;aDesc:string='');

    //TCP服务器时连接的客户端列表
    property ClientLists: TABClientList read FClientLists;

    //发送数据,UDP方式下支持255.255.255.255与192.168.0.255两种类型的广播及组播IP
    //aSocket=发送数据的Socket;
    //aType=数据类型,1=短信，2=文件,3=数据流; aData=发送的数据或对象; aSize=发送数据的大小
    //aToHost=发送到的主机;aToPort=发送到的主机端口; aProgressBar=发送时的进度条
    //aUseLessMemory=true时，使用TFileStream打开文件，文件发送时去读取硬盘
    //aUseLessMemory=False时，使用TMemoryStream打开文件，文件一次加载到内存
    function DoSend      (aSocket: TSocket;
                          aType  : TSocketObject; aData  : PByte  ; aSize  : LongInt;
                          aToHost: string ; aToPort: LongInt;
                          aProgressBar:TProgressBar=nil;aUseLessMemory:Boolean=True): Boolean;

    function SendFile    (aSocket: TSocket;aFileName:string     ; aToHost: string ; aToPort: Integer; aProgressBar:TProgressBar=nil; aUseLessMemory:Boolean=True): Boolean;overload;
    function SendText    (aSocket: TSocket;aData    :AnsiString ; aToHost: string ; aToPort: Integer; aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendStream  (aSocket: TSocket;aStream  :TStream    ; aToHost: string ; aToPort: Integer; aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendByteArray(aSocket: TSocket;aPByte: PByte;aCount:longint; aToHost: string ; aToPort: Integer; aProgressBar:TProgressBar=nil): Boolean;overload;

    //TCP服务端发送
    function SendFile    (aSocket: TSocket;aFileName: string   ; aProgressBar:TProgressBar=nil; aUseLessMemory:Boolean=True): Boolean;overload;
    function SendText    (aSocket: TSocket;aData    :AnsiString; aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendStream  (aSocket: TSocket;aStream  :TStream   ; aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendByteArray(aSocket: TSocket;aPByte: PByte;aCount:longint ; aProgressBar:TProgressBar=nil): Boolean;overload;

    //TCP客户端发送
    function SendFile    (aFileName:string    ; aProgressBar:TProgressBar=nil; aUseLessMemory:Boolean=True): Boolean;overload;
    function SendText    (aData    :AnsiString; aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendStream  (aStream  :TStream   ; aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendByteArray(aPByte: PByte;aCount:longint ; aProgressBar:TProgressBar=nil): Boolean;overload;

    //UDP发送
    function SendFile    (aFileName: string   ; aToHost: string ; aToPort: Integer;aProgressBar:TProgressBar=nil;aUseLessMemory:Boolean=True): Boolean;overload;
    function SendText    (aData    :AnsiString; aToHost: string ; aToPort: Integer;aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendStream  (aStream  :TStream   ; aToHost: string ; aToPort: Integer;aProgressBar:TProgressBar=nil): Boolean;overload;
    function SendByteArray(aPByte: PByte;aCount:longint; aToHost: string ; aToPort: Integer;aProgressBar:TProgressBar=nil): Boolean;overload;

  public
    //Socket模型对象
    property ModelObject: TObject read FModelObject;
    //本地Socket
    property Socket: TSocket read FSocket write FSocket;
    //本地Socket的TSockAddrIn结构
    property SocketAddrIn: TSockAddrIn read FSocketAddrIn  write FSocketAddrIn;
    //本地Socket状态
    property SocketState: TABSocketState read FSocketState write FSocketState;

    //缓存数据
    property Buff: TByteDynArray read FBuff write FBuff;

    //最后一次读取的结构信息
    property LastReadPackage: TSocketPackage read FLastReadPackage write FLastReadPackage;
    //最后一次发送的结构信息
    property LastWritePackage: TSocketPackage read FLastWritePackage write FLastWritePackage;
    //最后一次读取ACK的结构信息
    property LastReadACKPackage: TSocketPackage read FLastReadACKPackage write FLastReadACKPackage;
    //最后一次发送ACK的结构信息
    property LastWriteACKPackage: TSocketPackage read FLastWriteACKPackage write FLastWriteACKPackage;

    //发送数据后收到的ACK列表
    property ACKList: TStrings read FACKList write SetACKList;

    //打开Socket
    procedure Open;
    //关闭Socket
    procedure Close;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    //本地端，不指定时（=0）将由系统分配
    property LocalPort: Word read FLocalPort write FLocalPort;
    //目标IP
    property ToHost: string read FToHost write FToHost;
    //目标端口
    property ToPort: Word read FToPort write FToPort;

    //Socket类型
    property SocketType: TABSocketType read FSocketType  write FSocketType;
    //Socket模型
    property SocketModel: TABSocketModel read FSocketModel  write FSocketModel;

    //本地Socket错误触发事件
    property OnError: TOnError read FOnError write FOnError;
    //本地Socket收到数据时触发事件
    property OnData: TOnData read FOnData write FOnData;
    //本地Socket关闭触发事件
    property OnClose: TOnClose read FOnClose write FOnClose;
    //TCP客户端连接事件
    property OnConnect: TOnConnect read FOnConnect write FOnConnect;
    //同意TCP客户端连接事件
    property OnAccept: TOnAccept read FOnAccept write FOnAccept;
    //所有网络事件
    property OnEvent: TOnEvent read FOnEvent write FOnEvent;

    //一次发送与接收的缓存大小(包含通讯包开头的结构) UDP方式下取值范围： 25~65507；TCP无限制  default 1024
    property BuffSize: LongInt read FBuffSize write SetBufSize;
    //发送时许可的最大错误次数
    property MaxError: WORD read FMaxError  write FMaxError;

    //UDP组播IP,多个组播时用分号隔开
    property GroupBroadcastIP: string read FGroupBroadcastIP write FGroupBroadcastIP;
    //收到的ACK信息是否触发OnData事件
    property ACKOnData: Boolean read FACKOnData write FACKOnData;
  end;

  TABCustomThread = class(TThread)
  private
    FSocket:TABSocket;
  protected
    procedure Execute; override;
  public
    constructor Create(aSocket:TABSocket); overload;
  end;

  //一：select模型(有64TCP客户端限制)
  TABSelectModelThread = class(TABCustomThread)
  private
  protected
    procedure Execute; override;
  public
  end;

  //二：WSAAsyncSelect模型
  TABWSAAsyncModeClass = class(TComponent)
  private
    { Private declarations }
    FHandle: HWND;
    FSocket:TABSocket;
    procedure DoConnect(aSocket: TSocket);
    procedure DoWrite(aSocket: TSocket);
    function DoAccept(aSocket: TSocket): Boolean;
  protected
    //异步选择模型时消息过程
    procedure WndProc(var AMsg: TMessage);
  public
    function Open: boolean;
    procedure close;

    constructor Create(aSocket:TABSocket); overload;
    destructor Destroy; override;
  end;

  TABCustomEventThread = class(TABCustomThread)
  private
    FEventTotal: DWORD;
    FSockArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of TSocket;
    FEventArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of WSAEVENT;
  protected
    function CreateWSAEventSelect(aSocket: TSocket;aEventMask:LongInt): WSAEvent;
  public
  end;

  //  三：WSAEventSelect模型 (有64TCP客户端限制)
  TABWSAEventModeThread = class(TABCustomEventThread)
  private
  protected
    procedure Execute; override;
  public
  end;

  //  四：Overlapped I/O(重叠I/O) 用事件通知实现模型
  TABOverlapIO_EventModeThread = class(TABCustomEventThread)
  private
  protected
    procedure Execute; override;
  public
  end;

  //  五：Overlapped I/O 完成例程实现模型
  TABOverlapIO_CompleteModeThread = class(TABCustomEventThread)
  private
    FBuf: WSABUF;
    FCheckSocket:TSocket;
    Foverlap: WSAOVERLAPPED;
  protected
    procedure Execute; override;
  public
  end;

  //  六：IOCP模型
  //在典型的并发模型中，服务器为每一个客户端创建一个线程，如果很多客户同时请求，则这些线程都是运行的，
  //那么CPU就要一个个切换，CPU花费了更多的时间在线程切换，线程确没得到很多CPU时间。
  //到底应该创建多少个线程比较合适呢，微软件帮助文档上讲应该是2*CPU个

  //完成端口数据结构
  TIOCPModerecord = record
    Overlap: OVERLAPPED;
    BufData: WSABUF;
    Buf: array[0..1024 - 1] of ansiChar;
    OprtType: Integer;
  end;
  PIOCPModerecord = ^TIOCPModerecord;

  TABIOCPModeListenThread = class(TABCustomEventThread)
  private
  protected
    procedure Execute; override;
  public
  end;

  TABIOCPModeWorkThread = class(TThread)
  private
    FCompletPort: THandle;
  protected
    procedure Execute; override;
  public
  end;














//得到本地Socket错误FLastError的描述
function ABGetErrorDesc(Error: integer): string;
//根据TSockAddrIn结构得到其中的IP
function ABSockAddrInToAddress(SockAddrIn: TSockAddrIn): string;
//根据TSockAddrIn结构得到其中的端口
function ABSockAddrInToPort(SockAddrIn: TSockAddrIn): string;
//根据Socket得到其中的IP
function ABSocketToAddress(aSocket: TSocket): string;
//根据Socket得到其中的端口
function ABSocketToPort(aSocket: TSocket): string;

//根据参数生成 TSockAddrIn结构以供其它程序使用
function ABGetSockAddrIn(SocketProtocol:LongInt;
                       Host, Port: string;var SockAddrIn: TSockAddrIn): LongInt;overload;
function ABGetSockAddrIn(SocketProtocol:LongInt;
                       Port: string; var SockAddrIn: TSockAddrIn): LongInt;overload;

//Overlapped I/O 完成例程用户提供的回调函数，发生新的网络事件的时候系统将执行这个函数：
procedure ABWorkerRoutine(const dwError, cbTransferred: DWORD; const lpOverlapped: LPWSAOVERLAPPED; const dwFlags: DWORD); stdcall;

var
  FSocketBeginRecordSize:LongInt;

implementation
const
  FAllEventMask=FD_READ or FD_WRITE or FD_ACCEPT or FD_CONNECT or FD_CLOSE ;
var
  //为提高ABGetSockAddrIn函数速度而使用的缓存
  FABGetSockAddrInValueList:TStrings;



function ABGetErrorDesc(Error: integer): string;
begin
  case Error of
    WSAEINTR: Result := 'Interrupted system call';
    WSAEBADF: Result := 'Bad file number';
    WSAEACCES: Result := 'Permission denied';
    WSAEFAULT: Result := 'Bad address';
    WSAEINVAL: Result := 'Invalid argument';
    WSAEMFILE: Result := 'Too many open files';
    WSAEWOULDBLOCK: Result := 'Operation would block';
    WSAEINPROGRESS: Result := 'Operation now in progress';
    WSAEALREADY: Result := 'Operation already in progress';
    WSAENOTSOCK: Result := 'Socket operation on nonsocket';
    WSAEDESTADDRREQ: Result := 'Destination address required';
    WSAEMSGSIZE: Result := 'Message too long';
    WSAEPROTOTYPE: Result := 'Protocol wrong type for socket';
    WSAENOPROTOOPT: Result := 'Protocol not available';
    WSAEPROTONOSUPPORT: Result := 'Protocol not supported';
    WSAESOCKTNOSUPPORT: Result := 'Socket not supported';
    WSAEOPNOTSUPP: Result := 'Operation not supported on socket';
    WSAEPFNOSUPPORT: Result := 'Protocol family not supported';
    WSAEAFNOSUPPORT: Result := 'Address family not supported';
    WSAEADDRINUSE: Result := 'Address already in use';
    WSAEADDRNOTAVAIL: Result := 'Can''t assign requested address';
    WSAENETDOWN: Result := 'Network is down';
    WSAENETUNREACH: Result := 'Network is unreachable';
    WSAENETRESET: Result := 'Network dropped connection on reset';
    WSAECONNABORTED: Result := 'Software caused connection abort';
    WSAECONNRESET: Result := 'Connection reset by peer';
    WSAENOBUFS: Result := 'No buffer space available';
    WSAEISCONN: Result := 'Socket is already connected';
    WSAENOTCONN: Result := 'Socket is not connected';
    WSAESHUTDOWN: Result := 'Can''t send after socket shutdown';
    WSAETOOMANYREFS: Result := 'Too many references:can''t splice';
    WSAETIMEDOUT: Result := 'Connection timed out';
    WSAECONNREFUSED: Result := 'Connection refused';
    WSAELOOP: Result := 'Too many levels of symbolic links';
    WSAENAMETOOLONG: Result := 'File name is too long';
    WSAEHOSTDOWN: Result := 'Host is down';
    WSAEHOSTUNREACH: Result := 'No route to host';
    WSAENOTEMPTY: Result := 'Directory is not empty';
    WSAEPROCLIM: Result := 'Too many processes';
    WSAEUSERS: Result := 'Too many users';
    WSAEDQUOT: Result := 'Disk quota exceeded';
    WSAESTALE: Result := 'Stale NFS file Handle';
    WSAEREMOTE: Result := 'Too many levels of remote in path';
    WSASYSNOTREADY: Result := 'Network subsystem is unusable';
    WSAVERNOTSUPPORTED: Result := 'Winsock DLL cannot support this application';
    WSANOTINITIALISED: Result := 'Winsock not initialized';
    WSAHOST_NOT_FOUND: Result := 'Host not found';
    WSATRY_AGAIN: Result := 'Non authoritative - host not found';
    WSANO_RECOVERY: Result := 'Non recoverable error';
    WSANO_DATA: Result := 'Valid name, no data record of requested type'
  else
    Result := 'Not a Winsock error';
  end;
end;

function ABSockAddrInToAddress(SockAddrIn: TSockAddrIn): string;
begin
  Result := string(inet_ntoa(SockAddrIn.sin_addr));
end;

function ABSockAddrInToPort(SockAddrIn: TSockAddrIn): string;
begin
  Result := IntToStr(ntohs(SockAddrIn.sin_port));
end;

function ABSocketToAddress(aSocket: TSocket): string;
var
  SockAddrIn: TSockAddrIn;
  Len: integer;
begin
  Len := SizeOf(SockAddrIn);
  if getpeername(aSocket, sockaddr(SockAddrIn), Len) <> SOCKET_ERROR then
    Result := string(inet_ntoa(SockAddrIn.sin_addr));
end;

function ABSocketToPort(aSocket: TSocket): string;
var
  SockAddrIn: TSockAddrIn;
  Len: integer;
begin
  Len := SizeOf(SockAddrIn);
  if getpeername(aSocket, sockaddr(SockAddrIn), Len) <> SOCKET_ERROR then
    Result := IntToStr(ntohs(SockAddrIn.sin_port));
end;

function ABGetSockAddrIn(SocketProtocol:LongInt;
                       Host, Port: string;var SockAddrIn: TSockAddrIn): LongInt;
var
  ProtoEnt: PProtoEnt;
  ServEnt: PServEnt;
  HostEnt: PHostEnt;
  i:LongInt;
  tempSockAddrInP:PSockAddrIn;
begin
  Result := 0;
  i:=FABGetSockAddrInValueList.IndexOf(IntToStr(SocketProtocol)+'-'+Host+'-'+Port);
  if i>=0 then
  begin
    SockAddrIn:=PSockAddrIn(FABGetSockAddrInValueList.Objects[i])^;
  end
  else
  begin
    New(tempSockAddrInP);
    tempSockAddrInP.sin_family := AF_INET;

    ProtoEnt := getprotobynumber(SocketProtocol);
    if ProtoEnt = nil then
    begin
      result:=WSAGetLastError;
      Exit;
    end;

    ServEnt := getservbyname(PansiChar(AnsiString(Port)), ProtoEnt.p_name);
    if ServEnt = nil then
      tempSockAddrInP.sin_port := htons(StrToInt(Port))
    else
      tempSockAddrInP.sin_port := ServEnt.s_port;

    tempSockAddrInP.sin_addr.s_addr := inet_addr(PansiChar(AnsiString(Host)));
    if tempSockAddrInP.sin_addr.s_addr = INADDR_NONE then
    begin
      HostEnt := gethostbyname(PansiChar(AnsiString(Host)));
      if HostEnt = nil then
      begin
        result:=WSAGetLastError;
        Exit;
      end;
      tempSockAddrInP.sin_addr.S_addr := longint(plongint(HostEnt.h_addr_list^)^);
    end;
    SockAddrIn:=tempSockAddrInP^;
    FABGetSockAddrInValueList.AddObject(IntToStr(SocketProtocol)+'-'+Host+'-'+Port,TObject(tempSockAddrInP));
  end;
end;

function ABGetSockAddrIn(SocketProtocol:LongInt;
                       Port: string; var SockAddrIn: TSockAddrIn): LongInt;
var
  ProtoEnt: PProtoEnt;
  ServEnt: PServEnt;
begin
  Result := 0;
  SockAddrIn.sin_family := AF_INET;

  ProtoEnt := getprotobynumber(SocketProtocol);
  if ProtoEnt = nil then
  begin
    result:=WSAGetLastError;
    Exit;
  end;

  ServEnt := getservbyname(PansiChar(AnsiString(Port)), ProtoEnt.p_name);
  if ServEnt = nil then
    SockAddrIn.sin_port := htons(StrToInt(Port))
  else
    SockAddrIn.sin_port := ServEnt.s_port;

  SockAddrIn.sin_addr.s_addr := INADDR_ANY;
end;

(**** TABClientList Class ****)

constructor TABClientList.Create;
begin
  inherited Create;
  FSockets := TList.Create;
end;

destructor TABClientList.Destroy;
begin
  Clear;
  FSockets.Free;
  inherited Destroy;
end;

function TABClientList.GetSockets(Index: integer): TSocket;
begin
  Result := TSocket(FSockets[Index]);
end;

function TABClientList.GetCount: integer;
begin
  Result := FSockets.Count;
end;

function TABClientList.Add(aSocket: TSocket): boolean;
begin
  Result := (FSockets.Add(Ptr(aSocket)) >= 0);
end;

procedure TABClientList.Delete(aSocket: TSocket);
var
  i: integer;
begin
  for i := 0 to FSockets.Count - 1 do
  begin
    if TSocket(FSockets[i]) = aSocket then
    begin
      FSockets.Delete(i);
      Break;
    end;
  end;
end;

procedure TABClientList.Clear;
begin
  FSockets.Clear;
end;

function TABClientList.IndexOf(aSocket: TSocket): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to FSockets.Count - 1 do
  begin
    if TSocket(FSockets[i]) = aSocket then
    begin
      Result := i;
      Break;
    end;
  end;
end;

(**** TCustomWSocket Class ****)

constructor TABSocket.Create(AOwner: TComponent);
var
  WSAData: TWSAData;
begin
  inherited Create(AOwner);
  FACKList:=TStringList.Create;
  TStringList(FACKList).Sorted:=true;
  FClientLists := TABClientList.Create;

  FAllBroadcastFlag:= -1;
  BuffSize := 1024;
  FMaxError:=100;
  FSocket := 0;
  FSocketState := ssClosed;

  if WSAStartup($0202, WSAData) <> 0 then
    SocketError(WSAGetLastError);
end;

destructor TABSocket.Destroy;
begin
  Close;

  if WSACleanUp = SOCKET_ERROR then
    SocketError(WSAGetLastError);

  inherited Destroy;
end;

procedure TABSocket.SetACKList(const Value: TStrings);
begin
  FACKList.Assign(Value);
end;

procedure TABSocket.SetBufSize(const Value: LongInt);
begin
  FBuffSize := Value;
  setlength(FBuff,FBuffSize);
  setlength(FBuffAdd1,FBuffSize+1);
end;

procedure TABSocket.SocketError(aError: integer;aDesc:string);
begin
  if Assigned(FOnError) then
  begin
    if aDesc=EmptyStr then
      aDesc:= ABGetErrorDesc(Error);
    FOnError(Self, Error,aDesc);
  end;
end;

procedure TABSocket.RefreshRCVAndSNDBUF;
var
  tempSize,tempLength:LongInt;
begin
  tempLength:=4;
  if getsockopt(FSocket, SOL_SOCKET, SO_SNDBUF, @tempSize,tempLength)= SOCKET_ERROR then
  begin
    SocketError(WSAGetLastError);
  end
  else
  begin
    if (tempSize<>FBuffSize) and (FBuffSize>8192) then
      setsockopt(FSocket, SOL_SOCKET, SO_SNDBUF,PAnsiChar(@FBuffSize), SizeOf(FBuffSize));
  end;

  tempLength:=4;
  if getsockopt(FSocket, SOL_SOCKET, SO_RCVBUF, @tempSize,tempLength)= SOCKET_ERROR then
  begin
    SocketError(WSAGetLastError);
  end
  else
  begin
    if (tempSize<>FBuffSize) and (FBuffSize>8192) then
      setsockopt(FSocket, SOL_SOCKET, SO_RCVBUF,PAnsiChar(@FBuffSize), SizeOf(FBuffSize));
  end;
end;

procedure TABSocket.DoClose(aSocket: TSocket);
begin
  if (Assigned(FOnClose)) and
     (aSocket=FSocket) then
    FOnClose(Self, aSocket);

  FClientLists.Delete(aSocket);

  if closesocket(aSocket) <> 0 then
    SocketError(WSAGetLastError);
end;

function TABSocket.DoAccept(var aSocket: TSocket):Boolean;
var
  tempLen: integer;
  tempSockAddrIn: TSockAddrIn;
begin
  Result:=True;
  tempLen := SizeOf(tempSockAddrIn);
  aSocket := accept(FSocket, @tempSockAddrIn, @tempLen);
  if aSocket = INVALID_SOCKET then
  begin
    SocketError(WSAGetLastError);
    Result:=False;
  end
  else
  begin
    if not FClientLists.Add(aSocket) then
    begin
      closesocket(aSocket);
    end
    else if Assigned(FOnAccept) then
    begin
      FOnAccept(self, aSocket);
    end
  end;
end;

function TABSocket.GetSocketObjectACK(aSockecObject: TSocketObject): TSocketObject;
begin
  result:=soNull;
  case aSockecObject of
    soText:
    begin
      result:=soTextACK;
    end;
    soFile:
    begin
      result:=soTextACK;
    end;
    soStream:
    begin
      result:=soTextACK;
    end;
    soByteArray:
    begin
      result:=soTextACK;
    end;
  end;
end;

function TABSocket.DoACK(aSocket: TSocket;aSockAddrIn: TSockAddrIn): Boolean;
var
  tempErrorCount,
  tempCurSentResult: longint;
begin
  Result:=false;
  FLastReadACKPackage._Type              :=GetSocketObjectACK(FLastReadPackage._Type);
  FLastReadACKPackage._Number            :=FLastReadPackage._Number            ;
  FLastReadACKPackage._DataLength        :=FSocketBeginRecordSize              ;
  FLastReadACKPackage._SmallPkgNumber    :=FLastReadPackage._SmallPkgNumber    ;
  FLastReadACKPackage._SmallPkgDataLength:=FSocketBeginRecordSize              ;
  FLastReadACKPackage._SmallPkgCount     :=FLastReadPackage._SmallPkgCount     ;
  tempErrorCount := 0;
  Move(FLastReadACKPackage,FBuff[0],FSocketBeginRecordSize);
  repeat
    if FSocketType= stUDP then
    begin
      tempCurSentResult := sendto(aSocket, FBuff[0], FSocketBeginRecordSize, 0, @aSockAddrIn, SizeOf(aSockAddrIn));
    end
    else
    begin
      tempCurSentResult := send(aSocket, FBuff[0], FSocketBeginRecordSize, 0);
    end;

    if (tempCurSentResult = SOCKET_ERROR)  then
    begin
      Sleep(100);
      Inc(tempErrorCount);
      SocketError(WSAGetLastError);
    end
    else
    begin
      Result:=true;
      break;
    end
  until (Result) or (tempErrorCount > FMaxError) or (FSocketState=ssClosed);
end;

function TABSocket.DoSend(aSocket: TSocket;
                          aType  : TSocketObject; aData  : PByte; aSize  : LongInt;
                          aToHost: string ; aToPort: LongInt;
                          aProgressBar:TProgressBar;aUseLessMemory:Boolean): Boolean;
var
  tempOkEnd:boolean;
  tempSendCount,
  tempErrorCount,
  tempAlreadySentDataLength,
  tempCurSentResult:LongInt;
  tempSockAddrIn: TSockAddrIn;
  tempArray:TByteDynArray;
  //取得文件流的缓冲区字节数组
  function GetFileStreamBuff(aPosition,aCount: Integer;aBuff:TByteDynArray): Boolean;
  var
    tempStream:TStream;
  begin
    result:=false;
    if aUseLessMemory then
    begin
      tempStream:=TStream(aData);
      if tempStream is TFileStream then
      begin
        tempStream.Position := aPosition;
        tempStream.Read((@aBuff[0])^,aCount);
        result:=true;
      end;
    end;
  end;

  procedure BROADCASTInit(aToHost: string;aSockAddrIn: TSockAddrIn);
    procedure SetSO_BROADCASTValue(aValue:LongInt);
    var
      tempoptlen: integer;
    begin
      if FAllBroadcastFlag=-1 then
      begin
        tempoptlen := 1;
        if getsockopt(aSocket, SOL_SOCKET, SO_BROADCAST, @FAllBroadcastFlag,tempoptlen)= SOCKET_ERROR then
        begin
          SocketError(WSAGetLastError);
          closesocket(aSocket);
          Exit;
        end;
      end;

      if FAllBroadcastFlag<>aValue then
      begin
        FAllBroadcastFlag:= aValue;
        if setsockopt(aSocket,SOL_SOCKET,SO_BROADCAST,pansichar(@FAllBroadcastFlag),sizeof(FAllBroadcastFlag)) = SOCKET_ERROR then
        begin
          ABShow('无法进行UDP广播');
        end;
      end;
    end;
  begin
    //发送到255.255.255.255的广播应本局域网中监听发送端口的所有机器都会收到,
    //但需做 setsockopt操作且发送的结构中需aSockAddrIn.SIn_Addr.S_addr := INADDR_BROADCAST;
    if AnsiCompareText(aToHost,'255.255.255.255')=0 then
    begin
      SetSO_BROADCASTValue(1);
      aSockAddrIn.SIn_Addr.S_addr := INADDR_BROADCAST;
    end
    else
    begin
      SetSO_BROADCASTValue(0);
      //本类网的广播直接发送(如192.168.0.255)，在本子网中监听发送端口的其它机器都会收到些广播
      //if AnsiCompareText(aToHost,ABPubUser.BroadIP)=0 then
    end;
  end;

  function ACKAfterSendOK:boolean;
  var
    tempACKErrorCount:LongInt;
  begin
    Result:=False;
    //发送成功后在约4秒内等待是否收到了对方的应答，如收到表示发送成功，如没收则重发
    tempACKErrorCount:=0;
    repeat
      //如果没有收到ACK则继续发，直到tempErrorCount > FMaxError
      if (FLastReadACKPackage._Type=GetSocketObjectACK(aType)) and
         ((FSocketType=stUDP) and (FLastReadACKPackage._Number=FLastWritePackage._Number)  and
                                  (FLastReadACKPackage._SmallPkgNumber=FLastWritePackage._SmallPkgNumber) or
          (FSocketType<>stUDP) and (FLastReadACKPackage._Number=FLastWritePackage._Number)
           )  then
      begin
        Result:=True;
        Break;
      end
      else
      begin
        Sleep(1);
        Application.ProcessMessages;
        Inc(tempACKErrorCount);
      end;
    until (tempACKErrorCount > 1000)  or (FSocketState=ssClosed);
  end;

  procedure UpdateAfterSendOK;
  begin
    if FSocketType=stUDP then
    begin
      Inc(tempAlreadySentDataLength, tempCurSentResult-FSocketBeginRecordSize);
    end
    else
    begin
      if FLastWritePackage._smallPkgNumber=1 then
        Inc(tempAlreadySentDataLength, tempCurSentResult-FSocketBeginRecordSize)
      else
        Inc(tempAlreadySentDataLength, tempCurSentResult);
    end;

    if FLastWritePackage._smallPkgNumber<FLastWritePackage._SmallPkgCount then
      FLastWritePackage._smallPkgNumber:=FLastWritePackage._smallPkgNumber+1;

    tempSendCount:=tempSendCount+1;
  end;
begin
  Result:=false;
  if aSize>0 then
  begin
    FACKList.Clear;
    if aUseLessMemory then
      SetLength(tempArray,FBuffSize);

    if FSocketType=stUDP then
    begin
      if ABGetSockAddrIn(IPPROTO_UDP,FToHost, IntToStr(FToPort), tempSockAddrIn)<>0 then
        Exit;
      //UDP时进行广播的初始化
      BROADCASTInit(aToHost,tempSockAddrIn);
    end;

    //设置包头信息(UDP每包包含信息结构，TCP包只有包头有信息结构)
    FLastWritePackage._Type:=aType;
    FLastWritePackage._Number:=FLastWritePackage._Number+1;
    if FSocketType=stUDP then
    begin
      FLastWritePackage._DataLength:=aSize+ABCeil(aSize/(FBuffSize-FSocketBeginRecordSize))*FSocketBeginRecordSize;
    end
    else
    begin
      FLastWritePackage._DataLength:=aSize+FSocketBeginRecordSize;
    end;
    FLastWritePackage._smallPkgCount:=ABCeil(FLastWritePackage._DataLength/FBuffSize);

    repeat
      if (Assigned(aProgressBar)) and (FLastWritePackage._smallPkgCount>1) then
      begin
        aProgressBar.Min:=0;
        aProgressBar.Max:=FLastWritePackage._smallPkgCount;
        aProgressBar.Position:=0;
      end;

      FLastWritePackage._smallPkgNumber:=1;
      tempSendCount:=0;
      tempErrorCount := 0;
      tempAlreadySentDataLength := 0;
      repeat
        //计算每小包发送的长度
        //小包发送，无需分包
        if FLastWritePackage._smallPkgCount=1   then
        begin
          FLastWritePackage._smallPkgDataLength:= FLastWritePackage._DataLength;
        end
        else
        //大包发送，需分FLastWritePackage._smallPkgCount次发送
        begin
          //不是最后一个小包时每个小包发READ_BUFFER_SIZE
          if FLastWritePackage._smallPkgNumber<FLastWritePackage._smallPkgCount then
          begin
            FLastWritePackage._smallPkgDataLength:=FBuffSize;
          end
          //最后一次小包发余下的内容
          else
          begin
            FLastWritePackage._smallPkgDataLength:=FLastWritePackage._DataLength-(FBuffSize*(FLastWritePackage._smallPkgCount-1));
          end;
        end;

        if FSocketType=stUDP then
        begin
          Move(FLastWritePackage,FBuff[0],FSocketBeginRecordSize);

          if GetFileStreamBuff(tempAlreadySentDataLength,FLastWritePackage._smallPkgDataLength-FSocketBeginRecordSize,tempArray) then
            Move(tempArray[0],FBuff[FSocketBeginRecordSize],FLastWritePackage._smallPkgDataLength-FSocketBeginRecordSize)
          else
            Move(aData[tempAlreadySentDataLength],FBuff[FSocketBeginRecordSize],FLastWritePackage._smallPkgDataLength-FSocketBeginRecordSize);
          tempCurSentResult := sendto(aSocket, FBuff[0],FLastWritePackage._smallPkgDataLength, 0, @tempSockAddrIn, SizeOf(tempSockAddrIn));
        end
        else
        begin
          if tempAlreadySentDataLength=0 then
          begin
            Move(FLastWritePackage,FBuff[0],FSocketBeginRecordSize);
            if GetFileStreamBuff(0,FLastWritePackage._smallPkgDataLength-FSocketBeginRecordSize,tempArray) then
              Move(tempArray[0],FBuff[FSocketBeginRecordSize],FLastWritePackage._smallPkgDataLength-FSocketBeginRecordSize)
            else
              Move(aData[0],FBuff[FSocketBeginRecordSize],FLastWritePackage._smallPkgDataLength-FSocketBeginRecordSize);
          end
          else
          begin
            if GetFileStreamBuff(tempAlreadySentDataLength,FLastWritePackage._smallPkgDataLength,tempArray) then
              Move(tempArray[0],FBuff[0],FLastWritePackage._smallPkgDataLength)
            else
              Move(aData[tempAlreadySentDataLength],FBuff[0],FLastWritePackage._smallPkgDataLength);
          end;
          tempCurSentResult := send(aSocket, FBuff[0], FLastWritePackage._smallPkgDataLength, 0);
        end;
        if (Assigned(aProgressBar)) and (FLastWritePackage._smallPkgCount>1) then
        begin
          aProgressBar.Position:=FLastWritePackage._smallPkgNumber;
        end;

        Sleep(1);
        Application.ProcessMessages;

        if (tempCurSentResult = SOCKET_ERROR) or (tempCurSentResult<>FLastWritePackage._smallPkgDataLength)  then
        begin
          Sleep(100);
          Inc(tempErrorCount);
          SocketError(WSAGetLastError);
        end
        else
        begin
          if (FSocketType=stUDP) then
          begin
            if (ACKAfterSendOK) then
            begin
              UpdateAfterSendOK;
            end
            else
            begin
              Sleep(100);
              Inc(tempErrorCount);
            end;
          end
          else
          begin
            UpdateAfterSendOK;
          end;
        end;

        tempOkEnd:= (tempSendCount >= FLastWritePackage._SmallPkgCount);
      until  (tempOkEnd) or (tempErrorCount > FMaxError) or (FSocketState=ssClosed);

      Result:=tempOkEnd;
      if (Result) and
         (FSocketType<>stUDP) then
      begin
        Result:=false;
        if (ACKAfterSendOK) then
        begin
          Result:=true;
        end
        else
        begin
          Sleep(100);
          Inc(tempErrorCount);
        end;
      end;
    until (Result)  or (tempErrorCount > FMaxError) or (FSocketState=ssClosed);

    if (Assigned(aProgressBar)) and (FLastWritePackage._smallPkgCount>1) then
    begin
      aProgressBar.Min:=0;
      aProgressBar.Position:=0;
    end;
  end;
end;

function TABSocket.SendText(aSocket: TSocket; aData: AnsiString; aToHost: string;
  aToPort: Integer; aProgressBar: TProgressBar): Boolean;
begin
  result:=DoSend(aSocket,soText,Pbyte(pansiChar(aData)),Length(aData),aToHost,aToPort,aProgressBar,false);
end;

function TABSocket.SendFile(aSocket: TSocket; aFileName, aToHost: string;
  aToPort: Integer; aProgressBar: TProgressBar;aUseLessMemory:Boolean): Boolean;
var
  tempStream:TStream;
  tempPointer:Pointer;
begin
  if aUseLessMemory then
  begin
    tempStream:=TFileStream.Create(aFileName,fmOpenRead);
    tempPointer:=Pointer(tempStream);
  end
  else
  begin
    tempStream:=TMemoryStream.Create;
    TMemoryStream(tempStream).LoadFromFile(aFileName);
    tempPointer:=Pointer(TMemoryStream(tempStream).Memory);
  end;

  try
    result:=DoSend(aSocket,soFile,tempPointer,tempStream.Size,aToHost,aToPort,aProgressBar,aUseLessMemory);
  finally
    tempStream.Free;
  end;
end;

function TABSocket.SendStream(aSocket: TSocket; aStream: TStream;aToHost: string; aToPort: Integer; aProgressBar: TProgressBar): Boolean;
var
  tempPointer:Pointer;
  tempUseLessMemory:Boolean;
begin
  if aStream is TFileStream then
  begin
    tempPointer:=Pointer(aStream);
    tempUseLessMemory:=true;
  end
  else
  begin
    tempPointer:=Pointer(TMemoryStream(aStream).Memory);
    tempUseLessMemory:=False;
  end;

  result:=DoSend(aSocket,soStream,tempPointer,aStream.Size,aToHost,aToPort,aProgressBar,tempUseLessMemory);
end;

function TABSocket.SendByteArray(aSocket: TSocket; aPByte: PByte;aCount:longint;  aToHost: string; aToPort: Integer; aProgressBar: TProgressBar): Boolean;
begin
  result:=DoSend(aSocket,soByteArray,aPByte,aCount,aToHost,aToPort,aProgressBar,false);
end;

function TABSocket.SendText(aSocket: TSocket;aData    :AnsiString;aProgressBar: TProgressBar): Boolean;
begin
  result:=SendText(aSocket,aData,'',0,aProgressBar);
end;

function TABSocket.SendFile(aSocket: TSocket;aFileName: string   ; aProgressBar: TProgressBar;aUseLessMemory:Boolean): Boolean;
begin
  result:=SendFile(aSocket,aFileName,'',0,aProgressBar,aUseLessMemory);
end;

function TABSocket.SendStream(aSocket: TSocket; aStream: TStream;aProgressBar: TProgressBar): Boolean;
begin
  result:=SendStream(aSocket,aStream,'',0,aProgressBar);
end;

function TABSocket.SendByteArray(aSocket: TSocket; aPByte: PByte;aCount:longint;  aProgressBar: TProgressBar): Boolean;
begin
  result:=SendByteArray(aSocket,aPByte,aCount,'',0,aProgressBar);
end;

function TABSocket.SendText(aData    :AnsiString; aToHost: string ; aToPort: Integer;aProgressBar:TProgressBar): Boolean;
begin
  result:=SendText(FSocket,aData,aToHost,aToPort,aProgressBar);
end;

function TABSocket.SendFile(aFileName: string   ; aToHost: string ; aToPort: Integer;aProgressBar: TProgressBar;aUseLessMemory:Boolean): Boolean;
begin
  result:=SendFile(FSocket,aFileName,aToHost,aToPort,aProgressBar,aUseLessMemory);
end;

function TABSocket.SendStream(aStream: TStream; aToHost: string;aToPort: Integer; aProgressBar: TProgressBar): Boolean;
begin
  result:=SendStream(FSocket,aStream,aToHost,aToPort,aProgressBar);
end;

function TABSocket.SendByteArray(aPByte: PByte;aCount:longint; aToHost: string; aToPort: Integer; aProgressBar: TProgressBar): Boolean;
begin
  result:=SendByteArray(FSocket,aPByte,aCount,aToHost,aToPort,aProgressBar);
end;

function TABSocket.SendText(aData    :AnsiString; aProgressBar:TProgressBar): Boolean;
begin
  result:=SendText(FSocket,aData,'',0,aProgressBar);
end;

function TABSocket.SendFile(aFileName: string   ; aProgressBar: TProgressBar;aUseLessMemory:Boolean): Boolean;
begin
  result:=SendFile(FSocket,aFileName,'',0,aProgressBar,aUseLessMemory);
end;

function TABSocket.SendStream(aStream: TStream;aProgressBar: TProgressBar): Boolean;
begin
  result:=SendStream(FSocket,aStream,'',0,aProgressBar);
end;

function TABSocket.SendByteArray(aPByte: PByte;aCount:longint; aProgressBar: TProgressBar): Boolean;
begin
  result:=SendByteArray(FSocket,aPByte,aCount,'',0,aProgressBar);
end;

function TABSocket.DoRead(aSocket: TSocket;aReadLen:LongInt):LongInt;
var
  tempSockAddrIn: TSockAddrIn;
  tempLen: integer;
  procedure DoOnData(aSocketPackage:TSocketPackage;aBigPkg:Boolean=false);
  var
    tempFileName:string;
    tempFromIP,tempFromPort:string;
    tempBuf:PByte;
    tempFileStream:TFileStream;
  begin
    tempBuf:=nil;
    if FSocketType=stUDP then
    begin
      tempFromIP:=ABSockAddrInToAddress(tempSockAddrIn);
      tempFromPort:=ABSockAddrInToPort(tempSockAddrIn);
    end
    else
    begin
      tempFromIP:=ABSocketToAddress(aSocket);
      tempFromPort:=ABSocketToPort(aSocket);
    end;

    case aSocketPackage._Type of
      soText:
      begin
        if aSocketPackage._SmallPkgCount=1 then
        begin
          //小于一包大小的短信
          if result<fbuffsize then
          begin
            FBuff[result]:=0;
            tempBuf:=@FBuff[FSocketBeginRecordSize];
          end
          //等于一包大小的短信
          else if result=FBuffsize then
          begin
            Move(FBuff[0],FBuffAdd1[0],result);
            FBuffAdd1[result]:=0;
            tempBuf:=@FBuffAdd1[FSocketBeginRecordSize];
          end;
          if Assigned(FOnData) then
            FOnData(Self, aSocket,tempFromIP,tempFromPort,aSocketPackage,tempBuf,'');
        end
        else
        //大于一包大小的短信
        begin
          tempFileStream:=TFileStream.Create(ABAppPath+inttostr(FSocket)+'~FReadBigPkgFileName.tmp',fmOpenRead);
          try
            getmem(tempBuf,tempFileStream.Size+1);
            tempFileStream.Read(tempBuf^,tempFileStream.Size);

            if Assigned(FOnData) then
              FOnData(Self, aSocket,tempFromIP,tempFromPort,aSocketPackage,tempBuf,'');

            freemem(tempBuf);
          finally
            tempFileStream.free;
          end;
        end;
      end;
      //数据流和字节流暂时用处理文件的方式来处理，接收时保存在文件中
      soFile,soStream,soByteArray:
      begin
        //小于等于一包大小的文件
        if aSocketPackage._SmallPkgCount=1 then
        begin
          ABByteArrayWriteFile(TByteDynArray(@FBuff[FSocketBeginRecordSize]),aSocketPackage._DataLength-FSocketBeginRecordSize,ABAppPath+inttostr(FSocket)+'~FReadBigPkgFileName.tmp');
        end
        else
        begin
          //大于一包大小的文件在接收时已存于ABAppPath+inttostr(FSocket)+'~FReadBigPkgFileName.tmp'文件中
        end;

        //将接收临时文件转成接收端保存的文件
        tempFileName:=ABSaveFile();
        if tempFileName<>EmptyStr then
        begin
          ABMoveFile(ABAppPath+inttostr(FSocket)+'~FReadBigPkgFileName.tmp',tempFileName);
        end;

        if Assigned(FOnData) then
          FOnData(Self, aSocket,tempFromIP,tempFromPort,aSocketPackage,nil,tempFileName);
      end;
      //ACK
      soTextACK,soFileACK,soStreamACK,soByteArrayACK:
      begin
        FACKList.Add('From('+tempFromIP+':'+tempFromPort+');'+
                     'Data('+inttostr(aSocketPackage._Number)+');'+
                     'SendCount('+inttostr(aSocketPackage._SmallPkgNumber)+')');
      end;
    end;

  end;
  //对数据包进行检验,判断接收到的包是否合法的包
  function CheckPkg(aSocketPackage:TSocketPackage;var aOutACK:boolean):Boolean;
  begin
    //进行数据包内容的检测，合法的包返回True,否则返回False

    aOutACK:=((aSocketPackage._Type=soTextACK) or
              (aSocketPackage._Type=soFileACK) or
              (aSocketPackage._Type=soStreamACK) or
              (aSocketPackage._Type=soByteArrayACK)
              );
    result:=true;
  end;

  //从缓存取出数据进行处理
  procedure AfterRead(aCount:LongInt);
  var
    tempReadPackage: TSocketPackage;
    tempACK:boolean;
    tempCount: int64;
  begin
    tempCount:=0;
    //如果开始多包接收中
    if Assigned(FReadBigPkgFile) then
    begin
      if FSocketType=stUDP then
      begin
        Move(FBuff[0],tempReadPackage,FSocketBeginRecordSize);
        if (CheckPkg(tempReadPackage,tempACK))  then
        begin
          if (tempACK) and (tempReadPackage._DataLength=FSocketBeginRecordSize) then
          begin
            Move(FBuff[0],FLastReadACKPackage,FSocketBeginRecordSize);
            if FACKOnData then
              DoOnData(FLastReadACKPackage,false);

            exit;
          end
          else
          //如果是UDP方式下大包，每一个小包都要发送ACK
          begin
          {
            if tempReadPackage._SmallPkgNumber=998 then
            begin
              tempCount:=tempCount;
            end;
            }

            Move(FBuff[0],FLastReadPackage,FSocketBeginRecordSize);
            DoACK(aSocket,tempSockAddrIn);

            FReadBigPkgFile.Write((@FBuff[FSocketBeginRecordSize])^,aCount-FSocketBeginRecordSize);
            tempCount:=FReadBigPkgFile.Size +   tempReadPackage._SmallPkgNumber*FSocketBeginRecordSize;
          end;
        end;
      end
      else
      begin
        tempReadPackage:= FLastReadPackage;
        FReadBigPkgFile.Write((@FBuff[0])^,aCount);
        tempCount:=FReadBigPkgFile.size +  FSocketBeginRecordSize;
      end;

      //如果是多包接收的最后一包
      if tempReadPackage._DataLength=tempCount then
      begin
        FReadBigPkgFile.Free;
        FReadBigPkgFile:=nil;
        //如果是TCP方式下大包，在全接收完成后发送ACK
        if FSocketType<>stUDP then
        begin
          DoACK(aSocket,tempSockAddrIn);
        end;
        DoOnData(FLastReadPackage,True);
      end;
    end
    else
    begin
      Move(FBuff[0],tempReadPackage,FSocketBeginRecordSize);
      if (CheckPkg(tempReadPackage,tempACK))  then
      begin
        //取出的包是ACK
        if (tempACK) and (tempReadPackage._DataLength=FSocketBeginRecordSize) then
        begin
          Move(FBuff[0],FLastReadACKPackage,FSocketBeginRecordSize);
          if FACKOnData then
            DoOnData(tempReadPackage,false);
        end
        else
        begin
          Move(FBuff[0],FLastReadPackage,FSocketBeginRecordSize);
          //如果是1包接收
          if tempReadPackage._SmallPkgCount=1 then
          begin
            DoACK(aSocket,tempSockAddrIn);
            DoOnData(tempReadPackage,false);
          end
          //如果是多包接收第一包
          else if not Assigned(FReadBigPkgFile) then
          begin
            if FSocketType=stUDP then
              DoACK(aSocket,tempSockAddrIn);

            FReadBigPkgFile:=TFileStream.Create(ABAppPath+inttostr(FSocket)+'~FReadBigPkgFileName.tmp',fmCreate);
            FReadBigPkgFile.Write((@FBuff[FSocketBeginRecordSize])^,aCount-FSocketBeginRecordSize);
          end
        end;
      end;
    end;
  end;
begin
  result:=aReadLen;
  if result<=0 then
  begin
    if FSocketType=stUDP then
    begin
      tempLen := SizeOf(tempSockAddrIn);
      result := recvfrom(aSocket, FBuff[0], FBuffSize, 0, sockaddr(tempSockAddrIn), tempLen);
    end
    else
    begin
      result := recv(aSocket, FBuff[0], FBuffSize, 0);
    end;

    if result = SOCKET_ERROR then
    begin
      result:=0;
      if WSAGetLastError <> WSAEWOULDBLOCK then
        SocketError(WSAGetLastError);
    end;
  end;

  if result>0 then
    AfterRead(result);
end;

procedure TABSocket.Close;
var
  i: integer;
  tempSocket: TSocket;
  tempmreq:ip_mreq;
  tempStr1,tempStr2:string;
begin
  if (FSocketState = ssClosed) then
    Exit;

  for i := 0 to FClientLists.Count - 1 do
  begin
    tempSocket := FClientLists[i];
    closesocket(tempSocket);
  end;
  FClientLists.Clear;

  if (FSocketType=stUDP) and
     (FGroupBroadcastIP<>emptystr) then
  begin
    tempStr1:=ABStringReplace(FGroupBroadcastIP,',',';');
    for I := 1 to ABGetSpaceStrCount(tempStr1,';') do
    begin
      tempStr2:=ABGetSpaceStr(tempStr1, i, ';');
      tempmreq.imr_multiaddr.S_addr := inet_addr(pansichar(AnsiString(tempStr2)));//htonl(INADDR_ALLHOSTS_GROUP);
      tempmreq.imr_interface.S_addr := htonl(INADDR_ANY);
      if setsockopt(FSocket,IPPROTO_IP,IP_DROP_MEMBERSHIP,pansichar(@tempmreq),sizeof(tempmreq)) = SOCKET_ERROR then
      begin
        SocketError(WSAGetLastError);
      end;
    end;
  end;

  closesocket(FSocket);
  FSocket := 0;

  if Assigned(FReadBigPkgFile) then
  begin
    FReadBigPkgFile.Free;
    FReadBigPkgFile:=nil;
  end;

  case FSocketModel of
    smSelect:
    begin
      TABSelectModelThread(FModelObject).Terminate;
    end;
    smWSAAsyncSelect:
    begin
      TABWSAAsyncModeClass(FModelObject).free;
    end;
    smWSAEventSelect:
    begin
      TABWSAEventModeThread(FModelObject).Terminate;
    end;
    smOverlappedIO_Event:
    begin
      TABOverlapIO_EventModeThread(FModelObject).Terminate;
    end;
    smOverlappedIO_CompletionRoutine:
    begin
      TABOverlapIO_CompleteModeThread(FModelObject).Terminate;
    end;
    smIOCP:
    begin
      TABIOCPModeListenThread(FModelObject).Terminate;
    end;
  end;

  FSocketState := ssClosed;
end;

procedure TABSocket.Open;
var
  i,
  tempSocketProtocol,
  tempSocketType:LongInt;
  tempStr1,tempStr2:string;
  tempmreq:ip_mreq;
  tempSockAddrIn: TSockAddrIn;
begin
  if (FSocketState <> ssClosed) then
    Exit;

  //取得Socket协议和类型
  if FSocketType=stUDP then
  begin
    tempSocketProtocol := IPPROTO_UDP;
    tempSocketType := SOCK_DGRAM;
  end
  else
  begin
    tempSocketProtocol := IPPROTO_TCP;
    tempSocketType := SOCK_STREAM;
  end;

  //取得服务端的Socket地址结构
  if FSocketType<>stTCPClient then
  begin
    if ABGetSockAddrIn(tempSocketProtocol,IntToStr(FLocalPort), FSocketAddrIn)<>0 then
    begin
      SocketError(WSAGetLastError);
      Exit;
    end;
  end;

  //得到socket对象
  FSocket := WinSock2.socket(AF_INET, tempSocketType, tempSocketProtocol);
  if FSocket = INVALID_SOCKET then
  begin
    SocketError(WSAGetLastError);
    Exit;
  end;

  //将socket对象绑定到本地端口
  if (FSocketType<>stTCPClient) then
  begin
    if bind(FSocket, sockaddr(FSocketAddrIn), SizeOf(FSocketAddrIn)) <> 0 then
    begin
      SocketError(WSAGetLastError);
      closesocket(FSocket);
      Exit;
    end;
  end;

  //监听socket对象
  if (FSocketType=stTCPServer) then
  begin
    if listen(FSocket, 5) <> 0 then
    begin
      SocketError(WSAGetLastError);
      closesocket(FSocket);
      Exit;
    end;
  end;

  //设置UDP Socket组播
  if (FSocketType=stUDP) and
     (FGroupBroadcastIP<>emptystr) then
  begin
    tempStr1:=ABStringReplace(FGroupBroadcastIP,',',';');
    for I := 1 to ABGetSpaceStrCount(tempStr1,';') do
    begin
      tempStr2:=ABGetSpaceStr(tempStr1, i, ';');

      tempmreq.imr_multiaddr.S_addr := inet_addr(pansichar(AnsiString(tempStr2)));//htonl(INADDR_ALLHOSTS_GROUP);
      tempmreq.imr_interface.S_addr := htonl(INADDR_ANY);
      if setsockopt(FSocket,IPPROTO_IP,IP_ADD_MEMBERSHIP,pansichar(@tempmreq),sizeof(tempmreq)) = SOCKET_ERROR then
      begin
        SocketError(WSAGetLastError);
        closesocket(FSocket);
        Exit;
      end;
    end;
  end;

  //TCP 客户端连接
  if (FSocketType=stTCPClient) then
  begin
    if ABGetSockAddrIn(tempSocketProtocol,FToHost, inttostr(FTOPort), tempSockAddrIn)<>0 then
      Exit;

    i:=connect(FSocket, sockaddr(tempSockAddrIn), SizeOf(tempSockAddrIn));
    if  i<> 0 then
    begin
      i:=WSAGetLastError;
      if  i<> WSAEWOULDBLOCK then
      begin
        SocketError(WSAGetLastError);
        closesocket(FSocket);
        Exit;
      end;
    end;
  end;

  //设置 socket模型
  case FSocketModel of
    smSelect:
    begin
      FModelObject := TABSelectModelThread.Create(self);
    end;
    smWSAAsyncSelect:
    begin
      FModelObject:=TABWSAAsyncModeClass.Create(self);
    end;
    smWSAEventSelect:
    begin
      FModelObject := TABWSAEventModeThread.Create(self);
    end;
    smOverlappedIO_Event:
    begin
      FModelObject := TABOverlapIO_EventModeThread.Create(self);
    end;
    smOverlappedIO_CompletionRoutine:
    begin
      FModelObject := TABOverlapIO_CompleteModeThread.Create(self);
    end;
    smIOCP:
    begin
      FModelObject := TABIOCPModeListenThread.Create(self);
    end;
  end;

  RefreshRCVAndSNDBUF;

  FLastReadPackage._Number:=0;
  FLastWritePackage._Number:=0;
  FLastReadACKPackage._Number:=0;
  FLastWriteACKPackage._Number:=0;

  FSocketState:=ssOpen;
end;

{ TABCustomThread }

constructor TABCustomThread.Create(aSocket: TABSocket);
begin
  inherited Create(true);
  FSocket:=aSocket;
  FreeOnTerminate:=true;
  Resume;
end;

procedure TABCustomThread.Execute;
begin
  inherited;

end;

{ TABSelectModelThread }

procedure TABSelectModelThread.Execute;
var
  timeout: TTimeVal;
  tempInitSocketSet,tempCheckSocketSet: TFDSet;
  tempSelectReturn:LongInt;
  tempNewSocket: TSocket;
  i: Integer;
begin
  inherited;
  timeout.tv_sec := 0;      //秒
  timeout.tv_usec := TIME_OUT;   //微秒
  //清空套接字集合
  FD_ZERO(tempInitSocketSet);
  //将套接字加入到集合fdread中
  _FD_SET(FSocket.FSocket, tempInitSocketSet);

  while (not Terminated) do
  begin
    tempCheckSocketSet:=tempInitSocketSet;
    tempSelectReturn := select(0,@tempCheckSocketSet,nil,nil,@timeout);
    if (tempSelectReturn > 0) and
       (FSocket.FSocket>0)   then
    begin
      case FSocket.FSocketType of
        stUDP:
        begin
          //有数据读取时
          if (FD_ISSET(FSocket.FSocket,&tempCheckSocketSet)) then
          begin
            FSocket.DoRead(FSocket.FSocket);
          end;
        end;
        stTCPClient:
        begin
          //有数据读取时
          if (FD_ISSET(FSocket.FSocket,&tempCheckSocketSet)) then
          begin
            //返回值为0表示是服务器的关闭触发
            if FSocket.DoRead(FSocket.FSocket)=0 then
            begin
              FD_CLR(FSocket.FSocket,&tempInitSocketSet);
              FSocket.DoClose(FSocket.FSocket);
            end;
          end;
        end;
        stTCPServer:
        begin
          for I := 0 to tempCheckSocketSet.fd_count-1 do
          begin
            if (FD_ISSET(tempCheckSocketSet.fd_array[i],&tempCheckSocketSet)) then
            begin
              //stTCPServer收到新的连接
              if (tempCheckSocketSet.fd_array[i] = FSocket.FSocket) then
              begin
                if(tempCheckSocketSet.fd_count < FD_SETSIZE)then
                begin
                  if FSocket.DoAccept(tempNewSocket) then
                    _FD_SET(tempNewSocket,&tempInitSocketSet);
                end;
              end
              else
              //stTCPServer收到新的数据
              begin
                //返回值为0表示是客户端的关闭触发
                if FSocket.DoRead(tempCheckSocketSet.fd_array[i])=0 then
                begin
                  FD_CLR(tempCheckSocketSet.fd_array[i],&tempInitSocketSet);
                  FSocket.DoClose(tempCheckSocketSet.fd_array[i]);
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

{ TABWSAAsyncModeClass }

constructor TABWSAAsyncModeClass.Create(aSocket:TABSocket);
begin
  inherited Create(nil);
  FHandle := 0;
  FSocket:= aSocket;
  Open;
end;

procedure TABWSAAsyncModeClass.WndProc(var AMsg: TMessage);
var
  Error,
  tempEvent: word;
  tempEventDesc:string;
begin
  case AMsg.Msg of
    WM_ASYNCSELECT:
    begin
      if (FSocket.FSocketState = ssClosed) then
        Exit;

      Error := WSAGetSelectError(AMsg.LParam);
      if Error<>0 then
      begin
        FSocket.SocketError(Error);
        Exit;
      end;

      tempEvent:=  WSAGetSelectEvent(AMsg.LParam) ;

      if Assigned(FSocket.FOnEvent) then
      begin
        case tempEvent of
          FD_READ                    : tempEventDesc:=Trim('FD_READ                    ');
          FD_WRITE                   : tempEventDesc:=Trim('FD_WRITE                   ');
          FD_ACCEPT                  : tempEventDesc:=Trim('FD_ACCEPT                  ');
          FD_CONNECT                 : tempEventDesc:=Trim('FD_CONNECT                 ');
          FD_CLOSE                   : tempEventDesc:=Trim('FD_CLOSE                   ');
          FD_OOB                     : tempEventDesc:=Trim('FD_OOB                     ');
          FD_QOS                     : tempEventDesc:=Trim('FD_QOS                     ');
          FD_GROUP_QOS               : tempEventDesc:=Trim('FD_GROUP_QOS               ');
          FD_ROUTING_INTERFACE_CHANGE: tempEventDesc:=Trim('FD_ROUTING_INTERFACE_CHANGE');
          FD_ADDRESS_LIST_CHANGE     : tempEventDesc:=Trim('FD_ADDRESS_LIST_CHANGE     ');
          else                         tempEventDesc:=Trim('Other                      ');
        end;
        FSocket.FOnEvent(Self,AMsg.WParam, tempEvent,tempEventDesc);
      end;

      case tempEvent of
        FD_READ: FSocket.DoRead(AMsg.WParam);
        FD_WRITE: DoWrite(AMsg.WParam);
        FD_ACCEPT: DoAccept(AMsg.WParam);
        FD_CONNECT: DoConnect(AMsg.WParam);
        FD_CLOSE: FSocket.DoClose(AMsg.WParam);
        //FD_OOB,FD_QOS,FD_GROUP_QOS,FD_ROUTING_INTERFACE_CHANGE,FD_ADDRESS_LIST_CHANGE:;
      end;
    end;
  else
    AMsg.Result := DefWindowProc(FHandle, AMsg.Msg, AMsg.WParam, AMsg.LParam);
  end;
end;

procedure TABWSAAsyncModeClass.DoWrite(aSocket: TSocket);
begin

end;

procedure TABWSAAsyncModeClass.DoConnect(aSocket: TSocket);
begin
  if WSAASyncSelect(aSocket, FHandle, WM_ASYNCSELECT,FAllEventMask) <> 0 then
    FSocket.SocketError(WSAGetLastError)
  else
  begin
    if Assigned(FSocket.FOnConnect) then
      FSocket.FOnConnect(FSocket, aSocket);
  end;
end;

function TABWSAAsyncModeClass.DoAccept(aSocket: TSocket):Boolean;
var
  tempNewSocket: TSocket;
begin
  Result:=True;
  if FSocket.DoAccept(tempNewSocket) then
  begin
    if WSAASyncSelect(tempNewSocket, FHandle, WM_ASYNCSELECT, FAllEventMask) <> 0 then
    begin
      FSocket.SocketError(WSAGetLastError);
      closesocket(tempNewSocket);
      Result:=False;
    end;
  end;
end;

destructor TABWSAAsyncModeClass.Destroy;
begin
  close;

  inherited;
end;

procedure TABWSAAsyncModeClass.close;
begin
  if FHandle>0 then
  begin
    DeallocateHWnd(FHandle);
    FHandle:=0;
  end;
end;

function TABWSAAsyncModeClass.Open:boolean;
begin
  Result:=True;
  close;
  FHandle := AllocateHWnd(WndProc);

  if WSAASyncSelect(FSocket.FSocket, FHandle, WM_ASYNCSELECT,FAllEventMask ) <> 0 then
  begin
    FSocket.SocketError(WSAGetLastError);
    closesocket(FSocket.FSocket);
    Result:=false;
  end;
end;

{ TABCustomEventThread }

function TABCustomEventThread.CreateWSAEventSelect(aSocket: TSocket;aEventMask:LongInt): WSAEvent;
begin
  Result:=0;
  if FEventTotal < WSA_MAXIMUM_WAIT_EVENTS  then
  begin
    //创建一个事件对象。
    Result := WSACreateEvent();
    if Result = WSA_INVALID_EVENT then
    begin
    end
    else
    begin
      //将事件对象与套接口的网络事件集合相关
      if WSAEventSelect(aSocket, Result,aEventMask) = SOCKET_ERROR then
      begin
        WSACloseEvent(Result);
        Result:=0;
      end
      else
      begin
        FSockArray[FEventTotal]:=aSocket;
        FEventArray[FEventTotal]:=Result;
        Inc(FEventTotal);
      end;
    end;
  end;
end;

{ TABWSAEventModeThread }

procedure TABWSAEventModeThread.Execute;
var
  i,j: integer;
  tempCurNetworkEvent: TWSANetworkEvents;
  tempNewSocket: TSocket;
begin
  FEventTotal := 0;
  if CreateWSAEventSelect(FSocket.FSocket,FAllEventMask)=0 then
    Exit;

  while (not Terminated) do
  begin
    i := WSAWaitForMultipleEvents(FEventTotal, @FEventArray[0], FALSE, TIME_OUT, FALSE);
    if i = WSA_WAIT_TIMEOUT then
    begin
      Continue;
    end;
    if i = WSA_WAIT_FAILED then
    begin
      Exit;
    end;

    i:=i-WSA_WAIT_EVENT_0;
    FillChar(tempCurNetworkEvent, sizeof(tempCurNetworkEvent), 0);
    //检测所指定的套接字上发生了哪种事件
    WSAEnumNetworkEvents(FSockArray[i], FEventArray[i], tempCurNetworkEvent);
    //TCP连接事件
    if (tempCurNetworkEvent.lNetworkEvents and FD_ACCEPT) > 0 then
    begin
      if tempCurNetworkEvent.iErrorCode[FD_ACCEPT_BIT] <> 0 then
        continue;

      if FEventTotal < WSA_MAXIMUM_WAIT_EVENTS  then
      begin
        if FSocket.DoAccept(tempNewSocket) then
        begin
          if CreateWSAEventSelect(tempNewSocket,FAllEventMask)=0 then
          begin
            closesocket(tempNewSocket);
          end;
        end;
      end;
    end;

    //TCP关闭事件
    if (tempCurNetworkEvent.lNetworkEvents and FD_CLOSE) > 0 then
    begin
      if tempCurNetworkEvent.iErrorCode[FD_CLOSE_BIT] <> 0 then
        continue;

      case FSocket.FSocketType of
        stTCPClient:
        begin
          //服务器关闭引发
          FSocket.DoClose(FSockArray[i]);
          WSACloseEvent(FEventArray[i]);
          Dec(FEventTotal);
          break;
        end;
        stTCPServer:
        begin
          //客户端关闭引发
          FSocket.DoClose(FSockArray[i]);
          WSACloseEvent(FEventArray[i]);
          if i <> FEventTotal - 1 then
          begin
            for j := i to FEventTotal - 2 do
            begin
              FSockArray[j] := FSockArray[j + 1];
              FEventArray[j] := FEventArray[j + 1];
            end;
          end;
          Dec(FEventTotal);
        end;
      end;
    end;

    //读取数据事件
    if (tempCurNetworkEvent.lNetworkEvents and FD_READ) > 0 then
    begin
      if tempCurNetworkEvent.iErrorCode[FD_READ_BIT] <> 0 then
        continue;

      FSocket.DoRead(FSockArray[i]);
    end;
  end;

  for i :=  FEventTotal - 1 downto 0 do
  begin
    closesocket(FSockArray[i]);
    WSACloseEvent(FEventArray[i]);
  end;
end;

{ TABOverlapIO_EventModeThread }

procedure TABOverlapIO_EventModeThread.Execute;
var
  tempOverlappedResult: DWORD;
  tempEventTotal: DWORD;
  tempCurIndex: DWORD;
  tempCurNetworkEvent: TWSANetworkEvents;

  tempSockArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of TSocket;
  tempEventArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of WSAEVENT;
  temppOverlapsArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of PWSAOVERLAPPED;
  tempBufsArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of PWSABUF;
  temppwRecvdArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of DWORD;
  temppwFlagsArray: array[0..WSA_MAXIMUM_WAIT_EVENTS - 1] of DWORD;

  tempEvent: WSAEvent;
  i,tempMaxCount: integer;
  tempNewSocket: TSocket;
  function CreateWSAEventSelect(aSocket: TSocket):Boolean;
  begin
    Result:=false;
    if tempEventTotal < WSA_MAXIMUM_WAIT_EVENTS  then
    begin
      //创建一个事件对象。
      tempEvent := WSACreateEvent();
      if tempEvent = WSA_INVALID_EVENT then
      begin
      end
      else
      begin
        //将事件对象与套接口的网络事件集合相关
        if WSAEventSelect(aSocket, tempEvent,FD_READ or FD_WRITE or FD_ACCEPT) = SOCKET_ERROR then
        begin
        end
        else
        begin
          tempSockArray[tempEventTotal] :=aSocket;
          tempEventArray[tempEventTotal] := tempEvent;

          FillChar(temppOverlapsArray[tempEventTotal]^, sizeof(WSAOVERLAPPED), 0);
          temppOverlapsArray[tempEventTotal].hEvent := tempEventArray[tempEventTotal];
          WSARecv(tempSockArray[tempEventTotal], tempBufsArray[tempEventTotal], 1, temppwRecvdArray[tempEventTotal],
            temppwFlagsArray[tempEventTotal], temppOverlapsArray[tempEventTotal], nil);

          Inc(tempEventTotal);
          Result:=True;
        end;
      end;
    end;
  end;
begin
  if FSocket.FSocketType =stTCPServer then
  begin
    tempMaxCount:=WSA_MAXIMUM_WAIT_EVENTS;
  end
  else
  begin
    tempMaxCount:=1;
  end;

  tempEventTotal := 0;
  for i := 0 to tempMaxCount - 1 do
  begin
    temppwRecvdArray[i] := 0;
    temppwFlagsArray[i] := 0;
    New(temppOverlapsArray[i]);
    New(tempBufsArray[i]);

    tempBufsArray[i].len := FSocket.BuffSize;
    tempBufsArray[i].buf := AllocMem(FSocket.BuffSize);
  end;

  if not CreateWSAEventSelect(FSocket.FSocket) then
    Exit;

  while (not Terminated) do
  begin
    //如果事件数组中有某一个事件被传信了，函数会返回这个事件的索引值，
    //但是这个索引值需要减去预定义值 WSA_WAIT_EVENT_0才是这个事件在事件数组中的位置
    tempCurIndex := WSAWaitForMultipleEvents(tempEventTotal, @tempEventArray[0], FALSE, WSA_INFINITE, FALSE);
    if tempCurIndex = WSA_WAIT_FAILED then
    begin
      Exit;
    end;

    tempOverlappedResult:=0;
    tempCurIndex:=tempCurIndex-WSA_WAIT_EVENT_0;
    WSAResetEvent(tempEventArray[tempCurIndex]);
    WSAGetOverlappedResult(tempSockArray[tempCurIndex],
      temppOverlapsArray[tempCurIndex], tempOverlappedResult, FALSE,
      temppwFlagsArray[tempCurIndex]);

    case FSocket.FSocketType of
      stUDP:
      begin
        if tempOverlappedResult <> 0 then //连接已经关闭
        begin
     //tempBufsArray[tempCurIndex]^.buf
      //    FSocket.DoRead(tempSockArray[tempCurIndex]);
        end;
      end;
      stTCPServer:
      begin
        //TCP连接事件
        FillChar(tempCurNetworkEvent, sizeof(tempCurNetworkEvent), 0);
        //检测所指定的套接口上网络事件的发生
        WSAEnumNetworkEvents(tempSockArray[tempCurIndex], tempEventArray[tempCurIndex], tempCurNetworkEvent);

        //TCP连接事件
        if (tempCurNetworkEvent.lNetworkEvents and FD_ACCEPT) > 0 then
        begin
          if tempCurNetworkEvent.iErrorCode[FD_ACCEPT_BIT] <> 0 then
            continue;
          if tempEventTotal < WSA_MAXIMUM_WAIT_EVENTS  then
          begin
            if FSocket.DoAccept(tempNewSocket) then
            begin
              if not CreateWSAEventSelect(tempNewSocket) then
              begin
                closesocket(tempNewSocket);
              end;
            end;
          end;
        end;
      end;
      stTCPClient:
      begin
      {
        if tempOverlappedResult = 0 then //连接已经关闭
        begin
          closesocket(tempSockArray[tempCurIndex] );
          WSACloseEvent(tempEventArray[tempCurIndex]);
        end
        else
        begin
          FSocket.DoRead(tempSockArray[tempCurIndex]);
        end;
        }
      end;
    end;
  end;

  for i :=  tempEventTotal - 1 downto 0 do
  begin
    closesocket(tempSockArray[i]);
    WSACloseEvent(tempEventArray[i]);

    FreeMem(tempBufsArray[i].buf);
    Dispose(temppOverlapsArray[i]);
    Dispose(tempBufsArray[i]);
  end;

  tempEventTotal:=0;
end;

{ TABOverlapIO_CompleteModeThread }

procedure TABOverlapIO_CompleteModeThread.Execute;
var
  i: integer;
  tempCurNetworkEvent: TWSANetworkEvents;
  tempNewSocket: TSocket;
  procedure DoWSARecv(aSocket: TSocket);
  var
    dwTemp, dwFlag: DWORD;
  begin
    FCheckSocket:=aSocket;
    FBuf.len := FSocket.BuffSize;
    FBuf.buf := AllocMem(FSocket.BuffSize);
    dwFlag := 0;
    FillChar(Foverlap, sizeof(WSAOVERLAPPED), 0);
    Foverlap.hEvent := DWORD(self); 
    WSARecv(aSocket, @FBuf, 1, dwTemp, dwFlag, @Foverlap, @ABWorkerRoutine);
  end;
begin
  FEventTotal := 0;
  if FSocket.FSocketType in [stTCPServer] then
  begin
    if CreateWSAEventSelect(FSocket.FSocket,FAllEventMask)=0 then
      Exit;
  end
  else if FSocket.FSocketType in [stUDP,stTCPClient] then
  begin
    DoWSARecv(FSocket.FSocket);
  end;

  while (not Terminated) do
  begin
    //无此函数则进入不到WorkerRoutine中
    SleepEx(TIME_OUT, True);

    i := WSAWaitForMultipleEvents(FEventTotal, @FEventArray[0], FALSE, TIME_OUT, FALSE);
    if i = WSA_WAIT_TIMEOUT then
    begin
      Continue;
    end;
    if i = WSA_WAIT_FAILED then
    begin
      Exit;
    end;

    i:=i-WSA_WAIT_EVENT_0;
    FillChar(tempCurNetworkEvent, sizeof(tempCurNetworkEvent), 0);
    WSAEnumNetworkEvents(FSockArray[i], FEventArray[i], tempCurNetworkEvent);
    //TCP连接事件
    if (tempCurNetworkEvent.lNetworkEvents and FD_ACCEPT) > 0 then
    begin
      if tempCurNetworkEvent.iErrorCode[FD_ACCEPT_BIT] <> 0 then
        continue;

      if FEventTotal < WSA_MAXIMUM_WAIT_EVENTS  then
      begin
        if FSocket.DoAccept(tempNewSocket) then
        begin
          DoWSARecv(tempNewSocket);
        end;
      end;
    end;
  end;

  for i :=  FEventTotal - 1 downto 0 do
  begin
    closesocket(FSockArray[i]);
    WSACloseEvent(FEventArray[i]);
  end;
end;

//Overlapped I/O 完成例程要求用户提供一个回调函数，发生新的网络事件的时候系统将执行这个函数：
procedure ABWorkerRoutine(const dwError, cbTransferred: DWORD; const
  lpOverlapped: LPWSAOVERLAPPED; const dwFlags: DWORD);
var
  dwTemp, Flags: DWORD;
begin
  if (dwError <> 0) or (cbTransferred = 0) then
  begin
    closesocket(TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent).FCheckSocket);
    Exit;
  end;

  TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent).FSocket.Buff:=
    TByteDynArray((TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent)).FBuf.buf);

  TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent).FSocket.DoRead(
    TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent).FSocket.FSocket,
    lpOverlapped.InternalHigh
    );

  Flags := 0;
  if WSARecv(TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent).FCheckSocket,
    @(TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent)).FBuf, 1, dwTemp, Flags,
    @(TABOverlapIO_CompleteModeThread(lpOverlapped.hEvent)).Foverlap,
    @ABWorkerRoutine) = SOCKET_ERROR then
  begin
  end;
end;

{ TABIOCPModeListenThread }

procedure TABIOCPModeListenThread.Execute;
var
  tempAcceptSocket: TSocket;
  tempCompletPort: THandle;
  tempIOCPModerecord: PIOCPModerecord;
  tempSysInfo: SYSTEM_INFO;
  tempThread: TABIOCPModeWorkThread;
  tempaddr: TSockAddrIn;
  i, len: Integer;
  tempBytesRecv,
  tempFlags: DWORD;
begin
  //创建IOCP
  tempCompletPort := CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, 0, 0);
  if tempCompletPort = 0 then
  begin
    MessageBox(0, 'CreateIoCompletionPort failed.', 'Error', MB_OK);
    Exit;
  end;

  //根据CPU的数量创建CPU*2数量的工作者线程。
  GetSystemInfo(tempSysInfo);
  for I:=0 to tempSysInfo.dwNumberOfProcessors*2  -1 do
  begin
    tempThread := TABIOCPModeWorkThread.Create(True);
    tempThread.FCompletPort := tempCompletPort;
    tempThread.FreeOnTerminate := True;
    tempThread.Resume;
  end;

  while (not self.Terminated) do
  begin
    len := sizeof(tempaddr);
    tempAcceptSocket := accept(FSocket.FSocket, @tempaddr, @len);
    if tempAcceptSocket = INVALID_SOCKET then
    begin
      sleepex(TIME_OUT, false);
      continue;
    end;

    //把一个套接字句柄和一个完成端口绑定到一起。完成端口又同一个或多个设备相关联着，
    //所以以套接字为基础，投递发送和请求，对I/O处理。接着，可以依赖完成端口，接收有关I/O操作完成情况的通知
    CreateIoCompletionPort(tempAcceptSocket, tempCompletPort, tempAcceptSocket, 0);

    New(tempIOCPModerecord);
    FillChar(tempIOCPModerecord^.Overlap, sizeof(OVERLAPPED), 0);
    FillChar(tempIOCPModerecord^.Buf[0], FSocket.FBuffSize, 0);
    tempIOCPModerecord^.BufData.len := FSocket.FBuffSize;
    tempIOCPModerecord^.BufData.buf := tempIOCPModerecord^.Buf;
    tempIOCPModerecord.OprtType := RECV_POSTED;

    tempFlags := 0;
    WSARecv(tempAcceptSocket, @(tempIOCPModerecord.BufData), 1, tempBytesRecv, tempFlags,@(tempIOCPModerecord.Overlap), nil);
  end;

  PostQueuedCompletionStatus(tempCompletPort, 0, 0, nil);
  CloseHandle(tempCompletPort);
end;

{ TABIOCPModeWorkThread }

procedure TABIOCPModeWorkThread.Execute;
var
  {$IF (CompilerVersion>=26.0)}
  tempCompletKey:NativeUInt;
  {$ELSE}
  tempCompletKey:Cardinal;
  {$IFEND}
  tempBytesTransd,
  tempBytesRecv,
  tempFlags: DWORD;
  tempIOCPModerecord: PIOCPModerecord;
begin
  while (not self.Terminated) do
  begin
    tempBytesTransd := 0;
    tempCompletKey := 0;
    GetQueuedCompletionStatus(FCompletPort, tempBytesTransd, tempCompletKey,POVERLAPPED(tempIOCPModerecord), TIME_OUT);

    if (tempBytesTransd = 0) and
      ((tempIOCPModerecord = nil) or
      (tempIOCPModerecord.OprtType = RECV_POSTED) or
      (tempIOCPModerecord.OprtType = SEND_POSTED)) then
    begin
      closesocket(tempCompletKey);
      Dispose(tempIOCPModerecord);
      continue;
    end;

    if tempIOCPModerecord.OprtType = RECV_POSTED then
    begin
    {
      ABEnterCriticalSection;
      fmmain.Memo1.Lines.Add(tempIOCPModerecord^.BufData.buf);
      ABLeaveCriticalSection;
      }
    end;

    tempFlags := 0;
    FillChar(tempIOCPModerecord.Overlap, sizeof(OVERLAPPED), 0);
    FillChar(tempIOCPModerecord.Buf[0], 4096, 0);
    tempIOCPModerecord.BufData.len := 4096;
    tempIOCPModerecord.BufData.buf := tempIOCPModerecord.Buf;
    tempIOCPModerecord.OprtType := RECV_POSTED;

    WSARecv(tempCompletKey, @(tempIOCPModerecord^.BufData), 1, tempBytesRecv, tempFlags,@(tempIOCPModerecord.Overlap), nil);
  end;
end;

procedure ABFinalization;
var
  i:LongInt;
begin
  for I := 0 to FABGetSockAddrInValueList.Count-1 do
  begin
    Dispose(PSockAddrIn(FABGetSockAddrInValueList.Objects[i]))
  end;
  FABGetSockAddrInValueList.Clear;
  FABGetSockAddrInValueList.Free;
end;

procedure ABInitialization;
begin
  FSocketBeginRecordSize:=SizeOf(TSocketPackage);
  FABGetSockAddrInValueList:=TStringList.Create;
  TStringList(FABGetSockAddrInValueList).Sorted:=true;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.

