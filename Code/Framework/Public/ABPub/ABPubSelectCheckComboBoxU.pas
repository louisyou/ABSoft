{
CheckedComboBox选择窗体单元
}
unit ABPubSelectCheckComboBoxU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubCheckedComboBoxU,
  ABPubFuncU,
  ABPubDBU,

  SysUtils,Classes,Controls,Forms,ExtCtrls,DB,StdCtrls, ABPubFormU;

type
  TABSelectCheckComboBoxForm = class(TABPubForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    ABCheckedComboBox1: TABCheckedComboBox;
    Label1: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//选择数据集中的字段值，只支持单字段
//aViewField=显示字段
//aDownField=下拉字段
function ABSelectCheckComboBox(
                           var aValue:string;
                           aDataSet:TDataSet;
                           aDownFields,
                           aViewFields,
                           aSaveFields:string;
                           aCaption:string='';
                           aLabelCaption:string=''):boolean;overload;

//选择数据集中的字段名称
function ABSelectCheckComboBox(
                           var aValue:string;
                           aDataSet:TDataSet;
                           aCaption:string='';
                           aLabelCaption:string=''):boolean;overload;

implementation
{$R *.dfm}

function ABSelectCheckComboBox(
                           var aValue:string;
                           aDataSet:TDataSet;

                           aDownFields,
                           aViewFields,
                           aSaveFields:string;
                           aCaption:string;
                           aLabelCaption:string):boolean;
var
  tempForm:TABSelectCheckComboBoxForm;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  i, j: LongInt;
  tempStrings:TStrings;
begin
  Result:=false;

  tempForm := TABSelectCheckComboBoxForm.Create(nil);
  tempForm.ABCheckedComboBox1.Items.Clear;
  tempForm.ABCheckedComboBox1.ViewItems.Clear;

  if aCaption<>EmptyStr then
    tempForm.Caption:= aCaption;
  if aLabelCaption<>EmptyStr then
    tempForm.Label1.Caption:= aLabelCaption;

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataset.DisableControls;
  tempStrings:=TStringList.Create;
  try
    aDataSet.First;
    while not aDataSet.Eof do
    begin
      tempForm.ABCheckedComboBox1.Items.Add(ABGetFieldValue(aDataSet,aDownFields,[]));
      tempForm.ABCheckedComboBox1.ViewItems.Add(ABGetFieldValue(aDataSet,aViewFields,[]));

      tempStrings.Add(ABGetFieldValue(aDataSet,aSaveFields,[]));
      aDataSet.Next;
    end;

    for I := 1 to ABGetSpaceStrCount(aValue, ',') do
    begin
      for j := 0 to tempStrings.Count-1 do
      begin
        if AnsiCompareText(tempStrings[j],ABGetSpaceStr(aValue,i, ','))=0 then
        begin
          tempForm.ABCheckedComboBox1.SetCheck(j,true);
          break;
        end;
      end;
    end;

    if (tempForm.ShowModal=mrok)  then
    begin
      Result:=True;
      aValue:=EmptyStr;
      for I := 0 to tempStrings.Count-1 do
      begin
        if tempForm.ABCheckedComboBox1.IsChecked(i) then
        begin
          ABAddstr(aValue,tempStrings[i]);
        end;
      end;
    end;
  finally
    tempStrings.Free;
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.EnableControls;
    tempForm.Free;
  end;
end;

function ABSelectCheckComboBox(var aValue:string;
                               aDataSet:TDataSet;
                               aCaption:string;
                               aLabelCaption:string):boolean;
var
  tempForm:TABSelectCheckComboBoxForm;
  i, j: LongInt;
  tempStr1:string;
begin
  Result:=false;
  tempForm := TABSelectCheckComboBoxForm.Create(nil);
  tempForm.ABCheckedComboBox1.Items.Clear;
  tempForm.ABCheckedComboBox1.ViewItems.Clear;

  if aCaption<>EmptyStr then
    tempForm.Caption:= aCaption;
  if aLabelCaption<>EmptyStr then
    tempForm.Label1.Caption:= aLabelCaption;
  try
    for I := 0 to aDataSet.FieldCount - 1 do
    begin
      tempForm.ABCheckedComboBox1.Items.Add(aDataSet.Fields[i].DisplayLabel);
      tempForm.ABCheckedComboBox1.ViewItems.Add(aDataSet.Fields[i].FieldName);
    end;

    for I := 1 to ABGetSpaceStrCount(aValue, ',') do
    begin
      tempStr1:=ABGetSpaceStr(aValue,i, ',');
      for j := 0 to tempForm.ABCheckedComboBox1.ViewItems.Count - 1 do
      begin
        if AnsiCompareText(tempForm.ABCheckedComboBox1.ViewItems[j],tempStr1)=0 then
        begin
          tempForm.ABCheckedComboBox1.SetCheck(j,true);
          break;
        end;
      end;
    end;

    if (tempForm.ShowModal=mrok) then
    begin
      Result:=True;
      aValue:=tempForm.ABCheckedComboBox1.text;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABSelectCheckComboBoxForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrNo;
end;

procedure TABSelectCheckComboBoxForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;


end.
