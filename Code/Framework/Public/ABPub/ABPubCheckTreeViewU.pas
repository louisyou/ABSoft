{
CheckTreeView组件单元
}
unit ABPubCheckTreeViewU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubFuncU,
  ABPubMessageU,
  ABPubConstU,
  ABPubDBU,

  Forms,Windows,Classes,Variants,Graphics,Messages,ComCtrls,SysUtils,DB,Controls,
  StdCtrls,CommCtrl,Menus;
                                           

type
  TStateChangEvent = procedure(Sender: TObject; Node: TTreeNode;NewState: TCheckBoxState) of object;
  TStateChangingEvent = procedure(Sender: TObject; Node: TTreeNode;var AllowChange: Boolean) of object;

  TCheckKind = (ckNone, ckCheck, ckRadio, ckGroup);
  TCheckFlatness = (cfAlwaysFlat, cfAlways3d, cfHotTrack);
  TABCheckTreeNode = class;
  TABCheckTreeView = class(TCustomTreeView)
  private
    FDesignInteractive: Boolean;
    FGrayedIsChecked: Boolean;
    FStateImages: TImageList;
    FFlatness: TCheckFlatness;
    FOnStateChang: TStateChangEvent;
    FOnStateChanging: TStateChangingEvent;
    FMoveAfirm: boolean;
    function GetChecked(Node: TTreeNode): Boolean;
    procedure SetChecked(Node: TTreeNode; Value: Boolean);
    function GetState(Node: TTreeNode): TCheckBoxState;
    procedure SetState(Node: TTreeNode; Value: TCheckBoxState);
    function GetKind(Node: TTreeNode): TCheckKind;
    procedure SetKind(Node: TTreeNode; Value: TCheckKind);
    procedure SetFlatness(const Value: TCheckFlatness);
    function GetNodeEnabled(Node: TTreeNode): Boolean;
    procedure SetNodeEnabled(Node: TTreeNode; const Value: Boolean);
    procedure ReadData(Stream: TStream);
    procedure WriteData(Stream: TStream);
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure CMDesignHitTest(var Message: TCMDesignHitTest); message
      CM_DESIGNHITTEST;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message
      WM_LBUTTONDBLCLK;
    procedure CustomDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
  protected
    FHoverCache: TABCheckTreeNode;
    procedure ToggleNode(Node: TABCheckTreeNode); dynamic;
    procedure Change(Node: TTreeNode); override;
    procedure CreateCheckMarks; dynamic;
    function CreateNode: TTreeNode; override;
    procedure DefineProperties(Filer: TFiler); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y:
      Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Loaded; override;
    procedure Click;override;
    procedure CreateParams(var Param:TCreateParams);override;
  public
    procedure DragDrop(Source: TObject; X, Y: Integer); override;
    procedure DragOver(Source: TObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean); override;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure MakeRadioGroup(Node: TTreeNode);
    property Checked[Node: TTreeNode]: Boolean read GetChecked write SetChecked;
    property State[Node: TTreeNode]: TCheckBoxState read GetState write
      SetState;
    property CheckKind[Node: TTreeNode]: TCheckKind read GetKind write SetKind;
    property NodeEnabled[Node: TTreeNode]: Boolean read GetNodeEnabled write
      SetNodeEnabled;
  published

    property MoveAfirm: boolean read FMoveAfirm write FMoveAfirm;
    //add by grj
    property OnStateChang: TStateChangEvent read FOnStateChang write FOnStateChang;
    property OnStateChanging: TStateChangingEvent read FOnStateChanging write FOnStateChanging;

    property DesignActive: Boolean read FDesignInteractive write FDesignInteractive stored False;
    property Align;
    property Anchors;
    property AutoExpand;
    property BiDiMode;
    property BorderStyle;
    property BorderWidth;
    property ChangeDelay;
    property Color;
    property Ctl3D;
    property Constraints;
    property DragKind;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property Flatness: TCheckFlatness read FFlatness write SetFlatness;
    property GrayedIsChecked: Boolean read FGrayedIsChecked write
      FGrayedIsChecked;
    property HideSelection;
    property HotTrack;
    property Images;
    property Indent;
    property Items;
    property ParentBiDiMode;
    property ParentColor default False;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property RightClickSelect;
    property RowSelect;
    property ShowButtons;
    property ShowHint;
    property ShowLines;
    property ShowRoot;
    property SortType;
    //property StateImages;
    property TabOrder;
    property TabStop default True;
    property ToolTips;
    property Visible;
    property OnChange;
    property OnChanging;
    property OnClick;
    property OnCollapsing;
    property OnCollapsed;
    property OnCompare;
    property OnCustomDraw;
    property OnCustomDrawItem;
    property OnDblClick;
    property OnDeletion;
    property OnDragDrop;
    property OnDragOver;
    property OnEdited;
    property OnEditing;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnExpanding;
    property OnExpanded;
    property OnGetImageIndex;
    property OnGetSelectedIndex;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

  TABCheckTreeNode = class(TTreeNode)
  private
    FReflexChildren: Boolean;
    FCheckKind: TCheckKind;
    FCache: TCheckBoxState;
    FEnabled: Boolean;
    FReflexParent: Boolean;
    function IsEqual(Node: TTreeNode): Boolean;
    function GetItemIndex: Integer;
    function GetChecked: Boolean;
    procedure SetChecked(Value: Boolean);
    procedure SetCheckKind(Value: TCheckKind);
    procedure SetEnabled(Value: Boolean);
    function GetState: TCheckBoxState;
    procedure SetState(Value: TCheckBoxState);
    procedure SetItemIndex(Value: Integer);
    procedure SetReflexChildren(Value: Boolean);
    procedure SetReflexParent(Value: Boolean);
    procedure ReadSelf(Stream: TStream);
    procedure WriteSelf(Stream: TStream);
  public
    procedure InternalSetState(Value: TCheckBoxState;
      CheckChildren: Boolean = True; CheckParent: Boolean = True);
    procedure DoCheckChildren(Cur: TCheckBoxState);
    procedure DoCheckParent(Cur: TCheckBoxState);
    procedure UpdateHotTrack(Hover: Boolean); virtual;
  public
    procedure AfterConstruction; override;
    procedure MakeRadioGroup;
    procedure Assign(Source: TPersistent); override;
    property Checked: Boolean read GetChecked write SetChecked;
    property CheckKind: TCheckKind read FCheckKind write SetCheckKind;
    property CheckState: TCheckBoxState read GetState write SetState;
    property Enabled: Boolean read FEnabled write SetEnabled;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
    property ReflexChildren: Boolean read FReflexChildren write
      SetReflexChildren;
    property ReflexParent: Boolean read FReflexParent write SetReflexParent;
  end;

  ECheckTreeViewError = class(Exception);
  EIndexError = class(ECheckTreeViewError);


//设置CheckTree所有结点的选择
procedure ABSetCheckTreeViewState(aCheckTreeView: TABCheckTreeView; aCheckTreeViewOperator: TABCheckTreeViewOperator);overload;
//根据结点文本设置树中结点为选择,多个结点时aSetCheckTexts由分号分隔
procedure ABSetCheckTreeViewState(aCheckTreeView:TABCheckTreeView;aSetCheckTexts:string);overload;

//根据子节点刷新树中所有父结点状态(当父结点状态没有随子结点状态改变时需调用)
procedure ABSetCheckTreeViewParentNodeState(aCheckTreeView: TABCheckTreeView;aProgressBar:TProgressBar=nil);

//以下三个过程当需要用CheckTreeView来进行从表增删时使用（如1.设置某一操作员下有哪些角色）
//根据数据集刷新树中所有结点状态(在移动主表的记录时调用，用来重建CheckTreeView从表中设置了哪些内容，如移动操作员时刷新此操作员下的角色)
procedure ABRefreshCheckTreeViewState(var aDo:boolean;
                             aCheckTreeView:TABCheckTreeView;
                             aBaseDataset,aKeyDataset:Tdataset;
                             aBaseGuid,aKeyGuid:string;
                             aBaseDatasetCheckTrueFieldName:string='');
//CheckTree结点改变选择后同步数据集（CheckTreeView的StateChang中调用，用来将增删的操作同步到数据库中去）
procedure ABUpdateCheckTreeViewStateChange(aDo:boolean;
                             aBaseDataset,aKeyDataset:Tdataset;
                             aBaseGuid,aKeyGuid:string;
                             aNode: TTreeNode;aNewState: TCheckBoxState;
                             aBaseDatasetCheckTrueFieldName:string='');

implementation

type
  PCheckNodeData = ^TCheckNodeData;
  TCheckNodeData = packed record
    Kind: TCheckKind;
    Enabled: Boolean;
  end;

resourcestring
  SIndexError       = 'Tree node index (%d) out of range';
  SInvalidKind      = 'Trying to set index (%d) of non-radio item';

procedure ABSetCheckTreeViewState(aCheckTreeView:TABCheckTreeView;aSetCheckTexts:string);
var
  i: LongInt;
  tempText:string;
  tempNode:TTreeNode;
begin
  if aSetCheckTexts<>emptystr then
  begin
    for I := 1 to ABGetSpaceStrCount(aSetCheckTexts, ',') do
    begin
      tempText := ABGetSpaceStr(aSetCheckTexts, i, ',');
      if tempText<>emptystr then
      begin
        tempNode:=ABFindTree(aCheckTreeView.Items,tempText,[]);
        if Assigned(tempNode) then
          aCheckTreeView.Checked[tempNode]:=true;
      end;
    end;
  end;
end;

procedure ABSetCheckTreeViewParentNodeState(aCheckTreeView: TABCheckTreeView;aProgressBar:TProgressBar);
var
  i: LongInt;
  function GetCheck(aTreeNode: TTreeNode):TCheckBoxState;
  var
    j: LongInt;
    tempCheckCount,
    tempUnCheckCount:LongInt;

    tempCurState:TCheckBoxState ;
  begin
    Result:=cbUnchecked;
    if (aTreeNode.Count>0) then
    begin
      tempCheckCount:=0;
      tempUnCheckCount:=0;
      for j := 0 to aTreeNode.Count-1 do
      begin
        if aTreeNode.Item[j].HasChildren then
          TABCheckTreeNode(aTreeNode.Item[j]).InternalSetState(GetCheck(aTreeNode.Item[j]),False,false);

        tempCurState:=aCheckTreeView.State[aTreeNode.Item[j]];
        if (tempCurState=cbChecked) then
        begin
          tempCheckCount:=tempCheckCount+1;
        end
        else if (tempCurState=cbUnChecked) then
        begin
          tempUnCheckCount:=tempUnCheckCount+1;
        end;
      end;

      if tempCheckCount=aTreeNode.Count then
      begin
        Result:=cbChecked;
      end
      else if tempUnCheckCount=aTreeNode.Count then
      begin
        Result:=cbUnChecked;
      end
      else
      begin
        Result:=cbGrayed;
      end;
    end;
  end;
begin
  ABSetProgressBarBegin(aProgressBar,aCheckTreeView.Items.Count);
  try
    for I := 0 to aCheckTreeView.Items.Count-1 do
    begin
      ABSetProgressBarNext(aProgressBar);
      if (aCheckTreeView.Items[i].Level=0) and
         (aCheckTreeView.Items[i].HasChildren) then
        TABCheckTreeNode(aCheckTreeView.Items[i]).InternalSetState(GetCheck(aCheckTreeView.Items[i]),False,false);
    end;
  finally
    ABSetProgressBarEnd(aProgressBar);
  end;
end;

//为加快性能使用InternalSetState
procedure ABSetCheckTreeViewState(aCheckTreeView: TABCheckTreeView; aCheckTreeViewOperator: TABCheckTreeViewOperator);
var
  i: LongInt;
  tempState:TCheckBoxState;
begin
  aCheckTreeView.Items.BeginUpdate;
  try
    for i := 0 to aCheckTreeView.Items.Count - 1 do
    begin
      tempState:=aCheckTreeView.State[aCheckTreeView.Items[i]];
      case aCheckTreeViewOperator of
        coCheck:
        begin
          if tempState<>cbChecked then
            TABCheckTreeNode(aCheckTreeView.Items[i]).InternalSetState(cbChecked,False,false);
        end;
        coUnCheck:
        begin
          if tempState<>cbUnchecked then
            TABCheckTreeNode(aCheckTreeView.Items[i]).InternalSetState(cbUnchecked,False,false);
        end;
        coReverse:
        begin
          if tempState<>cbGrayed then
          begin
            TABCheckTreeNode(aCheckTreeView.Items[i]).InternalSetState(ABIIF(tempState=cbUnchecked,cbChecked,cbUnchecked),False,false);
          end;
        end;
      end;
    end;
  finally
    aCheckTreeView.Items.EndUpdate;
  end;
end;

procedure ABRefreshCheckTreeViewState(var aDo:boolean;
                             aCheckTreeView:TABCheckTreeView;
                             aBaseDataset,aKeyDataset:Tdataset;
                             aBaseGuid,aKeyGuid:string;
                             aBaseDatasetCheckTrueFieldName:string);
var
  i:LongInt;
begin
  if not aDo then
    exit;

  if (Assigned(aKeyDataset)) and
     (Assigned(aKeyDataset.DataSource)) and
     (Assigned(aKeyDataset.DataSource.DataSet)) then
    aCheckTreeView.Enabled:=(not aKeyDataset.DataSource.DataSet.IsEmpty);

  aDo:=false;
  aCheckTreeView.Items.BeginUpdate;
  aBaseDataset.DisableControls;
  aKeyDataset.DisableControls;
  try
    for I := 0 to aCheckTreeView.Items.Count - 1 do
    begin
      if (not ABPointerIsNull(aCheckTreeView.Items[i].Data)) and
         (not aKeyDataset.IsEmpty) then
      begin
        if (ABSetDatasetRecno(aBaseDataset,LongInt(aCheckTreeView.Items[i].Data))>0) and
           ((aBaseDatasetCheckTrueFieldName=emptystr) or
            (aBaseDataset.FieldByName(aBaseDatasetCheckTrueFieldName).AsBoolean)
            ) then
        begin
          if aKeyDataset.Locate(aKeyGuid,aBaseDataset.FindField(aBaseGuid).AsString,[]) then
          begin
            TABCheckTreeNode(aCheckTreeView.Items[i]).InternalSetState(cbchecked,False,false);
            //aCheckTreeView.Checked[aCheckTreeView.Items[i]] := true;
          end
          else
          begin
            TABCheckTreeNode(aCheckTreeView.Items[i]).InternalSetState(cbUnchecked,False,false);
            //aCheckTreeView.Checked[aCheckTreeView.Items[i]] := false;
          end;
        end;
      end
      else
      begin
        TABCheckTreeNode(aCheckTreeView.Items[i]).InternalSetState(cbUnchecked,False,false);
        //aCheckTreeView.Checked[aCheckTreeView.Items[i]] := false;
      end;
    end;
    ABSetCheckTreeViewParentNodeState(aCheckTreeView);
  finally
    aDo:=true;
    aCheckTreeView.Items.EndUpdate;
    aBaseDataset.EnableControls;
    aKeyDataset.EnableControls;
  end;
end;

procedure ABUpdateCheckTreeViewStateChange(aDo:boolean;
                             aBaseDataset,aKeyDataset:Tdataset;
                             aBaseGuid,aKeyGuid:string;
                             aNode: TTreeNode;aNewState: TCheckBoxState;
                             aBaseDatasetCheckTrueFieldName:string);
begin
  if not aDo then
    exit;

  if (not aNode.HasChildren) and
     (not ABPointerIsNull(aNode.Data)) then
  begin
    if (ABSetDatasetRecno(aBaseDataset,LongInt(aNode.Data))>0) and
       (aBaseDatasetCheckTrueFieldName=emptystr) or
       (aBaseDataset.FieldByName(aBaseDatasetCheckTrueFieldName).AsBoolean) then
    begin
      if aNewState=cbchecked then
      begin
        if not aKeyDataset.Locate(aKeyGuid,aBaseDataset.FindField(aBaseGuid).AsString,[]) then
        begin
          aKeyDataset.Append;
          aKeyDataset.FindField(aKeyGuid).AsString:=aBaseDataset.FindField(aBaseGuid).AsString;
          aKeyDataset.Post;
        end;
      end
      else if aNewState=cbUnchecked then
      begin
        if aKeyDataset.Locate(aKeyGuid,aBaseDataset.FindField(aBaseGuid).AsString,[]) then
        begin
          aKeyDataset.Delete;
        end;
      end
    end;
  end;
end;

{ TABCheckTreeView }
procedure TABCheckTreeView.CMDesignHitTest(var Message: TCMDesignHitTest);
var
  N                 : TTreeNode;
begin
  Message.Result := 0;
  if FDesignInteractive then
  begin
    N := GetNodeAt(Message.XPos, Message.YPos);
    if N <> nil then
      Message.Result := Integer(N.DisplayRect(True).Right > Message.XPos);
  end;
end;

procedure TABCheckTreeView.CMMouseLeave(var Message: TMessage);
begin
  if FHoverCache <> nil then FHoverCache.UpdateHotTrack(False);
  FHoverCache := nil;
  inherited;
end;

constructor TABCheckTreeView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  DesignActive :=True;
  //ReadOnly := True;
end;

procedure TABCheckTreeView.CreateCheckMarks;
const
  R                 : TRect = (Left: 2; Top: 2; Right: 15; Bottom: 15);
var
  Bmp, M            : TBitmap;

  procedure Add(MaskColor: TColor = clWhite);
  begin
    FStateImages.AddMasked(Bmp, MaskColor);
  end;

begin
  if FStateImages = nil then Exit;
  Items.BeginUpdate;
  Bmp := TBitmap.Create;
  M := TBitmap.Create;
  try
    Bmp.Width := 16;
    Bmp.Height := 16;
    M.Width := 16;
    M.Height := 16;

    { Add stub image }
    {1} Add;

    { Add flat images }
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or
      DFCS_FLAT);
    {2} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or
      DFCS_FLAT or DFCS_CHECKED);
    {3} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or
      DFCS_FLAT or DFCS_CHECKED or DFCS_BUTTON3STATE);
    {4} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOIMAGE or
      DFCS_FLAT);
    DrawFrameControl(M.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOMASK or
      DFCS_FLAT);
    {5} FStateImages.Add(Bmp, M);
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOIMAGE or
      DFCS_FLAT or DFCS_CHECKED);
    DrawFrameControl(M.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOMASK or
      DFCS_FLAT or DFCS_CHECKED);
    {6} FStateImages.Add(Bmp, M);

    { Add 3d images }
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK);
    {7} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or
      DFCS_CHECKED);
    {8} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or
      DFCS_CHECKED or DFCS_BUTTON3STATE);
    {9} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOIMAGE);
    DrawFrameControl(M.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOMASK);
    {10} FStateImages.Add(Bmp, M);
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOIMAGE or
      DFCS_CHECKED);
    DrawFrameControl(M.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOMASK or
      DFCS_CHECKED);
    {11} FStateImages.Add(Bmp, M);

    { Add disabled images }
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK
      or DFCS_INACTIVE);
    {12} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or
      DFCS_CHECKED or DFCS_INACTIVE);
    {13} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or
      DFCS_CHECKED or DFCS_BUTTON3STATE or DFCS_INACTIVE);
    {14} Add;
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOIMAGE
      or DFCS_INACTIVE);
    DrawFrameControl(M.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOMASK
      or DFCS_INACTIVE);
    {15} FStateImages.Add(Bmp, M);
    DrawFrameControl(Bmp.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOIMAGE or
      DFCS_CHECKED or DFCS_INACTIVE);
    DrawFrameControl(M.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONRADIOMASK or
      DFCS_CHECKED or DFCS_INACTIVE);
    {16} FStateImages.Add(Bmp, M);

  finally
    M.Free;
    Bmp.Free;
    Items.EndUpdate;
  end;
end;

function TABCheckTreeView.CreateNode: TTreeNode;
begin
  Result := TABCheckTreeNode.Create(Items);
{
  TABCheckTreeNode(Result)ReflexChildren := True;
  TABCheckTreeNode(Result)ReflexParent := True;
  }
end;

destructor TABCheckTreeView.Destroy;
begin
  inherited Destroy;
end;

procedure TABCheckTreeView.DragDrop(Source: TObject; X, Y: Integer);
var
  VL_Node, VL_tmpNode: TTreeNode;
  VL_I: integer;
begin
  inherited;

  if (Selected <> nil) and (Selected.level >= 0) then
  begin
    VL_Node := GetNodeAt(X, Y);
    if VL_Node <> nil then
    begin
      if Selected.Parent = VL_Node then Exit;
      if Selected = VL_Node then Exit;
      if Selected.Level < VL_Node.Level then
      begin
        VL_tmpNode := VL_Node;
        for VL_I := 0 to VL_Node.Level - Selected.Level - 1 do
        begin
          VL_tmpNode := VL_tmpNode.Parent;
        end;
        if Selected = VL_tmpNode then Exit;
      end;

      if (FMoveAfirm) and
         (ABShow('是否确认要移动该节点?',[],['是','否'],2)<>1) then
          Exit;

      Selected.MoveTo(VL_Node, naADDChild);
    end;
  end;
end;

procedure TABCheckTreeView.DragOver(Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  inherited;
  Accept:=true;
end;

function TABCheckTreeView.GetChecked(Node: TTreeNode): Boolean;
begin
  Result := TABCheckTreeNode(Node).Checked
end;

function TABCheckTreeView.GetNodeEnabled(Node: TTreeNode): Boolean;
begin
  Result := TABCheckTreeNode(Node).Enabled;
end;

function TABCheckTreeView.GetKind(Node: TTreeNode): TCheckKind;
begin
  Result := TABCheckTreeNode(Node).CheckKind
end;

function TABCheckTreeView.GetState(Node: TTreeNode): TCheckBoxState;
begin
  Result := TABCheckTreeNode(Node).CheckState
end;

procedure TABCheckTreeView.Loaded;
begin
  FStateImages := TImageList.Create(Self);
  StateImages := FStateImages;
  CreateCheckMarks;
  FDesignInteractive := True;
  inherited Loaded;
end;

procedure TABCheckTreeView.MakeRadioGroup(Node: TTreeNode);
begin
  TABCheckTreeNode(Node).MakeRadioGroup;
end;

procedure TABCheckTreeView.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) and (htOnStateIcon in GetHitTestInfoAt(X, Y)) then
    ToggleNode(TABCheckTreeNode(GetNodeAt(X, Y)));
  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TABCheckTreeView.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  N                 : TABCheckTreeNode;
begin
  N := TABCheckTreeNode(GetNodeAt(X, Y));
  if HotTrack and (Flatness = cfHotTrack) then
  begin
    if (FHoverCache <> N) then
    begin
      if FHoverCache <> nil then FHoverCache.UpdateHotTrack(False);
      if N <> nil then N.UpdateHotTrack(True);
      FHoverCache := N;
    end;
  end;
  inherited MouseMove(Shift, X, Y);
end;

procedure TABCheckTreeView.SetChecked(Node: TTreeNode; Value: Boolean);
begin
  TABCheckTreeNode(Node).Checked := Value
end;

procedure TABCheckTreeView.SetNodeEnabled(Node: TTreeNode; const Value: Boolean);
begin
  TABCheckTreeNode(Node).Enabled := Value
end;

procedure TABCheckTreeView.SetFlatness(const Value: TCheckFlatness);
var
  I                 : Integer;
begin
  if FFlatness <> Value then
  begin
    FFlatness := Value;
    for I := 0 to Items.Count - 1 do
      TABCheckTreeNode(Items[I]).InternalSetState(
        TABCheckTreeNode(Items[I]).GetState, False, False)
  end;
end;

procedure TABCheckTreeView.SetKind(Node: TTreeNode; Value: TCheckKind);
begin
  TABCheckTreeNode(Node).CheckKind := Value
end;

procedure TABCheckTreeView.SetState(Node: TTreeNode; Value: TCheckBoxState);
begin
  TABCheckTreeNode(Node).CheckState := Value
end;

procedure TABCheckTreeView.WMLButtonDblClk(var Message: TWMLButtonDblClk);
begin
  if not (htOnStateIcon in GetHitTestInfoAt(Message.XPos, Message.YPos)) then
    inherited;
end;

procedure TABCheckTreeView.Change(Node: TTreeNode);
begin
  if csDesigning in ComponentState then GetParentForm(Self).Designer.Modified;
  inherited Change(Node);
end;

procedure TABCheckTreeView.WMKeyDown(var Message: TWMKeyDown);
begin
  if Message.CharCode = VK_SPACE then
  begin
    ToggleNode(TABCheckTreeNode(Selected));
    Message.Result := 0;
  end
  else
    inherited;
end;

procedure TABCheckTreeView.ToggleNode(Node: TABCheckTreeNode);
var
  PrevCheck         : TCheckBoxState;
  Frm               : TCustomForm;
begin
  if (Node <> nil) and Node.Enabled then
  begin
    if Node.CheckKind = ckCheck then
    begin
      PrevCheck := Node.GetState;
      if PrevCheck <> cbChecked then
      begin
        Node.InternalSetState(cbChecked);
        if Node.GetState = PrevCheck then Node.InternalSetState(cbUnchecked);
      end
      else
        Node.InternalSetState(cbUnchecked);
    end
    else if Node.CheckKind = ckRadio then
      Node.Checked := True;
    if csDesigning in ComponentState then
    begin
      Frm := GetParentForm(Self);
      if Frm <> nil then
        Frm.Designer.Modified
    end;
  end;
end;

procedure TABCheckTreeView.DefineProperties(Filer: TFiler);

  function WriteNodes: Boolean;
  var
    I               : Integer;
    Nodes           : TTreeNodes;
  begin
    Nodes := TTreeNodes(Filer.Ancestor);
    if Nodes = nil then
      Result := Items.Count > 0
    else if Nodes.Count <> Items.Count then
      Result := True
    else
    begin
      Result := False;
      for I := 0 to Items.Count - 1 do
      begin
        Result := not TABCheckTreeNode(Items.Item[I]).IsEqual(Nodes[I]);
        if Result then Break;
      end
    end;
  end;

begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('CheckNodesData', ReadData, WriteData, WriteNodes);
end;

procedure TABCheckTreeView.ReadData(Stream: TStream);
var
  I                 : Integer;
begin
  for I := 0 to Items.Count - 1 do
    TABCheckTreeNode(Items[I]).ReadSelf(Stream);
end;

procedure TABCheckTreeView.WriteData(Stream: TStream);
var
  I                 : Integer;
begin
  for I := 0 to Items.Count - 1 do
    TABCheckTreeNode(Items[I]).WriteSelf(Stream);
end;

procedure TABCheckTreeView.CreateParams(var Param: TCreateParams);
begin
  inherited;
  Param.Style :=Param.Style or TVS_CHECKBOXES;
end;

procedure TABCheckTreeView.CustomDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  inherited;
end;

procedure TABCheckTreeView.Click;
begin
  inherited;
end;

{ TABCheckTreeNode }

const
  StateIndexes      : array[TCheckKind, TCheckBoxState] of Integer =
    ((-1, -1, -1), (1, 2, 3), (4, 5, 4), (-1, -1, -1));

procedure TABCheckTreeNode.AfterConstruction;
begin
  FEnabled := True;
  FReflexParent := True;
  FReflexChildren := True;
  FCheckKind := ckCheck;
end;

procedure TABCheckTreeNode.Assign(Source: TPersistent);
var
  Node              : TABCheckTreeNode;
begin
  inherited Assign(Source);
  if Source is TABCheckTreeNode then
  begin
    Node := TABCheckTreeNode(Source);
    StateIndex := Node.StateIndex;
    ReflexChildren := Node.ReflexChildren;
    ReflexParent := Node.ReflexParent;
    FCheckKind := Node.CheckKind;
  end;
end;

procedure TABCheckTreeNode.DoCheckChildren(Cur: TCheckBoxState);
var
  I                 : Integer;
  MustCheckParent   : Boolean;
  N, D              : TABCheckTreeNode;
begin
  MustCheckParent := False;
  D := nil;
  if (FCheckKind in [ckCheck, ckGroup]) and FReflexChildren and (Cur <> cbGrayed)
    then
  begin
    for I := 0 to Count - 1 do
    begin
      N := TABCheckTreeNode(Item[I]);
      if N.Enabled then
        N.InternalSetState(Cur, True, False)
      else
      begin
        MustCheckParent := True;
        D := N;
      end;
    end;
    if MustCheckParent then D.DoCheckParent(Cur);
  end;
end;

procedure TABCheckTreeNode.DoCheckParent(Cur: TCheckBoxState);
var
  I                 : Integer;
  Ch, Un            : Boolean;
begin
  Ch := True;
  Un := True;
  if (FCheckKind in [ckCheck, ckGroup]) and FReflexParent and (Parent <> nil) then
  begin
    for I := 0 to TABCheckTreeNode(Parent).Count - 1 do
    begin
      if TABCheckTreeNode(TABCheckTreeNode(Parent).Item[I]).FCheckKind in [ckCheck, ckGroup] then
        case TABCheckTreeNode(TABCheckTreeNode(Parent).Item[I]).GetState of
          cbUnchecked: Ch := False;
          cbChecked:
            Un := False
        else
          begin
            Ch := False;
            Un := False;
            Break;
          end
        end;
      if not Ch and not Un then Break;
    end;
    if Ch then
      TABCheckTreeNode(Parent).InternalSetState(cbChecked, False, True)
    else if Un then
      TABCheckTreeNode(Parent).InternalSetState(cbUnchecked, False, True)
    else
      TABCheckTreeNode(Parent).InternalSetState(cbGrayed, False, True)
  end;
end;

function TABCheckTreeNode.GetChecked: Boolean;
var
  S                 : TCheckBoxState;
begin
  S := GetState;
  Result := S = cbChecked;
  if (FCheckKind in [ckCheck, ckGroup]) and
    TABCheckTreeView(TreeView).FGrayedIsChecked and (S = cbGrayed) then
    Result := True
end;

function TABCheckTreeNode.GetItemIndex: Integer;
begin
  for Result := 0 to Count - 1 do
    if (TABCheckTreeNode(Item[Result]).FCheckKind = ckRadio) and TABCheckTreeNode(Item[Result]).Checked then Exit;
  Result := -1;
end;

function TABCheckTreeNode.GetState: TCheckBoxState;
begin
  case StateIndex of
    - 1..1, 4, 6, 9, 11, 14: Result := cbUnchecked;
    2, 5, 7, 10, 12, 15:
      Result := cbChecked
  else
    Result := cbGrayed
  end;
  if FCheckKind = ckGroup then Result := FCache;
end;

procedure TABCheckTreeNode.InternalSetState(Value: TCheckBoxState;
  CheckChildren, CheckParent: Boolean);
var
  I                 : Integer;
  Node              : TABCheckTreeNode;
  SI                : Integer;
  tempAllowChange:boolean;
begin
  //add by grj
  if Assigned(TABCheckTreeView(TreeView).OnStateChanging) then
  begin
    tempAllowChange:=true;
    TABCheckTreeView(TreeView).OnStateChanging(TABCheckTreeView(TreeView),Self,tempAllowChange);
    if not tempAllowChange then
      exit;
  end;

  SI := StateIndexes[FCheckKind, Value];
  if SI > 0 then
    if not Enabled then
      StateIndex := SI + 10
    else if TABCheckTreeView(TreeView).Flatness = cfAlways3d then
      StateIndex := SI + 5
    else
      StateIndex := SI;
  UpdateHotTrack(TABCheckTreeView(TreeView).FHoverCache = Self);
  if FCheckKind = ckGroup then FCache := Value;
  if (FCheckKind = ckRadio) and (Value = cbChecked) then
    for I := 0 to Parent.Count - 1 do
    begin
      Node := TABCheckTreeNode(Parent.Item[I]);
      if (Node <> Self) and (Node.FCheckKind = ckRadio) then
        Node.InternalSetState(cbUnchecked, False, False);
    end;
  if CheckChildren then DoCheckChildren(Value);
  if CheckParent then DoCheckParent(Value);

  //add by grj
  if Assigned(TABCheckTreeView(TreeView).OnStateChang) then
  begin
    TABCheckTreeView(TreeView).OnStateChang(TABCheckTreeView(TreeView),Self,Value);
  end;
end;

const
  BoolChecks        : array[Boolean] of TCheckBoxState = (cbUnchecked, cbChecked);

function TABCheckTreeNode.IsEqual(Node: TTreeNode): Boolean;
begin
  Result := (Text = Node.Text) and (Data = Node.Data);
end;

procedure TABCheckTreeNode.MakeRadioGroup;
var
  I                 : Integer;
begin
  CheckKind := ckNone;
  for I := Count - 1 downto 0 do
    if not Item[I].HasChildren then
        TABCheckTreeNode(Item[I]).CheckKind := ckRadio;
        //Checked := True
end;

procedure TABCheckTreeNode.ReadSelf(Stream: TStream);
var
  Data              : TCheckNodeData;
begin
  Stream.ReadBuffer(Data, SizeOf(Data));
  FCheckKind := Data.Kind;
  FEnabled := Data.Enabled;
end;

procedure TABCheckTreeNode.SetChecked(Value: Boolean);
begin
  if GetState <> BoolChecks[Value] then
    InternalSetState(BoolChecks[Value])
end;

procedure TABCheckTreeNode.SetCheckKind(Value: TCheckKind);
begin
  if FCheckKind <> Value then
  begin
    FCheckKind := Value;
    if FCheckKind = ckNone then
      StateIndex := -1
    else if FCheckKind = ckGroup then
    begin
      FCache := GetState;
      StateIndex := -1;
    end
    else
      InternalSetState(GetState);
  end;
end;

procedure TABCheckTreeNode.SetEnabled(Value: Boolean);
var
  I                 : Integer;
begin
  if FEnabled <> Value then
  begin
    FEnabled := Value;
    InternalSetState(GetState, False, False);
  end;
  for I := 0 to Count - 1 do
    TABCheckTreeNode(Item[I]).Enabled := Value;
end;

procedure TABCheckTreeNode.SetItemIndex(Value: Integer);
begin
  if Value > Count then raise EIndexError.CreateFmt(SIndexError, [Value]);
  if TABCheckTreeNode(Item[Value]).FCheckKind <> ckRadio then
    EIndexError.CreateFmt(SInvalidKind, [Value]);
  TABCheckTreeNode(Item[Value]).SetState(cbChecked);
end;

procedure TABCheckTreeNode.SetReflexChildren(Value: Boolean);
begin
  if FReflexChildren <> Value then
  begin
    FReflexChildren := Value;
    DoCheckChildren(GetState)
  end;
end;

procedure TABCheckTreeNode.SetReflexParent(Value: Boolean);
begin
  if FReflexParent <> Value then
  begin
    FReflexParent := Value;
    DoCheckParent(GetState);
  end;
end;

procedure TABCheckTreeNode.SetState(Value: TCheckBoxState);
begin
  if Value <> GetState then
    InternalSetState(Value)
end;

procedure TABCheckTreeNode.UpdateHotTrack(Hover: Boolean);
begin
  if (FCheckKind in [ckCheck, ckRadio]) and Enabled and
    (TABCheckTreeView(TreeView).Flatness <> cfAlways3d) then
  begin
    if Hover then
      StateIndex := StateIndexes[FCheckKind, GetState] + 5
    else
      StateIndex := StateIndexes[FCheckKind, GetState];
  end;
end;

procedure TABCheckTreeNode.WriteSelf(Stream: TStream);
var
  Data              : TCheckNodeData;
begin
  Data.Kind := CheckKind;
  Data.Enabled := Enabled;
  Stream.WriteBuffer(Data, SizeOf(Data));
end;

end.




