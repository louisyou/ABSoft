{
本地参数组件单元
}
unit ABPubLocalParamsU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubConstU,
  ABPubFuncU,
  ABPubVarU,
  ABPubStartFormU,
  ABPubCharacterCodingU,

  SysUtils,Classes,typinfo;

Type
  TABLocalParams = record
    HaveFileBeforeRun:Boolean;             //运行程序前参数文件是否存在,框架内部维护,外部访问不可修改

    LoginType:TABLoginType;                //连接类型
    ShowStartForm   :Boolean;              //显示启动窗体
    Debug           :Boolean;              //调试模式，记录详细日志

    DownPkg                :Boolean;       //是否启用下载包
    FuncUpdateCount        :LongInt;       //下载的索引号
 
    SockLocalPort          :LongInt;       //本地SOCK端口
    SockToPort             :LongInt;       //目的地SOCK端口

    Language               :string;        //当前语言
    LanguageEnabled        :Boolean;       //是否启用多语言

    CurSkinName            :string;        //使用的皮肤名称
    BackType               :TABBackType; //客户端背景类型

    SourceCodeChineseType  :TABChineseType;//源代码字符集,由编码者确定,用于源代码的字符集,框架内部维护,外部访问不可修改
    PriLanguageChineseType :TABChineseType;//上一语言对应的字符集,默认为源码编译的字符集,框架内部维护,外部访问不可修改
    LanguageChineseType    :TABChineseType;//语言对应的字符集,默认为当前语言对应的字符集,框架内部维护,外部访问不可修改
  end;

//保存本地参数值到INI配制文件中
procedure ABSetLocalParamsValue(aName: string; aValue: string;aGroup: string='';aLocalParamsFile:string='');
//从参数文件中重新刷新参数
procedure ABReReadLocalParams(aLocalParamsFile:string='');

var
  ABLocalParams: TABLocalParams;


implementation

procedure ABSetLocalParamsValue(aName: string; aValue: string;aGroup: string;aLocalParamsFile:string);
begin
  aGroup:=Trim(aGroup);
  aName:=Trim(aName);
  if aLocalParamsFile = EmptyStr then
    aLocalParamsFile := ABLocalParamsFile;
  if aGroup = EmptyStr then
    aGroup := 'Params';

  if AnsiCompareText(aGroup,'Params')=0 then
  begin
    if AnsiCompareText(aName,'LoginType')=0 then
    begin
      ABLocalParams.LoginType:=TABLoginType(GetEnumValue(TypeInfo(TABLoginType),aValue));
      ABWriteInI('Params','LoginType',aValue, aLocalParamsFile);
    end
    else if AnsiCompareText(aName,'ShowStartForm')=0 then
    begin
      ABLocalParams.ShowStartForm:=ABStrToBool(aValue);
      ABWriteInI('Params','ShowStartForm',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'Debug')=0 then
    begin
      ABLocalParams.Debug:=ABStrToBool(aValue);
      ABWriteInI('Params','Debug',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'FuncUpdateCount')=0 then
    begin
      ABLocalParams.FuncUpdateCount:=StrToInt(aValue);
      ABWriteInI('Params','FuncUpdateCount',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'SockLocalPort')=0 then
    begin
      ABLocalParams.SockLocalPort:=StrToInt(aValue);
      ABWriteInI('Params','SockLocalPort',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'SockToPort')=0 then
    begin
      ABLocalParams.SockToPort:=StrToInt(aValue);
      ABWriteInI('Params','SockToPort',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'Language')=0 then
    begin
      ABLocalParams.Language:=aValue;
      ABWriteInI('Params','Language',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'LanguageEnabled')=0 then
    begin
      ABLocalParams.LanguageEnabled:=ABStrToBool(aValue);
      ABWriteInI('Params','LanguageEnabled',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'CurSkinName')=0 then
    begin
      ABLocalParams.CurSkinName:=aValue;
      ABWriteInI('Params','CurSkinName',aValue, aLocalParamsFile);
    end
    else if  AnsiCompareText(aName,'BackType')=0 then
    begin
      ABLocalParams.BackType:=TABBackType(GetEnumValue(TypeInfo(TABBackType),aValue));
      ABWriteInI('Params','BackType',aValue, aLocalParamsFile);
    end;
    //Params分类下新参数的保存加在此处
  end
  else if AnsiCompareText(aGroup,'...')=0 then
  begin
    //新分类下新参数的保存加在此处

  end;
end;

procedure ABReReadLocalParams(aLocalParamsFile:string);
var
  tempStrings:TStrings;
begin
  if aLocalParamsFile = EmptyStr then
    aLocalParamsFile := ABLocalParamsFile;

  ABLocalParams.LoginType       :=ltCS_Two;
  ABLocalParams.ShowStartForm   :=false   ;
  ABLocalParams.Debug           :=false   ;

  ABLocalParams.DownPkg         :=true    ;
  ABLocalParams.FuncUpdateCount :=0       ;

  ABLocalParams.SockLocalPort   :=80001   ;
  ABLocalParams.SockToPort      :=80002   ;

  ABLocalParams.Language        :='Auto'  ;
  ABLocalParams.LanguageEnabled :=False   ;

  ABLocalParams.CurSkinName     :=''  ;
  ABLocalParams.BackType        :=btNull  ;

  //新参数的默认值加在此处
  //....
  if ABCheckFileExists(aLocalParamsFile) then
  begin
    tempStrings := TStringList.Create;
    try
      ABReadInI('Params', aLocalParamsFile, tempStrings);

      if tempStrings.IndexOfName(trim('LoginType       '))>=0 then ABLocalParams.LoginType      :=TABLoginType(GetEnumValue(TypeInfo(TABLoginType),tempStrings.Values['LoginType']))  else ABWriteInI('Params','LoginType'      ,GetEnumName(TypeInfo(TABLoginType),ord(ABLocalParams.LoginType)), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('ShowStartForm   '))>=0 then ABLocalParams.ShowStartForm  :=ABStrToBool(tempStrings.Values['ShowStartForm'])                                    else ABWriteInI('Params','ShowStartForm'  ,ABBoolToStr(ABLocalParams.ShowStartForm), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('Debug           '))>=0 then ABLocalParams.Debug          :=ABStrToBool(tempStrings.Values['Debug'])                                            else ABWriteInI('Params','Debug'          ,ABBoolToStr(ABLocalParams.Debug), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('FuncUpdateCount '))>=0 then ABLocalParams.FuncUpdateCount:=StrToInt(tempStrings.Values['FuncUpdateCount'])                                     else ABWriteInI('Params','FuncUpdateCount',inttostr(ABLocalParams.FuncUpdateCount), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('SockLocalPort   '))>=0 then ABLocalParams.SockLocalPort  :=StrToInt(tempStrings.Values['SockLocalPort'])                                       else ABWriteInI('Params','SockLocalPort'  ,inttostr(ABLocalParams.SockLocalPort), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('SockToPort      '))>=0 then ABLocalParams.SockToPort     :=StrToInt(tempStrings.Values['SockToPort'])                                          else ABWriteInI('Params','SockToPort'     ,inttostr(ABLocalParams.SockToPort), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('LanguageEnabled '))>=0 then ABLocalParams.LanguageEnabled:=ABStrToBool(tempStrings.Values['LanguageEnabled'])                                  else ABWriteInI('Params','LanguageEnabled',ABBoolToStr(ABLocalParams.LanguageEnabled), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('BackType        '))>=0 then ABLocalParams.BackType       :=TABBackType(GetEnumValue(TypeInfo(TABBackType),tempStrings.Values['BackType']))     else ABWriteInI('Params','BackType'       ,GetEnumName(TypeInfo(TABBackType),ord(ABLocalParams.BackType)), aLocalParamsFile);
      if tempStrings.IndexOfName(trim('Language        '))>=0 then ABLocalParams.Language       :=tempStrings.Values['Language']                                                      else ABWriteInI('Params','Language'       ,ABLocalParams.Language, aLocalParamsFile);
      if tempStrings.IndexOfName(trim('CurSkinName     '))>=0 then ABLocalParams.CurSkinName    :=tempStrings.Values['CurSkinName']                                                   else ABWriteInI('Params','CurSkinName'    ,ABLocalParams.CurSkinName, aLocalParamsFile);
      //新参数的读取加在此处

      //D2009之前繁体中文环境编译不能显示简体，所以切到自动
      if (AnsiCompareText( ABLocalParams.Language,'China')=0) and
         (ABGetSysLanguage=wlChinaTw) then
      begin
        ABLocalParams.Language        :='Auto'
      end;

    finally
      tempStrings.Free;
    end;
  end;
end;

procedure ABInitialization;
begin
  ABLocalParams.HaveFileBeforeRun :=ABCheckFileExists(ABLocalParamsFile);

  ABReReadLocalParams;

  ABLocalParams.SourceCodeChineseType  :=ctGBChs;
  ABLocalParams.PriLanguageChineseType := ABLocalParams.SourceCodeChineseType;
  ABLocalParams.LanguageChineseType    := ABLanguageToChineseType(ABLocalParams.Language);

  if ABLocalParams.ShowStartForm then
  begin
    ABShowStartForm;
  end;
end;

procedure ABFinalization;
begin
  if not ABCheckFileExists(ABLocalParamsFile) then
  begin
    ABWriteInI('Params','LoginType'      ,GetEnumName(TypeInfo(TABLoginType),ord(ABLocalParams.LoginType)), ABLocalParamsFile);
    ABWriteInI('Params','ShowStartForm'  ,ABBoolToStr(ABLocalParams.ShowStartForm), ABLocalParamsFile);
    ABWriteInI('Params','Debug'          ,ABBoolToStr(ABLocalParams.Debug), ABLocalParamsFile);
    ABWriteInI('Params','FuncUpdateCount',inttostr(ABLocalParams.FuncUpdateCount), ABLocalParamsFile);
    ABWriteInI('Params','SockLocalPort'  ,inttostr(ABLocalParams.SockLocalPort), ABLocalParamsFile);
    ABWriteInI('Params','SockToPort'     ,inttostr(ABLocalParams.SockToPort), ABLocalParamsFile);

    ABWriteInI('Params','Language'       ,ABLocalParams.Language, ABLocalParamsFile);
    ABWriteInI('Params','LanguageEnabled',ABBoolToStr(ABLocalParams.LanguageEnabled), ABLocalParamsFile);
    ABWriteInI('Params','CurSkinName'    ,ABLocalParams.CurSkinName, ABLocalParamsFile);
    ABWriteInI('Params','BackType'       ,GetEnumName(TypeInfo(TABBackType),ord(ABLocalParams.BackType)), ABLocalParamsFile);
  end;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.
