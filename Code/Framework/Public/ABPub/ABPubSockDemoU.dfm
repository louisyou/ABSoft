object ABPubSockDemoForm: TABPubSockDemoForm
  Left = 0
  Top = 0
  Caption = 'ABPubSockDemoForm'
  ClientHeight = 610
  ClientWidth = 1060
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 530
    Height = 610
    Align = alLeft
    TabOrder = 0
    object ABSockUI1: TABSockUI
      Left = 1
      Top = 1
      Width = 528
      Height = 304
      Align = alTop
      Caption = 'Socket1'
      TabOrder = 0
      OnBegReadData = ABSockUI1BegReadData
      OnEndReadData = ABSockUI1EndReadData
    end
    object ABSockUI2: TABSockUI
      Left = 1
      Top = 305
      Width = 528
      Height = 304
      Align = alClient
      Caption = 'Socket2'
      TabOrder = 1
      OnBegReadData = ABSockUI1BegReadData
      OnEndReadData = ABSockUI1EndReadData
    end
  end
  object Panel2: TPanel
    Left = 530
    Top = 0
    Width = 530
    Height = 610
    Align = alClient
    TabOrder = 1
    object ABSockUI3: TABSockUI
      Left = 1
      Top = 1
      Width = 528
      Height = 304
      Align = alTop
      Caption = 'Socket3'
      TabOrder = 0
      OnBegReadData = ABSockUI1BegReadData
      OnEndReadData = ABSockUI1EndReadData
    end
    object ABSockUI4: TABSockUI
      Left = 1
      Top = 305
      Width = 528
      Height = 304
      Align = alClient
      Caption = 'Socket4'
      TabOrder = 1
      OnBegReadData = ABSockUI1BegReadData
      OnEndReadData = ABSockUI1EndReadData
    end
  end
end
