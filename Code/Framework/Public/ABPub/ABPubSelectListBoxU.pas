{
ListBox选择窗体单元
}
unit ABPubSelectListBoxU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,

  SysUtils,Classes,Controls,Forms,StdCtrls,ExtCtrls,DB;

type
  TABSelectListBoxForm = class(TABPubForm)
    ListBox1: TListBox;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//通过ListBox选择数据集aDataset中aFieldName字段的值，返回在变量aText中
function ABSelectListBox(aDataset:Tdataset;aFieldName:string; var aText: string;ACaption: string): boolean;overload;
//通过ListBox选择aStrings中的项，返回在变量aText中
function ABSelectListBox(aStrings:Tstrings; var aText: string;ACaption: string): boolean;overload;

implementation

{$R *.dfm}
function ABSelectListBox(aStrings:Tstrings; var aText: string;ACaption: string): boolean;
var
  tempForm:TABSelectListBoxForm;
begin
  result:=False;
  tempForm := TABSelectListBoxForm.Create(nil);
  if aCaption<>EmptyStr then
    tempForm.Caption:=aCaption;
  try
    tempForm.ListBox1.Clear;
    tempForm.ListBox1.Items.Assign(aStrings);

    if (tempForm.ShowModal=mrok) and (tempForm.ListBox1.ItemIndex>=0) then
    begin
      aText:= tempForm.ListBox1.Items[tempForm.ListBox1.ItemIndex];
      result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

function ABSelectListBox(aDataset:Tdataset;aFieldName:string; var aText: string;ACaption: string): boolean;
var
  tempForm:TABSelectListBoxForm;
begin
  result:=False;
  tempForm := TABSelectListBoxForm.Create(nil);
  if aCaption<>EmptyStr then
    tempForm.Caption:=aCaption;
  aDataset.DisableControls;
  try
    tempForm.ListBox1.Clear;
    aDataset.First;
    while not aDataset.Eof do
    begin
      tempForm.ListBox1.Items.Add(aDataset.FindField(aFieldName).AsString);
      aDataset.Next;
    end;

    if (tempForm.ShowModal=mrok) and (tempForm.ListBox1.ItemIndex>=0) then
    begin
      aText:= tempForm.ListBox1.Items[tempForm.ListBox1.ItemIndex];
      result:=true;
    end;
  finally
    aDataset.EnableControls;
    tempForm.Free;
  end;
end;

procedure TABSelectListBoxForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABSelectListBoxForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrNo;
end;


end.



