{
启动窗体单元
}
unit ABPubStartFormU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubVarU,

  SysUtils,Classes,Forms,ExtCtrls,Controls;

type
  TABStartFormForm = class(TForm)
    Image1: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//显示启动窗体
function ABShowStartForm: boolean;
//关闭启动窗体
function ABCloseStartForm: boolean;


implementation

{$R *.dfm}

var
  ABPubStartForm: TABStartFormForm;

function ABShowStartForm: boolean;
  procedure CreatePubStartForm;
  var
    tempFileName:string;
  begin
    if (not Assigned(ABPubStartForm)) then
    begin
      tempFileName:=EmptyStr;
      if FileExists(ABSoftSetPath+'StartForm.jpg') then
      begin
        tempFileName:=ABSoftSetPath+'StartForm.jpg';
      end
      else  if FileExists(ABSoftSetPath+'StartForm.jpeg') then
      begin
        tempFileName:=ABSoftSetPath+'StartForm.jpeg';
      end
      else  if FileExists(ABSoftSetPath+'StartForm.bmp') then
      begin
        tempFileName:=ABSoftSetPath+'StartForm.bmp';
      end;

      if tempFileName<>EmptyStr then
      begin
        ABPubStartForm := TABStartFormForm.Create(nil);
        ABPubStartForm.Image1.Picture.LoadFromFile(ABSoftSetPath + 'StartForm.jpg');
      end;
    end;
  end;
begin
  result:=false;
  CreatePubStartForm;
  if Assigned(ABPubStartForm) then
  begin
    ABPubStartForm.Show;
    ABPubStartForm.Position:=poScreenCenter;

    //如果没有此名则显示会有拖后
    ABPubStartForm.update;

    result:=True;
  end;
end;

function ABCloseStartForm: boolean;
begin
  result:=false;
  if Assigned(ABPubStartForm) then
  begin
    ABPubStartForm.Close;
    ABPubStartForm.free;
    ABPubStartForm:=nil;
    result:=True;
  end;
end;

procedure ABFinalization;
begin
  if Assigned(ABPubStartForm) then
  begin
    ABPubStartForm.Free;
  end;
end;

Initialization

Finalization
  ABFinalization;

end.
