{
变量定义单元
}
unit ABPubVarU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubConstU,

  Classes,forms,SysUtils,DB;


var
  ABAppPath,                          //当前应用程序的路径(包括"/")
  ABAppDir,                           //当前应用程序的路径(不包括"/")
  ABAppName,                          //当前应用程序的名称(不包括路径和扩展名)
  ABAppFullName,                      //当前应用程序的全名称(包括路径和扩展名)

  ABPublicSetPath,                    //共用程序设置路径=ABAppPath + ABSoftName + '_Set\Public\';
  ABSoftSetPath,                      //当前程序设置路径=ABAppPath + ABSoftName + '_Set\'+ABAppName+'\';
  ABDefaultUserPath,                  //默认用户路径    =ABSoftSetPath + 'AllUser\Default\';
  ABConfigPath: string;               //配制路径        =ABSoftSetPath + 'Config\';
  ABDefaultValuePath: string;         //默认值的路径    =ABSoftSetPath + 'DefaultValue\';
  ABLocalTablePath:string;            //本地缓存表路径  =ABSoftSetPath + 'SaveLocalTable\';
  ABBackPath:string;                  //备份路径        =ABSoftSetPath + 'Back\';
  ABLanguagesPath:string;             //语言文件路径    =ABSoftSetPath + 'Languages\';
  ABCurUserPath,                      //当前用户路径    =ABSoftSetPath + 'AllUser\';
  ABOtherPath,                        //其它文件路径    =ABSoftSetPath + 'Other\';
  ABTempPath,                         //临时文件路径    =ABSoftSetPath + 'temp\';

  ABLogFile:string;                   //日志文件名      =ABSoftSetPath + 'Log.txt';
  ABExceptLogFile:string;             //异常日志文件名  =ABSoftSetPath + 'ExceptLog.txt';
  ABUserFile: string;                 //用户信息文件名  =ABSoftSetPath + 'User.ini';
  ABLocalVersionsFile: string;        //本地版本文件名  =ABSoftSetPath + 'LocalVersions.ini';
  ABLocalParamsFile : string;         //本地参数文件名  =ABSoftSetPath + 'LocalParamsFile.ini';

  ABStrDataType:TFieldTypes;        //字串的字段类型
  ABMemoDataType:TFieldTypes;       //备注的字段类型
  ABIntDataType:TFieldTypes;        //整数的字段类型
  ABDecimalDataType:TFieldTypes;    //小数的字段类型
  ABIntAndDecimalDataType:TFieldTypes;        //整数和小数的字段类型
  ABBlobDataType:TFieldTypes;       //二进制的字段类型
  ABStrAndMemoDataType:TFieldTypes; //字串和备注字段类型
  ABInDelphi:Boolean;                 //当前是否是在DELPHI开发环境中运行
  ABPubFuncPrintNames: TStrings;      //功能默认的打印机名称,格式为多行的“功能名=打印机名”

  ABPubParamDataset:TDataset;          //SQL或scrip中可自动替换参数的参数数据集
  ABPubParamNameFieldName,             //参数数据集参数名字段，此字段的值可以[ABParams_此字段值]的格式出现在SQL或scrip中
  ABPubParamValueFieldName:string;     //在SQL或scrip中[ABParams_此字段值]的格式替换成此字段的值

  ABPubReplaceSQLDataset:TDataset;     //替换数据集中的子SQL数据集,格式为
  ABPubReplaceSQLNameFieldName,        //子SQL数据集子SQL名字段，此字段的值可以[ABPublicSQL_此字段值]的格式出现在SQL中
  ABPubReplaceSQLValueFieldName:string;//在SQL中[ABPublicSQL_此字段值]的格式替换成此字段的值

  ABPubReplaceSQLStrings:TStrings;     //替换数据集中TStrings的名称为值(如可包含连接名到数据库名的转换)

  ABConns:array of TABConninfo;         //数据库连接列表
  ABConnFileName:string;                //数据库连接文件名

implementation

procedure ABInitialization;
  procedure ABInitVar;
  begin
    ABAppFullName :=Application.EXEName;
    ABAppName     :=ChangeFileExt(extractfilename(ABAppFullName), '');
    ABInDelphi    :=(AnsiCompareText(ABAppName,'BDS')=0) or
                    (AnsiCompareText(ABAppName,'Delphi32')=0) or
                    (DebugHook<>0);

    ABAppPath                := ExtractFilePath(ABAppFullName);
    ABAppDir                 := ExtractFileDir(ABAppFullName);

    ABPublicSetPath          := ABAppPath + ABSoftName + '_Set\Public\';
    ABSoftSetPath            := ABAppPath + ABSoftName + '_Set\'+ABAppName+'\';
    ABDefaultUserPath        := ABSoftSetPath + 'AllUser\Default\';
    ABConfigPath             := ABSoftSetPath + 'Config\';
    ABDefaultValuePath       := ABSoftSetPath + 'DefaultValue\';
    ABLocalTablePath         := ABSoftSetPath + 'LocalTable\';
    ABBackPath               := ABSoftSetPath + 'Back\';
    ABLanguagesPath          := ABSoftSetPath + 'Languages\';
    ABCurUserPath            := ABSoftSetPath + 'AllUser\';
    ABOtherPath              := ABSoftSetPath + 'Other\';
    ABTempPath               := ABSoftSetPath + 'Temp\';

    ABLogFile                := ABSoftSetPath + 'Log.txt';
    ABExceptLogFile          := ABSoftSetPath + 'ExceptLog.txt';
    ABUserFile               := ABSoftSetPath + 'User.ini';
    ABLocalVersionsFile      := ABSoftSetPath + 'LocalVersions.ini';
    ABLocalParamsFile        := ABSoftSetPath + 'LocalParams.ini';

    if not DirectoryExists(ABPublicSetPath) then
      ForceDirectories(ABPublicSetPath);
    if not DirectoryExists(ABSoftSetPath) then
      ForceDirectories(ABSoftSetPath);
    if not DirectoryExists(ABDefaultUserPath) then
      ForceDirectories(ABDefaultUserPath);
    if not DirectoryExists(ABConfigPath) then
      ForceDirectories(ABConfigPath);
    if not DirectoryExists(ABDefaultValuePath) then
      ForceDirectories(ABDefaultValuePath);
    if not DirectoryExists(ABLocalTablePath) then
      ForceDirectories(ABLocalTablePath);
    if not DirectoryExists(ABBackPath) then
      ForceDirectories(ABBackPath);
    if not DirectoryExists(ABLanguagesPath) then
      ForceDirectories(ABLanguagesPath);
    if not DirectoryExists(ABCurUserPath) then
      ForceDirectories(ABCurUserPath);
    if not DirectoryExists(ABOtherPath) then
      ForceDirectories(ABOtherPath);
    if not DirectoryExists(ABTempPath) then
      ForceDirectories(ABTempPath);

    ABStrDataType := [ftString, ftGuid,ftFixedChar, ftwideString];
    ABIntDataType:= [ftSmallint, ftInteger,ftLargeint];
    ABDecimalDataType:= [ftFloat, ftCurrency, ftBCD, ftWord,ftFMTBcd];

    ABIntAndDecimalDataType := [ftSmallint, ftInteger, ftFloat, ftCurrency, ftBCD, ftWord,ftLargeint,ftFMTBcd];
    ABMemoDataType := [ftMemo, ftFmtMemo{$IF (CompilerVersion>=18.5)} ,ftWideMemo {$IFEND}];
    ABBlobDataType := [ftBlob, ftGraphic, ftParadoxOle, ftDBaseOle, ftTypedBinary];

    ABStrAndMemoDataType := [ftMemo, ftFmtMemo,{$IF (CompilerVersion>=18.5)} ftWideMemo, {$IFEND}
                             ftString, ftGuid,ftFixedChar, ftwideString];
  end;
begin
  //是否在程序结束时显示存在的内存泄漏
  //ReportMemoryLeaksOnShutdown:= true;

  Application.HintHidePause:=600000;//延长停留时间为10秒

  ABInitVar;
  ABPubParamDataset:=nil;
  ABPubParamNameFieldName:=EmptyStr;
  ABPubParamValueFieldName:=EmptyStr;

  ABPubReplaceSQLDataset:=nil;
  ABPubReplaceSQLNameFieldName:=EmptyStr;
  ABPubReplaceSQLValueFieldName:=EmptyStr;

  ABPubReplaceSQLStrings:= TStringList.Create;
  ABPubFuncPrintNames:= TStringList.Create;
  if FileExists(ABDefaultValuePath+'FuncPrintName.ini') then
    ABPubFuncPrintNames.LoadFromFile(ABDefaultValuePath+'FuncPrintName.ini');

  ABConnFileName:=ABPublicSetPath+'Setup.ini';
end;

procedure ABFinalization;
begin
  ABPubFuncPrintNames.SaveToFile(ABDefaultValuePath+'FuncPrintName.ini');
  ABPubFuncPrintNames.Free;
  ABPubReplaceSQLStrings.Free;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.

























































