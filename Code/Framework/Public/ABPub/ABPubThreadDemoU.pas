{
多线程及示例单元
}
unit ABPubThreadDemoU;

interface
{$I ..\ABInclude.ini}

uses

  ABPubFormU,
  ABPubVarU,
  ABPubThreadU,

  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, StdCtrls, ExtCtrls, Spin;

type
  TABThread = class(TThread)
  private
    Findex:LongInt;
    //此过程由Synchronize调用，用来实现多个线程同时访问VCL更新
    procedure UpdateVCL;
  { Private declarations }
  protected
    constructor Create(CreateSuspended: Boolean;aindex:LongInt); overload;
    procedure Execute; override;
  end;

  TABThreadForm=class(TABPubForm)
    PageControl2: TPageControl;
    TabSheet7: TTabSheet;
    Memo2: TMemo;
    TabSheet8: TTabSheet;
    Label1111: TLabel;
    Label2222: TLabel;
    Image1: TImage;
    Button2: TButton;
    SpinEdit_Count: TSpinEdit;
    SpinEdit_Max: TSpinEdit;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button1: TButton;
    ListBox1: TListBox;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    ListBox2: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    Threads:array[1..10] of TABThread;

    APIThreadExit:boolean;
    APIThreads:array[1..10] of THandle;
    APIThreadIDs:array[1..10] of DWORD;
    { Private declarations }
  public
    procedure UpdateVcl;
    { Public declarations }
  end;


procedure ABShowThreadDemoForm;

implementation

{$R *.dfm}
var
  FABThreadForm: TABThreadForm;


procedure ABShowThreadDemoForm;
begin
  if not Assigned(FABThreadForm) then
    FABThreadForm := TABThreadForm.Create(nil);

  FABThreadForm.ShowModal;
end;

 {TTestThread}

constructor TABThread.Create(CreateSuspended: Boolean; aindex: Integer);
begin
  inherited Create(CreateSuspended);
  Findex:=aindex;
end;

procedure TABThread.Execute;
  procedure DoItem;
  begin
    FABThreadForm.ListBox1.Items.Add(IntToStr(FABThreadForm.ListBox1.Items.Count));
  end;
var
  i:LongInt;
  tempHandle:THandle;
begin
  if (FABThreadForm.RadioGroup2.ItemIndex=0) or
     (FABThreadForm.RadioGroup2.ItemIndex=1) or
     (FABThreadForm.RadioGroup2.ItemIndex=2) or
     (FABThreadForm.RadioGroup2.ItemIndex=3)
     then
  begin
    tempHandle:=ABGetMutex(ABAppFullName+'_insideCheck');
    for I := 1 to FABThreadForm.SpinEdit_Max.value do
    begin
      if Terminated then
       break;

      case FABThreadForm.RadioGroup2.ItemIndex of
        0: //线程循环直接调用
        begin
          DoItem;
        end;
        1: //Synchronize(主线程)
        begin
          Synchronize(UpdateVCL)
        end;
        2: //CriticalSection(临界区)
        begin
          EnterCriticalSection(ABPubCriticalSection);
          try
            DoItem;
          finally
            LeaveCriticalSection(ABPubCriticalSection);
          end;
        end;
        3: //Mutex (互斥对象)
        begin
          if WaitForSingleObject(tempHandle, INFINITE) = WAIT_OBJECT_0 then
          begin
            DoItem;
            ReleaseMutex(tempHandle);
          end;
        end;
      end;
    end;
  end
  else if (FABThreadForm.RadioGroup2.ItemIndex=4)
          then
  begin
    if WaitForSingleObject(ABPubSemaphore , INFINITE) = WAIT_OBJECT_0 then
    begin
      for I := 1 to FABThreadForm.SpinEdit_Max.value do
      begin
        if Terminated then
         break;
        //在Image1上输出数字，因为使用Lock，Unlock，所以此时不会造成多线程的冲突
        FABThreadForm.Image1.Canvas.Lock;
        FABThreadForm.Image1.Canvas.TextOut(10, 30*Findex, IntToStr(i));
        FABThreadForm.Image1.Canvas.Unlock;
        Sleep(0); //稍稍耽搁一点, 不然有时 Canvas 会协调不过来
      end;
    end;
    ReleaseSemaphore(ABPubSemaphore, 1, nil);
  end;
end;

function MyFun(p: Pointer): Integer; stdcall;
  procedure DoItem;
  begin
    FABThreadForm.ListBox1.Items.Add(IntToStr(FABThreadForm.ListBox1.Items.Count));
  end;
var
  tempMethod: TMethod;
  i,j:LongInt;
  tempHandle:THandle;
begin
  j:=Integer(p);
  if (FABThreadForm.RadioGroup2.ItemIndex=0) or
     (FABThreadForm.RadioGroup2.ItemIndex=1) or
     (FABThreadForm.RadioGroup2.ItemIndex=2) or
     (FABThreadForm.RadioGroup2.ItemIndex=3)
     then
  begin
    tempHandle:=ABGetMutex(ABAppFullName+'_insideCheck');
    tempMethod.Data := FABThreadForm;
    tempMethod.Code := @TABThreadForm.UpdateVcl;
    for I := 1 to FABThreadForm.SpinEdit_Max.value do
    begin
      if FABThreadForm.APIThreadExit then
        break;

      case FABThreadForm.RadioGroup2.ItemIndex of
        0: //线程循环直接调用
        begin
          DoItem;
        end;
        1: //Synchronize(主线程)
        begin
          TThread.Synchronize(nil, TThreadMethod(tempMethod));
        end;
        2: //CriticalSection(临界区)
        begin
          EnterCriticalSection(ABPubCriticalSection);
          try
            DoItem;
          finally
            LeaveCriticalSection(ABPubCriticalSection);
          end;
        end;
        3: //Mutex (互斥对象)
        begin
          if WaitForSingleObject(tempHandle, INFINITE) = WAIT_OBJECT_0 then
          begin
            DoItem;
            ReleaseMutex(tempHandle);
          end;
        end;
      end;
    end;
  end
  else if (FABThreadForm.RadioGroup2.ItemIndex=4)
          then
  begin
    if WaitForSingleObject(ABPubSemaphore , INFINITE) = WAIT_OBJECT_0 then
    begin
      for I := 1 to FABThreadForm.SpinEdit_Max.value do
      begin
        if FABThreadForm.APIThreadExit then
          break;
        //在Image1上输出数字，因为使用Lock，Unlock，所以此时不会造成多线程的冲突
        FABThreadForm.Image1.Canvas.Lock;
        FABThreadForm.Image1.Canvas.TextOut(10, 30*j, IntToStr(i));
        FABThreadForm.Image1.Canvas.Unlock;
        Sleep(0); //稍稍耽搁一点, 不然有时 Canvas 会协调不过来
      end;
    end;
    ReleaseSemaphore(ABPubSemaphore, 1, nil);
  end;

  Result := 0;
end;

procedure TABThread.UpdateVCL;
begin
  FABThreadForm.UpdateVcl;
end;

procedure TABThreadForm.UpdateVcl;
begin
  FABThreadForm.ListBox1.Items.Add(IntToStr(FABThreadForm.ListBox1.Items.Count));
end;

procedure TABThreadForm.Button3Click(Sender: TObject);
var
  i:LongInt;
  tempExitCode: DWORD;
begin
  case RadioGroup1.ItemIndex of
    0:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if Assigned(Threads[i]) then
        begin
          if GetExitCodeThread(Threads[i].Handle, tempExitCode) then
          begin
            case tempExitCode of
              0:
              begin
                ListBox2.Items.Add(Format('线程[%d]还未启动',[i]));
              end;
              STILL_ACTIVE:
              begin
                ListBox2.Items.Add(Format('线程[%d]正在运行，退出代码是[%d]',[i,tempExitCode]));
              end;
              else
              begin
                ListBox2.Items.Add(Format('线程[%d]已退出',[i]));
              end;
            end;
          end
          else
          begin
            ListBox2.Items.Add(Format('线程[%d]已退出',[i]));
          end;
        end
        else
        begin
          ListBox2.Items.Add(Format('线程[%d]不存在',[i]));
        end;
      end;
    end;
    1:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if APIThreads[i]<>0 then
        begin
          if GetExitCodeThread(APIThreads[i], tempExitCode) then
          begin
            case tempExitCode of
              0:
              begin
                ListBox2.Items.Add(Format('线程[%d]还未启动',[i]));
              end;
              STILL_ACTIVE:
              begin
                ListBox2.Items.Add(Format('线程[%d]正在运行，退出代码是[%d]',[i,tempExitCode]));
              end;
              else
              begin
                ListBox2.Items.Add(Format('线程[%d]已退出',[i]));
              end;
            end;
          end;
        end
        else
        begin
          ListBox2.Items.Add(Format('线程[%d]不存在',[i]));
        end;
      end;
    end;
  end;
end;

procedure TABThreadForm.Button4Click(Sender: TObject);
var
  i:LongInt;
begin
  case RadioGroup1.ItemIndex of
    0:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if Assigned(Threads[i]) then
        begin
          Threads[i].Suspend;
        end;
      end;
    end;
    1:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if APIThreads[i]<>0 then
        begin
          SuspendThread(APIThreads[i]);
        end;
      end;
    end;
  end;
end;

procedure TABThreadForm.Button5Click(Sender: TObject);
var
  i:LongInt;
begin
  case RadioGroup1.ItemIndex of
    0:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if Assigned(Threads[i]) then
        begin
          Threads[i].Resume;
        end;
      end;
    end;
    1:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if APIThreads[i]<>0 then
        begin
          ResumeThread(APIThreads[i]);
        end;
      end;
    end;
  end;
end;

procedure TABThreadForm.FormCreate(Sender: TObject);
var
  i:LongInt;
begin
  ListBox1.Clear;
  for I := 1 to SpinEdit_Count.value do
  begin
    APIThreads[i] := 0;
    Threads[i]:=nil;
  end;
end;

procedure TABThreadForm.FormDestroy(Sender: TObject);
var
  i:LongInt;
begin
  case RadioGroup1.ItemIndex of
    0:
    begin
    end;
    1:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if APIThreads[i]<>0 then
        begin
          CloseHandle(APIThreads[i]);
        end;
      end;
    end;
  end;

end;

procedure TABThreadForm.Button1Click(Sender: TObject);
var
  i:LongInt;
begin
  case RadioGroup1.ItemIndex of
    0:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if Assigned(Threads[i]) then
        begin
          Threads[i].Terminate;
        end;
      end;
    end;
    1:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        if APIThreads[i]<>0 then
        begin
          APIThreadExit:=true;
        end;
      end;
    end;
  end;
end;

procedure TABThreadForm.Button2Click(Sender: TObject);
var
  i:LongInt;
begin
  CloseHandle(ABPubSemaphore);
  //创建总数是5，初始是3的信号
  ABPubSemaphore := CreateSemaphore(nil, SpinEdit2.Value, SpinEdit1.Value, nil);

  Image1.Repaint;
  ListBox1.Clear;
  ListBox2.Clear;
  case RadioGroup1.ItemIndex of
    0:
    begin
      for I := 1 to SpinEdit_Count.value do
      begin
        Threads[i] := TABThread.Create(true,i);
        Threads[i].FreeOnTerminate:=true;
        //唤醒线程
        Threads[i].Resume;
      end;
    end;
    1:
    begin
      APIThreadExit:=false;
      for I := 1 to SpinEdit_Count.value do
      begin
        if APIThreads[i]<>0 then
        begin
          CloseHandle(APIThreads[i]);
        end;
        APIThreads[i] := CreateThread(nil, 0, @MyFun, Pointer(i), CREATE_SUSPENDED, APIThreadIDs[i]);
        //唤醒线程
        ResumeThread(APIThreads[i]);
      end;
    end;
  end;
end;



procedure ABFinalization;
begin
  if Assigned(FABThreadForm) then
    FABThreadForm.Free;
end;

Initialization

Finalization
  ABFinalization;


end.

