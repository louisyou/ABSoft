object ABRegisterForm: TABRegisterForm
  Left = 498
  Top = 167
  Caption = #21152#23494#29399#31649#29702
  ClientHeight = 550
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel10: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 550
    Align = alClient
    TabOrder = 0
    object PageControl2: TPageControl
      Tag = 91
      Left = 1
      Top = 161
      Width = 798
      Height = 354
      ActivePage = TabSheet3
      Align = alBottom
      TabOrder = 0
      object TabSheet3: TTabSheet
        Caption = #21152#23494#29399#26435#38480
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 790
          Height = 326
          Align = alClient
          TabOrder = 0
          object Label10: TLabel
            Left = 5
            Top = 151
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #21345#25968#37327
          end
          object Label17: TLabel
            Left = 152
            Top = 151
            Width = 65
            Height = 13
            AutoSize = False
            Caption = #25511#21046#22120#25968#37327
          end
          object Label18: TLabel
            Left = 5
            Top = 109
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #28040#36153#26426#25968#37327
          end
          object Label19: TLabel
            Left = 5
            Top = 89
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #35835#21345#22120#25968#37327
          end
          object Label2: TLabel
            Left = 5
            Top = 8
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #23458#25143#21517#31216
          end
          object Label12: TLabel
            Left = 5
            Top = 27
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #24320#22987#20351#29992#26102#38388
          end
          object Label13: TLabel
            Left = 5
            Top = 47
            Width = 72
            Height = 13
            Caption = #32467#26463#20351#29992#26102#38388
          end
          object Label14: TLabel
            Left = 5
            Top = 130
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #24182#21457#23458#25143#31471#25968
          end
          object Label7: TLabel
            Left = 5
            Top = 171
            Width = 76
            Height = 13
            AutoSize = False
            Caption = 'V100'#25968#37327
          end
          object Label8: TLabel
            Left = 152
            Top = 171
            Width = 65
            Height = 13
            AutoSize = False
            Caption = 'V200'#25968#37327
          end
          object Label11: TLabel
            Left = 5
            Top = 191
            Width = 76
            Height = 13
            AutoSize = False
            Caption = 'V300'#25968#37327
          end
          object SpeedButton7: TSpeedButton
            Left = 131
            Top = 296
            Width = 118
            Height = 25
            Caption = #20889#21152#23494#29399
            OnClick = SpeedButton7Click
          end
          object SpeedButton5: TSpeedButton
            Left = 13
            Top = 296
            Width = 118
            Height = 25
            Caption = #35835#21152#23494#29399
            OnClick = SpeedButton5Click
          end
          object Label3: TLabel
            Left = 5
            Top = 68
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #20215#26684
          end
          object lbl3: TLabel
            Left = 5
            Top = 212
            Width = 76
            Height = 13
            AutoSize = False
            Caption = #22791#27880
          end
          object cxDBCalcEdit2: TcxDBCalcEdit
            Left = 80
            Top = 148
            DataBinding.DataField = 'CardCount'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 0
            Width = 66
          end
          object cxDBCalcEdit5: TcxDBCalcEdit
            Left = 216
            Top = 148
            DataBinding.DataField = 'ControlCount'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 1
            Width = 66
          end
          object cxDBCalcEdit6: TcxDBCalcEdit
            Left = 80
            Top = 106
            DataBinding.DataField = 'TerminalCount'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 2
            Width = 66
          end
          object cxDBCalcEdit7: TcxDBCalcEdit
            Left = 80
            Top = 86
            DataBinding.DataField = 'ReaderCount'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 3
            Width = 66
          end
          object cxDBDateEdit3: TcxDBDateEdit
            Left = 80
            Top = 24
            DataBinding.DataField = 'BegDatetime'
            DataBinding.DataSource = DataSource1_2
            Properties.Kind = ckDateTime
            TabOrder = 4
            Width = 202
          end
          object cxDBDateEdit4: TcxDBDateEdit
            Left = 80
            Top = 44
            DataBinding.DataField = 'EndDatetime'
            DataBinding.DataSource = DataSource1_2
            Properties.Kind = ckDateTime
            TabOrder = 5
            Width = 202
          end
          object cxDBCalcEdit3: TcxDBCalcEdit
            Left = 80
            Top = 127
            DataBinding.DataField = 'SubsequentClientCount'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 6
            Width = 66
          end
          object cxDBTextEdit3: TcxDBTextEdit
            Left = 80
            Top = 4
            DataBinding.DataField = 'RegName'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 7
            Width = 202
          end
          object Panel5: TPanel
            Left = 296
            Top = 6
            Width = 433
            Height = 315
            BevelOuter = bvNone
            TabOrder = 8
            object Panel6: TPanel
              Left = 0
              Top = 285
              Width = 433
              Height = 30
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object Button1: TButton
                Left = 326
                Top = 5
                Width = 105
                Height = 25
                Caption = #20174#25968#25454#24211#21152#36733
                TabOrder = 0
                OnClick = Button1Click
              end
              object Button3: TButton
                Left = 0
                Top = 5
                Width = 70
                Height = 25
                Caption = #22686#21152
                TabOrder = 1
                OnClick = Button3Click
              end
              object Button4: TButton
                Left = 69
                Top = 5
                Width = 70
                Height = 25
                Caption = #21024#38500
                TabOrder = 2
                OnClick = Button4Click
              end
              object Button5: TButton
                Left = 218
                Top = 5
                Width = 105
                Height = 25
                Caption = #35774#20026#26681#32467#28857
                TabOrder = 3
                OnClick = Button5Click
              end
              object Button11: TButton
                Left = 138
                Top = 5
                Width = 70
                Height = 25
                Caption = #20462#25913
                TabOrder = 4
                OnClick = Button11Click
              end
            end
            object ABCheckTreeView1: TABCheckTreeView
              Left = 0
              Top = 0
              Width = 433
              Height = 285
              MoveAfirm = False
              Align = alClient
              AutoExpand = True
              Flatness = cfAlwaysFlat
              GrayedIsChecked = False
              HideSelection = False
              Indent = 19
              PopupMenu = PopupMenu1
              ReadOnly = True
              TabOrder = 1
              OnClick = ABCheckTreeView1Click
              OnEndDrag = ABCheckTreeView1EndDrag
            end
          end
          object cxDBCalcEdit1: TcxDBCalcEdit
            Left = 80
            Top = 168
            DataBinding.DataField = 'V100Count'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 9
            Width = 66
          end
          object cxDBCalcEdit4: TcxDBCalcEdit
            Left = 216
            Top = 168
            DataBinding.DataField = 'V200Count'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 10
            Width = 66
          end
          object cxDBCalcEdit8: TcxDBCalcEdit
            Left = 80
            Top = 188
            DataBinding.DataField = 'V300Count'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 11
            Width = 66
          end
          object cxDBCalcEdit9: TcxDBCalcEdit
            Left = 80
            Top = 65
            DataBinding.DataField = 'Price'
            DataBinding.DataSource = DataSource1_2
            TabOrder = 12
            Width = 202
          end
          object cxDBMemo1: TcxDBMemo
            Left = 5
            Top = 231
            DataBinding.DataField = 'Remark'
            DataBinding.DataSource = DataSource1_2
            Properties.ScrollBars = ssBoth
            TabOrder = 13
            Height = 60
            Width = 277
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = #21152#23494#29399#21015#34920
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label1: TLabel
          Left = 0
          Top = 206
          Width = 790
          Height = 15
          Align = alBottom
          Alignment = taCenter
          AutoSize = False
          Caption = #25805#20316#26085#24535
          ExplicitTop = 219
          ExplicitWidth = 752
        end
        object Splitter3: TSplitter
          Left = 0
          Top = 203
          Width = 790
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = -24
          ExplicitTop = 177
          ExplicitWidth = 714
        end
        object cxGrid2: TcxGrid
          Left = 0
          Top = 221
          Width = 790
          Height = 105
          Align = alBottom
          TabOrder = 0
          object cxGridDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.First.Visible = False
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.Prior.Visible = False
            Navigator.Buttons.Next.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Last.Visible = False
            Navigator.Buttons.Insert.Visible = True
            Navigator.Buttons.Append.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            DataController.DataSource = DataSource1_21
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.DataRowSizing = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object cxGridDBTableView1Datetime: TcxGridDBColumn
              DataBinding.FieldName = 'Datetime'
              PropertiesClassName = 'TcxDateEditProperties'
              Properties.Kind = ckDateTime
              Width = 120
            end
            object cxGridDBTableView1Remark: TcxGridDBColumn
              DataBinding.FieldName = 'Remark'
              Width = 580
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGridDBTableView1
          end
        end
        object cxGrid3: TcxGrid
          Left = 0
          Top = 0
          Width = 790
          Height = 203
          Align = alClient
          TabOrder = 1
          object cxGridDBTableView2: TcxGridDBTableView
            PopupMenu = pm1
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.First.Visible = False
            Navigator.Buttons.PriorPage.Visible = False
            Navigator.Buttons.Prior.Visible = False
            Navigator.Buttons.Next.Visible = False
            Navigator.Buttons.NextPage.Visible = False
            Navigator.Buttons.Last.Visible = False
            Navigator.Buttons.Insert.Visible = True
            Navigator.Buttons.Append.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            OnFocusedRecordChanged = cxGridDBTableView2FocusedRecordChanged
            DataController.DataSource = DataSource1_2
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object cxGridDBTableView2ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Width = 23
            end
            object cxGridDBTableView2RegName: TcxGridDBColumn
              DataBinding.FieldName = 'RegName'
              Width = 152
            end
            object cxGridDBTableView2BegDatetime: TcxGridDBColumn
              DataBinding.FieldName = 'BegDatetime'
            end
            object cxGridDBTableView2EndDatetime: TcxGridDBColumn
              DataBinding.FieldName = 'EndDatetime'
            end
            object cxGridDBTableView2SubsequentClientCount: TcxGridDBColumn
              DataBinding.FieldName = 'SubsequentClientCount'
              Width = 83
            end
            object cxGridDBTableView2CardCount: TcxGridDBColumn
              DataBinding.FieldName = 'CardCount'
              Width = 51
            end
            object cxGridDBTableView2ControlCount: TcxGridDBColumn
              DataBinding.FieldName = 'ControlCount'
              Width = 67
            end
            object cxGridDBTableView2V100Count: TcxGridDBColumn
              DataBinding.FieldName = 'V100Count'
              Width = 59
            end
            object cxGridDBTableView2V200Count: TcxGridDBColumn
              DataBinding.FieldName = 'V200Count'
              Width = 60
            end
            object cxGridDBTableView2V300Count: TcxGridDBColumn
              DataBinding.FieldName = 'V300Count'
              Width = 58
            end
            object cxGridDBTableView2ReaderCount: TcxGridDBColumn
              DataBinding.FieldName = 'ReaderCount'
              Width = 67
            end
            object cxGridDBTableView2TerminalCount: TcxGridDBColumn
              DataBinding.FieldName = 'TerminalCount'
              Width = 68
            end
            object cxGridDBTableView2Count: TcxGridDBColumn
              DataBinding.FieldName = 'Count'
            end
            object cxGridDBTableView2FuncRight: TcxGridDBColumn
              DataBinding.FieldName = 'FuncRight'
              Width = 171
            end
            object cxGridDBTableView2Column1: TcxGridDBColumn
              DataBinding.FieldName = 'Price'
            end
            object cxGridDBTableView2Column2: TcxGridDBColumn
              DataBinding.FieldName = 'Remark'
              Width = 225
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBTableView2
          end
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 798
      Height = 160
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Splitter1: TSplitter
        Left = 492
        Top = 17
        Height = 143
        Align = alRight
        ExplicitLeft = 6
        ExplicitTop = -6
        ExplicitHeight = 113
      end
      object Panel8: TPanel
        Left = 495
        Top = 17
        Width = 201
        Height = 143
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 0
          Top = 0
          Width = 201
          Height = 13
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #33756#21333
          ExplicitLeft = -89
          ExplicitTop = 75
          ExplicitWidth = 230
        end
        object ABCheckTreeView2: TABCheckTreeView
          Left = 0
          Top = 13
          Width = 201
          Height = 130
          MoveAfirm = False
          Align = alClient
          AutoExpand = True
          Flatness = cfAlwaysFlat
          GrayedIsChecked = False
          HideSelection = False
          Indent = 19
          PopupMenu = PopupMenu1
          ReadOnly = True
          TabOrder = 0
          OnClick = ABCheckTreeView2Click
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 798
        Height = 17
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label20: TLabel
          Left = 140
          Top = 0
          Width = 658
          Height = 17
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = #26435#38480#27169#26495
          ExplicitLeft = 146
          ExplicitTop = -6
          ExplicitWidth = 582
        end
        object Button7: TButton
          Left = 0
          Top = 0
          Width = 70
          Height = 17
          Align = alLeft
          Caption = #22686#21152
          TabOrder = 0
          OnClick = Button7Click
        end
        object Button8: TButton
          Left = 70
          Top = 0
          Width = 70
          Height = 17
          Align = alLeft
          Caption = #21024#38500
          TabOrder = 1
          OnClick = Button8Click
        end
      end
      object cxGrid1: TcxGrid
        Left = 0
        Top = 17
        Width = 492
        Height = 143
        Align = alClient
        TabOrder = 2
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Visible = False
          Navigator.Buttons.PriorPage.Visible = False
          Navigator.Buttons.Prior.Visible = False
          Navigator.Buttons.Next.Visible = False
          Navigator.Buttons.NextPage.Visible = False
          Navigator.Buttons.Last.Visible = False
          Navigator.Buttons.Insert.Visible = True
          Navigator.Buttons.Append.Visible = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Refresh.Visible = False
          Navigator.Buttons.SaveBookmark.Visible = False
          Navigator.Buttons.GotoBookmark.Visible = False
          Navigator.Buttons.Filter.Visible = False
          OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
          DataController.DataSource = DataSource1_1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.DataRowSizing = True
          OptionsSelection.MultiSelect = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Width = 25
          end
          object cxGrid1DBTableView1Name: TcxGridDBColumn
            DataBinding.FieldName = 'Name'
            Width = 113
          end
          object cxGrid1DBTableView1BegDatetime: TcxGridDBColumn
            DataBinding.FieldName = 'BegDatetime'
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.Kind = ckDateTime
          end
          object cxGrid1DBTableView1EndDatetime: TcxGridDBColumn
            DataBinding.FieldName = 'EndDatetime'
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.Kind = ckDateTime
          end
          object cxGrid1DBTableView1SubsequentClientCount: TcxGridDBColumn
            DataBinding.FieldName = 'SubsequentClientCount'
            Width = 80
          end
          object cxGrid1DBTableView1CardCount: TcxGridDBColumn
            DataBinding.FieldName = 'CardCount'
            Width = 47
          end
          object cxGrid1DBTableView1ControlCount: TcxGridDBColumn
            DataBinding.FieldName = 'ControlCount'
            Width = 68
          end
          object cxGrid1DBTableView1V100Count: TcxGridDBColumn
            DataBinding.FieldName = 'V100Count'
            Width = 58
          end
          object cxGrid1DBTableView1V200Count: TcxGridDBColumn
            DataBinding.FieldName = 'V200Count'
            Width = 61
          end
          object cxGrid1DBTableView1V300Count: TcxGridDBColumn
            DataBinding.FieldName = 'V300Count'
            Width = 58
          end
          object cxGrid1DBTableView1ReaderCount: TcxGridDBColumn
            DataBinding.FieldName = 'ReaderCount'
            Width = 67
          end
          object cxGrid1DBTableView1TerminalCount: TcxGridDBColumn
            DataBinding.FieldName = 'TerminalCount'
            Width = 66
          end
          object cxGrid1DBTableView1FuncRight: TcxGridDBColumn
            DataBinding.FieldName = 'FuncRight'
            Width = 106
          end
          object cxGrid1DBTableView1Column1: TcxGridDBColumn
            DataBinding.FieldName = 'Price'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object Panel4: TPanel
        Left = 696
        Top = 17
        Width = 102
        Height = 143
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object Label6: TLabel
          Left = 0
          Top = 0
          Width = 102
          Height = 13
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #22791#27880
          ExplicitLeft = -89
          ExplicitTop = 75
          ExplicitWidth = 230
        end
        object cxDBMemo2: TcxDBMemo
          Left = 0
          Top = 13
          Align = alClient
          DataBinding.DataField = 'Remark'
          DataBinding.DataSource = DataSource1_1
          Properties.ScrollBars = ssBoth
          TabOrder = 0
          Height = 130
          Width = 102
        end
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 515
      Width = 798
      Height = 34
      Align = alBottom
      TabOrder = 2
      object SpeedButton2: TSpeedButton
        Left = 13
        Top = 4
        Width = 130
        Height = 25
        Caption = #20174#27169#26495#29983#25104#26032#29399
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C40E0000C40E00000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C30
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE61
          00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
          0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE61
          0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE6100009C30
          0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
          FF009C300000CE610000CE610000CE6100009C300000CE610000CE610000CE61
          00009C300000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
          FF009C3000009C3000009C3000009C300000CE610000CE610000CE610000CE61
          0000CE6100009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
          0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
          0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
          FF009C300000CE610000CE610000CE610000CE610000CE610000CE610000CE61
          0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
          FF009C3000009C3000009C3000009C300000CE610000CE610000CE610000CE61
          0000CE6100009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
          0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF009C3000009C3000009C3000009C3000009C30
          00009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TSpeedButton
        Left = 149
        Top = 4
        Width = 130
        Height = 25
        Caption = #23558#27169#26495#21152#20837#21040#29399
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000C40E0000C40E00000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C30
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE61
          00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
          0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE61
          0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE6100009C30
          0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
          FF009C300000CE610000CE610000CE6100009C300000CE610000CE610000CE61
          00009C300000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
          FF009C3000009C3000009C3000009C300000CE610000CE610000CE610000CE61
          0000CE6100009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
          0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
          0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
          FF009C300000CE610000CE610000CE610000CE610000CE610000CE610000CE61
          0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
          FF009C3000009C3000009C3000009C300000CE610000CE610000CE610000CE61
          0000CE6100009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
          0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF009C3000009C3000009C3000009C3000009C30
          00009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        OnClick = SpeedButton1Click
      end
    end
  end
  object Panel7: TPanel
    Left = 482
    Top = 247
    Width = 142
    Height = 107
    TabOrder = 1
    Visible = False
    object Button2: TButton
      Left = 301
      Top = 212
      Width = 143
      Height = 79
      Caption = #35835#21152#23494#29399#20869#23481
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object DataSource1_1: TDataSource
    DataSet = ADOQuery1_1
    Left = 168
    Top = 107
  end
  object ADOQuery1_1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    AfterInsert = ADOQuery1_1AfterInsert
    Parameters = <>
    SQL.Strings = (
      'select * from Template order by ID')
    Left = 168
    Top = 64
    object ADOQuery1_1ID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object ADOQuery1_1Name: TWideStringField
      DisplayLabel = #21517#31216
      FieldName = 'Name'
      Size = 250
    end
    object ADOQuery1_1BegDatetime: TDateTimeField
      DisplayLabel = #24320#22987#26102#38388
      FieldName = 'BegDatetime'
    end
    object ADOQuery1_1EndDatetime: TDateTimeField
      DisplayLabel = #32467#26463#26102#38388
      FieldName = 'EndDatetime'
    end
    object ADOQuery1_1SubsequentClientCount: TIntegerField
      DisplayLabel = #24182#21457#23458#25143#31471#25968
      FieldName = 'SubsequentClientCount'
    end
    object ADOQuery1_1ClientInfo: TWideMemoField
      DisplayLabel = #25480#26435#23458#25143#31471
      FieldName = 'ClientInfo'
      BlobType = ftWideMemo
    end
    object ADOQuery1_1CardCount: TIntegerField
      DisplayLabel = #21345#25968#37327
      FieldName = 'CardCount'
    end
    object ADOQuery1_1ControlCount: TIntegerField
      DisplayLabel = #25511#21046#22120#25968#37327
      FieldName = 'ControlCount'
    end
    object ADOQuery1_1V100Count: TIntegerField
      DisplayLabel = 'V100'#25968#37327
      FieldName = 'V100Count'
    end
    object ADOQuery1_1V200Count: TIntegerField
      DisplayLabel = 'V200'#25968#37327
      FieldName = 'V200Count'
    end
    object ADOQuery1_1V300Count: TIntegerField
      DisplayLabel = 'V300'#25968#37327
      FieldName = 'V300Count'
    end
    object ADOQuery1_1ReaderCount: TIntegerField
      DisplayLabel = #35835#21345#22120#25968#37327
      FieldName = 'ReaderCount'
    end
    object ADOQuery1_1TerminalCount: TIntegerField
      DisplayLabel = #28040#36153#26426#25968#37327
      FieldName = 'TerminalCount'
    end
    object ADOQuery1_1FuncRight: TWideMemoField
      DisplayLabel = #33756#21333#26435#38480
      FieldName = 'FuncRight'
      BlobType = ftWideMemo
    end
    object ADOQuery1_1Price: TFloatField
      DisplayLabel = #20215#26684
      FieldName = 'Price'
    end
    object ADOQuery1_1CustomFunc: TWideMemoField
      DisplayLabel = #23450#21046#21151#33021
      FieldName = 'CustomFunc'
      BlobType = ftWideMemo
    end
    object ADOQuery1_1Remark: TWideMemoField
      DisplayLabel = #22791#27880
      FieldName = 'Remark'
      BlobType = ftWideMemo
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=D:\AB' +
      'Soft\Code\Framework\Exe\Packages\DelphiXE1\ABSoft_Set\ABSoftDogP' +
      '\Data.mdb;Mode=Share Deny None;Extended Properties="";Persist Se' +
      'curity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registr' +
      'y Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5' +
      ';Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk' +
      ' Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Databa' +
      'se Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:' +
      'Encrypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=Fa' +
      'lse;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP' +
      '=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 88
    Top = 64
  end
  object DataSource1_2: TDataSource
    DataSet = ADOQuery1_2
    Left = 264
    Top = 107
  end
  object ADOQuery1_2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    AfterInsert = ADOQuery1_1AfterInsert
    BeforePost = ADOQuery1_2BeforePost
    Parameters = <>
    SQL.Strings = (
      'select * from  List  order by ID')
    Left = 264
    Top = 64
    object ADOQuery1_2ID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object ADOQuery1_2RegName: TWideStringField
      DisplayLabel = #23458#25143#21517#31216
      FieldName = 'RegName'
      Size = 250
    end
    object ADOQuery1_2RegKey: TWideStringField
      DisplayLabel = #27880#20876#30721
      FieldName = 'RegKey'
      Size = 250
    end
    object ADOQuery1_2BegDatetime: TDateTimeField
      DisplayLabel = #24320#22987#26102#38388
      FieldName = 'BegDatetime'
    end
    object ADOQuery1_2EndDatetime: TDateTimeField
      DisplayLabel = #32467#26463#26102#38388
      FieldName = 'EndDatetime'
    end
    object ADOQuery1_2SubsequentClientCount: TIntegerField
      DisplayLabel = #24182#21457#23458#25143#31471#25968
      FieldName = 'SubsequentClientCount'
    end
    object ADOQuery1_2ClientInfo: TWideMemoField
      DisplayLabel = #25480#26435#23458#25143#31471
      FieldName = 'ClientInfo'
      BlobType = ftWideMemo
    end
    object ADOQuery1_2CardCount: TIntegerField
      DisplayLabel = #21345#25968#37327
      FieldName = 'CardCount'
    end
    object ADOQuery1_2ControlCount: TIntegerField
      DisplayLabel = #25511#21046#22120#25968#37327
      FieldName = 'ControlCount'
    end
    object ADOQuery1_2V100Count: TIntegerField
      DisplayLabel = 'V100'#25968#37327
      FieldName = 'V100Count'
    end
    object ADOQuery1_2V200Count: TIntegerField
      DisplayLabel = 'V200'#25968#37327
      FieldName = 'V200Count'
    end
    object ADOQuery1_2V300Count: TIntegerField
      DisplayLabel = 'V300'#25968#37327
      FieldName = 'V300Count'
    end
    object ADOQuery1_2ReaderCount: TIntegerField
      DisplayLabel = #35835#21345#22120#25968#37327
      FieldName = 'ReaderCount'
    end
    object ADOQuery1_2TerminalCount: TIntegerField
      DisplayLabel = #28040#36153#26426#25968#37327
      FieldName = 'TerminalCount'
    end
    object ADOQuery1_2FuncRight: TWideMemoField
      DisplayLabel = #33756#21333#26435#38480
      FieldName = 'FuncRight'
      BlobType = ftWideMemo
    end
    object ADOQuery1_2State: TWideStringField
      DisplayLabel = #29366#24577
      FieldName = 'State'
      Size = 50
    end
    object ADOQuery1_2Count: TIntegerField
      DisplayLabel = #21046#29399#27425#25968
      FieldName = 'Count'
    end
    object ADOQuery1_2Price: TFloatField
      DisplayLabel = #20215#26684
      FieldName = 'Price'
    end
    object ADOQuery1_2CustomFunc: TWideMemoField
      DisplayLabel = #23450#21046#21151#33021
      FieldName = 'CustomFunc'
      BlobType = ftWideMemo
    end
    object ADOQuery1_2Remark: TWideMemoField
      DisplayLabel = #22791#27880
      FieldName = 'Remark'
      BlobType = ftWideMemo
    end
  end
  object ADOConnection2: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 456
    Top = 352
  end
  object ADOQuery3: TADOQuery
    Connection = ADOConnection2
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from ('
      
        'select  Fu_Guid tempGuid,Fu_Ti_Guid tempParentGuid,'#39'['#39'+cast(dbo.' +
        'Func_GetFuncID(Fu_FileName) as nvarchar)+'#39']'#39'+Fu_Name tempname ,F' +
        'u_order tempOrder'
      
        'from ABSys_Org_Function                                         ' +
        '                                               '
      'where Fu_IsView=1'
      'union all'
      
        'select Ti_Guid tempGuid,Ti_ParentGuid tempParentGuid,Ti_Name tem' +
        'pname,ti_order '
      'from ABSys_Org_TreeItem a'
      
        'where Ti_tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Co' +
        'de='#39'Function Dir'#39') and '
      
        '      (exists(select * from ABSys_Org_Function where Fu_Ti_Guid=' +
        'Ti_Guid and Fu_IsView=1) or '
      
        '       exists(select * from ABSys_Org_TreeItem b where b.Ti_Pare' +
        'ntGuid=a.Ti_Guid)'
      '       )'
      ') aa'
      'order by tempOrder'
      ''
      ' ')
    Left = 640
    Top = 360
  end
  object PopupMenu1: TPopupMenu
    Left = 320
    Top = 280
    object N2: TMenuItem
      Caption = #20840#36873
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #20840#19981#36873
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Caption = #20462#25913#21151#33021#26641
      OnClick = N1Click
    end
  end
  object DataSource1_21: TDataSource
    DataSet = ADOQuery1_21
    Left = 344
    Top = 107
  end
  object ADOQuery1_21: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    DataSource = DataSource1_2
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = '5'
      end>
    SQL.Strings = (
      'select  *  from  Log where ListID = :ID ')
    Left = 344
    Top = 64
    object ADOQuery1_21ListID: TIntegerField
      FieldName = 'ListID'
    end
    object ADOQuery1_21Datetime: TDateTimeField
      DisplayLabel = #25805#20316#26102#38388
      FieldName = 'Datetime'
    end
    object ADOQuery1_21Remark: TWideMemoField
      DisplayLabel = #25805#20316#20869#23481
      FieldName = 'Remark'
      BlobType = ftWideMemo
    end
  end
  object pm1: TPopupMenu
    Left = 440
    Top = 248
    object MenuItem1: TMenuItem
      Caption = #23548#20986'EXCEL'
      OnClick = MenuItem1Click
    end
  end
end
