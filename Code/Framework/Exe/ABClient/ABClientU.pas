//框架包-->内部客户端
unit ABClientU;

interface
{
  加载过多皮肤会浪费大量的内存和增大EXE体积，此处有选择的加载几种皮肤
  加入如下几个皮肤单元
  dxSkinsDesignHelper,dxSkinBlack,dxSkiniMaginary,dxSkinGlassOceans,
  在项目的引用中要加入包dcldxSkinsDesignHelperRS21;dxSkinBlackRS21;dxSkinGlassOceansRS21; dxSkiniMaginaryRS21;
  }

uses
  ABPubFileRunOneU,
  ABPubFormU,
  ABPubAutoProgressBar_ThreadU,
  ABPubSelectStrU,
  ABPubDesignU,
  ABPubSelectCheckComboBoxU,
  ABPubManualProgressBarU,
  ABPubLanguageU,
  ABPubFuncU,
  ABPubMessageU,
  ABPubRegisterFileU,
  ABPubLocalParamsU,
  ABPubLogU,
  ABPubVarU,
  ABPubUserU,
  ABPubConstU,
  ABPubLocalParamsEditU,
  ABPubStartFormU,
  ABPubDBU,
  ABPubDesignAlignU,
  ABPubSelectCheckTreeViewU,

  ABThird_DevExtPopupMenuU,
  ABThirdDBU,
  ABThirdFuncU,
  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdFormU,

  ABFrameworkObjectInspectorU,
  ABFrameworkQueryU,
  ABFrameworkUpAndDownU,
  ABFrameworkControlU,
  ABFrameworkcxGridU,
  ABFrameworkUserU,
  ABFrameworkTreeItemEditU,
  ABFrameworkFuncFormU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkFuncU,
  ABFrameworkVarU,
  ABFrameworkTemplateFormU,

  ABClientBackU,

  cxTextEdit,
  cxEdit,
  cxOI,
  cxClasses,
  cxPC,
  cxVGrid,
  cxInplaceContainer,
  cxLocalization,
  cxLookAndFeels,
  cxCheckBox,
  cxDBExtLookupComboBox,
  cxDropDownEdit,
  cxGridDBBandedTableView,
  cxGridLevel,
  dxStatusBar,
  dxNavBar,
  dxNavBarStyles,
  dxNavBarCollns,
  dxDockPanel,
  dxDockControl,
  dxBar,
  dxSkinsForm,
  dxSkinsDesignHelper,
  dxSkinBlack,
  dxSkiniMaginary,
  dxSkinGlassOceans,

  TypInfo,Windows,Messages,SysUtils,Variants,Classes,ActnList,Menus,ImgList,Controls,DB,
  ComCtrls,ExtCtrls,Forms,StdConvs,Graphics,math,DBClient,
  Buttons, cxGraphics, cxControls, cxLookAndFeelPainters, dxBarBuiltInMenu,
  cxContainer, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, System.Actions, dxNavBarBase, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridCustomView, cxGrid, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, System.ImageList;


type
  TABClientForm = class(TABPubForm)
    ImageList_MainMenu: TImageList;
    ActionList1: TActionList;
    SubMenuOnClickAction: TAction;
    ChangUserAction: TAction;
    EditPasswordAction: TAction;
    LockAction: TAction;
    ExitAction: TAction;
    ParentMenuOnClickAction: TAction;
    DataSource1: TABDataSource;
    ClientDataSet1: TClientDataSet;
    SetAutoRun: TAction;
    PopupMenu_OpenFuncs: TPopupMenu;
    CloseFuncMenu: TMenuItem;
    SetupAction: TAction;
    ChangeLanguageAction: TAction;
    AfreshLoginAction: TAction;
    N4: TMenuItem;
    PopupMenu_OpenAllFunc: TPopupMenu;
    MenuItem1: TMenuItem;
    N5: TMenuItem;
    PopupMenu_ABLanguage: TPopupMenu;
    ChangeMainVCLSkinAction: TAction;
    dxSkinController1: TdxSkinController;
    PopupMenu_LeftMenu: TPopupMenu;
    MenuItem2: TMenuItem;
    FrequentlyDownListAction: TAction;
    UpToDatabaseAction: TAction;
    cxLocalizer1: TcxLocalizer;
    Timer1: TTimer;
    MainBackPanel: TPanel;
    ABdxStatusBar1: TABdxStatusBar;
    ABdxStatusBar1Container3: TdxStatusBarContainerControl;
    ABcxPageControl1: TABcxPageControl;
    ABdxStatusBar1Container4: TdxStatusBarContainerControl;
    ABcxExtLookupComboBox1: TABcxExtLookupComboBox;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    CustomToolBar1: TdxBar;
    CustomToolBar2: TdxBar;
    CustomToolBar3: TdxBar;
    CustomToolBar4: TdxBar;
    MainMenuBar: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton2: TdxBarLargeButton;
    dxBarButton3: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton4: TdxBarLargeButton;
    dxBarSkinItems: TdxBarSubItem;
    dxBarButton5: TdxBarLargeButton;
    dxBarButton1: TdxBarLargeButton;
    dxBarButton6: TdxBarLargeButton;
    dxBarButton7: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxDockingManager: TdxDockingManager;
    cxLookAndFeelController1: TcxLookAndFeelController;
    ChangeFuncVCLSkinAction: TAction;
    cxGrid1: TABcxGrid;
    cxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGrid1ABcxGridDBBandedTableView1Column1: TcxGridDBBandedColumn;
    cxGrid1ABcxGridDBBandedTableView1Column2: TcxGridDBBandedColumn;
    cxGrid1ABcxGridDBBandedTableView1Column3: TcxGridDBBandedColumn;
    cxGrid1Level1: TcxGridLevel;
    dxDockSite: TdxDockSite;
    dxLayoutDockSite: TdxLayoutDockSite;
    dxDockPanel_Custom: TdxDockPanel;
    mydxNavBar: TdxNavBar;
    Group3: TdxNavBarGroup;
    Group1: TdxNavBarGroup;
    Group2: TdxNavBarGroup;
    stBackground: TdxNavBarStyleItem;
    stGroup1Background: TdxNavBarStyleItem;
    stGroup2Background: TdxNavBarStyleItem;
    stGroup3Background: TdxNavBarStyleItem;
    mydxNavBarItem1: TdxNavBarItem;
    N2: TMenuItem;
    Timer2: TTimer;
    N1: TMenuItem;
    N6: TMenuItem;
    N6111: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N3: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure ChangUserActionExecute(Sender: TObject);
    procedure EditPasswordActionExecute(Sender: TObject);
    procedure LockActionExecute(Sender: TObject);
    procedure ExitActionExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetAutoRunExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure CloseFuncMenuClick(Sender: TObject);
    procedure SetupActionExecute(Sender: TObject);
    procedure ABcxExtLookupComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure AfreshLoginActionExecute(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure dxBarButton7Click(Sender: TObject);
    procedure ABcxPageControl1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure FrequentlyDownListActionExecute(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure UpToDatabaseActionExecute(Sender: TObject);
    procedure dxDockSiteShowControl(Sender: TdxDockSite;
      AControl: TdxCustomDockControl);
    procedure dxDockSiteHideControl(Sender: TdxDockSite;
      AControl: TdxCustomDockControl);
    procedure ABdxStatusBar1Resize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ChangeFuncVCLSkinActionExecute(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure PopupMenu_LeftMenuPopup(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure SubMenuOnClickActionExecute(Sender: TObject);
    //窗体的切换事件,用来显示当前运行程序的版本号等信息
    procedure ActiveMDIFormChange(Sender: TObject);
    //已打开功能菜单的点击
    procedure PopupMenu_OpenFuncsClick(Sender: TObject);
    //U盘插入与离开事件
    procedure WMDeviceChange(var Msg: TMessage); message WM_DEVICECHANGE;
    //Dev菜单点击时的事件
    procedure ChangeLanguageActionExecute(Sender: TObject);
    //收到消息时释放已关闭功能的BPL
    procedure FreeFormMsg(var Msg: Tmessage); message ABThirdFormFree_Msg;
  private
    FClientBackForm: TABClientBackForm;
    FUseDefaultINI:boolean;
    FBackTimerSencond: LongInt;
  private
    //系统设置的客户端心跳秒数
    FSetHeartbeat:longint;
    //距离上次客户端心跳有几秒了
    FSencond_FromHeartbeat:longint;
    //MDI当前活动窗体切换事件
    FOldActiveMDIFormChange:  TNotifyEvent;
  private
    //加载配制文件
    procedure LoadFromIniFile;
    //关闭所有已打开功能
    procedure CloseAllOpenFunc;
    //将当前文件的窗体置顶
    procedure CurFormToBring(aFileName: string);
    //计算状态栏中已打开功能pageControl的宽度
    procedure CalcFuncPageControlWidth;
    //设置MDI主窗体的背景
    procedure SetMainBack;
    //创建背景窗体
    procedure CreateBackForm;
    //删除布局文件
    procedure DeleteUserINI;
  protected
    //定时器事件
    procedure DoTimer;virtual;
    //自定义窗体的刷新事件
    function CustomRefresh(aDataset:TDataSet): Boolean;virtual;
    //加载程序图标
    procedure Loadico;virtual;
    //加载语言
    procedure LoadLanguageMenuItems;virtual;
    //加载皮肤
    procedure LoadVCLSkins;virtual;

    //登录时加载操作员的配制
    procedure LoadCurUser;virtual;
    //退出(也包含切换用户)时保存上一个操作员的信息
    procedure SaveCurUser;virtual;
    //用户登录
    procedure ClientLogin(aType:TLongType;aLogined:boolean=false);virtual;
    //检测常用列项中的项目是否可以显示出来，aGroupName=检测项目的组名
    function ShowDownList(aGroupName:string):boolean;virtual;
  Public
    //点击菜单打开相应的功能
    //aFileName=要打开的功能文件名（对应功能表ABSys_Org_Function中的FU_FileName字段）
    //aSaveLastCall=打开的功能是否放到最近访问列表中
    //aShowAVIProgress=打开功能时是否显示进度条
    function OpenFunc(aFileName: string;
                      aSaveLastCall:boolean=true;
                      aShowAVIProgress:boolean=true):Boolean;virtual;
    //背景定时到了多少秒
    property  BackTimerSencond:LongInt read FBackTimerSencond write FBackTimerSencond;
    //邮件背景双击打开邮件
    procedure  OpenMail(aGuid:string);virtual;
  end;


var
  ABClientForm: TABClientForm;

implementation

{$R *.dfm}

procedure TABClientForm.CalcFuncPageControlWidth;
var
  i,tempSumWidth:LongInt;
begin
  tempSumWidth:=0;
  for I :=0 to ABdxStatusBar1.Panels.Count - 1 do
  begin
    if (ABdxStatusBar1.Panels[i].Visible) and (i<>ABdxStatusBar1.Panels.Count - 2) then
    begin
      tempSumWidth:=tempSumWidth+ABdxStatusBar1.Panels[i].Width;
    end;
  end;
  ABdxStatusBar1.Panels[ABdxStatusBar1.Panels.Count - 2].Width:=ABdxStatusBar1.Width-tempSumWidth;
end;

//加载皮肤
procedure TABClientForm.LoadVCLSkins;
var
  I: Integer;
  tempStr1:string;
  tempItemLink:TdxBarItemLink;
  tempLargeButton:TdxBarLargeButton;
  //tempStrings:TStrings;
  procedure CreateSkinItem(aParent:TdxBarSubItem;
                           aCaption:string;aIndex,aGroupIndex:LongInt);
  var
    tempCaption:string;
  begin
    tempCaption:=aCaption;
    tempLargeButton := TdxBarLargeButton.Create(Self);
    tempLargeButton.GlyphLayout:=glLeft;
    tempLargeButton.AutoGrayScale:=false;
    tempLargeButton.Category := 0;
    tempLargeButton.ButtonStyle:=bsChecked;
    tempLargeButton.GroupIndex:=aGroupIndex ;
    tempLargeButton.Caption:=tempCaption;
    tempLargeButton.Name:='SkinName_'+ABStrToName(tempCaption);
    tempLargeButton.Index:=aIndex;
    tempLargeButton.OnClick := ChangeFuncVCLSkinActionExecute;

    if  AnsiCompareText(ABLocalParams.CurSkinName,tempCaption)=0 then
    begin
      tempLargeButton.Down:=true;
      dxSkinController1.UseSkins:= aIndex>=1;
      dxSkinController1.SkinName:=tempCaption;
      cxLookAndFeelController1.SkinName:=tempCaption;
    end;


    tempItemLink:=aParent.ItemLinks.Add;
    tempItemLink.Item:=tempLargeButton;
    if aIndex=1 then
    begin
      tempItemLink.BeginGroup:=true
    end;
  end;
begin
  //创功能窗体dxSkins
  if dxSkinsProjectSettings.UnitStateList.Count>0 then
  begin
    CreateSkinItem(dxBarSkinItems,'关闭皮肤',0,12);

    for I := 0 to dxSkinsProjectSettings.UnitStateList.Count - 1 do
    begin
      tempStr1:=dxSkinsProjectSettings.UnitStateList.Item[I].Name;
      CreateSkinItem(dxBarSkinItems,tempStr1,i+1,12);
    end;
  end;
end;

procedure TABClientForm.LoadLanguageMenuItems;
var
  I: Integer;
  tempItemLink:TdxBarItemLink;
  tempLargeButton:TdxBarLargeButton;
begin
  ABLanguage.ReFreshMenuItems(PopupMenu_ABLanguage.Items);
  ABCleardxBarItems(dxBarSubItem1.ItemLinks);
  for I := 0 to PopupMenu_ABLanguage.Items.Count - 1 do
  begin
    tempLargeButton := TdxBarLargeButton.Create(Self);
    tempLargeButton.GlyphLayout:=glLeft;
    tempLargeButton.AutoGrayScale:=false;
    tempLargeButton.OnClick := ChangeLanguageActionExecute;
    tempLargeButton.Category := 0;
    //因为ButtonStyle会改变GroupIndex,所以GroupIndex要放在ButtonStyle后面
    tempLargeButton.ButtonStyle:=bsChecked;
    tempLargeButton.GroupIndex:=10;
    tempLargeButton.Description:=PopupMenu_ABLanguage.Items[i].Hint;

    tempLargeButton.Caption:=PopupMenu_ABLanguage.Items[i].Caption;
    tempLargeButton.Name:='LanguageMenuItems_'+ABStrToName(tempLargeButton.Caption);

    if AnsiCompareText(PopupMenu_ABLanguage.Items[i].Hint,ABLanguage.Language)=0 then
    begin
      tempLargeButton.Down:=true;
      ChangeLanguageActionExecute(tempLargeButton);
    end;

    tempItemLink:=dxBarSubItem1.ItemLinks.Add;
    tempItemLink.Item:=tempLargeButton;
  end;
end;

//加载程序图标
procedure TABClientForm.Loadico;
begin
  //如果在设置目录下有程序图标文件则加载
  if ABCheckFileExists(ABSoftSetPath+'Application.ico') then
  begin
    Application.Icon.LoadFromFile(ABSoftSetPath+'Application.ico');
  end;
end;

procedure TABClientForm.FormCreate(Sender: TObject);
begin
  FSencond_FromHeartbeat:=0;

  FSetHeartbeat:=ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]),['Pa_Name'],['Heartbeat'],['Pa_Value'],60);

  FOldActiveMDIFormChange:=Screen.OnActiveFormChange;
  Screen.OnActiveFormChange:=ActiveMDIFormChange;

  Loadico;
  LoadLanguageMenuItems;
  ABLanguageSetComponentsText(ABdxStatusBar1);
  LoadVCLSkins;

  Caption:= ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]),['Pa_Name'],['RegName'],['Pa_Value'],'')+
            ' ['+
            ABLanguage.GetCodeStr('主版本')+':'+
            ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]),['Pa_Name'],['VersionNo'],['Pa_Value'],'')+' '+
            ABLanguage.GetCodeStr('编译版本')+':'+ABRegisterFile.GetVersion(ABAppFullName)+
             ']';

end;

procedure TABClientForm.FormShow(Sender: TObject);
begin
  CreateBackForm;
  ActiveMDIFormChange(nil);

  //加载用户相关的信息
  LoadCurUser;

  Timer1.Enabled:=true;
  ABCloseStartForm;
end;

function TABClientForm.ShowDownList(aGroupName:string):boolean;
begin
  Result:=(AnsiCompareText(aGroupName,'框架使用')=0);
end;

procedure TABClientForm.LoadFromIniFile;
begin
  // LoadFromIniFile的操作要在创建相应ITEM之后
  try
    //加载dxBarManager1
    if ABCheckFileExists(ABCurUserPath+ABAppName+'_dxBarManager1.ini') then
    begin
      //因未解决主菜单中因_dxBarManager1文件而多创建的原因,所以暂时先不启用LoadFromIniFile功能
      dxBarManager1.LoadFromIniFile( ABCurUserPath+ABAppName+'_dxBarManager1.ini');
    end;

    //加载dxDockingManager
    if ABCheckFileExists(ABCurUserPath+ABAppName+'_dxDockingManager.ini') then
      dxDockingManager.LoadLayoutFromIniFile( ABCurUserPath+ABAppName+'_dxDockingManager.ini');
  except
    dxDockSite.Visible:=false;
    SetMainBack;

    DeleteUserINI;
  end;
end;

procedure TABClientForm.LoadCurUser;
var
  I: Integer;
  tempStrings:TStrings;
  tempQuickOpenFunc,tempFuncFileName,tempFuncCaption :string;
  tempMainMenu:TMainMenu;
  tempDataset:Tdataset;
  tempList:Tstrings;
  procedure MainMenuToViews;
  var
    tempBitmap1,tempBitmap2:Graphics.TBitmap;
    function loadBitMap(aFileName:string):Boolean;
    begin
      //从字段中加载图标
      tempBitmap1.LoadFromFile(ABTempPath+aFileName);

      //将图标宿小到ImageList_MainMenu 的Width与Height大小
      tempBitmap2.Height := ImageList_MainMenu.Height;
      tempBitmap2.Width  := ImageList_MainMenu.Width ;
      tempBitmap2.Canvas.StretchDraw(Rect(0,0,ImageList_MainMenu.Width,ImageList_MainMenu.Height),tempBitmap1);
      //将宿小后的图标加入到ImageList_MainMenu中
      //为达到透明的效果,不用Add方法而用AddMasked方法,clWhite为透明的颜色
      //ImageList_MainMenu.AddMasked(tempBitmap2,clWhite);
      ImageList_MainMenu.InsertMasked(ImageList_MainMenu.Count,tempBitmap2,clWhite);

      result:=true;
    end;
    procedure DoMainMenuToViews(aMenuItem:TMenuItem;aLevel:LongInt=0;
                                aParentSubItem:TdxBarSubItem=nil);
    var
      i:longint;
      tempStr1:string;
      tempCustomItem:TdxBarLargeButton;

      tempChidrenNodeLink:TdxBarItemLink;
      tempChidrenNode:TdxBarItem;
      tempHaveChidrenNode:TdxBarSubItem;
      tempNoHaveChidrenNode:TdxBarLargeButton;
    begin
      tempHaveChidrenNode:=nil;
      tempNoHaveChidrenNode:=nil;
      for I := 0 to aMenuItem.Count - 1 do
      begin
        tempFuncFileName:=aMenuItem.Items[i].Hint;
        tempFuncCaption:=aMenuItem.Items[i].Caption;

        //增加顶窗的主菜单
        if AnsiCompareText(tempFuncCaption,'-')=0 then
        begin
        end
        else
        begin
          if aMenuItem[i].Count> 0 then
          //创建父菜单
          begin
            tempHaveChidrenNode:=TdxBarSubItem.Create(dxBarManager1.Owner);
            tempChidrenNode:=tempHaveChidrenNode;
          end
          else
          //创建子菜单
          begin
            tempNoHaveChidrenNode:=TdxBarLargeButton.Create(dxBarManager1.Owner);
            tempNoHaveChidrenNode.GlyphLayout:=glLeft;
            tempNoHaveChidrenNode.AutoGrayScale:=false;
            tempNoHaveChidrenNode.PaintStyle:=psCaptionGlyph;
            tempNoHaveChidrenNode.OnClick:=SubMenuOnClickActionExecute;
            tempChidrenNode:=tempNoHaveChidrenNode;
          end;

          if Assigned(aParentSubItem) then
          begin
            tempChidrenNodeLink:=aParentSubItem.ItemLinks.Add;
          end
          else
          begin
            tempChidrenNodeLink:=dxBarManager1.MainMenuBar.ItemLinks.Add;
          end;

          if (i-1>=0) and
             (AnsiCompareText(aMenuItem.Items[i-1].Caption,'-')=0) then
          begin
            tempChidrenNode.Hint:='1';
          end
          else
            tempChidrenNode.Hint:='0';

          tempChidrenNodeLink.Item:=tempChidrenNode;
          tempChidrenNode.Description:=tempFuncFileName;
          tempChidrenNode.Caption:=tempFuncCaption;

          if Assigned(aParentSubItem) then
            tempStr1:=aParentSubItem.Caption
          else
            tempStr1:=emptystr;

          tempChidrenNode.Name:='TopMainMenu_'+ABStrToName(tempFuncFileName+
                                                           tempFuncCaption+
                                                           tempStr1);
          tempChidrenNode.Caption:=ABFillStr(tempChidrenNode.Caption,Max(length(tempFuncCaption),4),' ',axdRight);
          tempList.Add(tempChidrenNodeLink.Item.Name+'='+inttostr(tempChidrenNodeLink.Index));

          if aMenuItem[i].Count> 0 then
          begin
            DoMainMenuToViews(aMenuItem[i],aLevel+1,tempHaveChidrenNode);
          end
          else
          begin
            if (tempFuncFileName<>emptystr) then
            begin
              //创建的是dxBarManager1中的,方便拖到工具栏中
              tempCustomItem := TdxBarLargeButton.Create(Self);
              tempCustomItem.GlyphLayout:=glLeft;
              tempCustomItem.AutoGrayScale:=false;
              tempCustomItem.OnClick:=SubMenuOnClickActionExecute;
              tempCustomItem.Category := 1;

              tempCustomItem.PaintStyle:=psCaptionGlyph;
              tempCustomItem.Description:=tempFuncFileName;
              tempCustomItem.Caption:=tempFuncCaption;
              tempCustomItem.Name:='dxBarManager1_CustomItem'+ABStrToName(tempCustomItem.Caption);


              if ABCheckFileExists(ABTempPath+tempFuncFileName)then
              begin
                //将图标加入到ImageList_MainMenu中
                if loadBitMap(tempFuncFileName) then
                begin
                  //设置菜单的图标号
                  tempCustomItem.ImageIndex := ImageList_MainMenu.Count-1;
                  tempNoHaveChidrenNode.ImageIndex:=ImageList_MainMenu.Count-1;

                  tempCustomItem.LargeImageIndex := ImageList_MainMenu.Count-1;
                  tempNoHaveChidrenNode.LargeImageIndex:=ImageList_MainMenu.Count-1;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  begin
    tempBitmap1:=TBitmap.Create;
    tempBitmap2:=TBitmap.Create;
    dxBarManager1.BeginUpdate;
    MainMenuBar.ItemLinks.BeginUpdate;
    try
      DoMainMenuToViews(tempMainMenu.Items);
    finally
      MainMenuBar.ItemLinks.EndUpdate;
      dxBarManager1.EndUpdate;
      tempBitmap1.Free;
      tempBitmap2.Free;
    end;
  end;
  procedure AfterLoadFromIniFile;
    procedure AfterLoadFromIniFile_Item(aItemLinks:TdxBarItemLinks);
    var
      I,tempIndex: Integer;
    begin
      for I := aItemLinks.Count - 1 downto 0 do
      begin
        if aItemLinks[i].Item is TdxBarSubItem then
          AfterLoadFromIniFile_Item(TdxBarSubItem(aItemLinks[i].Item).ItemLinks);

        if (Assigned(aItemLinks[i].Item)) then
        begin
          if ((aItemLinks[i].BeginGroup)<>(aItemLinks[i].Item.Hint='1')) then
            aItemLinks[i].BeginGroup:=aItemLinks[i].Item.Hint='1';

          tempIndex:= strtoint(tempList.Values[aItemLinks[i].Item.Name]);
          if tempIndex<>aItemLinks[i].Index then
            aItemLinks[i].Index:=tempIndex;
        end;
      end;
    end;
  begin
    AfterLoadFromIniFile_Item(MainMenuBar.ItemLinks);
  end;
  procedure InitUserInfo;
  var
    i:LongInt;
  begin
    if (not ABCheckFileExists(ABCurUserPath + ABAppName+'_Setup.ini')) and
       (ABCheckFileExists(ABDefaultUserPath + ABAppName+'_Setup.ini')) then
    begin
      ABCopyFile(ABDefaultUserPath + ABAppName+'_Setup.ini',ABCurUserPath + ABAppName+'_Setup.ini');
    end;

    //关闭上一用户打开的模块
    CloseAllOpenFunc;

    //清除上一用户创建的资源
    for I :=ImageList_MainMenu.Count-1 downto 9  do
    begin
      ImageList_MainMenu.Delete(i);
    end;

    //清除上一用户主菜单项
    dxBarManager1.BeginUpdate;
    try
      for I :=dxBarManager1.ItemCount - 1 downto 0 do
      begin
        if dxBarManager1.Items[i].Category=1 then
        begin
          dxBarManager1.Items[i].Free;
        end;
      end;
    finally
      dxBarManager1.EndUpdate;
    end;

    //清除上一用户主菜单连接
    ABCleardxBarItems(MainMenuBar.ItemLinks);

    //清除上一用户左则菜单项
    mydxNavBar.Items.Clear;
    //清除上一用户左则菜单连接
    Group1.ClearLinks;
    Group2.ClearLinks;
    Group3.ClearLinks;

    //仅admin,sysuser可见按钮"上传到服务器"
    dxBarLargeButton1.Visible:=ABIIF(ABPubUser.IsAdminOrSysuser,ivAlways,ivNever);

    //仅admin可见按钮"设计模块" ,"设置参数","属性编辑器" ,可设置"常用列项"
    if ABPubUser.IsAdmin  then
    begin
      dxBarButton7.Visible:=ivAlways;
      dxBarButton4.Visible:=ivAlways;
    end
    else
    begin
      dxBarButton4.Visible:=ivNever;
      dxBarButton7.Visible:=ivNever;
    end;

    ABdxStatusBar1.Panels[0].Text:=ABLanguage.GetCodeStr('操作员')+':'+ABPubUser.Name;
    ABdxStatusBar1.Panels[0].Width:=Length(AnsiString(ABdxStatusBar1.Panels[0].Text))*6+8;
    CalcFuncPageControlWidth;

    Timer2.Enabled:=true;
  end;
begin
  //初始用户的资源
  InitUserInfo;

  //加载快速启动的下拉框
  if (ABcxExtLookupComboBox1.Visible) then
  begin
    ABcxExtLookupComboBox1.Properties.DropDownListStyle := lsEditList;
    ABSetDatasetFilter(ABUser.FuncTreeDataSet, 'IsFunc=1');
    try
      ClientDataSet1.CloneCursor(ABUser.FuncTreeDataSet,False,true);
      if (ABcxExtLookupComboBox1.Visible) then
      begin
        cxGrid1ABcxGridDBBandedTableView1.ExtPopupMenu.Load;
        tempQuickOpenFunc:=ABReadInI('QuickOpenFunc','FileName', ABCurUserPath + ABAppName+'_Setup.ini');

        if tempQuickOpenFunc<>emptystr then
          ABcxExtLookupComboBox1.EditValue :=tempQuickOpenFunc;
      end;
    finally
      ABSetDatasetFilter(ABUser.FuncTreeDataSet,emptystr);
    end;
  end;

  tempStrings:=TStringlist.Create;
  dxBarManager1.BeginUpdate;
  ClientDataSet1.DisableControls;
  tempMainMenu:=TMainMenu.Create(nil);
  tempList:=TStringList.Create;
  TStringList(tempList).Sorted:=true;
  try
    //创建MainMenu
    ABDataSetsToMenu(tempMainMenu,
                     ABUser.FuncTreeDataSet,
                     'Fu_Ti_Guid',
                     'Fu_Guid',
                     'Fu_Name',
                     'Fu_FileName',
                     'Fu_IsBeginGroup',
                     '',
                     SubMenuOnClickActionExecute,
                     true
                      );

    //加载配制文件
    LoadFromIniFile;

    //MainMenu到窗体主菜单显示栏MainMenuBar
    MainMenuToViews;
    AfterLoadFromIniFile;

    //加载左则菜单
    try
      mydxNavBar.BeginUpdate;
      try
        //加载Group的打开状态
        ABReadInI('GroupsOpenState', ABCurUserPath + ABAppName+'_Setup.ini',tempStrings);
        for I := 0 to mydxNavBar.Groups.Count - 1 do
        begin
          if tempStrings.IndexOfName(mydxNavBar.Groups[i].Name)>=0 then
          begin
            mydxNavBar.Groups[i].Expanded:=(AnsiCompareText(tempStrings.Values[mydxNavBar.Groups[i].Name],'True')=0);
          end;
        end;

        //增加最近访问
        ABReadInI('LastCall', ABCurUserPath + ABAppName+'_Setup.ini',tempStrings);
        for I := 0 to tempStrings.Count - 1 do
        begin
          tempFuncCaption :=tempStrings.Names[i];
          tempFuncFileName:=tempStrings.ValueFromIndex[i];

          if ClientDataSet1.Locate('Fu_FileName',tempFuncFileName,[]) then
          begin
            ABAddNavBarGroupItem(mydxNavBar,Group1,tempFuncCaption,tempFuncFileName,SubMenuOnClickActionExecute,false,8);
          end;
        end;

        //加载自启动
        ABReadInI('AutoRun', ABCurUserPath + ABAppName+'_Setup.ini',tempStrings);
        for I := 0 to tempStrings.Count - 1 do
        begin
          tempFuncCaption :=tempStrings.Names[i];
          tempFuncFileName:=tempStrings.ValueFromIndex[i];
          if ClientDataSet1.Locate('Fu_FileName',tempFuncFileName,[]) then
          begin
            ABAddNavBarGroupItem(mydxNavBar,Group2,tempFuncCaption,tempFuncFileName,SubMenuOnClickActionExecute,false,-1);

            OpenFunc(tempFuncFileName);
          end;
        end;

        //加载常用列项
        ABReadInI('PubClient'+'.FrequentlyDownList', ABDefaultValuePath+'PubDefaultValue.ini',tempStrings);
        if tempStrings.Count>0 then
        begin
          for I := 0 to tempStrings.Count - 1 do
          begin
            tempFuncCaption :=tempStrings.Names[i];
            tempFuncFileName:=tempStrings.ValueFromIndex[i];

            tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Tree',[tempFuncFileName]);
            if not tempDataset.isempty then
            begin
              if  (ABClientTypeUse[1]) or (ShowDownList(tempDataset.FieldByName('tr_group').AsString)) then
              begin
                ABAddNavBarGroupItem(mydxNavBar,Group3,tempFuncCaption,tempFuncFileName,FrequentlyDownListActionExecute,false,-1);
              end;
            end;
          end;
        end;
      finally
        mydxNavBar.EndUpdate;
      end;
    except
      dxDockSite.Visible:=false;
      SetMainBack;
    end;
  finally
    tempList.Free;
    dxBarManager1.EndUpdate;
    ClientDataSet1.EnableControls;
    tempMainMenu.Free;
    tempStrings.Free;
  end;
end;

procedure TABClientForm.SaveCurUser;
var
  I: Integer;
begin
  //保存QuickOpenFunc
  if ABcxExtLookupComboBox1.Visible then
    ABWriteInI('QuickOpenFunc','FileName',VarToStrDef(ABcxExtLookupComboBox1.EditValue,''), ABCurUserPath + ABAppName+'_Setup.ini');

  //保存最近访问
  ABDeleteIni('LastCall',ABCurUserPath + ABAppName+'_Setup.ini');
  for I := 0 to Group1.LinkCount - 1 do
  begin
    ABWriteInI('LastCall',Group1.Links[i].Item.Caption,Group1.Links[i].Item.Hint, ABCurUserPath + ABAppName+'_Setup.ini');
  end;
  //保存自启动
  ABDeleteIni('AutoRun',ABCurUserPath + ABAppName+'_Setup.ini');
  for I := 0 to Group2.LinkCount - 1 do
  begin
    ABWriteInI('AutoRun',Group2.Links[i].Item.Caption,Group2.Links[i].Item.Hint, ABCurUserPath + ABAppName+'_Setup.ini');
  end;

  //保存Group的打开状态
  for I := 0 to mydxNavBar.Groups.Count - 1 do
  begin
    ABWriteInI('GroupsOpenState',mydxNavBar.Groups[i].Name,ABBoolToStr(mydxNavBar.Groups[i].Expanded), ABCurUserPath + ABAppName+'_Setup.ini');
  end;

  if not FUseDefaultINI then
  begin
    //保存dxBarManager1
    dxBarManager1.SaveToIniFile( ABCurUserPath+ABAppName+'_dxBarManager1.ini');

    //保存dxDockingManager
    dxDockingManager.SaveLayoutToIniFile(ABCurUserPath+ABAppName+'_dxDockingManager.ini');
  end;

  ABWriteInI(Name,'Left',IntToStr(Left), ABCurUserPath + ABAppName+'_Setup.ini');
  ABWriteInI(Name,'Top',IntToStr(Top), ABCurUserPath + ABAppName+'_Setup.ini');
  ABWriteInI(Name,'Width',IntToStr(Width), ABCurUserPath + ABAppName+'_Setup.ini');
  ABWriteInI(Name,'Height',IntToStr(Height), ABCurUserPath + ABAppName+'_Setup.ini');
end;

procedure TABClientForm.SubMenuOnClickActionExecute(Sender: TObject);
var
  tempStr1:string;
begin
  if Sender is TdxNavBarItem then
  begin
    tempStr1:= TdxNavBarItem(Sender).Hint;
  end
  else if Sender is TdxBarLargeButton then
  begin
    tempStr1:= TdxBarLargeButton(Sender).Description;
  end;
  OpenFunc(tempStr1,true);
end;

function TABClientForm.OpenFunc(aFileName: string;aSaveLastCall:boolean;aShowAVIProgress:boolean):Boolean;
var
  tempGuid,tempCaption,tempSubPath,
  tempHint:string;
  tempRunAVI,tempCreateSheet:Boolean;
  tempDataset:TDataSet;
  i:LongInt;
  tempSheet:TcxTabSheet;
  tempMenuItem:TMenuItem;
  tempForm:TForm;
  function ABDownFuncPkg(aSleepNum:LongInt=0):boolean;
  var
    tempPkgDataset:TDataSet;
    tempName: string;
    tempServerFu_Version: string;
  begin
    result := false;
    if aSleepNum>0  then
      Sleep(aSleepNum);

    tempName:=tempDataset.FindField('Fu_Name').AsString;
    tempServerFu_Version:=tempDataset.FindField('Fu_Version').AsString;

    if (ABLocalParams.Debug) then
      ABWriteLog('Begin ABDownFuncPkg '+tempName);

    if (ABRegisterFile.IsDown(tempDataset.FindField('Fu_FileName').AsString,
                             tempDataset.FindField('Fu_SubPath').AsString,
                             tempServerFu_Version)) then
    begin
      tempPkgDataset:=ABGetDataset('Main',
                                   ' SELECT Fu_Pkg '+
                                   ' from ABSys_Org_Function '+
                                   ' where Fu_FileName='+ABQuotedStr(tempDataset.FindField('Fu_FileName').AsString),
                                   []);
      try
        try
          if not ABDatasetIsEmpty(tempPkgDataset) and (not tempPkgDataset.Fields[0].IsNull) then
            ABRegisterFile.Down(
                              tempDataset.FindField('Fu_FileName').AsString,
                              tempDataset.FindField('Fu_SubPath').AsString,
                              TBlobField(tempPkgDataset.Fields[0]),
                              tempServerFu_Version
                                                );
        except
          ABShow('模块[%s]下载失败,请检查.',[tempName]);
          raise;
        end;
      finally
        tempPkgDataset.free;
      end;
    end;

    if (ABLocalParams.Debug) then
      ABWriteLog('End   ABDownFuncPkg '+tempName);
  end;
begin
  Result:=false;

  if aFileName=EmptyStr then
    exit;

  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Function',[aFileName]);
  if (not tempDataset.IsEmpty) then
  begin
    tempGuid   :=tempDataset.FieldByName('Fu_Guid').AsString;
    tempCaption:=tempDataset.FindField('Fu_Name').AsString;
    tempSubPath:=tempDataset.FindField('Fu_SubPath').AsString;
    tempRunAVI:=(aShowAVIProgress) and (tempDataset.FindField('Fu_RunAVI').AsBoolean);

    ABDoBusy(true);
    try
      if tempRunAVI then
      begin
        ABPubAutoProgressBar_ThreadU.ABRunProgressBar('创建中');
      end;

      if (ABLocalParams.DownPkg) and
         (not tempDataset.FindField('Fu_PkgIsNull').AsBoolean) and
         (tempDataset.FindField('FU_IsUpdate').AsBoolean)  then
        ABDownFuncPkg;

      if ABRegisterFile.LoadRun(aFileName,tempCaption,tempSubPath,tempGuid,tempDataset.FindField('FU_IsTemplate').AsBoolean,false,true) then
      begin
        tempForm:=ABRegisterFile.GetFormByFileName(aFileName);
        if (Assigned(tempForm)) and
           (tempForm.WindowState=wsNormal) then
        begin
          tempForm.Left := ABTrunc((MainBackPanel.Width-tempForm.Width)/2);
          tempForm.Top := ABTrunc((MainBackPanel.Height-tempForm.Height)/2);
        end;

        if (aSaveLastCall) then
        begin
          //最近访问的更新
          mydxNavBar.BeginUpdate;
          try
            ABAddNavBarGroupItem(mydxNavBar,Group1,tempCaption,aFileName,SubMenuOnClickActionExecute,true,8);
          finally
            mydxNavBar.EndUpdate;
          end;
        end;

        //窗体下部的功能页面更新
        if Assigned(ABRegisterFile.GetFormByFileName(aFileName)) then
        begin
          tempCreateSheet:=true ;
          for i := 0 to ABcxPageControl1.PageCount - 1 do
          begin
            if AnsiCompareText(aFileName,ABcxPageControl1.Pages[i].Hint)=0 then
            begin
              ABcxPageControl1.ActivePageIndex:=i;
              tempCreateSheet:=False;
              break;
            end;
          end;

          if tempCreateSheet then
          begin
            tempSheet:=TcxTabSheet.Create(ABcxPageControl1);
            tempSheet.PageControl:=ABcxPageControl1;
            tempSheet.Caption:=tempCaption;
            tempSheet.Name:='ABcxPageControl1_Sheet'+ABStrToName(tempSheet.Caption);
            tempSheet.ShowHint:=False;
            tempSheet.Hint:=aFileName;
            ABcxPageControl1.ActivePage:=tempSheet;

            tempMenuItem:=TMenuItem.Create(self);
            tempMenuItem.Hint:=aFileName;
            tempMenuItem.Caption:=tempCaption;
            tempMenuItem.Name:='PopupMenu_OpenFuncs_MenuItem'+ABStrToName(tempMenuItem.Caption);
            tempMenuItem.OnClick:=PopupMenu_OpenFuncsClick;
            N5.Add(tempMenuItem);
          end;
        end;

        ActiveMDIFormChange(nil);
        Result:=true;
      end;
    finally
      ABDoBusy(false);
      if tempRunAVI then
      begin
        ABPubAutoProgressBar_ThreadU.ABStopProgressBar;
      end;

      if (Result) and
         (ABLocalParams.Debug) then
      begin
        //因为空格写入日志时,4个空格才有一个汉字的长度,会有不对齐的情况,所以暂时用ABStringReplace
        tempHint:=
          Format(ABLanguage.GetCodeStr('%s 于 %s 开始加载,于 %s 结束加载,耗时 %s 秒.'),
                [tempCaption+ABCopyStr(' ',30-length(ansistring(tempCaption))),
                 ABDateTimeToStr(ABRegisterFile.LastRunFileBeginTime,23),
                 ABDateTimeToStr(ABRegisterFile.LastRunFileEndTime,23),
                 FloatToStr(ABRound(ABGetDateTimeSpan_Float(ABRegisterFile.LastRunFileBeginTime,ABRegisterFile.LastRunFileEndTime,tuSeconds),3))
                 ]);

        ABWriteLog(tempHint,True,false);
      end;
    end;
    SetMainBack ;
  end;
end;

//属性背景相关******************Begin******************
procedure TABClientForm.dxDockSiteHideControl(Sender: TdxDockSite;
  AControl: TdxCustomDockControl);
begin
  SetMainBack;
end;

procedure TABClientForm.dxDockSiteShowControl(Sender: TdxDockSite;
  AControl: TdxCustomDockControl);
begin
  SetMainBack;
end;

procedure TABClientForm.SetMainBack;
begin
  if (Assigned(FClientBackForm))  then
  begin
    MainBackPanel.Visible := True;
    try
      PostMessage(FClientBackForm.Handle, ABMainFormFIXMAXIMIZED_Msg,
                   MainBackPanel.ClientHeight, MainBackPanel.ClientWidth);
    finally
      MainBackPanel.Visible := False;
    end;
  end;
end;
//属性背景相关******************end******************

//关闭所有打开的模块
procedure TABClientForm.N4Click(Sender: TObject);
begin
  CloseAllOpenFunc;
end;

procedure TABClientForm.CloseAllOpenFunc;
  procedure ClearOpenFlag;
  var
    i:LongInt;
  begin
    for I := ABcxPageControl1.PageCount - 1 downto 0 do
    begin
      ABcxPageControl1.Pages[i].Free;
    end;
    for I := N5.Count - 1 downto 0 do
    begin
      N5.Items[i].Free;
    end;
  end;
var
  I: Integer;
begin
  for I := ABcxPageControl1.PageCount-1 downto 0 do
  begin
    ABRegisterFile.GetFormByFileName(ABcxPageControl1.Pages[i].Hint).Close;
  end;
  ClearOpenFlag;
end;

//设计模块
procedure TABClientForm.dxBarButton7Click(Sender: TObject);
begin
  if (Assigned(ActiveMDIChild)) and
     (ActiveMDIChild<>FClientBackForm) and
     (ActiveMDIChild.Visible) then
  begin
    if dxBarButton7.Down then
    begin
      ABBeginDesignClientOfOwner(ActiveMDIChild,ActiveMDIChild);
      ABShowObjectInspector(ABPubDesignerHook.Form);
    end
    else
    begin
      ABCloseObjectInspector;
      ABEndDesign(ActiveMDIChild);
    end;
  end;
end;

//切换模块
procedure TABClientForm.ActiveMDIFormChange(Sender: TObject);
var
  tempStr:string;
begin
  if (not Assigned(FClientBackForm)) then
    Exit;

  dxBarButton7.Enabled:=false;
  dxBarButton7.Down:=false;
  ABdxStatusBar1.Panels[1].Text:=emptystr;
  if (Assigned(ActiveMDIChild)) and
     (ActiveMDIChild<>FClientBackForm) and
     (ActiveMDIChild.Visible) then
  begin
    dxBarButton7.Down:=Assigned(ABPubDesignerHook);
    dxBarButton7.Enabled:= (not Assigned(ABPubDesignerHook)) or
                           ((Assigned(ABPubDesignerHook.Form)) and  (ABPubDesignerHook.Form=ActiveMDIChild));

    tempStr:=ABRegisterFile.GetVersion(ABRegisterFile.GetFileNameByFormName(ActiveMDIChild.Name));
    tempStr:= ' ['+tempStr+ ']';
    ABdxStatusBar1.Panels[1].Text:=ABLanguage.GetCodeStr(ActiveMDIChild.Caption)+tempStr;
    postMessage(Handle,ABChangeDesignControl_Msg, 0,0);
  end
  else
  begin
    if (Assigned(ActiveMDIChild)) and
       (ActiveMDIChild=FClientBackForm) and
       (ActiveMDIChild.Visible) and
       (ActiveMDIChild.WindowState= wsMaximized) then
    begin
      ActiveMDIChild.WindowState:= wsNormal;
    end;
  end;

  ABdxStatusBar1.Panels[1].Width:=Length(AnsiString(ABdxStatusBar1.Panels[1].Text))*6;
  CalcFuncPageControlWidth;
end;

Function ClientWindowProc( wnd: HWND; msg: Cardinal; wparam, lparam: Integer ): Integer; stdcall;
Var
  pUserdata: Pointer;
Begin
  pUserdata:= Pointer( GetWindowLong( wnd, GWL_USERDATA ));
  Case msg of
    WM_NCCALCSIZE:
    Begin
      If (GetWindowLong( wnd, GWL_STYLE ) and (WS_HSCROLL or WS_VSCROLL)) <> 0 Then
        SetWindowLong( wnd, GWL_STYLE, GetWindowLong(wnd, GWL_STYLE) and not (WS_HSCROLL or WS_VSCROLL));
    End;
  End;
  Result := CallWindowProc(pUserdata, wnd, msg, wparam, lparam );
end;

procedure TABClientForm.FormDestroy(Sender: TObject);
begin
  Timer1.Enabled:=false;

  Screen.OnActiveFormChange:=FOldActiveMDIFormChange;
  SaveCurUser;

  if Assigned(FClientBackForm) then
    FClientBackForm.Free;
end;

procedure TABClientForm.FrequentlyDownListActionExecute(Sender: TObject);
var
  tempStr1:string;
begin
  if Sender is TdxNavBarItem then
  begin
    tempStr1:= TdxNavBarItem(Sender).Hint;
    ABShowDownListItem(tempStr1,'Ti_Code',tempStr1);
  end;
end;

procedure TABClientForm.OpenMail(aGuid:string);
var
  tempStr1:string;
  tempForm:TForm;
begin
  tempStr1:='ABSys_Org_MailG.bpl';
  if (OpenFunc(tempStr1)) then
  begin
    tempForm := ABRegisterFile.GetFormByFileName(tempStr1);
    if (Assigned(tempForm)) and
       (aGuid<>EmptyStr) then
    begin
      ABLocateFormDataset(tempForm,'ABQuery1','Ma_Guid',aGuid,[]);
    end;
  end;
end;

procedure TABClientForm.SetupActionExecute(Sender: TObject);
var
  tempBackType:TABBackType;
begin
  tempBackType:=ABLocalParams.BackType;
  ABLocalParamsEdit;
  if tempBackType<>ABLocalParams.BackType then
  begin
    FBackTimerSencond:=0;
    FClientBackForm.SetBackType;
  end;
end;

procedure TABClientForm.ABcxExtLookupComboBox1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13) and
     (vartostrdef(ABcxExtLookupComboBox1.EditValue,'')<>EmptyStr) then
  begin
    if ClientDataSet1.Locate('Fu_Code',vartostrdef(ABcxExtLookupComboBox1.EditValue,''),[]) then
      OpenFunc(ClientDataSet1.FindField('Fu_FileName').AsString);
  end;
end;

procedure TABClientForm.MenuItem1Click(Sender: TObject);
var
  tempStr1:string;
begin
  ABGetPubManualProgressBarForm.Title:='加载中';
  ABGetPubManualProgressBarForm.MainMin:=0;
  ABGetPubManualProgressBarForm.MainPosition:=0;
  ABGetPubManualProgressBarForm.MainMax:=ClientDataSet1.RecordCount;
  ABGetPubManualProgressBarForm.Run(true);
  ClientDataSet1.DisableControls;
  try
    ClientDataSet1.First;
    while not ClientDataSet1.Eof do
    begin
      if ABGetPubManualProgressBarForm.ExitRun then
        break;
      ABGetPubManualProgressBarForm.NextMainIndex;
      ABGetPubManualProgressBarForm.Remark:=ABLanguage.GetCodeStr('正在启动')+' '+ClientDataSet1.FindField('FU_Name').AsString;

      tempStr1:=ClientDataSet1.FindField('Fu_FileName').AsString;
      OpenFunc(tempStr1,True,false);

      ClientDataSet1.Next;
    end;
    if (ClientDataSet1.Eof) and
       (tempStr1<>ClientDataSet1.FindField('Fu_FileName').AsString) then
      OpenFunc(ClientDataSet1.FindField('Fu_FileName').AsString,True,false);
  finally
    ABGetPubManualProgressBarForm.Stop;
    ClientDataSet1.EnableControls;
  end;
end;

procedure TABClientForm.SetAutoRunExecute(Sender: TObject);
var
  i:LongInt;
  tempFormSelectFuncStrings:TStrings;
begin
  tempFormSelectFuncStrings:=TStringList.Create;
  TStringList(tempFormSelectFuncStrings).Sorted:=true;
  try
    for I := 0 to Group2.LinkCount - 1 do
    begin
      tempFormSelectFuncStrings.Add(Group2.Links[i].Item.Hint);
    end;

    if ABSelectCheckTreeView(
        ABUser.FuncTreeDataSet,
        'Fu_Ti_Guid',
        'Fu_Guid',
        'Fu_Name',
        False,

        nil,
        '',
        '',

        tempFormSelectFuncStrings,
        'Fu_FileName'
        ) then
    begin
      mydxNavBar.BeginUpdate;
      try
        for I :=Group2.LinkCount - 1 downto 0  do
        begin
          mydxNavBar.Items.Remove(Group2.Links[i].Item);
        end;
        Group2.ClearLinks;

        for I := 0 to tempFormSelectFuncStrings.Count-1 do
        begin
          if ClientDataSet1.Locate('Fu_FileName',tempFormSelectFuncStrings[i],[]) then
          begin
            ABAddNavBarGroupItem(mydxNavBar,Group2,ClientDataSet1.FindField('Fu_Name').AsString,tempFormSelectFuncStrings[i],SubMenuOnClickActionExecute,false,-1);
          end;
        end;
      finally
        mydxNavBar.EndUpdate;
      end;
    end;
  finally
    tempFormSelectFuncStrings.Free;
  end;
end;

//编辑常用列项
procedure TABClientForm.MenuItem2Click(Sender: TObject);
var
  tempLeftValue,tempRightValue,tempRightValueCode:string;
  tempDataset:TDataset;
  tempName:string;
  I: Integer;
begin
  tempLeftValue:=EmptyStr;
  tempRightValue:=EmptyStr;
  for I := 0 to Group3.LinkCount - 1 do
  begin
    ABAddstr(tempRightValueCode,Group3.Links[i].Item.Hint);
  end;

  tempDataset:=ABGetDataset('Main','select Tr_Code,Tr_Name from ABSys_Org_Tree',[]);
  try
    tempDataset.First;
    while not tempDataset.Eof do
    begin
      if ABPos(','+tempDataset.FindField('Tr_Code').AsString+',',','+tempRightValueCode+',')<=0 then
      begin
        ABAddstr(tempLeftValue,tempDataset.FindField('Tr_Name').AsString);
      end
      else
      begin
        ABAddstr(tempRightValue,tempDataset.FindField('Tr_Name').AsString);
      end;

      tempDataset.Next;
    end;

    if ABSelectStr(tempLeftValue,tempRightValue,',') then
    begin
      mydxNavBar.BeginUpdate;
      try
        for I :=Group3.LinkCount - 1  downto 0 do
        begin
          mydxNavBar.Items.Remove(Group3.Links[i].Item);
        end;
        Group3.ClearLinks;
        for I := 1 to ABGetSpaceStrCount(tempRightValue, ',') do
        begin
          tempName := ABGetSpaceStr(tempRightValue, i, ',');
          if tempDataset.Locate('Tr_Name',tempName,[]) then
          begin
            ABAddNavBarGroupItem(mydxNavBar,Group3,tempName,tempDataset.FindField('Tr_Code').AsString,FrequentlyDownListActionExecute,false,-1);
          end;
        end;
      finally
        mydxNavBar.EndUpdate;
      end;

      tempDataset.First;
      while not tempDataset.Eof do
      begin
        if ABPos(','+tempDataset.FindField('Tr_Code').AsString+',',','+tempRightValueCode+',')>0 then
        begin
          ABDeleteIni('PubClient'+'.FrequentlyDownList',tempDataset.FindField('Tr_Name').AsString,ABDefaultValuePath+'PubDefaultValue.ini');
        end;
        tempDataset.Next;
      end;

      for I := 0 to Group3.LinkCount - 1 do
      begin
        ABWriteInI('PubClient'+'.FrequentlyDownList',Group3.Links[i].Item.Caption,Group3.Links[i].Item.Hint,ABDefaultValuePath+'PubDefaultValue.ini');
      end;
    end;
  finally
    tempDataset.Free;
  end;
end;

procedure TABClientForm.ABcxPageControl1Click(Sender: TObject);
begin
  if Assigned(ABcxPageControl1.ActivePage) then
    CurFormToBring(ABcxPageControl1.ActivePage.Hint);
end;

procedure TABClientForm.ABdxStatusBar1Resize(Sender: TObject);
begin
  CalcFuncPageControlWidth;
end;

procedure TABClientForm.EditPasswordActionExecute(Sender: TObject);
begin
  ABUser.EditPassWord;
end;

procedure TABClientForm.ExitActionExecute(Sender: TObject);
begin
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form))  then
  begin
    ABShow('请先结束设计模式再关闭窗体.');
  end
  else
  begin
    CloseAllOpenFunc;
    close;
  end;
end;

procedure TABClientForm.LockActionExecute(Sender: TObject);
begin
  ClientLogin(ltLock);
end;

procedure TABClientForm.FormResize(Sender: TObject);
begin
  SetMainBack;
end;

procedure TABClientForm.N1Click(Sender: TObject);
begin
  //打开左则菜单
  dxDockSite.Visible:=True;
  SetMainBack;
end;

procedure TABClientForm.N3Click(Sender: TObject);
var
  i:LongInt;
  tempFormSelectFuncStrings:TStrings;
begin
  tempFormSelectFuncStrings:=TStringList.Create;
  TStringList(tempFormSelectFuncStrings).Sorted:=true;
  try
    if ABSelectCheckTreeView(
        ABUser.FuncTreeDataSet,
        'Fu_Ti_Guid',
        'Fu_Guid',
        'Fu_Name',
        False,

        nil,
        '',
        '',

        tempFormSelectFuncStrings,
        'Fu_FileName'
        ) then
    begin
      ABGetPubManualProgressBarForm.Title:='加载中';
      ABGetPubManualProgressBarForm.MainMin:=0;
      ABGetPubManualProgressBarForm.MainPosition:=0;
      ABGetPubManualProgressBarForm.MainMax:=tempFormSelectFuncStrings.Count;
      ABGetPubManualProgressBarForm.Run(true);
      try
        for I := 0 to tempFormSelectFuncStrings.Count-1 do
        begin
          if ABGetPubManualProgressBarForm.ExitRun then
            break;
          ABGetPubManualProgressBarForm.NextMainIndex;
          if ClientDataSet1.Locate('Fu_FileName',tempFormSelectFuncStrings[i],[]) then
            ABGetPubManualProgressBarForm.Remark:=ABLanguage.GetCodeStr('正在启动')+' '+ClientDataSet1.FieldByName('Fu_Name').AsString;

          OpenFunc(tempFormSelectFuncStrings[i],True,false);
        end;
      finally
        ABGetPubManualProgressBarForm.Stop;
      end;
    end;
  finally
    tempFormSelectFuncStrings.Free;
  end;
end;

procedure TABClientForm.N6Click(Sender: TObject);
begin
  //关闭左则菜单
  dxDockSite.Visible:=false;
  SetMainBack;
end;

procedure TABClientForm.N7Click(Sender: TObject);
begin
  DeleteUserINI;

  LoadFromIniFile;

  CustomToolBar1.Visible:=false;
  CustomToolBar2.Visible:=false;
  CustomToolBar3.Visible:=false;
  CustomToolBar4.Visible:=false;

  FUseDefaultINI:=True;
end;

procedure TABClientForm.PopupMenu_LeftMenuPopup(Sender: TObject);
var
  tempControl:TWinControl;
  tempActiveGroup:TdxNavBarGroup;
begin
  ABSetControlVisible(PopupMenu_LeftMenu.Items,false);
  tempControl:=ABGetMouseControl;
  if (Assigned(tempControl)) then
  begin
    if (tempControl = mydxNavBar) then
    begin
      tempActiveGroup:=mydxNavBar.HotTrackedGroup;
      if (Assigned(tempActiveGroup)) then
      begin
        if tempActiveGroup =Group1 then
        begin

        end
        else if tempActiveGroup =Group2 then
        begin
          N2.Visible:=true;
        end
        else if ABPubUser.IsAdmin  then
        begin
          if tempActiveGroup =Group3 then
          begin
            MenuItem2.Visible:=true;
          end
        end;
      end;
    end
    else if (tempControl = ABdxStatusBar1) then
    begin
      N1.Visible:=true;
      N6.Visible:=true;
      N6111.Visible:=true;
      N7.Visible:=true;

      //菜单弹出前初始化显示与关闭左则菜单的Enabled
      N1.Enabled:=not dxDockSite.Visible;
      N6.Enabled:=not N1.Enabled;
    end;
  end;
end;

procedure TABClientForm.AfreshLoginActionExecute(Sender: TObject);
begin
  ClientLogin(ltAfreshLogin);
  FBackTimerSencond:=0;
  FClientBackForm.SetBackType;
end;

procedure TABClientForm.ChangUserActionExecute(Sender: TObject);
begin
  ClientLogin(ltChangUser);

  if ABLocalParams.BackType=btMail then
  begin
    FClientBackForm.ABQuery1.Close;
    ABReFreshQuery(FClientBackForm.ABQuery1,[]);
  end;
  SetMainBack;

  FBackTimerSencond:=0;
  FClientBackForm.SetBackType;
end;

procedure TABClientForm.CloseFuncMenuClick(Sender: TObject);
begin
  if Assigned(ABcxPageControl1.ActivePage) then
  begin
    ABRegisterFile.GetFormByFileName(ABcxPageControl1.ActivePage.Hint).Close;
  end;
end;

procedure TABClientForm.CreateBackForm;
begin
  FClientBackForm:=TABClientBackForm.Create(self);
end;

procedure TABClientForm.PopupMenu_OpenFuncsClick(Sender: TObject);
var
  i: Integer;
begin
  CurFormToBring(TMenuItem(Sender).Hint);
  for i := 0 to ABcxPageControl1.PageCount - 1 do
  begin
    if AnsiCompareText(TMenuItem(Sender).Hint,ABcxPageControl1.Pages[i].Hint)=0 then
    begin
      ABcxPageControl1.ActivePage:= ABcxPageControl1.Pages[i];
      break;
    end;
  end;
end;

procedure TABClientForm.CurFormToBring(aFileName:string);
var
  tempForm:TForm;
begin
  tempForm:=ABRegisterFile.GetFormByFileName(aFileName);
  if Assigned(tempForm) then
  begin
    if tempForm.WindowState in [wsMinimized] then
    begin
      tempForm.WindowState := wsnormal;
    end;
    tempForm.BringToFront;
  end;
end;

procedure TABClientForm.UpToDatabaseActionExecute(Sender: TObject);
begin
  ABUpdateAllPkgs;
end;

procedure TABClientForm.WMDeviceChange(var Msg: TMessage);
var
  tempForm:TForm;
begin
  tempForm := ABRegisterFile.GetFormByFileName('AIC_UToolsG.bpl');
  if (Assigned(tempForm)) then
  begin
    SendMessage(tempForm.Handle, WM_DEVICECHANGE,Msg.WParam,Msg.LParam);
  end;
end;

procedure TABClientForm.ClientLogin(aType:TLongType;aLogined:boolean);
begin
  if (aType=ltChangUser) or
     (aType=ltAfreshLogin) then
  begin
    SaveCurUser;
    if (ABUser.Login(aType)) then
    begin
      LoadCurUser;
    end
    else
      close;
  end
  else if (aType=ltLock) then
  begin
    if not (ABUser.Login(aType)) then
      close;
  end;
end;

procedure TABClientForm.DeleteUserINI;
begin
  if ABCheckFileExists(ABCurUserPath+ABAppName+'_dxBarManager1.ini') then
    DeleteFile(ABCurUserPath+ABAppName+'_dxBarManager1.ini');

  if ABCheckFileExists(ABCurUserPath+ABAppName+'_dxDockingManager.ini') then
    DeleteFile(ABCurUserPath+ABAppName+'_dxDockingManager.ini');
end;

procedure TABClientForm.ChangeFuncVCLSkinActionExecute(Sender: TObject);
begin
  dxSkinController1.UseSkins:= TdxBarLargeButton(Sender).Index>=1;
  dxSkinController1.SkinName:=TdxBarLargeButton(Sender).Caption;
  cxLookAndFeelController1.SkinName:=TdxBarLargeButton(Sender).Caption;

  ABSetLocalParamsValue('CurSkinName',dxSkinController1.SkinName,'Params');
end;

procedure TABClientForm.FreeFormMsg(var Msg: Tmessage);
var
  tempFreeBplFileNames,tempCurFreeBplFileName:string;
  i,j:LongInt;
begin
  Sleep(10);
  tempFreeBplFileNames:=ABRegisterFile.FreeHaveFreeFlag;

  if tempFreeBplFileNames<>EmptyStr then
  begin
    for i := 1 to ABGetSpaceStrCount(tempFreeBplFileNames, ',') do
    begin
      tempCurFreeBplFileName := ABGetSpaceStr(tempFreeBplFileNames, i, ',');
      for j := ABcxPageControl1.PageCount - 1 downto 0 do
      begin
        if AnsiCompareText(tempCurFreeBplFileName,ABcxPageControl1.Pages[j].Hint)=0 then
        begin
          ABcxPageControl1.Pages[j].Free;
          Break;
        end;
      end;

      for j := N5.Count - 1 downto 0 do
      begin
        if AnsiCompareText(tempCurFreeBplFileName,N5.Items[j].Hint)=0 then
        begin
          N5.Items[j].Free;
          //N5.Delete(j);
          Break;
        end;
      end;
    end;
  end;
end;

procedure TABClientForm.ChangeLanguageActionExecute(Sender: TObject);
var
  tempStr1:string;
begin
  ABLanguage.Language:=TdxBarLargeButton(Sender).Description;

  if (ABCheckFileExists(ABPublicSetPath+'Dev_'+ABLanguage.LanguageFileName+'.ini')) then
  begin
    cxLocalizer1.FileName:= ABPublicSetPath + 'Dev_'+ABLanguage.LanguageFileName+'.ini';
    cxLocalizer1.Active:=true;
    tempStr1:=trim(ABReadTxtFirstRow(cxLocalizer1.FileName));
    tempStr1:=ABStringReplace(tempStr1,'[','');
    tempStr1:=ABStringReplace(tempStr1,']','');
    tempStr1:=trim(tempStr1);

    cxLocalizer1.Locale:=StrToIntDef(tempStr1,0);
  end
  else
  begin
    cxLocalizer1.Active:=False;
  end;
end;

procedure TABClientForm.DoTimer;
    {
  //自定义任务栏气泡提示
  procedure ShowBalloonTitle(aBalloonTitle, aBalloonHint: string;
                            aBalloonTimeout:longint=3000; aBalloonFlags: TBalloonFlags=bfInfo);
  begin
    TrayIcon1.BalloonTitle:=aBalloonTitle;
    TrayIcon1.BalloonHint:=aBalloonHint;
    TrayIcon1.BalloonTimeout:=aBalloonTimeout;
    TrayIcon1.BalloonFlags:=aBalloonFlags;
    TrayIcon1.ShowBalloonHint;

    if not TrayIcon1.Visible then
    begin
      TrayIcon1.Visible:=true;
    end;
  end;
    }
var
  tempNewMailTitles:string;
begin
  FSencond_FromHeartbeat:=FSencond_FromHeartbeat+ABTrunc(Timer1.Interval/1000);

  //客户端心跳处理
  if (FSetHeartbeat>0) and
     (FSencond_FromHeartbeat>=FSetHeartbeat)  then
  begin
    //心跳一下
    ABUpdateHeartbeatDatetime;
    FSencond_FromHeartbeat:=0;
  end;
  //背景窗体刷新做的事情
  if (Assigned(FClientBackForm))  then
  begin
    case ABLocalParams.BackType of
      //邮箱背景
      btMail:
      begin
        if (FClientBackForm.CheckBox1.Checked) then
        begin
          if (FBackTimerSencond<StrToIntDef(FClientBackForm.ABcxTextEdit1.Text,10)) then
          begin
            FBackTimerSencond:=FBackTimerSencond+ABTrunc(Timer1.Interval/1000);
          end
          else
          //显示新邮件的标题到右下角
          begin
            //刷新邮件
            ABReFreshQuery(FClientBackForm.ABQuery1,[]);

            tempNewMailTitles:=emptystr;
            //检测新邮件
            if FClientBackForm.ABQuery1.Active then
            begin
              FClientBackForm.ABQuery1.First;
              while not FClientBackForm.ABQuery1.Eof do
              begin
                ABAddstr(tempNewMailTitles,FClientBackForm.ABQuery1.FieldByName('MA_Title').AsString,ABEnterWrapStr);

                FClientBackForm.ABQuery1.Next;
              end;

              if tempNewMailTitles<>emptystr then
              begin
                //ShowBalloonTitle('新邮件',tempNewMailTitles);
                //ABShow_NotModel(tempStr1);
              end;
            end;
            FBackTimerSencond:=0
          end;
        end;
      end;
      //自定义背景
      btCustom:
      begin
        if (FClientBackForm.ABcxCheckBox1.Checked) then
        begin
          if (FBackTimerSencond<StrToIntDef(FClientBackForm.ABcxTextEdit2.Text,10)) then
          begin
            FBackTimerSencond:=FBackTimerSencond+ABTrunc(Timer1.Interval/1000);
          end
          else
          begin
            CustomRefresh(FClientBackForm.ABQuery2);
            FBackTimerSencond:=0
          end;
        end;
      end;
    end;
  end;
end;

procedure TABClientForm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  try
    DoTimer
  finally
    Timer1.Enabled:=true;
  end;
end;

function TABClientForm.CustomRefresh(aDataset:TDataSet):Boolean;
begin
  ABReFreshQuery(aDataset,[]);
  result:= true;
end;

procedure TABClientForm.Timer2Timer(Sender: TObject);
begin
  //窗体大小与位置如果放在InitUserInfo中，在D7下设置的位置有问题，放在定时器中则没问题，暂时先这样解决
  Timer2.Enabled:=false;
  //设置一些其它的窗体布局
  Left:=StrToIntDef(ABReadInI(Name,'Left', ABCurUserPath + ABAppName+'_Setup.ini'),Left);
  Top:=StrToIntDef(ABReadInI(Name,'Top', ABCurUserPath + ABAppName+'_Setup.ini'),Top);
  Width:=StrToIntDef(ABReadInI(Name,'Width', ABCurUserPath + ABAppName+'_Setup.ini'),Width);
  Height:=StrToIntDef(ABReadInI(Name,'Height', ABCurUserPath + ABAppName+'_Setup.ini'),Height);
end;

end.


