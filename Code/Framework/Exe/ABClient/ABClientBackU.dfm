object ABClientBackForm: TABClientBackForm
  Left = 482
  Top = 145
  BorderStyle = bsSingle
  ClientHeight = 551
  ClientWidth = 732
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel_Flow: TPanel
    Left = 320
    Top = 1
    Width = 369
    Height = 168
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 3
    Visible = False
    object dxFlowChart1: TdxFlowChart
      Left = 139
      Top = 0
      Width = 230
      Height = 168
      Align = alClient
      Color = clWindow
      Images = ABClientForm.ImageList_MainMenu
      Options = [fcoCanSelect]
      PopupMenu = PopupMenu1
      OnDblClick = dxFlowChart1DblClick
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 131
      Height = 168
      Align = alLeft
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      TabOrder = 1
      object Panel2: TPanel
        Left = 1
        Top = 30
        Width = 129
        Height = 137
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          129
          137)
        object Label1: TLabel
          Left = 3
          Top = 117
          Width = 66
          Height = 13
          AutoSize = False
          Caption = #20840#23616#32972#26223':'
        end
        object ABcxButton3: TABcxButton
          Left = 2
          Top = 5
          Width = 123
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Caption = #22686#21152
          LookAndFeel.Kind = lfFlat
          TabOrder = 0
          OnClick = ABcxButton3Click
          ShowProgressBar = False
        end
        object ABcxButton4: TABcxButton
          Left = 2
          Top = 26
          Width = 123
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Caption = #20462#25913
          LookAndFeel.Kind = lfFlat
          TabOrder = 1
          OnClick = ABcxButton4Click
          ShowProgressBar = False
        end
        object ABcxButton5: TABcxButton
          Left = 2
          Top = 47
          Width = 123
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Caption = #21024#38500
          LookAndFeel.Kind = lfFlat
          TabOrder = 2
          OnClick = ABcxButton5Click
          ShowProgressBar = False
        end
        object ABcxColorComboBox1: TABcxColorComboBox
          Left = 71
          Top = 113
          Anchors = [akLeft, akTop, akRight]
          Properties.CustomColors = <>
          Properties.OnChange = ABcxColorComboBox1PropertiesChange
          Style.BorderStyle = ebsFlat
          TabOrder = 3
          Width = 54
        end
        object ABcxButton6: TABcxButton
          Left = 2
          Top = 89
          Width = 123
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Caption = #19978#20256
          LookAndFeel.Kind = lfFlat
          TabOrder = 4
          OnClick = ABcxButton6Click
          ShowProgressBar = False
        end
        object ABcxButton7: TABcxButton
          Left = 2
          Top = 68
          Width = 123
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Caption = #35774#32622#35843#29992#20851#31995
          LookAndFeel.Kind = lfFlat
          TabOrder = 5
          OnClick = ABcxButton7Click
          ShowProgressBar = False
        end
      end
      object ABcxListBox1: TABcxListBox
        Left = 1
        Top = 1
        Width = 129
        Height = 29
        Align = alClient
        ItemHeight = 13
        Style.BorderStyle = cbsFlat
        TabOrder = 1
        OnClick = ABcxListBox1Click
      end
    end
    object ABcxSplitter1: TABcxSplitter
      Left = 131
      Top = 0
      Width = 8
      Height = 168
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Panel1
    end
  end
  object Panel_Picture: TPanel
    Left = 131
    Top = 61
    Width = 185
    Height = 41
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object Image: TImage
      Left = 0
      Top = 0
      Width = 185
      Height = 41
      Align = alClient
      Stretch = True
    end
  end
  object Panel_Mail: TPanel
    Left = 22
    Top = 173
    Width = 609
    Height = 217
    TabOrder = 2
    Visible = False
    object ABDBcxGrid1: TABcxGrid
      Left = 1
      Top = 1
      Width = 607
      Height = 182
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        OnDblClick = ABcxGridDBBandedTableView1DblClick
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource1
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object cxGridLevel1: TcxGridLevel
        GridView = ABcxGridDBBandedTableView1
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 183
      Width = 607
      Height = 33
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object CheckBox1: TABcxCheckBox
        Tag = 91
        Left = 0
        Top = 6
        Caption = #33258#21160#21047#26032
        ParentFont = False
        Properties.NullStyle = nssUnchecked
        TabOrder = 0
        Transparent = True
        Width = 72
      end
      object ABcxTextEdit1: TABcxTextEdit
        Tag = 91
        Left = 143
        Top = 6
        TabOrder = 1
        Text = '10'
        Width = 63
      end
      object ABcxLabel1: TABcxLabel
        Left = 78
        Top = 8
        Caption = #38388#38548'('#31186') '
        ParentFont = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clBlack
        Style.Font.Height = -11
        Style.Font.Name = 'Times New Roman'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        AnchorY = 17
      end
      object ABcxButton1: TABcxButton
        Left = 209
        Top = 6
        Width = 64
        Height = 21
        Caption = #21047#26032
        LookAndFeel.Kind = lfFlat
        TabOrder = 2
        OnClick = ABcxButton1Click
        ShowProgressBar = False
      end
    end
  end
  object Panel_Null: TPanel
    Left = 111
    Top = 14
    Width = 185
    Height = 41
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    Visible = False
  end
  object Panel_Custom: TPanel
    Left = 96
    Top = 352
    Width = 609
    Height = 217
    TabOrder = 4
    Visible = False
    object ABDBcxGrid2: TABcxGrid
      Left = 1
      Top = 1
      Width = 607
      Height = 182
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource2
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object cxGridLevel2: TcxGridLevel
        GridView = ABcxGridDBBandedTableView2
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 183
      Width = 607
      Height = 33
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object ABcxCheckBox1: TABcxCheckBox
        Tag = 91
        Left = 0
        Top = 6
        Caption = #33258#21160#21047#26032
        ParentFont = False
        Properties.NullStyle = nssUnchecked
        TabOrder = 0
        Transparent = True
        Width = 72
      end
      object ABcxTextEdit2: TABcxTextEdit
        Tag = 91
        Left = 143
        Top = 6
        TabOrder = 1
        Text = '10'
        Width = 63
      end
      object ABcxLabel2: TABcxLabel
        Left = 78
        Top = 8
        Caption = #38388#38548'('#31186') '
        ParentFont = False
        Style.Font.Charset = ANSI_CHARSET
        Style.Font.Color = clBlack
        Style.Font.Height = -11
        Style.Font.Name = 'Times New Roman'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Vert = taVCenter
        Transparent = True
        AnchorY = 17
      end
      object ABcxButton2: TABcxButton
        Left = 209
        Top = 6
        Width = 64
        Height = 21
        Caption = #21047#26032
        LookAndFeel.Kind = lfFlat
        TabOrder = 2
        OnClick = ABcxButton2Click
        ShowProgressBar = False
      end
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select *'
      'from  ABSys_Org_Mail '
      'where Ma_OwnerOperator = :ABUser_Code and Ma_State='#39'Inbox'#39
      '')
    SqlUpdateDatetime = 42451.272071898150000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Mail')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Mail')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 128
    Top = 16
    ParamData = <
      item
        Name = 'ABUSER_CODE'
        ParamType = ptInput
      end>
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 112
    Top = 232
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2
    Left = 112
    Top = 360
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select 11')
    SqlUpdateDatetime = 42148.640344918980000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 272
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    OwnerDraw = True
    Left = 498
    Top = 127
    object N1: TMenuItem
      Caption = #22686#21152#27969#31243#22270
    end
    object N2: TMenuItem
      Caption = #20462#25913#27969#31243#22270
    end
    object N3: TMenuItem
      Caption = #21024#38500#27969#31243#22270
    end
  end
end
