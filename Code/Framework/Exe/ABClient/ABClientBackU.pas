unit ABClientBackU;

interface

uses
  ABPubFormU,
  ABPubFuncU,
  ABPubLocalParamsU,
  ABPubVarU,
  ABPubConstU,
  ABPubUserU,
  ABPubInPutStrU,
  ABPubMessageU,
  ABPubSelectNameAndValuesU,

  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdQueryU,

  ABFrameworkQueryU,
  ABFrameworkcxGridU,
  ABFrameworkControlU,
  ABFrameworkDictionaryQueryU,

  dxFcEdit,dxflchrt,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  FireDAC.Stan.Intf, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Vcl.StdCtrls,
  cxButtons, cxLabel, cxTextEdit, cxCheckBox, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, Vcl.Controls,
  Vcl.ExtCtrls, System.Classes ,

  SysUtils,Graphics,
  Windows,Messages, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGroupBox, cxRadioGroup, cxDBEdit, cxMaskEdit,
  cxDropDownEdit, cxColorComboBox, Vcl.ComCtrls, cxSplitter, cxListView,
  cxListBox;

const
  //主窗体最大最小化及大小改变的消息
  ABMainFormFIXMAXIMIZED_Msg = WM_USER + 100;

type
  TABClientBackForm = class(TABPubForm)
    Panel_Flow: TPanel;
    Panel_Picture: TPanel;
    Panel_Mail: TPanel;
    Image: TImage;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    Panel4: TPanel;
    CheckBox1: TABcxCheckBox;
    ABcxTextEdit1: TABcxTextEdit;
    ABcxLabel1: TABcxLabel;
    ABcxButton1: TABcxButton;
    Panel_Null: TPanel;
    Panel_Custom: TPanel;
    ABDBcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    Panel6: TPanel;
    ABcxCheckBox1: TABcxCheckBox;
    ABcxTextEdit2: TABcxTextEdit;
    ABcxLabel2: TABcxLabel;
    ABcxButton2: TABcxButton;
    ABDatasource2: TABDatasource;
    ABQuery2: TABQuery;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    dxFlowChart1: TdxFlowChart;
    Panel1: TPanel;
    Panel2: TPanel;
    ABcxSplitter1: TABcxSplitter;
    ABcxButton3: TABcxButton;
    ABcxButton4: TABcxButton;
    ABcxButton5: TABcxButton;
    ABcxColorComboBox1: TABcxColorComboBox;
    ABcxButton6: TABcxButton;
    ABcxListBox1: TABcxListBox;
    Label1: TLabel;
    ABcxButton7: TABcxButton;
    procedure FormCreate(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure ABcxGridDBBandedTableView1DblClick(Sender: TObject);
    procedure ABcxListBox1Click(Sender: TObject);
    procedure ABcxButton3Click(Sender: TObject);
    procedure ABcxButton4Click(Sender: TObject);
    procedure ABcxButton5Click(Sender: TObject);
    procedure ABcxButton6Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ABcxColorComboBox1PropertiesChange(Sender: TObject);
    procedure ABcxButton7Click(Sender: TObject);
    procedure dxFlowChart1DblClick(Sender: TObject);
  private
    FFileName:string;
    { Private declarations }
    procedure WMFixMaximized(var Msg: TMessage); message ABMainFormFIXMAXIMIZED_Msg;
    procedure LoadFlow(aFileName: String);
    procedure UpdateFlowFile(aFileName: String);
  public
    procedure SetBackType;
    { Public declarations }
  end;


implementation
uses ABClientU;
{$R *.dfm}

procedure TABClientBackForm.WMFixMaximized(var Msg: TMessage);
begin
  Height := Msg.WParam -4;
  Width  := Msg.LParam -4;
end;

procedure TABClientBackForm.ABcxGridDBBandedTableView1DblClick(
  Sender: TObject);
begin
  if not ABQuery1.IsEmpty then
    TABClientForm(Owner).OpenMail(ABQuery1.FieldByName('MA_Guid').AsString);
end;

procedure TABClientBackForm.ABcxButton1Click(Sender: TObject);
begin
  //邮件刷新
  ABReFreshQuery(ABQuery1,[]);
end;

procedure TABClientBackForm.ABcxButton2Click(Sender: TObject);
begin
  //自定义刷新
  Sleep(10);
  ABReFreshQuery(ABQuery2,[]);
end;

procedure TABClientBackForm.FormCreate(Sender: TObject);
begin
  if not DirectoryExists(ABSoftSetPath + 'Flows') then
    ForceDirectories(ABSoftSetPath + 'Flows');

  Windows.SetWindowLong(Handle, GWL_STYLE,
      Windows.GetWindowLong(Handle, GWL_STYLE)
      and (not WS_CAPTION) and (not WS_SIZEBOX));

  SetBackType;
end;

procedure TABClientBackForm.SetBackType;
var
  tempCurPanel:TPanel;
  tempStrings:TStrings;
  I: Integer;
  tempBackColor:string;
  tempOnChange: TNotifyEvent;
begin
  //设置MDI主窗体的背景
  tempCurPanel:=nil;
  Panel_Null     .Visible:=False;
  Panel_Null     .Align:=alNone;
  Panel_Picture  .Visible:=False;
  Panel_Picture  .Align:=alNone;
  Panel_Mail     .Visible:=False;
  Panel_Mail     .Align:=alNone;
  Panel_Flow     .Visible:=False;
  Panel_Flow     .Align:=alNone;
  Panel_Custom   .Visible:=False;
  Panel_Custom   .Align:=alNone;

  case ABLocalParams.BackType of
    btNull:
    begin
      tempCurPanel:=Panel_Null;
    end;
    btPicture:
    begin
      tempCurPanel:=Panel_Picture;
      if ABCheckFileExists(ABSoftSetPath+'MainBack.jpg') then
      begin
        Image.Picture.LoadFromFile(ABSoftSetPath + 'MainBack.jpg');
      end
      else  if ABCheckFileExists(ABSoftSetPath+'MainBack.jpeg') then
      begin
        Image.Picture.LoadFromFile(ABSoftSetPath + 'MainBack.jpeg');
      end
      else  if ABCheckFileExists(ABSoftSetPath+'MainBack.bmp') then
      begin
        Image.Picture.LoadFromFile(ABSoftSetPath + 'MainBack.bmp');
      end
      else
      begin
        Image.Visible:=false;
      end;
    end;
    btMail:
    begin
      tempCurPanel:=Panel_Mail;

      ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],ABQuery1);
    end;
    btFlow:
    begin
      //加载流程图
      tempCurPanel:=Panel_Flow;
      if ABPubParamDataset.Locate('Pa_Name','ActiveFlow',[])   then
      begin
        FFileName:=ABPubParamDataset.FieldByName('Pa_Value').AsString;
      end;
      if ABPubParamDataset.Locate('Pa_Name','ActiveFlowBackColor',[])   then
      begin
        tempBackColor:=ABPubParamDataset.FieldByName('Pa_Value').AsString;
      end;

      if (tempBackColor<>emptystr) then
      begin
        tempOnChange:=ABcxColorComboBox1.Properties.OnChange;
        ABcxColorComboBox1.Properties.OnChange:=nil;
        try
          ABcxColorComboBox1.ColorValue:=StrToInt64Def(tempBackColor,clWindow);
          dxFlowChart1.Color:=ABcxColorComboBox1.ColorValue;
        finally
          ABcxColorComboBox1.Properties.OnChange:=tempOnChange;
        end;
      end;

      Panel1.Visible:=false;
      ABcxSplitter1.Visible:=false;
      if ABPubUser.IsAdmin  then
      begin
        Panel1.Visible:=True;
        ABcxSplitter1.Visible:=True;
        tempStrings:=TStringlist.Create;
        try
          ABGetFileList(tempStrings,ABSoftSetPath + 'Flows',[],1,True,nil,False);
          for i := 0 to tempStrings.Count - 1 do
          begin
            if AnsiCompareText(tempStrings[i],'FlowObjectToFunc.ini')<>0 then
              ABcxListBox1.Items.Add(tempStrings[i]);
          end;

          if (FFileName<>emptystr) then
          begin
            if ABcxListBox1.Items.IndexOf(FFileName)>=0 then
              ABcxListBox1.ItemIndex:=ABcxListBox1.Items.IndexOf(FFileName);
            LoadFlow(FFileName);
          end;
        finally
          tempStrings.Free;
        end;
      end
      else
      begin
        if (FFileName<>emptystr) then
          LoadFlow(FFileName);
      end;
    end;
    btCustom:
    begin
      tempCurPanel:=Panel_Custom;

      ABInitFormDataSet([],[ABcxGridDBBandedTableView2],[],ABQuery2);
    end;
  end;
  if Assigned(tempCurPanel) then
  begin
    tempCurPanel.Visible:=true;
    tempCurPanel.Align:=alClient;
  end;
end;

procedure TABClientBackForm.ABcxListBox1Click(Sender: TObject);
begin
  if ABcxListBox1.ItemIndex>=0 then
  begin
    LoadFlow(ABcxListBox1.Items[ABcxListBox1.ItemIndex]);
  end
end;

procedure TABClientBackForm.dxFlowChart1DblClick(Sender: TObject);
var
  tempFileName: string;
begin
  if Assigned(dxFlowChart1.SelectedObject) then
  begin
    tempFileName:=ABReadInI(FFileName,dxFlowChart1.SelectedObject.Text, ABSoftSetPath + 'Flows\FlowObjectToFunc.ini');
    if tempFileName<>EmptyStr then
    begin
      if TABClientForm(Owner).ClientDataSet1.Locate('Fu_FileName',tempFileName,[]) then
      begin
        TABClientForm(Owner).OpenFunc(tempFileName);
      end;
    end;
  end;
end;

procedure TABClientBackForm.LoadFlow(aFileName: String);
begin
  if (aFileName<>emptystr) then
  begin
    FFileName:= aFileName;
    aFileName:=ABSoftSetPath + 'Flows\'+aFileName;
    if (ABCheckFileExists(aFileName)) then
    begin
      dxFlowChart1.LoadFromFile(aFileName);
    end
    else
    begin
      dxFlowChart1.Clear;
    end;
  end;
end;

procedure TABClientBackForm.ABcxButton3Click(Sender: TObject);
var
  tempStr: string;
begin
  if ABInPutStr('输入', tempStr) then
  begin
    if (tempStr<>emptystr) then
    begin
      ABcxListBox1.Items.Add(tempStr);
      ABcxListBox1.ItemIndex:=ABcxListBox1.Items.Count-1;
      dxFlowChart1.Clear;

      if ShowFlowChartEditor(dxFlowChart1, '增加流程图') then
      begin
        dxFlowChart1.SaveToFile(ABSoftSetPath + 'Flows\'+ABcxListBox1.Items[ABcxListBox1.ItemIndex]);
      end;
    end;
  end;
end;

procedure TABClientBackForm.ABcxButton4Click(Sender: TObject);
begin
  if ABcxListBox1.ItemIndex>=0 then
  begin
    ABcxListBox1Click(ABcxListBox1);
    if ShowFlowChartEditor(dxFlowChart1, '修改流程图') then
    begin
      dxFlowChart1.SaveToFile(ABSoftSetPath + 'Flows\'+ABcxListBox1.Items[ABcxListBox1.ItemIndex]);
    end;
  end;
end;

procedure TABClientBackForm.ABcxButton5Click(Sender: TObject);
begin
  if ABcxListBox1.ItemIndex>=0 then
  begin
    if ABShow('确认删除[%s]吗?',[ABcxListBox1.Items[ABcxListBox1.ItemIndex]],['是','否'],1)=1 then
    begin
      ABDeleteFile(ABSoftSetPath + 'Flows\'+ABcxListBox1.Items[ABcxListBox1.ItemIndex]);
      ABcxListBox1.Items.Delete(ABcxListBox1.ItemIndex);
      ABcxListBox1Click(ABcxListBox1);
    end;
  end;
end;

procedure TABClientBackForm.UpdateFlowFile(aFileName: String);
begin
  ABExecSQL('Main','exec Proc_SetPublicPkgNewVersion  '+QuotedStr(aFileName)+',''ABSoft_Set\ABClientP\Flows\'' ');
  ABUpFileToField('Main','ABSys_Org_Function','FU_Pkg',
                  ' where FU_FileName='+ABQuotedStr(aFileName),
                  ABSoftSetPath + 'Flows\'+aFileName);
end;

procedure TABClientBackForm.ABcxButton6Click(Sender: TObject);
var
  tempStrings:TStrings;
  I: Integer;
begin
  tempStrings:=TStringlist.Create;
  try
    ABGetFileList(tempStrings,ABSoftSetPath + 'Flows',[],1,True,nil,False);
    for i := 0 to tempStrings.Count - 1 do
    begin
      UpdateFlowFile(tempStrings[i]);
    end;
  finally
    tempStrings.Free;
  end;
end;

procedure TABClientBackForm.ABcxColorComboBox1PropertiesChange(Sender: TObject);
begin
  dxFlowChart1.Color:=ABcxColorComboBox1.ColorValue;
  if not ABPubParamDataset.Locate('Pa_Name','ActiveFlowBackColor',[])   then
  begin
    ABExecSQL('main',' insert into ABSys_Org_Parameter(PA_Guid,PA_SysUse,PA_Name,PA_Value,PubID) '+
                     ' values(dbo.Func_GetOrderGuid(),1,''ActiveFlowBackColor'','+QuotedStr(IntToStr(ABcxColorComboBox1.ColorValue))+',dbo.Func_GetOrderGuid())');
  end
  else
  begin
    ABExecSQL('main',' update ABSys_Org_Parameter'+
                     ' set PA_Value= '+QuotedStr(IntToStr(ABcxColorComboBox1.ColorValue))+
                     ' where PA_Name=''ActiveFlowBackColor'' ');
  end;
end;

procedure TABClientBackForm.FormDestroy(Sender: TObject);
begin
  if ABPubUser.IsAdmin  then
  begin
    if ABLocalParams.BackType = btFlow then
    begin
      if ABcxListBox1.ItemIndex>=0 then
      begin
        if not ABPubParamDataset.Locate('Pa_Name','ActiveFlow',[])   then
        begin
          ABExecSQL('main',' insert into ABSys_Org_Parameter(PA_Guid,PA_SysUse,PA_Name,PA_Value,PubID) '+
                           ' values(dbo.Func_GetOrderGuid(),1,''ActiveFlow'','+QuotedStr(ABcxListBox1.Items[ABcxListBox1.ItemIndex])+',dbo.Func_GetOrderGuid())');
        end
        else
        begin
          ABExecSQL('main',' update ABSys_Org_Parameter'+
                           ' set PA_Value= '+QuotedStr(ABcxListBox1.Items[ABcxListBox1.ItemIndex])+
                           ' where PA_Name=''ActiveFlow'' ');
        end;
      end;
    end;
  end;
end;

procedure TABClientBackForm.ABcxButton7Click(Sender: TObject);
var
  I,j: Integer;
  tempStr:string;
  temStrings:TStrings;
  temStrings1:TStrings;
  temStrings2:TStrings;
  tempEdit:boolean;
begin
  if ABcxListBox1.ItemIndex>=0 then
  begin
    tempEdit:=False;
    temStrings:= TStringList.Create;
    temStrings1:= TStringList.Create;
    temStrings2:= TStringList.Create;
    TABClientForm(Owner).ClientDataSet1.DisableControls;
    try
      for I := 0 to dxFlowChart1.ObjectCount-1 do
      begin
        temStrings1.Add(dxFlowChart1.Objects[i].Text);
      end;

      TABClientForm(Owner).ClientDataSet1.First;
      while not TABClientForm(Owner).ClientDataSet1.Eof do
      begin
        temStrings2.Add(TABClientForm(Owner).ClientDataSet1.FindField('FU_FileName').AsString);

        TABClientForm(Owner).ClientDataSet1.Next;
      end;

      ABReadInI(ABcxListBox1.Items[ABcxListBox1.ItemIndex], ABSoftSetPath + 'Flows\FlowObjectToFunc.ini',temStrings);
      tempStr:=ABStringsToStrs(temStrings);
      if ABSelectNameAndValues(temStrings1,temStrings2,tempStr,',','设置调用关系','流程图对象','功能文件名') then
      begin
        ABStrsToStrings(tempStr,',',temStrings);
        for I := 0 to temStrings.Count-1 do
        begin
          if StrToIntDef(Trim(ABStringReplace(temStrings.Names[i],'Object','')),-9999)<>-9999 then
          begin
            if TABClientForm(Owner).ClientDataSet1.Locate('Fu_FileName',temStrings.ValueFromIndex[i],[]) then
            begin
              for j := 0 to dxFlowChart1.ObjectCount-1 do
              begin
                if AnsiCompareText(temStrings.Names[i],dxFlowChart1.Objects[j].Text)=0 then
                begin
                  tempEdit:=true;
                  temStrings[i]:=TABClientForm(Owner).ClientDataSet1.FindField('FU_Name').AsString+'='+temStrings.ValueFromIndex[i];
                  dxFlowChart1.Objects[j].Text:=temStrings.Names[i];
                  break;
                end;
              end;
            end;
          end;
        end;
        ABWriteInI(ABcxListBox1.Items[ABcxListBox1.ItemIndex],ABSoftSetPath + 'Flows\FlowObjectToFunc.ini',temStrings);

        ABExecSQL('Main','exec Proc_SetPublicPkgNewVersion  '+QuotedStr('FlowObjectToFunc.ini')+',''ABSoft_Set\ABClientP\Flows\'' ');
        ABUpFileToField('Main','ABSys_Org_Function','FU_Pkg',
                        ' where FU_FileName='+ABQuotedStr('FlowObjectToFunc.ini'),
                        ABSoftSetPath + 'Flows\FlowObjectToFunc.ini');
        if tempEdit then
        begin
          dxFlowChart1.SaveToFile(ABSoftSetPath + 'Flows\'+ABcxListBox1.Items[ABcxListBox1.ItemIndex]);
          UpdateFlowFile(ABcxListBox1.Items[ABcxListBox1.ItemIndex]);
        end;
      end;
    finally
      TABClientForm(Owner).ClientDataSet1.EnableControls;
      temStrings.Free;
      temStrings1.Free;
      temStrings2.Free;
    end;
  end;
end;

end.

