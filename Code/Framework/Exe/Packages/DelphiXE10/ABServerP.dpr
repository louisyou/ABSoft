program ABServerP;

uses
  Forms,
  Main in '..\..\ABServer\Main.pas' {MainForm},
  ServerContainerUnit1 in '..\..\ABServer\ServerContainerUnit1.pas',
  ServerMethodsUnit1 in '..\..\ABServer\ServerMethodsUnit1.pas' {ABServer: TDSServerModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TServerContainer1, ServerContainer1);
  Application.Run;
end.

