program ABClientP;

uses
  Forms,
  Windows,
  SysUtils,

  ABPubUserU,
  ABPubLocalParamsU,
  ABPubVarU,
  ABPubLogU,
  ABPubmessageU,
  ABPubStartFormU,
  ABPubManualProgressBarU,

  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdDBU,

  ABFrameworkUserU,
  ABClientU in '..\..\ABClient\ABClientU.pas' {ABClientForm},
  ABClientBackU in '..\..\ABClient\ABClientBackU.pas' {ABClientBackForm};

{$R *.res}

var
  tempRun:boolean;
begin
  Application.Initialize;

  tempRun:=true;
  if ABCreateConns then
  begin
    ABLocalParams.DownPkg:=True;
    if ABPubParamDataset.Locate('Pa_Name','AutoDownPkg',[])   then
    begin
      if (AnsiCompareText(ABPubParamDataset.FieldByName('Pa_Value').AsString,'Down')<>0) and
         (AnsiCompareText(ABPubParamDataset.FieldByName('Pa_Value').AsString,'Auto')<>0) then
      begin
        ABLocalParams.DownPkg:=False;
      end;
    end;

    if ABLocalParams.DownPkg then
    begin
      if ABPubParamDataset.Locate('Pa_Name','FuncUpdateCount',[])   then
      begin
        if (ABLocalParams.FuncUpdateCount<ABPubParamDataset.FieldByName('Pa_Value').AsInteger) then
        begin
          tempRun:=False;
        end
        //当本地的更新序号很大时（比服务器的大），则在打开功能模块时不更新（唯一一种在具体客户端不更新打开模块的方法）
        else if (ABLocalParams.FuncUpdateCount>ABPubParamDataset.FieldByName('Pa_Value').AsInteger) then
        begin
          ABSetLocalParamsValue('FuncUpdateCount',inttostr(ABPubParamDataset.FieldByName('Pa_Value').AsInteger),'Params');
          ABLocalParams.DownPkg:=false;
        end;
      end;
    end;
  end;

  if (tempRun) or
     (ABPubUser.IsNoCheckHost) then
  begin
    if ABUser.Login(ltLongin) then
    begin
      try
        ABGetPubManualProgressBarForm.RefreshAll;
        ABGetPubManualProgressBarForm.NextMainIndex;

        Application.CreateForm(TABClientForm, ABClientForm);
      finally
        ABGetPubManualProgressBarForm.Stop;
        ABCloseStartForm;
      end;
    end;
  end
  else
  begin
    abshow('客户端版本需要升级后才能运行，请检查客户端是否是自动下载客户端.');
  end;

  Application.Run;
end.

