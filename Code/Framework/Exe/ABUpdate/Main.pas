unit Main;

interface

uses
  ABpubInPutPassWordU,
  ABPubConstU,
  abpubformu,
  abpubFuncU,
  abpubvarU,
  abpubDBU,
  abpubmessageU,
  ABPubServiceU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdConnDatabaseU,
  ABThirdCustomQueryU,

  stdconvs,
  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,DBCtrls,Mask,Buttons,ExtCtrls,ComCtrls,DateUtils,DB,SqlExpr,FMTBcd,
  TypInfo,Spin,HTTPApp,DBWeb,DBXpressWeb,ADODB,DBClient,SimpleDS,Provider,Menus,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TABUpdateForm = class(TABPubForm)
    Splitter1: TSplitter;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton2: TButton;
    SpeedButton3: TButton;
    BitBtn1: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    SpeedButton1: TButton;
    SpeedButton4: TButton;
    BitBtn3: TBitBtn;
    Edit3: TEdit;
    Edit4: TEdit;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    Label19: TLabel;
    ComboBox5: TComboBox;
    FireDACConnection_old: TFireDACConnection;
    FireDACConnection_New: TFireDACConnection;
    Panel8: TPanel;
    PageControl2: TPageControl;
    TabSheet11: TTabSheet;
    Panel2: TPanel;
    GroupBox3: TGroupBox;
    Memo_log: TMemo;
    GroupBox4: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo_updateSQL1: TMemo;
    GroupBox5: TGroupBox;
    Label9: TLabel;
    Edit_Caption1: TEdit;
    TabSheet2: TTabSheet;
    Memo_updateSQL2: TMemo;
    GroupBox6: TGroupBox;
    Label10: TLabel;
    Edit_Caption2: TEdit;
    TabSheet3: TTabSheet;
    Memo_updateSQL3: TMemo;
    GroupBox7: TGroupBox;
    Label11: TLabel;
    Edit_Caption3: TEdit;
    TabSheet4: TTabSheet;
    Memo_updateSQL4: TMemo;
    GroupBox8: TGroupBox;
    Label12: TLabel;
    Edit_Caption4: TEdit;
    TabSheet5: TTabSheet;
    Memo_updateSQL5: TMemo;
    GroupBox9: TGroupBox;
    Label13: TLabel;
    Edit_Caption5: TEdit;
    TabSheet6: TTabSheet;
    Memo_updateSQL6: TMemo;
    GroupBox10: TGroupBox;
    Label14: TLabel;
    Edit_Caption6: TEdit;
    TabSheet7: TTabSheet;
    Memo_updateSQL7: TMemo;
    GroupBox11: TGroupBox;
    Label15: TLabel;
    Edit_Caption7: TEdit;
    TabSheet8: TTabSheet;
    Memo_updateSQL8: TMemo;
    GroupBox12: TGroupBox;
    Label16: TLabel;
    Edit_Caption8: TEdit;
    TabSheet9: TTabSheet;
    Memo_updateSQL9: TMemo;
    GroupBox13: TGroupBox;
    Label17: TLabel;
    Edit_Caption9: TEdit;
    TabSheet10: TTabSheet;
    Memo_updateSQL10: TMemo;
    GroupBox14: TGroupBox;
    Label18: TLabel;
    Edit_Caption10: TEdit;
    GroupBox15: TGroupBox;
    CheckBox_Run1: TCheckBox;
    CheckBox_Run2: TCheckBox;
    CheckBox_Run3: TCheckBox;
    CheckBox_Run4: TCheckBox;
    CheckBox_Run5: TCheckBox;
    CheckBox_Run6: TCheckBox;
    CheckBox_Run7: TCheckBox;
    CheckBox_Run8: TCheckBox;
    CheckBox_Run9: TCheckBox;
    CheckBox_Run10: TCheckBox;
    CheckBox2: TCheckBox;
    Panel3: TPanel;
    Button_Run: TButton;
    Button_OpenSetup: TButton;
    Button_CloseSetup: TButton;
    Button_Stop: TButton;
    Animate1: TAnimate;
    FireDACQuery1_New: TABThirdReadDataQuery;
    FireDACQuery1_Old: TABThirdReadDataQuery;
    FireDACQuery2_New: TABThirdReadDataQuery;
    FireDACQuery3_New: TABThirdReadDataQuery;
    FireDACQuery2_Old: TABThirdReadDataQuery;
    FireDACQuery3_Old: TABThirdReadDataQuery;
    Label25: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Memo_updateSQL1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboBox5Change(Sender: TObject);
    procedure Button_RunClick(Sender: TObject);
    procedure Button_StopClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button_CloseSetupClick(Sender: TObject);
    procedure Button_OpenSetupClick(Sender: TObject);
    procedure PageControl2Changing(Sender: TObject; var AllowChange: Boolean);
  private                 
    FOldDatabaseType:TABDatabaseType;

    FtempNewQuery1,
    FtempNewQuery2,
    FtempNewQuery3,

    FtempOldQuery1,
    FtempOldQuery2,
    FtempOldQuery3:TDataSet;

    procedure SetCanSetup(aBoolean: boolean);
    function CheckConn(aShowAbortMsg:boolean): boolean;
    procedure DoOldToNewUpdate;
    function RefreshStop:boolean;
    procedure RefreshConn;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABUpdateForm: TABUpdateForm;

implementation

{$R *.dfm}

procedure TABUpdateForm.RefreshConn;
begin
  FOldDatabaseType:=  TABDatabaseType(GetEnumValue(Typeinfo(TABDatabaseType),'dt'+ComboBox5.Text));

  FireDACConnection_Old.Close;
  FtempOldQuery1:=FireDACQuery1_Old;
  FtempOldQuery2:=FireDACQuery2_Old;
  FtempOldQuery3:=FireDACQuery3_Old;

  FireDACConnection_New.Close;
  FtempNewQuery1:=FireDACQuery1_New;
  FtempNewQuery2:=FireDACQuery2_New;
  FtempNewQuery3:=FireDACQuery3_New;
end;

procedure TABUpdateForm.BitBtn1Click(Sender: TObject);
var
  tempBoolean:boolean;
begin
  RefreshConn;

  if ComboBox1.Text=emptystr  then
  begin
    ComboBox1.Text:='.';
  end;

  tempBoolean:= ABOpenConnection(FireDACConnection_Old,
                                  FOldDatabaseType,
                                  ComboBox1.Text,
                                  ComboBox2.Text,
                                  Edit1.Text,
                                  Edit2.Text,
                                  Assigned(Sender),True,
                                  ComboBox2.Text,true
                                  );

  if tempBoolean then
  begin
    if (FOldDatabaseType=dtsqlserver) then
    begin
      ABFillStringsBySQL('Old',' select name from master.dbo.sysdatabases ',[],nil,ComboBox2.Items);
    end;

    if ComboBox1.Items.IndexOf(ComboBox1.Text) < 0 then
    begin
      ComboBox1.Items.Add(ComboBox1.Text);
    end;
  end;
end;

procedure TABUpdateForm.BitBtn3Click(Sender: TObject);
var
  tempBoolean:boolean;
begin
  RefreshConn;
  if ComboBox3.Text=emptystr  then
  begin
    ComboBox3.Text:='.';
  end;

  tempBoolean:= ABOpenConnection(FireDACConnection_New,
                                  dtSQLServer,
                                  ComboBox3.Text,
                                  ComboBox4.Text,
                                  Edit3.Text,
                                  Edit4.Text,
                                  Assigned(Sender),True,
                                  ComboBox4.Text,true
                                  );

  if tempBoolean then
  begin
    ABFillStringsBySQL('New',' select name from master.dbo.sysdatabases  ',[],nil,ComboBox4.Items);
    if ComboBox3.Items.IndexOf(ComboBox3.Text) < 0 then
    begin
      ComboBox3.Items.Add(ComboBox3.Text);
    end;
  end;
end;

procedure TABUpdateForm.ComboBox5Change(Sender: TObject);
begin
  if AnsiCompareText(ComboBox5.Text,'Oracle')=0 then
  begin
    ComboBox2.Enabled:=false;
  end
  else
  begin
    ComboBox2.Enabled:=true;
  end;
end;

procedure TABUpdateForm.FormCreate(Sender: TObject);
var
  tempEditText:TEdit;
  tempCheckBox:TCheckBox;
  i:longint;
  tempTabSheet: TTabSheet;
begin
  ABAddToConnList('New',FireDACConnection_New);
  ABAddToConnList('Old',FireDACConnection_Old);

  for I := 1 to 10 do
  begin
    tempEditText:=nil;
    tempTabSheet:=nil;
    tempCheckBox:=nil;

    if Assigned(FindComponent('TabSheet'+inttostr(i))) then
      tempTabSheet:=TTabSheet(FindComponent('TabSheet'+inttostr(i)));
    if Assigned(FindComponent('Edit_Caption'+inttostr(i))) then
      tempEditText:=TEdit(FindComponent('Edit_Caption'+inttostr(i)));
    if Assigned(FindComponent('CheckBox_Run'+inttostr(i))) then
      tempCheckBox:=TCheckBox(FindComponent('CheckBox_Run'+inttostr(i)));

    if (Assigned(tempEditText)) and (trim(tempEditText.Text)<>emptystr) then
    begin
      if (Assigned(tempTabSheet)) then
        tempTabSheet.Caption   :=trim(tempEditText.Text);
      if (Assigned(tempCheckBox)) then
        tempCheckBox.Caption   :=trim(tempEditText.Text);
    end;
  end;

  ComboBox5Change(ComboBox5);
  SetCanSetup(False);
end;

procedure TABUpdateForm.Memo_updateSQL1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      TMemo(Sender).SelectAll;
    end;
  end;
end;

procedure TABUpdateForm.SpeedButton1Click(Sender: TObject);
begin
  ABGetSqlServerList(ComboBox3.Items);
end;

procedure TABUpdateForm.SpeedButton2Click(Sender: TObject);
begin
  ABGetSqlServerList(ComboBox1.Items);
end;

procedure TABUpdateForm.SpeedButton3Click(Sender: TObject);
begin
  if BitBtn1.CanFocus then
    BitBtn1.SetFocus;

  BitBtn1Click(nil);
end;

procedure TABUpdateForm.SpeedButton4Click(Sender: TObject);
begin
  if BitBtn3.CanFocus then
    BitBtn3.SetFocus;

  BitBtn3Click(nil);
end;

function TABUpdateForm.CheckConn(aShowAbortMsg:boolean):boolean;
begin
  result:=false;
  RefreshConn;
  if ABOpenConnection(FireDACConnection_Old,FOldDatabaseType,
            ComboBox1.Text,
            ComboBox2.Text,
            Edit1.Text,
            Edit2.Text,
            False,aShowAbortMsg,
            ComboBox2.Text
            ) then
  begin
    if ABOpenConnection(FireDACConnection_New,dtSQLServer,
              ComboBox3.Text,
              ComboBox4.Text,
              Edit3.Text,
              Edit4.Text,
              False,aShowAbortMsg,
              ComboBox3.Text
              ) then
    begin
      result:=True;
    end;
  end;
end;

procedure TABUpdateForm.DoOldToNewUpdate;
var
  I: Integer;
  tempMemoSQL:TMemo;
  tempUpdateSQL,
  tempOldBackName:string;
  tempCheckBox:TCheckBox;
  tempTabSheet: TTabSheet;
  procedure KillConn(aConnName:string;aDatabaseName:string);
  begin
    ABExecSQL(aConnName,'use master');
    if (ABGetSQLValue(aConnName,'select count(*) from  master.dbo.sysprocesses where dbid=db_id('+ABQuotedStr(aDatabaseName)+')',[],null)>='1')   then
    begin
      ABExecSQL(aConnName,
      ' declare    @sql    nvarchar(500)                  '+
      ' declare    @spid    int                           '+
      ' set    @sql='' declare    getspid    cursor    for  '+
                     ' select    spid                       '+
                     ' from    master.dbo.sysprocesses      '+
                     ' where    dbid=db_id('+''''''+aDatabaseName+''''''+')'''+
      ' exec    (@sql)                                    '+
      ' open    getspid                                   '+
      ' fetch    next    from    getspid    into    @spid '+
      ' while    @@fetch_status    <>-1                   '+
      '   begin                                           '+
      '   exec('' kill    ''+@spid)                         '+
      '   fetch    next    from    getspid    into    @spid '+
      ' end                                                 '+
      ' close    getspid                                    '+
      ' deallocate    getspid                               ');
    end;
  end;
begin
  Memo_log.Clear;
  Memo_log.Lines.Add(ABDateTimeToStr(now)+' 开始.');
  try
    if CheckConn(true) then
    begin
      for I := 1 to 10 do
      begin
        Application.ProcessMessages;

        tempCheckBox:=nil;
        tempMemoSQL:=nil;
        tempTabSheet:=nil;

        if Assigned(FindComponent('CheckBox_Run'+inttostr(i))) then
          tempCheckBox:=TCheckBox(FindComponent('CheckBox_Run'+inttostr(i)));
        if Assigned(FindComponent('TabSheet'+inttostr(i))) then
          tempTabSheet:=TTabSheet(FindComponent('TabSheet'+inttostr(i)));
        if (Assigned(tempCheckBox)) and (tempCheckBox.Checked) then
        begin
          if Assigned(FindComponent('Memo_updateSQL'+inttostr(i))) then
            tempMemoSQL:=TMemo(FindComponent('Memo_updateSQL'+inttostr(i)));
          if (Assigned(tempMemoSQL)) and (trim(tempMemoSQL.Text)<>emptystr) then
          begin
            tempUpdateSQL:=trim(tempMemoSQL.Text);

            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @NewSource'      ,'set @NewSource      ='+QuotedStr(ComboBox3.Text)+' -- ');
            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @NewDataBaseName','set @NewDataBaseName='+QuotedStr(ComboBox4.Text)+' -- ');
            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @NewUserName'    ,'set @NewUserName    ='+QuotedStr(edit3.Text)+' -- ');
            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @Newpassandword'    ,'set @Newpassandword    ='+QuotedStr(edit4.Text)+' -- ');

            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @OldDataBaseType','set @OldDataBaseType='+QuotedStr(ComboBox5.Text)+' -- ');
            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @OldSource'      ,'set @OldSource      ='+QuotedStr(ComboBox1.Text)+' -- ');
            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @OldDataBaseName','set @OldDataBaseName='+QuotedStr(ComboBox2.Text)+' -- ');
            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @OldUserName'    ,'set @OldUserName    ='+QuotedStr(edit1.Text)+' -- ');
            tempUpdateSQL:=ABStringReplace(tempUpdateSQL,'set @Oldpassandword'    ,'set @Oldpassandword    ='+QuotedStr(edit2.Text)+' -- ');
            try
              if RefreshStop then
              begin
                break;
              end;
              //FireDACConnection_New.Errors.Clear;
              ABExecSQL('New',tempUpdateSQL);

              if RefreshStop then
              begin
                break;
              end;

              if (Assigned(tempTabSheet)) then
              begin
                if AnsiCompareText(ABGetSQLValue('New','select isnull(dbo.Func_GetPublicVar_str1(''UpdateDatabaseState''),'''') ',[],''),
                                   'End')<>0 then
                begin
                  Memo_log.Lines.Add(ABDateTimeToStr(now)+' '+tempTabSheet.Caption+'执行失败.');
                  Button_StopClick(Button_Stop);
                  break;
                end
                else
                begin
                  Memo_log.Lines.Add(ABDateTimeToStr(now)+' '+tempTabSheet.Caption+'执行成功.');
                end;
              end;
            except
              on E: Exception do
              begin
                if (Assigned(tempTabSheet)) then
                begin
                  Memo_log.Lines.Add(ABDateTimeToStr(now)+' '+tempTabSheet.Caption+'执行失败.'+ABEnterWrapStr+
                                     e.Message );
                  Button_StopClick(Button_Stop);
                  break;
                end;
              end;
            end;
          end;
        end;
      end;

      {
      for j := 0 to  FireDACConnection_New.Errors.Count - 1 do
      begin
        Memo_log.Lines.Add(ABDateTimeToStr(now)+' '+
        FireDACConnection_New.Errors.Item[j].Source+
        FireDACConnection_New.Errors.Item[j].Description+
        FireDACConnection_New.Errors.Item[j].SQLState);
      end;
      }
      if RefreshStop  then
      begin
        exit;
      end;

      if (CheckBox2.Checked) and
         (FOldDatabaseType=dtSQLServer) and
         (ABShow(CheckBox2.caption+ABEnterWrapStr+
                 '将先断开新旧数据库的其它程序连接才能继续操作,是否确认?',[],['是','否'],1)=1) then
      begin
        KillConn('Old',ComboBox2.Text);
        KillConn('New',ComboBox4.Text);

        ABExecSQL('Old','use master');
        ABExecSQL('New','use master');

        tempOldBackName:=ComboBox2.Text+'_'+ABStringReplace(ABDateTimeToStr(now(),15,'',''),' ','');
        ABExecSQL('Old','EXEC sp_renamedb '+QuotedStr(ComboBox2.Text)+','+QuotedStr(tempOldBackName));
        ABExecSQL('New','EXEC sp_renamedb '+QuotedStr(ComboBox4.Text)+','+QuotedStr(ComboBox2.Text));

        ComboBox4.Text:=ComboBox2.Text;
        ComboBox2.Text:=tempOldBackName;

        BitBtn1Click(nil);
        BitBtn3Click(nil);
      end;
    end;
  finally
    GroupBox1.Enabled:=True;
    GroupBox2.Enabled:=True;
    Button_OpenSetup.Visible:=True;
    Button_CloseSetup.Visible:=True;
    Button_StopClick(Button_Stop);
    RefreshStop ;
  end;
end;

procedure TABUpdateForm.Button_RunClick(Sender: TObject);
begin
  if CheckConn(true) then
  begin
    Button_OpenSetup.Visible:=false;
    Button_CloseSetup.Visible:=false;

    Button_Run.Enabled:=false;
    Button_Stop.Enabled:=True;
    GroupBox1.Enabled:=false;
    GroupBox2.Enabled:=false;
    //运行正在处理的AVI文件
    if ABCheckFileExists(ABSoftSetPath+'Run.avi') then
      Animate1.FileName:=ABSoftSetPath+'Run.avi'
    else
      Animate1.FileName:=EmptyStr;
    if Animate1.FileName<>EmptyStr then
      Animate1.Active:=true;

    case PageControl2.ActivePageIndex of
      0:
      begin
        DoOldToNewUpdate;
      end;
      1:
      begin
      end;
    end;
  end;
end;

procedure TABUpdateForm.Button_StopClick(Sender: TObject);
begin
  Button_Stop.Enabled:=False;
  Button_Run.Enabled:=True;
end;

procedure TABUpdateForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if (Button_Stop.Enabled) then
  begin
    CanClose:=false;
    Button_Stop.Tag:=1;
    Button_StopClick(Button_Stop);
  end;
end;

function TABUpdateForm.RefreshStop:boolean;
begin
  result:=false;
  if not Button_Stop.Enabled then
  begin
    result:=True;
    GroupBox1.Enabled:=True;
    GroupBox2.Enabled:=True;

    Button_OpenSetup.Visible:=true;
    Button_CloseSetup.Visible:=true;

    Button_Run.Enabled:=True;
    if Animate1.Active then
      Animate1.Active:=False;

    if Button_Stop.Tag=1 then
      close;
  end;
end;

procedure TABUpdateForm.Button_OpenSetupClick(Sender: TObject);
begin
  if Button_Run.Enabled then
  begin
    if ABInputPassWord then
    begin
      SetCanSetup(True);
    end;
  end;
end;

procedure TABUpdateForm.Button_CloseSetupClick(Sender: TObject);
begin
  if Button_Run.Enabled then
  begin
    SetCanSetup(False);
  end;
end;

procedure TABUpdateForm.SetCanSetup(aBoolean: boolean);
begin
  Button_OpenSetup.Enabled:=not aBoolean;
  Button_CloseSetup.Enabled:=aBoolean;

  TabSheet11.Enabled:=aBoolean;
end;

procedure TABUpdateForm.PageControl2Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange:=(not Button_Stop.Enabled) and (ABInputPassWord);
end;

end.
