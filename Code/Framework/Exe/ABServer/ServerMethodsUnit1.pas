unit ServerMethodsUnit1;

interface

uses
  ABPubVarU,
  ABPubFuncU,
  ABPubUserU,
  ABPubConstU,
  ABPubLogU,
  ABPubLocalParamsU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdConnDatabaseU,
  FireDAC.Stan.ExprFuncs,
  FireDAC.Stan.Expr,

  typinfo,
  FireDAC.Stan.Option, FireDAC.Stan.Param,
  DSProviderDataModuleAdapter,JSON,
  System.SysUtils,System.StrUtils,System.Classes,Datasnap.DSServer,Data.DB,
  DBXCommon,DBXJSON,DBXJSONReflect,DBXDBReaders,Variants,
  DSIntf, FireDAC.Stan.StorageJSON, FireDAC.Stan.StorageBin,
  FireDAC.Stan.StorageXML, FireDAC.Stan.Intf, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TABServer = class(TDSServerModule)
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    FDStanStorageXMLLink1: TFDStanStorageXMLLink;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
  private
    { Private declarations }
  public
    { Public declarations }
    //取得数据库的连接串,多个库间有回车换行
    function GetConnStrings:string;
    //取得SQL的数据流
    function GetDatasetStream(aConnName:string;aSQL: string;aInitTableNames: string;aParams:TParams;var aResult:Boolean): TStream;
    //执行SQL
    function ExecSQL(aConnName:string;aSQL: string;aParams:OleVariant;aParamTypes:OleVariant): LongInt;

    procedure StartTransaction(aConnName:string);
    procedure Commit(aConnName:string);
    procedure Rollback(aConnName:string);
    procedure SetTimeout(aConnName:string;aTimeout:LongInt);

    //客户端回调函数,客户端在调用服务端的方法时传入的回调函数，这种情况还是客户端主动发起，服务器回应而已
    //在过程或函数中增加一个或多个TDBXCallback类型的参数，如aCallback: TDBXCallback
    //在客户端,我们必须定义一个新类,继承在TDBXCallback,重写其Execute方法
    //客户端调用时传入新类的对象，在服务端执行接收到的新类对象execute时，将等待客户端执行重写的Execute方法完毕才继续
    //可用于如显示服务器方法的执行进度
    procedure LongTimeRunFunc(aCallback: TDBXCallback);
  end;


implementation

{$R *.dfm}
uses Main;

var
  FSys_Org_Table:TFireDACQuery;
  FQuery_Select:TFireDACQuery;

  FParamValues:array of Variant;
  FParamTypes:array of TFieldType;

procedure TABServer.LongTimeRunFunc(aCallback: TDBXCallback);
var
  I: Integer;
begin
  for I := 0 to 5 do
  begin
    aCallback.Execute(TJSONNumber.Create(I)).Free;
  end;
end;

procedure TABServer.StartTransaction(aConnName: string);
var
  tempConn:TCustomConnection;
begin
  tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
  if Assigned(tempConn) then
  begin
    MainForm.addLog('StartTransaction('+aConnName+')');
    TFireDACConnection(tempConn).StartTransaction;
  end;
end;

procedure TABServer.Commit(aConnName: string);
var
  tempConn:TCustomConnection;
begin
  tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
  if Assigned(tempConn) then
  begin
    MainForm.addLog('Commit('+aConnName+')');
    TFireDACConnection(tempConn).Commit;
  end;
end;

procedure TABServer.Rollback(aConnName: string);
var
  tempConn:TCustomConnection;
begin
  tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
  if Assigned(tempConn) then
  begin
    MainForm.addLog('Rollback('+aConnName+')');
    TFireDACConnection(tempConn).Rollback;
  end;
end;

procedure TABServer.SetTimeout(aConnName: string; aTimeout: Integer);
var
  tempConn:TCustomConnection;
begin
  tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
  if Assigned(tempConn) then
  begin
    MainForm.addLog('SetTimeout('+aConnName+','+inttostr(aTimeout)+')');
    ABWriteItemInStrings('LoginTimeout',inttostr(aTimeout), TFireDACConnection(tempConn).Params);
  end;
end;

function TABServer.GetDatasetStream(aConnName:string;aSQL: string;aInitTableNames: string;aParams:TParams;var aResult:Boolean): TStream;
  procedure AddDatasetExtFields;
  var
    i:LongInt;
    tempTableName:string;
    tempAddBaseField:boolean;
    tempDef:TStrings;
  begin
    if ABLocalParams.LoginType=ltCS_Two then
    begin
      tempDef := TStringList.Create;
      try
        tempDef.Text:=aInitTableNames;
        if (tempDef.Count>0) then
        begin
          if not FSys_Org_Table.Active then
          begin
            FSys_Org_Table.close;
            FSys_Org_Table.Connection:=nil;
            if ABSetDatasetConnByConnName(FSys_Org_Table,'') then
            begin
              FSys_Org_Table.SQL.Text:='select CL_Name,Ta_Name,TA_CalcFieldsDef,TA_AggregateFieldsDef from ABSys_Org_ConnList left join ABSys_Org_Table on CL_Guid=Ta_CL_Guid';
              FSys_Org_Table.open;
            end;
          end;

          if FSys_Org_Table.Active then
          begin
            if aConnName=EmptyStr then
              aConnName:='Main';

            tempAddBaseField:=false;
            for i := 0 to tempDef.Count-1 do
            begin
              tempTableName:=trim(tempDef[i]);
              if tempTableName=emptystr then
                exit;

              if (FSys_Org_Table.Locate('CL_Name;Ta_Name',VarArrayOf([aConnName,tempTableName]),[])) and
                 ((FSys_Org_Table.FieldByName('TA_CalcFieldsDef').AsString<>EmptyStr) or
                  (FSys_Org_Table.FieldByName('TA_AggregateFieldsDef').AsString<>EmptyStr)
                  )  then
              begin
                ABAddDatasetExtFields( FQuery_Select,
                                       FSys_Org_Table.FieldByName('TA_CalcFieldsDef').AsString,
                                       FSys_Org_Table.FieldByName('TA_AggregateFieldsDef').AsString,
                                       tempAddBaseField);
              end;
            end;
          end;
        end;
      finally
        tempDef.Free;
      end;
    end;
  end;
begin
  Result :=TMemoryStream.Create;
  MainForm.addLog('GetDatasetStream('+aSQL+')');

  aResult:=false;
  FQuery_Select.close;
  FQuery_Select.Connection:=nil;
  if ABSetDatasetConnByConnName(FQuery_Select,aConnName) then
  begin
    FQuery_Select.Fields.Clear;
    FQuery_Select.FieldDefs.Clear;
    FQuery_Select.SQL.Text:=aSQL;
    FQuery_Select.Params.Clear;
    ABCopyParams(aParams,FQuery_Select.Params);

    AddDatasetExtFields ;
    FQuery_Select.open;
    try
      Result.Position:=0;
      FQuery_Select.SaveToStream(Result);
      aResult:=True;
      Result.Position:=0;
    finally
      FQuery_Select.close;    
    end;
  end;
end;

function TABServer.ExecSQL(aConnName:string;aSQL: string;aParams:OleVariant;aParamTypes:OleVariant): LongInt;
var
  i:LongInt;
  tempConn:TCustomConnection;
begin
  Result:=-1;
  tempConn:=ABGetConnInfoByConnName(aConnName,false).Conn;
  if Assigned(tempConn) then
  begin
    MainForm.addLog('ExecSQL('+aSQL+')');

    if not VarIsEmpty(aParams) then
    begin
      SetLength(FParamValues,VarArrayHighBound(aParams,1)+1);
      for I := VarArrayLowBound(aParams,1) to VarArrayHighBound(aParams,1) do
      begin
        FParamValues[i]:=aParams[i];
      end;
    end
    else
    begin
      SetLength(FParamValues,0);
    end;
    
    if not VarIsEmpty(aParamTypes) then
    begin
      SetLength(FParamTypes,VarArrayHighBound(aParamTypes,1)+1);
      for I := VarArrayLowBound(aParamTypes,1) to VarArrayHighBound(aParamTypes,1) do
      begin
        FParamTypes[i]:=aParamTypes[i];
      end;
    end
    else
    begin
      SetLength(FParamTypes,0);
    end;

    result:=TFireDACConnection(tempConn).ExecSQL(aSql,FParamValues,FParamTypes);
  end;
end;

function TABServer.GetConnStrings: string;
var
  i:LongInt;
  tempStrings:Tstrings;
begin
  MainForm.addLog('GetConnStrings');
  tempStrings:=TStringList.Create;
  try
    for I := low(ABConns) to High(ABConns) do
    begin
      tempStrings.add('[ConnName]'+ABConns[i].Conninfo.ConnName+
                      '[Host]'    +ABConns[i].Conninfo.Host+
                      '[Database]'+ABConns[i].Conninfo.Database+
                      '[UserName]'+ABConns[i].Conninfo.UserName+
                      '[Password]'+ABConns[i].Conninfo.Password+
                      '[Manual]'  +abbooltostr(ABConns[i].Conninfo.Manual)+
                      '[IsMain]'  +abbooltostr(ABConns[i].Conninfo.IsMain)+
                      '[DatabaseType]'  +GetEnumName(TypeInfo(TABDatabaseType), Ord(ABConns[i].Conninfo.DatabaseType))
                      );
    end;
    Result:=tempStrings.text;
  finally
    tempStrings.Free;
  end;
end;

procedure ABInitialization;
begin
  FQuery_Select:=TFireDACQuery.Create(nil);
  FQuery_Select.FetchOptions.Mode:=fmAll;
  FSys_Org_Table:=TFireDACQuery.Create(nil);
  FSys_Org_Table.FetchOptions.Mode:=fmAll;
end;

procedure ABFinalization;
begin
  FQuery_Select.Free;
  FSys_Org_Table.Free;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.
     {

procedure TABServer.GetVariant(aConnName: string; aSQL: string;out aData:OleVariant;
                               out aFieldCount:longint;out aRecoredCount:longint);
  procedure StreamToVariant(Stream: TStream; var V: OLEVariant);
  var
    P : Pointer;
  begin
    try
      V := VarArrayCreate ([0, Stream.Size - 1], varByte);
      P := VarArrayLock (V);
      Stream.Position := 0;
      Stream.Read (P^, Stream.Size);
      VarArrayUnlock (V);
    except
      Exit;
    end;
  end;

  procedure VariantToStream(const V: OLEVariant; Stream: TStream);
  var
    P: Pointer;
  begin
    try
      Stream.Position := 0;
      Stream.Size := VarArrayHighBound (V, 1) - VarArrayLowBound (V, 1) + 1;
      P := VarArrayLock (V);
      Stream.Write (P^, Stream.Size);
      VarArrayUnlock (V);
      Stream.Position := 0;
    except
      Exit;
    end;
  end;

  function CompressData(V: OleVariant): OleVariant;
  var
    M, M0: TMemoryStream;
  begin
    try
      M := TMemoryStream.Create;
      M0 := TMemoryStream.Create;
      try
        VariantToStream(V,M);
        M.Position := 0;
        ZCompressStream(M, M0);
        StreamToVariant(M0, V);
      finally
//        M.Free;
//        M0.Free
      end;
      Result := V;
    except
      Exit;
    end;
  end;

  function DeCompressData(V: OleVariant): OleVariant;
  var
    M, M0: TMemoryStream;
  begin
    try
      M := TMemoryStream.Create;
      M0 := TMemoryStream.Create;
      try
        VariantToStream(V,M);
        M.Position := 0;
        ZDeCompressStream(M, M0);
        StreamToVariant(M0, V);
      finally
//        M.Free;
//        M0.Free
      end;
      Result := V;
    except
      Exit;
    end;
  end;
var
  m : Integer;
  nCurRec : Integer;
begin
  SQLDataSet1.Close;
  SQLDataSet1.CommandText:=aSQL;
  SQLDataSet1.Open;

  aFieldCount:=SQLDataSet1.FieldCount;
  aRecoredCount:=SQLDataSet1.RecordCount;
  nCurRec := 0;
  aData := VarArrayCreate([0, aFieldCount*aRecoredCount-1],varVariant);
  vararraylock(aData);
  try
    while not SQLDataSet1.Eof do
    begin
      for m := 0 to aFieldCount-1 do
        aData[nCurRec*aFieldCount+m] := SQLDataSet1.Fields[m].Value;

      SQLDataSet1.Next;
      Inc(nCurRec);
    end;
  finally
    vararrayunlock(aData);
    aData:=CompressData(aData);
  end;
end;

procedure TABServer.GetSQLDataset(aConnName: string; aSQL: string;out aDataset:TDataset);
begin
  SQLDataSet1.Close;
  SQLDataSet1.CommandText:=aSQL;
  SQLDataSet1.Open;
  aDataset:=SQLDataSet1;
end;


}
