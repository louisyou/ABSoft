{ *****************************************************************************
  WARNING: This component file was generated using the EventSinkImp utility.
           The contents of this file will be overwritten everytime EventSinkImp
           is asked to regenerate this sink component.

  NOTE:    When using this component at the same time with the XXX_TLB.pas in
           your Delphi projects, make sure you always put the XXX_TLB unit name
           AFTER this component unit name in the USES


  clauseoftheinterfacesectionofyourunit; otherwise you may get interface conflict
           errors from the Delphi compiler.

           EventSinkImp is written by Binh Ly (bly@techvanguards.com)
  *****************************************************************************
  //Sink Classes//
  TSQLDMOServerSink
  TSQLDMOBulkCopySink
  TSQLDMOTransferSink
  TSQLDMORestoreSink
  TSQLDMOReplicationSink
  TSQLDMOBackupSink
}

{$IFDEF VER100}
{$DEFINE D3}
{$ENDIF}

//SinkUnitName//
unit SQLDMOEvents;

interface

uses
  Windows, ActiveX, Classes, ComObj, OleCtrls
  //SinkUses//
  , StdVCL
  , SQLDMO_TLB
  ;

type
  { backward compatibility types }
  {$IFDEF D3}
  OLE_COLOR = TOleColor;
  {$ENDIF}

  TSQLDMOEventsBaseSink = class (TComponent, IUnknown, IDispatch)
  protected
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): HResult; {$IFNDEF D3} override; {$ENDIF} stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;

    { IDispatch }
    function GetIDsOfNames(const IID: TGUID; Names: Pointer;
      NameCount, LocaleID: Integer; DispIDs: Pointer): HResult; virtual; stdcall;
    function GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult; virtual; stdcall;
    function GetTypeInfoCount(out Count: Integer): HResult; virtual; stdcall;
    function Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult; virtual; stdcall;
  protected
    FCookie: integer;
    FCP: IConnectionPoint;
    FSinkIID: TGUID;
    FSource: IUnknown;
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; virtual; abstract;
  public
    destructor Destroy; override;
    procedure Connect (const ASource: IUnknown);
    procedure Disconnect;
    property SinkIID: TGUID read FSinkIID write FSinkIID;
    property Source: IUnknown read FSource;
  end;

  //SinkImportsForwards//

  //SinkImports//

  //SinkIntfStart//

  //SinkEventsForwards//
  TServerSinkQueryTimeoutEvent = function (Sender: TObject; const Message: WideString; out Continue: WordBool): HResult of object;
  TServerSinkServerMessageEvent = function (Sender: TObject; Severity: Integer; MessageNumber: Integer; MessageState: Integer; const Message: WideString): HResult of object;
  TServerSinkConnectionBrokenEvent = function (Sender: TObject; const Message: WideString; out Retry: WordBool): HResult of object;
  TServerSinkRemoteLoginFailedEvent = function (Sender: TObject; Severity: Integer; MessageNumber: Integer; MessageState: Integer; const Message: WideString): HResult of object;
  TServerSinkCommandSentEvent = function (Sender: TObject; const SQLCommand: WideString): HResult of object;

  //SinkComponent//
  TSQLDMOServerSink = class (TSQLDMOEventsBaseSink
    , ServerSink
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    { ServerSink }
    function ServerSink.QueryTimeout = DoQueryTimeout;
    function ServerSink.ServerMessage = DoServerMessage;
    function ServerSink.ConnectionBroken = DoConnectionBroken;
    function ServerSink.RemoteLoginFailed = DoRemoteLoginFailed;
    function ServerSink.CommandSent = DoCommandSent;
  public
    { system methods }
    constructor Create (AOwner: TComponent); override;
  protected
    //SinkInterface//
    function DoQueryTimeout(const Message: WideString; out Continue: WordBool): HResult; stdcall;
    function DoServerMessage(Severity: Integer; MessageNumber: Integer; MessageState: Integer; const Message: WideString): HResult; stdcall;
    function DoConnectionBroken(const Message: WideString; out Retry: WordBool): HResult; stdcall;
    function DoRemoteLoginFailed(Severity: Integer; MessageNumber: Integer; MessageState: Integer; const Message: WideString): HResult; stdcall;
    function DoCommandSent(const SQLCommand: WideString): HResult; stdcall;
  protected
    //SinkEventsProtected//
    FQueryTimeout: TServerSinkQueryTimeoutEvent;
    FServerMessage: TServerSinkServerMessageEvent;
    FConnectionBroken: TServerSinkConnectionBrokenEvent;
    FRemoteLoginFailed: TServerSinkRemoteLoginFailedEvent;
    FCommandSent: TServerSinkCommandSentEvent;
  published
    //SinkEventsPublished//
    property QueryTimeout: TServerSinkQueryTimeoutEvent read FQueryTimeout write FQueryTimeout;
    property ServerMessage: TServerSinkServerMessageEvent read FServerMessage write FServerMessage;
    property ConnectionBroken: TServerSinkConnectionBrokenEvent read FConnectionBroken write FConnectionBroken;
    property RemoteLoginFailed: TServerSinkRemoteLoginFailedEvent read FRemoteLoginFailed write FRemoteLoginFailed;
    property CommandSent: TServerSinkCommandSentEvent read FCommandSent write FCommandSent;
  end;


  //SinkEventsForwards//
  TBulkCopySinkRowsCopiedEvent = function (Sender: TObject; const Message: WideString; Rows: Integer): HResult of object;
  TBulkCopySinkBatchImportedEvent = function (Sender: TObject; const Message: WideString): HResult of object;

  //SinkComponent//
  TSQLDMOBulkCopySink = class (TSQLDMOEventsBaseSink
    , BulkCopySink
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    { BulkCopySink }
    function BulkCopySink.RowsCopied = DoRowsCopied;
    function BulkCopySink.BatchImported = DoBatchImported;
  public
    { system methods }
    constructor Create (AOwner: TComponent); override;
  protected
    //SinkInterface//
    function DoRowsCopied(const Message: WideString; Rows: Integer): HResult; stdcall;
    function DoBatchImported(const Message: WideString): HResult; stdcall;
  protected
    //SinkEventsProtected//
    FRowsCopied: TBulkCopySinkRowsCopiedEvent;
    FBatchImported: TBulkCopySinkBatchImportedEvent;
  published
    //SinkEventsPublished//
    property RowsCopied: TBulkCopySinkRowsCopiedEvent read FRowsCopied write FRowsCopied;
    property BatchImported: TBulkCopySinkBatchImportedEvent read FBatchImported write FBatchImported;
  end;


  //SinkEventsForwards//
  TTransferSinkStatusMessageEvent = function (Sender: TObject; const Message: WideString): HResult of object;
  TTransferSinkPercentCompleteAtStepEvent = function (Sender: TObject; const Message: WideString; Percent: Integer): HResult of object;
  TTransferSinkScriptTransferPercentCompleteEvent = function (Sender: TObject; const Message: WideString; Percent: Integer): HResult of object;
  TTransferSinkTransferPercentCompleteEvent = function (Sender: TObject; const Message: WideString; Percent: Integer): HResult of object;

  //SinkComponent//
  TSQLDMOTransferSink = class (TSQLDMOEventsBaseSink
    , TransferSink
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    { TransferSink }
    function TransferSink.StatusMessage = DoStatusMessage;
    function TransferSink.PercentCompleteAtStep = DoPercentCompleteAtStep;
    function TransferSink.ScriptTransferPercentComplete = DoScriptTransferPercentComplete;
    function TransferSink.TransferPercentComplete = DoTransferPercentComplete;
  public
    { system methods }
    constructor Create (AOwner: TComponent); override;
  protected
    //SinkInterface//
    function DoStatusMessage(const Message: WideString): HResult; stdcall;
    function DoPercentCompleteAtStep(const Message: WideString; Percent: Integer): HResult; stdcall;
    function DoScriptTransferPercentComplete(const Message: WideString; Percent: Integer): HResult; stdcall;
    function DoTransferPercentComplete(const Message: WideString; Percent: Integer): HResult; stdcall;
  protected
    //SinkEventsProtected//
    FStatusMessage: TTransferSinkStatusMessageEvent;
    FPercentCompleteAtStep: TTransferSinkPercentCompleteAtStepEvent;
    FScriptTransferPercentComplete: TTransferSinkScriptTransferPercentCompleteEvent;
    FTransferPercentComplete: TTransferSinkTransferPercentCompleteEvent;
  published
    //SinkEventsPublished//
    property StatusMessage: TTransferSinkStatusMessageEvent read FStatusMessage write FStatusMessage;
    property PercentCompleteAtStep: TTransferSinkPercentCompleteAtStepEvent read FPercentCompleteAtStep write FPercentCompleteAtStep;
    property ScriptTransferPercentComplete: TTransferSinkScriptTransferPercentCompleteEvent read FScriptTransferPercentComplete write FScriptTransferPercentComplete;
    property TransferPercentComplete: TTransferSinkTransferPercentCompleteEvent read FTransferPercentComplete write FTransferPercentComplete;
  end;


  //SinkEventsForwards//
  TRestoreSinkPercentCompleteEvent = function (Sender: TObject; const Message: WideString; Percent: Integer): HResult of object;
  TRestoreSinkNextMediaEvent = function (Sender: TObject; const Message: WideString): HResult of object;
  TRestoreSinkCompleteEvent = function (Sender: TObject; const Message: WideString): HResult of object;

  //SinkComponent//
  TSQLDMORestoreSink = class (TSQLDMOEventsBaseSink
    , RestoreSink
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    { RestoreSink }
    function RestoreSink.PercentComplete = DoPercentComplete;
    function RestoreSink.NextMedia = DoNextMedia;
    function RestoreSink.Complete = DoComplete;
  public
    { system methods }
    constructor Create (AOwner: TComponent); override;
  protected
    //SinkInterface//
    function DoPercentComplete(const Message: WideString; Percent: Integer): HResult; stdcall;
    function DoNextMedia(const Message: WideString): HResult; stdcall;
    function DoComplete(const Message: WideString): HResult; stdcall;
  protected
    //SinkEventsProtected//
    FPercentComplete: TRestoreSinkPercentCompleteEvent;
    FNextMedia: TRestoreSinkNextMediaEvent;
    FComplete: TRestoreSinkCompleteEvent;
  published
    //SinkEventsPublished//
    property PercentComplete: TRestoreSinkPercentCompleteEvent read FPercentComplete write FPercentComplete;
    property NextMedia: TRestoreSinkNextMediaEvent read FNextMedia write FNextMedia;
    property Complete: TRestoreSinkCompleteEvent read FComplete write FComplete;
  end;


  //SinkEventsForwards//
  TReplicationSinkStatusMessageEvent = function (Sender: TObject; const Message: WideString): HResult of object;
  TReplicationSinkPercentCompleteEvent = function (Sender: TObject; const Message: WideString; Percent: Integer): HResult of object;

  //SinkComponent//
  TSQLDMOReplicationSink = class (TSQLDMOEventsBaseSink
    , ReplicationSink
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    { ReplicationSink }
    function ReplicationSink.StatusMessage = DoStatusMessage;
    function ReplicationSink.PercentComplete = DoPercentComplete;
  public
    { system methods }
    constructor Create (AOwner: TComponent); override;
  protected
    //SinkInterface//
    function DoStatusMessage(const Message: WideString): HResult; stdcall;
    function DoPercentComplete(const Message: WideString; Percent: Integer): HResult; stdcall;
  protected
    //SinkEventsProtected//
    FStatusMessage: TReplicationSinkStatusMessageEvent;
    FPercentComplete: TReplicationSinkPercentCompleteEvent;
  published
    //SinkEventsPublished//
    property StatusMessage: TReplicationSinkStatusMessageEvent read FStatusMessage write FStatusMessage;
    property PercentComplete: TReplicationSinkPercentCompleteEvent read FPercentComplete write FPercentComplete;
  end;


  //SinkEventsForwards//
  TBackupSinkPercentCompleteEvent = function (Sender: TObject; const Message: WideString; Percent: Integer): HResult of object;
  TBackupSinkNextMediaEvent = function (Sender: TObject; const Message: WideString): HResult of object;
  TBackupSinkCompleteEvent = function (Sender: TObject; const Message: WideString): HResult of object;

  //SinkComponent//
  TSQLDMOBackupSink = class (TSQLDMOEventsBaseSink
    , BackupSink
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    { BackupSink }
    function BackupSink.PercentComplete = DoPercentComplete;
    function BackupSink.NextMedia = DoNextMedia;
    function BackupSink.Complete = DoComplete;
  public
    { system methods }
    constructor Create (AOwner: TComponent); override;
  protected
    //SinkInterface//
    function DoPercentComplete(const Message: WideString; Percent: Integer): HResult; stdcall;
    function DoNextMedia(const Message: WideString): HResult; stdcall;
    function DoComplete(const Message: WideString): HResult; stdcall;
  protected
    //SinkEventsProtected//
    FPercentComplete: TBackupSinkPercentCompleteEvent;
    FNextMedia: TBackupSinkNextMediaEvent;
    FComplete: TBackupSinkCompleteEvent;
  published
    //SinkEventsPublished//
    property PercentComplete: TBackupSinkPercentCompleteEvent read FPercentComplete write FPercentComplete;
    property NextMedia: TBackupSinkNextMediaEvent read FNextMedia write FNextMedia;
    property Complete: TBackupSinkCompleteEvent read FComplete write FComplete;
  end;

  //SinkIntfEnd//

procedure Register;

implementation

uses
  SysUtils;

{ globals }

procedure BuildPositionalDispIds (pDispIds: PDispIdList; const dps: TDispParams);
var
  i: integer;
begin
  Assert (pDispIds <> nil);
  
  { by default, directly arrange in reverse order }
  for i := 0 to dps.cArgs - 1 do
    pDispIds^ [i] := dps.cArgs - 1 - i;

  { check for named args }
  if (dps.cNamedArgs <= 0) then Exit;

  { parse named args }
  for i := 0 to dps.cNamedArgs - 1 do
    pDispIds^ [dps.rgdispidNamedArgs^ [i]] := i;
end;


{ TSQLDMOEventsBaseSink }

function TSQLDMOEventsBaseSink.GetIDsOfNames(const IID: TGUID; Names: Pointer;
  NameCount, LocaleID: Integer; DispIDs: Pointer): HResult;
begin
  Result := E_NOTIMPL;
end;

function TSQLDMOEventsBaseSink.GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult;
begin
  Result := E_NOTIMPL;
  pointer (TypeInfo) := nil;
end;

function TSQLDMOEventsBaseSink.GetTypeInfoCount(out Count: Integer): HResult;
begin
  Result := E_NOTIMPL;
  Count := 0;
end;

function TSQLDMOEventsBaseSink.Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult;
var
  dps: TDispParams absolute Params;
  bHasParams: boolean;
  pDispIds: PDispIdList;
  iDispIdsSize: integer;
begin
  { validity checks }
  if (Flags AND DISPATCH_METHOD = 0) then
    raise Exception.Create (
      Format ('%s only supports sinking of method calls!', [ClassName]
    ));

  { build pDispIds array. this maybe a bit of overhead but it allows us to
    sink named-argument calls such as Excel's AppEvents, etc!
  }
  pDispIds := nil;
  iDispIdsSize := 0;
  bHasParams := (dps.cArgs > 0);
  if (bHasParams) then
  begin
    iDispIdsSize := dps.cArgs * SizeOf (TDispId);
    GetMem (pDispIds, iDispIdsSize);
  end;  { if }

  try
    { rearrange dispids properly }
    if (bHasParams) then BuildPositionalDispIds (pDispIds, dps);
    Result := DoInvoke (DispId, IID, LocaleID, Flags, dps, pDispIds, VarResult, ExcepInfo, ArgErr);
  finally
    { free pDispIds array }
    if (bHasParams) then FreeMem (pDispIds, iDispIdsSize);
  end;  { finally }
end;

function TSQLDMOEventsBaseSink.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if (GetInterface (IID, Obj)) then
  begin
    Result := S_OK;
    Exit;
  end
  else
    if (IsEqualIID (IID, FSinkIID)) then
      if (GetInterface (IDispatch, Obj)) then
      begin
        Result := S_OK;
        Exit;
      end;
  Result := E_NOINTERFACE;
  pointer (Obj) := nil;
end;

function TSQLDMOEventsBaseSink._AddRef: Integer;
begin
  Result := 2;
end;

function TSQLDMOEventsBaseSink._Release: Integer;
begin
  Result := 1;
end;

destructor TSQLDMOEventsBaseSink.Destroy;
begin
  Disconnect;
  inherited;
end;

procedure TSQLDMOEventsBaseSink.Connect (const ASource: IUnknown);
var
  pcpc: IConnectionPointContainer;
begin
  Assert (ASource <> nil);
  Disconnect;
  try
    OleCheck (ASource.QueryInterface (IConnectionPointContainer, pcpc));
    OleCheck (pcpc.FindConnectionPoint (FSinkIID, FCP));
    OleCheck (FCP.Advise (Self, FCookie));
    FSource := ASource;
  except
    raise Exception.Create (Format ('Unable to connect %s.'#13'%s',
      [Name, Exception (ExceptObject).Message]
    ));
  end;  { finally }
end;

procedure TSQLDMOEventsBaseSink.Disconnect;
begin
  if (FSource = nil) then Exit;
  try
    OleCheck (FCP.Unadvise (FCookie));
    FCP := nil;
    FSource := nil;
  except
    pointer (FCP) := nil;
    pointer (FSource) := nil;
  end;  { except }
end;


//SinkImplStart//

function TSQLDMOServerSink.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
  //SinkInvokeEnd//
end;

constructor TSQLDMOServerSink.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);
  //SinkInit//
  FSinkIID := ServerSink;
end;

//SinkImplementation//
function TSQLDMOServerSink.DoQueryTimeout(const Message: WideString; out Continue: WordBool): HResult;
begin
  Result := S_OK;
  if not Assigned (QueryTimeout) then System.Exit;
  Result := QueryTimeout (Self, Message, Continue);
end;

function TSQLDMOServerSink.DoServerMessage(Severity: Integer; MessageNumber: Integer; MessageState: Integer; const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (ServerMessage) then System.Exit;
  Result := ServerMessage (Self, Severity, MessageNumber, MessageState, Message);
end;

function TSQLDMOServerSink.DoConnectionBroken(const Message: WideString; out Retry: WordBool): HResult;
begin
  Result := S_OK;
  if not Assigned (ConnectionBroken) then System.Exit;
  Result := ConnectionBroken (Self, Message, Retry);
end;

function TSQLDMOServerSink.DoRemoteLoginFailed(Severity: Integer; MessageNumber: Integer; MessageState: Integer; const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (RemoteLoginFailed) then System.Exit;
  Result := RemoteLoginFailed (Self, Severity, MessageNumber, MessageState, Message);
end;

function TSQLDMOServerSink.DoCommandSent(const SQLCommand: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (CommandSent) then System.Exit;
  Result := CommandSent (Self, SQLCommand);
end;



function TSQLDMOBulkCopySink.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
  //SinkInvokeEnd//
end;

constructor TSQLDMOBulkCopySink.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);
  //SinkInit//
  FSinkIID := BulkCopySink;
end;

//SinkImplementation//
function TSQLDMOBulkCopySink.DoRowsCopied(const Message: WideString; Rows: Integer): HResult;
begin
  Result := S_OK;
  if not Assigned (RowsCopied) then System.Exit;
  Result := RowsCopied (Self, Message, Rows);
end;

function TSQLDMOBulkCopySink.DoBatchImported(const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (BatchImported) then System.Exit;
  Result := BatchImported (Self, Message);
end;



function TSQLDMOTransferSink.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
  //SinkInvokeEnd//
end;

constructor TSQLDMOTransferSink.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);
  //SinkInit//
  FSinkIID := TransferSink;
end;

//SinkImplementation//
function TSQLDMOTransferSink.DoStatusMessage(const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (StatusMessage) then System.Exit;
  Result := StatusMessage (Self, Message);
end;

function TSQLDMOTransferSink.DoPercentCompleteAtStep(const Message: WideString; Percent: Integer): HResult;
begin
  Result := S_OK;
  if not Assigned (PercentCompleteAtStep) then System.Exit;
  Result := PercentCompleteAtStep (Self, Message, Percent);
end;

function TSQLDMOTransferSink.DoScriptTransferPercentComplete(const Message: WideString; Percent: Integer): HResult;
begin
  Result := S_OK;
  if not Assigned (ScriptTransferPercentComplete) then System.Exit;
  Result := ScriptTransferPercentComplete (Self, Message, Percent);
end;

function TSQLDMOTransferSink.DoTransferPercentComplete(const Message: WideString; Percent: Integer): HResult;
begin
  Result := S_OK;
  if not Assigned (TransferPercentComplete) then System.Exit;
  Result := TransferPercentComplete (Self, Message, Percent);
end;



function TSQLDMORestoreSink.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
  //SinkInvokeEnd//
end;

constructor TSQLDMORestoreSink.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);
  //SinkInit//
  FSinkIID := RestoreSink;
end;

//SinkImplementation//
function TSQLDMORestoreSink.DoPercentComplete(const Message: WideString; Percent: Integer): HResult;
begin
  Result := S_OK;
  if not Assigned (PercentComplete) then System.Exit;
  Result := PercentComplete (Self, Message, Percent);
end;

function TSQLDMORestoreSink.DoNextMedia(const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (NextMedia) then System.Exit;
  Result := NextMedia (Self, Message);
end;

function TSQLDMORestoreSink.DoComplete(const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (Complete) then System.Exit;
  Result := Complete (Self, Message);
end;



function TSQLDMOReplicationSink.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
  //SinkInvokeEnd//
end;

constructor TSQLDMOReplicationSink.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);
  //SinkInit//
  FSinkIID := ReplicationSink;
end;

//SinkImplementation//
function TSQLDMOReplicationSink.DoStatusMessage(const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (StatusMessage) then System.Exit;
  Result := StatusMessage (Self, Message);
end;

function TSQLDMOReplicationSink.DoPercentComplete(const Message: WideString; Percent: Integer): HResult;
begin
  Result := S_OK;
  if not Assigned (PercentComplete) then System.Exit;
  Result := PercentComplete (Self, Message, Percent);
end;



function TSQLDMOBackupSink.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps: TDispParams; pDispIds: PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
  //SinkInvokeEnd//
end;

constructor TSQLDMOBackupSink.Create (AOwner: TComponent);
begin
  inherited Create (AOwner);
  //SinkInit//
  FSinkIID := BackupSink;
end;

//SinkImplementation//
function TSQLDMOBackupSink.DoPercentComplete(const Message: WideString; Percent: Integer): HResult;
begin
  Result := S_OK;
  if not Assigned (PercentComplete) then System.Exit;
  Result := PercentComplete (Self, Message, Percent);
end;

function TSQLDMOBackupSink.DoNextMedia(const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (NextMedia) then System.Exit;
  Result := NextMedia (Self, Message);
end;

function TSQLDMOBackupSink.DoComplete(const Message: WideString): HResult;
begin
  Result := S_OK;
  if not Assigned (Complete) then System.Exit;
  Result := Complete (Self, Message);
end;


//SinkImplEnd//

procedure Register;
begin
  //SinkRegisterStart//
  RegisterComponents ('ActiveX', [TSQLDMOServerSink]);
  RegisterComponents ('ActiveX', [TSQLDMOBulkCopySink]);
  RegisterComponents ('ActiveX', [TSQLDMOTransferSink]);
  RegisterComponents ('ActiveX', [TSQLDMORestoreSink]);
  RegisterComponents ('ActiveX', [TSQLDMOReplicationSink]);
  RegisterComponents ('ActiveX', [TSQLDMOBackupSink]);
  //SinkRegisterEnd//
end;

end.
