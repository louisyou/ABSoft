unit main;

interface

uses
  ABPubFuncU,
  ABPubFormU,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm9 = class(TABPubForm)
    edt1: TEdit;
    btn1: TButton;
    edt2: TEdit;
    edt3: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    mmo1: TMemo;
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form9: TForm9;

implementation

{$R *.dfm}

procedure TForm9.btn1Click(Sender: TObject);
var
  Rec:TSearchRec;
  I:Integer;
  procedure changename(Full:string);
  begin
    renamefile(edt1.Text+'\'+Full,edt1.Text+'\'+ABStringReplace(Full,edt2.Text,edt3.Text));
    mmo1.Lines.Add(edt1.Text+'\'+Full);
  end;
begin
  if (edt1.Text=EmptyStr) or
     (edt2.Text=EmptyStr) then
    exit;

  mmo1.Lines.Clear;
  I:=FindFirst(edt1.Text+'\*'+edt2.Text+'*.*',faAnyFile,Rec);
  while I=0 do
  begin
    ChangeName(Rec.Name);
    I:=findNext(Rec);
  end;
  FindClose(Rec);
end;

end.
