object Form9: TForm9
  Left = 0
  Top = 0
  Anchors = [akLeft, akTop, akBottom]
  Caption = #25209#37327#20462#25913#25991#20214#12289#30446#24405#21517
  ClientHeight = 484
  ClientWidth = 861
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    861
    484)
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 8
    Top = 21
    Width = 48
    Height = 13
    Caption = #23558#30446#24405#65306
  end
  object lbl2: TLabel
    Left = 392
    Top = 21
    Width = 36
    Height = 13
    Anchors = [akTop, akRight]
    Caption = #20013#21253#21547
    ExplicitLeft = 287
  end
  object lbl3: TLabel
    Left = 529
    Top = 21
    Width = 108
    Height = 13
    Anchors = [akTop, akRight]
    Caption = #30340#25991#20214#19982#30446#24405#20462#25913#20026
    ExplicitLeft = 424
  end
  object edt1: TEdit
    Tag = 91
    Left = 57
    Top = 17
    Width = 329
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    ExplicitWidth = 224
  end
  object btn1: TButton
    Left = 770
    Top = 16
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #25191#34892
    TabOrder = 0
    OnClick = btn1Click
    ExplicitLeft = 665
  end
  object edt2: TEdit
    Tag = 91
    Left = 434
    Top = 17
    Width = 89
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 2
    ExplicitLeft = 329
  end
  object edt3: TEdit
    Tag = 91
    Left = 643
    Top = 17
    Width = 121
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 3
    ExplicitLeft = 538
  end
  object mmo1: TMemo
    Left = 8
    Top = 48
    Width = 837
    Height = 428
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'mmo1')
    TabOrder = 4
    ExplicitWidth = 732
  end
end
