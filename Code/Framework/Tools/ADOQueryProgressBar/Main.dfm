object ABADOProgressBaForm: TABADOProgressBaForm
  Left = 192
  Top = 138
  Caption = #24314#20010#36827#24230#26465
  ClientHeight = 514
  ClientWidth = 747
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 120
    Top = 24
    Width = 353
    Height = 13
    AutoSize = False
  end
  object Label2: TLabel
    Left = 120
    Top = 8
    Width = 361
    Height = 13
    AutoSize = False
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 112
    Width = 673
    Height = 337
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 24
    Top = 47
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 0
  end
  object ProgressBar1: TProgressBar
    Left = 16
    Top = 88
    Width = 665
    Height = 16
    TabOrder = 1
  end
  object Button1: TButton
    Left = 24
    Top = 16
    Width = 75
    Height = 25
    Caption = 'close open'
    TabOrder = 3
    OnClick = Button1Click
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 24
    Top = 144
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    OnFetchComplete = ADOQuery1FetchComplete
    OnFetchProgress = ADOQuery1FetchProgress
    Parameters = <>
    Left = 56
    Top = 144
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 104
    Top = 144
  end
end
