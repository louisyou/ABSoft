unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RegExpr, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    MemoInput: TMemo;
    Button3: TButton;
    Panel2: TPanel;
    Memo1: TMemo;
    Panel3: TPanel;
    Label1: TLabel;
    edtExpression: TEdit;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ExprTest(AExpression, AInput: string);
  end;

var
  Form1: TForm1;
  RegExpr: TRegExpr;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  i: integer;
begin
  //增加一个循环，实现对每一行的匹配检测
  for i := 0 to MemoInput.Lines.Count - 1 do
  begin
    ExprTest(edtExpression.Text, MemoInput.Lines[i]);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  RegExpr := TRegExpr.Create;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(RegExpr);
end;


procedure TForm1.ExprTest(AExpression, AInput: string);
var
  i: integer;
begin
//正则表达式 http://www\.mynet\.com/register\.asp\?id=(\d+)&name=(\w+)
//输入字符串 http://www.mynet.com/register.asp?id=999&name=demo_ds
  if AExpression = '' then
  begin
    Application.MessageBox(PChar('正则表达式不能为空'),
      PChar('提示'));
    Exit;
  end;
{  if AInput='' then
  begin
    Application.MessageBox(PAnsiChar('输入字符串不能为空'),
                           PAnsiChar('提示'));
    Exit;
  end;  }
//  Memo1.Clear;

  RegExpr.Expression := AExpression;
  RegExpr.InputString := AInput;
  if RegExpr.Exec(AInput) then
  begin
    
    Memo1.Lines.Add('**************************');
    Memo1.Lines.Add('匹配结果：');
    Memo1.Lines.Add(Format('输入字符串为"%s"', [RegExpr.Match[0]]));

    Memo1.Lines.Add(Format('匹配子项数目: %d 个', [RegExpr.SubExprMatchCount]));
    for i := 1 to RegExpr.SubExprMatchCount do
    begin
      Memo1.Lines.Add(Format('Match[%d]=', [i]) + RegExpr.Match[i]);
    end;
    Memo1.Lines.Add('匹配成功！');
  end ;
  Application.ProcessMessages;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  MemoInput.Clear;
end;

end.

