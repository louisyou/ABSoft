object MainForm: TMainForm
  Left = 0
  Top = 0
  Anchors = []
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  Caption = #25209#37327'Ping'
  ClientHeight = 414
  ClientWidth = 742
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 742
    Height = 374
    Align = alClient
    BevelInner = bvLowered
    Caption = 'Panel1'
    TabOrder = 0
    object Label5: TLabel
      Left = 2
      Top = 2
      Width = 738
      Height = 13
      Align = alTop
      Caption = 
        'Ping'#22320#22336#21015#34920'                                                        ' +
        '                                           Ping'#32467#26524
      ExplicitWidth = 409
    end
    object Memo1: TMemo
      Tag = 910
      Left = 2
      Top = 15
      Width = 207
      Height = 357
      Align = alLeft
      ScrollBars = ssVertical
      TabOrder = 0
      OnKeyDown = Memo2KeyDown
    end
    object Memo2: TMemo
      Tag = 910
      Left = 209
      Top = 15
      Width = 531
      Height = 357
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 1
      OnKeyDown = Memo2KeyDown
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 374
    Width = 742
    Height = 40
    Align = alBottom
    TabOrder = 1
    object label2: TLabel
      Left = 8
      Top = 8
      Width = 34
      Height = 13
      Caption = 'IP'#22320#22336
    end
    object Label1: TLabel
      Left = 317
      Top = 8
      Width = 12
      Height = 13
      Caption = #20010
    end
    object Label3: TLabel
      Left = 338
      Top = 9
      Width = 48
      Height = 13
      Caption = #36229#26102#35774#32622
    end
    object Label4: TLabel
      Left = 446
      Top = 9
      Width = 24
      Height = 13
      Caption = #27627#31186
    end
    object Edit1: TEdit
      Tag = 91
      Left = 48
      Top = 5
      Width = 82
      Height = 21
      ImeName = #24555#20048#20116#31508
      TabOrder = 0
      Text = '192.168.0.1'
    end
    object Button1: TButton
      Left = 487
      Top = 6
      Width = 75
      Height = 25
      Caption = #28155#21152#21040#21015#34920
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 575
      Top = 6
      Width = 75
      Height = 25
      Caption = #24320#22987'Ping'
      TabOrder = 2
      OnClick = Button2Click
    end
    object CheckBox1: TCheckBox
      Tag = 91
      Left = 136
      Top = 6
      Width = 130
      Height = 17
      Caption = #25353#26368#21518#19968#27573#36830#32493#28155#21152
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object Button3: TButton
      Left = 656
      Top = 6
      Width = 75
      Height = 25
      Caption = #20572' '#27490
      TabOrder = 4
      OnClick = Button3Click
    end
    object Edit2: TEdit
      Tag = 91
      Left = 264
      Top = 6
      Width = 47
      Height = 21
      ImeName = #24555#20048#20116#31508
      TabOrder = 5
      Text = '255'
    end
    object Edit3: TEdit
      Tag = 91
      Left = 392
      Top = 6
      Width = 47
      Height = 21
      ImeName = #24555#20048#20116#31508
      TabOrder = 6
      Text = '100'
    end
  end
  object IdIcmp: TIdIcmpClient
    ReceiveTimeout = 100
    Port = 10024
    Protocol = 1
    ProtocolIPv6 = 58
    IPVersion = Id_IPv4
    OnReply = IdIcmpReply
    Left = 200
    Top = 120
  end
end
