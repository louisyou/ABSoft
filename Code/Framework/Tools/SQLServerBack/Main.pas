unit Main;

interface
                              
uses
  ABPubUserU,
  ABPubVarU,
  ABPubFuncU,
  ABPubFormU,
  ABPubConstU,
  ABPubServiceU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdConnDatabaseU,

  Dialogs,Menus,Variants,SQLDMOEvents,DB,ADODB,ExtCtrls,Buttons,StdCtrls,Controls,
  Classes,ComCtrls,SQLDMO_TLB,ComObj,SysUtils,Forms;


type
  TMainForm = class(TForm)
    ProgressBar1: TProgressBar;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    RadioGroup1: TRadioGroup;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    Button4: TButton;
    RadioGroup2: TRadioGroup;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    SpeedButton1: TSpeedButton;
    Label7: TLabel;
    Edit3: TEdit;
    Button1: TButton;
    ComboBox2: TComboBox;
    ADOConnection1: TADOConnection;
    SaveDialog1: TSaveDialog;
    SQLDMOBackupSink1: TSQLDMOBackupSink;
    SQLDMORestoreSink1: TSQLDMORestoreSink;
    SpeedButton2: TSpeedButton;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    procedure Button4Click(Sender: TObject);
    function SQLDMOBackupSink1PercentComplete(Sender: TObject;
      const Message: WideString; Percent: Integer): HRESULT;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    FshowMsg_ByParams,
    FExit_ByParams:boolean;
    { Private declarations }
  public
    tempSQLDataPath:string;
    procedure AutoRunByParams;
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation


                                               
{$R *.dfm}

procedure TMainForm.AutoRunByParams;
begin
  if (ParamCount>=1) then
  begin
    ComboBox1.Text :=ParamStr(1);
    if (ParamCount>=2) then
    begin
      Edit1.Text :=ParamStr(2);
    end;
    if (ParamCount>=3) then
    begin
      Edit2.Text :=ParamStr(3);
    end;

    if (ParamCount>=4) then
    begin
      FshowMsg_ByParams:=false;
      Button4Click(Button4);
      if not FExit_ByParams then
      begin
        if (ParamCount>=4) then
        begin
          Edit3.Text :=ParamStr(4);
        end;
        if (ParamCount>=5) then
        begin
          ComboBox2.Text :=ParamStr(5);
        end;
        Button1Click(Button1);

        ShowMessage('备份数据库完成.');
        Close;
      end;
    end;
  end;
end;

procedure TMainForm.Button1Click(Sender: TObject);
var
  sv:_SQLServer;
  bk:_Backup;
  SQLServer:Variant;
  Backup: Variant;
  tempStr1:string;
begin
  if  ((Edit3.Text)='')  then
  begin
    if FshowMsg_ByParams then
    begin
      ShowMessage('请先选择备份后的文件名.');
      if Edit3.CanFocus then
        Edit3.SetFocus;
    end;
    Exit;
  end;
  if (ComboBox2.Text='') then
  begin
    if FshowMsg_ByParams then
    begin
      ShowMessage('请先选择要备份的数据库名.');
      if ComboBox2.CanFocus then
        ComboBox2.SetFocus;
    end;
    Exit;
  end;

  ProgressBar1.Position:=0;
  Button1.Enabled:=False;
  StatusBar1.Panels[0].Text:=('正在备份文件,请稍等');
  Application.ProcessMessages;
  try
    DeleteFile('c:\tempUnBackDataBase.bak');
    if RadioGroup1.ItemIndex=0 then
    begin
      sv := CoSQLServer.Create;
      sv.ApplicationName :='_SQLServer';
      bk:=CoBackup.Create;
      sv.LoginSecure := false;
      sv.Connect(ComboBox1.Text,edit1.Text,edit2.Text);
      bk.Database := ComboBox2.Text;
      bk.Action :=0;   //0完整备份，1差异备份，2文件组备份，3日志备份
      if ABIsLocal(ComboBox1.Text) then
        bk.Files := ABGetShortFileName(edit3.Text)
      else
        bk.Files :='c:\tempUnBackDataBase.bak';
      bk.Initialize := True;
      SQLDMOBackupSink1.Connect((bk));
      bk.SQLBackup(sv);
    end
    else if RadioGroup1.ItemIndex=1 then
    begin
      sqlserver := CreateOleObject( 'SQLDMO.SQLServer');
      Backup :=   CreateOleObject( 'SQLDMO.Backup');
      sqlserver.LoginSecure := false;
      sqlserver.Connect(ComboBox1.Text,edit1.Text,edit2.Text)  ;
      Backup.Database := ComboBox2.Text;
      backup.Action :=0;  //0完整备份，1差异备份，2文件组备份，3日志备份
      if ABIsLocal(ComboBox1.Text) then
        backup.Files := ABGetShortFileName(edit3.Text)
      else
        backup.Files :='c:\tempUnBackDataBase.bak';

      backup.Initialize := True;
      SQLDMOBackupSink1.Connect(IUnknown(backup));
      backup.SQLBackup(sqlserver);
      sqlserver.close;
    end;

    if not ABIsLocal(ComboBox1.Text) then
    begin
      tempStr1:=
      ' execute master..xp_cmdshell ''c:\TextCopy.exe /S localhost /U '+edit1.Text+' /P '+
                                     edit2.Text+' /D '+ComboBox2.Text+' /T '+
                                     '##Temp_BackDatabase'+' /C '+'Pkg'+' /W "'+
                                     'where Name=''''Back'''''+'" /F '+
                                     'c:\tempUnBackDataBase.bak'+' /I '''+
      ' execute master..xp_cmdshell ''del "'+'c:\tempUnBackDataBase.bak'+'"'+'''' ;
      ADOConnection1.Execute(tempStr1);
      ABDownToFileFromField('Main','##Temp_BackDatabase','Pkg','where Name='+ABQuotedStr('Back'),edit3.Text);
    end;

    if Assigned(Sender) then
    begin
      StatusBar1.Panels[0].Text:='备份成功';
      if FshowMsg_ByParams then
        ShowMessage('数据库备份成功.');
    end;
  finally
    ProgressBar1.Position:=0;
    Button1.Enabled:=true;
    StatusBar1.Panels[0].Text:=' ';

    sqlserver := null;
    backup := null;
    bk:= nil;
    sv:= nil;
  end;
end;

procedure TMainForm.Button4Click(Sender: TObject);
var
  tempStr:string;
begin
  inherited;
  TButton(Sender).Enabled:=False;
  tempStr:='';
  try
    try
      ADOConnection1.Close;
      if RadioGroup2.ItemIndex=0 then
      begin
        tempStr:='Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False';
        if ComboBox1.Text<>EmptyStr then
          tempStr:=tempStr+';Data Source='+ComboBox1.Text;
      end
      else if RadioGroup2.ItemIndex=1 then
      begin
        tempStr:='Provider=SQLOLEDB.1';
        if Edit2.Text<>EmptyStr then
          tempStr:=tempStr+';Password='+Edit2.Text;
        tempStr:=tempStr+';Persist Security Info=True';
        if Edit1.Text<>EmptyStr then
          tempStr:=tempStr+';User ID='+Edit1.Text;
        if ComboBox1.Text<>EmptyStr then
          tempStr:=tempStr+';Data Source='+ComboBox1.Text;
      end;
      ADOConnection1.ConnectionString:=tempStr;
      ADOConnection1.Open;
      ABFillStringsBySQL('Main',
                         'use master  select name from sysdatabases ',
                         [],nil,
                         ComboBox2.items
                         );

      tempSQLDataPath := ExtractFilePath(
        ABGetSQLValue('Main','select filename from master..sysdatabases where name=''master''',[],'' )
        );

      //因为TextCopy.exe执行时路径不能包含空格，故先将其copy到根目录下
      tempStr:=                                                       
      ' declare  @tempSQL  varchar(2000)                           '+
      ' exec  master.dbo.xp_regread                                     '+
      '            ''HKEY_LOCAL_MACHINE'',                                '+
      '            ''SOFTWARE\Microsoft\MSSQLSERVER\setup'',              '+
      '            ''SQLPath'',@tempSQL  output                      '+
      ' set  @tempSQL  = ''copy "''+ @tempSQL  +  ''\binn\TextCopy.exe " "c:\TextCopy.exe"'''+

      ' execute master..xp_cmdshell @tempSQL '+
      ' if exists (select * from tempdb.dbo.sysobjects where id = object_id(N''tempdb.[dbo].[##Temp_BackDatabase]'')  )' +
      ' drop table [##Temp_BackDatabase]' +
      ' CREATE TABLE  [##Temp_BackDatabase] (' +
      '	[Name] [varchar] (100) COLLATE Chinese_PRC_CI_AS NULL ,' +
      '	[Pkg] [image] NULL' +
      ' ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]' +
      ' insert into ##Temp_BackDatabase(Name,Pkg) values(''Back'' ,'''') ';

      ADOConnection1.Execute(tempStr);
      
      if RadioGroup2.ItemIndex=0 then
      begin
        FExit_ByParams:=true;
        GroupBox2.Enabled:=false;
        ShowMessage('连接成功,只有以SQL方式连接才可备份数据库.');
      end
      else if RadioGroup2.ItemIndex=1 then
      begin
        GroupBox2.Enabled:=true;
        if FshowMsg_ByParams then
          ShowMessage('连接成功.');
      end;
    except
      FExit_ByParams:=true;
      GroupBox2.Enabled:=false;
      ShowMessage('连接失败.');
    end;
  finally
    TButton(Sender).Enabled:=True;
  end;
end;

procedure TMainForm.ComboBox1Change(Sender: TObject);
begin
  GroupBox2.Enabled:=false;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  tempStr:string;
begin
  ABAddToConnList('Main',ADOConnection1);

  FshowMsg_ByParams:=True;
  FExit_ByParams:=false;

  GroupBox2.Enabled:=false;

  ABGetSqlServerList(ComboBox1.Items);

  tempStr:=ABPubUser.HostIP;
  if ComboBox1.Items.IndexOf(tempStr)<0 then
    ComboBox1.Items.Add(tempStr);

  tempStr:=ABPubUser.HostName;
  if ComboBox1.Items.IndexOf(tempStr)<0 then
    ComboBox1.Items.Add(tempStr);

  Timer1.Enabled:=true;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  inherited;
  if ADOConnection1.Connected then
    ADOConnection1.Execute(' execute master..xp_cmdshell ''del "c:\TextCopy.exe"'''+
                           ' drop table ##Temp_BackDatabase '
                           );
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  Edit3.Text:=ABSaveFile(Edit3.Text);

end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
begin
  ABGetSqlServerList(ComboBox1.Items);
end;

function TMainForm.SQLDMOBackupSink1PercentComplete(Sender: TObject;
  const Message: WideString; Percent: Integer): HRESULT;
begin
  inherited;
  result:=1;
  ProgressBar1.Position := percent;
  Application.ProcessMessages;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  try
    Timer1.Enabled:=false;
    AutoRunByParams;
  finally
    Timer1.Enabled:=true;
  end;
end;

end.








