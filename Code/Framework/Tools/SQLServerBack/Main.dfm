object MainForm: TMainForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'SQLServer'#25968#25454#24211#22791#20221
  ClientHeight = 207
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 172
    Width = 343
    Height = 16
    Align = alBottom
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 343
    Height = 100
    Align = alTop
    Caption = #36830#25509#20449#24687
    TabOrder = 0
    object Label8: TLabel
      Left = 41
      Top = 45
      Width = 42
      Height = 14
      Caption = #31649#29702#21592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 13
      Top = 71
      Width = 70
      Height = 14
      Caption = #31649#29702#21592#23494#30721
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object Label22: TLabel
      Left = 27
      Top = 20
      Width = 56
      Height = 14
      Caption = #26381#21153#22120#21517
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton2: TSpeedButton
      Left = 202
      Top = 15
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object RadioGroup1: TRadioGroup
      Tag = 91
      Left = 230
      Top = 10
      Width = 102
      Height = 26
      Columns = 2
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #23435#20307
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Sink'
        'OLE')
      ParentFont = False
      TabOrder = 3
    end
    object Edit1: TEdit
      Tag = 91
      Left = 90
      Top = 43
      Width = 135
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 1
      Text = 'sa'
      OnChange = ComboBox1Change
    end
    object Edit2: TEdit
      Tag = 91
      Left = 90
      Top = 69
      Width = 135
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 2
      OnChange = ComboBox1Change
    end
    object ComboBox1: TComboBox
      Tag = 91
      Left = 89
      Top = 15
      Width = 111
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 0
      OnChange = ComboBox1Change
    end
    object Button4: TButton
      Left = 230
      Top = 70
      Width = 100
      Height = 21
      Caption = #27979#35797#36830#25509
      Default = True
      TabOrder = 5
      OnClick = Button4Click
    end
    object RadioGroup2: TRadioGroup
      Tag = 91
      Left = 230
      Top = 36
      Width = 102
      Height = 29
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'WIN'
        'SQL')
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 100
    Width = 343
    Height = 72
    Align = alClient
    Caption = #22791#20221#21040#25991#20214
    Color = clBtnFace
    Enabled = False
    ParentColor = False
    TabOrder = 1
    object Label11: TLabel
      Left = 12
      Top = 22
      Width = 70
      Height = 14
      Caption = #22791#20221#25991#20214#21517
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton1: TSpeedButton
      Left = 308
      Top = 14
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label7: TLabel
      Left = 27
      Top = 45
      Width = 56
      Height = 14
      Caption = #25968#25454#24211#21517
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #26032#23435#20307
      Font.Style = []
      ParentFont = False
    end
    object Edit3: TEdit
      Tag = 91
      Left = 90
      Top = 15
      Width = 215
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 0
    end
    object Button1: TButton
      Left = 230
      Top = 40
      Width = 100
      Height = 21
      Caption = #22791#20221#21040#25991#20214
      TabOrder = 2
      OnClick = Button1Click
    end
    object ComboBox2: TComboBox
      Tag = 91
      Left = 90
      Top = 41
      Width = 139
      Height = 22
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -14
      Font.Name = #23435#20307
      Font.Style = []
      ImeName = #24555#20048#20116#31508
      ParentFont = False
      TabOrder = 1
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 188
    Width = 343
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Left = 184
    Top = 48
  end
  object SaveDialog1: TSaveDialog
    Left = 136
    Top = 48
  end
  object SQLDMOBackupSink1: TSQLDMOBackupSink
    PercentComplete = SQLDMOBackupSink1PercentComplete
    Left = 176
    Top = 80
  end
  object SQLDMORestoreSink1: TSQLDMORestoreSink
    PercentComplete = SQLDMOBackupSink1PercentComplete
    Left = 144
    Top = 76
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 216
    Top = 152
  end
end
