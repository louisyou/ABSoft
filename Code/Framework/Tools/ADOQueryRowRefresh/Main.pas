unit Main;

interface

uses
  ABPubVarU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DBCtrls, Grids, DBGrids, DB, ADODB, StdCtrls,adoint;

type
  TABRowRefreshForm = class(TForm)
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Button3: TButton;
    Edit1: TEdit;
    Button1: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABRowRefreshForm: TABRowRefreshForm;

implementation

{$R *.dfm}
var
  beginTime,EndTime:longint;

procedure TABRowRefreshForm.Button2Click(Sender: TObject);
begin
  beginTime:=GetTickCount;

  adoquery1.UpdateCursorPos;
//Dataset.RecordSet.resync是保持RecordSet和数据库同步
  adoquery1.Recordset.Resync(AdAffectCurrent, AdResyncAllValues);
//DataSet.Resync是保持DataSet数据和RecordSet同步
  adoquery1.Resync([]);


  EndTime := GetTickCount;
  Label1.Caption:= '总共花了'  + FloatToStr((EndTime  - beginTime) / 1000.0) +  '秒' ;
end;

procedure TABRowRefreshForm.Button3Click(Sender: TObject);
begin
  beginTime:=GetTickCount;
  ADOQuery1.Close;
  ADOQuery1.open;

  EndTime := GetTickCount;
  Label1.Caption:=( '总共花了' )+ FloatToStr((EndTime  - beginTime) / 1000.0) +( '秒');
  Label2.Caption:= ( '共有') + IntToStr( ADOQuery1.RecordCount) +( ' 笔资料');
end;

procedure TABRowRefreshForm.FormCreate(Sender: TObject);
begin
  ADOConnection1.Connected:=false;
  ADOConnection1.ConnectionString:=
    'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+ABSoftSetPath+'Data.mdb;Persist Security Info=False';
  ADOConnection1.Connected:=true;
  Button3Click(nil);
end;

procedure TABRowRefreshForm.Button1Click(Sender: TObject);
begin
  ADOConnection1.Execute(Edit1.Text);
end;

end.


