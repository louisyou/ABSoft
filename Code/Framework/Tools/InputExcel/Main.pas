unit Main;
                        
interface

uses
  ABPubUserU,
  ABPubVarU,
  ABPubFuncU,
  ABPubFormU,
  ABPubConstU,
  ABPubServiceU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdConnDatabaseU,

  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxGraphics,
  cxControls,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  ADODB,StdCtrls,ExtCtrls,ComCtrls,Buttons,Grids,ValEdit,ExcelXP,OleServer,
  cxSSheet;

type
  TMainForm = class(TABPubForm)
    ADOConnection1: TADOConnection;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    Button2: TButton;
    Button3: TButton;
    Panel2: TPanel;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Panel3: TPanel;
    Button8: TButton;
    Button9: TButton;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    ComboBox1: TComboBox;
    GroupBox2: TGroupBox;
    SpeedButton2: TSpeedButton;
    Edit3: TEdit;
    ProgressBar1: TProgressBar;
    Panel5: TPanel;
    Splitter1: TSplitter;
    RichEdit2: TRichEdit;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label7: TLabel;
    ComboBox2: TComboBox;
    Edit4: TEdit;
    Label2: TLabel;
    Edit5: TEdit;
    Label3: TLabel;
    Edit6: TEdit;
    Label5: TLabel;
    Edit7: TEdit;
    Label6: TLabel;
    Edit8: TEdit;
    Label10: TLabel;
    ValueListEditor1: TValueListEditor;
    GroupBox4: TGroupBox;
    RichEdit1: TRichEdit;
    OpenDialog1: TOpenDialog;
    ADOQuery1: TADOQuery;
    ProgressBar2: TProgressBar;
    cxSpreadSheetBook1: TcxSpreadSheetBook;
    Label11: TLabel;
    Edit10: TEdit;
    Edit9: TEdit;
    Label4: TLabel;
    Label12: TLabel;
    Edit11: TEdit;
    CheckBox1: TCheckBox;
    SpeedButton1: TSpeedButton;
    RichEdit3: TRichEdit;
    pnl1: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure FindColumnToField;
    procedure SpeedButton1Click(Sender: TObject);
  private
    TableList:TStringList;
    function FisrtSheetNext: boolean;
    function SecondSheetNext: boolean;
    function SecondSheetPri: boolean;
    function ThirdSheetPri: boolean;
    function PageControl1beforeChang: boolean;
    function CheckEmptyRow(aCurRow, aColumnCount: Integer): Boolean;
    function GetTableName(aCurRow, aColumnCount: Integer): string;
    function GetFieldValue(aSql,aTableName, aFieldName: string; aCurRow,aFieldNameRow,
      aColumnCount: Integer): string;
    procedure deleteTables;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation
                                                           
{$R *.dfm}
type
  TABCaleType=(ctadd,ctdec) ;


var
  tempCaleType:TABCaleType;

procedure TMainForm.FormCreate(Sender: TObject);
var
  tempStr:string;
begin
  ABAddToConnList('Main',ADOConnection1);

  TableList:=TStringList.Create;
  ABGetSqlServerList(ComboBox1.Items);

  tempStr:=ABPubUser.HostIP;
  if ComboBox1.Items.IndexOf(tempStr)<0 then
    ComboBox1.Items.Add(tempStr);

  tempStr:=ABPubUser.HostName;
  if ComboBox1.Items.IndexOf(tempStr)<0 then
    ComboBox1.Items.Add(tempStr);

  PageControl1.ActivePageIndex:=0;
  ProgressBar1.Visible:=false;

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  TableList.Free;
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectFile(ABGetFilepath(Edit3.Text),ABGetFilename(Edit3.Text,false));
  if tempStr<>EmptyStr then
  begin
    Edit3.Text:=tempStr;
  end;
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  tempCaleType:=ctAdd;
  if PageControl1beforeChang then
    PageControl1.ActivePageIndex:=PageControl1.ActivePageIndex+1;
end;

procedure TMainForm.Button8Click(Sender: TObject);
begin
  tempCaleType:=ctdec;
  if PageControl1beforeChang then
    PageControl1.ActivePageIndex:=PageControl1.ActivePageIndex-1;
end;

procedure TMainForm.Button3Click(Sender: TObject);
begin
  close;
end;

procedure TMainForm.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange:=false;
end;

function TMainForm.PageControl1beforeChang:boolean;
begin
  Result:=false;
  case PageControl1.ActivePageIndex of
    0:
    begin
      Result:= FisrtSheetNext;
    end;
    1:
    begin
      if tempCaleType=ctAdd then
        Result:=SecondSheetNext
      else if tempCaleType=ctdec then
        Result:=SecondSheetPri;
    end;
    2:
    begin
      Result:=ThirdSheetPri;
    end;
  end;
end;

function TMainForm.FisrtSheetNext:boolean;
var
  tempStr:string;
begin
  result:=False;
  if ((Edit3.Text)='')  then
  begin
    ShowMessage('连接的EXCEL文件不能为空,请检查.');
    if Edit3.CanFocus then
      Edit3.SetFocus;
    Exit;
  end
  else if not ABCheckFileExists(Edit3.Text) then
  begin
    ShowMessage('连接的EXCEL文件不存在,请检查.');
    if Edit3.CanFocus then
      Edit3.SetFocus;
    Exit;
  end;

  ADOConnection1.Close;
  if trim(Edit1.Text)=EmptyStr then
  begin
    tempStr:='Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False';
    if ComboBox1.Text<>EmptyStr then
      tempStr:=tempStr+';Data Source='+ComboBox1.Text;
  end
  else
  begin
    tempStr:='Provider=SQLOLEDB.1';
    if Edit2.Text<>EmptyStr then
      tempStr:=tempStr+';Password='+Edit2.Text;
    tempStr:=tempStr+';Persist Security Info=True';
    if Edit1.Text<>EmptyStr then
      tempStr:=tempStr+';User ID='+Edit1.Text;
    if ComboBox1.Text<>EmptyStr then
      tempStr:=tempStr+';Data Source='+ComboBox1.Text;
  end;
  ADOConnection1.ConnectionString:=tempStr;
  ADOConnection1.Open;
  ABFillStringsBySQL('Main','use master  select name from sysdatabases ',[],nil,ComboBox2.items);
  try
    cxSpreadSheetBook1.ClearAll;
    cxSpreadSheetBook1.LoadFromFile(Edit3.Text);
  except
  end;

  if ValueListEditor1.Strings.Count<=0 then
    FindColumnToField;
  result:=true;
end;

function TMainForm.CheckEmptyRow(aCurRow,aColumnCount:LongInt):Boolean;
var
  k : integer;
begin
  result:=true;
  for k:=0 to aColumnCount do
  begin
    if trim(cxSpreadSheetBook1.ActiveSheet.GetCellObject(k,aCurRow).Text)<>'' then
    begin
      result:=false;
      Break;
    end;
    if k>StrToIntDef(Edit8.Text,5) then
      Break;
  end;
end;

function TMainForm.GetTableName(aCurRow,aColumnCount:LongInt):string;
var
  i,j,k : integer;
  tempStr1:string;
begin
  result:=emptystr;
  for k:=StrToIntDef(edit11.text,1) to StrToIntDef(edit11.text,1) do
  begin
    tempStr1:= trim(cxSpreadSheetBook1.ActiveSheet.GetCellObject(k,aCurRow).Text);
    i:=ABPos(edit4.text,tempStr1);
    j:=ABPos(edit5.text,tempStr1);

    if (i>0) and
       (j>0)   then
    begin
      result:=Copy(tempStr1,i+1,j-i-1);
      break;
    end;
  end;
end;

function TMainForm.SecondSheetPri:boolean;
begin
  result:=true;
end;

function TMainForm.GetFieldValue(aSql,aTableName,aFieldName:string;aCurRow,aFieldNameRow,aColumnCount:LongInt):string;
var
  tempStr1,tempStr2:string;
  i:LongInt;
  tempQuery1:TADOQuery;
begin
  Result:=EmptyStr;
  if trim(aSql)=EmptyStr then
    Exit;

  tempQuery1:=TADOQuery.Create(nil);
  tempQuery1.Connection:=ADOConnection1;
  try
    ADOQuery1.Close;
    ADOQuery1.SQL.Text:=trim(aSql);
    if  Assigned(ADOQuery1.Parameters.FindParam('TableName')) then
      ADOQuery1.Parameters.FindParam('TableName').Value:=aTableName;
    if  Assigned(ADOQuery1.Parameters.FindParam('FieldName')) then
      ADOQuery1.Parameters.FindParam('FieldName').Value:=aFieldName;
    for i:=0 to aColumnCount do
    begin
      tempStr1:=trim(cxSpreadSheetBook1.ActiveSheet.GetCellObject(i,aFieldNameRow).Text);
      if  Assigned(ADOQuery1.Parameters.FindParam(tempStr1)) then
      begin
        tempStr2:=trim(cxSpreadSheetBook1.ActiveSheet.GetCellObject(i,aCurRow).Text);
        ADOQuery1.Parameters.FindParam(tempStr1).Value:=tempStr2;
      end;
    end;

    try
      ADOQuery1.Open;
    except
      ShowMessage(aSql);
      raise;
    end;
    
    tempStr1:=trim(ADOQuery1.Fields[0].AsString);

    if ABCheckBeginStr(tempStr1,'SELECT') then
    begin
      result:= GetFieldValue(tempStr1,aTableName,aFieldName,aCurRow,aFieldNameRow,aColumnCount);
    end
    else
      result:=tempStr1;
  finally
    tempQuery1.Free;
  end;
end;

procedure TMainForm.deleteTables;
var
  i,j,tempRowCount,tempColumnCount:LongInt;
  tempTableName:string;
begin
  TableList.Clear;
  j:=0;
  tempRowCount:=StrToIntDef(Edit10.Text,100);
  if cxSpreadSheetBook1.ActiveSheet.RowCount<tempRowCount then
    tempRowCount:=cxSpreadSheetBook1.ActiveSheet.RowCount;
  tempColumnCount:=StrToIntDef(Edit9.Text,50);
  if cxSpreadSheetBook1.ActiveSheet.ColumnCount<tempColumnCount then
    tempColumnCount:=cxSpreadSheetBook1.ActiveSheet.ColumnCount;

  while j<=tempRowCount do
  begin
    ProgressBar2.Position:=j+1;

    tempTableName:=GetTableName(j,tempColumnCount);
    if tempTableName<>EmptyStr then
    begin
      if TableList.IndexOf(tempTableName)<0 then
        TableList.Add(tempTableName);
    end;
    j:=j+1;
    Application.ProcessMessages;
  end;

  for i := 0 to TableList.Count-1 do
  begin
    ADOConnection1.Execute(' delete '+TableList.Strings[i]);
  end;
end;

function TMainForm.SecondSheetNext:boolean;
var
  i,j,k,tempRowCount,tempColumnCount,tempTableNameRow,tempFieldNameRow:LongInt;
  tempStr1,tempStr2,tempStr3,tempExecSQL_FieldName,tempExecSQL_Fieldvalue,tempExecSQL_Where,tempTableName:string;
begin
  result:=False;
  if combobox2.Text= EmptyStr then
    exit;
  RichEdit3.Text:=StringReplace(RichEdit3.Text,';',',',[]);
  tempTableNameRow:=0;
  tempFieldNameRow:=0;
  ADOConnection1.Execute('use '+combobox2.Text);
  if ValueListEditor1.Strings.Count<=0 then
    FindColumnToField;
  if (ValueListEditor1.Strings.Count>0) and (CheckBox1.Checked) then
    deleteTables;

  ProgressBar1.Visible:=true;
  ProgressBar1.Max:=cxSpreadSheetBook1.PageCount;
  ProgressBar1.Min:=0;
  ProgressBar1.Position:=0;
  ProgressBar2.Visible:=true;
  try
    for i := 0 to cxSpreadSheetBook1.PageCount-1 do
    begin
      cxSpreadSheetBook1.ActivePage:=i;
      ProgressBar1.Position:=i+1;

      tempColumnCount:=StrToIntDef(Edit9.Text,50);
      if cxSpreadSheetBook1.ActiveSheet.ColumnCount<tempColumnCount then
        tempColumnCount:=cxSpreadSheetBook1.ActiveSheet.ColumnCount;

      tempRowCount:=StrToIntDef(Edit10.Text,100);
      if cxSpreadSheetBook1.ActiveSheet.RowCount<tempRowCount then
        tempRowCount:=cxSpreadSheetBook1.ActiveSheet.RowCount;

      ProgressBar2.Max:=tempRowCount;
      ProgressBar2.Min:=0;
      ProgressBar2.Position:=0;
      j:=0;
      while j<=tempRowCount do
      begin
        ProgressBar2.Position:=j+1;

        if not CheckEmptyRow(j,tempColumnCount) then
        begin

          tempExecSQL_FieldName:=EmptyStr;
          tempExecSQL_Fieldvalue:=EmptyStr;
          tempExecSQL_Where:=EmptyStr;

          tempStr1:=GetTableName(j,tempColumnCount);
          if tempStr1<>EmptyStr then
          begin
            tempTableName:=tempStr1;
            tempTableNameRow:=j;
            j:=j+StrToIntDef(Edit6.Text,0);
            tempFieldNameRow:=j;
          end
          else if j<>tempTableNameRow+StrToIntDef(Edit7.Text,2) then
          begin
            for k:=0 to tempColumnCount do
            begin
              tempStr1:= Trim(cxSpreadSheetBook1.ActiveSheet.GetCellObject(k,tempFieldNameRow).Text);
              tempStr2:= Trim(cxSpreadSheetBook1.ActiveSheet.GetCellObject(k,j).Text);
              if tempStr2= EmptyStr then
                tempStr2:=GetFieldValue(trim(RichEdit1.Text),tempTableName,tempStr1,j,tempFieldNameRow,tempColumnCount);

              if tempStr2<> EmptyStr then
              begin
                if (ABPos('800',tempStr1)<=0) and (ABPos('700',tempStr1)<=0) and
                   (
                    (ABPos(','+tempStr1+',',','+RichEdit3.Text+',')<=0)
                    ) then
                begin
                  ABAddstr(tempExecSQL_Where,tempStr1+'='+ABQuotedStr(tempStr2),' and ');
                end;

                ABAddstr(tempExecSQL_FieldName,tempStr1);

                ABAddstr(tempExecSQL_Fieldvalue,ABQuotedStr(tempStr2));
              end;
            end;
            if tempExecSQL_FieldName<>EmptyStr then
            begin
              try
                if not CheckBox1.Checked then
                begin
                  tempStr3:='delete '+ tempTableName+' where  '+tempExecSQL_Where;
                  ADOConnection1.Execute(tempStr3);
                end;

                tempStr3:='insert '+
                                        tempTableName+'('+tempExecSQL_FieldName+')'+
                                        ' values('+tempExecSQL_Fieldvalue+')';
                ADOConnection1.Execute(tempStr3);
              except
                ShowMessage(cxSpreadSheetBook1.ActiveSheet.Caption+'材'+inttostr(j+1)+'︽岿'+#13#10+tempStr3);
                raise;
                Exit;
              end;
            end;
          end;
        end;
        j:=j+1;
        Application.ProcessMessages;
      end;
      Application.ProcessMessages;
    end;
  finally
    ProgressBar1.Visible:=false;
    ProgressBar1.Position:=0;
    ProgressBar2.Visible:=false;
    ProgressBar2.Position:=0;
  end;

  result:=true;
end;

function TMainForm.ThirdSheetPri:boolean;
begin
  result:=true;
end;

procedure TMainForm.FindColumnToField;
var
  i,j,k,tempRowCount,tempColumnCount:LongInt;
  tempStr1,tempTableName:string;
begin
  ValueListEditor1.Strings.Clear;
  ProgressBar1.Visible:=true;
  ProgressBar1.Max:=cxSpreadSheetBook1.PageCount;
  ProgressBar1.Min:=0;
  ProgressBar1.Position:=0;
  ProgressBar2.Visible:=true;
  try
    for i := 0 to cxSpreadSheetBook1.PageCount-1 do
    begin
      cxSpreadSheetBook1.ActivePage:=i;
      ProgressBar1.Position:=i+1;

      tempColumnCount:=StrToIntDef(Edit9.Text,50);
      if cxSpreadSheetBook1.ActiveSheet.ColumnCount<tempColumnCount then
        tempColumnCount:=cxSpreadSheetBook1.ActiveSheet.ColumnCount;

      tempRowCount:=StrToIntDef(Edit10.Text,100);
      if cxSpreadSheetBook1.ActiveSheet.RowCount<tempRowCount then
        tempRowCount:=cxSpreadSheetBook1.ActiveSheet.RowCount;

      ProgressBar2.Max:=tempRowCount;
      ProgressBar2.Min:=0;
      ProgressBar2.Position:=0;
      j:=0;
      while j<=tempRowCount do
      begin
        ProgressBar2.Position:=j+1;

        tempTableName:=GetTableName(j,tempColumnCount);
        if tempTableName<>EmptyStr then
        begin
          j:=j+StrToIntDef(Edit6.Text,0);
          for k:=0 to tempColumnCount do
          begin
            tempStr1:= Trim(cxSpreadSheetBook1.ActiveSheet.GetCellObject(k,j).Text);
            if tempStr1<> EmptyStr then
              ValueListEditor1.Strings.Add(tempTableName+'.'+tempStr1+'='+tempTableName+'.'+tempStr1);
          end;
        end;
        j:=j+1;
        Application.ProcessMessages;
      end;
      Application.ProcessMessages;
    end;
  finally
    ProgressBar1.Visible:=false;
    ProgressBar1.Position:=0;
    ProgressBar2.Visible:=false;
    ProgressBar2.Position:=0;
  end;
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
begin
  FindColumnToField;
end;

end.
