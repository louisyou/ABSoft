object MainForm: TMainForm
  Left = 187
  Top = 160
  Caption = #23548#20837'EXCEL'#25968#25454#24037#20855
  ClientHeight = 566
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 792
    Height = 532
    ActivePage = TabSheet2
    Align = alClient
    MultiLine = True
    TabOrder = 0
    TabStop = False
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = '(1).'#36830#25509#35774#32622
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 463
        Width = 784
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          784
          41)
        object Button2: TButton
          Left = 604
          Top = 8
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #19979#19968#27493
          TabOrder = 0
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 692
          Top = 8
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #36864#20986
          TabOrder = 1
          OnClick = Button3Click
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 113
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 249
          Height = 113
          Align = alLeft
          Caption = #25968#25454#24211#36830#25509
          TabOrder = 0
          object Label8: TLabel
            Left = 39
            Top = 50
            Width = 42
            Height = 14
            Caption = #31649#29702#21592
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #26032#23435#20307
            Font.Style = []
            ParentFont = False
          end
          object Label9: TLabel
            Left = 13
            Top = 76
            Width = 70
            Height = 14
            Caption = #31649#29702#21592#23494#30721
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #26032#23435#20307
            Font.Style = []
            ParentFont = False
          end
          object Label22: TLabel
            Left = 27
            Top = 25
            Width = 56
            Height = 14
            Caption = #26381#21153#22120#21517
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #26032#23435#20307
            Font.Style = []
            ParentFont = False
          end
          object Edit1: TEdit
            Tag = 91
            Left = 90
            Top = 48
            Width = 135
            Height = 22
            Font.Charset = GB2312_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #23435#20307
            Font.Style = []
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            ParentFont = False
            TabOrder = 1
            Text = 'sa'
          end
          object Edit2: TEdit
            Tag = 91
            Left = 90
            Top = 74
            Width = 135
            Height = 22
            Font.Charset = GB2312_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #23435#20307
            Font.Style = []
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            ParentFont = False
            PasswordChar = '*'
            TabOrder = 2
          end
          object ComboBox1: TComboBox
            Tag = 91
            Left = 90
            Top = 19
            Width = 135
            Height = 22
            Font.Charset = GB2312_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #23435#20307
            Font.Style = []
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            ParentFont = False
            TabOrder = 0
          end
        end
        object GroupBox2: TGroupBox
          Left = 249
          Top = 0
          Width = 535
          Height = 113
          Align = alClient
          Caption = 'EXCEL'#25991#20214#36830#25509
          TabOrder = 1
          DesignSize = (
            535
            113)
          object SpeedButton2: TSpeedButton
            Left = 495
            Top = 19
            Width = 22
            Height = 22
            Anchors = [akTop, akRight]
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object Edit3: TEdit
            Tag = 91
            Left = 16
            Top = 20
            Width = 469
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = '(2).EXCEL'#20998#35299#35774#32622
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 0
        Top = 140
        Width = 784
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object Panel2: TPanel
        Left = 0
        Top = 463
        Width = 784
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          784
          41)
        object Button4: TButton
          Left = 516
          Top = 8
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #19978#19968#27493
          TabOrder = 0
          OnClick = Button8Click
        end
        object Button5: TButton
          Left = 604
          Top = 8
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #19979#19968#27493
          TabOrder = 1
          OnClick = Button2Click
        end
        object Button6: TButton
          Left = 692
          Top = 8
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #36864#20986
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 143
        Width = 784
        Height = 320
        Align = alBottom
        TabOrder = 1
        object GroupBox3: TGroupBox
          Left = 1
          Top = 1
          Width = 273
          Height = 318
          Align = alLeft
          Caption = #23545#24212#20851#31995#35774#32622
          TabOrder = 0
          DesignSize = (
            273
            318)
          object Label1: TLabel
            Left = 8
            Top = 48
            Width = 77
            Height = 13
            AutoSize = False
            Caption = #34920#21517#21069#32512
          end
          object Label7: TLabel
            Left = 133
            Top = 22
            Width = 85
            Height = 14
            AutoSize = False
            Caption = #25968#25454#24211
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #26032#23435#20307
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 132
            Top = 47
            Width = 55
            Height = 13
            AutoSize = False
            Caption = #34920#21517#21518#32512
          end
          object Label3: TLabel
            Left = 8
            Top = 98
            Width = 159
            Height = 13
            AutoSize = False
            Caption = #34920#21517#21518#31532'N'#34892#20026#23383#27573'(N'#20026')'
          end
          object Label5: TLabel
            Left = 8
            Top = 122
            Width = 159
            Height = 13
            AutoSize = False
            Caption = #34920#21517#21518#31532'N'#34892#20026#27880#37322'(N'#20026')'
          end
          object Label6: TLabel
            Left = 8
            Top = 146
            Width = 135
            Height = 13
            AutoSize = False
            Caption = 'N'#21015#20026#31354#26102#36339#36807'(N'#20026')'
          end
          object Label10: TLabel
            Left = 8
            Top = 190
            Width = 149
            Height = 13
            AutoSize = False
            Caption = #23383#27573#21517#23545#24212#20851#31995
          end
          object Label11: TLabel
            Left = 132
            Top = 73
            Width = 80
            Height = 13
            AutoSize = False
            Caption = #26368#22823#34892
          end
          object Label4: TLabel
            Left = 7
            Top = 74
            Width = 80
            Height = 13
            AutoSize = False
            Caption = #26368#22823#21015
          end
          object Label12: TLabel
            Left = 8
            Top = 23
            Width = 77
            Height = 13
            AutoSize = False
            Caption = #34920#21517#21015
          end
          object SpeedButton1: TSpeedButton
            Left = 112
            Top = 184
            Width = 153
            Height = 22
            Caption = #37325#26032#20998#26512#34920#21517#19982#23383#27573#21517
            Flat = True
            OnClick = SpeedButton1Click
          end
          object ComboBox2: TComboBox
            Tag = 91
            Left = 188
            Top = 18
            Width = 79
            Height = 22
            Font.Charset = GB2312_CHARSET
            Font.Color = clWindowText
            Font.Height = -14
            Font.Name = #23435#20307
            Font.Style = []
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            ParentFont = False
            TabOrder = 0
          end
          object Edit4: TEdit
            Tag = 91
            Left = 72
            Top = 45
            Width = 53
            Height = 21
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 1
            Text = '['
          end
          object Edit5: TEdit
            Tag = 91
            Left = 188
            Top = 45
            Width = 77
            Height = 21
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 2
            Text = ']'
          end
          object Edit6: TEdit
            Tag = 91
            Left = 188
            Top = 98
            Width = 76
            Height = 21
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 3
            Text = '5'
          end
          object Edit7: TEdit
            Tag = 91
            Left = 188
            Top = 122
            Width = 76
            Height = 21
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 4
            Text = '5'
          end
          object Edit8: TEdit
            Tag = 91
            Left = 188
            Top = 146
            Width = 76
            Height = 21
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 5
            Text = '5'
          end
          object ValueListEditor1: TValueListEditor
            Left = 8
            Top = 208
            Width = 257
            Height = 100
            Anchors = [akLeft, akTop, akBottom]
            Strings.Strings = (
              '')
            TabOrder = 6
            TitleCaptions.Strings = (
              'Excel'#20013#34920#21517'+'#21015#21517'>>'
              #25968#25454#24211#20013#34920#21517'+'#23383#27573#21517)
            ColWidths = (
              150
              101)
          end
          object Edit10: TEdit
            Tag = 91
            Left = 188
            Top = 71
            Width = 76
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 7
            Text = '100'
          end
          object Edit9: TEdit
            Tag = 91
            Left = 72
            Top = 71
            Width = 53
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 8
            Text = '50'
          end
          object Edit11: TEdit
            Tag = 91
            Left = 72
            Top = 20
            Width = 53
            Height = 21
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            TabOrder = 9
            Text = '1'
          end
          object CheckBox1: TCheckBox
            Tag = 91
            Left = 8
            Top = 168
            Width = 105
            Height = 17
            Caption = #23548#20837#21069#28165#31354#34920
            TabOrder = 10
          end
        end
        object GroupBox4: TGroupBox
          Left = 274
          Top = 1
          Width = 509
          Height = 318
          Align = alClient
          Caption = #21015#21462#20540#35774#32622
          TabOrder = 1
          DesignSize = (
            509
            318)
          object RichEdit1: TRichEdit
            Left = 10
            Top = 15
            Width = 490
            Height = 297
            Anchors = [akLeft, akTop, akRight, akBottom]
            Font.Charset = GB2312_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
            ParentFont = False
            ScrollBars = ssBoth
            TabOrder = 0
            Zoom = 100
          end
          object RichEdit3: TRichEdit
            Left = 2
            Top = 264
            Width = 505
            Height = 52
            Align = alBottom
            Font.Charset = GB2312_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ImeName = '?'#28165'?'#20116'??'#20837#27861' FOR WINXP'
            ParentFont = False
            ScrollBars = ssBoth
            TabOrder = 1
            Zoom = 100
          end
          object pnl1: TPanel
            Left = 2
            Top = 240
            Width = 505
            Height = 24
            Align = alBottom
            Alignment = taLeftJustify
            Caption = #30070#23566#20837#21069#19981#28165#31354#34920#26178#19981#20986#29694#22312'WHERE'#20013#30340#23383#27573
            TabOrder = 2
          end
        end
      end
      object cxSpreadSheetBook1: TcxSpreadSheetBook
        Left = 0
        Top = 0
        Width = 784
        Height = 140
        Align = alClient
        DefaultStyle.Font.Name = 'MS Sans Serif'
        HeaderFont.Charset = DEFAULT_CHARSET
        HeaderFont.Color = clWindowText
        HeaderFont.Height = -11
        HeaderFont.Name = 'MS Sans Serif'
        HeaderFont.Style = []
        RowHeaderWidth = 40
      end
    end
    object TabSheet3: TTabSheet
      Caption = '(3).'#32467#26524#25551#36848
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel3: TPanel
        Left = 0
        Top = 463
        Width = 784
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          784
          41)
        object Button8: TButton
          Left = 604
          Top = 8
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #19978#19968#27493
          TabOrder = 0
          OnClick = Button8Click
        end
        object Button9: TButton
          Left = 692
          Top = 8
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = #36864#20986
          TabOrder = 1
          OnClick = Button3Click
        end
      end
      object RichEdit2: TRichEdit
        Tag = 910
        Left = 0
        Top = 0
        Width = 784
        Height = 463
        Align = alClient
        Font.Charset = GB2312_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ImeName = #39118#28165#25196#20116#31508#36755#20837#27861' FOR WINXP'
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 1
        Zoom = 100
      end
    end
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 532
    Width = 792
    Height = 17
    Align = alBottom
    TabOrder = 1
    Visible = False
  end
  object ProgressBar2: TProgressBar
    Left = 0
    Top = 549
    Width = 792
    Height = 17
    Align = alBottom
    TabOrder = 2
    Visible = False
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Left = 144
    Top = 512
  end
  object OpenDialog1: TOpenDialog
    Left = 8
    Top = 512
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 176
    Top = 512
  end
end
