// 对象浏览器控件.
//
unit ObjectsBrowser;

interface
uses


  Windows,Messages,Classes,SysUtils,Controls,StdCtrls,ComCtrls,ExtCtrls,Graphics,
  ImgList,Variants,ADOInt,Pub,Forms,Menus;

type
  TObjectsBrowserPanel = class(TPanel)
  private
    FComboBox: TComboBox;
    FTreeView: TTreeView;
    FConnection: _Connection;
    FImageList: TImageList;
    FMenu: TPopupMenu;
    FRefreshMenu: TMenuItem;
    FServerInfos: TList;

    procedure LoadResBitmap;
    procedure OnSelectServer(Sender: TObject);
    procedure OnTreeNodeDeletion(Sender: TObject; Node: TTreeNode);
    procedure OnTreeNodeChanged(Sender: TObject; Node: TTreeNode);
    procedure OnTreeNodeExpanding(Sender: TObject; Node: TTreeNode;
                        var AllowExpansion: Boolean);

    procedure FillDatabases(Node: TTreeNode);
    procedure FillUserTables(Node: TTreeNode; dbName: string);
    procedure FillSysTables(Node: TTreeNode; dbName: string);
    procedure FillViews(Node: TTreeNode; dbName: string);
    procedure FillUserTypes(Node: TTreeNode; dbName: string);
    procedure FillTableColumns(Node: TTreeNode; Data: TObject);
    procedure FillTableIndexes(Node: TTreeNode; Data: TObject);
    procedure FillTableContraints(Node: TTreeNode; Data: TObject);
    procedure FillDepends(Node: TTreeNode; DBName: string; ObjID: Integer);
    procedure FillTriggers(Node: TTreeNode; DBName: string; ObjID: Integer);
    procedure FillViewColumns(Node: TTreeNode; Data: TObject);
    procedure FillViewIndexes(Node: TTreeNode; Data: TObject);
    procedure FillStoredProcs(Node: TTreeNode; dbName: string);
    procedure FillFunctions(Node: TTreeNode; dbName: string);
    procedure FillSProcParams(Node: TTreeNode; dbName, dbo, spname: string);

    function ExecuteSql(const Sql: string): Integer;
    function ExecuteRst(const Sql: string): _Recordset;
    function GetColumnDescription(colname, typename: string;
        size, prec, scale: Integer; nullable: Boolean): string;

    procedure Relayout;

    procedure DoRefresh(Sender: TObject);
    procedure DoMenuPopup(Sender: TObject);
  protected
    procedure CreateWnd; override;
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure RefreshObjects;
    procedure AddServer(AServer, AUserName, APwd,
                        ADataSource, ALoginName: string;
                        AuthType: TAuthType);
  end;

implementation

{$R ObjectsBrowser.RES}

type
  TServerInfo = class
    Server: string;
    UserName: string;
    Password: string;
    AuthType: TAuthType;
    DataSource: string;
  end;

  TDBItemType = (dtUnknown, dtUserTable, dtSysTable, dtView, dtStoredProc,
                  dtFunction, dtUserType);
  TTableItemType = (ttUnknown, ttColumn, ttIndex, ttContraint, ttDepend, ttTrigger);
  TViewItemType = (vtUnknown, vtColumn, vtIndex, vtDepend, vtTrigger);
  TSPItemType = (stUnknown, stParameter, stDepend);
  TFunctionItemType = (ftUnknown, ftParameter, ftDepend);

  TDBNode = class(TObject);

  TDBItemInfo = class
  public
    FDBName: string;
    FItemType: TDBItemType;
    constructor Create(ADBName: string; AType: TDBItemType);
  end;

  TTableNode = class(TObject);

  TTableItemInfo = class
  public
    FDBName: string;
    FTableID: Integer;
    FItemType: TTableItemType;
    constructor Create(ADBName: string; ATableID: Integer; AType: TTableItemType);
  end;

  TViewNode = class(TObject);

  TViewItemInfo = class
  public
    FDBName: string;
    FViewID: Integer;
    FViewName: string;
    FOwner: string;
    FItemType: TViewItemType;
    constructor Create(ADBName: string; AViewID: Integer;
                    AViewName: string; AOwner: string; AType: TViewItemType);
  end;

  TSPNode = class(TObject);
  
  TSPItemInfo = class
  public
    FDBName: string;
    FOwner: string;
    FSPName: string;
    FSPID: Integer;
    FItemType: TSPItemType;
    constructor Create(ADBName, AOwner, ASPName: string; ASPID: Integer;
                      AItemType: TSPItemType);
  end;

  TFuncNode = class(TObject);

  TFuncItemInfo = class
  public
    FDBName: string;
    FOwner: string;
    FFuncName: string;
    FFuncID: Integer;
    FItemType: TFunctionItemType;

    constructor Create(ADBName, AOwner, AFuncName: string; AFuncID: Integer;
                      AItemType: TFunctionItemType);
  end;

{ TDBItemInfo }

constructor TDBItemInfo.Create(ADBName: string; AType: TDBItemType);
begin
  FDBName := ADBName;
  FItemType := AType;
end;

{ TTableItemInfo }

constructor TTableItemInfo.Create(ADBName: string; ATableID: Integer;
                          AType: TTableItemType);
begin
  FDBName := ADBName;
  FTableID := ATableID;
  FItemType := AType;
end;

{ TViewItemInfo }

constructor TViewItemInfo.Create(ADBName: string; AViewID: Integer;
                    AViewName: string; AOwner: string; AType: TViewItemType);
begin
  FDBName := ADBName;
  FViewID := AViewID;
  FViewName := AViewName;
  FOwner := AOwner;
  FItemType := AType;
end;

{ TSPItemInfo }

constructor TSPItemInfo.Create(ADBName, AOwner, ASPName: string; ASPID: Integer;
                  AItemType: TSPItemType);
begin
  FDBName := ADBName;
  FOwner := AOwner;
  FSPName := ASPName;
  FSPID := ASPID;
  FItemType := AItemType;
end;

{ TFuncItemInfo }

constructor TFuncItemInfo.Create(ADBName, AOwner, AFuncName: string; AFuncID: Integer;
                  AItemType: TFunctionItemType);
begin
  FDBName := ADBName;
  FOwner := AOwner;
  FFuncName := AFuncName;
  FFuncID := AFuncID;
  FItemType := AItemType;
end;

const
  ConnStr1 = 'Provider=SQLOLEDB.1;Integrated Security=SSPI;' +
             'Persist Security Info=False;Data Source=%s;Application Name=SQL查询器-对象浏览器';

  ConnStr2 = 'Provider=SQLOLEDB.1;Password=%s;' +
             'Persist Security Info=True;User ID=%s;Data Source=%s;' +
             'Application Name=SQL查询器-对象浏览器';

procedure LoadRes;
var
  b: TBitmap;
begin
  b := TBitmap.Create;
  try
    b.LoadFromResourceName(SysInit.HInstance, 'B0');
  finally
    b.Free;
  end;
end;

{ TObjectsBrowserPanel }

procedure TObjectsBrowserPanel.AddServer(AServer, AUserName, APwd,
  ADataSource, ALoginName: string; AuthType: TAuthType);
  
  function FindServer: Boolean;
  var
    I: Integer;
    info: TServerInfo;
  begin
    for I := 0 to FComboBox.Items.Count-1 do
    begin
      info := TServerInfo(FComboBox.Items.Objects[I]);
      if info = nil then Continue;
      if SameText(info.Server, AServer) and SameText(info.UserName, AUserName)
          and SameText(info.Password, APwd) and (info.AuthType = AuthType) then
      begin
        Result := True;
        Exit;
      end;
    end;
    Result := False;
  end;     
var
  info: TServerInfo;
begin
  if FindServer then Exit;

  info := TServerInfo.Create;
  info.Server := AServer;
  info.UserName := AUserName;
  info.Password := APwd;
  info.AuthType := AuthType;
  info.DataSource := ADataSource;

  FServerInfos.Add(Pointer(info));
  FComboBox.AddItem(ADataSource + '(' + ALoginName + ')', TObject(info));
end;

constructor TObjectsBrowserPanel.Create(AOwner: TComponent);
begin
  inherited;

  Self.BevelOuter := bvNone;
  FServerInfos := TList.Create;

  FComboBox := TComboBox.Create(nil);
  FTreeView := TTreeView.Create(nil);
  FMenu := TPopupMenu.Create(nil);
  FRefreshMenu := TMenuItem.Create(nil);
  FMenu.Items.Add(FRefreshMenu);
end;

procedure TObjectsBrowserPanel.CreateWnd;
begin
  inherited;

  FComboBox.Parent := Self;
  FComboBox.Style := csDropDownList;

  FTreeView.Parent := Self;
  FTreeView.HideSelection := False;
  FTreeView.ReadOnly := True;
  FTreeView.PopupMenu := FMenu;

  FRefreshMenu.Caption := '刷新(&R)';
  FRefreshMenu.ShortCut := ShortCut(116 {VK_F5}, []);
  FRefreshMenu.OnClick := DoRefresh;
  FMenu.OnPopup := DoMenuPopup;

  LoadResBitmap;
  FTreeView.Images := FImageList;

  FComboBox.OnSelect := OnSelectServer;
  FTreeView.OnDeletion := OnTreeNodeDeletion;
  FTreeView.OnChange := OnTreeNodeChanged;
  FTreeView.OnExpanding := OnTreeNodeExpanding;

  Resize;
end;

destructor TObjectsBrowserPanel.Destroy;
var
  I: Integer;
begin
  FComboBox.Free;
  FTreeView.Free;
  FRefreshMenu.Free;
  FMenu.Free;
  FImageList.Free;;

  for I := 0 to FServerInfos.Count-1 do
    TObject(FServerInfos[I]).Free;
  FServerInfos.Free;

  FConnection := nil;

  inherited;
end;

procedure TObjectsBrowserPanel.DoMenuPopup(Sender: TObject);
var
  pt: TPoint;
  Node: TTreeNode;
begin
  GetCursorPos(pt);
  pt := FTreeView.ScreenToClient(pt);
  Node := FTreeView.GetNodeAt(pt.X, pt.Y);
  if Node <> nil then
    Node.Selected := True;

  if Node = nil then
    Node := FTreeView.Selected;

  if Node = nil then Exit;

  FMenu.Items[0].Enabled := Node.Data <> nil;
end;

procedure TObjectsBrowserPanel.DoRefresh(Sender: TObject);
var
  Node, theNode: TTreeNode;
  obj: TObject;
  I: Integer;
begin
  Node := FTreeView.Selected;
  if Node = nil then Exit;

  obj := TObject(Node.Data);
  if obj = nil then Exit;

  if (obj is TDBNode) or (obj is TTableNode) or (obj is TViewNode)
        or (obj is TSPNode) or (obj is TFuncNode) then
  begin
    for I := 0 to Node.Count-1 do
    begin
      theNode := Node[I];
      theNode.DeleteChildren;
      theNode.HasChildren := True;
    end;
  end
  else if (obj is TDBItemInfo) or (obj is TTableItemInfo)
      or (obj is TViewItemInfo) or (obj is TSPItemInfo)
      or (obj is TFuncItemInfo) then
  begin
    Node.DeleteChildren;
    Node.HasChildren := True;
  end;
end;

function TObjectsBrowserPanel.ExecuteRst(const Sql: string): _Recordset;
var
  VarAffected: OleVariant;
begin
  Result := FConnection.Execute(Sql, VarAffected, 0);
end;

function TObjectsBrowserPanel.ExecuteSql(const Sql: string): Integer;
var
  VarAffected: OleVariant;
begin
  FConnection.Execute(Sql, VarAffected, adExecuteNoRecords);
  Result := VarAffected;
end;

procedure TObjectsBrowserPanel.FillDatabases(Node: TTreeNode);

  procedure AddItemNode(PNode: TTreeNode; s: string; data: TDBItemInfo);
  var
    itemNode: TTreeNode;
  begin
    itemNode := FTreeView.Items.AddChild(PNode, s);
    itemNode.HasChildren := True;
    itemNode.ImageIndex := 2;
    itemNode.SelectedIndex := 3;
    itemNode.Data := Pointer(data);
  end;

const
  sql = 'exec sp_MShasdbaccess';
var
  Rst: _Recordset;
  dbNode: TTreeNode;
  dbname: string;
begin
  Rst := ExecuteRst(sql);

  while not Rst.EOF do
  begin
    dbname := String(Rst.Fields['dbname'].Value);
    dbNode := FTreeView.Items.AddChild(Node, dbname);
    dbNode.ImageIndex := 1;
    dbNode.SelectedIndex := 1;
    dbNode.Data := Pointer(TDBNode.Create);
    
    AddItemNode(dbNode, '用户表', TDBItemInfo.Create(dbname, dtUserTable));
    AddItemNode(dbNode, '系统表', TDBItemInfo.Create(dbname, dtSysTable));
    AddItemNode(dbNode, '视图', TDBItemInfo.Create(dbname, dtView));
    AddItemNode(dbNode, '存储过程', TDBItemInfo.Create(dbname, dtStoredProc));
    AddItemNode(dbNode, '函数', TDBItemInfo.Create(dbname, dtFunction));
    AddItemNode(dbNode, '用户定义的数据类型', TDBItemInfo.Create(dbname, dtUserType));

    Rst.MoveNext;
  end;

  Node.Expand(False);
end;

procedure TObjectsBrowserPanel.FillDepends(Node: TTreeNode;
  DBName: string; ObjID: Integer);
const
  sql1 =
'select distinct owner = user_name(o.uid), o.name, o.type, d.id ' +
'from [%s].dbo.sysdepends d, [%s].dbo.sysobjects o ' +
'where d.depid = %d and d.id = o.id ';

  sql2 =
'select distinct owner = user_name(o.uid), o.name, o.type, o.id ' +
'from [%s].dbo.sysobjects o, [%s].dbo.sysdepends d ' +
'where d.id = %d and o.id = d.depid';

var
  Rst: _Recordset;
  dNode: TTreeNode;
  dbo, dname: string;
begin
  ExecuteSql(Format('use [%s]', [DBName]));

  // 引用自身的其它对象
  Rst := ExecuteRst(Format(sql1, [DBName, DBName, ObjID]));
  while not Rst.EOF do
  begin
    dname := Rst.Fields.Item['name'].Value;
    dbo := Rst.Fields.Item['owner'].Value;

    dNode := FTreeView.Items.AddChild(Node, dbo + '.' + dname);
    dNode.ImageIndex := 8;
    dNode.SelectedIndex := 8;

    Rst.MoveNext;
  end;

  // 被自身引用的其它对象
  Rst := ExecuteRst(Format(sql2, [DBName, DBName, ObjID]));
  while not Rst.EOF do
  begin
    dname := Rst.Fields.Item['name'].Value;
    dbo := Rst.Fields.Item['owner'].Value;

    dNode := FTreeView.Items.AddChild(Node, dbo + '.' + dname);
    dNode.ImageIndex := 9;
    dNode.SelectedIndex := 9;

    Rst.MoveNext;
  end;

  if Node.Count = 0 then Node.HasChildren := False;
{
select distinct owner = user_name(o.uid), o.name, o.type, d.id
from [pubs].dbo.sysdepends d, [pubs].dbo.sysobjects o
where d.depid = 213575799 and d.id = o.id

select distinct owner = user_name(o.uid), o.name, o.type, o.id
from [pubs].dbo.sysobjects o, [pubs].dbo.sysdepends d
where d.id = 213575799 and o.id = d.depid
}
end;

procedure TObjectsBrowserPanel.FillFunctions(Node: TTreeNode;
  dbName: string);
  
  procedure AddFuncItemNode(ParentNode: TTreeNode; Title: string;
                                Data: TFuncItemInfo);
  var
    Node: TTreeNode;
  begin
    Node := FTreeView.Items.AddChildObject(ParentNode, Title, Pointer(Data));
    Node.ImageIndex := 2;
    Node.SelectedIndex := 3;
    Node.HasChildren := True;
  end;

const
  sql =
'select id, owner = user_name(uid), name, type ' +
'from [%s].dbo.sysobjects ' +
'where type = N''TF'' or type = N''IF'' or type = N''FN'' order by name';
var
  Rst: _Recordset;
  dbo, fnname: string;
  fnID: Integer;
  fnNode: TTreeNode;
begin
  ExecuteSql(Format('use [%s]', [dbName]));
  Rst := ExecuteRst(Format(sql, [dbName]));
  while not Rst.EOF do
  begin
    dbo := String(Rst.Fields.Item['owner'].Value);
    fnname := String(Rst.Fields.Item['name'].Value);
    fnID := Integer(Rst.Fields.Item['id'].Value);

    fnNode := FTreeView.Items.AddChild(Node, dbo + '.' + fnname);
    fnNode.ImageIndex := 14;
    fnNode.SelectedIndex := 14;
    fnNode.Data := Pointer(TFuncNode.Create);

    AddFuncItemNode(fnNode, '参数',
        TFuncItemInfo.Create(dbName, dbo, fnname, fnID, ftParameter));
    AddFuncItemNode(fnNode, '相关性',
        TFuncItemInfo.Create(dbName, dbo, fnname, fnID, ftDepend));

    Rst.MoveNext;
  end;
  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillSProcParams(Node: TTreeNode;
  dbName, dbo, spname: string);
const
  sql =
'exec sp_sproc_columns N%s, N%s, N%s, NULL, @ODBCVer = 3';
var
  Rst: _Recordset;
  column, typename: string;
  coltype: Integer;
  parNode: TTreeNode;

  function GetParamDisplayText: string;
  var
    s: string;
  begin
    Result := column + '(' + typename + ', ';
    case coltype of
      1: s := '输入';
      2: s := '输入/输出';
      3, 5: s := '返回值';
      4: s := '输出';
      else s := '未知';
    end;
    Result := Result + s + ')';
  end;
begin
  ExecuteSql(Format('use [%s]', [dbName]));
  Rst := ExecuteRst(Format(sql,
              [QuotedStr(spname), QuotedStr(dbo), QuotedStr(dbName)]));
  while not Rst.EOF do
  begin
    column := Rst.Fields['COLUMN_NAME'].Value;
    typename := Rst.Fields['TYPE_NAME'].Value;
    coltype := Rst.Fields['COLUMN_TYPE'].Value;

    parNode := FTreeView.Items.AddChild(Node, GetParamDisplayText);
    parNode.ImageIndex := 13;
    parNode.SelectedIndex := 13;

    Rst.MoveNext;
  end;
  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillStoredProcs(Node: TTreeNode;
  dbName: string);

  procedure AddSPItemNode(ParentNode: TTreeNode; Title: string;
                                Data: TSPItemInfo);
  var
    Node: TTreeNode;
  begin
    Node := FTreeView.Items.AddChildObject(ParentNode, Title, Pointer(Data));
    Node.ImageIndex := 2;
    Node.SelectedIndex := 3;
    Node.HasChildren := True;
  end;

const
  sql =
'select id, owner = user_name(uid), name, status ' +
'from [%s].dbo.sysobjects ' +
'where type = N''P'' order by name';
var
  Rst: _Recordset;
  dbo, spname: string;
  spID: Integer;
  spNode: TTreeNode;
begin
  ExecuteSql(Format('use [%s]', [dbName]));
  Rst := ExecuteRst(Format(sql, [dbName]));
  while not Rst.EOF do
  begin
    dbo := String(Rst.Fields.Item['owner'].Value);
    spname := String(Rst.Fields.Item['name'].Value);
    spID := Integer(Rst.Fields.Item['id'].Value);

    spNode := FTreeView.Items.AddChild(Node, dbo + '.' + spname);
    spNode.ImageIndex := 12;
    spNode.SelectedIndex := 12;
    spNode.Data := Pointer(TSPNode.Create);

    AddSPItemNode(spNode, '参数',
        TSPItemInfo.Create(dbName, dbo, spname, spID, stParameter));
    AddSPItemNode(spNode, '相关性',
        TSPItemInfo.Create(dbName, dbo, spname, spID, stDepend));

    Rst.MoveNext;
  end;
  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillSysTables(Node: TTreeNode;
  dbName: string);

  procedure AddTableItemNode(ParentNode: TTreeNode; Title: string;
                                Data: TTableItemInfo);
  var
    Node: TTreeNode;
  begin
    Node := FTreeView.Items.AddChildObject(ParentNode, Title, Pointer(Data));
    Node.ImageIndex := 2;
    Node.SelectedIndex := 3;
    Node.HasChildren := True;
  end;

const
  sql = 'select id, owner = user_name(uid), name, status ' +
        'from [%s].dbo.sysobjects where type = N''S'' order by name';
var
  Rst: _Recordset;
  tabNode: TTreeNode;
  dbo, table: string;
  tabID: Integer;
begin
  ExecuteSql(Format('use [%s]', [dbName]));
  Rst := ExecuteRst(Format(sql, [dbName]));
  while not Rst.EOF do
  begin
    dbo := String(Rst.Fields.Item['owner'].Value);
    table := String(Rst.Fields.Item['name'].Value);
    tabID := Integer(Rst.Fields.Item['id'].Value);

    tabNode := FTreeView.Items.AddChild(Node, dbo + '.' + table);
    tabNode.ImageIndex := 4;
    tabNode.SelectedIndex := 4;
    tabNode.Data := Pointer(TTableNode.Create);

    AddTableItemNode(tabNode, '列', TTableItemInfo.Create(dbName, tabID, ttColumn));
    AddTableItemNode(tabNode, '索引', TTableItemInfo.Create(dbName, tabID, ttIndex));
    AddTableItemNode(tabNode, '约束', TTableItemInfo.Create(dbName, tabID, ttContraint));
    AddTableItemNode(tabNode, '相关性', TTableItemInfo.Create(dbName, tabID, ttDepend));
    AddTableItemNode(tabNode, '触发器', TTableItemInfo.Create(dbName, tabID, ttTrigger));

    Rst.MoveNext;
  end;
  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillTableColumns(Node: TTreeNode;
  Data: TObject);
const
  sql =
'select C.name, C.type, C.xtype, C.xusertype, ' +
'    C.length, C.prec, C.scale, C.isnullable, ' +
'    typename = T.name ' +
'from [%s].dbo.syscolumns C, [%s].dbo.systypes T ' +
'where C.xusertype = T.xusertype ' +
'    and id = %d order by C.colorder ';
var
  info: TTableItemInfo;
  Rst: _Recordset;
  cNode: TTreeNode;
  title, cname, typename: string;
  prec: SmallInt;
  scale: ShortInt;
  length: Integer;
  nullable: Boolean;
begin
  info := Data as TTableItemInfo;

  ExecuteSql(Format('use [%s]', [info.FDBName]));
  Rst := ExecuteRst(Format(sql, [info.FDBName, info.FDBName, info.FTableID]));
  while not Rst.EOF do
  begin
    cname := Rst.Fields.Item['name'].Value;
    typename := Rst.Fields.Item['typename'].Value;
    prec := Rst.Fields.Item['prec'].Value;
    scale := Rst.Fields.Item['scale'].Value;
    length := Rst.Fields.Item['length'].Value;
    nullable := Rst.Fields.Item['isnullable'].Value;

    title := GetColumnDescription(cname, typename, length, prec, scale, nullable);

    cNode := FTreeView.Items.AddChild(Node, title);
    cNode.ImageIndex := 5;
    cNode.SelectedIndex := 5;
    
    Rst.MoveNext;
  end;

  if Node.Count = 0 then Node.HasChildren := False;
{
USE [LerpSys]
go
select name, type, xtype, xusertype, length, prec, scale, isnullable from [LerpSys].dbo.syscolumns where id = 1509580416 order by colorder
go
select xtype, xusertype, type, name from [LerpSys].dbo.systypes order by xtype
go
select xtype, xusertype, type, name from [LerpSys].dbo.systypes where xusertype > 256 order by xtype
go
}
end;

procedure TObjectsBrowserPanel.FillTableContraints(Node: TTreeNode;
  Data: TObject);
const
  sql =
'select id, owner = user_name(uid), name, type, info, xtype ' +
'from [%s].dbo.sysobjects ' +
'where parent_obj = %d ' +
'  and (type = N''C'' or type = N''D'' or type = N''F'' or type = N''K'')';
var
  info: TTableItemInfo;
  Rst: _Recordset;
  cNode: TTreeNode;
  cname, dbo: string;
begin
  info := Data as TTableItemInfo;

  ExecuteSql(Format('use [%s]', [info.FDBName]));
  Rst := ExecuteRst(Format(sql, [info.FDBName, info.FTableID]));
  while not Rst.EOF do
  begin
    dbo := Rst.Fields.Item['owner'].Value;
    cname := Rst.Fields.Item['name'].Value;
    cNode := FTreeView.Items.AddChild(Node, dbo + '.' + cname);
    cNode.ImageIndex := 7;
    cNode.SelectedIndex := 7;
    
    Rst.MoveNext;
  end;

  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillTableIndexes(Node: TTreeNode;
  Data: TObject);
const
  sql =
'select I.name, I.status from [%s].dbo.sysindexes I ' +
'where I.id = %d and I.indid > 0 and I.indid < 255 ' +
'  and INDEXPROPERTY(I.id, I.name, N''IsStatistics'') = 0 ' +
'  and INDEXPROPERTY(I.id, I.name, N''IsHypothetical'') = 0 ' +
'  and I.name not in ' +
'    ( ' +
'    select O.name from sysobjects O ' +
'    where O.parent_obj = I.id and OBJECTPROPERTY(O.id, N''isConstraint'') = 1 ' +
'    )';

var
  info: TTableItemInfo;
  Rst: _Recordset;
  idxNode: TTreeNode;
  idxname: string;
begin
  info := Data as TTableItemInfo;

  ExecuteSql(Format('use [%s]', [info.FDBName]));
  Rst := ExecuteRst(Format(sql, [info.FDBName, info.FTableID]));
  while not Rst.EOF do
  begin
    idxname := Rst.Fields.Item['name'].Value;
    idxNode := FTreeView.Items.AddChild(Node, idxname);
    idxNode.SelectedIndex := 6;
    idxNode.ImageIndex := 6;
    
    Rst.MoveNext;
  end;

  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillTriggers(Node: TTreeNode;
  DBName: string; ObjID: Integer);
const
  sql =
'select name, owner = user_name(uid) ' +
'from [%s].dbo.sysobjects ' +
'where type = N''TR'' and parent_obj = %d';

var
  Rst: _Recordset;
  trgNode: TTreeNode;
  dbo, trigger: string;
begin
  ExecuteSql(Format('use [%s]', [DBName]));
  Rst := ExecuteRst(Format(sql, [DBName, ObjID]));
  while not Rst.EOF do
  begin
    dbo := Rst.Fields.Item['owner'].Value;
    trigger := Rst.Fields.Item['name'].Value;

    trgNode := FTreeView.Items.AddChild(Node, dbo + '.' + trigger);
    trgNode.ImageIndex := 10;
    trgNode.SelectedIndex := 10;
    
    Rst.MoveNext;
  end;

  if Node.Count = 0 then Node.HasChildren := False;
{
select name, owner = user_name(uid)
from [pubs].dbo.sysobjects
where type = N'TR' and parent_obj = 213575799
}
end;

procedure TObjectsBrowserPanel.FillUserTables(Node: TTreeNode;
  dbName: string);

  procedure AddTableItemNode(ParentNode: TTreeNode; Title: string;
                                Data: TTableItemInfo);
  var
    Node: TTreeNode;
  begin
    Node := FTreeView.Items.AddChildObject(ParentNode, Title, Pointer(Data));
    Node.ImageIndex := 2;
    Node.SelectedIndex := 3;
    Node.HasChildren := True;
  end;

const
  sql = 'select id, owner = user_name(uid), name, status ' +
        'from [%s].dbo.sysobjects where type = N''U'' order by name';
var
  Rst: _Recordset;
  tabNode: TTreeNode;
  dbo, table: string;
  tabID: Integer;
begin
  ExecuteSql(Format('use [%s]', [dbName]));
  Rst := ExecuteRst(Format(sql, [dbName]));
  while not Rst.EOF do
  begin
    dbo := String(Rst.Fields.Item['owner'].Value);
    table := String(Rst.Fields.Item['name'].Value);
    tabID := Integer(Rst.Fields.Item['id'].Value);

    tabNode := FTreeView.Items.AddChild(Node, dbo + '.' + table);
    tabNode.ImageIndex := 4;
    tabNode.SelectedIndex := 4;
    tabNode.Data := Pointer(TTableNode.Create);

    AddTableItemNode(tabNode, '列', TTableItemInfo.Create(dbName, tabID, ttColumn));
    AddTableItemNode(tabNode, '索引', TTableItemInfo.Create(dbName, tabID, ttIndex));
    AddTableItemNode(tabNode, '约束', TTableItemInfo.Create(dbName, tabID, ttContraint));
    AddTableItemNode(tabNode, '相关性', TTableItemInfo.Create(dbName, tabID, ttDepend));
    AddTableItemNode(tabNode, '触发器', TTableItemInfo.Create(dbName, tabID, ttTrigger));

    Rst.MoveNext;
  end;
  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillUserTypes(Node: TTreeNode;
  dbName: string);
const
  sql =
'select xtype, xusertype, type, name ' +
'from [%s].dbo.systypes where xusertype > 256 order by xtype';
var
  Rst: _Recordset;
  tNode: TTreeNode;
  typename: string;
begin
  ExecuteSql(Format('use [%s]', [dbName]));
  Rst := ExecuteRst(Format(sql, [dbName]));
  while not Rst.EOF do
  begin
    typename := Rst.Fields.Item['name'].Value;
    tNode := FTreeView.Items.AddChild(Node, typename);
    tNode.ImageIndex := 15;
    tNode.SelectedIndex := 15;

    Rst.MoveNext;
  end;

  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillViewColumns(Node: TTreeNode;
  Data: TObject);
const
  sql = 'select * from %s.%s.%s';

  function ADOTypeToSqlType(adotype: Word): string;
  begin
    case adotype of
      adTinyInt, adUnsignedTinyInt: Result := 'tinyint';
      adSmallInt, adUnsignedSmallInt: Result := 'smallint';
      adInteger, adUnsignedInt: Result := 'int';
      adBigInt, adUnsignedBigInt: Result := 'bigint';
      adSingle: Result := 'real';
      adDouble: Result := 'float';
      adCurrency: Result := 'money';
      adDecimal: Result := 'decimal';
      adNumeric, adVarNumeric: Result := 'numeric';
      adBoolean: Result := 'bit';
      adVariant: Result := 'sql_variant';
      adGUID: Result := 'uniqueidentifier';

      adDate, adDBDate, adDBTime,
      adDBTimeStamp, adFileTime, adDBFileTime:
          Result := 'datetime';

      adBSTR: Result := 'varchar';
      adChar: Result := 'char';
      adVarChar: Result := 'varchar';
      adLongVarChar: Result := 'text';
      adWChar: Result := 'nchar';
      adVarWChar: Result := 'nvarchar';
      adLongVarWChar: Result := 'ntext';
      adBinary: Result := 'binary';
      adVarBinary: Result := 'binary';
      adLongVarBinary: Result := 'image';
    else
      Result := 'unknown';
    end;
  end;

  function GetColumnText(f: Field): string;
  begin
    Result := GetColumnDescription(f.Name, ADOTypeToSqlType(f.Type_),
                        f.DefinedSize, f.Precision, f.NumericScale,
                        (f.Attributes and adFldIsNullable) = adFldIsNullable);
  end;

var
  info: TViewItemInfo;
  Rst: _Recordset;
  cNode: TTreeNode;
  s: string;
  I: Integer;
begin
  info := Data as TViewItemInfo;

  ExecuteSql(Format('use [%s]', [info.FDBName]));
  Rst := ExecuteRst(Format(sql, [info.FDBName, info.FOwner, info.FViewName]));
  for I := 0 to Rst.Fields.Count - 1 do
  begin
    s := GetColumnText(Rst.Fields.Item[I]);
    cNode := FTreeView.Items.AddChild(Node, s);
    cNode.ImageIndex := 5;
    cNode.SelectedIndex := 5;
  end;

  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillViewIndexes(Node: TTreeNode;
  Data: TObject);
const
  sql =
'select I.name, I.status ' +
'from [%s].dbo.sysindexes I ' +
'where I.id = %d and I.indid > 0 and I.indid < 255';

var
  info: TViewItemInfo;
  Rst: _Recordset;
  iNode: TTreeNode;
  idxname: string;
begin
  info := Data as TViewItemInfo;

  ExecuteSql(Format('use [%s]', [info.FDBName]));
  Rst := ExecuteRst(Format(sql, [info.FDBName, info.FViewID]));
  while not Rst.EOF do
  begin
    idxname := Rst.Fields.Item['name'].Value;
    iNode := FTreeView.Items.AddChild(Node, idxname);
    iNode.ImageIndex := 6;
    iNode.SelectedIndex := 6;
    
    Rst.MoveNext;
  end;
  if Node.Count = 0 then Node.HasChildren := False;
end;

procedure TObjectsBrowserPanel.FillViews(Node: TTreeNode; dbName: string);

  procedure AddViewItemNode(ParentNode: TTreeNode; Title: string;
                                Data: TViewItemInfo);
  var
    Node: TTreeNode;
  begin
    Node := FTreeView.Items.AddChildObject(ParentNode, Title, Pointer(Data));
    Node.ImageIndex := 2;
    Node.SelectedIndex := 3;
    Node.HasChildren := True;
  end;

const
  sql = 'select id, owner = user_name(uid), name, status, ' +
        'OBJECTPROPERTY(id, N''isschemabound'') ' +
        'from [%s].dbo.sysobjects where type = N''V'' order by name';
var
  Rst: _Recordset;
  tabNode: TTreeNode;
  dbo, view: string;
  viewID: Integer;
begin
  ExecuteSql(Format('use [%s]', [dbName]));
  Rst := ExecuteRst(Format(sql, [dbName]));
  while not Rst.EOF do
  begin
    dbo := String(Rst.Fields.Item['owner'].Value);
    view := String(Rst.Fields.Item['name'].Value);
    viewID := Rst.Fields.Item['id'].Value;
    
    tabNode := FTreeView.Items.AddChildObject(Node, dbo + '.' + view, nil);
    tabNode.ImageIndex := 11;
    tabNode.SelectedIndex := 11;

    AddViewItemNode(tabNode, '列',
      TViewItemInfo.Create(dbName, viewID, view, dbo, vtColumn));
    AddViewItemNode(tabNode, '索引',
      TViewItemInfo.Create(dbName, viewID, view, dbo, vtIndex));
    AddViewItemNode(tabNode, '相关性',
      TViewItemInfo.Create(dbName, viewID, view, dbo, vtDepend));
    AddViewItemNode(tabNode, '触发器',
      TViewItemInfo.Create(dbName, viewID, view, dbo, vtTrigger));

    Rst.MoveNext;
  end;
  if Node.Count = 0 then Node.HasChildren := False;
end;

function TObjectsBrowserPanel.GetColumnDescription(colname, typename: string;
    size, prec, scale: Integer; nullable: Boolean): string;
var
  s: string;
begin
  if SameText(typename, 'numeric') or
    SameText(typename, 'decimal') then
  begin
    s := Format('%s(%d, %d)', [typename, prec, scale]);
  end
  else if SameText(typename, 'varchar') or
    SameText(typename, 'nvarchar') or
    SameText(typename, 'binary') or
    SameText(typename, 'varbinary') or
    SameText(typename, 'char') or
    SameText(typename, 'nchar') then
  begin
    s := Format('%s(%d)', [typename, size]);
  end
  else
    s := typename;
  Result := colname + '(' + s;
  if nullable then
    Result := Result + ', Null)'
  else
    Result := Result + ', Not Null)';  
end;

procedure TObjectsBrowserPanel.LoadResBitmap;
var
  I: Integer;
  bmp: TBitmap;
begin
  if FImageList = nil then
    FImageList := TImageList.Create(nil);
  FImageList.Clear;
  bmp := TBitmap.Create;
  try
    for I := 0 to 20 do
    begin
      bmp.LoadFromResourceName(SysInit.HInstance, 'B' + IntToStr(I));
      FImageList.AddMasked(bmp, clFuchsia);
    end;
  finally
    bmp.Free;
  end;
end;

procedure TObjectsBrowserPanel.OnSelectServer(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    RefreshObjects;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TObjectsBrowserPanel.OnTreeNodeChanged(Sender: TObject;
  Node: TTreeNode);
begin

end;

procedure TObjectsBrowserPanel.OnTreeNodeDeletion(Sender: TObject;
  Node: TTreeNode);
begin
  if Node.Data <> nil then
  begin
    TObject(Node.Data).Free;
    Node.Data := nil;
  end;
end;

procedure TObjectsBrowserPanel.OnTreeNodeExpanding(Sender: TObject;
  Node: TTreeNode; var AllowExpansion: Boolean);
var
  obj: TObject;
  dbinfo: TDBItemInfo;
  tabinfo: TTableItemInfo;
  vwinfo: TViewItemInfo;
  spinfo: TSPItemInfo;
  funcinfo: TFuncItemInfo;
begin
  if Node.Data = nil then Exit;
  if Node.Count > 0 then Exit;

  obj := TObject(Node.Data);
  Screen.Cursor := crHourGlass;
  try

  if obj is TDBItemInfo then
  begin
    dbinfo := TDBItemInfo(obj);
    case dbinfo.FItemType of
      dtUserTable: FillUserTables(Node, dbinfo.FDBName);
      dtSysTable: FillSysTables(Node, dbinfo.FDBName);
      dtView: FillViews(Node, dbinfo.FDBName);
      dtStoredProc: FillStoredProcs(Node, dbinfo.FDBName);
      dtFunction: FillFunctions(Node, dbinfo.FDBName);
      dtUserType: FillUserTypes(Node, dbinfo.FDBName);
    end;
  end
  else if obj is TTableItemInfo then
  begin
    tabinfo := TTableItemInfo(obj);
    case tabinfo.FItemType of
      ttColumn: FillTableColumns(Node, tabinfo);
      ttIndex: FillTableIndexes(Node, tabinfo);
      ttContraint: FillTableContraints(Node, tabinfo);
      ttDepend: FillDepends(Node, tabinfo.FDBName, tabinfo.FTableID);
      ttTrigger: FillTriggers(Node, tabinfo.FDBName, tabinfo.FTableID);
    end;
  end
  else if obj is TViewItemInfo then
  begin
    vwinfo := TViewItemInfo(obj);
    case vwinfo.FItemType of
      vtColumn: FillViewColumns(Node, vwinfo);
      vtIndex: FillViewIndexes(Node, vwinfo);
      vtDepend: FillDepends(Node, vwinfo.FDBName, vwinfo.FViewID);
      vtTrigger: FillTriggers(Node, vwinfo.FDBName, vwinfo.FViewID);
    end;
  end
  else if obj is TSPItemInfo then
  begin
    spinfo := TSPItemInfo(obj);
    case spinfo.FItemType of
      stParameter: FillSProcParams(Node, spinfo.FDBName, spinfo.FOwner, spinfo.FSPName);
      stDepend: FillDepends(Node, spinfo.FDBName, spinfo.FSPID);
    end;
  end
  else if obj is TFuncItemInfo then
  begin
    funcinfo := TFuncItemInfo(obj);
    case funcinfo.FItemType of
      ftParameter: FillSProcParams(Node, funcinfo.FDBName, funcinfo.FOwner, funcinfo.FFuncName);
      ftDepend: FillDepends(Node, funcinfo.FDBName, funcinfo.FFuncID);
    end;
  end;

  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TObjectsBrowserPanel.RefreshObjects;
var
  serv_info: TServerInfo;
  RootNode: TTreeNode;
begin
  if FComboBox.ItemIndex < 0 then Exit;

  with FComboBox do
    serv_info := TServerInfo(Items.Objects[ItemIndex]);

  if FConnection = nil then
    FConnection := CoConnection.Create;

  if (FConnection.State and adStateOpen) = adStateOpen then
    FConnection.Close;

  if serv_info.AuthType = atWindows then
    FConnection.ConnectionString := Format(ConnStr1, [serv_info.Server])
  else
    FConnection.ConnectionString := Format(ConnStr2,
        [serv_info.Password, serv_info.UserName, serv_info.Server]);
  FConnection.Open('', '', '', 0);

  FTreeView.Items.Clear;
  RootNode := FTreeView.Items.Add(nil, serv_info.DataSource);
  RootNode.ImageIndex := 0;
  RootNode.SelectedIndex := 0;

  FillDatabases(RootNode);
end;

procedure TObjectsBrowserPanel.Relayout;
begin
  if (FComboBox = nil) or (FTreeView = nil) then Exit;

  if FComboBox.Parent <> nil then FComboBox.HandleNeeded;
  FComboBox.Left := 0;
  FComboBox.Top := 0;
  FComboBox.Width := Self.Width;

  FTreeView.Left := 0;
  FTreeView.Top := FComboBox.Height + 2;
  FTreeView.Width := Self.Width;
  FTreeView.Height := Self.Height - FComboBox.Height - 2;
end;

procedure TObjectsBrowserPanel.Resize;
begin
  Relayout;
  inherited;
end;

initialization
  Variants.NullStrictConvert := False;
end.
