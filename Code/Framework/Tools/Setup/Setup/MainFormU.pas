unit MainFormU;

interface

uses
  ABPubFuncU,
  ABPubMessageU,
  ABPubVarU,
  ABPubConstU,
  ABPubServiceU,
  ABPubUserU,
  ABPubPassU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdConnDatabaseU,

  BackGroupFormU,

  ShellAPI,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,filectrl,registry,jpeg, ComCtrls,
  ABPubCheckTreeViewU, Buttons, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxPCdxBarPopupMenu, cxPC, cxContainer, cxEdit,
  Types,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxLabel, Spin, cxDropDownEdit, DB, ADODB,activex,shlobj,
  dxBarBuiltInMenu;

type
  TStepInfo = record
    Title:string;
    PriButton:Boolean;
    NextButton:Boolean;
    ExitButton:Boolean;
    ExitButtonCaption:string;
  end;

  TMainForm = class(TForm)
    Panel1: TPanel;
    label_Title: TLabel;
    imgleft: TImage;
    Panel3: TPanel;
    PriButton: TButton;
    NextButton: TButton;
    ExitButton: TButton;
    Panel2: TPanel;
    label_ShortTitle: TLabel;
    Bevel1: TBevel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Label_license: TLabel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    ABCheckTreeView1: TABCheckTreeView;
    Panel5: TPanel;
    Label5: TLabel;
    SpeedButton1: TSpeedButton;
    Edit1: TcxTextEdit;
    GroupBox2: TGroupBox;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    Panel7: TPanel;
    chbrun: TCheckBox;
    cxPageControl2: TcxPageControl;
    cxTabSheet7: TcxTabSheet;
    cxTabSheet8: TcxTabSheet;
    cxTabSheet9: TcxTabSheet;
    Label2: TLabel;
    cxTabSheet10: TcxTabSheet;
    cxTabSheet11: TcxTabSheet;
    cxTabSheet12: TcxTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    ProgressBar1: TProgressBar;
    Label9: TLabel;
    SpeedButton2: TSpeedButton;
    ComboBox1: TcxComboBox;
    Label11: TLabel;
    Edit2: TcxTextEdit;
    Label12: TLabel;
    Edit3: TcxTextEdit;
    Label10: TLabel;
    ComboBox2: TcxComboBox;
    cxTabSheet13: TcxTabSheet;
    Label1: TLabel;
    Label14: TLabel;
    RadioGroup5: TRadioGroup;
    SpeedButton3: TSpeedButton;
    ADOConnection1: TADOConnection;
    cxTabSheet15: TcxTabSheet;
    Label17: TLabel;
    Label18: TLabel;
    Panel10: TPanel;
    RadioGroup4: TRadioGroup;
    ProgressBar2: TProgressBar;
    cxTabSheet14: TcxTabSheet;
    Label16: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure NextButtonClick(Sender: TObject);
    procedure PriButtonClick(Sender: TObject);
    procedure ExitButtonClick(Sender: TObject);
    procedure RadioGroup4Click(Sender: TObject);
    procedure RadioGroup5Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
    procedure ABCheckTreeView1Click(Sender: TObject);
    procedure ABCheckTreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure ABCheckTreeView1Collapsing(Sender: TObject; Node: TTreeNode;
      var AllowCollapse: Boolean);
    procedure FormDestroy(Sender: TObject);
  private
    FSetupStrings:Tstrings;
    FExit:boolean;
    StepInfo:array [1..5] of TStepInfo;
    procedure InitStepInfo;
    procedure RefreshStepInfo;
    procedure RefreshcxPageControl2(aSelectFuncText: string);
    function SelectFuncText(aSelectFuncText: string): boolean;
    function ConnDatabase(aShowOk: boolean; aDatabaseName: string): boolean;
  public
    { Public declarations }
  end;

procedure ShowSetup;

implementation

{$R *.dfm}

procedure ShowSetup;
var
  FMainForm: TMainForm;
  FBackGroundForm: TBackGroundForm;
begin
  FMainForm := TMainForm.Create(nil);
  FBackGroundForm := TBackGroundForm.Create(nil);
  try
    FBackGroundForm.Show;
    FMainForm.ShowModal;
  finally
    FBackGroundForm.Free;
    FMainForm.Free;
  end;
end;

//初始安装信息
procedure TMainForm.InitStepInfo;
var
  tempPath:array[0..255] of char;
begin
  //取得配制文件中的
  GetEnvironmentVariable('ProgramFiles',tempPath,256);

                   Caption:=         ABReadIniInStrings('Value','Title'          ,FSetupStrings,'Armous一卡通安装程序','Armous一卡通安装程序');
  label_ShortTitle.Caption:=         ABReadIniInStrings('Value','ShortTitle'     ,FSetupStrings,'Armous一卡通'        ,'Armous一卡通'        );
  Edit1.text           :=            ABReadIniInStrings('Value','SetupPath'      ,FSetupStrings,ABGetpath(string(tempPath))+'Armous');
  ABReadIniInStrings('Value','CardType'       ,FSetupStrings,'h' ,'h' );
  ABReadIniInStrings('Value','WiegandCode'    ,FSetupStrings,'26','26');
  ABReadIniInStrings('Value','4Or8System'     ,FSetupStrings,'8' ,'8' );
  ABReadIniInStrings('Value','UseUsbdrv'      ,FSetupStrings,'0' ,'0' );
  ABReadIniInStrings('Value','DatabaseConsume',FSetupStrings,'0' ,'0' );
  ABReadIniInStrings('Value','ClientSoftDog'  ,FSetupStrings,'0' ,'0' );
  ABReadIniInStrings('Value','ServerSoftDog'  ,FSetupStrings,'1' ,'1' );

  ComboBox1.Text       :=            ABReadIniInStrings('Value','ConnServer'     ,FSetupStrings,'.'             ,'.'             );
  ComboBox2.Text       :=            ABReadIniInStrings('Value','ConnDatabase'   ,FSetupStrings,'ABFramework','ABFramework');
  RadioGroup5.ItemIndex:=StrToInt(   ABReadIniInStrings('Value','ConnLoginType'  ,FSetupStrings,'0'             ,'0'             ));
  Edit2.Text           :=            ABReadIniInStrings('Value','ConnUserName'   ,FSetupStrings,'sa'            ,'sa'            );
  Edit3.Text           :=            ABReadIniInStrings('Value','ConnPassword'   ,FSetupStrings,''              ,''              );
  chbrun.Checked       :=ABStrToBool(ABReadIniInStrings('Value','RunAfterSetup'  ,FSetupStrings,'1'             ,'1'             ));

  Label_license   .Caption:=         ABReadTxt(ABSoftSetPath+'licence.txt');
  ABSetCheckTreeViewState(ABCheckTreeView1,ABReadIniInStrings('Value','FuncNames',FSetupStrings ));

  Edit1           .Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','SetupPath'      ,FSetupStrings,'True','True'));
  ComboBox1       .Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','ConnServer'     ,FSetupStrings,'True','True'));
  ComboBox2       .Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','ConnDatabase'   ,FSetupStrings,'True','True'));
  RadioGroup5     .Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','ConnLoginType'  ,FSetupStrings,'True','True'));
  Edit2           .Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','ConnUserName'   ,FSetupStrings,'True','True'));
  Edit3           .Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','ConnPassword'   ,FSetupStrings,'True','True'));
  chbrun          .Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','RunAfterSetup'  ,FSetupStrings,'True','True'));
  ABCheckTreeView1.Enabled:= ABStrToBool(ABReadIniInStrings('Enabled','FuncNames'      ,FSetupStrings,'True','True'));
  RadioGroup4     .Enabled:= ABCheckTreeView1.Enabled;

  StepInfo[1].Title              :='许可协议';
  StepInfo[1].PriButton          :=false;
  StepInfo[1].NextButton         :=True;
  StepInfo[1].ExitButton         :=True;
  StepInfo[1].ExitButtonCaption  :='退出';

  StepInfo[2].Title              :='组件与目录选择';
  StepInfo[2].PriButton          :=True;
  StepInfo[2].NextButton         :=True;
  StepInfo[2].ExitButton         :=True;
  StepInfo[2].ExitButtonCaption  :='退出';

  StepInfo[3].Title              :='数据库连接设置';
  StepInfo[3].PriButton          :=True;
  StepInfo[3].NextButton         :=True;
  StepInfo[3].ExitButton         :=True;
  StepInfo[3].ExitButtonCaption  :='退出';

  StepInfo[4].Title              :='正在安装';
  StepInfo[4].PriButton          :=false;
  StepInfo[4].NextButton         :=false;
  StepInfo[4].ExitButton         :=True;
  StepInfo[4].ExitButtonCaption  :='退出';

  StepInfo[5].Title              :='启动项设置';
  StepInfo[5].PriButton          :=false;
  StepInfo[5].NextButton         :=false;
  StepInfo[5].ExitButton         :=True;
  StepInfo[5].ExitButtonCaption  :='完成安装';

  ABCheckTreeView1.FullExpand;
end;

procedure TMainForm.RadioGroup4Click(Sender: TObject);
begin
  case RadioGroup4.ItemIndex of
    0:
    begin
      ABSetCheckTreeViewState(ABCheckTreeView1,coUnCheck);
      ABSetCheckTreeViewState(ABCheckTreeView1,'数据库,门禁系统,加密狗服务,客户端软件');
    end;
    1:
    begin
      ABSetCheckTreeViewState(ABCheckTreeView1,coUnCheck);
      ABSetCheckTreeViewState(ABCheckTreeView1,'数据库,消费系统,加密狗服务,客户端软件');
    end;
    2:
    begin
      ABSetCheckTreeViewState(ABCheckTreeView1,coUnCheck);
      ABSetCheckTreeViewState(ABCheckTreeView1,'客户端软件');
    end;
    3:
    begin
      ABSetCheckTreeViewState(ABCheckTreeView1,coCheck);
    end;
  end;
end;

procedure TMainForm.RadioGroup5Click(Sender: TObject);
begin
  if RadioGroup5.ItemIndex=0 then
  begin
    Edit2.Clear;
    Edit3.Clear;

    Edit2.Enabled:= false;
    Edit3.Enabled:= false;
  end
  else
  begin
    Edit2.Enabled:= True;
    Edit3.Enabled:= True;
  end;
end;

procedure TMainForm.RefreshStepInfo;
begin
  label_Title .Caption:= StepInfo[cxPageControl1.ActivePageIndex+1].Title            ;
  PriButton   .Enabled:= StepInfo[cxPageControl1.ActivePageIndex+1].PriButton        ;
  NextButton  .Enabled:= StepInfo[cxPageControl1.ActivePageIndex+1].NextButton       ;
  ExitButton  .Enabled:= StepInfo[cxPageControl1.ActivePageIndex+1].ExitButton       ;
  ExitButton  .Caption:= StepInfo[cxPageControl1.ActivePageIndex+1].ExitButtonCaption;
end;

function TMainForm.SelectFuncText(aSelectFuncText: string): boolean;
var
  tempNode:TTreeNode;
begin
  result:=False;
  tempNode:=ABFindTree(ABCheckTreeView1.Items,aSelectFuncText,[]);
  if Assigned(tempNode) then
    result:=ABCheckTreeView1.Checked[tempNode];
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr := ABSelectDirectory(Edit1.Text);
  if tempStr<>EmptyStr then
  begin
    Edit1.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
begin
  ABGetSqlServerList(ComboBox1.Properties.Items);
end;

procedure TMainForm.SpeedButton3Click(Sender: TObject);
begin
  if ConnDatabase(true,'master') then
  begin
    ABFillStringsBySQL('Main',' select name from master.dbo.sysdatabases  ',[],nil,ComboBox2.Properties.Items);
  end;
end;

function TMainForm.ConnDatabase(aShowOk:boolean;aDatabaseName:string):boolean;
begin
  if ComboBox1.Text=EmptyStr  then
  begin
    ComboBox1.Text:='.';
  end;

  result:= ABOpenConnection(ADOConnection1, dtSQLServer,
            ComboBox1.Text,
            aDatabaseName,
            ABIIF(RadioGroup5.ItemIndex=0,'', Edit2.Text),
            Edit3.Text,
            True,
            aShowOk
            );
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  ABAddToConnList('Main',ADOConnection1);

  FSetupStrings:=TStringList.Create;
  if ABCheckFileExists(ABSoftSetPath+'setup.ini')  then
    FSetupStrings.LoadFromFile(ABSoftSetPath+'setup.ini');
  FSetupStrings.Text:=ABUnDOPassWord(FSetupStrings.Text);

  cxPageControl1.ActivePageIndex :=0;
  cxPageControl1.HideTabs :=true;

  cxPageControl2.ActivePageIndex :=cxPageControl2.PageCount-1;
  cxPageControl2.HideTabs :=true;

  InitStepInfo;
  RefreshStepInfo;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FSetupStrings.Text:=ABDOPassWord(FSetupStrings.Text);
  FSetupStrings.SaveToFile(ABSoftSetPath+'setup.ini');
  FSetupStrings.Free;
end;

procedure TMainForm.RefreshcxPageControl2(aSelectFuncText:string);
var
  tempIndex:LongInt;
begin
  tempIndex:=cxPageControl2.PageCount-1;
  if AnsiCompareText(aSelectFuncText,'数据库')=0 then
    tempIndex:=0
  else if AnsiCompareText(aSelectFuncText,'门禁系统')=0 then
    tempIndex:=1
  else if AnsiCompareText(aSelectFuncText,'巡更系统')=0 then
    tempIndex:=2
  else if AnsiCompareText(aSelectFuncText,'访客系统')=0 then
    tempIndex:=3
  else if AnsiCompareText(aSelectFuncText,'会议签到系统')=0 then
    tempIndex:=4
  else if AnsiCompareText(aSelectFuncText,'消费系统')=0 then
    tempIndex:=5
  else if AnsiCompareText(aSelectFuncText,'加密狗服务')=0 then
    tempIndex:=6
  else if AnsiCompareText(aSelectFuncText,'客户端软件')=0 then
    tempIndex:=7;

  cxPageControl2.ActivePageIndex:=tempIndex;
end;

procedure TMainForm.ABCheckTreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  RefreshcxPageControl2(Node.Text);
end;

procedure TMainForm.ABCheckTreeView1Click(Sender: TObject);
begin
  RadioGroup4.ItemIndex :=-1;
end;

procedure TMainForm.ABCheckTreeView1Collapsing(Sender: TObject; Node: TTreeNode;
  var AllowCollapse: Boolean);
begin
  AllowCollapse:=false;
end;

procedure TMainForm.cxPageControl1Change(Sender: TObject);
begin
  RefreshStepInfo;
end;

procedure TMainForm.ExitButtonClick(Sender: TObject);
var
  myreg : TRegistry;
begin
  case cxPageControl1.ActivePageIndex of
    5:
    begin
      //写入最后页面的设置
      if (SelectFuncText('门禁系统')) or
         (SelectFuncText('消费系统')) or
         (SelectFuncText('加密狗服务')) or
         (SelectFuncText('客户端软件'))
          then
      begin
        //桌面快捷方式
        if SelectFuncText('客户端软件') then
          ABCreateShortCut(CSIDL_DESKTOP,ABGetpath(Edit1.Text)+'Client\WaukeenCli.exe','一卡通客户端');

        //开始程序快捷方式
        if SelectFuncText('门禁系统') then
          ABCreateShortCut(CSIDL_COMMON_PROGRAMS,ABGetpath(Edit1.Text)+'Access\Resert.bat','重启动门禁服务','Armous一卡通');
        if SelectFuncText('消费系统') then
          ABCreateShortCut(CSIDL_COMMON_PROGRAMS,ABGetpath(Edit1.Text)+'Consume\Resert.bat','重启动消费服务','Armous一卡通');
        if SelectFuncText('加密狗服务') then
          ABCreateShortCut(CSIDL_COMMON_PROGRAMS,ABGetpath(Edit1.Text)+'SoftDog\Resert.bat','重启动加密狗服务','Armous一卡通');
        if SelectFuncText('客户端软件') then
          ABCreateShortCut(CSIDL_COMMON_PROGRAMS,ABGetpath(Edit1.Text)+'Client\WaukeenCli.exe','一卡通客户端','Armous一卡通');

        ABCreateShortCut(CSIDL_COMMON_PROGRAMS,ABGetpath(Edit1.Text)+'UnInstall.exe','一卡通卸载','Armous一卡通');
      end;

      //写入反安装程序;
      myreg := TRegistry.Create;
      try
        myreg.RootKey := HKEY_LOCAL_MACHINE;

        myreg.OpenKey('software\Armous',true);
        myreg.WriteString('SetupPath',ABGetpath(Edit1.Text));

        myreg.OpenKey('software\microsoft\windows\currentversion\uninstall\Armous',true);
        myreg.WriteString('displayname','Armous一卡通 卸载');
        myreg.WriteString('uninstallstring',ABGetpath(Edit1.Text)+'uninstall.exe');
      finally
        myreg.CloseKey;
        myreg.Free;
      end;

      if (chbrun.Checked) then
      begin
        if (SelectFuncText('门禁系统'))
            then
        begin
          if ABCheckFileExists(ABGetpath(Edit1.Text)+'Access\CtrlSrv.exe') then
            ShellExecute(0,nil,PChar(ABGetpath(Edit1.Text)+'Access\CtrlSrv.exe'),nil,nil,sw_shownormal);
        end;
        if (SelectFuncText('消费系统'))
            then
        begin
          if ABCheckFileExists(ABGetpath(Edit1.Text)+'Consume\install.bat') then
          begin
            SetCurrentDir(ABGetpath(Edit1.Text)+'Consume\');
            ShellExecute(0,nil,PChar(ABGetpath(Edit1.Text)+'Consume\install.bat'),nil,nil,sw_shownormal);
          end;
        end;
        if  (SelectFuncText('加密狗服务'))
            then
        begin
          if ABCheckFileExists(ABGetpath(Edit1.Text)+'SoftDog\install.bat') then
          begin
            SetCurrentDir(ABGetpath(Edit1.Text)+'SoftDog\');
            ShellExecute(0,nil,PChar(ABGetpath(Edit1.Text)+'SoftDog\install.bat'),nil,nil,sw_shownormal);
          end;
        end;
        if SelectFuncText('客户端软件') then
        begin
          if ABCheckFileExists(ABGetpath(Edit1.Text)+'Client\WaukeenCli.exe') then
            ShellExecute(0,nil,PChar(ABGetpath(Edit1.Text)+'Client\WaukeenCli.exe'),nil,nil,sw_shownormal)
        end;
      end;
      Close;
    end;
    else
    begin
      if messagebox(handle,'确定要退出本程序的安装吗?','退出安装',mb_okcancel)=1 then
      begin
        FExit:=true;
        Close;
      end;
    end;
  end;
end;

procedure TMainForm.PriButtonClick(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex:=cxPageControl1.ActivePageIndex-1;
end;

procedure TMainForm.NextButtonClick(Sender: TObject);
var
  tempNode:TTreeNode;
  tempAdd:LongInt;
  tempFGDatabse:boolean;
  tempFGDatabseMdfFileName,
  tempFGDatabseLdfFileName:string;
  tempToPath,
  tempDatabsePath:string;
  tempStr:string;

  procedure DoCopyFile(aSfieldName:string;aTfieldName: string);
  begin
    Label8.Caption:='源文件:'+ABEnterWrapStr+
                            aSfieldName+
                            ABEnterWrapStr+
                            ABEnterWrapStr+
                    '目标文件:'+ABEnterWrapStr+
                              aTfieldName;

    ABCopyFile(aSfieldName,aTfieldName,ProgressBar2);
  end;
  function DoCopy(aFuncText:string;aFromPath,aToPath:string;
                  aSFileNameArray:array of string;
                  aTFileNameArray:array of string;
                  aDelToPath:boolean=true):boolean;
  var
    i:LongInt;
    tempToFilename:string;
  begin
    Result:=false;
    try
      if SelectFuncText(aFuncText) then
      begin
        if aDelToPath then
          ABDeltree(aToPath);

        if not DirectoryExists(aToPath) then
          ForceDirectories(aToPath);

        for I := Low(aSFileNameArray) to High(aSFileNameArray) do
        begin
          ProgressBar1.Position:=ProgressBar1.Position+1;

          if ABCheckFileExists(aFromPath+aSFileNameArray[i]) then
          begin
            if i<=High(aTFileNameArray) then
              tempToFilename:=aTFileNameArray[i]
            else
              tempToFilename:=aSFileNameArray[i];

            DoCopyFile(aFromPath+aSFileNameArray[i],aToPath+tempToFilename);
          end;
        end;
        Result:=True;
      end;
    finally
    end;
  end;
begin
  tempFGDatabse:=false;
  tempAdd:=1;
  if cxPageControl1.ActivePageIndex=1 then
  begin
    if not SelectFuncText('数据库') then
    begin
      //组件与目录选择中没选择数据库但选择了应用功能则转到数据库连接页面
      if (SelectFuncText('门禁系统')) or
         (SelectFuncText('巡更系统')) or
         (SelectFuncText('访客系统')) or
         (SelectFuncText('会议签到系统')) or
         (SelectFuncText('消费系统')) or
         (SelectFuncText('加密狗服务'))
          then
      begin
      end
      else
      begin
        //组件与目录选择中什么也没选择则提示，如选择了客户端软件则跳过数据库参数与连接页面
        if not SelectFuncText('客户端软件') then
        begin
          ABShow('请选择安装功能.');
          exit;
        end;
        tempAdd:=2;
      end;
    end;
  end
  //检测数据库是否正常连接
  else if cxPageControl1.ActivePageIndex=2 then
  begin
    if (SelectFuncText('数据库')) then
    begin
      if (ConnDatabase(False,'master')) then
      begin
        tempDatabsePath:=ABGetFilepath(Trim(ABGetSQLValue('Main','select FileName from sysfiles where Fileid=1',[],'')));
        if (ABGetSQLValue('Main',
            'select count(*) from master.dbo.sysdatabases where name= '+QuotedStr(ComboBox2.Text),[],0)>0) then
        begin
          if (ConnDatabase(False,ComboBox2.Text)) then
          begin
            if (ABShow('[%s]数据库已存在,覆盖后原数据库数据将全部丢失,请慎重选择?',[ComboBox2.Text],['覆盖','退出'],2)=1) then
            begin
              tempFGDatabse:=true;
              tempFGDatabseMdfFileName:=Trim(ABGetSQLValue('Main','select FileName from sysfiles where Fileid=1',[],''));
              tempFGDatabseLdfFileName:=Trim(ABGetSQLValue('Main','select FileName from sysfiles where Fileid=2',[],''));
            end
            else
            begin
              exit;
            end;
          end
          else
          begin
            exit;
          end;
        end;
      end
      else
      begin
        exit
      end;
    end
    else
    begin
      if (ConnDatabase(False,ComboBox2.Text)) then
      begin
      end
      else
      begin
        exit
      end;
    end;
  end;

  cxPageControl1.ActivePageIndex:=cxPageControl1.ActivePageIndex+tempAdd;

  if cxPageControl1.ActivePageIndex=2 then
  begin
    RadioGroup5Click(RadioGroup5);
  end
  //拷贝文件
  //安装数据库与设置
  else if cxPageControl1.ActivePageIndex=3 then
  begin
    //拷贝文件
    try
      ProgressBar1.Min:=0;
      ProgressBar1.Max:=35;
      ProgressBar1.Position:=0;

      Label18.Caption:='安装进行中...';

      if not DirectoryExists(abgetpath(edit1.text)) then
        ForceDirectories(abgetpath(edit1.text));

      ABCopyFile(ABSoftSetPath+'UnInstall_RunByEditName.exe',abgetpath(edit1.text)+'UnInstall.exe',ProgressBar2);
      if SelectFuncText('数据库') then
      begin
        if tempFGDatabse then
        begin
          ABExecSQL('Main','use master');
          //分离数据库并备份到备份目录中
          ABExecSQL('Main','EXEC sp_detach_db '+ComboBox2.Text);

          tempToPath:=abgetpath(edit1.text)+'DatabaseBack\'+ABDateTimeToStr(now,19,'','')+'\';
          if not DirectoryExists(tempToPath) then
            ForceDirectories(tempToPath);

          DoCopyFile(tempFGDatabseMdfFileName,tempToPath+ABGetFilename(tempFGDatabseMdfFileName,false));
          DoCopyFile(tempFGDatabseLdfFileName,tempToPath+ABGetFilename(tempFGDatabseLdfFileName,false));
        end;

        DoCopy('数据库',abgetpath(ABReadIniInStrings('Path','数据库',FSetupStrings,ABOtherPath+'数据库')),
                        tempDatabsePath,
                        ['ABFramework_Data.mdf','ABFramework_log.ldf'],
                        [ComboBox2.Text+'_Data.mdf',ComboBox2.Text+'_log.ldf']);
        //附加数据库
        ABExecSQL('Main',
                  ' EXEC sp_attach_db @dbname = N'''+ComboBox2.Text+''','+
                  ' @filename1 = N'''+tempDatabsePath+ComboBox2.Text+'_Data.mdf'''+','+
                  ' @filename2 = N'''+tempDatabsePath+ComboBox2.Text+'_log.ldf''');
        ABExecSQL('Main','use '+ComboBox2.Text);
      end;

      if DoCopy('门禁系统',abgetpath(ABReadIniInStrings('Path','门禁系统',FSetupStrings,ABOtherPath+'门禁系统')),
                        abgetpath(edit1.text)+'Access\',[
                                  'AccessDB',
                                  'BuildDumpCards.dll',
                                  'cardfmt.fmt',
                                  'config.ini',
                                  'CtrlSrv.exe',
                                  'h10301_99.vff',
                                  'IdentDB',
                                  'usbdrv.dll',
                                  'Resert.bat'
                                  ],[]) then
      begin
        ABWriteInI('Database','connstring',
                   ABIIF(RadioGroup5.ItemIndex=0,
                         'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text,
                         'Provider=SQLOLEDB.1;Password='+Edit3.Text+';Persist Security Info=True;User ID='+Edit2.Text+';Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text),
                   abgetpath(edit1.text)+'Access\'+'config.ini');
      end;

      if ABReadIniInStrings('Value','4Or8System'     ,FSetupStrings,'8' )='4' then
      begin
        if DoCopy('消费系统',abgetpath(ABReadIniInStrings('Path','消费系统4片',FSetupStrings,ABOtherPath+'消费系统\4片')),
                          abgetpath(edit1.text)+'Consume\',[
                                    'install.bat',
                                    'Service.exe',
                                    'SerialPort.dll',
                                    'sock2com.dll',
                                    'uninstall.bat',
                                    'config.xml',
                                    'Resert.bat'
                                    ],[]) then
        begin
          tempStr:=ABReadTxt(abgetpath(edit1.text)+'Consume\'+'config.xml');
          ABSetItemValue(
                         tempStr,
                        'DBProxy connstring=',
                         QuotedStr(
                          ABIIF(RadioGroup5.ItemIndex=0,
                               'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text,
                               'Provider=SQLOLEDB.1;Password='+Edit3.Text+';Persist Security Info=True;User ID='+Edit2.Text+';Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text)
                               ),
                        ' conncount='
                        );
          ABWriteTxt(abgetpath(edit1.text)+'Consume\'+'config.xml',tempStr);
        end;
      end
      else
      begin
        if DoCopy('消费系统',abgetpath(ABReadIniInStrings('Path','消费系统8片',FSetupStrings,ABOtherPath+'消费系统\8片')),
                          abgetpath(edit1.text)+'Consume\',[
                                    'install.bat',
                                    'POSService.exe',
                                    'uninstall.bat',
                                    'config.ini',
                                    'Resert.bat'
                                    ],[]) then
        begin
          ABWriteInI('DBProxy','connstring',
                     ABIIF(RadioGroup5.ItemIndex=0,
                           'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text,
                           'Provider=SQLOLEDB.1;Password='+Edit3.Text+';Persist Security Info=True;User ID='+Edit2.Text+';Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text),
                     abgetpath(edit1.text)+'Consume\'+'config.ini');
        end;
      end;

      if DoCopy('加密狗服务',abgetpath(ABReadIniInStrings('Path','加密狗服务',FSetupStrings,ABOtherPath+'加密狗服务')),
                          abgetpath(edit1.text)+'SoftDog\',[
                                  'install.bat',
                                  'PosSecService.exe',
                                  'uninstall.bat',
                                  'config.ini' ,
                                  'Resert.bat'
                                  ],[]) then
      begin
        ABWriteInI('DBProxy','connstring',
                   ABIIF(RadioGroup5.ItemIndex=0,
                         'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text,
                         'Provider=SQLOLEDB.1;Password='+Edit3.Text+';Persist Security Info=True;User ID='+Edit2.Text+';Initial Catalog='+ComboBox2.Text+';Data Source='+ComboBox1.Text),
                   abgetpath(edit1.text)+'SoftDog\'+'config.ini');
      end;
      if DoCopy('客户端软件',abgetpath(ABReadIniInStrings('Path','客户端软件',FSetupStrings,ABOtherPath+'客户端软件')),
                          abgetpath(edit1.text)+'Client\',[
                                  'dbxdrivers.ini',
                                  'dbxmss.dll',
                                  'WaukeenCli.exe'
                                  ],[]) then
      begin
        if DoCopy('客户端软件',abgetpath(ABReadIniInStrings('Path','客户端连接文件',FSetupStrings,ABOtherPath+'客户端软件\客户端连接文件')),
                            abgetpath(edit1.text)+'Client\ABClientP\',[
                                    'ABConnDatabase.txt'
                                    ],[],false) then
        begin
          tempStr:=ABReadTxt(abgetpath(edit1.text)+'Client\ABClientP\'+'ABConnDatabase.txt');
          ABSetItemValue(
                        tempStr,
                        '<ROWDATA><ROW ',
                        'ConnName="Main" ConnServer="'+ComboBox1.Text+'" ConnDatabase="'+ComboBox2.Text+'" ConnIsMain="TRUE" ConnIsDefault="TRUE" ConnUser="'+
                                                       Edit2.Text+'" ConnPassword="'+ABDOPassWord(Edit3.Text)+'"',
                        '/></ROWDATA>');
          ABWriteTxt(abgetpath(edit1.text)+'Client\ABClientP\'+'ABConnDatabase.txt',tempStr);
        end;
      end;
      {
      if ABReadIniInStrings('Value','UseUsbdrv',FSetupStrings,'0')='0' then
      begin
        DoCopy('客户端软件',abgetpath(ABReadIniInStrings('Path','Usbdrv无驱',FSetupStrings,ABOtherPath+'Usbdrv\无驱')),
                            abgetpath(edit1.text)+'Client\',[
                                    'usbdrv.dll'
                                    ],[],false);
      end
      else
      begin
        DoCopy('客户端软件',abgetpath(ABReadIniInStrings('Path','Usbdrv有驱',FSetupStrings,ABOtherPath+'Usbdrv\有驱')),
                            abgetpath(edit1.text)+'Client\',[
                                    'usbdrv.dll'
                                    ],[]);
        DoCopy('客户端软件',abgetpath(ABReadIniInStrings('Path','Usbdrv驱动',FSetupStrings,ABOtherPath+'usbdrv\有驱\USB 驱动')),
                            abgetpath(edit1.text)+'USBDrv\',[
                                    'billicrossUsb.inf',
                                    'billicrossUsb.sys'
                                    ],[],false);
      end;
      }

      ProgressBar1.Position:=32;

      if SelectFuncText('数据库') then
      begin
        if (SelectFuncText('门禁系统')) or
           (SelectFuncText('巡更系统')) or
           (SelectFuncText('访客系统')) or
           (SelectFuncText('会议签到系统')) or
           (SelectFuncText('消费系统')) or
           (SelectFuncText('加密狗服务'))
            then
        begin
          ABExecSQL('Main',
            ' Exec Proc_SetClientType '+
               QuotedStr(ABRandomStr(1,15000)+','+ABRandomStr(15001,30000)+','+
                         ABRandomStr(30001,45000)+','+ABRandomStr(45001,60000)+',')+','+
               QuotedStr('0'+
                          ABIIF(SelectFuncText('消费系统'),'1','0')+
                          ABIIF(SelectFuncText('门禁系统'),'1','0')+
                          '0'+
                          ABIIF(SelectFuncText('访客系统'),'1','0')+
                          ABIIF(SelectFuncText('巡更系统'),'1','0')+
                          '0'+
                          '0'+
                          '0'+
                          ABIIF(SelectFuncText('会议签到系统'),'1','0')+
                          ABReadIniInStrings('Value','ServerSoftDog',FSetupStrings,'1')
                          )+','+
               QuotedStr(ABIIF(ABReadIniInStrings('Value','4Or8System',FSetupStrings,'8')='4','V4.0','V5.1'))+','+
                         '0');
          if (SelectFuncText('门禁系统'))
              then
          begin
            ABExecSQL('Main',' update AIC_Base_Host '+
                                     ' set HO_IP_Mac1='+QuotedStr(ABPubUser.HostIP)+',HO_IP_Mac2='+QuotedStr(ABPubUser.HostIP)+
                                     ' where Ho_Type=''Access'' '
                                     );
          end;
          if (SelectFuncText('消费系统')) 
              then
          begin
            ABExecSQL('Main',' update AIC_Base_Host '+
                                     ' set HO_IP_Mac1='+QuotedStr(ABPubUser.HostIP)+',HO_IP_Mac2='+QuotedStr(ABPubUser.HostIP)+
                                     ' where Ho_Type=''Consume'' ' );
          end;
          if  (SelectFuncText('加密狗服务'))
              then
          begin
            ABExecSQL('Main',' update AIC_Base_Host '+
                                     ' set HO_IP_Mac1='+QuotedStr(ABPubUser.HostIP)+',HO_IP_Mac2='+QuotedStr(ABPubUser.HostIP)+
                                     ' where Ho_Type=''Right'' ' );
          end;
        end;
        ABExecSQL('Main',' update ABSys_Org_Parameter '+
                                 ' set Pa_Value='+ABReadIniInStrings('Value','ClientSoftDog'  ,FSetupStrings,'0')+
                                 ' where Pa_Name='+QuotedStr('CheckSoftDog') );
        ABExecSQL('Main',' update ABSys_Org_TreeItem '+
                                 ' set Ti_bit1=case  when Ti_Code in ('+QuotedStr(ABReadIniInStrings('Value','CardType',FSetupStrings,'h'))+') then 1 else 0 end '+
                                 ' where Ti_Tr_Guid =(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = ''Card Type'')' );
        ABExecSQL('Main',' update ABSys_Org_TreeItem '+
                                 ' set Ti_bit1=case  when Ti_Code in ('+QuotedStr(ABReadIniInStrings('Value','WiegandCode',FSetupStrings,'26'))+') then 1 else 0 end '+
                                 ' where Ti_Tr_Guid =(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = ''Wiegand'')' );
        ABExecSQL('Main',' update ABSys_Org_TreeItem '+
                                 ' set Ti_bit2='+ABReadIniInStrings('Value','DatabaseConsume',FSetupStrings,'0')+
                                 ' where Ti_Tr_Guid =(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = ''Card Type'') and Ti_bit1=1 ' );
      end;
      ProgressBar1.Position:=35;
      cxPageControl1.ActivePageIndex:=cxPageControl1.ActivePageIndex+1;
    except
      cxPageControl1.ActivePageIndex:=cxPageControl1.ActivePageIndex-tempAdd;
      ProgressBar1.Position:=0;

      raise;
    end;
  end;

  if FExit then
  begin
    close;
  end;
end;

end.
