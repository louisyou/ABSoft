object frmuninstall: Tfrmuninstall
  Left = 276
  Top = 149
  BorderStyle = bsDialog
  Caption = #21368#36733
  ClientHeight = 173
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Animate1: TAnimate
    Left = 30
    Top = 22
    Width = 304
    Height = 60
    CommonAVI = aviDeleteFile
    StopFrame = 24
  end
  object ProgressBar: TProgressBar
    Left = 19
    Top = 96
    Width = 329
    Height = 16
    TabOrder = 1
  end
  object btnFinish: TButton
    Left = 120
    Top = 124
    Width = 97
    Height = 25
    Caption = #21368#36733
    TabOrder = 2
    OnClick = btnFinishClick
  end
end
