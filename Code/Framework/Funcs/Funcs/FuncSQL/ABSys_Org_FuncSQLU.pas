unit ABSys_Org_FuncSQLU;

interface                 
                              
uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubMessageU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkDBPanelU,
  ABFrameworkcxGridU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  cxLookAndFeels,
  cxSplitter,
  cxDBExtLookupComboBox,
  cxDBLookupEdit,
  cxLookupEdit,
  cxButtonEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxPC,
  cxLabel,
  cxContainer,
  cxTextEdit,
  cxDBEdit,
  cxCalc,
  cxDropDownEdit,
  cxMaskEdit,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxControls,
  cxClasses,
  cxGridLevel,
  cxMemo,
  cxDBNavigator,
  cxNavigator,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxBlobEdit,
  cxCustomData,
  cxStyles,

  Forms,Dialogs,StdConvs,Types,Variants,DB,DBClient,ComCtrls,ExtCtrls,Controls,
  Classes,SysUtils,StdCtrls,Menus, dxBarBuiltInMenu,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxDBTL, cxTLData;


type
  TABSys_Org_FuncSQLForm = class(TABFuncForm)
    ABDatasource3: TABDatasource;
    ABQuery3: TABQuery;
    cxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    ABDBPanel1: TABDBPanel;
    ABDBcxGrid2: TABcxGrid;
    ABDBcxGrid2Level1: TcxGridLevel;
    ABQuery_Do: TABQuery;
    ABDatasource_Do: TABDatasource;
    ABDBNavigator1: TABDBNavigator;
    ABDBcxGrid2ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxSplitter1: TABcxSplitter;
    ABcxMemo2: TABcxMemo;
    ABcxMemo1: TABcxMemo;
    Panel3: TPanel;
    Label2: TLabel;
    ABcxButton1: TABcxButton;
    ABcxButton2: TABcxButton;
    Panel5: TPanel;
    ABcxSplitter3: TABcxSplitter;
    Panel6: TPanel;
    Panel7: TPanel;
    ABcxGrid1: TABcxGrid;
    ABDBcxGrid1DBBandedTableView1: TABcxGridDBBandedTableView;
    ABDBcxGrid1Level1: TcxGridLevel;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    Panel4: TPanel;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDatasource2: TABDatasource;
    ABQuery2: TABQuery;
    ABcxDBTreeView1: TABcxDBTreeView;
    ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn;
    ABcxSplitter2: TABcxSplitter;
    ABDBNavigator2: TABDBNavigator;
    Splitter1: TABcxSplitter;
    procedure FormCreate(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
  private
    procedure DoQuery(aParamvalues: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_FuncSQLForm: TABSys_Org_FuncSQLForm;

implementation



{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_FuncSQLForm.ClassName;
end;
                                      
exports                                
   ABRegister ;
                               

procedure TABSys_Org_FuncSQLForm.FormCreate(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex:=0;
  ABInitFormDataSet([],[],[],ABQuery1);
  ABInitFormDataSet([],[ABDBcxGrid1DBBandedTableView1],[],ABQuery2);
  ABInitFormDataSet([ABDBPanel1],[ABcxGridDBBandedTableView1],[],ABQuery3);
end;

procedure TABSys_Org_FuncSQLForm.ABcxButton2Click(Sender: TObject);
begin
  ABExecSQLs('Main',ABcxMemo1.Text,[]);
  ABShow('执行完成');
end;

procedure TABSys_Org_FuncSQLForm.ABcxButton1Click(Sender: TObject);
begin
  DoQuery('');
end;

procedure TABSys_Org_FuncSQLForm.DoQuery(aParamvalues:string);
var
  i :longint;
  tempParamValue:TStringList;
  tempSQL:string;
  tempBegDateTime,tempEndDateTime:TDateTime;
begin
  tempBegDateTime:=now();
  tempParamValue:=TStringList.Create;
  tempParamValue.Text:=aParamvalues;
  try
    tempSQL:=trim(ABcxMemo1.Text);
    ABQuery_Do.Close;
    ABQuery_Do.Sql.Text:= tempSQL;
    ABcxMemo2.Text:=tempSQL;
    for  i:= 0 to ABQuery_Do.Params.Count-1 do
    begin
      if i+1<= tempParamValue.Count then
      begin
        ABQuery_Do.Params[i].Value:=tempParamValue[i];
      end;
      ABcxMemo2.Text:=
        ABStringReplace(ABcxMemo2.Text,':'+ABQuery_Do.Params[i].Name,ABQuotedStr(ABQuery_Do.Params[i].AsString));
    end;
      
    ABQuery_Do.Open;
    cxPageControl1.ActivePageIndex:=1;
    ABDBcxGrid2ABcxGridDBBandedTableView1.ExtPopupMenu.CreateAllItems;
  finally
    tempEndDateTime:=now();
    ABcxMemo2.Text:=('--开始时间:')+ABDateTimeToStr(tempBegDateTime,23)+ABEnterWrapStr+
                    ('--结束时间:')+ABDateTimeToStr(tempEndDateTime,23)+ABEnterWrapStr+
                    ('--耗时:')+FloatToStr(ABRound(ABGetDateTimeSpan_Float(tempBegDateTime,tempEndDateTime,tuSeconds),3))+('秒')+ABEnterWrapStr+
                    ('SQL执行后返回了')+inttostr(ABQuery_Do.RecordCount)+('条记录 ')+ABEnterWrapStr+
                    ABcxMemo2.Text;
    tempParamValue.Free;
  end;
end;

Initialization
  RegisterClass(TABSys_Org_FuncSQLForm);
Finalization
  UnRegisterClass(TABSys_Org_FuncSQLForm);

end.
