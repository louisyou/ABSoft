unit ABSys_Org_FuncControlPropertyU;

interface

uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkcxGridU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,
  ABFrameworkConstU,

  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,  FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Menus, cxTL, cxMaskEdit,
  cxTLdxBarBuiltInMenu, ABFrameworkDBPanelU, cxSplitter, cxInplaceContainer,
  cxDBTL, cxTLData, cxButtons;

type
  TABSys_Org_FuncControlPropertyForm = class(TABFuncForm)
    ABDBNavigator1: TABDBNavigator;
    ABDatasource3: TABDatasource;
    ABQuery3: TABQuery;
    Panel5: TPanel;
    ABcxSplitter3: TABcxSplitter;
    Panel6: TPanel;
    Panel7: TPanel;
    ABcxGrid1: TABcxGrid;
    ABDBcxGrid1DBBandedTableView1: TABcxGridDBBandedTableView;
    ABDBcxGrid1Level1: TcxGridLevel;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    Panel4: TPanel;
    ABcxDBTreeView1: TABcxDBTreeView;
    ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn;
    ABcxSplitter2: TABcxSplitter;
    ABDBNavigator2: TABDBNavigator;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDatasource2: TABDatasource;
    ABQuery2: TABQuery;
    Splitter1: TABcxSplitter;
    ABDBPanel1: TABDBPanel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_FuncControlPropertyForm: TABSys_Org_FuncControlPropertyForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_FuncControlPropertyForm.ClassName;
end;

exports
   ABRegister ;


procedure TABSys_Org_FuncControlPropertyForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[],[],ABQuery1);
  ABInitFormDataSet([],[ABDBcxGrid1DBBandedTableView1],[],ABQuery2);
  ABInitFormDataSet([ABDBPanel1],[ABcxGridDBBandedTableView1],[],ABQuery3);
end;


Initialization
  RegisterClass(TABSys_Org_FuncControlPropertyForm);

Finalization
  UnRegisterClass(TABSys_Org_FuncControlPropertyForm);

end.
