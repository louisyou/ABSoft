object ABSys_Org_FunctionForm: TABSys_Org_FunctionForm
  Left = 249
  Top = 175
  Caption = #33756#21333#21151#33021#33756#21333
  ClientHeight = 550
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 512
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter2: TABcxSplitter
      Left = 167
      Top = 0
      Width = 8
      Height = 512
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Panel5
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 167
      Height = 512
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TABcxSplitter
        Left = 0
        Top = 202
        Width = 167
        Height = 8
        Cursor = crVSplit
        HotZoneClassName = 'TcxMediaPlayer8Style'
        AlignSplitter = salTop
        InvertDirection = True
        Control = ABcxDBTreeView1
      end
      object Panel6: TPanel
        Left = 0
        Top = 27
        Width = 167
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        Caption = #33756#21333#20998#31867
        TabOrder = 0
      end
      object Panel7: TPanel
        Left = 0
        Top = 210
        Width = 167
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        Caption = #33756#21333#21015#34920
        TabOrder = 3
      end
      object ABDBcxGrid1: TABcxGrid
        Left = 0
        Top = 234
        Width = 167
        Height = 251
        Align = alClient
        TabOrder = 4
        OnEnter = ABDBcxGrid1Enter
        LookAndFeel.NativeStyle = False
        object ABDBcxGrid1DBBandedTableView1: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABDBcxGrid1DBBandedTableView1
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource2
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.ExpandButtonsForEmptyDetails = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 26
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABDBcxGrid1DBBandedTableView1
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object ABDBcxGrid1Level1: TcxGridLevel
          GridView = ABDBcxGrid1DBBandedTableView1
        end
      end
      object ABcxDBTreeView1: TABcxDBTreeView
        Left = 0
        Top = 51
        Width = 167
        Height = 151
        Align = alTop
        Bands = <
          item
          end>
        DataController.DataSource = ABDatasource1
        DataController.ParentField = 'Ti_ParentGuid'
        DataController.KeyField = 'Ti_Guid'
        DragMode = dmAutomatic
        Navigator.Buttons.CustomButtons = <>
        OptionsBehavior.ExpandOnIncSearch = True
        OptionsBehavior.IncSearch = True
        OptionsBehavior.IncSearchItem = ABcxDBTreeView1cxDBTreeListColumn1
        OptionsData.Editing = False
        OptionsData.Deleting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Headers = False
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.DefaultParentValue = '0'
        RootValue = -1
        TabOrder = 1
        Active = True
        ExtFullExpand = False
        CanSelectParent = True
        object ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn
          DataBinding.FieldName = 'Ti_Name'
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn
          Visible = False
          DataBinding.FieldName = 'Ti_Order'
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          SortOrder = soAscending
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
      object ABDBNavigator2: TABDBNavigator
        Left = 0
        Top = 485
        Width = 167
        Height = 27
        Align = alBottom
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 5
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource2
        VisibleButtons = [nbInsert, nbCopy, nbDelete]
        ButtonRangeType = RtCustom
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #33258#23450#20041'1'
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkStandard
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbNull
        ButtonNativeStyle = False
        BtnCaptions.Strings = (
          'BtnFirstRecord='
          'BtnPreviousRecord='
          'BtnNextRecord='
          'BtnLastRecord='
          'BtnInsert='
          'btnCopy='
          'BtnDelete='
          'BtnEdit='
          'BtnPost='
          'BtnCancel='
          'btnQuery='
          'BtnReport='
          'BtnCustom1='
          'BtnCustom2='
          'BtnCustom3='
          'BtnCustom4='
          'BtnCustom5='
          'BtnCustom6='
          'BtnCustom7='
          'BtnCustom8='
          'BtnCustom9='
          'BtnCustom10='
          'BtnExit=')
      end
      object ABDBNavigator1: TABDBNavigator
        Left = 0
        Top = 0
        Width = 167
        Height = 27
        Align = alTop
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 6
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1
        VisibleButtons = [nbQuery, nbReport, nbExitSpacer, nbExit]
        ButtonRangeType = RtCustom
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #33258#23450#20041'1'
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkStandard
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbNull
        ButtonNativeStyle = False
        BtnCaptions.Strings = (
          'BtnFirstRecord='
          'BtnPreviousRecord='
          'BtnNextRecord='
          'BtnLastRecord='
          'BtnInsert='
          'btnCopy='
          'BtnDelete='
          'BtnEdit='
          'BtnPost='
          'BtnCancel='
          'btnQuery='
          'BtnReport='
          'BtnCustom1='
          'BtnCustom2='
          'BtnCustom3='
          'BtnCustom4='
          'BtnCustom5='
          'BtnCustom6='
          'BtnCustom7='
          'BtnCustom8='
          'BtnCustom9='
          'BtnCustom10='
          'BtnExit=')
      end
    end
    object ABcxPageControl2: TABcxPageControl
      Left = 175
      Top = 0
      Width = 625
      Height = 512
      Align = alClient
      ParentShowHint = False
      ShowHint = False
      TabOrder = 2
      Properties.ActivePage = cxTabSheet1
      Properties.CustomButtons.Buttons = <>
      Properties.TabSlants.Kind = skCutCorner
      Properties.TabSlants.Positions = []
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      TabSlants.Kind = skCutCorner
      TabSlants.Positions = []
      ActivePageIndex = 0
      ClientRectBottom = 511
      ClientRectLeft = 1
      ClientRectRight = 624
      ClientRectTop = 21
      object cxTabSheet1: TcxTabSheet
        Caption = #33756#21333#35774#32622
        ImageIndex = 0
        object ABDBPanel2: TABDBPanel
          Left = 0
          Top = 0
          Width = 623
          Height = 155
          Align = alTop
          BevelOuter = bvNone
          ShowCaption = False
          TabOrder = 0
          ReadOnly = False
          DataSource = ABDatasource2
          AddAnchors_akRight = True
          AddAnchors_akBottom = True
          AutoHeight = True
          AutoWidth = True
        end
        object Panel1: TPanel
          Left = 0
          Top = 441
          Width = 623
          Height = 49
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            623
            49)
          object Label1: TcxLabel
            Left = 54
            Top = 2
            AutoSize = False
            Transparent = True
            Height = 15
            Width = 238
          end
          object Label2: TcxLabel
            Left = 473
            Top = 2
            Anchors = [akTop, akRight]
            AutoSize = False
            Caption = '0'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'MS Sans Serif'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            Transparent = True
            Height = 16
            Width = 141
          end
          object cxLabel2: TcxLabel
            Left = 1
            Top = 24
            AutoSize = False
            Caption = #30446#24405
            Transparent = True
            Height = 16
            Width = 53
          end
          object ABcxComboBox2: TABcxComboBox
            Tag = 911
            Left = 53
            Top = 22
            Anchors = [akLeft, akTop, akRight]
            Properties.ClearKey = 46
            Properties.DropDownRows = 20
            Properties.DropDownSizeable = True
            Properties.OnChange = ABcxComboBox2PropertiesChange
            Style.BorderStyle = ebs3D
            TabOrder = 3
            Width = 389
          end
          object SpeedButton1: TABcxButton
            Left = 444
            Top = 22
            Width = 23
            Height = 21
            Anchors = [akTop, akRight]
            Caption = '...'
            LookAndFeel.Kind = lfFlat
            TabOrder = 4
            OnClick = SpeedButton1Click
            ShowProgressBar = False
          end
          object ABcxComboBox1: TABcxComboBox
            Tag = 91
            Left = 473
            Top = 22
            Anchors = [akTop, akRight]
            Properties.ClearKey = 46
            Properties.DropDownRows = 20
            Properties.DropDownSizeable = True
            Properties.Items.Strings = (
              '*.*'
              'AB*.bpl'
              '*.bpl'
              'AB*.exe'
              '*.exe'
              '*.dll'
              '*.dot'
              '*.doc'
              '*.xlt'
              '*.xls'
              '*.Pak')
            Properties.OnChange = ABcxComboBox1PropertiesChange
            Style.BorderStyle = ebs3D
            Style.LookAndFeel.Kind = lfFlat
            StyleDisabled.LookAndFeel.Kind = lfFlat
            StyleFocused.LookAndFeel.Kind = lfFlat
            StyleHot.LookAndFeel.Kind = lfFlat
            TabOrder = 6
            Text = '*.*'
            Width = 142
          end
          object cxLabel1: TcxLabel
            Left = 439
            Top = 2
            Anchors = [akTop, akRight]
            AutoSize = False
            Caption = #29256#26412
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'MS Sans Serif'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            Transparent = True
            Height = 19
            Width = 36
          end
          object cxLabel3: TcxLabel
            Left = 1
            Top = 2
            AutoSize = False
            Caption = #22823#23567
            ParentColor = False
            ParentFont = False
            Style.Color = clBtnFace
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'MS Sans Serif'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            Transparent = True
            Height = 19
            Width = 54
          end
        end
        object FileListBox1: TABFileListBox
          Left = 0
          Top = 155
          Width = 623
          Height = 286
          Hint = #38750#26694#26550#30340'BPL'#27169#22359','#40664#35748#22686#21152#21040#31995#32479#21253#20013','#13#10#21487#25163#21160#20462#25913#20197#20998#37197#21040#20854#23427#30340#29238#33756#21333#20013
          Align = alClient
          ImeName = #24555#20048#20116#31508
          ItemHeight = 13
          MultiSelect = True
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnChange = FileListBox1Change
          OnDblClick = FileListBox1DblClick
        end
      end
      object cxTabSheet4: TcxTabSheet
        Caption = #27169#26495#35774#32622
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 623
          Height = 490
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object ABcxGrid1: TABcxGrid
            Left = 0
            Top = 28
            Width = 129
            Height = 462
            Align = alLeft
            TabOrder = 0
            LookAndFeel.Kind = lfFlat
            LookAndFeel.NativeStyle = False
            object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ABDatasource2_1
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.MultiSelect = True
              OptionsView.CellAutoHeight = True
              OptionsView.GroupByBox = False
              OptionsView.BandHeaders = False
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object cxGridLevel1: TcxGridLevel
              GridView = ABcxGridDBBandedTableView1
            end
          end
          object ABcxSplitter2: TABcxSplitter
            Left = 129
            Top = 28
            Width = 8
            Height = 462
            HotZoneClassName = 'TcxMediaPlayer8Style'
            InvertDirection = True
            Control = ABcxGrid1
          end
          object Panel4: TPanel
            Left = 137
            Top = 28
            Width = 486
            Height = 462
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object ABDBPanel5: TABDBPanel
              Left = 0
              Top = 24
              Width = 486
              Height = 67
              Align = alTop
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 0
              ReadOnly = False
              DataSource = ABDatasource2_1
              AddAnchors_akRight = True
              AddAnchors_akBottom = True
              AutoHeight = True
              AutoWidth = True
            end
            object Panel10: TPanel
              Left = 0
              Top = 91
              Width = 486
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              Caption = #20027#34920#38754#26495#20449#24687
              TabOrder = 1
            end
            object Panel11: TPanel
              Left = 0
              Top = 0
              Width = 486
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              Caption = #20027#34920#20449#24687
              TabOrder = 2
            end
            object Panel12: TPanel
              Left = 0
              Top = 257
              Width = 486
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              Caption = #20027#34920#26126#32454#20449#24687
              TabOrder = 3
            end
            object ABDBNavigator4: TABDBNavigator
              Left = 0
              Top = 115
              Width = 486
              Height = 27
              Align = alTop
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 4
              BigGlyph = False
              ImageLayout = blGlyphLeft
              DataSource = ABDatasource2_1_1
              VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
              ButtonRangeType = RtCustom
              BtnCustom1ImageIndex = -1
              BtnCustom2ImageIndex = -1
              BtnCustom3ImageIndex = -1
              BtnCustom4ImageIndex = -1
              BtnCustom5ImageIndex = -1
              BtnCustom6ImageIndex = -1
              BtnCustom7ImageIndex = -1
              BtnCustom8ImageIndex = -1
              BtnCustom9ImageIndex = -1
              BtnCustom10ImageIndex = -1
              BtnCustom1Caption = #33258#23450#20041'1'
              BtnCustom2Caption = #33258#23450#20041'2'
              BtnCustom3Caption = #33258#23450#20041'3'
              BtnCustom4Caption = #33258#23450#20041'4'
              BtnCustom5Caption = #33258#23450#20041'5'
              BtnCustom6Caption = #33258#23450#20041'6'
              BtnCustom7Caption = #33258#23450#20041'7'
              BtnCustom8Caption = #33258#23450#20041'8'
              BtnCustom9Caption = #33258#23450#20041'9'
              BtnCustom10Caption = #33258#23450#20041'10'
              BtnCustom1Kind = cxbkStandard
              BtnCustom2Kind = cxbkStandard
              BtnCustom3Kind = cxbkStandard
              BtnCustom4Kind = cxbkStandard
              BtnCustom5Kind = cxbkStandard
              BtnCustom6Kind = cxbkStandard
              BtnCustom7Kind = cxbkStandard
              BtnCustom8Kind = cxbkStandard
              BtnCustom9Kind = cxbkStandard
              BtnCustom10Kind = cxbkStandard
              ApprovedRollbackButton = nbNull
              ApprovedCommitButton = nbNull
              ButtonNativeStyle = False
              BtnCaptions.Strings = (
                'BtnFirstRecord='
                'BtnPreviousRecord='
                'BtnNextRecord='
                'BtnLastRecord='
                'BtnInsert='
                'btnCopy='
                'BtnDelete='
                'BtnEdit='
                'BtnPost='
                'BtnCancel='
                'btnQuery='
                'BtnReport='
                'BtnCustom1='
                'BtnCustom2='
                'BtnCustom3='
                'BtnCustom4='
                'BtnCustom5='
                'BtnCustom6='
                'BtnCustom7='
                'BtnCustom8='
                'BtnCustom9='
                'BtnCustom10='
                'BtnExit=')
            end
            object ABcxGrid2: TABcxGrid
              Left = 0
              Top = 142
              Width = 486
              Height = 115
              Align = alTop
              TabOrder = 5
              LookAndFeel.Kind = lfFlat
              LookAndFeel.NativeStyle = False
              object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
                PopupMenu.AutoHotkeys = maManual
                PopupMenu.CloseFootStr = False
                PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                PopupMenu.AutoApplyBestFit = True
                PopupMenu.AutoCreateAllItem = True
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = ABDatasource2_1_1
                DataController.Filter.Options = [fcoCaseInsensitive]
                DataController.Filter.AutoDataSetFilter = True
                DataController.Filter.TranslateBetween = True
                DataController.Filter.TranslateIn = True
                DataController.Filter.TranslateLike = True
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.AlwaysShowEditor = True
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.GoToNextCellOnEnter = True
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.DataRowSizing = True
                OptionsData.Deleting = False
                OptionsData.Inserting = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.MultiSelect = True
                OptionsView.CellAutoHeight = True
                OptionsView.GroupByBox = False
                OptionsView.BandHeaders = False
                Bands = <
                  item
                  end>
                ExtPopupMenu.AutoHotkeys = maManual
                ExtPopupMenu.CloseFootStr = False
                ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                ExtPopupMenu.AutoApplyBestFit = True
                ExtPopupMenu.AutoCreateAllItem = True
              end
              object cxGridLevel2: TcxGridLevel
                GridView = ABcxGridDBBandedTableView2
              end
            end
            object ABDBNavigator5: TABDBNavigator
              Left = 0
              Top = 281
              Width = 486
              Height = 27
              Align = alTop
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 6
              BigGlyph = False
              ImageLayout = blGlyphLeft
              DataSource = ABDatasource2_1_2
              VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
              ButtonRangeType = RtCustom
              BtnCustom1ImageIndex = -1
              BtnCustom2ImageIndex = -1
              BtnCustom3ImageIndex = -1
              BtnCustom4ImageIndex = -1
              BtnCustom5ImageIndex = -1
              BtnCustom6ImageIndex = -1
              BtnCustom7ImageIndex = -1
              BtnCustom8ImageIndex = -1
              BtnCustom9ImageIndex = -1
              BtnCustom10ImageIndex = -1
              BtnCustom1Caption = #33258#23450#20041'1'
              BtnCustom2Caption = #33258#23450#20041'2'
              BtnCustom3Caption = #33258#23450#20041'3'
              BtnCustom4Caption = #33258#23450#20041'4'
              BtnCustom5Caption = #33258#23450#20041'5'
              BtnCustom6Caption = #33258#23450#20041'6'
              BtnCustom7Caption = #33258#23450#20041'7'
              BtnCustom8Caption = #33258#23450#20041'8'
              BtnCustom9Caption = #33258#23450#20041'9'
              BtnCustom10Caption = #33258#23450#20041'10'
              BtnCustom1Kind = cxbkStandard
              BtnCustom2Kind = cxbkStandard
              BtnCustom3Kind = cxbkStandard
              BtnCustom4Kind = cxbkStandard
              BtnCustom5Kind = cxbkStandard
              BtnCustom6Kind = cxbkStandard
              BtnCustom7Kind = cxbkStandard
              BtnCustom8Kind = cxbkStandard
              BtnCustom9Kind = cxbkStandard
              BtnCustom10Kind = cxbkStandard
              ApprovedRollbackButton = nbNull
              ApprovedCommitButton = nbNull
              ButtonNativeStyle = False
              BtnCaptions.Strings = (
                'BtnFirstRecord='
                'BtnPreviousRecord='
                'BtnNextRecord='
                'BtnLastRecord='
                'BtnInsert='
                'btnCopy='
                'BtnDelete='
                'BtnEdit='
                'BtnPost='
                'BtnCancel='
                'btnQuery='
                'BtnReport='
                'BtnCustom1='
                'BtnCustom2='
                'BtnCustom3='
                'BtnCustom4='
                'BtnCustom5='
                'BtnCustom6='
                'BtnCustom7='
                'BtnCustom8='
                'BtnCustom9='
                'BtnCustom10='
                'BtnExit=')
            end
            object Panel9: TPanel
              Left = 0
              Top = 308
              Width = 486
              Height = 154
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 7
              object ABcxGrid3: TABcxGrid
                Left = 0
                Top = 0
                Width = 121
                Height = 154
                Align = alLeft
                TabOrder = 0
                LookAndFeel.Kind = lfFlat
                LookAndFeel.NativeStyle = False
                object ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView3
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource2_1_2
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsData.Deleting = False
                  OptionsData.Inserting = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.MultiSelect = True
                  OptionsView.CellAutoHeight = True
                  OptionsView.GroupByBox = False
                  OptionsView.BandHeaders = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView3
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel3: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView3
                end
              end
              object ABDBPanel6: TABDBPanel
                Left = 129
                Top = 0
                Width = 357
                Height = 154
                Align = alClient
                BevelOuter = bvNone
                ShowCaption = False
                TabOrder = 1
                ReadOnly = False
                DataSource = ABDatasource2_1_2
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
              object ABcxSplitter1: TABcxSplitter
                Left = 121
                Top = 0
                Width = 8
                Height = 154
                HotZoneClassName = 'TcxMediaPlayer8Style'
                InvertDirection = True
                Control = ABcxGrid3
              end
            end
          end
          object TPanel
            Left = 0
            Top = 0
            Width = 623
            Height = 28
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object ABDBNavigator3: TABDBNavigator
              Left = 0
              Top = 0
              Width = 337
              Height = 28
              Align = alLeft
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 0
              BigGlyph = False
              ImageLayout = blGlyphLeft
              DataSource = ABDatasource2_1
              VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
              ButtonRangeType = RtCustom
              BtnCustom1ImageIndex = -1
              BtnCustom2ImageIndex = -1
              BtnCustom3ImageIndex = -1
              BtnCustom4ImageIndex = -1
              BtnCustom5ImageIndex = -1
              BtnCustom6ImageIndex = -1
              BtnCustom7ImageIndex = -1
              BtnCustom8ImageIndex = -1
              BtnCustom9ImageIndex = -1
              BtnCustom10ImageIndex = -1
              BtnCustom1Caption = #33258#23450#20041'1'
              BtnCustom2Caption = #33258#23450#20041'2'
              BtnCustom3Caption = #33258#23450#20041'3'
              BtnCustom4Caption = #33258#23450#20041'4'
              BtnCustom5Caption = #33258#23450#20041'5'
              BtnCustom6Caption = #33258#23450#20041'6'
              BtnCustom7Caption = #33258#23450#20041'7'
              BtnCustom8Caption = #33258#23450#20041'8'
              BtnCustom9Caption = #33258#23450#20041'9'
              BtnCustom10Caption = #33258#23450#20041'10'
              BtnCustom1Kind = cxbkStandard
              BtnCustom2Kind = cxbkStandard
              BtnCustom3Kind = cxbkStandard
              BtnCustom4Kind = cxbkStandard
              BtnCustom5Kind = cxbkStandard
              BtnCustom6Kind = cxbkStandard
              BtnCustom7Kind = cxbkStandard
              BtnCustom8Kind = cxbkStandard
              BtnCustom9Kind = cxbkStandard
              BtnCustom10Kind = cxbkStandard
              ApprovedRollbackButton = nbNull
              ApprovedCommitButton = nbNull
              ButtonNativeStyle = False
              BtnCaptions.Strings = (
                'BtnFirstRecord='
                'BtnPreviousRecord='
                'BtnNextRecord='
                'BtnLastRecord='
                'BtnInsert='
                'btnCopy='
                'BtnDelete='
                'BtnEdit='
                'BtnPost='
                'BtnCancel='
                'btnQuery='
                'BtnReport='
                'BtnCustom1='
                'BtnCustom2='
                'BtnCustom3='
                'BtnCustom4='
                'BtnCustom5='
                'BtnCustom6='
                'BtnCustom7='
                'BtnCustom8='
                'BtnCustom9='
                'BtnCustom10='
                'BtnExit=')
            end
            object ABDBPanel1: TABDBPanel
              Left = 337
              Top = 0
              Width = 286
              Height = 28
              Align = alClient
              BevelOuter = bvNone
              ShowCaption = False
              TabOrder = 1
              ReadOnly = False
              DataSource = ABDatasource2
              AddAnchors_akRight = True
              AddAnchors_akBottom = True
              AutoHeight = True
              AutoWidth = True
            end
          end
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 512
    Width = 800
    Height = 38
    Align = alBottom
    TabOrder = 1
    object Button5: TABcxButton
      Left = 14
      Top = 6
      Width = 146
      Height = 25
      Caption = #26356#26032#25110#27880#20876
      DropDownMenu = PopupMenu1
      Kind = cxbkDropDownButton
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      TabOrder = 0
      ShowProgressBar = False
    end
    object ABcxButton3: TABcxButton
      Left = 176
      Top = 6
      Width = 216
      Height = 25
      Caption = #21387#32553#22810#36873#30340#25991#20214#24182#19978#20256#21040#20849#29992#31243#24207#21253
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      TabOrder = 1
      OnClick = ABcxButton3Click
      ShowProgressBar = False
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * '
      'from ABSys_Org_TreeItem '
      'where Ti_Tr_Guid=dbo.Func_GetTreeGuid('#39'Function Dir'#39')'
      'order by TI_TR_Guid,TI_Group,TI_Order')
    ConnName = 'Main'
    SqlUpdateDatetime = 42090.663057164350000000
    BeforeDeleteAsk = True
    FieldDefaultValues = 'Ti_Bit1=1,Ti_Tr_Guid=select dbo.Func_GetTreeGuid('#39'Function Dir'#39')'
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 39
    Top = 114
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 123
    Top = 115
  end
  object ABQuery2: TABQuery
    BeforePost = ABQuery2BeforePost
    AfterInsert = ABQuery2AfterInsert
    ActiveStoredUsage = []
    ReadOnly = False
    AfterPost = ABQuery2AfterPost
    AfterScroll = ABQuery2AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ti_Guid'
    DetailFields = 'Fu_Ti_Guid'
    SQL.Strings = (
      'select                                   '
      'GetFieldNames('#39'ABSys_Org_Function'#39','#39'FU_Pkg'#39') '
      'from ABSys_Org_Function '
      'where Fu_Ti_Guid  =:Ti_Guid            '
      'order by FU_TI_Guid,FU_Order')
    ConnName = 'Main'
    SqlUpdateDatetime = 42218.468369108800000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Function')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Function')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 41
    Top = 255
    ParamData = <
      item
        Name = 'Ti_Guid'
        ParamType = ptInput
      end>
  end
  object ABDatasource2: TABDatasource
    DataSet = ABQuery2
    Left = 120
    Top = 253
  end
  object PopupMenu1: TPopupMenu
    Left = 368
    Top = 282
    object N1: TMenuItem
      Caption = #28165#38500#31243#24207#21253#25991#20214
      OnClick = N1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #26356#26032#24403#21069#20998#31867#33756#21333
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #26356#26032#25152#26377#20998#31867#33756#21333
      OnClick = N3Click
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N12: TMenuItem
      Caption = #26356#26032#25110#27880#20876#36873#25321#25991#20214
      OnClick = N12Click
    end
  end
  object ABDatasource2_1: TABDatasource
    DataSet = ABQuery2_1
    Left = 584
    Top = 221
  end
  object ABQuery2_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource2
    MasterFields = 'FU_Guid'
    DetailFields = 'FM_FU_Guid'
    SQL.Strings = (
      'select   *'
      'from ABSys_Org_FuncMainTemplateSetup'
      'where FM_FU_Guid  =:FU_Guid            '
      'order by FM_FU_Guid ,FM_Order')
    ConnName = 'Main'
    SqlUpdateDatetime = 42266.861394062500000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_FuncMainTemplateSetup')
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_FuncTemplateMainSetup'
        Fields = 'FM_FU_Guid;FM_Order'
      end
      item
        Name = 'PK_ABSys_Org_FuncTemplateMainSetup'
        Fields = 'FM_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ABSys_Org_FuncMainTemplateSetup')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 489
    Top = 223
    ParamData = <
      item
        Name = 'FU_GUID'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object ABQuery2_1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource2_1
    MasterFields = 'FM_Guid'
    DetailFields = 'FP_FM_Guid'
    SQL.Strings = (
      'select   *'
      'from ABSys_Org_FuncMainTemplatePanelSetup'
      'where FP_FM_Guid  =:FM_Guid            '
      'order by FP_FM_Guid ,FP_Order')
    ConnName = 'Main'
    SqlUpdateDatetime = 42266.863064537040000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_FuncMainTemplatePanelSetup')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_FuncMainTemplatePanelSetup')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 489
    Top = 287
    ParamData = <
      item
        Name = 'FM_GUID'
        DataType = ftWideString
        ParamType = ptInput
        Size = 100
        Value = Null
      end>
  end
  object ABDatasource2_1_1: TABDatasource
    DataSet = ABQuery2_1_1
    Left = 584
    Top = 285
  end
  object ABDatasource2_1_2: TABDatasource
    DataSet = ABQuery2_1_2
    Left = 584
    Top = 333
  end
  object ABQuery2_1_2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource2_1
    MasterFields = 'FM_Guid'
    DetailFields = 'FD_FM_Guid'
    SQL.Strings = (
      'select   *'
      'from ABSys_Org_FuncDetailTemplateSetup'
      'where FD_FM_Guid  =:FM_Guid            '
      'order by FD_FM_Guid ,FD_Order')
    ConnName = 'Main'
    SqlUpdateDatetime = 42266.869211990750000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_FuncDetailTemplateSetup')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_FuncDetailTemplateSetup')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 489
    Top = 335
    ParamData = <
      item
        Name = 'FM_GUID'
        DataType = ftWideString
        ParamType = ptInput
        Size = 100
        Value = Null
      end>
  end
end
