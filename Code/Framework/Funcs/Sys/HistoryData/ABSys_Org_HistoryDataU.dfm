object ABSys_Org_HistoryDataForm: TABSys_Org_HistoryDataForm
  Left = 0
  Top = 0
  Caption = #21382#21490#25968#25454#28165#38500
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ABcxPageControl1: TABcxPageControl
    Left = 0
    Top = 0
    Width = 750
    Height = 550
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    OnChange = ABcxPageControl1Change
    ActivePageIndex = 0
    ClientRectBottom = 549
    ClientRectLeft = 1
    ClientRectRight = 749
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = #35774#35745#39029#38754
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ABDBNavigator1: TABDBNavigator
        Left = 0
        Top = 0
        Width = 748
        Height = 26
        Align = alTop
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 0
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1
        VisibleButtons = [nbFirstRecord, nbPreviousRecord, nbNextRecord, nbLastRecord, nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport]
        ButtonRangeType = RtCustom
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #33258#23450#20041'1'
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkStandard
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbNull
        ButtonNativeStyle = False
        BtnCaptions.Strings = (
          'BtnFirstRecord='
          'BtnPreviousRecord='
          'BtnNextRecord='
          'BtnLastRecord='
          'BtnInsert='
          'btnCopy='
          'BtnDelete='
          'BtnEdit='
          'BtnPost='
          'BtnCancel='
          'btnQuery='
          'BtnReport='
          'BtnCustom1='
          'BtnCustom2='
          'BtnCustom3='
          'BtnCustom4='
          'BtnCustom5='
          'BtnCustom6='
          'BtnCustom7='
          'BtnCustom8='
          'BtnCustom9='
          'BtnCustom10='
          'BtnExit=')
      end
      object ABDBcxGrid1: TABcxGrid
        Left = 0
        Top = 26
        Width = 748
        Height = 502
        Align = alClient
        TabOrder = 1
        LookAndFeel.NativeStyle = False
        object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource1
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object cxGridLevel1: TcxGridLevel
          GridView = ABcxGridDBBandedTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #25805#20316#39029#38754
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 487
        Width = 748
        Height = 41
        Align = alBottom
        TabOrder = 0
        object ABcxButton1: TABcxButton
          Left = 24
          Top = 6
          Width = 100
          Height = 25
          Caption = #25191#34892#21024#38500
          LookAndFeel.Kind = lfFlat
          TabOrder = 0
          OnClick = ABcxButton1Click
          ShowProgressBar = False
        end
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 470
        Width = 748
        Height = 17
        Align = alBottom
        TabOrder = 1
        Visible = False
      end
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 748
        Height = 470
        Align = alClient
        TabOrder = 2
        ExplicitTop = -6
        object cxGrid1TableView1: TcxGridTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skSum
            end
            item
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsSelection.CellSelect = False
          OptionsSelection.MultiSelect = True
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Header = False
          OptionsView.Indicator = True
          object cxGrid1TableView1Column0: TcxGridColumn
            Options.Editing = False
            Width = 30
          end
          object cxGrid1TableView1Column1: TcxGridColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Options.Editing = False
            Width = 686
          end
          object cxGrid1TableView1Column2: TcxGridColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Visible = False
            Options.Editing = False
            Width = 46
          end
          object cxGrid1TableView1Column3: TcxGridColumn
            Visible = False
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1TableView1
        end
      end
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      ' select *  from ABSys_Org_HistoryData order by HD_Order')
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_HistoryData')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_HistoryData')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 50
    Top = 326
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 49
    Top = 278
  end
end
