unit ABSys_Org_EnterpriseU;

interface

uses
  ABPubCheckTreeViewU,
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubMessageU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkcxGridU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,
  ABFrameworkUserU,

  cxTLdxBarBuiltInMenu,
  cxLookAndFeels,
  cxButtons,
  cxLookAndFeelPainters,
  cxTreeView,
  cxTextEdit,
  cxTLData,
  cxDBTL,
  cxInplaceContainer,
  cxMaskEdit,
  cxTL,
  cxLabel,
  cxContainer,
  cxSplitter,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxdbtree,
  dxtree,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,Buttons,Menus,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TABSys_Org_EnterpriseForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    Sheet1_Panel1: TPanel;
    Sheet1_Splitter1: TABcxSplitter;
    Sheet1_Panel2: TPanel;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    ABQuery1_1_1: TABQuery;
    ABDatasource1_1_1: TABDatasource;
    Panel2: TPanel;
    Panel3: TPanel;
    ABDBcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    Splitter1: TABcxSplitter;
    Panel1: TPanel;
    Panel4: TPanel;
    ABcxDBTreeView1: TABcxDBTreeView;
    ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn;
    RzCheckTree3: TABCheckTreeview;
    Panel6: TPanel;
    Panel7: TPanel;
    ABQuery_AllKey: TABQuery;
    Panel8: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    Button3: TABcxButton;
    ABDBNavigator3: TABDBNavigator;
    ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn;
    ABcxButton1: TABcxButton;
    Panel5: TPanel;
    CheckBox1: TCheckBox;
    ABQuery_AllUser: TABQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    ABcxSplitter1: TABcxSplitter;
    Panel10: TPanel;
    Panel11: TPanel;
    RzCheckTree1: TABCheckTreeview;
    Panel12: TPanel;
    ABcxButton2: TABcxButton;
    ABcxButton3: TABcxButton;
    ABcxButton4: TABcxButton;
    Panel13: TPanel;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABDatasource_AllKey: TABDatasource;
    ABQuery4: TABQuery;
    ABDBNavigator2: TABDBNavigator;
    ABQuery_TreeAllUser: TABQuery;
    procedure FormCreate(Sender: TObject);
    procedure ABQuery1_1AfterScroll(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ABQuery1_1BeforePost(DataSet: TDataSet);
    procedure ABcxButton1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure ABcxButton3Click(Sender: TObject);
    procedure ABcxButton4Click(Sender: TObject);
    procedure ABQuery_AllKeyAfterScroll(DataSet: TDataSet);
    procedure ABQuery1_1AfterInsert(DataSet: TDataSet);
    procedure RzCheckTree3StateChang(Sender: TObject; Node: TTreeNode;
      NewState: TCheckBoxState);
    procedure RzCheckTree1StateChang(Sender: TObject; Node: TTreeNode;
      NewState: TCheckBoxState);
  private
    FBegCheckTreeStateChange:boolean;
    procedure ABAllKeyToTree;
    procedure RefreshType(aIsAllUser: boolean);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_EnterpriseForm: TABSys_Org_EnterpriseForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_EnterpriseForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Org_EnterpriseForm.ABcxButton1Click(Sender: TObject);
begin
  ABReFreshQuery(ABQuery_AllKey,[]);
  ABAllKeyToTree;
  ABQuery1_1AfterScroll(ABDatasource1_1.DataSet);
end;

procedure TABSys_Org_EnterpriseForm.ABcxButton2Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(RzCheckTree1,coCheck);
end;

procedure TABSys_Org_EnterpriseForm.ABcxButton3Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(RzCheckTree1,coUnCheck);
end;

procedure TABSys_Org_EnterpriseForm.ABcxButton4Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(RzCheckTree1,coReverse);
end;

procedure TABSys_Org_EnterpriseForm.ABQuery1_1BeforePost(
  DataSet: TDataSet);
begin
  if (AnsiCompareText(DataSet.FieldByName('Op_Code').AsString,'admin')=0) or
     (AnsiCompareText(DataSet.FieldByName('Op_Code').AsString,'sysuser')=0) or
     (AnsiCompareText(DataSet.FieldByName('Op_Name').AsString,'admin')=0) or
     (AnsiCompareText(DataSet.FieldByName('Op_Name').AsString,'sysuser')=0)
      then
  begin
    ABShow(('[admin]与[sysuser]是系统保留用户,不能增加,请修改用户后再保存.'));
    Abort;
  end;
end;

procedure TABSys_Org_EnterpriseForm.Button1Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(RzCheckTree3,coCheck);
end;

procedure TABSys_Org_EnterpriseForm.Button2Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(RzCheckTree3,coUnCheck);
end;

procedure TABSys_Org_EnterpriseForm.Button3Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(RzCheckTree3,coReverse);
end;

procedure TABSys_Org_EnterpriseForm.CheckBox1Click(Sender: TObject);
begin
  RefreshType(CheckBox1.Checked);
end;

procedure TABSys_Org_EnterpriseForm.ABAllKeyToTree;
begin
  ABDataSetsToTree
  (
   RzCheckTree3.Items,
   ABQuery_AllKey,
   'Ke_Guid',
   'Ke_Guid',
   'Ke_Name'
   );
end;

procedure TABSys_Org_EnterpriseForm.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex:=0;
  RefreshType(CheckBox1.Checked);
end;

procedure TABSys_Org_EnterpriseForm.RefreshType(aIsAllUser:boolean);
begin
  FBegCheckTreeStateChange:=false;
  case PageControl1.ActivePageIndex of
    0:
    begin
      ABInitFormDataSet([],[],[],ABQuery_AllKey);
      ABAllKeyToTree;
      ABInitFormDataSet([],[],[],ABQuery1);

      if aIsAllUser then
      begin
        Panel6.Visible:=false;
        Sheet1_Splitter1.Visible:=false;
        Panel1.Visible:=false;

        ABInitFormDataSet([],[],[],ABQuery_AllUser,True,True,False);
        ABDatasource1_1.DataSet:=ABQuery_AllUser;
        ABInitFormDataSet([],[ABcxGridDBBandedTableView2],[],TABQuery(ABDatasource1_1.DataSet),True,false);
      end
      else
      begin
        Panel6.Visible:=true;
        Sheet1_Splitter1.Visible:=true;
        Sheet1_Splitter1.Left:=ABcxDBTreeView1.Left+10;
        Panel1.Visible:=true;

        ABcxDBTreeView1.FullExpand;
        ABDatasource1_1.DataSet:=ABQuery1_1;
        ABInitFormDataSet([],[ABcxGridDBBandedTableView2],[],TABQuery(ABDatasource1_1.DataSet));
      end;
      ABInitFormDataSet([],[],[],ABQuery1_1_1);

      FBegCheckTreeStateChange:=true;
      ABQuery1_1AfterScroll(ABDatasource1_1.DataSet);
    end;
    1:
    begin
      ABInitFormDataSet([],[],[],ABQuery_TreeAllUser);
      ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],ABQuery_AllKey);
      ABInitFormDataSet([],[],[],ABQuery4);

      ABDataSetsToTree
      (
       RzCheckTree1.Items,
       ABQuery_TreeAllUser,
       'Op_Guid' ,
       'Op_Guid',
       'Op_Name'
       );

      FBegCheckTreeStateChange:=true;
      ABQuery_AllKeyAfterScroll(ABQuery_AllKey);
    end;
  end;
end;

procedure TABSys_Org_EnterpriseForm.PageControl1Change(Sender: TObject);
begin
  RefreshType(CheckBox1.Checked);
end;

procedure TABSys_Org_EnterpriseForm.ABQuery1_1AfterInsert(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('OP_TI_Guid').AsString:=ABQuery1.FieldByName('TI_Guid').AsString;
end;

procedure TABSys_Org_EnterpriseForm.ABQuery1_1AfterScroll(
  DataSet: TDataSet);
var
  i:LongInt;
begin
  FBegCheckTreeStateChange:=false;
  try
    for I := 0 to RzCheckTree3.Items.Count - 1 do
    begin
      if not ABPointerIsNull(RzCheckTree3.Items[i].Data) then
      begin
        if (ABSetDatasetRecno(ABQuery_AllKey,LongInt(RzCheckTree3.Items[i].Data))>0) and
           (ABQuery1_1_1.Active) and
           (ABQuery1_1_1.Locate('Ok_Ke_Guid',ABQuery_AllKey.FindField('Ke_Guid').AsString,[])) then
        begin
          RzCheckTree3.State[RzCheckTree3.Items[i]]:=cbchecked;
        end
        else
        begin
          RzCheckTree3.State[RzCheckTree3.Items[i]]:=cbUnchecked;
        end;
      end;
    end;
  finally
    FBegCheckTreeStateChange:=true;
  end;
end;

procedure TABSys_Org_EnterpriseForm.ABQuery_AllKeyAfterScroll(
  DataSet: TDataSet);
var
  i:LongInt;
begin
  if not FBegCheckTreeStateChange then
    exit;

  FBegCheckTreeStateChange:=false;
  try
    case PageControl1.ActivePageIndex of
      0:
      begin
      end;
      1:
      begin
        for I := 0 to RzCheckTree1.Items.Count - 1 do
        begin
          if not ABPointerIsNull(RzCheckTree1.Items[i].Data) then
          begin
            if (ABSetDatasetRecno(ABQuery_TreeAllUser,LongInt(RzCheckTree1.Items[i].Data))>0) and
               (ABQuery4.Locate('Ok_Op_Guid',ABQuery_TreeAllUser.FindField('Op_Guid').AsString,[])) then
            begin
              RzCheckTree1.State[RzCheckTree1.Items[i]]:=cbchecked;
            end
            else
            begin
              RzCheckTree1.State[RzCheckTree1.Items[i]]:=cbUnchecked;
            end;
          end;
        end;
      end;
    end;
  finally
    FBegCheckTreeStateChange:=true;
  end;
end;

procedure TABSys_Org_EnterpriseForm.RzCheckTree1StateChang(Sender: TObject;
  Node: TTreeNode; NewState: TCheckBoxState);
begin
   ABUpdateCheckTreeViewStateChange(FBegCheckTreeStateChange,ABQuery_TreeAllUser,ABQuery4, 'Op_Guid','Ok_Op_Guid',
                         Node,NewState,'');

end;

procedure TABSys_Org_EnterpriseForm.RzCheckTree3StateChang(Sender: TObject;
  Node: TTreeNode; NewState: TCheckBoxState);
begin
   ABUpdateCheckTreeViewStateChange(FBegCheckTreeStateChange,ABQuery_AllKey,ABQuery1_1_1, 'Ke_Guid','Ok_Ke_Guid',
                         Node,NewState,'');
end;

Initialization
  RegisterClass(TABSys_Org_EnterpriseForm);

Finalization
  UnRegisterClass(TABSys_Org_EnterpriseForm);

end.
