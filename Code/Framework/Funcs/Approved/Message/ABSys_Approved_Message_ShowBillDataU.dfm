object ABSys_Approved_Message_ShowBillDataForm: TABSys_Approved_Message_ShowBillDataForm
  Left = 311
  Top = 166
  Caption = #26174#31034#21333#25454#25968#25454
  ClientHeight = 550
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Sheet1_Panel1: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 550
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Sheet1_Splitter1: TABcxSplitter
      Left = 167
      Top = 0
      Width = 8
      Height = 550
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Sheet1_Grid1
    end
    object Sheet1_Grid1: TABcxGrid
      Left = 0
      Top = 0
      Width = 167
      Height = 550
      Align = alLeft
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object Sheet1_TableView1: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = Sheet1_TableView1
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource1
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = Sheet1_TableView1
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object Sheet1_Level1: TcxGridLevel
        GridView = Sheet1_TableView1
      end
    end
    object Sheet1_Panel2: TPanel
      Left = 175
      Top = 0
      Width = 625
      Height = 550
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Sheet1_Main_Panel1: TABDBPanel
        Left = 0
        Top = 0
        Width = 625
        Height = 161
        Align = alTop
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        DataSource = ABDatasource1
        AddAnchors_akRight = True
        AddAnchors_akBottom = True
        AutoHeight = True
        AutoWidth = True
      end
      object Sheet1_Detail_Grid1: TABcxGrid
        Left = 0
        Top = 161
        Width = 625
        Height = 389
        Align = alClient
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        object Sheet1_Detail_TableView1: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = Sheet1_Detail_TableView1
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource1_1
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = Sheet1_Detail_TableView1
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object Sheet1_Detail_Level1: TcxGridLevel
          GridView = Sheet1_Detail_TableView1
        end
      end
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    ConnName = 'Main'
    SqlUpdateDatetime = 42278.630179733790000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_Tree'
        Fields = 'TR_Order'
      end
      item
        Name = 'IX_ABSys_Org_Tree_1'
        Fields = 'TR_Name'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_Tree_2'
        Fields = 'TR_Code'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_Tree'
        Fields = 'TR_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 15
    Top = 190
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 91
    Top = 198
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'TR_Guid'
    DetailFields = 'Ti_TR_Guid'
    SqlUpdateDatetime = 42278.630128009260000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_TreeItem'
        Fields = 'TI_Name;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_1'
        Fields = 'TI_Order;TI_ParentGuid;TI_TR_Guid'
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_2'
        Fields = 'TI_Code;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_TreeItem'
        Fields = 'TI_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 17
    Top = 258
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 89
    Top = 266
  end
end
