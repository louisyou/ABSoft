unit ABSys_Approved_BillU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFrameworkcxGridU,
  ABFrameworkQuerySelectFieldPanelU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,FMTBcd,Provider,SqlExpr,Grids,
  DBGrids,Menus, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxMaskEdit,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid,
  cxInplaceContainer, cxDBTL, cxTLData, dxStatusBar, cxSplitter,
  cxTLdxBarBuiltInMenu, ABFrameworkDBPanelU, dxBarBuiltInMenu, cxPC;

type
  TABSys_Approved_BillForm = class(TABFuncForm)
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBNavigator1: TABDBNavigator;
    Panel2: TPanel;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBPanel1: TABDBPanel;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    ABDBPanel2: TABDBPanel;
    ABDBPanel3: TABDBPanel;
    Splitter1: TABcxSplitter;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Approved_BillForm: TABSys_Approved_BillForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Approved_BillForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Approved_BillForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([ABDBPanel1,ABDBPanel2,ABDBPanel3],[ABcxGridDBBandedTableView1],[],ABQuery1);
end;

Initialization
  RegisterClass(TABSys_Approved_BillForm);

Finalization
  UnRegisterClass(TABSys_Approved_BillForm);

end.

