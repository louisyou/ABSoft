unit ABSys_Log_CustomEventU;

interface
                                      
uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFrameworkcxGridU,
  ABFrameworkQuerySelectFieldPanelU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,FMTBcd,Provider,SqlExpr,Grids,
  DBGrids,Menus, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxMaskEdit,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid,
  cxInplaceContainer, cxDBTL, cxTLData, dxStatusBar, cxSplitter,
  cxTLdxBarBuiltInMenu;

type
  TABSys_Log_CustomEventForm = class(TABFuncForm)
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBNavigator1: TABDBNavigator;
    Panel2: TPanel;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Log_CustomEventForm: TABSys_Log_CustomEventForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Log_CustomEventForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Log_CustomEventForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],ABQuery1,false);
end;

Initialization
  RegisterClass(TABSys_Log_CustomEventForm);

Finalization
  UnRegisterClass(TABSys_Log_CustomEventForm);

end.
