unit ABSys_Log_SQLMonitorU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,
  ABPubSQLLogU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFrameworkFuncFormU,
  ABFrameworkQueryU,
  ABFrameworkControlU,

  cxButtons,
  cxCheckBox,
  cxRadioGroup,
  cxGroupBox,
  cxPC,
  cxMemo,
  cxTextEdit,
  cxEdit,
  cxContainer,
  cxControls,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxGraphics,

  Dialogs,SqlExpr,StdCtrls,ComCtrls,Controls,Classes,ExtCtrls,Menus,
  dxBarBuiltInMenu;

type
  TABSys_Log_SQLMonitorForm = class(TABFuncForm)
    Memo1: TABcxMemo;
    Panel2: TPanel;
    Button3: TABcxButton;
    Button4: TABcxButton;
    Button1: TABcxButton;
    Button2: TABcxButton;
    CheckBox1: TABcxCheckBox;
    RadioGroup1: TABcxRadioGroup;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ABSQLLog1ExecSQL(aType, aSQL: String);
    procedure FormDestroy(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Log_SQLMonitorForm: TABSys_Log_SQLMonitorForm;

implementation

uses SysUtils, Forms;


{$R *.dfm}


procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Log_SQLMonitorForm.ClassName;
end;

exports
   ABRegister ;


procedure TABSys_Log_SQLMonitorForm.Button1Click(Sender: TObject);
var
  tempFileName:string;
begin
  tempFileName:=ABSelectFile;
  if tempFileName<>EmptyStr then
    Memo1.Lines.Text:=ABReadTxt(tempFileName);
end;

procedure TABSys_Log_SQLMonitorForm.Button2Click(Sender: TObject);
var
  tempFileName:string;
begin
  tempFileName:=ABSaveFile('','SQLMonitor'+ABDateToStr(now())+'.txt');
  if tempFileName<>EmptyStr then
    Memo1.Lines.SaveToFile(tempFileName);
end;

procedure TABSys_Log_SQLMonitorForm.Button3Click(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TABSys_Log_SQLMonitorForm.Button4Click(Sender: TObject);
begin
  close;
end;

procedure TABSys_Log_SQLMonitorForm.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then
    FormStyle:=fsstayontop
  else                                                  
    FormStyle:=fsNormal;
end;

procedure TABSys_Log_SQLMonitorForm.FormCreate(Sender: TObject);
begin
  ABSQLLog.ExecSQL:=ABSQLLog1ExecSQL;
  ABcxPageControl1.ActivePageIndex:=0;
  CheckBox1.Checked := FormStyle=fsstayontop;
end;

procedure TABSys_Log_SQLMonitorForm.FormDestroy(Sender: TObject);
begin
  ABSQLLog.ExecSQL:=nil;
end;

procedure TABSys_Log_SQLMonitorForm.ABSQLLog1ExecSQL(aType, aSQL: String);
begin
  if (Trim(aSQL)<>EmptyStr) and (RadioGroup1.ItemIndex=0) then
    Memo1.Lines.Add(aSQL);
end;

Initialization
  RegisterClass(TABSys_Log_SQLMonitorForm);
Finalization
  UnRegisterClass(TABSys_Log_SQLMonitorForm);


end.

