unit ABSys_Log_CurLinkU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFrameworkcxGridU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkDBNavigatorU,
  ABFrameworkFuncFormU,
  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkFuncU,

  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TABSys_Log_CurLinkForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBNavigator1: TABDBNavigator;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Log_CurLinkForm: TABSys_Log_CurLinkForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Log_CurLinkForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Log_CurLinkForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],[],ABQuery1);
end;

Initialization
  RegisterClass(TABSys_Log_CurLinkForm);

Finalization
  UnRegisterClass(TABSys_Log_CurLinkForm);

end.
