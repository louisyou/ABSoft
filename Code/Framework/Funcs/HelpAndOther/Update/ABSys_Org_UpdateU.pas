unit ABSys_Org_UpdateU;
                                        
interface
                              
uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubIndyU,
  ABPubRegisterFileU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkControlU,
  ABFrameworkQueryU,
  ABFrameworkUserU,
  ABFrameworkFuncFormU,
  ABFrameworkFuncU,

  cxButtons,
  cxCheckListBox,
  cxProgressBar,
  cxDropDownEdit,
  cxMaskEdit,
  cxTextEdit,
  cxEdit,
  cxContainer,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxControls,
  cxGraphics,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Menus,Dialogs,
  StdCtrls,Buttons,ExtCtrls,ComCtrls,ImgList,FileCtrl,DB,IdBaseComponent,
  IdComponent,IdTCPConnection,IdTCPClient,IdFTP,IdFTPList,IdAntiFreezeBase,
  IdExplicitTLSClientServerBase,IdAllFTPListParsers;

type
  TABSys_Org_UpdateForm = class(TABFuncForm)
    Panel1: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    Button3: TABcxButton;
    Button4: TABcxButton;
    cxCheckListBox1: TcxCheckListBox;
    ABcxButton1: TABcxButton;
    cxCheckListBox2: TcxCheckListBox;
    FTPClient: TIdFTP;
    ABdxStatusBar1: TABdxStatusBar;
    ABdxStatusBar1Container2: TdxStatusBarContainerControl;
    ABcxProgressBar1: TABcxProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FTPClientWork(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Integer);
    procedure FTPClientWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Integer);
    procedure FTPClientWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure ABcxButton1Click(Sender: TObject);

  private
    AbortTransfer: Boolean;
    TransferrignData: Boolean;
    BytesToTransfer: LongWord;
    STime: TDateTime;
    AverageSpeed:Double;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_UpdateForm: TABSys_Org_UpdateForm;

implementation

{$R *.dfm}


//���ע�����
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_UpdateForm.ClassName;
end;

exports
   ABRegister ;
//���ע�����


procedure TABSys_Org_UpdateForm.ABcxButton1Click(Sender: TObject);
begin
  AbortTransfer:=true;
end;

procedure TABSys_Org_UpdateForm.Button1Click(Sender: TObject);
var
  tempFileSize:Longint;
  tempUpdateFTP,
  tempServerFu_Version:string;
  tempServerFu_FileName:string;
  tempPath:string;
  tempDataset:TDataset;
  procedure DownFiles(acxCheckListBox:TcxCheckListBox;aBeginIndex:LongInt=0);
  var
    i:Longint;
  begin
    for i := aBeginIndex to acxCheckListBox.Items.Count-1 do
    begin
      acxCheckListBox.ItemIndex:=i;
      if (acxCheckListBox.Items[i].Checked) and
         (tempDataset.Locate('Fu_Name',acxCheckListBox.Items[i].Text,[])) then
      begin
        tempServerFu_Version:=ABReadInI('Version',tempDataset.FindField('Fu_FileName').AsString,
                                         ABTempPath + 'tempFTPVersions.ini');
        if ABRegisterFile.IsDown(tempDataset.FindField('Fu_FileName').AsString,
                                 tempDataset.FindField('Fu_SubPath').AsString,
                                 tempServerFu_Version,False) then
        begin
          tempServerFu_FileName:=ABStringReplace(tempDataset.FindField('Fu_SubPath').AsString,'\','/');
          tempServerFu_FileName:=ABGetFtppath(tempPath+tempServerFu_FileName);
          tempServerFu_FileName:=ABGetFtppath(ABStringReplace(tempServerFu_FileName,'\\','\'))+
                                 tempDataset.FindField('Fu_FileName').AsString;

          tempFileSize:=FTPClient.Size(tempServerFu_FileName);
          if tempFileSize>-1 then //-1��ʾ�ļ�������
          begin
            ABDownFromFTP(    tempServerFu_FileName,tempServerFu_Version,
                              tempDataset.FindField('Fu_SubPath').AsString,
                              tempDataset.FindField('Fu_FileName').AsString,
                              FTPClient);
            BytesToTransfer := tempFileSize;
          end;
        end;
      end;
    end;
  end;
begin
  tempUpdateFTP:=ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]),['Pa_Name'],['UpdateFTP'],['Pa_Value'],'');
  if tempUpdateFTP<>EmptyStr then
  begin
    ABSetIDFtpConn(tempUpdateFTP,FTPClient);
    tempPath := FTPClient.RetrieveCurrentDir;
    tempPath:=ABGetFtppath(tempPath);
    tempDataset:= ABGetDataset('Main',
                               ' SELECT  Fu_Name,Fu_FileName,Fu_SubPath  '+
                               ' FROM ABSys_Org_Function '+
                               ' where Fu_Pkg is not null ',[],nil,true);

    try
      FTPClient.Get('ABSoft_Set/ABClientP/LocalVersions.ini',ABTempPath + 'tempFTPVersions.ini',true);
      cxCheckListBox1.ItemIndex:=0;
      DownFiles(cxCheckListBox2);
      DownFiles(cxCheckListBox1,1);
    finally
      DeleteFile(ABTempPath + 'tempFTPVersions.ini');
      tempDataset.Free;
    end;
  end;
end;

procedure TABSys_Org_UpdateForm.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TABSys_Org_UpdateForm.Button3Click(Sender: TObject);
var
  i:Longint;
begin
  for i := 1 to cxCheckListBox1.Items.Count-1 do
  begin
    if cxCheckListBox1.Items[i].Enabled then
      cxCheckListBox1.Items[i].Checked:=true;
  end;
end;

procedure TABSys_Org_UpdateForm.Button4Click(Sender: TObject);
var
  i:Longint;
begin
  for i := 1 to cxCheckListBox1.Items.Count-1 do
  begin
    if cxCheckListBox1.Items[i].Enabled then
      cxCheckListBox1.Items[i].Checked:=false;
  end;
end;                                            

procedure TABSys_Org_UpdateForm.FormCreate(Sender: TObject);
var
  tempDataset:TDataSet;
begin
  cxCheckListBox1.Items.Clear;
  cxCheckListBox2.Items.Clear;
  tempDataset:= ABGetDataset('Main',
                            ' SELECT  Fu_Public,Fu_FileName,Fu_Name  '+
                            ' FROM ABSys_Org_Function ' ,[],nil,true);
  cxCheckListBox1.Items.BeginUpdate;
  cxCheckListBox2.Items.BeginUpdate;
  try
    cxCheckListBox1.Items.Add;
    cxCheckListBox1.Items[cxCheckListBox1.Items.Count-1].Checked:=true;
    cxCheckListBox1.Items[cxCheckListBox1.Items.Count-1].Enabled:=false;
    cxCheckListBox1.Items[cxCheckListBox1.Items.Count-1].Text:=('ϵͳ����(����)');
    tempDataset.First;
    while not tempDataset.Eof do                          
    begin
      if tempDataset.FindField('Fu_Public').AsBoolean then
      begin
        cxCheckListBox2.Items.Add;
        cxCheckListBox2.Items[cxCheckListBox2.Items.Count-1].Checked:=true;
        cxCheckListBox2.Items[cxCheckListBox2.Items.Count-1].Text:=tempDataset.FindField('Fu_Name').AsString;
      end
      else if ABUser.FuncTreeDataSet.Locate('Fu_FileName',tempDataset.FindField('Fu_FileName').AsString,[]) then
      begin
        cxCheckListBox1.Items.Add;
        cxCheckListBox1.Items[cxCheckListBox1.Items.Count-1].Checked:=true;
        cxCheckListBox1.Items[cxCheckListBox1.Items.Count-1].Text:=tempDataset.FindField('Fu_Name').AsString;
      end;
      tempDataset.Next;
    end;
  finally
    cxCheckListBox1.Items.EndUpdate;
    cxCheckListBox2.Items.EndUpdate;
    tempDataset.Free;
  end;
end;

procedure TABSys_Org_UpdateForm.FTPClientWork(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Integer);
var
  S: string;
  TotalTime: TDateTime;
  H, M, Sec, MS: Word;
  DLTime: Double;            
begin
  if AbortTransfer then
  begin //�ж�����
    FTPClient.Disconnect;
    FTPClient.Abort;
  end
  else
  begin
    TotalTime := Now - STime;
    DecodeTime(TotalTime, H, M, Sec, MS);
    Sec := Sec + M * 60 + H * 3600;
    DLTime := Sec + MS / 1000;
    if DLTime > 0 then
      AverageSpeed := {(AverageSpeed + }(AWorkCount / 1024) / DLTime {) / 2};

    S := FormatFloat('0.00 KB/s', AverageSpeed);
    case AWorkMode of
      wmRead: ABdxStatusBar1.Panels[1].Text := S;
    end;
    ABcxProgressBar1.Position := AWorkCount;
    AbortTransfer := false;
  end;
  application.ProcessMessages;
end;

procedure TABSys_Org_UpdateForm.FTPClientWorkBegin(ASender: TObject;
  AWorkMode: TWorkMode; AWorkCountMax: Integer);
begin
  TransferrignData := true;
  AbortTransfer := false;
  STime := Now;
  if AWorkCountMax > 0 then
    ABcxProgressBar1.Properties.Max := AWorkCountMax
  else
    ABcxProgressBar1.Properties.Max := BytesToTransfer;
  AverageSpeed := 0;
end;

procedure TABSys_Org_UpdateForm.FTPClientWorkEnd(ASender: TObject;
  AWorkMode: TWorkMode);
begin
  BytesToTransfer := 0;
  TransferrignData := false;
  ABcxProgressBar1.Position := 0;
  AverageSpeed := 0;
end;


//���ע�����
Initialization
  RegisterClass(TABSys_Org_UpdateForm);
Finalization
  UnRegisterClass(TABSys_Org_UpdateForm);
//���ע�����


end.







