object ABSys_Org_AboutForm: TABSys_Org_AboutForm
  Left = 174
  Top = 131
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  Caption = #20851#20110
  ClientHeight = 450
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    350
    450)
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 24
    Top = 110
    Width = 300
    Height = 13
    AutoSize = False
    Caption = #24320#21457#32773
    Transparent = True
  end
  object Label3: TLabel
    Left = 24
    Top = 129
    Width = 300
    Height = 13
    AutoSize = False
    Caption = #24320#21457#26085#26399
    Transparent = True
  end
  object Label6: TLabel
    Left = 24
    Top = 313
    Width = 300
    Height = 13
    AutoSize = False
    Caption = #27880#20876#21517#31216
    Transparent = True
  end
  object Label5: TLabel
    Left = 8
    Top = 91
    Width = 193
    Height = 13
    AutoSize = False
    Caption = #29256#26435':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label7: TLabel
    Left = 8
    Top = 293
    Width = 193
    Height = 13
    AutoSize = False
    Caption = #27880#20876#29992#25143':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label4: TLabel
    Left = 233
    Top = 69
    Width = 101
    Height = 21
    AutoSize = False
    Caption = 'V3.0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -21
    Font.Name = #26999#20307'_GB2312'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    Transparent = True
  end
  object Label1: TLabel
    Left = 22
    Top = 24
    Width = 312
    Height = 47
    AutoSize = False
    Caption = 'ABSoft'
    Font.Charset = GB2312_CHARSET
    Font.Color = clBlue
    Font.Height = -29
    Font.Name = #26999#20307'_GB2312'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    Transparent = True
  end
  object SpeedButton1: TABcxButton
    Left = 253
    Top = 411
    Width = 75
    Height = 25
    Caption = #20851#38381
    Default = True
    LookAndFeel.Kind = lfFlat
    TabOrder = 1
    OnClick = SpeedButton1Click
    ShowProgressBar = False
  end
  object Memo1: TABcxMemo
    Left = 27
    Top = 148
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      '10'
      '11'
      '12'
      '13'
      '14'
      '15'
      '16')
    ParentFont = False
    Properties.ReadOnly = True
    Properties.ScrollBars = ssVertical
    Style.BorderStyle = ebsUltraFlat
    Style.LookAndFeel.Kind = lfFlat
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 0
    Height = 135
    Width = 315
  end
  object ABcxDBImage1: TABcxDBImage
    Left = 27
    Top = 334
    DataBinding.DataField = 'Pa_ExpandValue'
    DataBinding.DataSource = ABDatasource1
    Properties.FitMode = ifmProportionalStretch
    Properties.GraphicClassName = 'TJPEGImage'
    Properties.PopupMenuLayout.MenuItems = [pmiCut, pmiCopy, pmiPaste, pmiDelete, pmiLoad, pmiWebCam, pmiSave, pmiCustom]
    Properties.PopupMenuLayout.CustomMenuItemCaption = #21387#32553
    Properties.ReadOnly = True
    Properties.ZipWidth = 0
    Properties.ZipHeight = 0
    Style.Color = clWhite
    TabOrder = 2
    Height = 78
    Width = 73
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 289
    Top = 310
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      
        ' Select Pa_ExpandValue from ABSys_Org_Parameter where Pa_Name='#39'L' +
        'ogo'#39)
    SqlUpdateDatetime = 42205.449252534730000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Parameter')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Parameter')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 290
    Top = 358
  end
end
