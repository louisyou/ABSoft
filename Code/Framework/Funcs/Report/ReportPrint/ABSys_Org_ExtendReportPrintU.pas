unit ABSys_Org_ExtendReportPrintU;

interface

uses
  ABPubAutoProgressBar_ThreadU,
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFrameworkVarU,
  ABFrameworkcxGridU,
  ABFrameworkFastReportU,
  ABFrameworkControlU,
  ABFrameworkFuncU,
  ABFrameworkDictionaryQueryU,
  ABFrameworkFuncFormU,

  cxLookAndFeels,
  cxLabel,
  cxSpinEdit,
  cxMaskEdit,
  cxTextEdit,
  cxDBPivotGrid,
  cxCustomPivotGrid,
  cxGrid,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxGridLevel,
  cxDBData,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxPC,
  cxClasses,
  cxTreeView,
  cxSplitter,
  cxCheckBox,
  cxEdit,
  cxContainer,
  cxControls,
  cxButtons,
  cxLookAndFeelPainters,
  dxBar,
  dxdbtree,
  dxtree,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Menus,DB,
  DBClient,StdCtrls,ComCtrls,ExtCtrls,frxClass,frxPreview,Mask,ImgList,ToolWin,
  dxBarBuiltInMenu, cxNavigator, System.ImageList;

type
  TABSys_Org_ExtendReportPrintForm = class(TABFuncForm)
    Panel2: TPanel;
    ABcxButton2: TABcxButton;
    ABcxButton1: TABcxButton;
    Panel1: TPanel;
    Splitter6: TABcxSplitter;
    Panel811: TPanel;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Panel7: TPanel;
    frxPreview1: TfrxPreview;
    dxBarDockControl1: TdxBarDockControl;
    cxTabSheet2: TcxTabSheet;
    Rpt_ImageList: TImageList;
    PopupMenu1: TPopupMenu;
    Preview1: TMenuItem;
    Preview2: TMenuItem;
    Preview3: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    Rpt_ToPageEdit: TdxBarEdit;
    Rpt_CustScaleCbx: TdxBarCombo;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    ABcxDBPivotGrid1: TABcxDBPivotGrid;
    ABcxPageControl3: TABcxPageControl;
    cxTabSheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    cxTabSheet7: TcxTabSheet;
    cxTabSheet8: TcxTabSheet;
    cxTabSheet9: TcxTabSheet;
    cxTabSheet10: TcxTabSheet;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABDBcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABDBcxGrid3: TABcxGrid;
    ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView;
    cxGridLevel3: TcxGridLevel;
    ABDBcxGrid4: TABcxGrid;
    ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView;
    cxGridLevel4: TcxGridLevel;
    ABDBcxGrid5: TABcxGrid;
    ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView;
    cxGridLevel5: TcxGridLevel;
    ABDBcxGrid6: TABcxGrid;
    ABcxGridDBBandedTableView6: TABcxGridDBBandedTableView;
    cxGridLevel6: TcxGridLevel;
    Panel3: TPanel;
    TreeView1: TABcxTreeView;
    Panel4: TPanel;
    ABcxButton120: TABcxButton;
    PopupMenu2: TPopupMenu;
    N5: TMenuItem;
    N6: TMenuItem;
    ABcxSplitter1: TABcxSplitter;
    Panel8: TPanel;
    ABcxButton29: TABcxButton;
    ScrollBox1: TScrollBox;
    procedure FormCreate(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Rpt_MulViewBtnClick(Sender: TObject);
    procedure frxPreview1PageChanged(Sender: TfrxPreview; PageNo: Integer);
    procedure ABcxButton1Click(Sender: TObject);
    procedure dxBarButton6Click(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
    procedure dxBarButton4Click(Sender: TObject);
    procedure dxBarButton5Click(Sender: TObject);
    procedure dxBarButton3Click(Sender: TObject);
    procedure dxBarButton2Click(Sender: TObject);
    procedure dxBarButton7Click(Sender: TObject);
    procedure dxBarButton11Click(Sender: TObject);
    procedure dxBarButton10Click(Sender: TObject);
    procedure Rpt_ToPageEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Rpt_CustScaleCbxChange(Sender: TObject);
    procedure Preview3Click(Sender: TObject);
    procedure Preview1Click(Sender: TObject);
    procedure Preview2Click(Sender: TObject);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure TreeView1DblClick(Sender: TObject);
    procedure ABcxButton5Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ABcxButton29Click(Sender: TObject);
  private
    FLastChickButtonName:string;

    FCreateDatasetExtendReport: TDataSet;
    FSysUser:Boolean;
    procedure ABFillDataSet;
    procedure DoShowReport;
    procedure ChangeActivePageIndex(aCurFormReport, aNewFormReport, aGrid,
      aPivotGrid: boolean);
    procedure RefreshButtonReport;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_ExtendReportPrintForm: TABSys_Org_ExtendReportPrintForm;

implementation
uses ABFrameworkUserU;

{$R *.dfm}

procedure ABRegister(var aFormClassName: string);
  stdcall;
begin
  aFormClassName  :=TABSys_Org_ExtendReportPrintForm.ClassName;
end;

exports
  ABRegister;

procedure TABSys_Org_ExtendReportPrintForm.FormCreate(Sender: TObject);
begin
  FSysUser:=false;
  ABFillDataSet;
  ChangeActivePageIndex(Preview1.Checked,Preview2.Checked,N3.Checked,N4.Checked);

  ABcxButton29.Visible:=ABPubUser.IsAdmin;
  ABcxButton120.Visible:=ABPubUser.IsAdmin;

  ABExecBeforeReportSQL;
end;

procedure TABSys_Org_ExtendReportPrintForm.FormDestroy(Sender: TObject);
begin
  if not FSysUser then
    if Assigned(FCreateDatasetExtendReport) then
      FreeAndNil(FCreateDatasetExtendReport);
end;

procedure TABSys_Org_ExtendReportPrintForm.FormShow(Sender: TObject);
begin
  RefreshButtonReport;
end;

procedure TABSys_Org_ExtendReportPrintForm.frxPreview1PageChanged(
  Sender: TfrxPreview; PageNo: Integer);
begin
  Rpt_ToPageEdit.Text:=inttostr(PageNo);
end;

procedure TABSys_Org_ExtendReportPrintForm.N3Click(Sender: TObject);
begin
  ChangeActivePageIndex(Preview1.Checked,Preview2.Checked,N3.Checked,N4.Checked);
end;

procedure TABSys_Org_ExtendReportPrintForm.N4Click(Sender: TObject);
begin
  ChangeActivePageIndex(Preview1.Checked,Preview2.Checked,N3.Checked,N4.Checked);
end;

procedure TABSys_Org_ExtendReportPrintForm.N5Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  RefreshButtonReport;
end;

procedure TABSys_Org_ExtendReportPrintForm.N6Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  RefreshButtonReport;
end;

procedure TABSys_Org_ExtendReportPrintForm.Rpt_MulViewBtnClick(Sender: TObject);
begin
  frxPreview1.ZoomMode := zmManyPages;
  Rpt_CustScaleCbx.Text := FloattoStr(frxPreview1.Zoom*100);
end;

procedure TABSys_Org_ExtendReportPrintForm.ABFillDataSet;
var
  tempReportKeyField,tempKeyReportLinkField,
  tempKeyItem:string;

  tempDatasetTree,
  tempDatasetExtendReport:Tdataset;
begin
  if Assigned(ABUser.OperatorKeyExtendReportDataSet.FindField('Kr_Er_Name')) then
  begin
    tempReportKeyField:='Er_Name';
    tempKeyReportLinkField:='Kr_Er_Name';
  end
  else
  begin
    tempReportKeyField:='Er_Guid';
    tempKeyReportLinkField:='KR_ER_Guid';
  end;

  tempDatasetExtendReport := ABGetDataset('Main',' select Er_Ti_Guid,Er_Guid,Er_Name,Er_Order,ER_ShowType,ER_DefaultPrint '+
                                                 ' from ABSys_Org_ExtendReport ',[],nil,true);
  if (ABPubUser.IsAdminOrSysuser) then
  begin
    FCreateDatasetExtendReport := tempDatasetExtendReport;
    FSysUser:=true;
  end
  else
  begin
    FCreateDatasetExtendReport :=
      ABCreateClientDataSet(
      ['Er_Ti_Guid', 'Er_Guid', 'Er_Name', 'Er_Order','ER_ShowType','ER_DefaultPrint'],
      [],
      [ftString, ftString, ftString, ftInteger, ftString, ftString],
      [100, 100, 100, 100, 100, 100]
      );

    if TClientDataSet(FCreateDatasetExtendReport).IndexDefs.IndexOf('ER_TI_Guid_ER_Order_Index')<0 then
      TClientDataSet(FCreateDatasetExtendReport).IndexDefs.Add('ER_TI_Guid_ER_Order_Index','ER_TI_Guid;ER_Order',[]);
    TClientDataSet(FCreateDatasetExtendReport).IndexFieldNames:='ER_TI_Guid;ER_Order';

    ABUser.OperatorKeyExtendReportDataSet.First;
    while not ABUser.OperatorKeyExtendReportDataSet.Eof do
    begin
      tempKeyItem:=ABUser.OperatorKeyExtendReportDataSet.FindField(tempKeyReportLinkField).AsString;
      if tempDatasetExtendReport.Locate(tempReportKeyField,tempKeyItem, []) then
      begin
        ABSetFieldValue(
          tempDatasetExtendReport, ['Er_Ti_Guid', 'Er_Guid', 'Er_Name', 'Er_Order','ER_ShowType','ER_DefaultPrint'],
          FCreateDatasetExtendReport, ['Er_Ti_Guid', 'Er_Guid', 'Er_Name', 'Er_Order','ER_ShowType','ER_DefaultPrint']
          );
      end;

      ABUser.OperatorKeyExtendReportDataSet.Next;
    end;
  end;

  tempDatasetTree:= ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Extend ReportType']);
  try
    ABDataSetsToTree
      (
      TreeView1.Items,
      tempDatasetTree,
      'Ti_ParentGuid',
      'Ti_Guid',
      'Ti_Name',
      False,

      FCreateDatasetExtendReport,
      'Er_Ti_Guid',
      'Er_Name'  ,

      nil,
      True,
      false
      );
  finally
    absetdatasetfilter(tempDatasetTree,emptystr);
  end;
end;

procedure TABSys_Org_ExtendReportPrintForm.RefreshButtonReport;
var
  i:longint;
  tempButton:TABcxButton;
begin
  TreeView1.Visible:=True;
  ScrollBox1.Visible:=false;
  if N6.Checked then
  begin
    TreeView1.Visible:=false;
    ScrollBox1.Visible:=True;

    for I := 0 to TreeView1.Items.Count - 1 do
    begin
      if not TreeView1.Items[i].HasChildren then
      begin
        tempButton:=TABcxButton(FindComponent('CreateReportButton'+inttostr(I)));
        if not Assigned(tempButton) then
        begin
          tempButton:=TABcxButton.Create(self);
          tempButton.Name:='CreateReportButton'+inttostr(I);
          tempButton.Caption:=TreeView1.Items[i].text;
          tempButton.Align:=alTop;

          tempButton.OnClick:=ABcxButton5Click;
          tempButton.Parent:=Panel4;
        end;
      end;
    end;
  end;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton10Click(Sender: TObject);
begin
  frxPreview1.Zoom := 1.00;
  Rpt_CustScaleCbx.Text := FloattoStr(frxPreview1.Zoom*100) ;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton11Click(Sender: TObject);
begin

  frxPreview1.ZoomMode := zmPageWidth;
  Rpt_CustScaleCbx.Text := FloattoStr(frxPreview1.Zoom*100);
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton1Click(Sender: TObject);
begin
  frxPreview1.PageSetupDlg;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton2Click(Sender: TObject);
begin
  frxPreview1.Next;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton3Click(Sender: TObject);
begin
  frxPreview1.Prior;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton4Click(Sender: TObject);
begin
  if ABPubFrxReport.PagesCount > 0 then
    frxPreview1.Print;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton5Click(Sender: TObject);
begin
  frxPreview1.First;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton6Click(Sender: TObject);
begin
  frxPreview1.Last;
end;

procedure TABSys_Org_ExtendReportPrintForm.dxBarButton7Click(Sender: TObject);
begin

  frxPreview1.ZoomMode := zmWholePage;
  Rpt_CustScaleCbx.Text := FloattoStr(frxPreview1.Zoom*100);
end;

procedure TABSys_Org_ExtendReportPrintForm.Rpt_CustScaleCbxChange(Sender: TObject);
var
  tmpScale:Double;
  tmpStr:String;
begin
  if Rpt_CustScaleCbx.Items.Count > 0 then
    tmpStr := Rpt_CustScaleCbx.Text;
  tmpScale := strtoFloat(tmpStr)/100;
  frxPreview1.Zoom := tmpScale;
end;

procedure TABSys_Org_ExtendReportPrintForm.Rpt_ToPageEditKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=13 then
    frxPreview1.PageNo:=StrToIntDef(Rpt_ToPageEdit.Text,1);
end;

//打印报表
procedure TABSys_Org_ExtendReportPrintForm.ABcxButton2Click(Sender: TObject);
begin
  if (Assigned(TreeView1.Selected)) and
     (Assigned(TreeView1.Selected.Data)) and
     (ABSetDatasetRecno(FCreateDatasetExtendReport,LongInt(TreeView1.Selected.Data),'2')>0) then
  begin
    //在本窗体中预览(此打印按钮调用打印)
    if (Preview1.Checked) then
    begin
      ABPubFrxReport.Init;
      ABPubFrxReport.DefaultPrint :=FCreateDatasetExtendReport.FindField('ER_DefaultPrint').AsString;
      ABPubFrxReport.DefaultPrint :=Caption;
      ABPubFrxReport.Preview:=frxPreview1;
      ABPubFrxReport.CreateOwner:= self;
      ABPubFrxReport.DataSource:= nil;
      ABPubFrxReport.InputParamParent:= Panel8;
      ABPubFrxReport.QuoteParamParent:= nil;
      ABPubFrxReport.ReportType:= rtExtend;
      ABPubFrxReport.PrintReport(FCreateDatasetExtendReport.FindField('Er_Guid').AsString);
    end
    //在新窗体中预览(打印按钮调用打印)
    else if (Preview2.Checked) then
    begin
      ABPubFrxReport.Init;
      ABPubFrxReport.DefaultPrint :=FCreateDatasetExtendReport.FindField('ER_DefaultPrint').AsString;
      ABPubFrxReport.Preview:=nil;
      ABPubFrxReport.CreateOwner:= self;
      ABPubFrxReport.DataSource:= nil;
      ABPubFrxReport.InputParamParent:= Panel8;
      ABPubFrxReport.QuoteParamParent:= nil;
      ABPubFrxReport.ReportType:= rtExtend;
      ABPubFrxReport.PrintReport(FCreateDatasetExtendReport.FindField('Er_Guid').AsString);
    end
    //在表格中显示(打印按钮调用预览)
    else
    begin
      if not Assigned(ABPubFrxReport.DataSource) then
      begin
        ABPubFrxReport.Init;
        ABPubFrxReport.DefaultPrint :=FCreateDatasetExtendReport.FindField('ER_DefaultPrint').AsString;
        ABPubFrxReport.Preview:=nil;
        ABPubFrxReport.CreateOwner:= self;
        ABPubFrxReport.DataSource:= nil;
        ABPubFrxReport.InputParamParent:= Panel8;
        ABPubFrxReport.QuoteParamParent:= nil;
        ABPubFrxReport.ReportType:= rtExtend;
      end;

      ABPubFrxReport.PreviewReport(FCreateDatasetExtendReport.FindField('Er_Guid').AsString);
    end;
  end;
end;

procedure TABSys_Org_ExtendReportPrintForm.ABcxButton29Click(Sender: TObject);
var
  I: Integer;
begin
  ABcxButton29.Enabled:=False;
  try
    for I := 0 to TreeView1.Items.Count - 1 do
    begin
      TreeView1.Items[i].Selected:=true;
      DoShowReport;
      Application.ProcessMessages;
      Sleep(100);
    end;
  finally
    ABcxButton29.Enabled:=true;
  end;
end;

procedure TABSys_Org_ExtendReportPrintForm.ABcxButton5Click(Sender: TObject);
var
  tempFind:TTreeNode;
  function FindTreeView1:boolean;
  begin
    result:=False;
    tempFind:= ABFindTree(TreeView1.Items,TcxButton(Sender).Caption,[]);
    if Assigned(tempFind) then
    begin
      tempFind.Selected:=true;
      result:=true;
    end;
  end;
begin
  if AnsiCompareText(FLastChickButtonName,TcxButton(Sender).Caption)=0 then
  begin
    TreeView1DblClick(TreeView1);
  end
  else
  begin
    FLastChickButtonName:=TcxButton(Sender).Caption;
    if FindTreeView1 then
    begin
      //TreeView1Change(TreeView1,tempFind);
    end;
  end;
end;

procedure TABSys_Org_ExtendReportPrintForm.TreeView1Change(Sender: TObject;
  Node: TTreeNode);
var
  tempDataset:TDataSet;
  tempStr1:string;
begin
  ABPubFrxReport.DataSource:= nil;

  ABcxButton1.Enabled:=TreeView1.Selected.Count<=0;
  ABcxButton2.Enabled:=ABcxButton1.Enabled;

  if (Assigned(TreeView1.Selected)) and
     (Assigned(TreeView1.Selected.Data)) and
     (ABSetDatasetRecno(FCreateDatasetExtendReport,LongInt(TreeView1.Selected.Data),'2')>0) then
  begin
    //取FCreateDatasetExtendReport.FindField('Er_Guid').AsString下第一个SQL
    tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_ExtendReportDatasets',[FCreateDatasetExtendReport.FindField('Er_Guid').AsString]);
    tempStr1:=  ABDownToTextFromField('Main',
                 'ABSys_Org_ExtendReportDatasets','Ed_SQL',
                 'isnull(ED_ParentGuid,''0'')=''0'' and Ed_Guid='+ABQuotedStr(tempDataset.FindField('Ed_Guid').AsString));

    ABCreateInputParams(tempStr1,
                        Panel8,
                        Panel8.owner,
                        False,

                        4,
                        False,

                        0,true,
                        0,False,

                        ABDBPanelParams_Leftspacing    ,
                        ABDBPanelParams_Topspacing     ,
                        ABDBPanelParams_Righttspacing  ,
                        ABDBPanelParams_Bottomspacing  ,
                        ABDBPanelParams_Xspacing       ,
                        ABDBPanelParams_Yspacing);
  end;
  ABcxSplitter1.Left:=Panel811.Left+Panel811.Width+10;
end;

procedure TABSys_Org_ExtendReportPrintForm.Preview1Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  ChangeActivePageIndex(Preview1.Checked,Preview2.Checked,N3.Checked,N4.Checked);
end;

procedure TABSys_Org_ExtendReportPrintForm.Preview2Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  ChangeActivePageIndex(Preview1.Checked,Preview2.Checked,N3.Checked,N4.Checked);
end;

procedure TABSys_Org_ExtendReportPrintForm.Preview3Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  ChangeActivePageIndex(Preview1.Checked,Preview2.Checked,N3.Checked,N4.Checked);
end;

procedure TABSys_Org_ExtendReportPrintForm.ChangeActivePageIndex(aCurFormReport,aNewFormReport,aGrid,aPivotGrid:boolean);
begin
  ABcxButton1.ShowProgressBar:=not Preview2.Checked;

  //在frxPreview1中预览数据
  if aCurFormReport then
  begin
    ABcxPageControl1.ActivePageIndex:=0;
  end
  //在frxPreview1中预览数据
  else  if aNewFormReport then
  begin
    ABcxPageControl1.ActivePageIndex:=0;
  end
  //在Grid中预览数据
  else
  begin
    ABcxPageControl1.ActivePageIndex:=1;
    ABcxPageControl2.ActivePageIndex:=0;
    cxTabSheet5.TabVisible:=true;
    cxTabSheet6.TabVisible:=false;
    cxTabSheet7.TabVisible:=false;
    cxTabSheet8.TabVisible:=false;
    cxTabSheet9.TabVisible:=false;
    cxTabSheet10.TabVisible:=false;
    ABcxPageControl3.HideTabs:=true;

    ABcxPageControl2.HideTabs:=(not aGrid) or (not aPivotGrid);
    if ABcxPageControl2.HideTabs then
    begin
      if aPivotGrid then
      begin
        ABcxPageControl2.ActivePageIndex:=1;
      end
      else
      begin
        ABcxPageControl2.ActivePageIndex:=0;
      end;
    end;
  end;
end;

procedure TABSys_Org_ExtendReportPrintForm.TreeView1DblClick(Sender: TObject);
begin
  if (Assigned(TreeView1.Selected)) and (not TreeView1.Selected.HasChildren) then
  begin
    ABPubAutoProgressBar_ThreadU.ABRunProgressBar(ABcxButton1.ProgressBarCaption);
    try
      DoShowReport;
    finally
      ABPubAutoProgressBar_ThreadU.ABStopProgressBar;
    end;
  end;
end;

procedure TABSys_Org_ExtendReportPrintForm.ABcxButton1Click(Sender: TObject);
begin
  DoShowReport;
end;

procedure TABSys_Org_ExtendReportPrintForm.DoShowReport;
var
  I: Integer;
  tempShowType:string;
begin
  if (Assigned(TreeView1.Selected)) and
     (Assigned(TreeView1.Selected.Data)) and
     (ABSetDatasetRecno(FCreateDatasetExtendReport,LongInt(TreeView1.Selected.Data),'2')>0) then
  begin
    tempShowType:=FCreateDatasetExtendReport.FindField('ER_ShowType').AsString;
    //如果当前报表没有设置预览或显示类型，则取当前的设置
    if (tempShowType=EmptyStr) or
       (AnsiCompareText(tempShowType,'Default')=0) then
    begin
      tempShowType:=EmptyStr;
      if Preview1.Checked then
      begin
        tempShowType:='CurFormReport';
      end
      else if Preview2.Checked then
      begin
        tempShowType:='NewFormReport';
      end
      else
      begin
        tempShowType:='Grid';

        if (N3.Checked) and (not N4.Checked) then
        begin
          tempShowType:='Grid';
        end
        else if (N4.Checked) and (not N3.Checked) then
        begin
          tempShowType:='PivotGrid';
        end
        else if (N4.Checked) and (N3.Checked) then
        begin
          tempShowType:='GridAndPivotGrid';
        end;
      end;
    end;
    ChangeActivePageIndex(AnsiCompareText(tempShowType,'CurFormReport')=0,
                          AnsiCompareText(tempShowType,'NewFormReport')=0,
                          (AnsiCompareText(tempShowType,'Grid')=0) or
                          (AnsiCompareText(tempShowType,'GridAndPivotGrid')=0),
                          (AnsiCompareText(tempShowType,'PivotGrid')=0) or
                          (AnsiCompareText(tempShowType,'GridAndPivotGrid')=0)
                          );

    if AnsiCompareText(tempShowType,'CurFormReport')=0 then
    begin
      //当前窗口的预览报表
      Rpt_CustScaleCbx.Text:='100';
      Rpt_ToPageEdit.Text:='0';

      ABPubFrxReport.Init;
      ABPubFrxReport.DefaultPrint :=FCreateDatasetExtendReport.FindField('ER_DefaultPrint').AsString;
      ABPubFrxReport.Preview:=frxPreview1;
      ABPubFrxReport.CreateOwner:= self;
      ABPubFrxReport.DataSource:= nil;
      ABPubFrxReport.InputParamParent:= Panel8;
      ABPubFrxReport.QuoteParamParent:= nil;
      ABPubFrxReport.ReportType:= rtExtend;
      ABPubFrxReport.PreviewReport(FCreateDatasetExtendReport.FindField('Er_Guid').AsString);
    end
    else if AnsiCompareText(tempShowType,'NewFormReport')=0 then
    begin
      //自定义窗口的预览报表
      ABPubFrxReport.Init;
      ABPubFrxReport.DefaultPrint :=FCreateDatasetExtendReport.FindField('ER_DefaultPrint').AsString;
      ABPubFrxReport.Preview:=nil;
      ABPubFrxReport.CreateOwner:= self;
      ABPubFrxReport.DataSource:= nil;
      ABPubFrxReport.InputParamParent:= Panel8;
      ABPubFrxReport.QuoteParamParent:= nil;
      ABPubFrxReport.ReportType:= rtExtend;
      ABPubFrxReport.PreviewReport(FCreateDatasetExtendReport.FindField('Er_Guid').AsString);
    end
    else
    begin
      //在表格中预览
      ABPubFrxReport.Init;
      ABPubFrxReport.DefaultPrint :=FCreateDatasetExtendReport.FindField('ER_DefaultPrint').AsString;
      ABPubFrxReport.Preview:=nil;
      ABPubFrxReport.CreateOwner:= self;
      ABPubFrxReport.DataSource:= nil;
      ABPubFrxReport.InputParamParent:= Panel8;
      ABPubFrxReport.QuoteParamParent:= nil;
      ABPubFrxReport.ReportType:= rtExtend;
      ABPubFrxReport.CreateRes(FCreateDatasetExtendReport.FindField('Er_Guid').AsString);

      if High(ABPubFrxReport.FExtendDataSources)-low(ABPubFrxReport.FExtendDataSources)>=0 then
      begin
        if (AnsiCompareText(tempShowType,'Grid')=0) or
           (AnsiCompareText(tempShowType,'GridAndPivotGrid')=0) then
        begin
          cxTabSheet5.TabVisible:=false;
          cxTabSheet6.TabVisible:=false;
          cxTabSheet7.TabVisible:=false;
          cxTabSheet8.TabVisible:=false;
          cxTabSheet9.TabVisible:=false;
          cxTabSheet10.TabVisible:=false;

          if High(ABPubFrxReport.FExtendDataSources)+1>1 then
          begin
            ABcxPageControl3.HideTabs:=false;
          end
          else
          begin
            ABcxPageControl3.HideTabs:=True;
          end;

          for I := Low(ABPubFrxReport.FExtendDataSources) to High(ABPubFrxReport.FExtendDataSources) do
          begin
            case i+1 of
              1:
              begin
                ABcxGridDBBandedTableView1.DataController.DataSource:=ABPubFrxReport.FExtendDataSources[i];
                cxTabSheet5.TabVisible:=true;

                ABcxGridDBBandedTableView1.ClearItems;
                ABcxGridDBBandedTableView1.ExtPopupMenu.SetupFileNameSuffix:=
                  TreeView1.Selected.Parent.Text+'_'+
                  TreeView1.Selected.Text+'_'+'新分类';

                TABcxGridDBBandedTableView(ABcxGridDBBandedTableView1).CreateAllColumn;
              end;
              2:
              begin
                ABcxGridDBBandedTableView2.ExtPopupMenu.SetupFileNameSuffix:=
                  TreeView1.Selected.Parent.Text+'_'+
                  TreeView1.Selected.Text+'_'+'新分类'+inttostr(1);
                ABcxGridDBBandedTableView2.ClearItems;

                ABcxGridDBBandedTableView2.DataController.DataSource:=ABPubFrxReport.FExtendDataSources[i];
                cxTabSheet6.Visible:=True;

                TABcxGridDBBandedTableView(ABcxGridDBBandedTableView2).CreateAllColumn;
              end;
              3:
              begin
                ABcxGridDBBandedTableView3.ExtPopupMenu.SetupFileNameSuffix:=
                  TreeView1.Selected.Parent.Text+'_'+
                  TreeView1.Selected.Text+'_'+'新分类'+inttostr(1);
                ABcxGridDBBandedTableView3.ClearItems;

                ABcxGridDBBandedTableView3.DataController.DataSource:=ABPubFrxReport.FExtendDataSources[i];
                cxTabSheet7.Visible:=True;

                TABcxGridDBBandedTableView(ABcxGridDBBandedTableView3).CreateAllColumn;
              end;
              4:
              begin
                ABcxGridDBBandedTableView4.ExtPopupMenu.SetupFileNameSuffix:=
                  TreeView1.Selected.Parent.Text+'_'+
                  TreeView1.Selected.Text+'_'+'新分类'+inttostr(1);
                ABcxGridDBBandedTableView4.ClearItems;

                ABcxGridDBBandedTableView4.DataController.DataSource:=ABPubFrxReport.FExtendDataSources[i];
                cxTabSheet8.Visible:=True;

                TABcxGridDBBandedTableView(ABcxGridDBBandedTableView4).CreateAllColumn;
              end;
              5:
              begin
                ABcxGridDBBandedTableView5.ExtPopupMenu.SetupFileNameSuffix:=
                  TreeView1.Selected.Parent.Text+'_'+
                  TreeView1.Selected.Text+'_'+'新分类'+inttostr(1);
                ABcxGridDBBandedTableView5.ClearItems;

                ABcxGridDBBandedTableView5.DataController.DataSource:=ABPubFrxReport.FExtendDataSources[i];
                cxTabSheet9.Visible:=True;

                TABcxGridDBBandedTableView(ABcxGridDBBandedTableView5).CreateAllColumn;
              end;
              6:
              begin
                ABcxGridDBBandedTableView6.ExtPopupMenu.SetupFileNameSuffix:=
                  TreeView1.Selected.Parent.Text+'_'+
                  TreeView1.Selected.Text+'_'+'新分类'+inttostr(1);
                ABcxGridDBBandedTableView6.ClearItems;

                ABcxGridDBBandedTableView6.DataController.DataSource:=ABPubFrxReport.FExtendDataSources[i];
                cxTabSheet10.Visible:=True;

                TABcxGridDBBandedTableView(ABcxGridDBBandedTableView6).CreateAllColumn;
              end;
            end;
          end;
          ABcxPageControl3.ActivePageIndex:=0;
        end;
        if (AnsiCompareText(tempShowType,'PivotGrid')=0) or
           (AnsiCompareText(tempShowType,'GridAndPivotGrid')=0) then
        begin
          ABcxDBPivotGrid1.DeleteAllFields;
          ABcxDBPivotGrid1.ExtPopupMenu.SetupFileNameSuffix:=
                  TreeView1.Selected.Parent.Text+'_'+
                  TreeView1.Selected.Text;
          ABcxDBPivotGrid1.DataSource:=ABPubFrxReport.FExtendDataSources[0];
          ABcxDBPivotGrid1.ExtPopupMenu.load;
          ABcxDBPivotGrid1.ExpandAll;
        end;

        ABPubFrxReport.DataSource:=ABPubFrxReport.FExtendDataSources[0];
      end;
    end;
  end;
end;


Initialization
  RegisterClass(TABSys_Org_ExtendReportPrintForm);
Finalization
  UnRegisterClass(TABSys_Org_ExtendReportPrintForm);

end.

