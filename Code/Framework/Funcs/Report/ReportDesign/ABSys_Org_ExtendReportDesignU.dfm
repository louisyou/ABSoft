object ABSys_Org_ExtendReportDesignForm: TABSys_Org_ExtendReportDesignForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = #25193#23637#25253#34920#35774#35745
  ClientHeight = 550
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 900
    Height = 513
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter4: TABcxSplitter
      Left = 179
      Top = 0
      Width = 8
      Height = 513
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Panel6
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 179
      Height = 513
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter5: TABcxSplitter
        Left = 0
        Top = 200
        Width = 179
        Height = 8
        Cursor = crVSplit
        HotZoneClassName = 'TcxMediaPlayer8Style'
        AlignSplitter = salTop
        InvertDirection = True
        Control = ABcxDBTreeView1
      end
      object ABDBcxGrid1: TABcxGrid
        Left = 0
        Top = 208
        Width = 179
        Height = 305
        Align = alClient
        TabOrder = 0
        LookAndFeel.NativeStyle = False
        object ABDBcxGrid1DBBandedTableView1: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABDBcxGrid1DBBandedTableView1
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource1_1
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.ExpandButtonsForEmptyDetails = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABDBcxGrid1DBBandedTableView1
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object ABDBcxGrid1Level1: TcxGridLevel
          GridView = ABDBcxGrid1DBBandedTableView1
        end
      end
      object ABcxDBTreeView1: TABcxDBTreeView
        Left = 0
        Top = 0
        Width = 179
        Height = 200
        Align = alTop
        Bands = <
          item
          end>
        DataController.DataSource = ABDatasource1
        DataController.ParentField = 'Ti_ParentGuid'
        DataController.KeyField = 'Ti_Guid'
        DragMode = dmAutomatic
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Navigator.Buttons.CustomButtons = <>
        OptionsBehavior.ExpandOnIncSearch = True
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsData.Deleting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRect = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Headers = False
        ParentFont = False
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.DefaultParentValue = '0'
        RootValue = -1
        TabOrder = 1
        Active = True
        ExtFullExpand = False
        CanSelectParent = True
        object ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn
          DataBinding.FieldName = 'Ti_Name'
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn
          Visible = False
          DataBinding.FieldName = 'Ti_Order'
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          SortOrder = soAscending
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
    end
    object Panel7: TPanel
      Left = 405
      Top = 0
      Width = 495
      Height = 513
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object ABcxPageControl1: TABcxPageControl
        Left = 0
        Top = 0
        Width = 495
        Height = 513
        Align = alClient
        TabOrder = 0
        Properties.ActivePage = cxTabSheet1
        Properties.CustomButtons.Buttons = <>
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        OnChange = ABcxPageControl1Change
        ActivePageIndex = 0
        ClientRectBottom = 512
        ClientRectLeft = 1
        ClientRectRight = 494
        ClientRectTop = 21
        object cxTabSheet1: TcxTabSheet
          Caption = #25968#25454#28304'  '
          ImageIndex = 0
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Splitter1: TABcxSplitter
            Left = 135
            Top = 0
            Width = 8
            Height = 491
            HotZoneClassName = 'TcxMediaPlayer8Style'
            InvertDirection = True
            Control = Panel3
          end
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 135
            Height = 491
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Splitter2: TABcxSplitter
              Left = 0
              Top = 145
              Width = 135
              Height = 8
              Cursor = crVSplit
              HotZoneClassName = 'TcxMediaPlayer8Style'
              AlignSplitter = salTop
              InvertDirection = True
              Control = ABcxDBTreeView2
            end
            object Panel9: TPanel
              Left = 0
              Top = 153
              Width = 135
              Height = 338
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel9'
              TabOrder = 0
              object ListBox1: TABcxListBox
                Left = 0
                Top = 20
                Width = 135
                Height = 318
                Align = alClient
                ImeName = #24555#20048#20116#31508
                ItemHeight = 13
                Style.LookAndFeel.Kind = lfFlat
                StyleDisabled.LookAndFeel.Kind = lfFlat
                StyleFocused.LookAndFeel.Kind = lfFlat
                StyleHot.LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnDblClick = ListBox1DblClick
              end
              object Panel10: TPanel
                Left = 0
                Top = 0
                Width = 135
                Height = 20
                Align = alTop
                BevelOuter = bvNone
                Caption = #21487#29992#30340#36755#20837#25511#20214#31867#22411
                TabOrder = 1
              end
            end
            object ABcxDBTreeView2: TABcxDBTreeView
              Left = 0
              Top = 0
              Width = 135
              Height = 145
              Align = alTop
              Bands = <
                item
                end>
              DataController.DataSource = ABDatasource1_1_1
              DataController.ParentField = 'Ed_ParentGuid'
              DataController.KeyField = 'Ed_Guid'
              DragMode = dmAutomatic
              Navigator.Buttons.CustomButtons = <>
              OptionsBehavior.ExpandOnIncSearch = True
              OptionsBehavior.IncSearch = True
              OptionsData.Inserting = True
              OptionsSelection.CellSelect = False
              OptionsSelection.HideFocusRect = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.Headers = False
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.DefaultParentValue = '0'
              RootValue = -1
              TabOrder = 1
              OnClick = ABcxDBTreeView2Click
              OnDblClick = ABcxDBTreeView2DblClick
              Active = True
              ExtFullExpand = False
              CanSelectParent = True
              object cxDBTreeListColumn1: TcxDBTreeListColumn
                DataBinding.FieldName = 'Ed_Name'
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object ABcxDBTreeView2cxDBTreeListColumn1: TcxDBTreeListColumn
                Visible = False
                DataBinding.FieldName = 'Ed_Order'
                Position.ColIndex = 1
                Position.RowIndex = 0
                Position.BandIndex = 0
                SortOrder = soAscending
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
          end
          object Panel11: TPanel
            Left = 143
            Top = 0
            Width = 350
            Height = 491
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 350
              Height = 491
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel12'
              TabOrder = 0
              object ABcxPageControl2: TABcxPageControl
                Left = 0
                Top = 0
                Width = 350
                Height = 491
                Align = alClient
                TabOrder = 0
                Properties.ActivePage = cxTabSheet4
                Properties.CustomButtons.Buttons = <>
                LookAndFeel.Kind = lfFlat
                ActivePageIndex = 0
                ClientRectBottom = 487
                ClientRectLeft = 2
                ClientRectRight = 346
                ClientRectTop = 22
                object cxTabSheet4: TcxTabSheet
                  Caption = 'SQL'#20869#23481
                  ImageIndex = 0
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Memo1: TABcxMemo
                    Left = 0
                    Top = 0
                    Align = alClient
                    Properties.ScrollBars = ssBoth
                    Style.LookAndFeel.Kind = lfFlat
                    StyleDisabled.LookAndFeel.Kind = lfFlat
                    StyleFocused.LookAndFeel.Kind = lfFlat
                    StyleHot.LookAndFeel.Kind = lfFlat
                    TabOrder = 0
                    OnExit = Memo1Exit
                    Height = 411
                    Width = 344
                  end
                  object ABDBPanel1: TABDBPanel
                    Left = 0
                    Top = 411
                    Width = 344
                    Height = 54
                    Align = alBottom
                    BevelOuter = bvNone
                    ShowCaption = False
                    TabOrder = 1
                    ReadOnly = False
                    DataSource = ABDatasource1_1_1
                    AddAnchors_akRight = True
                    AddAnchors_akBottom = True
                    AutoHeight = True
                    AutoWidth = True
                    ExplicitTop = 415
                    ExplicitWidth = 348
                  end
                end
                object cxTabSheet5: TcxTabSheet
                  Caption = 'SQL'#20889#27861
                  ImageIndex = 1
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Memo2: TABcxMemo
                    Left = 0
                    Top = 0
                    Align = alClient
                    Lines.Strings = (
                      'SQL'#20889#27861
                      'SQL SERVER'#25903#25345#30340#20889#27861#37117#21487#20197#65288#20165#21442#25968#37096#20998#26159#30001#26694#26550#35299#26512#65292#35299#26512#23436#25104#21518#36865'SQLServer '#25191#34892#65289
                      #25191#34892#37096#20998#29992'[ExceSQLBegin]'#19982'[ExceSQLEnd]'#20316#20026#24320#22987#19982#32467#26463#31526
                      #21462#25968#25454#37096#20998#20197'select '#35821#21477#36820#22238
                      ''
                      #20854#20013#21442#25968#26684#24335#30001'[Begin]'#19982'[End]'#20316#20026#24320#22987#19982#32467#26463#31526#65292#35828#26126#22914#19979
                      #26684#24335#35828#26126':'
                      '[Begin]--'#24320#22987#31526
                      '[ClassName]--'#36755#20837#25511#20214#31867#21517#65292#20174#8220#21487#29992#30340#36755#20837#25511#20214#31867#22411#8221#20013#36873#25321
                      '[Caption]--'#36755#20837#25511#20214#26631#31614#21517
                      '[Name]--'#36755#20837#25511#20214#30340#21517#31216#65292#19968#33324#19981#20351#29992#65288#20869#37096#24050#30001#26631#31614#21517#20195#30721#65289
                      '[MinCharNum]--'#36755#20837#25511#20214#26631#31614#26368#23567#30340#23383#31526#38271#24230
                      '[DefaultValue]--'#36755#20837#25511#20214#30340#40664#35748#20540
                      '[FillSql]--'#36755#20837#25511#20214#30340#25968#25454#26469#28304
                      '[PatherField]--'#36755#20837#25511#20214#20026#26641#24418#36873#25321#26102#65292#25351#23450#29238#23383#27573
                      '[Keyfield]--'#36755#20837#25511#20214#20027#38190#23383#27573
                      '[ListField]--'#36755#20837#25511#20214#26174#31034#23383#27573
                      '[DownField]--'#36755#20837#25511#20214#19979#25289#21015#34920#23383#27573
                      '[ValueReplaceName]--'#22312#20840#37096'SQL'#20013#26367#25442#30340#28304#20018
                      '[ValueReplaceValue]--'#22312#20840#37096'SQL'#20013#26367#25442#30340#30446#26631#20018#65292#30446#26631#20018#20013#36890#36807' CurValue'#26631#24535#26469#24341#29992#24403#21069#21442#25968#30340#20540
                      '[LabelTop][Labelleft][LabelWidth][LabelHeight]--'#36755#20837#25511#20214#26631#31614#30340#20301#32622#19982#22823#23567
                      
                        '[ControlTop][Controlleft][ControlWidth][ControlHeight]--'#36755#20837#25511#20214#20301#32622#19982#22823 +
                        #23567
                      '[End]--'#32467#26463#31526
                      ''
                      #31034#20363'SQL'#22914#19979
                      '[ExceSQLBegin]'
                      'exec InitReport'
                      '[ExceSQLEnd]'
                      ''
                      'declare @inputBegDatetime datetime'
                      'declare @inputEndDatetime datetime'
                      'declare @inputBn_Code varchar(100) '
                      'declare @inputcr_ct_Name varchar(8000)'
                      'declare @inputCr_Op_Name varchar(8000)'
                      ''
                      
                        'set @inputBegDatetime    =[begin][ClassName]TABcxDatetimeEdit[Ca' +
                        'ption]'#26085#26399#36215'[DefaultValue]select GetDate()[Controlleft]83 [End]'
                      
                        'set @inputEndDatetime    =[begin][ClassName]TABcxDatetimeEdit[Ca' +
                        'ption]'#26085#26399#27490'[DefaultValue]select GetDate()[Controlleft]83[End]'
                      
                        'set @inputBn_Code        =[begin][ClassName]TABcxLookupComboBox[' +
                        'Caption]'#38065#21253'[FillSql]SELECT * FROM ABSys_Org_TreeItem[DownField]Ti' +
                        '_code,Ti_Name[Keyfield]Ti_Cod[ListField]Ti_Name[Controlleft]83[E' +
                        'nd]'
                      
                        'set @inputcr_ct_Name     =[begin][ClassName]TABcxListBox [Captio' +
                        'n]'#20132#26131#31867#22411'[DefaultValue][FillSql]select Ti_Name from absys_org_treeI' +
                        'TEM[End]'
                      
                        'set @inputCr_Op_Name     =[begin][ClassName]TABcxCheckComboBox [' +
                        'Caption]'#25805#20316#20154#21592'[DefaultValue][FillSql]select Op_Name from ABSys_Org' +
                        '_Operator[End]'
                      ''
                      'select  *'
                      'from AIC_Consume_ConsumeRecord '
                      
                        'where      ( @inputcr_ct_Name ='#39#39' or charindex( '#39','#39'+cr_ct_Name +' +
                        #39','#39', '#39','#39'+@inputcr_ct_Name +'#39','#39')>0 )  and  '
                      
                        '           ( @inputCr_Op_Name    ='#39#39' or charindex( '#39','#39'+ Cr_Op_Na' +
                        'me +'#39','#39', '#39','#39'+ @inputCr_Op_Name    +'#39','#39')>0 )  and'
                      '           ( @inputBn_Code=ED_Bn_Code) and'
                      '           (ED_msgtime>= @inputBegDatatime ) and '
                      '           (ED_msgtime< @inputEndDatatime  )  '
                      ''
                      '')
                    ParentFont = False
                    Properties.ScrollBars = ssBoth
                    Style.LookAndFeel.Kind = lfFlat
                    StyleDisabled.LookAndFeel.Kind = lfFlat
                    StyleFocused.LookAndFeel.Kind = lfFlat
                    StyleHot.LookAndFeel.Kind = lfFlat
                    TabOrder = 0
                    ExplicitWidth = 348
                    ExplicitHeight = 469
                    Height = 465
                    Width = 344
                  end
                end
              end
            end
          end
        end
        object cxTabSheet2: TcxTabSheet
          Caption = #25968#25454#28304#21015#34920#25968#25454
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object ABDBcxGrid2: TABcxGrid
            Left = 0
            Top = 0
            Width = 493
            Height = 472
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = False
            object ABDBcxGrid2ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = ABDBcxGrid2ABcxGridDBBandedTableView1
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ABDatasource2
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.MultiSelect = True
              OptionsView.CellAutoHeight = True
              OptionsView.GroupByBox = False
              OptionsView.BandHeaders = False
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = ABDBcxGrid2ABcxGridDBBandedTableView1
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object ABDBcxGrid2Level1: TcxGridLevel
              GridView = ABDBcxGrid2ABcxGridDBBandedTableView1
            end
          end
          object ABDBStatusBar1: TABdxDBStatusBar
            Left = 0
            Top = 472
            Width = 493
            Height = 19
            Panels = <
              item
                PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
                Width = 50
              end>
            PaintStyle = stpsUseLookAndFeel
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            StopUpdate = False
            DataSource = ABDatasource2
          end
        end
        object cxTabSheet3: TcxTabSheet
          Caption = #25968#25454#28304#20132#21449#25968#25454
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object ABcxDBPivotGrid1: TABcxDBPivotGrid
            Left = 0
            Top = 0
            Width = 493
            Height = 491
            Align = alClient
            DataSource = ABDatasource2
            Groups = <>
            OptionsView.ColumnFields = False
            OptionsView.ColumnGrandTotalText = #24635#35745
            OptionsView.DataFields = False
            OptionsView.FilterFields = False
            OptionsView.RowGrandTotalText = #24635#35745
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.LinkcxDBPivotGrid = ABcxDBPivotGrid1
            TabOrder = 0
          end
        end
      end
    end
    object Panel811: TPanel
      Left = 187
      Top = 0
      Width = 210
      Height = 513
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      PopupMenu = PopupMenu2
      TabOrder = 3
      object Panel8: TPanel
        Left = 1
        Top = 1
        Width = 208
        Height = 511
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
    object ABcxSplitter1: TABcxSplitter
      Left = 397
      Top = 0
      Width = 8
      Height = 513
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Panel811
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 513
    Width = 900
    Height = 37
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      900
      37)
    object ABcxButton1: TABcxButton
      Left = 188
      Top = 6
      Width = 80
      Height = 25
      Caption = #35774#35745
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = ABcxButton1Click
      ShowProgressBar = False
    end
    object ABcxButton2: TABcxButton
      Left = 274
      Top = 6
      Width = 80
      Height = 25
      Caption = #25171#21360
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = ABcxButton2Click
      ShowProgressBar = False
    end
    object ABcxButton3: TABcxButton
      Left = 8
      Top = 6
      Width = 56
      Height = 25
      Caption = #26032#22686
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      OnClick = ABcxButton3Click
      ShowProgressBar = False
    end
    object ABcxButton4: TABcxButton
      Left = 65
      Top = 6
      Width = 56
      Height = 25
      Caption = #21024#38500
      LookAndFeel.Kind = lfFlat
      TabOrder = 3
      OnClick = ABcxButton4Click
      ShowProgressBar = False
    end
    object Button3: TABcxButton
      Left = 804
      Top = 6
      Width = 80
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      LookAndFeel.Kind = lfFlat
      TabOrder = 4
      OnClick = Button3Click
      ShowProgressBar = False
    end
    object ABcxButton5: TABcxButton
      Left = 122
      Top = 6
      Width = 56
      Height = 25
      Caption = #20462#25913
      LookAndFeel.Kind = lfFlat
      TabOrder = 5
      OnClick = ABcxButton5Click
      ShowProgressBar = False
    end
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 111
    Top = 31
  end
  object ABQuery1: TABQuery
    AfterInsert = ABQuery1AfterInsert
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * from ABSys_Org_TreeItem '
      'where Ti_Tr_Guid=dbo.Func_GetTreeGuid('#39'Extend ReportType'#39')  and'
      '           Ti_Code<>'#39'PublicQuery'#39
      'order by TI_TR_Guid,TI_Group,TI_Order')
    SqlUpdateDatetime = 42095.493280266200000000
    BeforeDeleteAsk = True
    FieldDefaultValues = 'Ti_Tr_Guid=select dbo.Func_GetTreeGuid('#39'Extend ReportType'#39') '
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_TreeItem'
        Fields = 'TI_TR_Guid;TI_Name'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_1'
        Fields = 'TI_TR_Guid;TI_Order'
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_2'
        Fields = 'TI_TR_Guid;TI_Code'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_TreeItem'
        Fields = 'TI_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 49
    Top = 23
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1_1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'TI_Guid'
    DetailFields = 'ER_TI_Guid'
    SQL.Strings = (
      'select  '
      'GetNoBigDateFieldNames('#39'ABSys_Org_ExtendReport'#39') '
      'from ABSys_Org_ExtendReport'
      'where ER_TI_Guid=:TI_Guid')
    SqlUpdateDatetime = 42218.447982824070000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_ExtendReport')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_ExtendReport')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 63
    Top = 126
    ParamData = <
      item
        Name = 'Ti_Guid'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 96
    Top = 125
  end
  object ABQuery1_1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1_1
    MasterFields = 'ER_Guid'
    DetailFields = 'ED_ER_Guid'
    SQL.Strings = (
      'select  '
      'GetNoBigDateFieldNames('#39'ABSys_Org_ExtendReportDatasets'#39') '
      'from ABSys_Org_ExtendReportDatasets'
      'where ED_ER_Guid=:ER_Guid')
    SqlUpdateDatetime = 42218.444615671300000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_ExtendReportDatasets')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_ExtendReportDatasets')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 62
    Top = 155
    ParamData = <
      item
        Name = 'Er_Guid'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1_1
    Left = 95
    Top = 155
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select *'#13#10' from ERP_Base_Cust'#13#10)
    ConnName = 'Main'
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    EditControlType = ctSingle
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_Cust')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ERP_Base_Cust')
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 620
    Top = 442
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2
    Left = 658
    Top = 442
  end
  object PopupMenu2: TPopupMenu
    Left = 234
    Top = 353
    object MenuItem1: TMenuItem
      Caption = #20445#23384'('#20301#32622'+'#23485#39640')'
      OnClick = MenuItem1Click
    end
    object N7: TMenuItem
      Caption = #20445#23384'('#25511#20214#24038#36317')'
      OnClick = MenuItem1Click
    end
  end
end
